$( document ).ready(function() {
    var total_indice = parseInt($('#indice').text())
    var total_placa = parseFloat($('#placa').text())
    var total_azucar = parseInt($('#azucar').text())

    if(total_indice<=2){
        $('#cpo_bajo').attr("class", "table-primary")
    } else if(total_indice<=4){
        $('#cpo_medio').attr("class", "table-primary")
    } else{
        $('#cpo_alto').attr("class", "table-primary")
    }

    //Placa
    if(total_placa<=20){
        $('#placa_bajo').attr("class", "table-primary")
    } else if(total_placa<=50){
        $('#placa_medio').attr("class", "table-primary")
    } else{
        $('#placa_alto').attr("class", "table-primary")
    }

    //Azucar
    if(total_azucar<=4){
        $('#azucar_bajo').attr("class", "table-primary")
    } else if(total_azucar<=7){
        $('#azucar_medio').attr("class", "table-primary")
    } else{
        $('#azucar_alto').attr("class", "table-primary")
    }

});