function onSelectItem(item) {
    // prepare the data
    var source = {
        dataType: "json",
        dataFields: [
            {name: 'id', type: 'integer'},
            {name: 'label', type: 'string'},
            {name: 'tipo', type: 'string'},
            {name: 'ponderacion', type: 'string'}
        ],
        id: 'id',
        url: routes.evaluaciones + "?idPadre=" + ((item.id == 'unidad') ? "" : item.id) + "&idUnidadIntegracion=" + item.value.idUnidadIntegracion
    };


    if (item.id == 'unidad' || item.value.tipoEvaluacion == 'calculada') {
        $('#btnAdd').show();
    } else {
        $('#btnAdd').hide();
    }


    var dataAdapter = new $.jqx.dataAdapter(source);
    $("#dataTable").jqxDataTable('clear');
    $("#dataTable").jqxDataTable(
        {
            localization: getLocalization(),
            pageable: false,
            width: '100%',
            height: '100%',
            pagerButtonsCount: 10,
            source: dataAdapter,
            columnsResize: true,
            columns: [
                {text: 'Actividad', dataField: 'label'},
                {text: 'Tipo', dataField: 'tipo', width: 100},
                {
                    text: 'Porcentaje', dataField: 'ponderacion', width: 100, cellsAlign: 'right',
                    cellsRenderer: function (row, column, value, rowData) {
                        return value + "%";
                    }
                },
                {
                    text: 'Acción', width: 200, dataField: 'id', cellsRenderer: function (row, column, value, rowData) {
                        var image = "<button onclick=\"onEdit(" + value + ")\" class=\"btn btn-info active btn-sm\" type=\"button\" style=\"height:32px;margin-right:5px;\">" +
                            "<i class=\"fa fa-edit\"></i></button>\n" +
                            "<button onclick=\"onRemove(" + value + "," + row + ")\"  class=\"btn btn-danger active btn-sm\" type=\"button\" style=\"height:32px;\">" +
                            "<i class=\"fa fa-trash\"></i></button>";
                        return image;
                    }
                },
            ],
        }
    );
}