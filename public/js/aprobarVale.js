
$(document).ready(function () {
var id_vale=$('#vale_id').val();
var id_almacen=$('#almacen_id').val();
var vale_estado=$('#vale_estado').val();

var url = '/almacen/vale/movimientos/list/'+parseInt(id_vale);

if(vale_estado==='aprobado'){
    $('#aprobar').html('Aprobado');
    $('#aprobar').attr('disabled','disabled');
}
var bool=$('#is_edit').val();
if(bool==='true'){
    $.get(url, function (data) {

        if(data.length>0){
            $.each(data, function (i, item) {
                var tds = $('#materiales_tabla').children('tbody').children('tr:last').children('td').length;

                if(tds===6 || tds===0){
                    var row='<tr>' +
                        '<td id="'+item.t_id+'" >'+ item.a_nombre+'('+item.su_nombre+')' +'</td>'+
                        '<td id="'+item.t_id+'"><input type="number"  style="width: 70%"  required min="0" value="'+-1*item.m_cantidad+'" data-idtratamiento="'+item.t_id+'" data-idpresentacion="'+item.ap_id+'" ></td>'+
                        '</tr>';
                    $('#materiales_tabla tr:last').after('<tr>'+row+'</tr>');
                }else{
                    $('#materiales_tabla > tbody>tr:last').append('<td id="'+item.t_id+'" >'+ item.a_nombre+'('+item.su_nombre+')' +'</td>'
                        +'<td id="'+item.t_id+'" ><input type="number" style="width: 70%"  required min="0" value="'+-1*item.m_cantidad+'" data-idtratamiento="'+item.t_id+'" data-idpresentacion="'+item.ap_id+'" ></td>');
                }
            });
        }
    }).fail(function () {
        console.error('Falló');
    });
}else {
    $.get(url, function (data) {
        if(data.length>0){
            $.each(data, function (i, item) {
                var tds = $('#materiales_tabla').children('tbody').children('tr:last').children('td').length;
                if(tds===6 || tds===0){
                    var row='<tr>' +
                        '<td >'+ item.a_nombre+'('+item.su_nombre+')' +'</td>'+
                        '<td >'+-1*item.m_cantidad+'</td>'+
                        '</tr>';
                    $('#materiales_tabla tr:last').after('<tr>'+row+'</tr>');
                }else{
                    $('#materiales_tabla > tbody>tr:last').append('<td >'+ item.a_nombre+'('+item.su_nombre+')' +'</td>'
                        +'<td >'+-1*item.m_cantidad+'</td>');
                }
            });
        }
    }).fail(function () {
        console.error('Falló');
    });

}

$('#aprobar').click(function(event){
    var url = '/almacen/vale/movimientos/validar/'+parseInt(id_vale)+'/'+parseInt(id_almacen);
    var urlAprobar = '/almacen/vale/movimientos/aprobar/'+parseInt(id_vale)+'/'+parseInt(id_almacen);
    $.get(url, function (data) {
        if(data.error===1){
            bootbox.alert(data.message);
        }else{
            $.get(urlAprobar, function (data) {
                if (data.error === 0) {
                    bootbox.alert(data.message);
                    $('#aprobar').html('Aprobado');
                    $('#aprobar').attr('disabled','disabled');
                    $('#edit_vale').css({"pointer-events":" none","background-color": "#8a9096"});

                } else {
                    bootbox.alert(data);
                }
            });
        }
    }).fail(function () {
        console.error('Falló');
    });
});

});