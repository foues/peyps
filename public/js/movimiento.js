$(document).ready(function () {
    var almacen = $('#almacen').val();
    $('.table').dataTable({
        "oLanguage": {
            "sInfoFiltered": "(filtrados de _MAX_ artículos en total)",
            "sInfo": "Viendo _START_ a _END_ de _TOTAL_ artículos"
        },
        "order": [[1, "asc"]],
    });

    var articulo_select = $('#articulo');
    var presentacion_select = $('#presentacion');
    var cantidad_previa_input = $('#cantidad_previa');
    var cantidad_input = $('#cantidad');
    var precio_previo_input = $('#precio_previo');
    var precio_input = $('#precio');
    var adquisicion_input = $('#adquisicion');
    var vencimiento_input = $('#vencimiento');
    var subunidad_label;
    var subunidad_cantidad;

    articulo_select.select2({width: '100%', dropdownParent: $('#existencia_modal')});
    presentacion_select.select2({width: '100%', dropdownParent: $('#existencia_modal')});

    $('.btn-cargar').click(function (event) {
        var url = '/almacen/' + almacen + '/articulos';
        var placeholder = '<option selected disabled>Seleccione</option>';
        $.get(url, function (data) {
            articulo_select.html("");
            articulo_select.append(placeholder);
            $.each(data, function (i, item) {
                var option = '<option value="' + item.id + '" >' + item.nombre + '</option>';
                articulo_select.append(option)
            });
        }).fail(function () {
            console.error('Falló');
        });
    });

    articulo_select.change(function (event) {
        var articulo = articulo_select.val();
        if (articulo) {
            var url = '/almacen/' + almacen + '/articulo/' + articulo + '/presentacion';
            var placeholder = '<option selected disabled>Seleccione</option>';
            $.get(url, function (data) {
                presentacion_select.html("");
                presentacion_select.append(placeholder);
                $.each(data, function (i, item) {
                    var option = '<option value="' + item.presentacion_id + '"'
                        + ' data-subunidad="' + item.subunidad_nombre + '"'
                        + ' data-cantidad="' + item.presentacion_cantidad + '">'
                        + item.unidad_nombre + ' (' + item.presentacion_cantidad + ' ' + item.subunidad_nombre +
                        ')' + '</option>';
                    presentacion_select.append(option)
                });
            }).fail(function () {
                console.error('Falló');
            });
        }
    });

    presentacion_select.change(function (event) {
        subunidad_label = presentacion_select.find('option:selected').data('subunidad');
        subunidad_cantidad = presentacion_select.find('option:selected').data('cantidad');
        $('#subunidad_label').text(subunidad_label);
    });

    cantidad_previa_input.on('input change', function (event) {
        var cantidad = subunidad_cantidad * cantidad_previa_input.val();
        cantidad_input.val(cantidad);
    });

    precio_previo_input.on('input change', function (event) {
        var precio = precio_previo_input.val() / cantidad_input.val();
        precio_input.val(Number(precio.toFixed(4)));
    });

    $('#form_cargar').submit(function (event) {
        event.preventDefault();
        var url = '/almacen/' + almacen + '/movimiento/cargar';
        var data = $(this).serialize();
        $.post(url, data, function (data) {
            if (data.error > 0) {
                //console.error(data.message);
                bootbox.alert(data.message);
            } else {
                bootbox.alert(data.message);
                $('#existencia_modal').modal('hide');
                location.reload();
            }
        });
    });

    $('#existencia_modal').on('hidden.bs.modal', function () {
        $(this).find('form').trigger('reset');
        $('#subunidad_desc_label').text('');
        $('#subunidad_desc_label2').text('');
        articulo_select.empty();
        presentacion_select.empty();
        cantidad_previa_input.text('');
        cantidad_input.text('');
        precio_previo_input.text('');
        precio_input.text('');
        adquisicion_input.text('');
        vencimiento_input.text('');
    });


    //Descargar existencia
    var articulo_desc_select = $('#articulo_desc');
    var presentacion_desc_select = $('#presentacion_desc');
    var cantidad_previa_desc_input = $('#cantidad_previa_desc');
    var cantidad_desc_input = $('#cantidad_desc');

    var subunidad_desc_label;
    var subunidad_cantidad_desc;

    articulo_desc_select.select2({width: '100%', dropdownParent: $('#existencia_descarga_modal')});
    presentacion_desc_select.select2({width: '100%', dropdownParent: $('#existencia_descarga_modal')});

    $('.btn-descargar').click(function (event) {
        var url = '/almacen/' + almacen + '/articulosEnExistencia';
        var placeholder = '<option selected disabled>Seleccione</option>';
        $.get(url, function (data) {
            articulo_desc_select.html("");
            articulo_desc_select.append(placeholder);
            $.each(data, function (i, item) {
                var option = '<option value="' + item.a_id + '" >' + item.a_nombre + '</option>';
                articulo_desc_select.append(option)
            });
        }).fail(function () {
            console.error('Falló');
        });
    });

    articulo_desc_select.change(function (event) {
        var articulo = articulo_desc_select.val();
        if (articulo) {
            var url = '/almacen/' + almacen + '/articulo/' + articulo + '/presentacion';
            var placeholder = '<option selected disabled value="null">Seleccione</option>';
            $.get(url, function (data) {
                presentacion_desc_select.html("");
                presentacion_desc_select.append(placeholder);
                $.each(data, function (i, item) {
                    var option = '<option value="' + item.presentacion_id + '"'
                        + ' data-subunidad="' + item.subunidad_nombre + '"'
                        + ' data-cantidad="' + item.presentacion_cantidad + '">'
                        + item.unidad_nombre + ' (' + item.presentacion_cantidad + ' '
                        + item.subunidad_nombre +
                        ')' + '</option>';
                    presentacion_desc_select.append(option)
                });
            }).fail(function () {
                console.error('Falló');
            });
        }
    });

    presentacion_desc_select.change(function (event) {
        subunidad_desc_label = presentacion_desc_select.find('option:selected').data('subunidad');
        subunidad_cantidad_desc = presentacion_desc_select.find('option:selected').data('cantidad');
        $('#subunidad_desc_label').text(subunidad_desc_label);
        $('#subunidad_desc_label2').text(subunidad_desc_label);
        var articulo_presentacion = presentacion_desc_select.val();
        if (articulo_presentacion) {
            var url = '/almacen/' + almacen + '/totalExistencia/' + articulo_presentacion;
            $.get(url, function (data) {
                $('#disponible').val(data);
            }).fail(function () {
                console.error('Falló');
            });
        }
    });

    cantidad_previa_desc_input.on('input change', function (event) {
        var cantidad_desc = subunidad_cantidad_desc * cantidad_previa_desc_input.val();
        cantidad_desc_input.val(cantidad_desc);
        if (cantidad_desc > $('#disponible').val()) {
            bootbox.alert('Error: la cantidad a descargar es mayor a la disponible');
            cantidad_desc_input.val('');
            cantidad_previa_desc_input.val('');
        } else {
            cantidad_desc_input.val(cantidad_desc);
        }
    });

    $('#form_descargar').submit(function (event) {
        event.preventDefault();
        var url = '/almacen/' + almacen + '/movimiento/descargar';
        var data = $(this).serialize();
        $.post(url, data, function (data) {
            if (data.error > 0) {
                bootbox.alert(data.message);
            } else {
                bootbox.alert(data.message);
                $('#existencia_descarga_modal').modal('hide');
                location.reload();
            }
        });
    })

    $('#existencia_descarga_modal').on('hidden.bs.modal', function () {
        $(this).find('form').trigger('reset');
        $('#subunidad_desc_label').text('');
        $('#subunidad_desc_label2').text('');
        presentacion_desc_select.empty();
    })
});