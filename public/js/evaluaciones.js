function onSelectItem(item) {
    var idPeriodoCiclo = $('#ciclo').val();
    var idCurso = $('#curso').val();
    var fecha = $('#fechaCalificar').val();
    $("#notas").children().remove();
    switch (item.value.tipoEvaluacion) {
        case 'calculada':
            // bootbox.dialog({ message: '<div class="text-center"><i class="fa fa-spin fa-spinner"></i> Procesando...</div>' });
            $('#notas').load(routes.notas_calculada + item.id + "?periodoCiclo=" + idPeriodoCiclo, function () {
                // bootbox.hideAll();
            });
            break;
        case 'unica':
            // bootbox.dialog({ message: '<div class="text-center"><i class="fa fa-spin fa-spinner"></i> Procesando...</div>' });
            $('#notas').load(routes.notas_unica + item.id, function () {
                // bootbox.hideAll();
            });
            break;
        case 'detalle':
            // bootbox.dialog({ message: '<div class="text-center"><i class="fa fa-spin fa-spinner"></i> Procesando...</div>' });
            $('#notas').load(routes.notas_diaria, {curso: idCurso, periodoCiclo: idPeriodoCiclo, fecha: fecha}, function () {
                // bootbox.hideAll();
            });
            break;
    }
}

function validarForm(id) {
    var inputs = $("#" + id + " table input[type='number']");

    for (i = 0; i < inputs.length; i++) {
        if (!inputs[i].checkValidity()) {
            return false;
        }
    }
    return true;
}

//Obtiene las califaciones y llena los div correspondientes
function obtenerCalificacionesGrupo() {

    var fecha = $('#fechaCalificar').val();
    var item = $('#jqxTree').jqxTree('getSelectedItem');
    var grupo = $('#grupo').val();

    if (fecha == '') {
        bootbox.alert({
            message: "Seleccione una fecha",
            backdrop: true
        });
        return;
    }

    if (!grupo) {
        return;
    }

    $('#calificacionGrupo').load(routes.calificacion_grupo + grupo + "?evaluacion=" + item.id + "&fecha=" + fecha, function () {
        // $("#fechaCalificar").prop('disabled', true);
    });
}

/*Obtiene las calificaciones de los estudiantes */
function obtenerCalificaciones() {
    var fecha = $('#fechaCalificar').val();
    var item = $('#jqxTree').jqxTree('getSelectedItem');

    $('#calificacionGrupo').load(routes.calculada_individual + item.id + "?fecha=" + fecha, function () {
        // $("#fechaCalificar").prop('disabled', true);
    });
}


function onLoadDetalle(id) {
    var fecha = $('#fechaCalificar').val();
    var item = $('#jqxTree').jqxTree('getSelectedItem');
    $('#myModal').load(routes.calculada_detalle + item.id + "?estudiante=" + id + "&fecha=" + fecha
        , function () {
            $('#myModal').modal({show: true});
        });
}


function calcularNotaGrupo() {
    if (!validarForm("notas")) {
        return;
    }
    if (!validarForm("myModal")) {
        return;
    }
    var data = walk("notasGrupo");
    var nota = 0;
    for (var i = 0; i < data.length; i++) {
        if (data[i].length < 3)
            continue;

        valor = parseFloat(data[i][1]);
        porcentaje = parseFloat(data[i][2]);
        nota += valor * porcentaje;
    }
    nota = nota / 100;
    nota = nota.toFixed(2);
    $('#totalGroup').text(nota);
}

function updateCalculada() {

    if (!validarForm("notas")) {
        bootbox.alert({
            message: "Por favor corrija los datos antes de continuar",
            backdrop: true
        });
        bootbox.hideAll();
        return;
    }
    // bootbox.dialog({message: '<div class="text-center"><i class="fa fa-spin fa-spinner"></i> Procesando...</div>'});
    var data = walk("notasGrupo");

    var fecha = $('#fechaCalificar').val();
    var item = $('#jqxTree').jqxTree('getSelectedItem');
    var grupo = $('#grupo').val();
    $.ajax({
        url: routes.notas_calculada_update + grupo + "?evaluacion=" + item.id + "&fecha=" + fecha,
        type: "GET",
        dataType: "JSON",
        data: {
            criterios: JSON.stringify(data)
        },
        success: function (result) {
            bootbox.hideAll();
            bootbox.alert({
                message: result.message,
                backdrop: true
            });
            obtenerCalificacionesGrupo();
        },
        error: function (err) {
            bootbox.hideAll();
            bootbox.alert({
                message: "Ocurrió un error",
                backdrop: true
            });
        }
    });
}

function updateCalculadaIndividual() {

    if (!validarForm("myModal")) {
        bootbox.alert({
            message: "Por favor corrija los datos antes de continuar",
            backdrop: true
        });
        bootbox.hideAll();
        return;
    }
    // bootbox.dialog({message: '<div class="text-center"><i class="fa fa-spin fa-spinner"></i> Procesando...</div>'});
    var data = walk("notasGrupo");

    var fecha = $('#fechaCalificar').val();
    var evaluacion = $('#evaluacionPadre').val();
    var estudiante = $('#estudianteCurso').val();
    $.ajax({
        url: routes.calculada_individual_update + evaluacion + "?fecha=" + fecha + "&estudiante=" + estudiante,
        type: "GET",
        dataType: "JSON",
        data: {
            criterios: JSON.stringify(data)
        },
        success: function (result) {
            bootbox.hideAll();
            bootbox.alert({
                message: result.message,
                backdrop: true
            });
            obtenerCalificaciones();
        },
        error: function (err) {
            bootbox.hideAll();
            bootbox.alert({
                message: "Ocurrió un error",
                backdrop: true
            });
        }
    });
}


function addPenalizacion(penalizacion, calificacion) {
    $.ajax({
        url: routes.penalizacion_calificacion + calificacion,
        type: "GET",
        dataType: "JSON",
        data: {
            penalizacion: penalizacion
        },
        success: function (result) {
            bootbox.hideAll();
            bootbox.alert({
                message: result.message,
                backdrop: true
            });
            obtenerCalificacionesGrupo();
        },
        error: function (err) {
            bootbox.hideAll();
            bootbox.alert({
                message: "Ocurrió un error",
                backdrop: true
            });
        }
    });
}

function removePenalizacion(id) {
    $.ajax({
        url: routes.penalizacion_remove + id,
        type: "GET",
        dataType: "JSON",
        success: function (result) {
            bootbox.hideAll();
            bootbox.alert({
                message: result.message,
                backdrop: true
            });
            obtenerCalificacionesGrupo();
        },
        error: function (result) {
            bootbox.hideAll();
            bootbox.alert({
                message: 'No se pudo eliminar',
                backdrop: true
            });
        }
    });
}

function update() {

    if (!validarForm("notas")) {
        bootbox.alert({
            message: "Por favor corrija los datos antes de continuar",
            backdrop: true
        });
        bootbox.hideAll();
        return;
    }
    // bootbox.dialog({message: '<div class="text-center"><i class="fa fa-spin fa-spinner"></i> Procesando...</div>'});
    var data = walk("tblUnica");

    $.ajax({
        url: routes.notas_unica_update + $("#idEvaluacion").val(),
        type: "GET",
        dataType: "JSON",
        data: {
            data: JSON.stringify(data)
        },
        success: function (result) {
            bootbox.hideAll();
            bootbox.alert({
                message: result.message,
                backdrop: true
            });
        },
        error: function (err) {
            bootbox.hideAll();
            bootbox.alert({
                message: "Ocurrió un error",
                backdrop: true
            });
        }
    });
}