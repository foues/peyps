$(document).ready(function() {
  $("#tbl_reporte").DataTable({
    dom: '<"row"<"filterPad"f<"input-group-append">>> <"row"t><"row"><"d-flex justify-content-center"p><"clear">',
    sorting: false,
    ordering: false,
    searching: false,
    paging: false,
    emptyTable: "Sin datos que mostrar",
    zeroRecords: "No se encontraron resultados"
  });

  $("#criterio").change(function() {
    window.location.assign(location.protocol + '//' + location.host + location.pathname + '?criterio=' + $(this).val());
  })

  $("#printBtn").click(function() {
    var fecha = new Intl.DateTimeFormat("en-GB").format(new Date());
    var criterio = $("#criterio").val();
    var pdf = new jsPDF('p', 'pt', 'letter');
    pdf.setFontSize(16);
    var h1 = "UNIVERSIDAD DE EL SALVADOR";
		var h2 = "FACULTAD DE ODONTOLOGÍA";
    var h3 = "Reporte de necesidades de tratamiento " + criterio;
    pdf.addImage(logo_odonto,"JPEG",30, 40, 80, 100);
		pdf.addImage(logo_ues,"PNG", 500, 40, 80, 100);
    var x1 = (pdf.internal.pageSize.width / 2) - (pdf.getStringUnitWidth(h1) * pdf.internal.getFontSize() / 2);
    pdf.text(x1, 60, h1);
    var x2 = (pdf.internal.pageSize.width / 2) - (pdf.getStringUnitWidth(h2) * pdf.internal.getFontSize() / 2);
    pdf.text(x2, 80, h2);
    var x3 = (pdf.internal.pageSize.width / 2) - (pdf.getStringUnitWidth(h3) * pdf.internal.getFontSize() / 2);
    pdf.text(x3, 180, h3);
    texpediente = pdf.autoTableHtmlToJson(document.getElementById("tbl_expediente"));
    tcontent = pdf.autoTableHtmlToJson(document.getElementById("tbl_reporte"));
    pdf.setFontSize(14);
    pdf.autoTable(texpediente.columns, texpediente.data, {
      startY: 230,
      createdHeaderCell: function (cell, data) {
        cell.styles.textColor = 80;
        cell.styles.fillColor = [255,255,255];
        if(!(data.column.index % 2 == 0)) {
          cell.styles.fontStyle = "normal";
        }
      },
      createdCell: function (cell, data) {
        cell.styles.textColor = 80;
        cell.styles.fillColor = [255,255,255];
      },
      styles: {
        overflow: "linebreak",
        lineWidth: 1,
        columnWidth: "wrap",
        halign: "center"
      },
      columnStyles: {
        0: {
          columnWidth: "auto",
          fontStyle: "bold"
        },
        1: {
          columnWidth: "auto",
          fontStyle: "normal"
        },
        2: {
          columnWidth: "auto",
          fontStyle: "bold"
        },
        3: {
          columnWidth: "auto",
          fontStyle: "normal"
        },
        4: {
          columnWidth: "auto",
          fontStyle: "bold"
        },
        5: {
          columnWidth: "auto",
          fontStyle: "normal"
        },
        6: {
          columnWidth: "auto",
          fontStyle: "bold"
        }
      }
    });
    pdf.autoTable(tcontent.columns, tcontent.data, {
		    startY: pdf.autoTable.previous.finalY + 30,
				styles: {
					overflow: "linebreak",
					lineWidth: 1,
					columnWidth: "wrap",
          halign: "center"
				},
				columnStyles: {
					0: {
						columnWidth: "auto"
					},
					1: {
						columnWidth: "auto"
					},
					2: {
						columnWidth: "auto"
					},
					3: {
						columnWidth: "auto"
					},
					4: {
						columnWidth: "auto"
					},
					5: {
						columnWidth: "auto"
					}
				}
		});
    pdf.setFontSize(10);
    var pageH = pdf.internal.pageSize.height || pdf.internal.pageSize.getHeight();
    var pageW = pdf.internal.pageSize.width || pdf.internal.pageSize.getWidth();
    pdf.text("Fecha: " + fecha.toString(), pageW - 100, pageH - 10);
    pdf.save(nombre);
  });
});
