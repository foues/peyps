$(document).ready(function () {
    localStorage.removeItem("infoExpediente");

    var nombre_persona = $('#nombre_persona');
    var expediente = $('#codigo_expediente');
    var dui_persona = $('#dui_persona');
    var nit_persona = $('#nit_persona');
    var ficha_select = $('#fichas');
    var tratamiento_select = $('#tratamientos');
    var informacion_llenar = $('#informacion_llenar');
    var tratamiento_select_aux = null;
    $('#buscar_exp').click(function (event) {

        event.preventDefault();
        nombre_persona.text("");
        dui_persona.text("");
        nit_persona.text("");
        ficha_select.empty();
        tratamiento_select.empty();
        $('#tratamientos_tabla td').parent().remove();
        var codigo_expediente = $('#codigo_expediente').val();
        var url = '/almacen/vale/persona/expediente/' + codigo_expediente;
        var urlFichas = '/almacen/vale/list/fichas/' + codigo_expediente;
        $.get(url, function (data) {
            if (data && data.id) {
                informacion_llenar.show();
                nombre_persona.text(data.persona.nombre + " " + data.persona.apellido);
                dui_persona.text("DUI: " + (data.persona.dui || '-'));
                nit_persona.text("NIT: " + (data.persona.nit || '-'));
                $.get(urlFichas, function (data) {
                    if (data.length > 0) {
                        var placeholder = '<option selected disabled>Seleccione</option>';
                        ficha_select.append(placeholder);
                        $.each(data, function (i, item) {
                            var date = moment(item.creado).format('YYYY-MM-DD');
                            var option = $('<option/>').val(item.id).text(item.id + ' / ' + date);
                            ficha_select.append(option)
                        });
                    }
                    // verificamos si en caso el expediente es 1, por lo que pondra
                    if ($('#codigo_expediente').val() == 1) {
                        $('#fichas option[value=1][value=1]').attr('selected', 'selected').change();

                    }

                }).fail(function () {
                    console.error('Falló');
                });
            } else {
                bootbox.alert('No se encontraron expedientes');
                informacion_llenar.hide();
            }
        }).fail(function () {
            console.error('Falló');
        });

    });

    var tratamientosAux = [];
    var tratamientosTable = [];
    ficha_select.change(function (event) {
        var id_ficha = ficha_select.val();
        tratamiento_select.empty();
        tratamientosTable = [];
        $('#tratamientos_tabla td').parent().remove();
        $('#materiales_tabla td').parent().remove();
        var url = '/almacen/vale/list/tratamientos/' + id_ficha;
        $.get(url, function (data) {

            if (data.length > 0) {
                var placeholder = '<option selected disabled>Seleccione</option>';
                tratamiento_select.html("");
                tratamiento_select.append(placeholder);
                $.each(data, function (i, item) {
                    //var tratamiento={"t_id":item.t_id,"ft_pieza_dental":item.ft_pieza_dental,"t_nombre":item.t_nombre};
                    var tratamiento = {
                        "t_id": item.tratamiento.id,
                        "ft_pieza_dental": item.piezaDental,
                        "ft_id": item.id,
                        "t_nombre": item.tratamiento.nombre
                    };
                    var option = '<option value="' + tratamiento.t_id + '"'
                        + ' data-pieza="' + tratamiento.ft_pieza_dental + '"'
                        + ' data-fichatratamiento="' + tratamiento.ft_id + '"'
                        + ' data-tratamiento="' + tratamiento.t_nombre + '">' + tratamiento.t_nombre + ' (' + tratamiento.ft_pieza_dental + ')</option>';
                    tratamientosAux.push(tratamiento);
                    tratamiento_select.append(option)
                });
            }
        }).fail(function () {
            console.error('Falló');
        });
    });


    $('#add_tratamiento').click(function (event) {
        var tratamiento = tratamiento_select.val();
        tratamiento_select_aux = tratamiento;
        if (tratamiento !== null) {
            var pieza_dental = tratamiento_select.find('option:selected').data('pieza');
            var nombre_tratamiento = tratamiento_select.find('option:selected').data('tratamiento');
            var id_ficha_tratamiento = tratamiento_select.find('option:selected').data('fichatratamiento');

            var row = '<tr id="' + tratamiento + '" data-fichatratamiento="' + id_ficha_tratamiento + '">' +
                '<td>' + nombre_tratamiento + '</td>' +
                '<td>' + pieza_dental + '</td>' +
                '<td>' +
                '<button id="' + tratamiento + '"  class="buttonDelete btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>' +
                '</td>' +
                '</tr>'
            $('#tratamientos_tabla tr:last').after('<tr>' + row + '</tr>');
            $("#tratamientos").find('option[value=' + tratamiento + ']').remove();
            $('#tratamientos').val('').trigger('chosen:updated');

            tratamientosTable.push({"t_id": tratamiento});


            var url = '/almacen/vale/list/articulos/' + tratamiento;
            $.get(url, function (data) {
                if (data.length > 0) {
                    $.each(data, function (i, item) {
                        var ap = item.articuloPresentacion;
                        var tds = $('#materiales_tabla').children('tbody').children('tr:last').children('td').length;
                        if (tds === 6 || tds === 0) {
                            var row = '<tr>' +
                                '<td id="' + tratamiento + '">' + ap.articulo.nombre + '(' + ap.subunidadMedida.nombre + ')' + '</td>' +
                                '<td id="' + tratamiento + '" data-articulo="' + ap.id + '"><input style="width: 70%" type="number" required min="0" value="' + item.cantidad + '" data-idtratamiento="' + tratamiento + '" data-idpresentacion="' + ap.id + '"></td>' +
                                '</tr>';
                            $('#materiales_tabla tr:last').after('<tr>' + row + '</tr>');
                        } else {
                            $('#materiales_tabla > tbody>tr:last').append('<td id="' + tratamiento + '">' + ap.articulo.nombre + '(' + ap.subunidadMedida.nombre + ')' + '</td>'
                                + '<td id="' + tratamiento + '" data-articulo="' + ap.id + '"><input style="width: 70%" type="number" required min="0" value="' + item.cantidad + '" data-idtratamiento="' + tratamiento + '" data-idpresentacion="' + ap.id + '"></td>');
                        }
                    });
                }
            }).fail(function () {
                console.error('Falló');
            });
        }
    });

    $(document).on('click', '.buttonDelete', function () {
        tratamiento_select_aux = null;
        $(this).closest('tr').remove();
        var id = parseInt(this.id);
        var item = $.grep(tratamientosAux, function (obj) {
            return obj.t_id === id;
        })[0];
        var option = '<option value="' + item.t_id + '"'
            + ' data-pieza="' + item.ft_pieza_dental + '"'
            + ' data-tratamiento="' + item.t_nombre + '">' + item.t_nombre + '</option>';
        tratamiento_select.append(option);
        tratamiento_select.val(null);
        var vm = this;

        $('#materiales_tabla> tbody tr td').each(function () {
            $(this).remove();
        });

        $('#tratamientos_tabla> tbody tr').each(function () {

            if (this.id > 0) {
                var url = '/almacen/vale/list/articulos/' + this.id;
                var tratamiento = this.id;
                $.get(url, function (data) {

                    if (data.length > 0) {
                        $.each(data, function (i, item) {
                            var ap = item.articuloPresentacion;
                            var tds = $('#materiales_tabla').children('tbody').children('tr:last').children('td').length;
                            if (tds === 6 || tds === 0) {
                                var row = '<tr>' +
                                    '<td id="' + tratamiento + '">' + ap.articulo.nombre + '(' + ap.subunidadMedida.nombre + ')' + '</td>' +
                                    '<td id="' + tratamiento + '" ><input type="number" required min="0" value="' + item.cantidad + '" data-idtratamiento="' + tratamiento + '" data-idpresentacion="' + ap.id + '"></td>' +
                                    '</tr>';
                                $('#materiales_tabla tr:last').after('<tr>' + row + '</tr>');
                            } else {
                                $('#materiales_tabla > tbody>tr:last').append('<td id="' + tratamiento + '">' + ap.articulo.nombre + '(' + ap.subunidadMedida.nombre + ')' + '</td>'
                                    + '<td id="' + tratamiento + '" ><input type="number" required min="0" value="' + item.cantidad + '" data-idtratamiento="' + tratamiento + '" data-idpresentacion="' + ap.id + '"></td>');
                            }
                        });
                    }
                }).fail(function () {
                    console.error('Falló');
                });
            }
        });
    });


    $('#guardar_vale').click(function (event) {
        var inputs = 0;
        var id_vale;
        var url = '/almacen/vale/action/guardar';
        var trs = $('#tratamientos_tabla').children('tbody').children('tr').length;
        if (expediente !== '' && ficha_select.val() !== null && tratamiento_select_aux !== null && trs > 1) {
            inputs = validarInputs();
            if (inputs === 0) {
                $.get(url, {info: localStorage.infoExpediente}, function (data) {
                        if (data.error == 0) {
                            id_vale = parseInt(data.vale);
                            $('#tratamientos_tabla> tbody>tr').each(function () {
                                var id_ficha_tratamiento = $(this).attr("data-fichatratamiento");
                                if (!isNaN(id_ficha_tratamiento) && id_ficha_tratamiento > 0) {
                                    var url = '/almacen/vale/action/guardar/valeTratamiento/' + id_vale + '/' + id_ficha_tratamiento;
                                    $.get(url).then(function (data) {
                                        if (data.error > 0) {
                                            bootbox.alert('Error: ' + data.message);
                                        }
                                    });
                                }

                            });

                            $('#materiales_tabla> tbody tr td input').each(function () {
                                var id_tratamiento = $(this).attr("data-idtratamiento");
                                var id_articulo_presentacion = parseInt($(this).attr("data-idpresentacion"));
                                var id_almacen = parseInt($('#almacen').val());
                                var cantidad = parseFloat($(this).val());
                                cantidad = -1 * cantidad;
                                var movimiento = {
                                    "vale": id_vale,
                                    "almacen": id_almacen,
                                    "articulo_presentacion": id_articulo_presentacion,
                                    "concepto": "Descarga de inventario",
                                    "precio": 0.00,
                                    "cantidad": cantidad,
                                    "confirmado": false
                                };
                                var url = '/almacen/vale/action/guardarMovimiento';
                                // aca se guarda el vale
                                $.post(url, movimiento, function (data) {
                                        if (data.error > 0) {
                                            data = data;
                                        } else {
                                            data = data;
                                        }
                                    }
                                );
                            });
                            bootbox.alert('Vale creado con exito', function () {
                                location.reload();
                            });
                            informacion_llenar.hide();
                            $('#codigo_expediente').val('');
                            nombre_persona.text(" ");
                            dui_persona.text(" ");
                            nit_persona.text(" ");
                            ficha_select.empty();
                            tratamiento_select.empty();
                            $('#tratamientos_tabla> tbody tr td').each(function () {
                                $(this).remove();
                            });
                            $('#tratamientos_tabla> tbody tr').each(function () {
                                $(this).remove();
                            });

                            $('#tratamientos_tabla tbody').append('<tr></tr>');
                            $('#materiales_tabla> tbody tr td').each(function () {
                                $(this).remove();
                            });

                        }
                        else {
                            bootbox.alert('Error: ' + data.message);
                        }
                    }
                );
            } else {
                bootbox.alert('Error: las cantidades de los materiales deben ser mayores a cero');
            }
        } else {
            bootbox.alert('Error al guardar, favor verificar que no hayan campos vacios');
        }


    });

    function validarInputs() {
        var cont = 0;
        $('#materiales_tabla').find('input').each(function () {
            var cantidad = parseFloat($(this).val());
            if (cantidad <= 0) {
                $(this).focus();
                cont++;
            }
        });
        return cont;

    }


    $("#sin_expediente_chck").change(function () {
        if (this.checked) {

            $('#myModal').modal('show');
            expediente.prop('disabled', true);
            ficha_select.attr('disabled', true);


            expediente.val("1");
            $("#buscar_exp").click();
            $('#fichas option[value=1]').attr('selected', 'selected').change();


            nombre_persona.text('');
            dui_persona.text('');
            nit_persona.text('');
            ficha_select.empty();
            tratamiento_select.empty();
            var id_ficha = ficha_select.val();
            tratamiento_select.empty();
            tratamientosTable = [];
            $('#tratamientos_tabla td').parent().remove();
            $('#materiales_tabla td').parent().remove();
            // setTratamientosSinCheck()
            esconderCambioExpedien();
        } else {

            location.reload();
            informacion_llenar.hide();
            nombre_persona.text("");
            edad_persona.text("");
            procedencia_persona.text("");
            referencia_persona.text("");


            ficha_select.empty();
            tratamiento_select.empty();
            expediente.prop('disabled', false);
            ficha_select.attr('disabled', false);
        }
    });


    var edad_persona = $('#edad_persona');
    var procedencia_persona = $('#procedencia_persona');
    var referencia_persona = $('#referencia_persona');
    var editRefe = $('#editRefe');

    $("#cerrarModal").on('submit', function (e) {
        e.preventDefault();


        nombre_persona.text($('#nombre_persona_ref')[0].value);
        edad_persona.text("Edad: " + $('#edad_persona_ref')[0].value);
        procedencia_persona.text("Procedencia: " + $('#procedencia_persona_ref')[0].value);
        referencia_persona.text("Referencia: " + $('#referencia_persona_ref')[0].value);
        editRefe.html("<span class=\"badge badge-info\">editar</span>");
        dui_persona.html("");
        nit_persona.html("");

        var jsonDataNoExped = '{"nombre":"' + $("#nombre_persona_ref")[0].value
            + '", "edad":"' + $("#edad_persona_ref")[0].value
            + '", "procedencia":"' + $("#procedencia_persona_ref")[0].value
            + '", "referencia":"' + $("#referencia_persona_ref")[0].value + '"}';
        localStorage.infoExpediente = (jsonDataNoExped);

        informacion_llenar.show();
        $('#myModal').modal('hide');

    });


    function setTratamientosSinCheck() {
        // var url = '/almacen/vale/tratamientos';
        // $.get(url, function (data) {
        //     if(data.length>0){
        //         var placeholder = '<option selected disabled>Seleccione</option>';
        //         tratamiento_select.html("");
        //         tratamiento_select.append(placeholder);
        //         $.each(data, function (i, item) {
        //             var tratamiento={"t_id":item.t_id,"ft_pieza_dental":item.ft_pieza_dental,"t_nombre":item.t_nombre};
        //             var option = '<option value="' + item.t_id + '"'
        //                 + ' data-pieza="' + item.ft_pieza_dental + '"'
        //                 + ' data-tratamiento="' + item.t_nombre + '">'+item.t_nombre+ '</option>';
        //             tratamientosAux.push(tratamiento);
        //             tratamiento_select.append(option)
        //         });
        //     }
        // }).fail(function () {
        //     console.error('Falló');
        // });

    }

    $(".close").click(function () {
        location.reload();
    });


    function esconderCambioExpedien() {

        $("#divExpdiente").hide();
        $("#divFicha").hide();

    }

    $("#editRefe").click(function () {
        $('#myModal').modal('show');
    });


});