//Para manejar los cambios en las rutas

var routes = {
    programasByCurso: "/academico/programas",
    ciclosByCurso: "/academico/ciclos",
    unidadIntegracion: "/academico/unidadIntegracion",
    evaluaciones: "/academico/evaluaciones",
    add_evaluacion: "/academico/add",
    edit_evaluacion: "/academico/edit/",
    remove_evaluacion: "/academico/remove/",
    add_unidadIntegracion: "/academico/unidadIntegacion/add",
    notas_diaria: '/academico/notas/diaria',
    notas_calculada: '/academico/notas/calculada/',
    notas_unica: '/academico/notas/unica/view/',
    notas_unica_update: '/academico/notas/unica/update/',
    calificacion_grupo: '/academico/notas/calificacionGrupo/',
    notas_calculada_update: '/academico/notas/calculada/update/',
    penalizacion_calificacion: '/academico/notas/calculada/penalizacion/',
    penalizacion_remove: '/academico/notas/penalizacion/remove/',
    calculada_individual: '/academico/notas/calculada/individual/',
    calculada_detalle: '/academico/notas/calculada/detalle/',
    calculada_individual_update: '/academico/notas/calculada/individual/update/'

};

//Procesando la insercion para posteriormente mostrar mensajes
$("#myModal").on("submit", ".form-create", function () {
    var form = $(this);
    var edicion = (form.attr("action")).includes('edit');
    $.ajax({
        url: form.attr("action"),
        data: form.serialize(),
        type: form.attr("method"),
        dataType: 'json',
        success: function (data) {
            if (data.status == 'success') {
                $('#myModal').modal('toggle');
                updateTree();
            } else {
                $('#alert_template .close').trigger('click');
                $("#alert_template button").after('<span>' + data.message + '</span>');
                $('#alert_template').fadeIn('slow');
                $('#alert_template .close').click(function (e) {
                    $("#alert_template span").remove();
                    $("#alert_template").attr("style", "display: none;");

                });
                $("#modal-book .modal-content").html(data.html_form);
            }
        }
    });
    return false;
});


function addUnidadIntegracion() {
    if ($('#programa').val() == '' || $('#curso').val() == '' || $('#ciclo').val() == '') {
        bootbox.hideAll();
        bootbox.alert({
            message: "Seleccione el curso, programa y ciclo",
            backdrop: true
        });
        return;
    }
    $('#myModal').load(routes.add_unidadIntegracion + "?programa=" + $('#programa').val() + "&periodoCiclo=" + $('#ciclo').val() + "&planEstudioCurso=" + $('#curso').val()
        , function () {
            $('#myModal').modal({show: true});
            bootbox.hideAll();
        });
}

function add() {
    // bootbox.dialog({message: '<div class="text-center"><i class="fa fa-spin fa-spinner"></i> Procesando...</div>'});
    if ($('#jqxTree').jqxTree('getItems').length == 0) {
        addUnidadIntegracion();
        return;
    }

    var item = $('#jqxTree').jqxTree('getSelectedItem');

    if (item == null) {
        bootbox.alert({
            message: "Seleccione un elemento padre desde el arbol",
            backdrop: true
        });
    }

    if (item.id == 'unidad') {
        idEvaluacionPadre = null;
    } else {
        if (item.value.tipoEvaluacion != 'calculada') {
            bootbox.hideAll();
            bootbox.alert({
                message: "Solo puede agregar subevaluaciones a las evaluaciones Calculadas",
                backdrop: true
            });
            return;
        }
        idEvaluacionPadre = item.id;
    }


    $('#myModal').load(routes.add_evaluacion + "?idEvaluacionPadre=" + idEvaluacionPadre + "&idUnidadIntegracion=" + item.value.idUnidadIntegracion,
        function () {
            $('#myModal').modal({show: true});
            bootbox.hideAll();
        });
}

function onEdit(id) {
    $('#myModal').load(routes.edit_evaluacion + id,
        function () {
            $('#myModal').modal({show: true});
        });
}

function onRemove(id, row) {
    bootbox.confirm('¿Desea eliminar esta evaluación?',
        function (result) {
            if (result) {
                $.ajax({
                    url: routes.remove_evaluacion + id,
                    type: "GET",
                    dataType: "JSON",
                    success: function (result) {
                        if (result.status == 'success') {
                            updateTree();
                        } else {
                            bootbox.alert({
                                message: result.message,
                                backdrop: true
                            });
                        }
                    },
                    error: function (err) {
                        alert("Ocurrió un error de red");
                    }
                });
            }
        });
}

function updateTree() {
    var item = $('#jqxTree').jqxTree('getSelectedItem');
    getArbol();
    if (item) {
        setTimeout(function () {
            $("#jqxTree").jqxTree('selectItem', $("#" + item.element.id)[0]);
        }, 1000);
    }
}


function getArbol() {
    $('#btnAdd').hide();
    if (!$('#programa').val() || !$('#curso').val() || !$('#ciclo').val()) {
        return;
    }
    $.ajax({
        url: routes.unidadIntegracion,
        type: "GET",
        dataType: "JSON",
        data: {
            programa: $('#programa').val(),
            periodoCiclo: $('#ciclo').val(),
            planEstudioCurso: $('#curso').val()
        },
        success: function (result) {
            if (result.length > 0) {
                $('.no-data').html('');
            } else {
                $('#btnAdd').show();
                var mensaje = "No se encontró alguna unidad de integracion";
                $('.no-data').html(mensaje);
            }
            // create jqxTree
            $('#jqxTree').jqxTree({source: result, width: '100%', height: '100%'});

            $('#jqxTree').off('select');
            $('#jqxTree').on('select', function (event) {
                var args = event.args;
                var item = $('#jqxTree').jqxTree('getItem', args.element);
                onSelectItem(item);
            });
        },
        error: function (err) {
            var mensaje = "No se encontró alguna unidad de integracion";
            $('.no-data').html(mensaje);
        }
    });
}

//Obteniendo los programas
$('#curso').change(function () {

    if ($('#jqxTree').length > 0) {
        $('#jqxTree').jqxTree('clear');
    }
    if ($('#dataTable').length > 0) {
        $("#dataTable").jqxDataTable('clear');
    }
    if ($('#btnAdd').length > 0) {
        // $("#btnAdd").hide();
    }

    $("#programa").html('');

    $("#ciclo").html('');
    var cursoSelector = $(this);
    if (cursoSelector.val() == '') {
        return;
    }
    $.ajax({
        url: routes.programasByCurso + '/' + cursoSelector.val(),
        type: "GET",
        dataType: "JSON",
        data: {
            curso: cursoSelector.val()
        },
        success: function (neighborhoods) {
            var programasSelect = $("#programa");

            // Remove current options
            programasSelect.html('');

            // Empty value ...
            if (neighborhoods.length > 1)
                programasSelect.append('<option selected>Seleccione un programa</option>');

            $.each(neighborhoods, function (key, neighborhood) {
                programasSelect.append('<option value="' + neighborhood.id + '">' + neighborhood.name + '</option>');
            });
            programasSelect.trigger('change');
        },
        error: function (err) {
            alert("No se pudo obtener la data");
        }
    });
    $.ajax({
        url: routes.ciclosByCurso + '/' + cursoSelector.val(),
        type: "GET",
        dataType: "JSON",
        data: {
            curso: cursoSelector.val()
        },
        success: function (neighborhoods) {
            var ciclosSelect = $("#ciclo");

            // Remove current options
            ciclosSelect.html('');

            // Empty value ...
            // if (neighborhoods.length > 1)
            //     ciclosSelect.append('<option value="" selected>Seleccione un ciclo</option>');

            $.each(neighborhoods, function (key, neighborhood) {
                ciclosSelect.append('<option value="' + neighborhood.id + '">' + neighborhood.name + '</option>');
            });
            ciclosSelect.trigger('change');
        },
        error: function (err) {
            alert("No se pudo obtener la data");
        }
    });
});


var getLocalization = function () {
    var localizationobj = {};
    localizationobj.emptydatastring = "No hay datos para mostrar";
    localizationobj.loadtext = "Cargando ...";

    return localizationobj;
};

function isNumberKey(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode == 8)
        return true;
    if (charCode > 31 && (charCode != 46 && (charCode < 48 || charCode > 57)))
        return false;

    var valor = parseFloat(evt.target.value.replace(/\./g, ' ') + String.fromCharCode(charCode))

    if (valor == 'undefined')
        return false;

    if (charCode == 46 && (evt.target.value.length == 0 || evt.target.value.includes("\.")))
        return false;


    if (valor < 0.0 || valor > 10.0)
        return false;


    return true;
}

$('#curso').trigger('change');
