function agregarRetratamiento(evaluacion){
    var expedienteId = $('#expedienteId').val();
    var fichaId = $('#fichaId').val();

    $.post('/clinico/expedientes/'+expedienteId+'/fichas/'+fichaId+'/tratamiento/'+evaluacion, {
      evaluacion_id: evaluacion
    }, function(r) {
      console.log(r)
      $('#tratamientos > tbody:last-child').append(
        '<tr id="trat'+r.tratamiento_id+'"><td>Retratamiento</td>'+
        '<td>'+r.tratamiento_usuario+'</td>'+
        '<td>'+(new Date()).toLocaleDateString("es-ES", {day: "2-digit", month: "2-digit", year: "numeric"})+'</td>'+
        '<td style="text-align:center;"><a id="borrar'+r.tratamiento_id+'" class="btn btn-danger active btn-sm" onclick="removeRetratamiento('+r.tratamiento_id+')"><i class="material-icons">clear</i></a></td>'+
        '</tr>'
        );  

    }, 'json');

}

function removeRetratamiento(tratamiento){
    var expedienteId = $('#expedienteId').val();
    var fichaId = $('#fichaId').val();

    $.ajax({
        url: '/clinico/expedientes/'+expedienteId+'/fichas/'+fichaId+'/tratamiento/'+tratamiento,
        type: 'DELETE',
        success: function(r){
            $('#trat'+tratamiento).remove()
        }
    })

}