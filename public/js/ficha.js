$( document ).ready(function() {
    if($('#fichaType_pieza_trauma').val()!='') $('#fichaType_pieza_trauma').show();
    if($('#fichaType_tipo_reaccion').val()!='') $('#fichaType_tipo_reaccion').show();
})

$('#fichaType_reacciones_adversas').change(function() {
    $('#fichaType_tipo_reaccion')[this.checked ? "show" : "hide"]();
});

$('#fichaType_trauma_dento').change(function() {
    $('#fichaType_pieza_trauma')[this.checked ? "show" : "hide"]();
});