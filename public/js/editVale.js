$(document).ready(function () {
    var id_vale = $('#vale_id').val();
    var id_almacen = $('#almacen_id').val();
    var vale_estado = $('#vale_estado').val();
    var expediente = $('#codigo_expediente').val();
    var tratamiento_select_aux = null;
    var ficha_select = $('#fichas');
    var id_ficha = ficha_select.val();

    var tratamientos_select = $('#tratamientos');
    var tratamientosAux = [];
    var tratamientosTable = [];

    var url = '/almacen/vale/list/tratamientos/' + id_ficha;
    tratamiento_select_aux = tratamientos_select.val();
    $.get(url, function (data) {
        if (data.length > 0) {
            $.each(data, function (i, item) {
                var tratamiento = {
                    "t_id": item.tratamiento.id,
                    "ft_pieza_dental": item.piezaDental,
                    "ft_id": item.id,
                    "t_nombre": item.tratamiento.nombre
                };
                tratamientosAux.push(tratamiento);
            });
        }
    }).fail(function () {
        console.error('Falló');
    });

    $('#tratamientos_tabla> tbody tr').each(function () {
        var idTratamiento = this.id;
        tratamientos_select.find('option[value=' + idTratamiento + ']').remove();
    });

    tratamientosAux = [];
    ficha_select.change(function (event) {
        var id_ficha = ficha_select.val();
        tratamientos_select.empty();
        tratamientosTable = [];
        $('#tratamientos_tabla td').parent().remove();
        $('#materiales_tabla td').parent().remove();
        var url = '/almacen/vale/list/tratamientos/' + id_ficha;
        $.get(url, function (data) {

            if (data.length > 0) {
                var placeholder = '<option selected disabled>Seleccione</option>';
                tratamientos_select.html("");
                tratamientos_select.append(placeholder);
                $.each(data, function (i, item) {
                    //var tratamiento={"t_id":item.t_id,"ft_pieza_dental":item.ft_pieza_dental,"t_nombre":item.t_nombre};
                    var tratamiento = {
                        "t_id": item.tratamiento.id,
                        "ft_pieza_dental": item.piezaDental,
                        "ft_id": item.id,
                        "t_nombre": item.tratamiento.nombre
                    };
                    var option = '<option value="' + tratamiento.t_id + '"'
                        + ' data-pieza="' + tratamiento.ft_pieza_dental + '"'
                        + ' data-fichatratamiento="' + tratamiento.ft_id + '"'
                        + ' data-tratamiento="' + tratamiento.t_nombre + '">' + tratamiento.t_nombre + '</option>';
                    tratamientosAux.push(tratamiento);
                    tratamientos_select.append(option)
                });
            }
        }).fail(function () {
            console.error('Falló');
        });
    });


    $('#add_tratamiento2').click(function (event) {
        var tratamiento = tratamientos_select.val();
        tratamiento_select_aux = tratamiento;
        if (tratamiento !== null) {
            var pieza_dental = tratamientos_select.find('option:selected').data('pieza');
            var nombre_tratamiento = tratamientos_select.find('option:selected').data('tratamiento');
            var id_ficha_tratamiento = tratamientos_select.find('option:selected').data('fichatratamiento');
            var row = '<tr id="' + tratamiento + '" data-fichatratamiento="' + id_ficha_tratamiento + '">' +
                '<td>' + nombre_tratamiento + '</td>' +
                '<td>' + pieza_dental + '</td>' +
                '<td>' +
                '<button id="' + tratamiento + '"  class="buttonDelete2 btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>' +
                '</td>' +
                '</tr>'
            $('#tratamientos_tabla tbody').append(row);
            $("#tratamientos").find('option[value=' + tratamiento + ']').remove();
            $('#tratamientos').val('').trigger('chosen:updated');

            tratamientosTable.push({"t_id": tratamiento});


            var url = '/almacen/vale/list/articulos/' + tratamiento;
            $.get(url, function (data) {
                if (data.length > 0) {
                    $.each(data, function (i, item) {
                        var ap = item.articuloPresentacion;
                        var tds = $('#materiales_tabla').children('tbody').children('tr:last').children('td').length;
                        if (tds === 6 || tds === 0) {
                            var row = '<tr>' +
                                '<td id="' + tratamiento + '">' + ap.articulo.nombre + '(' + ap.subunidadMedida.nombre + ')' + '</td>' +
                                '<td id="' + tratamiento + '" data-articulo="' + ap.id + '"><input style="width: 70%" type="number" required min="0" value="' + item.cantidad + '" data-idtratamiento="' + tratamiento + '" data-idpresentacion="' + ap.id + '"></td>' +
                                '</tr>';
                            $('#materiales_tabla tr:last').after('<tr>' + row + '</tr>');
                        } else {
                            $('#materiales_tabla > tbody>tr:last').append('<td id="' + tratamiento + '">' + ap.articulo.nombre + '(' + ap.subunidadMedida.nombre + ')' + '</td>'
                                + '<td id="' + tratamiento + '" data-articulo="' + ap.id + '"><input style="width: 70%" type="number" required min="0" value="' + item.cantidad + '" data-idtratamiento="' + tratamiento + '" data-idpresentacion="' + ap.id + '"></td>');
                        }
                    });
                }
            }).fail(function () {
                console.error('Falló');
            });
        }
    });

    $(document).on('click', '.buttonDelete2', function () {
        tratamiento_select_aux = null;
        $(this).closest('tr').remove();
        var id = parseInt(this.id);

        var item = $.grep(tratamientosAux, function (obj) {
            return obj.t_id === id;
        })[0];
        var option = '<option value="' + item.t_id + '"'
            + ' data-pieza="' + item.ft_pieza_dental + '"'
            + ' data-tratamiento="' + item.t_nombre + '">' + item.t_nombre + '</option>';
        tratamientos_select.append(option);
        tratamientos_select.val(null);

        $('#materiales_tabla> tbody tr td').each(function () {
            $(this).remove();
        });

        $('#tratamientos_tabla> tbody>tr').each(function () {

            if (this.id > 0) {
                var url = '/almacen/vale/list/articulos/' + this.id;
                var tratamiento = this.id;
                $.get(url, function (data) {
                    if (data.length > 0) {
                        $.each(data, function (i, item) {
                            var ap = item.articuloPresentacion;
                            var tds = $('#materiales_tabla').children('tbody').children('tr:last').children('td').length;
                            if (tds === 6 || tds === 0) {
                                var row = '<tr>' +
                                    '<td id="' + tratamiento + '">' + ap.articulo.nombre + '(' + ap.subunidadMedida.nombre + ')' + '</td>' +
                                    '<td id="' + tratamiento + '" ><input type="number" required min="0" value="' + item.cantidad + '" data-idtratamiento="' + tratamiento + '" data-idpresentacion="' + ap.id + '"></td>' +
                                    '</tr>';
                                $('#materiales_tabla tr:last').after('<tr>' + row + '</tr>');
                            } else {
                                $('#materiales_tabla > tbody>tr:last').append('<td id="' + tratamiento + '">' + ap.articulo.nombre + '(' + ap.subunidadMedida.nombre + ')' + '</td>'
                                    + '<td id="' + tratamiento + '" ><input type="number" required min="0" value="' + item.cantidad + '" data-idtratamiento="' + tratamiento + '" data-idpresentacion="' + ap.id + '" ></td>');
                            }
                        });
                    }
                }).fail(function () {
                    console.error('Falló');
                });
            }
        });
    });

    $('#actualizar_vale').click(function (event) {

        var id_vale = parseInt($('#vale_id').val());
        var url = '/almacen/vale/beforeUpdate/' + id_vale + '/';
        var trs = $('#tratamientos_tabla').children('tbody').children('tr').length;
        if (expediente !== '' && ficha_select.val() !== null /* && tratamiento_select_aux !== null */ && trs >= 1) {
            inputs = validarInputs();
            if (inputs === 0) {
                $.get(url, function (data) {
                    if (data) {
                        $('#tratamientos_tabla> tbody tr').each(function () {
                            var id_ficha_tratamiento = parseInt($(this).attr("data-fichatratamiento"));
                            if (!isNaN(id_ficha_tratamiento) && id_ficha_tratamiento > 0) {
                                var url = '/almacen/vale/action/guardar/valeTratamiento/' + parseInt($('#vale_id').val()) + '/' + id_ficha_tratamiento;
                                $.get(url).then(function (data) {
                                    if (data.error > 0) {
                                        bootbox.alert('Error: ' + data.message);
                                    }
                                });
                            }
                        });

                        $('#materiales_tabla> tbody tr td input').each(function () {
                            var id_tratamiento = $(this).attr("data-idtratamiento");
                            var id_articulo_presentacion = parseInt($(this).attr("data-idpresentacion"));
                            var id_almacen = parseInt($('#almacen').val());
                            var cantidad = parseFloat($(this).val());
                            cantidad = -1 * cantidad;
                            var movimiento = {
                                "vale": parseInt($('#vale_id').val()),
                                "almacen": id_almacen,
                                "articulo_presentacion": id_articulo_presentacion,
                                "concepto": "Descarga de inventario",
                                "precio": 0.00,
                                "cantidad": cantidad,
                                "confirmado": false
                            };
                            var url = '/almacen/vale/action/guardarMovimiento';
                            $.post(url, movimiento, function (data) {
                                    if (data.error > 0) {
                                        data = data;
                                    } else {
                                        data = data;
                                    }
                                }
                            );
                        });
                        bootbox.alert('Vale actualizado con exito');
                        /* $('#codigo_expediente').val('');
                         nombre_persona.text(" ");
                         dui_persona.text(" ");
                         nit_persona.text(" ");
                         ficha_select.empty();
                         tratamiento_select.empty();
                         $('#tratamientos_tabla> tbody tr td').each(function(){
                             $(this).remove();
                         });
                         $('#materiales_tabla> tbody tr td').each(function(){
                             $(this).remove();
                         });*/
                    }

                });
            } else {
                bootbox.alert('Error: las cantidades de los materiales deben ser mayores a cero');
            }
        } else {
            bootbox.alert('Error al guardar, favor verificar que no hayan campos vacios');
        }


    });

    function validarInputs() {
        var cont = 0;
        $('#materiales_tabla').find('input').each(function () {
            var cantidad = parseFloat($(this).val());
            if (cantidad <= 0) {
                $(this).focus();
                cont++;
            }
        });
        return cont;

    }


});