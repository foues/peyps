var tooth = 0;

var allowEdit = 0;

var injuriesMap = {};

var selectedInjuries = [];

// **********************************************************************************************
// ***********************************     Events          **************************************
// **********************************************************************************************

$(document).ready(function() {

  // Allow Editing? -> "#allowEdit" is a hidden input.
  allowEdit = parseInt($("#allowEdit").val());

  // Get all posible codes
  $("#injuries .injurie > select > option").each(function() {
    var parentId = $(this).parent().attr('id');
    var value = $(this).val();

    if(!injuriesMap[parentId]) injuriesMap[parentId] = [];
    if(value != "0") injuriesMap[parentId].push(value);
  });

  // ***************             Click on a tooth of the odontogram      *******************************
  $("div.tooth").click(function() {

    selectedInjuries = [];

    // Get stored injuries
    var ids = $(this).attr('data-id') ? $(this).attr('data-id').trim().split(" ") : [];
    var injuries = $(this).attr('data-injury') != "" ? $(this).attr('data-injury').trim().split(" ") : [];

    $("#injuries .injurie > select").each(function() {
      $(this).removeClass("selected");
    });

    clearOptions();

    for(var i = 0; i < injuries.length; i++){
      selectedInjuries.push({id: ids[i], injury: injuries[i]});

      for(var type in injuriesMap){
        if(injuriesMap[type].includes(injuries[i])){
          $('#' + type).val(injuries[i]);
          $('#' + type).addClass("selected");
        }
      };
    }

    if(allowEdit != 1){
      $("#injuries .injurie > select").each(function() {
        $(this).attr("disabled", "disabled");
      });
    }

    // Get the class of the tooth div.
    var classDiv = $(this).attr("class");

    // Set the global variable as int.
    tooth = parseInt(classDiv.match(/\d+/));

    // Set modal title.
    $("#modal-title-label").text('Lesiones de la pieza ' + tooth);
    $("#tooth-title-img").attr('class', classDiv.replace('tooth', 'tooth-title'));

    // Display modal
    $('#tooth-modal').modal('show');
  });

  $("#injuries .injurie > select").focus(function(){
    selectedInjuries = selectedInjuries.filter(function(injury){
      return injury.injury != this;
    }, $(this).val());

    $(this).removeClass("selected");
  });

  $("#injuries .injurie > select").change(function(){
    var value = $(this).val();
    if(value != '0'){
      selectedInjuries.push({id: "", injury: value});
      $(this).addClass("selected");
    }
    else{
      selectedInjuries = selectedInjuries.filter(function(injury){
        return injury.injury != this;
      }, $(this).val());

      $(this).removeClass("selected");
    }
  });



  $("#saveBtn").click(function(){
    var fichaId = $('#fichaId').val();

    $.post('/clinico/lesion/nuevo', {
      fichaId: fichaId,
      tooth: tooth,
      injuries: selectedInjuries
    }, function(r) {
      $("div.tooth.d" + r.number).attr("data-id", r.ids);
      $("div.tooth.d" + r.number).attr("data-injury", r.injuries);

      if(r.ids == "") $("div.tooth.d" + r.number).removeClass("diente-marcado")
      else $("div.tooth.d" + r.number).addClass("diente-marcado")

    }, 'json');

    $('#tooth-modal').modal('hide');
  });
});


// *******************************************************************************************
// *************************           Functions               *******************************
// *******************************************************************************************

function clearOptions(){
  // Clear previous data.
  $("#injuries .injurie > select").attr('disabled', false);
  $("#injuries .injurie > select").val('0');
}
