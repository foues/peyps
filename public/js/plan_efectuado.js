function cambiarEfectuado(evaluacion){
    var fichaId = $('#fichaId').val();

    $.post('/clinico/fichas/'+fichaId+'/tratamientos/realizado', {
      evaluacion_id: evaluacion
    }, function(r) {
      console.log(r.estado)
      
      if(r.estado==0){
        neoClass = 'btn btn-warning active btn-sm'
        neoText = 'clear'
        $('#estado'+evaluacion).text('Pendiente')
        $('#hist'+evaluacion).hide()
      } else {
        var neoClass = 'btn btn-success active btn-sm'
        var neoText = 'done'
        $('#hist'+evaluacion).show()
        $('#estado'+evaluacion).text('Realizado')
      }

      $('#eval'+evaluacion).removeClass().addClass(neoClass)
      $('#eval'+evaluacion).find('i').text(neoText)
    }, 'json');

}