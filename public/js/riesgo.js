$( document ).ready(function() {

    var step_count = parseInt($("#riesgoType_flow_riesgo_step").val())

    switch (step_count) {
        case 1:
            var sum = 0;
            $('input[id^="riesgoType_c"]').each(function(){
                sum += +$(this).val();
            });
            $("#riesgoType_total_indice").val(sum);
            break;
        case 2:
            var sum = 0;
            $('select[id^="riesgoType_placa"]').each(function(){
                sum += +$(this).val();
            });
            $("#riesgoType_total_placa").val((sum*100/24).toFixed(2).toString() + '%');
            break;

        case 4:
            var total_indice = parseInt($("#riesgoType_total_indice").val())
            var total_placa = parseFloat($("#riesgoType_total_placa").val())
            var total_azucar = parseInt($("#riesgoType_total_ingesta").val())

            //Indice
            if(total_indice<=2){
                $('#cpo_bajo').attr("class", "table-primary")
            } else if(total_indice<=4){
                $('#cpo_medio').attr("class", "table-primary")
            } else{
                $('#cpo_alto').attr("class", "table-primary")
            }

            //Placa
            if(total_placa<=20){
                $('#placa_bajo').attr("class", "table-primary")
            } else if(total_placa<=50){
                $('#placa_medio').attr("class", "table-primary")
            } else{
                $('#placa_alto').attr("class", "table-primary")
            }

            //Azucar
            if(total_azucar<=4){
                $('#azucar_bajo').attr("class", "table-primary")
            } else if(total_azucar<=7){
                $('#azucar_medio').attr("class", "table-primary")
            } else{
                $('#azucar_alto').attr("class", "table-primary")
            }
        break;

    }


});

$(document).on('change', 'select[id^="riesgoType_placa"]', function() {
    var sum = 0;
    $('select[id^="riesgoType_placa"]').each(function(){
        sum += +$(this).val();
    });
    $("#riesgoType_total_placa").val((sum*100/24).toFixed(2).toString() + '%');
    
    if(!isNaN(sum)) localStorage.setItem('placa', (sum*100/24).toFixed(2))
});

