$(document).ready(function () {
    var cicloSelect = $('#ciclo');
    var cursoSelect = $('#curso');
    var programaSelect = $('#programa');
    var treeGrid = $("#treegrid");

    cicloSelect.change(function (event) {
        var ciclo = cicloSelect.val();
        if (!ciclo) {
            return;
        }

        cursoSelect.empty();
        programaSelect.empty();
        treeGrid.jqxTreeGrid('clear');
        var url = '/academico/mis_cursos/ciclo/' + ciclo;
        $.get(url, function (data) {
            $.each(data, function (index, curso) {
                cursoSelect.append(
                    $('<option/>')
                        .val(curso.id)
                        .text(curso.codigo + ' - ' + curso.nombre)
                )
            });
            cursoSelect.trigger('change');
        }, 'json').fail(function (xhr, status, error) {
            console.error(error)
        });
    });

    cursoSelect.change(function (event) {
        var curso = cursoSelect.val();
        if (!curso) {
            return;
        }

        programaSelect.empty();
        treeGrid.jqxTreeGrid('clear');
        var url = '/academico/mis_cursos/' + curso + '/programas';
        $.get(url, function (data) {
            programaSelect.append($('<option selected disabled/>').text('Seleccione'));
            $.each(data, function (index, programa) {
                programaSelect.append(
                    $('<option/>')
                        .val(programa.id)
                        .text(programa.codigo.toUpperCase() + ' - ' + programa.nombre)
                )
            });
        }, 'json').fail(function (xhr, status, error) {
            console.error(error)
        });
    });

    programaSelect.change(function () {
        var ciclo = cicloSelect.val();
        var curso = cursoSelect.val();
        var programa = programaSelect.val();
        if (!ciclo || !curso || !programa) {
            return;
        }

        getUnidadIntegracion(ciclo, curso, programa);
        fillTreeGrid(ciclo, curso, programa);
    });

    function getUnidadIntegracion(ciclo, curso, programa) {
        var url = '/academico/unidad_integracion/ciclo/' + ciclo + '/curso/' + curso + '/programa/' + programa;

        $.get(url, function (data) {
            if (data.unidad_integracion) {
                $('#ponderacion').val(data.unidad_integracion.ponderacion);
            }
            if (data.nota_parcial != 'null') {
                $('#promedio').val(parseFloat(data.nota_parcial.promedio).toFixed(2));
                $('#nota').val(parseFloat(data.nota_parcial.nota).toFixed(2));
            }
        }).fail(function (xhr, status, error) {
            console.error(error);
        })
    }

    function fillTreeGrid(ciclo, curso, programa) {
        var url = '/academico/calificaciones/ciclo/' + ciclo + '/curso/' + curso + '/programa/' + programa;
        var source =
            {
                dataType: "json",
                dataFields: [
                    {name: 'id', type: 'number'},
                    {name: 'padre', type: 'number'},
                    {name: 'nombre', type: 'string'},
                    {name: 'ponderacion', type: 'number'},
                    {name: 'nota', type: 'number'},
                    {name: 'fecha', type: 'date'},
                ],
                hierarchy:
                    {
                        keyDataField: {name: 'id'},
                        parentDataField: {name: 'padre'}
                    },
                id: 'id',
                url: url,
            };

        var dataAdapter = new $.jqx.dataAdapter(source);

        treeGrid.jqxTreeGrid('clear');
        treeGrid.jqxTreeGrid({
            width: '100%',
            source: dataAdapter,
            pageable: false,
            columnsResize: true,
            columns: [
                {text: 'Evaluación', dataField: 'nombre', minWidth: 100, width: '50%'},
                {text: 'Fecha', dataField: 'fecha', width: '20%', cellsformat: 'dd/MM/yyyy'},
                {text: 'Ponderación (%)', dataField: 'ponderacion', width: '15%'},
                {text: 'Nota', dataField: 'nota', width: '15%', cellsformat: 'd2'},
            ],
        });
    }

    cicloSelect.trigger('change');
});