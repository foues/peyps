
//tootltips de para prototipo
$(function () {
  $('[data-toggle="tooltip"]').tooltip()
})

// para que cuando desee agregar una unidad de integracion no sobrepase el 100%
$(function  () {
element = document.getElementById("totalGroup");

element.addEventListener("click", function(e) {
  e.preventDefault;
  element.classList.remove("tada");
  void element.offsetWidth;
  element.classList.add("tada");

}, false);
})

//para alertart de un intercambio sin mover alumno 
$("#moverSinIntercambio").on('click touchstart', function () {
  bootbox.confirm({ 
  size: "small",
  message: "Cambiaras a un estudiante sin intercambiarlo?", 
  callback: function(result){ /* result is a boolean; true = OK, false = Cancel*/ }
})
});

//para alertar de dar baja a un estudiantes 
$(".darBaja").on('click touchstart', function () {
  console.log(this)
  
  bootbox.confirm({ 
  size: "small",
  message: "Deseas dar de baja a este estudiante", 
  callback: function(result){ /* result is a boolean; true = OK, false = Cancel*/ }
})
});


//para alertar al crear una nueva ficha a un expediente 
$(".addFicha").on('click touchstart', function () {
  bootbox.confirm({ 
 /* size: "small",*/
  message: "Deseas agregar un nuevo control a este expediente", 
  callback: function(result){
		/* result is a boolean; true = OK, false = Cancel*/ 
	if(result)
	window.location.href = "../clinico/fichaNueva.html";	
	}})
});

// para que los botones puedan desplasarce entre los nab 
// deben de tener las clases espeficias en este segmento 
$('.btnNext').click(function(){
  $('.nav-tabs > .active').next('li').find('a').trigger('click');
});

  $('.btnPrevious').click(function(){
  $('.nav-tabs > .active').prev('li').find('a').trigger('click');
});
 


// para una lista de "div Whith link podemos chekear de una lista"
$('.list-group-item.list-group-item-action').on('click', function() {
  var checkBox =  $(this).children();
  if ($(checkBox).attr('checked')){ 
    $(checkBox).removeAttr('checked');
    $(this).removeClass( "active" );}
  else{ 
    $(checkBox).attr('checked', 'checked');
    $(this).addClass( "active" );}
  return false;
})


// preliminarRotacion Modal para publicar la rotacion
 $("#publicar").on('click touchstart', function () {
  bootbox.confirm({ 
  size: "small",
  message: "Se enviará un correo electrónico a los estudiantes, se cambiará el estado de la rotación a publicada", 
  callback: function(result){ /* result is a boolean; true = OK, false = Cancel*/ }
})
});
 
$("#acepRotar").on('click touchstart', function () {
  bootbox.confirm({ 
  size: "small",
  message: "Aceptar la rotacion final , se cambiará el estado de la rotación a Iniciada", 
  callback: function(result){
		if(result)
	window.location.href = "../rotacion/nominaOficial.html";	
	}})
});





