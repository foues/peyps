var tooth = 0;

var id = "";
var state = "";
var surface = "";
var need = "";

var allowEdit = 0;

var statesNames = {};
var needsNames = {};

var treatments = [];

// **********************************************************************************************
// ***********************************     Events          **************************************
// **********************************************************************************************

$(document).ready(function() {

  // Allow Editing? -> "#allowEdit" is a hidden input.
  allowEdit = parseInt($("#allowEdit").val());

  // Get all posible codes
  $("#restoration > option").each(function() {
    var resId = $(this).val();
    var resName = $(this).text();

    if(resId != '10') $("#cavity > option").each(function() {
      var cavId = $(this).val();
      var cavName = $(this).text();

      if(cavId != '10') statesNames[resId + cavId] = resName + "; " + cavName;
    });
  });

  $("#selectCompletes label").each(function(){
    var input = $(this).children('input')["0"];
    var span = $(this).children('span')["0"]
    var val = input.value;
    if(val != '100' && val != '1') statesNames[val] = span.innerText;
  });

  $("#div_state_full label").each(function(){
    var input = $(this).children('input')["0"];
    var span = $(this).children('span')["0"]
    var val = input.value;
    statesNames[val] = span.innerText;
  });


  // Get all posible treatment needs
  $("div.treatment").each(function() {
    needsNames[$(this).attr('treatment-id')] = $(this).attr('treatment-name');
  });

  $("#div-treatments").remove();


  // ***************             Click on a tooth of the odontogram      *******************************
  $("div.tooth, div.BC").click(function() {

    $("#needsTable tbody").html('');
    treatments = [];

    $("#selectCompletes input[data-type='full']").parent().attr('hidden', false);
    $("#div-select-surface").attr('hidden', false);
    $("#div-select-state").attr('hidden', false);
    $("#div-select-need").attr('hidden', false);

    // Get stored treatments
    var ids = $(this).attr('data-id') ? $(this).attr('data-id').trim().split(" ") : [];
    var states = $(this).attr('data-state') != "" ? $(this).attr('data-state').trim().split(" ") : [];
    var surfaces = $(this).attr('data-surface') ? $(this).attr('data-surface').trim().split(" ") : [];
    var needs = $(this).attr('data-need') ? $(this).attr('data-need').trim().split(" ") : [];

    clearOptions();
    $("map area").removeClass("diente-disabled");

    for(var i = 0; i < states.length; i++){
      id = ids[i];
      state = states[i];
      surface = surfaces[i];
      need = needs[i];

      addNeed();
    }

    // Get the class of the tooth div.
    var classDiv = $(this).attr("class");

    // Set the global variable as int.
    tooth = parseInt(classDiv.match(/\d+/));


    $("#div-error").text("");
    $("#div-error").removeClass("alert alert-danger");
    $("#div-error").removeClass("alert alert-success");

    if(isNaN(tooth)){
      tooth = 'BC';
      $("#modal-title-label").text('Evaluación dental para la boca completa');
      $("#div-select-surface").attr('hidden', true);

      $("#div_state_full").attr('hidden', false);
      $("#div_state_cavity").attr('hidden', true);
      $("#div_state_restoration").attr('hidden', true);

      surface = 'N/A';

      $("#needs-title").text("Necesidades de tratamiento para boca completa");
      $('#options-needs').html('Cargando tratamientos...');
      need = "";
    }
    else{
      $("#div_state_full").attr('hidden', true);
      $("#div_state_cavity").attr('hidden', false);
      $("#div_state_restoration").attr('hidden', false);


      // Set modal title.
      $("#modal-title-label").text('Evaluación dental para la pieza ' + tooth);
      $("#tooth-title-img").attr('class', classDiv.replace('tooth', 'tooth-title'));
    }

    // Display modal
    if(allowEdit != "1"){
      $("#div-select-surface").attr('hidden', true);
      $("#div-select-state").attr('hidden', true);
      $("#div-select-need").attr('hidden', true);
    }
    $('#tooth-modal').modal('show');
  });


  // ***************          When tooth state radio inputs change.      *******************************
  $("#selectCompletes input").on('change', function() {
    if (allowEdit == 1) {
      // Get the selected code.
      var dataCode = $(this).val();

      // Disable selects.
      enableDrop(false);

      if (dataCode != '1' && dataCode != "100"){
        // Delete error messages
        $("#div-error").text("");
        $("#div-error").removeClass("alert alert-danger");
        $("#div-error").removeClass("alert alert-success");
      }

      // If code selected is default
      if (dataCode == '100') {
        // Set selects to default values; clear surface image; enable surface image.
        selectDrop("default");
        changeSurfaceImage('diente-limpio');
        $("map").removeClass("diente-disabled");

        surface = "";

        // Clear title.
        $("#needs-title").text("Necesidades de tratamiento");

        $("#div-select-state").attr('hidden', false);

        state = "";

      // If code selected is a full-tooth code.
      } else if (dataCode == '101'){
        // Clear selects; change surface image to full-tooth; disable surface image.
        selectDrop('default');
        changeSurfaceImage('diente-completo');
        $("map").addClass("diente-disabled");

        $('#div-msg').text('Ha seleccionado la pieza completa');
        $('#options-needs').html('');
        $('#options-needs').append('<div>Seleccione un estado...</div>');

        $("#div-select-state").attr('hidden', false);

        // Clear title.
        $("#needs-title").text("Necesidades de tratamiento");
        enableDrop(true);

        // Clear selection and get needs;
        surface = "completo";
        state = "";
      } else if (dataCode != '1'){
        state = dataCode;
        // Clear selects; change surface image to full-tooth; disable surface image.
        selectDrop('N/A');
        changeSurfaceImage('diente-completo');
        $("map").addClass("diente-disabled");

        $("#div-select-state").attr('hidden', true);

        // Clear selection and get needs;
        surface = "completo";
        getNeeds();

        // Set title.
        $("#needs-title").text("Necesidades de tratamiento para el estado " + state);
      }
    }
  });


  // *********************       When click in an dental surface           *********************
  $("area").click(function() {
    if (allowEdit == 1 && !$(this).hasClass('diente-disabled') && !$("map").hasClass('diente-disabled')) {
      // Set selected surface.
      surface = $(this).attr("id");
      var imageSurfaceName = $(this).attr("data-image");

      // Set title.
      $('#div-msg').text('Ha seleccionado la superficie: ' + surface);

      // Delete error messages
      $("#div-error").text("");
      $("#div-error").removeClass("alert alert-danger");
      $("#div-error").removeClass("alert alert-success");

      // Change surface image and enable selects.
      changeSurfaceImage(imageSurfaceName);
      enableDrop(true);
    }
  });


  // *************              Call "getNeeds" on selects change.     ****************
  $("#restoration").change(getNeeds);
  $("#cavity").change(getNeeds);

  $("#div_state_full input").on('change', function() {
    state = $(this).val();
    getNeeds();
  });


  $("#searchBtn").click(function(){
    var search = $("#searchInput").val();
    if(search != ""){
      $("#needs-title").text("Resultados de la búsqueda: " + search);
      $('#options-needs').html('Cargando tratamientos...');
      need = "";

      $.get('/clinico/tratamientos/byName/' + search, function(needs) {
        $('#options-needs').html('');

        if(needs.length == 0){
          $('#options-needs').append('<div>No hay tratamientos disponibles.</div>');
        }
        else{
          needs.forEach(function(_need){
             var needDiv = $('<div/>', {class: 'list-group-item list-group-item-action list-group-item-primary'});
             var needLabel = $('<label/>');
             var needInput = $('<input/>', {type: 'radio', name: 'need-tratamiento', value: _need.id, style: 'margin-right: 15px;'})

             needInput.change(function(){
                need = $(this).val();
             });

             needLabel.html(needsNames[_need.id]);
             needLabel.prepend(needInput);
             needDiv.append(needLabel);

             $('#options-needs').append(needDiv);
          });
        }
      }, 'json');
    }
    else{
      $("#needs-title").text("Necesidades de tratamiento");
    }
  });


  // *************              Click on add need     ****************
  $("#addNeedBtn").click(function(){
    if(state == ""){
      $("#div-error").text("Seleccione un estado!");
      $("#div-error").addClass("alert alert-danger");
    }
    else if(need == ""){
      $("#div-error").text("Seleccione un tratamiento!");
      $("#div-error").addClass("alert alert-danger");
    }
    else{
      $("#div-error").text("Necesidad agregada! Guarde los cambios si ya ha agregado todas las necesidades.");
      $("#div-error").removeClass("alert alert-danger");
      $("#div-error").addClass("alert alert-success");
      addNeed();
    }
  });


  $("#saveBtn").click(function(){
    var fichaId = $('#fichaId').val();

    $.post('/clinico/icdas/nuevo', {
      fichaId: fichaId,
      tooth: tooth,
      treatments: treatments
    }, function(r) {
      if(state.length <= 2){
        $("div.tooth.d" + r.number).attr("data-id", r.ids);
        $("div.tooth.d" + r.number).attr("data-state", r.states);
        $("div.tooth.d" + r.number).attr("data-surface", r.surfaces);
        $("div.tooth.d" + r.number).attr("data-need", r.needs);
        $("div.tooth.d" + r.number).attr("class", "tooth d" + r.number + " " + r.class);
      }
      else{
        $("div.BC").attr("data-id", r.ids);
        $("div.BC").attr("data-state", r.states);
        $("div.BC").attr("data-surface", r.surfaces);
        $("div.BC").attr("data-need", r.needs);
      }
    }, 'json');

    $('#tooth-modal').modal('hide');
  });
});


// *******************************************************************************************
// *************************           Functions               *******************************
// *******************************************************************************************

// *************              selectDrop     ****************
function selectDrop(dataCode) {
  // Clear values.
  if(dataCode == "N/A"){
    $('#restoration').val(-1);
    $('#cavity').val(-1);
    $('#div-msg').text('Ha seleccionado la pieza completa');
  }

  // Default values.
  else if(dataCode == "default"){
    $('#restoration').val(10);
    $('#cavity').val(10);
    $('#div-msg').text('Seleccione una superficie de la imagen de arriba');
    $('#options-needs').html('');
    $('#options-needs').append('<div>Seleccione un estado...</div>');
  }

  // State's values
  else{
    $('#restoration').val(dataCode[0]);
    $('#cavity').val(dataCode[1]);
  }
}


// *************        enableDrop     ****************
function enableDrop(enable) {
  if (enable) {
    $("#restoration").removeAttr("disabled", false);
    $("#cavity").removeAttr("disabled", false);
  } else {
    $("#restoration").attr("disabled", true);
    $("#cavity").attr("disabled", true);
  }
}


// *************          changeSurfaceImage     ****************
function changeSurfaceImage(image) {
  var src = "/img/odontograma/" + image + '.png';
  $('#surface-img').attr('src', src);
}


// *************           getNeeds     ****************
function getNeeds() {
  if(state.length <= 2){
    var codeCavity = $("#cavity").find('option:selected').data('code');
    var codeRestoration = $("#restoration").find('option:selected').data('code');

    // If both selects has been selected.
    if(codeRestoration != "10" && codeCavity != "10" && codeRestoration != null && codeCavity != null){
      state = "" + codeRestoration + codeCavity;
    }
  }

  if(state != ""){
    $("#needs-title").text("Necesidades de tratamiento para el estado " + state);
    $('#options-needs').html('Cargando tratamientos...');
    need = "";

    $.get('/clinico/tratamientos/' + state, function(needs) {
      $('#options-needs').html('');

      if(needs.length == 0){
        $('#options-needs').append('<div>No hay tratamientos disponibles.</div>');
      }
      else{
        needs.forEach(function(_need){
           var needDiv = $('<div/>', {class: 'list-group-item list-group-item-action list-group-item-primary'});
           var needLabel = $('<label/>');
           var needInput = $('<input/>', {type: 'radio', name: 'need-tratamiento', value: _need.id, style: 'margin-right: 15px;'})

           needInput.change(function(){
              need = $(this).val();
           });

           needLabel.html(needsNames[_need.id]);
           needLabel.prepend(needInput);
           needDiv.append(needLabel);

           $('#options-needs').append(needDiv);
        });
      }
    }, 'json');
  }
  else{
    $("#needs-title").text("Necesidades de tratamiento");
  }
}


// *************           addNeed     ****************
function addNeed(){
  var i = surface + state + need;

  var row = $("<tr/>");
  row.attr("data-i", i);

  var data = $("<td/>");
  data.text(surface[0].toUpperCase() + surface.substring(1));
  row.append(data);

  data = $("<td/>");
  data.text(statesNames[state]);
  row.append(data);

  data = $("<td/>");
  data.text(needsNames[need]);
  row.append(data);

  if(allowEdit == 1){
    data = $("<td/>", {class: "align-content-center", style: "text-align:center;"});
    var btn = $("<button/>",{class: "btn btn-danger active btn-sm", type: "button"});
    var icon = $("<i/>", {class: "material-icons"});
    icon.text("delete_forever");
    btn.html(icon);
    data.html(btn);
    row.append(data);

    btn.click(function(){
      var row = $(this).closest('tr');
      var i = row.attr("data-i");
      var treatment_index;

      treatments.forEach(function(treatment, index){
        if(treatment.i == i){
          treatment_index = index;
        }
      });

      if(treatments[treatment_index].surface == "Completo" || treatments[treatment_index].surface == "completo"){
        $("#div-select-surface").attr('hidden', false);
        $("#div-select-state").attr('hidden', false);
        $("#div-select-need").attr('hidden', false);
        $("#selectCompletes input").attr('disabled', false);
        $("#completes_100").parent().button('toggle');
      }
      else if(treatments[treatment_index].surface == "N/A"){

      }
      else{
        $("area#" + treatments[treatment_index].surface).removeClass('diente-disabled');
        if(treatments.length <= 1){
          $("#selectCompletes input[data-type='full']").parent().attr('hidden', false);
          $("#selectCompletes input[data-type='full']").attr('disabled', false);
        }

      }

      treatments.splice(treatment_index, 1);

      row.remove();
    });
  }

  $("#needsTable tbody").append(row);

  treatments.push({i: i, id: id, surface: surface, state: state, need: need});

  if(surface == "Completo" || surface == "completo"){
    clearOptions();
    $("#completes_1").parent().button('toggle');
    $("#selectCompletes input").attr('disabled', true);
    $("map").addClass("diente-disabled");
    $("#div-select-surface").attr('hidden', true);
    $("#div-select-state").attr('hidden', true);
    $("#div-select-need").attr('hidden', true);
  }
  else if(surface == "N/A"){
    need = "";
    id = "";
  }
  else{
    $("area#" + surface).addClass('diente-disabled');
    clearOptions();
    $("#selectCompletes input[data-type='full']").attr('disabled', true);
    $("#selectCompletes input[data-type='full']").parent().attr('hidden', true);
  }
}

function clearOptions(){
  // Clear previous data.
  surface = "";
  need = "";
  id = "";

  $("#selectCompletes input").attr('disabled', false);
  $("#searchInput").val("");

  $("#completes_1").parent().button('toggle');
  $("#completes_100").parent().button('toggle');
}
