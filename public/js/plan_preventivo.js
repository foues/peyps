function cambiarEstado(evaluacion){
    var fichaId = $('#fichaId').val();

    $.post('/clinico/fichas/'+fichaId+'/tratamientos/prioridad', {
      evaluacion_id: evaluacion
    }, function(r) {
      console.log(r.estado)
      var neoClass = 'btn btn-success active btn-sm'
      var neoText = 'done'

      if(r.estado==0){
        neoClass = 'btn btn-danger active btn-sm'
        neoText = 'clear'
      }

      $('#eval'+evaluacion).removeClass().addClass(neoClass)
      $('#eval'+evaluacion).find('i').text(neoText)
    }, 'json');

}