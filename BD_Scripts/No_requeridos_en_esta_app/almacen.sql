-- phpMyAdmin SQL Dump
-- version 4.6.6deb4
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jun 29, 2020 at 02:11 AM
-- Server version: 10.4.12-MariaDB-1:10.4.12+maria~stretch-log
-- PHP Version: 7.0.33-19+0~20200202.27+debian9~1.gbp5d283d

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `almacen`
--

-- --------------------------------------------------------

--
-- Table structure for table `almacen`
--

CREATE TABLE `almacen` (
  `id` int(11) NOT NULL,
  `id_institucion` int(11) NOT NULL,
  `codigo` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nombre` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `descripcion` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `creado` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `almacen`
--

INSERT INTO `almacen` (`id`, `id_institucion`, `codigo`, `nombre`, `descripcion`, `creado`) VALUES
(1, 108, 'cesa', 'Almacén de la Clínica Extramural de Santa Ana', NULL, '2020-02-18 23:18:11');

-- --------------------------------------------------------

--
-- Table structure for table `articulo`
--

CREATE TABLE `articulo` (
  `id` int(11) NOT NULL,
  `codigo` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nombre` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `descripcion` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `marca` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `creado` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `articulo`
--

INSERT INTO `articulo` (`id`, `codigo`, `nombre`, `descripcion`, `marca`, `creado`) VALUES
(190, 'aguja_corta', 'AGUJA CORTAS', NULL, 'PREHMA', '2020-02-18 23:18:02'),
(191, 'aguja_larga', 'AGUJA LARGAS', NULL, 'ISCOT', '2020-02-18 23:18:02'),
(192, 'aguja_sutura', 'AGUJA PARA SUTURA CON HILO MONTADO', NULL, 'S/MARCA', '2020-02-18 23:18:02'),
(193, 'alcohol', 'ALCOHOL', NULL, 'PURIFSA', '2020-02-18 23:18:02'),
(194, 'alginato', 'ALGINATO', NULL, 'RITE DENT', '2020-02-18 23:18:02'),
(195, 'algodon', 'ALGODON', NULL, 'TEXPOL', '2020-02-18 23:18:02'),
(196, 'anestesico', 'ANESTESICO AL 2%', NULL, 'DENTOCAIN', '2020-02-18 23:18:02'),
(197, 'anestesico_topico', 'ANESTESICO TOPICO', NULL, 'RITE DENT', '2020-02-18 23:18:02'),
(198, 'bajalenguas', 'BAJALENGUAS', NULL, 'MEDICAL', '2020-02-18 23:18:02'),
(199, 'banda_celuloide', 'BANDA DE CELULOIDE', NULL, 'S/MARCA', '2020-02-18 23:18:02'),
(200, 'banda_ancha', 'BANDA MATRIZ ANCHA', NULL, 'RITE DENT', '2020-02-18 23:18:02'),
(201, 'banda_angosta', 'BANDA MATRIZ ANGOSTA', NULL, 'RITE DENT', '2020-02-18 23:18:02'),
(202, 'banda_seccionada', 'BANDA MATRIZ SECCIONADA', NULL, '', '2020-02-18 23:18:02'),
(203, 'bicarbonato', 'BICARBONATO', NULL, '', '2020-02-18 23:18:02'),
(204, 'bolsa_esterilizar', 'BOLSA PARA ESTERILIZAR', NULL, 'CROSSTEX', '2020-02-18 23:18:02'),
(205, 'brocha_profilactica', 'BROCHA PROFILACTICA', NULL, 'DENTAMERICA', '2020-02-18 23:18:02'),
(206, 'calcibiotics', 'CALCIBIOTIC (CEMENTO P/ENDO)', NULL, 'HYGENIC', '2020-02-18 23:18:02'),
(207, 'campo', 'CAMPO', NULL, 'PREHMA', '2020-02-18 23:18:02'),
(208, 'cavit', 'CAVIT \"G\"', NULL, '3M ESPE', '2020-02-18 23:18:02'),
(209, 'coltosol', 'COLTOSOL (CEMENTO PROV.)', NULL, 'COLTENE', '2020-02-18 23:18:02'),
(210, 'copa_profilactica', 'COPA PROFILACTICA', NULL, 'DENTAMERICA', '2020-02-18 23:18:02'),
(211, 'cuña_mediana', 'CUÑA DE MADERA MEDIANA', NULL, 'CARLES', '2020-02-18 23:18:02'),
(212, 'cuña_pequenia', 'CUÑA DE MADERA PEQUERA', NULL, 'S/MARCA', '2020-02-18 23:18:02'),
(213, 'detector_caries', 'DETECTOR DE CARIES', NULL, '', '2020-02-18 23:18:02'),
(214, 'dique_goma', 'DIQUE DE GOMA 6X6', NULL, 'NIC TONE', '2020-02-18 23:18:02'),
(215, 'disco_sofflex', 'DISCO SOFFLEX', NULL, '3M', '2020-02-18 23:18:02'),
(216, 'enjuague_clorexidina', 'ENJUAGUE DE CLOREXIDINA', NULL, 'KIN', '2020-02-18 23:18:02'),
(217, 'eugenol', 'EUGENOL', NULL, 'RITE DENT', '2020-02-18 23:18:02'),
(218, 'eyector', 'EYECTOR', NULL, 'VIARDEN', '2020-02-18 23:18:02'),
(219, 'fluor_barniz', 'FLUOR BARNIZ', NULL, 'VOCCO', '2020-02-18 23:18:02'),
(220, 'formocresol', 'FORMOCRESOL', NULL, 'VIARDEN', '2020-02-18 23:18:02'),
(221, 'fresa_330', 'FRESA No.330', NULL, 'SSWHITE', '2020-02-18 23:18:02'),
(222, 'fresa_medio', 'FRESA 1/2', NULL, 'SSWHITE', '2020-02-18 23:18:02'),
(223, 'fresa_cuarto', 'FRESA 1/4', NULL, 'SSWHITE', '2020-02-18 23:18:02'),
(224, 'fresa_4', 'FRESA 4', NULL, 'SSWHITE', '2020-02-18 23:18:02'),
(225, 'fresa_5', 'FRESA 5', NULL, 'SSWHITE', '2020-02-18 23:18:02'),
(226, 'fresa_flama', 'FRESA DE DIAMANTE FORMA DE FLAMA', NULL, 'CARLES', '2020-02-18 23:18:02'),
(227, 'fresa_redondo', 'FRESA DE DIAMANTE EXTREMO REDONDO', NULL, 'DIATECH', '2020-02-18 23:18:02'),
(228, 'fresa_interproximal', 'FRESA DE DIAMANTE INTERPROXIMAL', NULL, 'DENTSPLY', '2020-02-18 23:18:02'),
(229, 'fresa_2', 'FRESA No.2', NULL, 'AMERIDENT', '2020-02-18 23:18:02'),
(230, 'fresa_quirurgica', 'FRESA QUIRURGICAS', NULL, 'SSWHITE', '2020-02-18 23:18:02'),
(231, 'frigenol', 'FRIGENOL (CEMENTO PROVISIONAL)', NULL, 'GC AMERICA', '2020-02-18 23:18:02'),
(232, 'gasa', 'GASA', NULL, '', '2020-02-18 23:18:02'),
(233, 'gorro_desechable', 'GORRO DESECHABLE P/ENFERMERA', NULL, 'HEALTH', '2020-02-18 23:18:02'),
(234, 'guante_l', 'GUANTES TALLA \"L\"', NULL, 'BLOSSON', '2020-02-18 23:18:02'),
(235, 'guante_m', 'GUANTES TALLA \"M\"', NULL, 'PRODERMA', '2020-02-18 23:18:02'),
(236, 'guante_s', 'GUANTES TALLA \"S\"', NULL, 'NUGARD', '2020-02-18 23:18:02'),
(237, 'gutapercha_15', 'GUTAPERCHA 15-40', NULL, 'HYGENIC', '2020-02-18 23:18:02'),
(238, 'Gutapercha_25', 'GUTAPERCHA 25', NULL, 'HYGENIC', '2020-02-18 23:18:02'),
(239, 'Gutapercha_30', 'GUTAPERCHA 30', NULL, 'HYGENIC', '2020-02-18 23:18:02'),
(240, 'Gutapercha_35', 'GUTAPERCHA 35', NULL, 'HYGENIC', '2020-02-18 23:18:02'),
(241, 'Gutapercha_40', 'GUTAPERCHA 40', NULL, 'HYGENIC', '2020-02-18 23:18:02'),
(242, 'Gutapercha_45', 'GUTAPERCHA 45', NULL, 'HYGENIC', '2020-02-18 23:18:02'),
(243, 'Gutapercha_80', 'GUTAPERCHA 45-80', NULL, 'HYGENIC', '2020-02-18 23:18:02'),
(244, 'Gutapercha_50', 'GUTAPERCHA 50', NULL, 'HYGENIC', '2020-02-18 23:18:02'),
(245, 'gutapercha_medio_fina', 'GUTAPERCHA ACC MEDIO-FINA', NULL, 'HYGENIC', '2020-02-18 23:18:02'),
(246, 'gutapercha_fina', 'GUTAPERCHA ACC. FINA', NULL, 'HYGENIC', '2020-02-18 23:18:02'),
(247, 'gutapercha_fina_fina', 'GUTAPERCHA ACC. FINA-FINA', NULL, 'HYGENIC', '2020-02-18 23:18:02'),
(248, 'hidroxido_calcio', 'HIDROXIDO DE CALCIO', NULL, 'RITE DENT', '2020-02-18 23:18:02'),
(249, 'hilo_dental', 'HILO DENTAL', NULL, 'SANIFILL', '2020-02-18 23:18:02'),
(250, 'hilo_retractor', 'HILO RETRACTOR', NULL, 'SULTAN', '2020-02-18 23:18:02'),
(251, 'hipoclorito', 'HIPOCLORITO AL 5%', NULL, 'MACROCLEANER', '2020-02-18 23:18:02'),
(252, 'hoja_bisturi', 'HOJA DE BISTURI', NULL, '', '2020-02-18 23:18:02');

-- --------------------------------------------------------

--
-- Table structure for table `articulo_presentacion`
--

CREATE TABLE `articulo_presentacion` (
  `id` int(11) NOT NULL,
  `id_articulo` int(11) NOT NULL,
  `id_unidad_medida` int(11) NOT NULL,
  `id_subunidad_medida` int(11) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `creado` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `articulo_presentacion`
--

INSERT INTO `articulo_presentacion` (`id`, `id_articulo`, `id_unidad_medida`, `id_subunidad_medida`, `cantidad`, `creado`) VALUES
(190, 190, 55, 56, 100, '2020-02-18 23:18:02'),
(191, 191, 55, 56, 100, '2020-02-18 23:18:02'),
(192, 192, 55, 56, 12, '2020-02-18 23:18:02'),
(193, 193, 57, 58, 3800, '2020-02-18 23:18:02'),
(194, 194, 59, 56, 1, '2020-02-18 23:18:02'),
(195, 195, 60, 61, 90, '2020-02-18 23:18:02'),
(196, 196, 55, 56, 50, '2020-02-18 23:18:02'),
(197, 197, 62, 63, 30, '2020-02-18 23:18:02'),
(198, 198, 55, 56, 100, '2020-02-18 23:18:02'),
(199, 199, 64, 56, 100, '2020-02-18 23:18:02'),
(200, 200, 55, 65, 1, '2020-02-18 23:18:02'),
(201, 201, 55, 65, 1, '2020-02-18 23:18:02'),
(202, 202, 66, 56, 111, '2020-02-18 23:18:02'),
(203, 203, 67, 63, 750, '2020-02-18 23:18:02'),
(204, 204, 55, 56, 2000, '2020-02-18 23:18:02'),
(205, 205, 55, 56, 144, '2020-02-18 23:18:02'),
(206, 206, 66, 56, 1, '2020-02-18 23:18:02'),
(207, 207, 55, 56, 500, '2020-02-18 23:18:02'),
(208, 208, 62, 63, 10, '2020-02-18 23:18:02'),
(209, 209, 62, 56, 1, '2020-02-18 23:18:02'),
(210, 210, 55, 56, 144, '2020-02-18 23:18:02'),
(211, 211, 59, 56, 500, '2020-02-18 23:18:02'),
(212, 212, 59, 56, 500, '2020-02-18 23:18:02'),
(213, 213, 68, 56, 1, '2020-02-18 23:18:02'),
(214, 214, 55, 56, 36, '2020-02-18 23:18:02'),
(215, 215, 66, 56, 240, '2020-02-18 23:18:02'),
(216, 216, 68, 58, 2000, '2020-02-18 23:18:02'),
(217, 217, 69, 56, 1, '2020-02-18 23:18:02'),
(218, 218, 70, 56, 100, '2020-02-18 23:18:02'),
(219, 219, 66, 58, 10, '2020-02-18 23:18:02'),
(220, 220, 68, 58, 10, '2020-02-18 23:18:02'),
(221, 221, 56, 56, 1, '2020-02-18 23:18:02'),
(222, 222, 56, 56, 1, '2020-02-18 23:18:02'),
(223, 223, 56, 56, 1, '2020-02-18 23:18:02'),
(224, 224, 56, 56, 1, '2020-02-18 23:18:02'),
(225, 225, 56, 56, 1, '2020-02-18 23:18:02'),
(226, 226, 56, 56, 1, '2020-02-18 23:18:02'),
(227, 227, 56, 56, 1, '2020-02-18 23:18:02'),
(228, 228, 56, 56, 1, '2020-02-18 23:18:02'),
(229, 229, 56, 56, 1, '2020-02-18 23:18:02'),
(230, 230, 56, 56, 1, '2020-02-18 23:18:02'),
(231, 231, 66, 56, 1, '2020-02-18 23:18:02'),
(232, 232, 71, 56, 1, '2020-02-18 23:18:02'),
(233, 233, 59, 56, 100, '2020-02-18 23:18:02'),
(234, 234, 55, 56, 100, '2020-02-18 23:18:02'),
(235, 235, 55, 56, 100, '2020-02-18 23:18:02'),
(236, 236, 55, 56, 100, '2020-02-18 23:18:02'),
(237, 237, 66, 56, 120, '2020-02-18 23:18:02'),
(238, 238, 55, 56, 110, '2020-02-18 23:18:02'),
(239, 239, 55, 56, 100, '2020-02-18 23:18:02'),
(240, 240, 55, 56, 100, '2020-02-18 23:18:02'),
(241, 241, 55, 56, 100, '2020-02-18 23:18:02'),
(242, 242, 55, 56, 100, '2020-02-18 23:18:02'),
(243, 243, 55, 56, 120, '2020-02-18 23:18:02'),
(244, 244, 55, 56, 100, '2020-02-18 23:18:02'),
(245, 245, 55, 56, 100, '2020-02-18 23:18:02'),
(246, 246, 66, 56, 100, '2020-02-18 23:18:02'),
(247, 247, 55, 56, 100, '2020-02-18 23:18:02'),
(248, 248, 68, 63, 750, '2020-02-18 23:18:02'),
(249, 249, 55, 65, 500, '2020-02-18 23:18:02'),
(250, 250, 55, 72, 72, '2020-02-18 23:18:02'),
(251, 251, 57, 58, 3800, '2020-02-18 23:18:02'),
(252, 252, 56, 56, 1, '2020-02-18 23:18:02');

-- --------------------------------------------------------

--
-- Table structure for table `consumo`
--

CREATE TABLE `consumo` (
  `id` int(11) NOT NULL,
  `id_vale` int(11) NOT NULL,
  `id_movimiento` int(11) NOT NULL,
  `creado` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Triggers `consumo`
--
DELIMITER $$
CREATE TRIGGER `consumo_delete` AFTER DELETE ON `consumo` FOR EACH ROW DELETE FROM almacen.movimiento where id=OLD.id_movimiento
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `existencia`
--

CREATE TABLE `existencia` (
  `id` int(11) NOT NULL,
  `id_almacen` int(11) NOT NULL,
  `id_articulo_presentacion` int(11) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `precio` float NOT NULL,
  `adquisicion` date NOT NULL,
  `vencimiento` date NOT NULL,
  `creado` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `movimiento`
--

CREATE TABLE `movimiento` (
  `id` int(11) NOT NULL,
  `id_almacen` int(11) NOT NULL,
  `id_articulo_presentacion` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `concepto` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `precio` float NOT NULL,
  `cantidad` int(11) NOT NULL,
  `fecha` date NOT NULL,
  `confirmado` tinyint(3) NOT NULL,
  `creado` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='http://saitenlinea.com/wiki/Inventario/Tipos_de_Movimientos';

-- --------------------------------------------------------

--
-- Table structure for table `tratamiento_articulo_presentacion`
--

CREATE TABLE `tratamiento_articulo_presentacion` (
  `id` int(11) NOT NULL,
  `id_tratamiento` int(11) NOT NULL,
  `id_articulo_presentacion` int(11) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `creado` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `unidad_medida`
--

CREATE TABLE `unidad_medida` (
  `id` int(11) NOT NULL,
  `codigo` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nombre` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subunidades` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `creado` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `unidad_medida`
--

INSERT INTO `unidad_medida` (`id`, `codigo`, `nombre`, `subunidades`, `creado`) VALUES
(55, 'caja', 'CAJA', NULL, '2020-02-18 23:18:02'),
(56, 'unidad', 'UNIDAD', NULL, '2020-02-18 23:18:02'),
(57, 'galon', 'GALON', NULL, '2020-02-18 23:18:02'),
(58, 'mililitro', 'MILILITRO', NULL, '2020-02-18 23:18:02'),
(59, 'bolsa', 'BOLSA', NULL, '2020-02-18 23:18:02'),
(60, 'libra', 'LIBRA', NULL, '2020-02-18 23:18:02'),
(61, 'porcion', 'PORCION', NULL, '2020-02-18 23:18:02'),
(62, 'tarro', 'TARRO', NULL, '2020-02-18 23:18:02'),
(63, 'gramo', 'GRAMO', NULL, '2020-02-18 23:18:02'),
(64, 'tubos', 'TUBOS', NULL, '2020-02-18 23:18:02'),
(65, 'metro', 'METRO', NULL, '2020-02-18 23:18:02'),
(66, 'estuche', 'ESTUCHE', NULL, '2020-02-18 23:18:02'),
(67, 'kilogramo', 'KILOGRAMO', NULL, '2020-02-18 23:18:02'),
(68, 'frasco', 'FRASCO', NULL, '2020-02-18 23:18:02'),
(69, 'onza', 'ONZA', NULL, '2020-02-18 23:18:02'),
(70, 'bolsas', 'BOLSAS', NULL, '2020-02-18 23:18:02'),
(71, 'rollo', 'ROLLO', NULL, '2020-02-18 23:18:02'),
(72, 'pulgada', 'PULGADA', NULL, '2020-02-18 23:18:02');

-- --------------------------------------------------------

--
-- Table structure for table `vale`
--

CREATE TABLE `vale` (
  `id` int(11) NOT NULL,
  `id_vale_estado` int(11) NOT NULL,
  `descripcion` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `datos` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `creado` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `id_usuario` int(11) NOT NULL,
  `extra` tinyint(3) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='No se podrá registrar vales si no se cuenta con fichas ni expedientes';

--
-- Triggers `vale`
--
DELIMITER $$
CREATE TRIGGER `vale_estado_historial_insert` AFTER INSERT ON `vale` FOR EACH ROW INSERT INTO almacen.vale_estado_historial(id_vale,id_vale_estado) 
            values (NEW.id,NEW.id_vale_estado)
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `vale_estado_historial_update` AFTER UPDATE ON `vale` FOR EACH ROW INSERT INTO almacen.vale_estado_historial(id_vale,id_vale_estado) 
            values (NEW.id,NEW.id_vale_estado)
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `vale_estado`
--

CREATE TABLE `vale_estado` (
  `id` int(11) NOT NULL,
  `codigo` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nombre` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `descripcion` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `creado` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `vale_estado`
--

INSERT INTO `vale_estado` (`id`, `codigo`, `nombre`, `descripcion`, `creado`) VALUES
(10, 'pendiente', 'Pendiente', 'Vales en espera de revisión para su respectiva aprobación o denegación', '2020-02-18 23:18:01'),
(11, 'aprobado', 'Aprobado', 'Vales que han sido revisados y aprobados para la entrega de insumos', '2020-02-18 23:18:01'),
(12, 'denegado', 'Denegado', 'Vales que han sido revisados y se les deniega la entrega de insumos.', '2020-02-18 23:18:01');

-- --------------------------------------------------------

--
-- Table structure for table `vale_estado_historial`
--

CREATE TABLE `vale_estado_historial` (
  `id` int(11) NOT NULL,
  `id_vale` int(11) NOT NULL,
  `id_vale_estado` int(11) NOT NULL,
  `descripcion` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `creado` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `vale_evaluacion_dental`
--

CREATE TABLE `vale_evaluacion_dental` (
  `id` int(11) NOT NULL,
  `id_vale` int(11) NOT NULL,
  `id_evaluacion_dental` int(11) NOT NULL,
  `creado` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `almacen`
--
ALTER TABLE `almacen`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_almacen_institucion` (`id_institucion`);

--
-- Indexes for table `articulo`
--
ALTER TABLE `articulo`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `codigo` (`codigo`);

--
-- Indexes for table `articulo_presentacion`
--
ALTER TABLE `articulo_presentacion`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_articulo` (`id_articulo`),
  ADD KEY `fk_unidad_medida` (`id_unidad_medida`),
  ADD KEY `fk_sub_unidad_medida` (`id_subunidad_medida`);

--
-- Indexes for table `consumo`
--
ALTER TABLE `consumo`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `unique_consumo` (`id_vale`,`id_movimiento`),
  ADD KEY `fk_consumo_movimiento` (`id_movimiento`);

--
-- Indexes for table `existencia`
--
ALTER TABLE `existencia`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_existencia_almacen` (`id_almacen`),
  ADD KEY `fk_existencia_articulo_presentacion` (`id_articulo_presentacion`);

--
-- Indexes for table `movimiento`
--
ALTER TABLE `movimiento`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_movimiento_almacen` (`id_almacen`),
  ADD KEY `fk_movimiento_articulo_presentacion` (`id_articulo_presentacion`),
  ADD KEY `fk_movimiento_usuario` (`id_usuario`);

--
-- Indexes for table `tratamiento_articulo_presentacion`
--
ALTER TABLE `tratamiento_articulo_presentacion`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `unique_tratamiento_articulo_presentacion` (`id_tratamiento`,`id_articulo_presentacion`),
  ADD KEY `fk_articulo_presentacion_tratamiento` (`id_articulo_presentacion`);

--
-- Indexes for table `unidad_medida`
--
ALTER TABLE `unidad_medida`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `codigo` (`codigo`);

--
-- Indexes for table `vale`
--
ALTER TABLE `vale`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_vale_estado` (`id_vale_estado`),
  ADD KEY `fk_vale_usuario` (`id_usuario`);

--
-- Indexes for table `vale_estado`
--
ALTER TABLE `vale_estado`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `codigo` (`codigo`);

--
-- Indexes for table `vale_estado_historial`
--
ALTER TABLE `vale_estado_historial`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_vale_estado_historial` (`id_vale`),
  ADD KEY `fk_historial_vale_estado` (`id_vale_estado`);

--
-- Indexes for table `vale_evaluacion_dental`
--
ALTER TABLE `vale_evaluacion_dental`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `unique_vale_evaluacion_dental` (`id_vale`,`id_evaluacion_dental`),
  ADD KEY `fk_evaluacion_dental_vale` (`id_evaluacion_dental`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `almacen`
--
ALTER TABLE `almacen`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `articulo`
--
ALTER TABLE `articulo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=253;
--
-- AUTO_INCREMENT for table `articulo_presentacion`
--
ALTER TABLE `articulo_presentacion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=253;
--
-- AUTO_INCREMENT for table `consumo`
--
ALTER TABLE `consumo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `existencia`
--
ALTER TABLE `existencia`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `movimiento`
--
ALTER TABLE `movimiento`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tratamiento_articulo_presentacion`
--
ALTER TABLE `tratamiento_articulo_presentacion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `unidad_medida`
--
ALTER TABLE `unidad_medida`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=73;
--
-- AUTO_INCREMENT for table `vale`
--
ALTER TABLE `vale`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `vale_estado`
--
ALTER TABLE `vale_estado`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `vale_estado_historial`
--
ALTER TABLE `vale_estado_historial`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `vale_evaluacion_dental`
--
ALTER TABLE `vale_evaluacion_dental`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `almacen`
--
ALTER TABLE `almacen`
  ADD CONSTRAINT `fk_almacen_institucion` FOREIGN KEY (`id_institucion`) REFERENCES `administracion`.`institucion` (`id`);

--
-- Constraints for table `articulo_presentacion`
--
ALTER TABLE `articulo_presentacion`
  ADD CONSTRAINT `fk_articulo` FOREIGN KEY (`id_articulo`) REFERENCES `articulo` (`id`),
  ADD CONSTRAINT `fk_sub_unidad_medida` FOREIGN KEY (`id_subunidad_medida`) REFERENCES `unidad_medida` (`id`),
  ADD CONSTRAINT `fk_unidad_medida` FOREIGN KEY (`id_unidad_medida`) REFERENCES `unidad_medida` (`id`);

--
-- Constraints for table `consumo`
--
ALTER TABLE `consumo`
  ADD CONSTRAINT `fk_consumo_movimiento` FOREIGN KEY (`id_movimiento`) REFERENCES `movimiento` (`id`),
  ADD CONSTRAINT `fk_consumo_vale` FOREIGN KEY (`id_vale`) REFERENCES `vale` (`id`);

--
-- Constraints for table `existencia`
--
ALTER TABLE `existencia`
  ADD CONSTRAINT `fk_existencia_almacen` FOREIGN KEY (`id_almacen`) REFERENCES `almacen` (`id`),
  ADD CONSTRAINT `fk_existencia_articulo_presentacion` FOREIGN KEY (`id_articulo_presentacion`) REFERENCES `articulo_presentacion` (`id`);

--
-- Constraints for table `movimiento`
--
ALTER TABLE `movimiento`
  ADD CONSTRAINT `fk_movimiento_almacen` FOREIGN KEY (`id_almacen`) REFERENCES `almacen` (`id`),
  ADD CONSTRAINT `fk_movimiento_articulo_presentacion` FOREIGN KEY (`id_articulo_presentacion`) REFERENCES `articulo_presentacion` (`id`),
  ADD CONSTRAINT `fk_movimiento_usuario` FOREIGN KEY (`id_usuario`) REFERENCES `administracion`.`usuario` (`id`);

--
-- Constraints for table `tratamiento_articulo_presentacion`
--
ALTER TABLE `tratamiento_articulo_presentacion`
  ADD CONSTRAINT `fk_articulo_presentacion_tratamiento` FOREIGN KEY (`id_articulo_presentacion`) REFERENCES `articulo_presentacion` (`id`),
  ADD CONSTRAINT `fk_tratamiento_articulo_presentacion` FOREIGN KEY (`id_tratamiento`) REFERENCES `clinico`.`tratamiento` (`id`);

--
-- Constraints for table `vale`
--
ALTER TABLE `vale`
  ADD CONSTRAINT `fk_vale_estado` FOREIGN KEY (`id_vale_estado`) REFERENCES `vale_estado` (`id`),
  ADD CONSTRAINT `fk_vale_usuario` FOREIGN KEY (`id_usuario`) REFERENCES `administracion`.`usuario` (`id`);

--
-- Constraints for table `vale_estado_historial`
--
ALTER TABLE `vale_estado_historial`
  ADD CONSTRAINT `fk_historial_vale_estado` FOREIGN KEY (`id_vale_estado`) REFERENCES `vale_estado` (`id`),
  ADD CONSTRAINT `fk_vale_estado_historial` FOREIGN KEY (`id_vale`) REFERENCES `vale` (`id`);

--
-- Constraints for table `vale_evaluacion_dental`
--
ALTER TABLE `vale_evaluacion_dental`
  ADD CONSTRAINT `fk_evaluacion_dental_vale` FOREIGN KEY (`id_evaluacion_dental`) REFERENCES `clinico`.`evaluacion_dental` (`id`),
  ADD CONSTRAINT `fk_vale_evaluacion_dental` FOREIGN KEY (`id_vale`) REFERENCES `vale` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
