-- phpMyAdmin SQL Dump
-- version 4.6.6deb4
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jun 29, 2020 at 02:10 AM
-- Server version: 10.4.12-MariaDB-1:10.4.12+maria~stretch-log
-- PHP Version: 7.0.33-19+0~20200202.27+debian9~1.gbp5d283d

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `rotacion`
--

-- --------------------------------------------------------

--
-- Stand-in structure for view `cupo_intitucion_programa`
-- (See below for the actual view)
--
CREATE TABLE `cupo_intitucion_programa` (
`cantidad_estudiante` int(11)
,`codigo` varchar(50)
,`nombre` varchar(255)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `estudiante_activo_materia`
-- (See below for the actual view)
--
CREATE TABLE `estudiante_activo_materia` (
`carnet` varchar(8)
,`cum` decimal(8,2)
,`tipo` varchar(25)
,`codigoEstudiante` varchar(50)
,`codigoCurso` varchar(10)
,`nombre` varchar(255)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `estudiante_activo_materia_grupo`
-- (See below for the actual view)
--
CREATE TABLE `estudiante_activo_materia_grupo` (
`carnet` varchar(8)
,`cum` decimal(8,2)
,`tipo` varchar(25)
,`codicoEstudiante` varchar(50)
,`codigoCurso` varchar(10)
,`nombre` varchar(255)
,`grupoEstudiante` varchar(50)
,`peridoCiclo` int(11)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `fechas_rotacion_view`
-- (See below for the actual view)
--
CREATE TABLE `fechas_rotacion_view` (
`grupo` varchar(50)
,`rotacion` varchar(50)
,`id_grupo` int(11)
,`periodoCiclo` int(11)
,`INICIO` date
,`FIN` date
,`inicio_csns` date
,`inicio_cesa` date
,`inicio_hnr` date
,`inicio_ulc` date
,`inicio_clc` date
,`inicio_ppe` date
,`fin_csns` date
,`fin_cesa` date
,`fin_hnr` date
,`fin_ppe` date
);

-- --------------------------------------------------------

--
-- Table structure for table `grupo`
--

CREATE TABLE `grupo` (
  `id` int(11) NOT NULL,
  `id_padre` int(11) DEFAULT NULL,
  `id_periodo_ciclo` int(11) NOT NULL,
  `nombre` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rotable` tinyint(3) NOT NULL DEFAULT 0,
  `creado` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `calificable` tinyint(3) DEFAULT 0,
  `id_plan_estudio_curso` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `grupo`
--

INSERT INTO `grupo` (`id`, `id_padre`, `id_periodo_ciclo`, `nombre`, `rotable`, `creado`, `calificable`, `id_plan_estudio_curso`) VALUES
(1, NULL, 22, 'A', 0, '2020-02-18 23:18:12', 0, NULL),
(2, 1, 22, '1', 1, '2020-02-18 23:18:12', 0, NULL),
(3, 1, 22, '2', 1, '2020-02-18 23:18:12', 0, NULL),
(4, 1, 22, '3', 1, '2020-02-18 23:18:12', 0, NULL),
(5, NULL, 22, 'B', 0, '2020-02-18 23:18:12', 0, NULL),
(6, 5, 22, '4', 1, '2020-02-18 23:18:12', 0, NULL),
(7, 5, 22, '5', 1, '2020-02-18 23:18:12', 0, NULL),
(8, 5, 22, '6', 1, '2020-02-18 23:18:12', 0, NULL),
(9, NULL, 22, 'C', 0, '2020-02-18 23:18:12', 0, NULL),
(10, 9, 22, '7', 1, '2020-02-18 23:18:12', 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `grupo_estudiante`
--

CREATE TABLE `grupo_estudiante` (
  `id` int(11) NOT NULL,
  `id_estudiante_curso` int(11) NOT NULL,
  `id_grupo` int(11) NOT NULL,
  `creado` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Stand-in structure for view `institucion_programa`
-- (See below for the actual view)
--
CREATE TABLE `institucion_programa` (
`idPrograma` int(11)
,`codigoPrograma` varchar(50)
,`nombrePrograma` varchar(255)
,`idInstitucion` int(11)
,`nombreInstitucion` varchar(255)
);

-- --------------------------------------------------------

--
-- Table structure for table `planificacion`
--

CREATE TABLE `planificacion` (
  `id` int(11) NOT NULL,
  `id_periodo_ciclo` int(11) NOT NULL,
  `confirmado` tinyint(3) NOT NULL,
  `creado` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `planificacion`
--

INSERT INTO `planificacion` (`id`, `id_periodo_ciclo`, `confirmado`, `creado`) VALUES
(1, 22, 0, '2020-02-18 23:18:12');

-- --------------------------------------------------------

--
-- Table structure for table `programa`
--

CREATE TABLE `programa` (
  `id` int(11) NOT NULL,
  `codigo` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nombre` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `descripcion` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `creado` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `programa`
--

INSERT INTO `programa` (`id`, `codigo`, `nombre`, `descripcion`, `creado`) VALUES
(19, 'ppe', 'Programa Preventivo Escolar', NULL, '2020-02-18 23:18:07'),
(20, 'ulc', 'Universidad Libre de Caries', NULL, '2020-02-18 23:18:07'),
(21, 'clc', 'Comunidad Libre de Caries', NULL, '2020-02-18 23:18:07'),
(22, 'cesa', 'Clínica Extramural Santa Ana', NULL, '2020-02-18 23:18:07'),
(23, 'csns', 'Clinicas Sistema Nacional de Salud', NULL, '2020-02-18 23:18:07'),
(24, 'hnr', 'Hospital Nacional Rosales', NULL, '2020-02-18 23:18:07');

-- --------------------------------------------------------

--
-- Table structure for table `programa_calendario`
--

CREATE TABLE `programa_calendario` (
  `id` int(11) NOT NULL,
  `id_programa` int(11) NOT NULL,
  `id_periodo_ciclo` int(11) NOT NULL,
  `id_grupo` int(11) DEFAULT NULL,
  `tipo` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fecha` date NOT NULL,
  `descripcion` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `creado` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `programa_calendario`
--

INSERT INTO `programa_calendario` (`id`, `id_programa`, `id_periodo_ciclo`, `id_grupo`, `tipo`, `fecha`, `descripcion`, `creado`) VALUES
(1, 22, 22, 2, 'INICIO', '2018-07-23', NULL, '2020-02-18 23:18:12'),
(2, 22, 22, 2, 'FIN', '2018-08-10', NULL, '2020-02-18 23:18:12'),
(3, 22, 22, 3, 'INICIO', '2018-08-15', NULL, '2020-02-18 23:18:12'),
(4, 22, 22, 3, 'FIN', '2018-08-29', NULL, '2020-02-18 23:18:12'),
(5, 22, 22, 4, 'INICIO', '2018-09-04', NULL, '2020-02-18 23:18:12'),
(6, 22, 22, 4, 'FIN', '2018-09-18', NULL, '2020-02-18 23:18:12'),
(7, 22, 22, 6, 'INICIO', '2018-10-01', NULL, '2020-02-18 23:18:12'),
(8, 22, 22, 6, 'FIN', '2018-10-15', NULL, '2020-02-18 23:18:12'),
(9, 22, 22, 7, 'INICIO', '2018-10-19', NULL, '2020-02-18 23:18:12'),
(10, 22, 22, 7, 'FIN', '2018-11-05', NULL, '2020-02-18 23:18:12'),
(11, 22, 22, 8, 'INICIO', '2018-11-09', NULL, '2020-02-18 23:18:12'),
(12, 22, 22, 8, 'FIN', '2018-11-23', NULL, '2020-02-18 23:18:12'),
(13, 23, 22, 2, 'INICIO', '2018-07-23', NULL, '2020-02-18 23:18:12'),
(14, 23, 22, 2, 'FIN', '2018-08-10', NULL, '2020-02-18 23:18:12'),
(15, 23, 22, 3, 'INICIO', '2018-08-15', NULL, '2020-02-18 23:18:12'),
(16, 23, 22, 3, 'FIN', '2018-08-29', NULL, '2020-02-18 23:18:12'),
(17, 23, 22, 4, 'INICIO', '2018-09-04', NULL, '2020-02-18 23:18:12'),
(18, 23, 22, 4, 'FIN', '2018-09-18', NULL, '2020-02-18 23:18:12'),
(19, 23, 22, 6, 'INICIO', '2018-10-01', NULL, '2020-02-18 23:18:12'),
(20, 23, 22, 6, 'FIN', '2018-10-15', NULL, '2020-02-18 23:18:12'),
(21, 23, 22, 7, 'INICIO', '2018-10-19', NULL, '2020-02-18 23:18:12'),
(22, 23, 22, 7, 'FIN', '2018-11-05', NULL, '2020-02-18 23:18:12'),
(23, 23, 22, 8, 'INICIO', '2018-11-09', NULL, '2020-02-18 23:18:12'),
(24, 23, 22, 8, 'FIN', '2018-11-23', NULL, '2020-02-18 23:18:12'),
(25, 24, 22, 2, 'INICIO', '2018-07-23', NULL, '2020-02-18 23:18:12'),
(26, 24, 22, 2, 'FIN', '2018-08-14', NULL, '2020-02-18 23:18:12'),
(27, 24, 22, 3, 'INICIO', '2018-08-15', NULL, '2020-02-18 23:18:12'),
(28, 24, 22, 3, 'FIN', '2018-09-03', NULL, '2020-02-18 23:18:12'),
(29, 24, 22, 4, 'INICIO', '2018-09-04', NULL, '2020-02-18 23:18:12'),
(30, 24, 22, 4, 'FIN', '2018-09-21', NULL, '2020-02-18 23:18:12'),
(31, 24, 22, 6, 'INICIO', '2018-10-01', NULL, '2020-02-18 23:18:12'),
(32, 24, 22, 6, 'FIN', '2018-10-17', NULL, '2020-02-18 23:18:12'),
(33, 24, 22, 7, 'INICIO', '2018-10-19', NULL, '2020-02-18 23:18:12'),
(34, 24, 22, 7, 'FIN', '2018-11-07', NULL, '2020-02-18 23:18:12'),
(35, 24, 22, 8, 'INICIO', '2018-11-09', NULL, '2020-02-18 23:18:12'),
(36, 24, 22, 8, 'FIN', '2018-11-27', NULL, '2020-02-18 23:18:12'),
(37, 20, 22, 2, 'DIA UNICO', '2018-08-13', NULL, '2020-02-18 23:18:12'),
(38, 20, 22, 3, 'DIA UNICO', '2018-08-31', NULL, '2020-02-18 23:18:12'),
(39, 20, 22, 4, 'DIA UNICO', '2018-09-19', NULL, '2020-02-18 23:18:12'),
(40, 20, 22, 6, 'DIA UNICO', '2018-10-16', NULL, '2020-02-18 23:18:12'),
(41, 20, 22, 7, 'DIA UNICO', '2018-11-06', NULL, '2020-02-18 23:18:12'),
(42, 20, 22, 8, 'DIA UNICO', '2018-11-26', NULL, '2020-02-18 23:18:12'),
(43, 21, 22, 2, 'DIA UNICO', '2018-08-14', NULL, '2020-02-18 23:18:12'),
(44, 21, 22, 3, 'DIA UNICO', '2018-09-03', NULL, '2020-02-18 23:18:12'),
(45, 21, 22, 4, 'DIA UNICO', '2018-09-21', NULL, '2020-02-18 23:18:12'),
(46, 21, 22, 6, 'DIA UNICO', '2018-10-17', NULL, '2020-02-18 23:18:12'),
(47, 21, 22, 7, 'DIA UNICO', '2018-11-07', NULL, '2020-02-18 23:18:12'),
(48, 21, 22, 8, 'DIA UNICO', '2018-11-27', NULL, '2020-02-18 23:18:12'),
(49, 19, 22, 10, 'INICIO', '2018-07-19', NULL, '2020-02-18 23:18:12'),
(50, 19, 22, 10, 'FIN', '2018-11-22', NULL, '2020-02-18 23:18:12');

-- --------------------------------------------------------

--
-- Table structure for table `programa_institucion`
--

CREATE TABLE `programa_institucion` (
  `id` int(11) NOT NULL,
  `id_programa` int(11) NOT NULL,
  `id_institucion` int(11) NOT NULL,
  `cantidad_estudiante` int(11) NOT NULL,
  `activo` tinyint(3) NOT NULL,
  `creado` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `programa_institucion`
--

INSERT INTO `programa_institucion` (`id`, `id_programa`, `id_institucion`, `cantidad_estudiante`, `activo`, `creado`) VALUES
(52, 19, 109, 44, 1, '2020-02-18 23:18:07'),
(53, 19, 110, 44, 1, '2020-02-18 23:18:07'),
(54, 19, 111, 44, 1, '2020-02-18 23:18:07'),
(55, 19, 112, 44, 1, '2020-02-18 23:18:07'),
(56, 19, 113, 44, 1, '2020-02-18 23:18:07'),
(57, 19, 114, 44, 1, '2020-02-18 23:18:07'),
(58, 23, 101, 8, 1, '2020-02-18 23:18:07'),
(59, 23, 102, 8, 1, '2020-02-18 23:18:07'),
(60, 23, 103, 8, 1, '2020-02-18 23:18:07'),
(61, 23, 104, 8, 1, '2020-02-18 23:18:07'),
(62, 23, 105, 8, 1, '2020-02-18 23:18:07'),
(63, 23, 106, 8, 1, '2020-02-18 23:18:07'),
(64, 24, 107, 50, 1, '2020-02-18 23:18:07'),
(65, 22, 108, 48, 1, '2020-02-18 23:18:07'),
(66, 20, 97, 50, 1, '2020-02-18 23:18:07'),
(67, 21, 115, 50, 1, '2020-02-18 23:18:07'),
(68, 21, 116, 50, 1, '2020-02-18 23:18:07');

-- --------------------------------------------------------

--
-- Stand-in structure for view `programa_materia`
-- (See below for the actual view)
--
CREATE TABLE `programa_materia` (
`codigo` varchar(10)
,`nombre` varchar(255)
,`romano` varchar(5)
,`codigoPrograma` varchar(50)
);

-- --------------------------------------------------------

--
-- Table structure for table `rotacion`
--

CREATE TABLE `rotacion` (
  `id` int(11) NOT NULL,
  `id_padre` int(11) DEFAULT NULL,
  `id_planificacion` int(11) NOT NULL,
  `id_grupo` int(11) NOT NULL,
  `numero` int(11) NOT NULL,
  `inicio` date NOT NULL,
  `fin` date NOT NULL,
  `creado` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Stand-in structure for view `rotacion_detalle_view`
-- (See below for the actual view)
--
CREATE TABLE `rotacion_detalle_view` (
`curso` varchar(255)
,`carnet` varchar(8)
,`rotacion` int(11)
,`periodo_ciclo` int(11)
,`nombre_periodo_ciclo` varchar(255)
,`programa` varchar(50)
,`institucion` varchar(255)
,`inicio` date
,`fin` date
,`fechas` varchar(13)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `rotacion_detalle_view_completa`
-- (See below for the actual view)
--
CREATE TABLE `rotacion_detalle_view_completa` (
`curso` varchar(255)
,`carnet` varchar(8)
,`nombre` varchar(101)
,`rotacion` int(11)
,`tipo_rotacion` varchar(1)
,`periodo_ciclo` int(11)
,`nombre_periodo_ciclo` varchar(255)
,`programa` varchar(50)
,`institucion` varchar(255)
,`fechas` varchar(13)
);

-- --------------------------------------------------------

--
-- Table structure for table `rotacion_estudiante`
--

CREATE TABLE `rotacion_estudiante` (
  `id` int(11) NOT NULL,
  `id_rotacion` int(11) NOT NULL,
  `id_programa_institucion` int(11) NOT NULL,
  `id_grupo_estudiante` int(11) NOT NULL,
  `creado` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Stand-in structure for view `rotacion_resumen_email`
-- (See below for the actual view)
--
-- CREATE TABLE `rotacion_resumen_email` (
-- );

-- --------------------------------------------------------

--
-- Stand-in structure for view `vw_cupos_por_rotacion`
-- (See below for the actual view)
--
CREATE TABLE `vw_cupos_por_rotacion` (
`id_periodo_ciclo` int(11)
,`rotacion` int(11)
,`nombreRotacion` int(11)
,`programa` varchar(50)
,`id_institucion` int(11)
,`nombre_institucion` varchar(255)
,`cupos_max` decimal(11,0)
,`activos` bigint(21)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `vw_fechas_rotacion`
-- (See below for the actual view)
--
CREATE TABLE `vw_fechas_rotacion` (
`id_periodo_ciclo` int(11)
,`id_rotacion` int(11)
,`id_grupo` int(11)
,`rotacion` int(11)
,`programa` varchar(50)
,`fechas` varchar(13)
);

-- --------------------------------------------------------

--
-- Structure for view `cupo_intitucion_programa`
--
DROP TABLE IF EXISTS `cupo_intitucion_programa`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `cupo_intitucion_programa`  AS  (select `programa_institucion`.`cantidad_estudiante` AS `cantidad_estudiante`,`programa`.`codigo` AS `codigo`,`administracion`.`institucion`.`nombre` AS `nombre` from ((`programa_institucion` join `programa` on(`programa_institucion`.`id_programa` = `programa`.`id`)) join `administracion`.`institucion` on(`programa_institucion`.`id_institucion` = `administracion`.`institucion`.`id`))) ;

-- --------------------------------------------------------

--
-- Structure for view `estudiante_activo_materia`
--
DROP TABLE IF EXISTS `estudiante_activo_materia`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `estudiante_activo_materia`  AS  (select `academico`.`estudiante`.`carnet` AS `carnet`,`academico`.`estudiante_plan_estudio`.`cum` AS `cum`,`academico`.`estudiante_estado`.`tipo` AS `tipo`,`academico`.`estudiante_estado`.`codigo` AS `codigoEstudiante`,`academico`.`curso`.`codigo` AS `codigoCurso`,`academico`.`curso`.`nombre` AS `nombre` from ((((`academico`.`estudiante_plan_estudio` join `academico`.`estudiante` on(`academico`.`estudiante_plan_estudio`.`id_estudiante` = `academico`.`estudiante`.`id`)) join `academico`.`estudiante_curso` on(`academico`.`estudiante_curso`.`id_estudiante_plan_estudio` = `academico`.`estudiante_plan_estudio`.`id`)) join `academico`.`curso` on(`academico`.`estudiante_curso`.`id_curso` = `academico`.`curso`.`id`)) join `academico`.`estudiante_estado` on(`academico`.`estudiante_plan_estudio`.`id_estudiante_estado` = `academico`.`estudiante_estado`.`id`))) ;

-- --------------------------------------------------------

--
-- Structure for view `estudiante_activo_materia_grupo`
--
DROP TABLE IF EXISTS `estudiante_activo_materia_grupo`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `estudiante_activo_materia_grupo`  AS  (select `academico`.`estudiante`.`carnet` AS `carnet`,`academico`.`estudiante_plan_estudio`.`cum` AS `cum`,`academico`.`estudiante_estado`.`tipo` AS `tipo`,`academico`.`estudiante_estado`.`codigo` AS `codicoEstudiante`,`academico`.`curso`.`codigo` AS `codigoCurso`,`academico`.`curso`.`nombre` AS `nombre`,`grupo`.`nombre` AS `grupoEstudiante`,`grupo`.`id_periodo_ciclo` AS `peridoCiclo` from ((((((`academico`.`estudiante_plan_estudio` join `academico`.`estudiante` on(`academico`.`estudiante_plan_estudio`.`id_estudiante` = `academico`.`estudiante`.`id`)) join `academico`.`estudiante_curso` on(`academico`.`estudiante_curso`.`id_estudiante_plan_estudio` = `academico`.`estudiante_plan_estudio`.`id`)) join `academico`.`curso` on(`academico`.`estudiante_curso`.`id_curso` = `academico`.`curso`.`id`)) join `academico`.`estudiante_estado` on(`academico`.`estudiante_plan_estudio`.`id_estudiante_estado` = `academico`.`estudiante_estado`.`id`)) join `grupo_estudiante` on(`academico`.`estudiante_curso`.`id` = `grupo_estudiante`.`id_estudiante_curso`)) join `grupo` on(`grupo_estudiante`.`id_grupo` = `grupo`.`id`))) ;

-- --------------------------------------------------------

--
-- Structure for view `fechas_rotacion_view`
--
DROP TABLE IF EXISTS `fechas_rotacion_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `fechas_rotacion_view`  AS  (select `resumen`.`grupo` AS `grupo`,`resumen`.`rotacion` AS `rotacion`,`resumen`.`id_grupo` AS `id_grupo`,`resumen`.`periodoCiclo` AS `periodoCiclo`,min(case when `resumen`.`programa` <> 'PPE' then `resumen`.`fecha` else NULL end) AS `INICIO`,max(case when `resumen`.`programa` <> 'PPE' then `resumen`.`fecha` else NULL end) AS `FIN`,str_to_date(sum(case when (`resumen`.`tipo` = 'INICIO' and `resumen`.`programa` = 'CSNS') then `resumen`.`fecha` else NULL end),'%Y%m%d') AS `inicio_csns`,str_to_date(sum(case when (`resumen`.`tipo` = 'INICIO' and `resumen`.`programa` = 'CESA') then `resumen`.`fecha` else NULL end),'%Y%m%d') AS `inicio_cesa`,str_to_date(sum(case when (`resumen`.`tipo` = 'INICIO' and `resumen`.`programa` = 'HNR') then `resumen`.`fecha` else NULL end),'%Y%m%d') AS `inicio_hnr`,str_to_date(sum(case when (`resumen`.`tipo` = 'DIA UNICO' and `resumen`.`programa` = 'ULC') then `resumen`.`fecha` else NULL end),'%Y%m%d') AS `inicio_ulc`,str_to_date(sum(case when (`resumen`.`tipo` = 'DIA UNICO' and `resumen`.`programa` = 'CLC') then `resumen`.`fecha` else NULL end),'%Y%m%d') AS `inicio_clc`,str_to_date(sum(case when (`resumen`.`tipo` = 'INICIO' and `resumen`.`programa` = 'PPE') then `resumen`.`fecha` else NULL end),'%Y%m%d') AS `inicio_ppe`,str_to_date(sum(case when (`resumen`.`tipo` = 'FIN' and `resumen`.`programa` = 'CSNS') then `resumen`.`fecha` else NULL end),'%Y%m%d') AS `fin_csns`,str_to_date(sum(case when (`resumen`.`tipo` = 'FIN' and `resumen`.`programa` = 'CESA') then `resumen`.`fecha` else NULL end),'%Y%m%d') AS `fin_cesa`,str_to_date(sum(case when (`resumen`.`tipo` = 'FIN' and `resumen`.`programa` = 'HNR') then `resumen`.`fecha` else NULL end),'%Y%m%d') AS `fin_hnr`,str_to_date(sum(case when (`resumen`.`tipo` = 'FIN' and `resumen`.`programa` = 'PPE') then `resumen`.`fecha` else NULL end),'%Y%m%d') AS `fin_ppe` from (select `a`.`fecha` AS `fecha`,`a`.`tipo` AS `tipo`,`b`.`codigo` AS `programa`,`c`.`id` AS `id_grupo`,`c`.`nombre` AS `rotacion`,`d`.`nombre` AS `grupo`,`e`.`id_periodo` AS `periodoCiclo` from ((((`programa_calendario` `a` join `programa` `b` on(`a`.`id_programa` = `b`.`id`)) join `grupo` `c` on(`a`.`id_grupo` = `c`.`id`)) join `grupo` `d` on(`c`.`id_padre` = `d`.`id`)) join `academico`.`periodo_ciclo` `e` on(`a`.`id_periodo_ciclo` = `e`.`id`))) `resumen` group by `resumen`.`periodoCiclo`,`resumen`.`rotacion`,`resumen`.`periodoCiclo` order by `resumen`.`periodoCiclo`,`resumen`.`periodoCiclo`) ;

-- --------------------------------------------------------

--
-- Structure for view `institucion_programa`
--
DROP TABLE IF EXISTS `institucion_programa`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `institucion_programa`  AS  (select `programa`.`id` AS `idPrograma`,`programa`.`codigo` AS `codigoPrograma`,`programa`.`nombre` AS `nombrePrograma`,`administracion`.`institucion`.`id` AS `idInstitucion`,`administracion`.`institucion`.`nombre` AS `nombreInstitucion` from ((`programa_institucion` join `programa` on(`programa_institucion`.`id_programa` = `programa`.`id`)) join `administracion`.`institucion` on(`administracion`.`institucion`.`id` = `programa_institucion`.`id_institucion`))) ;

-- --------------------------------------------------------

--
-- Structure for view `programa_materia`
--
DROP TABLE IF EXISTS `programa_materia`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `programa_materia`  AS  (select `academico`.`curso`.`codigo` AS `codigo`,`academico`.`curso`.`nombre` AS `nombre`,`academico`.`plan_estudio_ciclo`.`romano` AS `romano`,`programa`.`codigo` AS `codigoPrograma` from ((((`academico`.`plan_estudio_curso` join `academico`.`curso` on(`academico`.`plan_estudio_curso`.`id_curso` = `academico`.`curso`.`id`)) join `academico`.`curso_programa` on(`academico`.`curso_programa`.`id_plan_estudio_curso` = `academico`.`plan_estudio_curso`.`id`)) join `academico`.`plan_estudio_ciclo` on(`academico`.`plan_estudio_curso`.`id_plan_estudio_ciclo` = `academico`.`plan_estudio_ciclo`.`id`)) join `programa` on(`academico`.`curso_programa`.`id_programa` = `programa`.`id`))) ;

-- --------------------------------------------------------

--
-- Structure for view `rotacion_detalle_view`
--
DROP TABLE IF EXISTS `rotacion_detalle_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `rotacion_detalle_view`  AS  select `i`.`nombre` AS `curso`,`l`.`carnet` AS `carnet`,`b`.`numero` AS `rotacion`,`g`.`id_periodo_ciclo` AS `periodo_ciclo`,`m`.`nombre` AS `nombre_periodo_ciclo`,`e`.`codigo` AS `programa`,`d`.`nombre` AS `institucion`,`b`.`inicio` AS `inicio`,`b`.`fin` AS `fin`,concat(date_format(`b`.`inicio`,'%d-%m'),' / ',date_format(`b`.`fin`,'%d-%m')) AS `fechas` from ((((((((((((`rotacion_estudiante` `a` join `rotacion` `b` on(`a`.`id_rotacion` = `b`.`id`)) join `programa_institucion` `c` on(`a`.`id_programa_institucion` = `c`.`id`)) join `administracion`.`institucion` `d` on(`c`.`id_institucion` = `d`.`id`)) join `programa` `e` on(`c`.`id_programa` = `e`.`id`)) join `grupo_estudiante` `f` on(`a`.`id_grupo_estudiante` = `f`.`id`)) join `grupo` `g` on(`f`.`id_grupo` = `g`.`id`)) join `academico`.`estudiante_curso` `h` on(`f`.`id_estudiante_curso` = `h`.`id`)) join `academico`.`curso` `i` on(`h`.`id_curso` = `i`.`id`)) join `academico`.`estudiante_curso` `j` on(`f`.`id_estudiante_curso` = `j`.`id`)) join `academico`.`estudiante_plan_estudio` `k` on(`j`.`id_estudiante_plan_estudio` = `k`.`id`)) join `academico`.`estudiante` `l` on(`k`.`id_estudiante` = `l`.`id`)) join `academico`.`periodo` `m` on(`g`.`id_periodo_ciclo` = `m`.`id`)) order by `g`.`id_periodo_ciclo`,`b`.`numero`,`e`.`codigo` ;

-- --------------------------------------------------------

--
-- Structure for view `rotacion_detalle_view_completa`
--
DROP TABLE IF EXISTS `rotacion_detalle_view_completa`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `rotacion_detalle_view_completa`  AS  select `a`.`curso` AS `curso`,`a`.`carnet` AS `carnet`,`a`.`nombre` AS `nombre`,`a`.`rotacion` AS `rotacion`,case when `a`.`rotacion` between 1 and 3 then 'A' when `a`.`rotacion` between 4 and 6 then 'B' else 'C' end AS `tipo_rotacion`,`a`.`periodo_ciclo` AS `periodo_ciclo`,`a`.`nombre_periodo_ciclo` AS `nombre_periodo_ciclo`,`a`.`programa` AS `programa`,`a`.`institucion` AS `institucion`,`b`.`fechas` AS `fechas` from ((select `i`.`nombre` AS `curso`,`l`.`carnet` AS `carnet`,concat(`n`.`nombre`,' ',`n`.`apellido`) AS `nombre`,`b`.`numero` AS `rotacion`,`g`.`id_periodo_ciclo` AS `periodo_ciclo`,`m`.`nombre` AS `nombre_periodo_ciclo`,`e`.`codigo` AS `programa`,`d`.`nombre` AS `institucion`,`b`.`inicio` AS `inicio`,`b`.`fin` AS `fin`,concat(date_format(`b`.`inicio`,'%d-%m'),' / ',date_format(`b`.`fin`,'%d-%m')) AS `fechas` from (((((((((((((`rotacion_estudiante` `a` join `rotacion` `b` on(`a`.`id_rotacion` = `b`.`id`)) join `programa_institucion` `c` on(`a`.`id_programa_institucion` = `c`.`id`)) join `administracion`.`institucion` `d` on(`c`.`id_institucion` = `d`.`id`)) join `programa` `e` on(`c`.`id_programa` = `e`.`id`)) join `grupo_estudiante` `f` on(`a`.`id_grupo_estudiante` = `f`.`id`)) join `grupo` `g` on(`f`.`id_grupo` = `g`.`id`)) join `academico`.`estudiante_curso` `h` on(`f`.`id_estudiante_curso` = `h`.`id`)) join `academico`.`curso` `i` on(`h`.`id_curso` = `i`.`id`)) join `academico`.`estudiante_curso` `j` on(`f`.`id_estudiante_curso` = `j`.`id`)) join `academico`.`estudiante_plan_estudio` `k` on(`j`.`id_estudiante_plan_estudio` = `k`.`id`)) join `academico`.`estudiante` `l` on(`k`.`id_estudiante` = `l`.`id`)) join `academico`.`periodo` `m` on(`g`.`id_periodo_ciclo` = `m`.`id`)) join `administracion`.`persona` `n` on(`l`.`id_persona` = `n`.`id`)) order by `g`.`id_periodo_ciclo`,`b`.`numero`,`e`.`codigo`) `a` join `vw_fechas_rotacion` `b` on(`a`.`periodo_ciclo` = `b`.`id_periodo_ciclo` and `a`.`rotacion` = `b`.`rotacion` and `a`.`programa` = `b`.`programa`)) ;

-- --------------------------------------------------------

--
-- Structure for view `rotacion_resumen_email`
--
DROP TABLE IF EXISTS `rotacion_resumen_email`;
-- in use(#1267 - Illegal mix of collations (utf8mb4_bin,NONE) and (utf8mb4_bin,NONE) for operation 'max(')

-- --------------------------------------------------------

--
-- Structure for view `vw_cupos_por_rotacion`
--
DROP TABLE IF EXISTS `vw_cupos_por_rotacion`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `vw_cupos_por_rotacion`  AS  select `e`.`id_periodo_ciclo` AS `id_periodo_ciclo`,`a`.`id_rotacion` AS `rotacion`,`d`.`numero` AS `nombreRotacion`,`f`.`codigo` AS `programa`,`b`.`id_institucion` AS `id_institucion`,`c`.`nombre` AS `nombre_institucion`,round(max(case when `f`.`codigo` = 'ppe' then `b`.`cantidad_estudiante` else `b`.`cantidad_estudiante` / 2 end),0) AS `cupos_max`,count(0) AS `activos` from (((((`rotacion_estudiante` `a` join `programa_institucion` `b` on(`a`.`id_programa_institucion` = `b`.`id`)) join `administracion`.`institucion` `c` on(`b`.`id_institucion` = `c`.`id`)) join `rotacion` `d` on(`a`.`id_rotacion` = `d`.`id`)) join `grupo` `e` on(`d`.`id_grupo` = `e`.`id`)) join `programa` `f` on(`b`.`id_programa` = `f`.`id`)) group by `e`.`id_periodo_ciclo`,`a`.`id_rotacion`,`c`.`nombre`,`f`.`codigo`,`b`.`id_institucion` order by 2,3 ;

-- --------------------------------------------------------

--
-- Structure for view `vw_fechas_rotacion`
--
DROP TABLE IF EXISTS `vw_fechas_rotacion`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `vw_fechas_rotacion`  AS  select `a`.`id_periodo_ciclo` AS `id_periodo_ciclo`,`a`.`id_rotacion` AS `id_rotacion`,`a`.`id_grupo` AS `id_grupo`,`a`.`rotacion` AS `rotacion`,`a`.`programa` AS `programa`,case when `a`.`programa` in ('ulc','clc') then date_format(`a`.`inicio`,'%d-%m') else concat(date_format(`a`.`inicio`,'%d-%m'),' / ',date_format(`a`.`fin`,'%d-%m')) end AS `fechas` from (select `a`.`id_periodo_ciclo` AS `id_periodo_ciclo`,`c`.`numero` AS `rotacion`,`c`.`id` AS `id_rotacion`,`a`.`id_grupo` AS `id_grupo`,`b`.`codigo` AS `programa`,min(`a`.`fecha`) AS `inicio`,max(`a`.`fecha`) AS `fin` from ((`programa_calendario` `a` join `programa` `b` on(`a`.`id_programa` = `b`.`id`)) join `rotacion` `c` on(`a`.`id_grupo` = `c`.`id_grupo`)) group by `a`.`id_periodo_ciclo`,`b`.`codigo`,`a`.`id_grupo`,`c`.`numero`,`c`.`id` order by 1,2,3) `a` ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `grupo`
--
ALTER TABLE `grupo`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_grupo_padre` (`id_padre`),
  ADD KEY `fk_grupo_periodo_ciclo` (`id_periodo_ciclo`),
  ADD KEY `fk_grupo_plan_estudio_curso` (`id_plan_estudio_curso`);

--
-- Indexes for table `grupo_estudiante`
--
ALTER TABLE `grupo_estudiante`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_grupo_estudiante_curso` (`id_estudiante_curso`),
  ADD KEY `fk_grupo_estudiante` (`id_grupo`);

--
-- Indexes for table `planificacion`
--
ALTER TABLE `planificacion`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_planificacion_periodo_ciclo` (`id_periodo_ciclo`);

--
-- Indexes for table `programa`
--
ALTER TABLE `programa`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `codigo` (`codigo`);

--
-- Indexes for table `programa_calendario`
--
ALTER TABLE `programa_calendario`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_programa_periodo_ciclo` (`id_periodo_ciclo`),
  ADD KEY `fk_programa_calendario_grupo` (`id_grupo`),
  ADD KEY `fk_programa_calendario` (`id_programa`);

--
-- Indexes for table `programa_institucion`
--
ALTER TABLE `programa_institucion`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `unique_programa_institucion` (`id_programa`,`id_institucion`),
  ADD KEY `fk_institucion_programa` (`id_institucion`);

--
-- Indexes for table `rotacion`
--
ALTER TABLE `rotacion`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_rotacion_padre` (`id_padre`),
  ADD KEY `fk_rotacion_planificacion` (`id_planificacion`),
  ADD KEY `fk_rotacion_grupo` (`id_grupo`);

--
-- Indexes for table `rotacion_estudiante`
--
ALTER TABLE `rotacion_estudiante`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_rotacion_estudiante` (`id_rotacion`),
  ADD KEY `fk_rotacion_estudiante_programa_institucion` (`id_programa_institucion`),
  ADD KEY `fk_rotacion_estudiante_grupo` (`id_grupo_estudiante`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `grupo`
--
ALTER TABLE `grupo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `grupo_estudiante`
--
ALTER TABLE `grupo_estudiante`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `planificacion`
--
ALTER TABLE `planificacion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `programa`
--
ALTER TABLE `programa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `programa_calendario`
--
ALTER TABLE `programa_calendario`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;
--
-- AUTO_INCREMENT for table `programa_institucion`
--
ALTER TABLE `programa_institucion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=69;
--
-- AUTO_INCREMENT for table `rotacion`
--
ALTER TABLE `rotacion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `rotacion_estudiante`
--
ALTER TABLE `rotacion_estudiante`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `grupo`
--
ALTER TABLE `grupo`
  ADD CONSTRAINT `fk_grupo_padre` FOREIGN KEY (`id_padre`) REFERENCES `grupo` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `fk_grupo_periodo_ciclo` FOREIGN KEY (`id_periodo_ciclo`) REFERENCES `academico`.`periodo_ciclo` (`id`),
  ADD CONSTRAINT `fk_grupo_plan_estudio_curso` FOREIGN KEY (`id_plan_estudio_curso`) REFERENCES `academico`.`plan_estudio_curso` (`id`);

--
-- Constraints for table `grupo_estudiante`
--
ALTER TABLE `grupo_estudiante`
  ADD CONSTRAINT `fk_grupo_estudiante` FOREIGN KEY (`id_grupo`) REFERENCES `grupo` (`id`),
  ADD CONSTRAINT `fk_grupo_estudiante_curso` FOREIGN KEY (`id_estudiante_curso`) REFERENCES `academico`.`estudiante_curso` (`id`);

--
-- Constraints for table `planificacion`
--
ALTER TABLE `planificacion`
  ADD CONSTRAINT `fk_planificacion_periodo_ciclo` FOREIGN KEY (`id_periodo_ciclo`) REFERENCES `academico`.`periodo_ciclo` (`id`);

--
-- Constraints for table `programa_calendario`
--
ALTER TABLE `programa_calendario`
  ADD CONSTRAINT `fk_programa_calendario` FOREIGN KEY (`id_programa`) REFERENCES `programa` (`id`),
  ADD CONSTRAINT `fk_programa_calendario_grupo` FOREIGN KEY (`id_grupo`) REFERENCES `grupo` (`id`),
  ADD CONSTRAINT `fk_programa_periodo_ciclo` FOREIGN KEY (`id_periodo_ciclo`) REFERENCES `academico`.`periodo_ciclo` (`id`);

--
-- Constraints for table `programa_institucion`
--
ALTER TABLE `programa_institucion`
  ADD CONSTRAINT `fk_institucion_programa` FOREIGN KEY (`id_institucion`) REFERENCES `administracion`.`institucion` (`id`),
  ADD CONSTRAINT `fk_programa_institucion` FOREIGN KEY (`id_programa`) REFERENCES `programa` (`id`);

--
-- Constraints for table `rotacion`
--
ALTER TABLE `rotacion`
  ADD CONSTRAINT `fk_rotacion_grupo` FOREIGN KEY (`id_grupo`) REFERENCES `grupo` (`id`),
  ADD CONSTRAINT `fk_rotacion_padre` FOREIGN KEY (`id_padre`) REFERENCES `rotacion` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `fk_rotacion_planificacion` FOREIGN KEY (`id_planificacion`) REFERENCES `planificacion` (`id`);

--
-- Constraints for table `rotacion_estudiante`
--
ALTER TABLE `rotacion_estudiante`
  ADD CONSTRAINT `fk_rotacion_estudiante` FOREIGN KEY (`id_rotacion`) REFERENCES `rotacion` (`id`),
  ADD CONSTRAINT `fk_rotacion_estudiante_grupo` FOREIGN KEY (`id_grupo_estudiante`) REFERENCES `grupo_estudiante` (`id`),
  ADD CONSTRAINT `fk_rotacion_estudiante_programa_institucion` FOREIGN KEY (`id_programa_institucion`) REFERENCES `programa_institucion` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
