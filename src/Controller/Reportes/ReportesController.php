<?php

namespace App\Controller\Reportes;

use App\Service\ReporteService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Dompdf\Dompdf;
use Dompdf\Options;

/**
 * Class ReportesController
 * @package App\Controller\Reportes
 */
class ReportesController extends AbstractController
{

    /**
     * ReportesController constructor.
     * @param ContainerInterface $container
     * @param ReporteService $reporteService
     */
    public function __construct(ContainerInterface $container, ReporteService $reporteService)
    {
        $this->container = $container;
        $this->denyAccessUnlessGranted('reporte');
        $this->reporteService = $reporteService;
    }

    /**
     * @Route("/reportes/index",name="reportes_index")
     * @return Response
     */
    public function index()
    {
        return $this->render('reportes/index.html.twig');
    }

    /**
     * @Route("reportes/direccion/index", name="parametros_direccion", methods="GET|POST")
     * @return Response
     */
    public function direccion_index()
    {
        return $this->render('reportes/parametros_reportes.html.twig');
    }

    /**
     * @Route("/reportes/extramural/index",name="extramural_index")
     * @return Response
     */
    public function extramural_index()
    {
        return $this->render('reportes/extramural_index.html.twig');
    }

    /**
     * @Route("reportes/extramural/parametros/ingresos", name="parametros_ingresos", methods="GET|POST")
     * @return Response
     */
    public function parametros_ingresos()
    {
        return $this->render('reportes/parametros_extramural_1.html.twig');

    }

    /**
     * @Route("reportes/extramural/parametros/procedencia", name="parametros_procedencia", methods="GET|POST")
     * @return Response
     */
    public function parametros_procedencia()
    {
        return $this->render('reportes/parametros_extramural_2.html.twig');

    }

    /**
     * @Route("reportes/extramural/parametros/almacen", name="parametros_almacen", methods="GET|POST")
     * @return Response
     */
    public function parametros_almacen()
    {
        $almacen = $this->reporteService->getAlmacenAll();
        return $this->render('reportes/parametros_extramural_3.html.twig', ['almacenes' => $almacen]);
    }

    /**
     * @Route("/reportes/direccion", name="reportes_estadisticos")
     * @param Request $request
     * @return Response
     * @throws \Doctrine\DBAL\DBALException
     */
    public function estadisticos_view(Request $request)
    {
        $atendidas = $this->reporteService->obtenerPersonasAtendidas($request->get('fecha_inicio'), $request->get('fecha_fin'));
        //dd($atendidas);
        $primera_vez = $this->reporteService->obtenerAtendidosPrimeraVez($request->get('fecha_inicio'), $request->get('fecha_fin'));
        $embarazadas = $this->reporteService->obtenerEmbarazadasAtendidas($request->get('fecha_inicio'), $request->get('fecha_fin'));
        $tratamientos = $this->reporteService->obtenerTratamientosEjecutados($request->get('fecha_inicio'), $request->get('fecha_fin'));
        return $this->render('reportes/estadisticos.html.twig', ['atendidas' => $atendidas, 'primera_vez' => $primera_vez, 'embarazadas' => $embarazadas, 'tratamientos' => $tratamientos, 'fecha_inicio' => $request->get('fecha_inicio'), 'fecha_fin' => $request->get('fecha_fin')]);
    }

    /**
     * @Route("/reportes/extramural/ingresos", name="reporte_ingresos_view")
     * @param Request $request
     * @return Response
     * @throws \Doctrine\DBAL\DBALException
     */
    public function ingresos_view(Request $request)
    {

        $ingresos = $this->reporteService->obtenerIngresosPorTratamientos($request->get('fecha_inicio'), $request->get('fecha_fin'));
        return $this->render('reportes/extramural_ingresos_view.html.twig', ['ingresos' => $ingresos, 'fecha_inicio' => $request->get('fecha_inicio'), 'fecha_fin' => $request->get('fecha_fin')]);
    }

    /**
     * @Route("/reportes/extramural/procedencia", name="reporte_procedencia_view")
     * @param Request $request
     * @return Response
     * @throws \Doctrine\DBAL\DBALException
     */
    public function procedencia_view(Request $request)
    {

        $procedencias = $this->reporteService->obtenerTratamientosPorProcedencia($request->get('fecha_inicio'), $request->get('fecha_fin'));
        return $this->render('reportes/extramural_procedencia_view.html.twig', ['procedencias' => $procedencias, 'fecha_inicio' => $request->get('fecha_inicio'), 'fecha_fin' => $request->get('fecha_fin')]);
    }

    /**
     * @Route("/reportes/extramural/almacen", name="reporte_almacen_view")
     * @param Request $request
     * @return Response
     * @throws \Doctrine\DBAL\DBALException
     */
    public function almacen_view(Request $request)
    {
        //dd($request->get('fecha_inicio'),$request->get('fecha_fin'));
        $almacen = $this->reporteService->obtenerConsumoFecha($request->get('fecha_inicio'), $request->get('fecha_fin'), $request->get('almacen'));
        //dd($almacen);
        return $this->render('reportes/extramural_almacen_view.html.twig', ['almacenes' => $almacen, 'fecha_inicio' => $request->get('fecha_inicio'), 'fecha_fin' => $request->get('fecha_fin'), 'almacen' => $request->get('almacen')]);
    }

    /**
     * @Route("/reportes/estadisticos/view",name="reportes_est")
     * @param Request $request
     * @return Response
     * @throws \Doctrine\DBAL\DBALException
     */
    public function reporte_direccion(Request $request)
    {
        $atendidas = $this->reporteService->obtenerPersonasAtendidas($request->get('fecha_inicio'), $request->get('fecha_fin'));
        $primera_vez = $this->reporteService->obtenerAtendidosPrimeraVez($request->get('fecha_inicio'), $request->get('fecha_fin'));
        $embarazadas = $this->reporteService->obtenerEmbarazadasAtendidas($request->get('fecha_inicio'), $request->get('fecha_fin'));
        $tratamientos = $this->reporteService->obtenerTratamientosEjecutados($request->get('fecha_inicio'), $request->get('fecha_fin'));
        // Configure Dompdf according to your needs
        $pdfOptions = new Options();
        $pdfOptions->set('defaultFont', 'Arial');

        // Instantiate Dompdf with our options
        $dompdf = new Dompdf($pdfOptions);

        // Retrieve the HTML generated in our twig file
        $html = $this->renderView('reportes/estadisticos_print.html.twig', [
            'title' => "Bienvenido al sistema SIGEPREX",
            'atendidas' => $atendidas,
            'primera_vez' => $primera_vez,
            'embarazadas' => $embarazadas,
            'tratamientos' => $tratamientos
        ]);

        // Load HTML to Dompdf
        $dompdf->loadHtml($html);

        // (Optional) Setup the paper size and orientation 'portrait' or 'portrait'
        $dompdf->setPaper('A4', 'portrait');

        // Render the HTML as PDF
        $dompdf->render();

        // Output the generated PDF to Browser (force download)
        $dompdf->stream("peyps.pdf", [
            "Attachment" => false
        ]);
    }

    /**
     * @Route("/reportes/extramural/ingresos/print",name="reporte_ingresos_print")
     * @param Request $request
     * @return Response
     * @throws \Doctrine\DBAL\DBALException
     */
    public function reporte_ingresos(Request $request)
    {
        $ingresos = $this->reporteService->obtenerIngresosPorTratamientos($request->get('fecha_inicio'), $request->get('fecha_fin'));
        // Configure Dompdf according to your needs
        $pdfOptions = new Options();
        $pdfOptions->set('defaultFont', 'Arial');

        // Instantiate Dompdf with our options
        $dompdf = new Dompdf($pdfOptions);

        // Retrieve the HTML generated in our twig file
        $html = $this->renderView('reportes/extramural_ingresos_print.html.twig', [
            'title' => "Bienvenido al sistema SIGEPREX",
            'ingresos' => $ingresos
        ]);

        // Load HTML to Dompdf
        $dompdf->loadHtml($html);

        // (Optional) Setup the paper size and orientation 'portrait' or 'portrait'
        $dompdf->setPaper('A4', 'portrait');

        // Render the HTML as PDF
        $dompdf->render();

        // Output the generated PDF to Browser (force download)
        $dompdf->stream("peyps.pdf", [
            "Attachment" => false
        ]);
    }

    /**
     * @Route("/reportes/extramural/procedencia/print",name="reporte_procedencia_print")
     * @param Request $request
     * @return Response
     * @throws \Doctrine\DBAL\DBALException
     */
    public function reporte_procedencia(Request $request)
    {
        $procedencia = $this->reporteService->obtenerTratamientosPorProcedencia($request->get('fecha_inicio'), $request->get('fecha_fin'));
        // Configure Dompdf according to your needs
        $pdfOptions = new Options();
        $pdfOptions->set('defaultFont', 'Arial');

        // Instantiate Dompdf with our options
        $dompdf = new Dompdf($pdfOptions);

        // Retrieve the HTML generated in our twig file
        $html = $this->renderView('reportes/extramural_procedencia_print.html.twig', [
            'title' => "Bienvenido al sistema SIGEPREX",
            'procedencias' => $procedencia
        ]);

        // Load HTML to Dompdf
        $dompdf->loadHtml($html);

        // (Optional) Setup the paper size and orientation 'portrait' or 'portrait'
        $dompdf->setPaper('A4', 'portrait');

        // Render the HTML as PDF
        $dompdf->render();

        // Output the generated PDF to Browser (force download)
        $dompdf->stream("peyps.pdf", [
            "Attachment" => false
        ]);
    }

    /**
     * @Route("/reportes/extramural/almacen/print",name="reporte_almacen_print")
     * @param Request $request
     * @return Response
     * @throws \Doctrine\DBAL\DBALException
     */
    public function reporte_almacen(Request $request)
    {
        $almacen = $this->reporteService->obtenerConsumoFecha($request->get('fecha_inicio'), $request->get('fecha_fin'), $request->get('almacen'));
        // Configure Dompdf according to your needs
        $pdfOptions = new Options();
        $pdfOptions->set('defaultFont', 'Arial');

        // Instantiate Dompdf with our options
        $dompdf = new Dompdf($pdfOptions);

        // Retrieve the HTML generated in our twig file
        $html = $this->renderView('reportes/extramural_almacen_print.html.twig', [
            'title' => "Bienvenido al sistema SIGEPREX",
            'almacenes' => $almacen
        ]);

        // Load HTML to Dompdf
        $dompdf->loadHtml($html);

        // (Optional) Setup the paper size and orientation 'portrait' or 'portrait'
        $dompdf->setPaper('A4', 'portrait');

        // Render the HTML as PDF
        $dompdf->render();

        // Output the generated PDF to Browser (force download)
        $dompdf->stream("peyps.pdf", [
            "Attachment" => false
        ]);
    }

}