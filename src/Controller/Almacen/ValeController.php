<?php

namespace App\Controller\Almacen;

use App\Entity\Administracion\Usuario;
use App\Entity\Almacen\Almacen;
use App\Entity\Almacen\ArticuloPresentacion;
use App\Entity\Almacen\Consumo;
use App\Entity\Almacen\Existencia;
use App\Entity\Almacen\Movimiento;
use App\Entity\Almacen\Vale;
use App\Entity\Almacen\ValeEstado;
use App\Entity\Almacen\ValeEvaluacionDental;
use App\Entity\Clinico\EvaluacionDental;
use App\Entity\Clinico\Ficha;
use App\Entity\Clinico\Tratamiento;
use App\Form\Almacen\ValeType;
use Doctrine\DBAL\DBALException;
use Doctrine\ORM\Query\ResultSetMapping;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * @Route("/almacen/vale")
 */
class ValeController extends AbstractController
{
    /**
     * ValeController constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->denyAccessUnlessGranted('vale');
    }

    /**
     * @Route("/", name="almacen_vale_index", methods="GET")
     */
    public function index(Request $request): Response
    {
        $em = $this->getDoctrine()->getManager();
        $id_almacen = $request->query->get('almacen');
        $almacen = $em->getRepository(Almacen::class)->find($id_almacen);
        $vales = $em->getRepository(Vale::class)->findAll();
        return $this->render('almacen/vale/index.html.twig', [
            'almacen' => $almacen,
            'vales' => $vales,
        ]);
    }

    /**
     * @Route("/{almacen}/new", name="almacen_vale_new", methods="GET|POST")
     * @param Request $request
     * @param Almacen $almacen
     * @return Response
     */
    public function new(Request $request, Almacen $almacen): Response
    {
        $vale = new Vale();
        $usuario = $this->getUser();
        $form = $this->createForm(ValeType::class, $vale);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($vale);
            $em->flush();

            return $this->redirectToRoute('almacen_vale_index');
        }
        return $this->render('almacen/vale/new.html.twig', [
            'vale' => $vale,
            'almacen' => $almacen,
            'usuario' => $usuario,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/{almacen}", name="almacen_vale_show", methods="GET", requirements={"id"="\d+","almacen"="\d+"})
     */
    public function show(Vale $vale, Almacen $almacen): Response
    {
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();
        /** @var ValeEvaluacionDental[] $vale_evaluacion_dental */
        $vale_evaluacion_dental = $em->getRepository(ValeEvaluacionDental::class)->findBy(['vale' => $vale]);
        $no_expediente = $no_ficha = $persona = $ficha = $expediente = null;
        if (isset($vale_evaluacion_dental[0])) {
            $ficha = $vale_evaluacion_dental[0]->getEvaluacionDental()->getFicha();
            $expediente = $ficha->getExpedienteClinico();
            $persona = $expediente->getPersona();
            $no_ficha = $ficha->getId();
            $no_expediente = $expediente->getCodigoExpediente();
        }
        $datosSinFicha = (array)json_decode($vale->getDatos());


        return $this->render('almacen/vale/show.html.twig', ['vale' => $vale,
            'persona' => $persona,
            'no_ficha' => $no_ficha,
            'no_expediente' => $no_expediente,
            'almacen' => $almacen,
            'datosSinFicha' => $datosSinFicha,
            'tratamientos' => $vale_evaluacion_dental
        ]);
    }

    /**
     * @Route("/{id}/{almacen}/edit", name="almacen_vale_edit", methods="GET|POST", requirements={"id"="\d+","almacen"="\d+"})
     */
    public function edit(Request $request, Vale $vale, Almacen $almacen): Response
    {
        $form = $this->createForm(ValeType::class, $vale);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('almacen_vale_edit', ['id' => $vale->getId()]);
        }

        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();
        /** @var ValeEvaluacionDental[] $vale_evaluacion_dental */
        $vale_evaluacion_dental = $em->getRepository(ValeEvaluacionDental::class)
            ->findBy(['vale' => $vale]);
        $evaluaciones_dentales = [];
        $persona = $no_expediente = $no_ficha = $expediente = $ficha = null;

        $fichas = [];
        if (isset($vale_evaluacion_dental[0])) {
            $ficha = $vale_evaluacion_dental[0]->getEvaluacionDental()->getFicha();
            $expediente = $ficha->getExpedienteClinico();
            $persona = $expediente->getPersona();
            $no_ficha = $ficha->getId();
            $no_expediente = $expediente->getId();
            $fichas = $em->getRepository('Clinico:Ficha')
                ->findByCodigoExpediente($expediente->getCodigoExpediente());
            $evaluaciones_dentales = $em->getRepository('Clinico:EvaluacionDental')
                ->findPrioritariosByFicha($ficha);
        }
        $usuario = $this->getUser();

        $datosSinFicha = (array)json_decode($vale->getDatos());

        return $this->render('almacen/vale/edit.html.twig', [
            'vale' => $vale,
            'form' => $form->createView(),
            'persona' => $persona,
            'usuario' => $usuario,
            'no_ficha' => $no_ficha,
            'no_expediente' => $no_expediente,
            'almacen' => $almacen,
            'tratamientos' => $evaluaciones_dentales,
            'valeTratamientos' => $vale_evaluacion_dental,
            'datosSinFicha' => $datosSinFicha,
            'fichas' => $fichas,
        ]);
    }

    /**
     * @Route("/{vale}/almacen/{almacen}", name="almacen_vale_delete", methods="DELETE", requirements={"vale"="\d+","almacen"="\d+"})
     * @param Request $request
     * @param Vale $vale
     * @param Almacen $almacen
     * @param LoggerInterface $logger
     * @return Response
     */
    public function delete(Request $request, Vale $vale, Almacen $almacen, LoggerInterface $logger): Response
    {
        if ($this->isCsrfTokenValid('delete' . $vale->getId(), $request->request->get('_token'))) {
            if ($vale->getValeEstado()->getCodigo() == 'pendiente') {
                /** @var \Doctrine\ORM\EntityManager $em */
                $em = $this->getDoctrine()->getManager();
                try {
                    $em->beginTransaction();
                    $consumos = $em->getRepository('Almacen:Consumo')
                        ->findBy(['vale' => $vale]);
                    foreach ($consumos as $consumo) {
                        $em->remove($consumo);
                    }
                    $vale_evaluaciones_dentales = $em->getRepository('Almacen:ValeEvaluacionDental')
                        ->findBy(['vale' => $vale]);
                    foreach ($vale_evaluaciones_dentales as $vale_evaluacion_dental) {
                        $em->remove($vale_evaluacion_dental);
                    }
                    $vale_estado_historiales = $em->getRepository('Almacen:ValeEstadoHistorial')
                        ->findBy(['vale' => $vale]);
                    foreach ($vale_estado_historiales as $vale_estado_historial) {
                        $em->remove($vale_estado_historial);
                    }
                    $em->remove($vale);
                    $em->flush();
                    $em->commit();
                    $this->addFlash('success', 'Vale eliminado!');
                } catch (\Exception $e) {
                    $em->rollback();
                    $logger->error($e);
                    $this->addFlash('danger', 'No se pudo eliminar el vale!');
                }
            } else {
                $this->addFlash('warning', 'El vale no puede ser eliminado!');
            }
        }
        return $this->redirectToRoute('almacen_vale_index', ['almacen' => $almacen->getId()]);
    }


    /**
     * @Route("/persona/expediente/{codigo_expediente}", name="vale_expediente", methods={"GET"})
     */
    public function getExpediente(string $codigo_expediente, SerializerInterface $serializer)
    {
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();
        $expediente = $em->getRepository('Clinico:ExpedienteClinico')->findOneBy(['codigo_expediente' => $codigo_expediente]);
        $data = $serializer->serialize($expediente, 'json');
        return JsonResponse::fromJsonString($data);
    }

    /**
     * @Route("/list/fichas/{codigo_expediente}", name="vale_fichas", methods={"GET"})
     */
    public function getFichas(string $codigo_expediente, SerializerInterface $serializer)
    {
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();
        $fichas = $em->getRepository('Clinico:Ficha')->findByCodigoExpediente($codigo_expediente);
        $data = $serializer->serialize($fichas, 'json');
        return JsonResponse::fromJsonString($data);
    }

    /**
     * @Route("/list/tratamientos/{ficha}", name="vale_tratamientos", methods={"GET"})
     */
    public function getTratamientos(Ficha $ficha, SerializerInterface $serializer)
    {
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();
        $evaluaciones_dentales = $em->getRepository('Clinico:EvaluacionDental')->findPrioritariosByFicha($ficha);
        $data = $serializer->serialize($evaluaciones_dentales, 'json');
        return JsonResponse::fromJsonString($data);
    }

    /**
     * @Route("/list/articulos/{tratamiento}", name="vale_articulos", methods={"GET"})
     */
    public function getArticulos(Tratamiento $tratamiento, SerializerInterface $serializer)
    {
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();
        $tratamientos_articulos = $em->getRepository('Almacen:TratamientoArticuloPresentacion')
            ->findByTratamiento($tratamiento);
        $data = $serializer->serialize($tratamientos_articulos, 'json');
        return JsonResponse::fromJsonString($data);
    }

    /**
     * @Route("/validar/{id_almacen}/{id_articulo_presentacion}", name="vale_validar_articulo", methods={"GET"})
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function validarArticuloPresentacion(int $id_almacen, int $id_articulo_presentacion)
    {
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();
        $dql = "SELECT SUM(e.cantidad) AS total FROM Almacen:Existencia e 
            JOIN e.articulo_presentacion ap
            JOIN e.almacen a
            WHERE a.id = ?1
            AND ap.id=?2";
        $total = $em->createQuery($dql)
            ->setParameter(1, $id_almacen)
            ->setParameter(2, $id_articulo_presentacion)
            ->getSingleScalarResult();
        return new JsonResponse($total);
    }

    /**
     * @Route("/action/guardar", name="vale_guardar", methods={"GET"})
     */
    public function guardarVale(Request $request, LoggerInterface $logger)
    {

        $info = $request->get('info');
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();
        $vale = new Vale();
        $vale->setDatos($info);
        $vale_estado = $em->getRepository(ValeEstado::class)->findOneBy(['codigo' => 'pendiente']);
        $em->beginTransaction();
        try {
            $vale->setValeEstado($vale_estado);
            $vale->setUsuario($this->getUser());
            $em->persist($vale);
            $em->flush();
            $em->commit();

            $data = ['error' => 0, 'message' => 'Vale guardado', 'vale' => $vale->getId()];
        } catch (\Exception $e) {
            $em->rollback();
            $logger->error($e);
            $data = ['error' => 1, 'message' => 'No se pudo guardar el vale', 'exception' => $e->getMessage()];
        }
        return new JsonResponse($data);
    }


    /**
     * @Route("/action/guardar/valeTratamiento/{vale}/{evaluacion_dental}", name="vale_evaluacion_dental_guardar", methods={"GET"})
     */
    public function guardarValeTratamiento(Vale $vale, EvaluacionDental $evaluacion_dental, LoggerInterface $logger)
    {
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();
        $vale_evaluacion_dental = new ValeEvaluacionDental();

        $em->beginTransaction();
        try {
            $vale_evaluacion_dental->setVale($vale);
            $vale_evaluacion_dental->setEvaluacionDental($evaluacion_dental);
            $em->persist($vale_evaluacion_dental);
            $em->flush();
            $em->commit();

            $data = ['error' => 0, 'message' => 'Tratamiento asociado', 'vale_evaluacion_dental' => $vale_evaluacion_dental->getId()];
        } catch (\Exception $e) {
            $em->rollback();
            $logger->error($e);
            $data = ['error' => 1, 'message' => 'No se pudo asociar el tratamiento', 'exception' => $e->getMessage()];
        }
        return new JsonResponse($data);
    }


    /**
     * @Route("/action/guardarMovimiento", name="vale_guardar_movimiento", methods={"POST"})
     * @throws \Exception
     */
    public function guardarMovimiento(Request $request)
    {
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();
        $id_almacen = $request->request->get('almacen');
        $id_articulo_presentacion = $request->request->get('articulo_presentacion');
        $concepto = $request->request->get('concepto');
        $precio = $request->request->get('precio');
        $cantidad = $request->request->get('cantidad');
        $id_vale = $request->request->get('vale');

        $articulo_presentacion = $em->getRepository(ArticuloPresentacion::class)->findOneBy(["id" => $id_articulo_presentacion]);
        $almacen = $em->getRepository(Almacen::class)->findOneBy(["id" => $id_almacen]);
        $usuario = $em->getRepository(Usuario::class)->findOneByUsername($this->getUser()->getUsername());
        $vale = $em->getRepository(Vale::class)->findOneBy(["id" => $id_vale]);
        $em->beginTransaction();
        try {
            $movimiento = new Movimiento();
            $movimiento->setAlmacen($almacen);
            $movimiento->setArticuloPresentacion($articulo_presentacion);
            $movimiento->setUsuario($usuario);
            $movimiento->setCantidad($cantidad);
            $movimiento->setConcepto($concepto);
            $movimiento->setConfirmado(false);
            $movimiento->setFecha(new \DateTime());
            $movimiento->setPrecio($precio);
            $em->persist($movimiento);
            $em->flush();
            $movimiento->getId();

            $consumo = new Consumo();
            $consumo->setMovimiento($movimiento);
            $consumo->setVale($vale);
            $em->persist($consumo);
            $em->flush();
            $em->commit();
            $data = [
                'error' => 0,
                'message' => 'Vale creado con exito',
            ];
        } catch (\Exception $e) {
            $data = [
                'error' => 1,
                'message' => 'No se pudo crear el vale',
                'exception' => $e->getMessage(),
            ];
        }
        return new JsonResponse($data);
    }


    /**
     * @Route("/movimientos/list/{vale}", name="vale_movimientos", methods={"GET"})
     */
    public function getMovimientos(Vale $vale, SerializerInterface $serializer)
    {
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();
        $consumos = $em->getRepository('Almacen:Consumo')->findWithTratamientosByValeScalar($vale);
        $data = $serializer->serialize($consumos, 'json');
        return JsonResponse::fromJsonString($data);
    }

    /*Funcion que valida las cantidades de los movimientos contra las existencias del almacen
      Retorna los movimientos que exceden de la cantidad en existencia disponible  */
    /**
     * @Route("/movimientos/validar/{idVale}/{idAlmacen}", name="vale_movimientos_validar", methods={"GET"})
     */
    public function validarMovimientos(int $idVale, int $idAlmacen)
    {
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();
        $rsm = new ResultSetMapping();
        $rsm->addEntityResult('App\Entity\Almacen\Movimiento', 'm');
        $rsm->addFieldResult('m', 'total', 'cantidad');
        $rsm->addJoinedEntityResult('App\Entity\Almacen\ArticuloPresentacion', 'ap', 'm', 'articulo_presentacion');
        $rsm->addFieldResult('ap', 'id_articulo_presentacion', 'id');
        $rsm->addFieldResult('ap', 'disponible', 'cantidad');
        $rsm->addJoinedEntityResult('App\Entity\Almacen\Articulo', 'ar', 'ap', 'articulo');
        $rsm->addFieldResult('ar', 'nombre_articulo', 'nombre');
        $rsm->addJoinedEntityResult('App\Entity\Almacen\UnidadMedida', 'su', 'ap', 'subunidad_medida');
        $rsm->addFieldResult('su', 'subunidad', 'nombre');
        $query = $em->createNativeQuery("	
	            SELECT api.id as id_articulo_presentacion,ar.id ,-1*SUM(mo.cantidad) as total,
	            ar.nombre AS nombre_articulo, su.nombre as subunidad, ( SELECT SUM(e.cantidad) as disponible FROM almacen.existencia e 
                JOIN almacen.articulo_presentacion ap ON (ap.id=e.id_articulo_presentacion)
                WHERE e.id_almacen=1 AND ap.id=api.id GROUP BY ap.id ORDER BY ap.id ) as disponible
				FROM almacen.consumo co JOIN almacen.movimiento mo ON (mo.id=co.id_movimiento) 
				JOIN almacen.articulo_presentacion api ON (api.id=mo.id_articulo_presentacion)
				JOIN almacen.articulo ar ON (ar.id=api.id_articulo)
				JOIN almacen.unidad_medida su ON (su.id=api.id_subunidad_medida)
				WHERE co.id_vale=? AND mo.confirmado=0 
				GROUP BY api.id
				HAVING (-1*SUM(mo.cantidad)) > ( SELECT SUM(e.cantidad) as disponible FROM almacen.existencia e 
                JOIN almacen.articulo_presentacion ap ON (ap.id=e.id_articulo_presentacion)
                WHERE e.id_almacen=? AND ap.id=api.id GROUP BY ap.id ORDER BY ap.id )
                ORDER BY api.id", $rsm);
        $query->setParameter(1, $idVale);
        $query->setParameter(2, $idAlmacen);
        $movimientos = $query->getScalarResult();
        $msg = '';
        if (count($movimientos) > 0) {
            $msg = 'Error de disponibilidad de existencias, favor verificar:' . "\n";
            foreach ($movimientos as $movimiento) {
                $msg = $msg . $movimiento['ar_nombre'] . '(' . $movimiento['su_nombre'] . ') = ' . $movimiento['m_cantidad'] . ' Disponible=' . $movimiento['ap_cantidad'] . "\n";
            }
            $data = [
                'error' => 1,
                'message' => $msg,
            ];
        } else {
            $data = [
                'error' => 0,
                'message' => 'Informacion lista para ser actualizada',
            ];
        }
        return new JsonResponse($data);
    }


    /**
     * @Route("/movimientos/aprobar/{id_vale}/{id_almacen}", name="almacen_vale_aprobar", methods={"GET"})
     * @throws \Exception
     */
    public function aprobarVale(int $id_vale, int $id_almacen)
    {
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();
        $rsm = new ResultSetMapping();
        $rsm->addEntityResult('App\Entity\Almacen\Movimiento', 'mo');
        $rsm->addFieldResult('mo', 'id_movimiento', 'id');
        $rsm->addFieldResult('mo', 'total', 'cantidad');
        $rsm->addJoinedEntityResult('App\Entity\Almacen\ArticuloPresentacion', 'api', 'mo', 'articulo_presentacion');
        $rsm->addFieldResult('api', 'id_articulo_presentacion', 'id');
        $query = $em->createNativeQuery("	
	            SELECT mo.id as id_movimiento,api.id as id_articulo_presentacion,-1*mo.cantidad as total
				FROM almacen.consumo co JOIN almacen.movimiento mo ON (mo.id=co.id_movimiento) 
				JOIN almacen.articulo_presentacion api ON (api.id=mo.id_articulo_presentacion)
				JOIN almacen.articulo ar ON (ar.id=api.id_articulo)
				JOIN almacen.unidad_medida su ON (su.id=api.id_subunidad_medida)
				WHERE co.id_vale=? AND mo.confirmado=0 
                ORDER BY mo.id", $rsm);
        $query->setParameter(1, $id_vale);
        $movimientos = $query->getScalarResult();

        //$em->getConnection()->setAutoCommit(false);
        $em->beginTransaction();
        try {

            foreach ($movimientos as $movimiento) {
                $rsm = new ResultSetMapping();
                $rsm->addEntityResult('App\Entity\Almacen\Existencia', 'e');
                $rsm->addFieldResult('e', 'id', 'id');
                $rsm->addFieldResult('e', 'cantidad', 'cantidad');
                $rsm->addFieldResult('e', 'precio', 'precio');
                $query = $em->createNativeQuery("	SELECT e.id,e.cantidad,e.vencimiento,a.nombre,e.precio
                                            FROM almacen.existencia e
						                    JOIN almacen.articulo_presentacion ap on (e.id_articulo_presentacion=ap.id)
                                            JOIN almacen.articulo a on (ap.id_articulo=a.id)
   					                        WHERE e.id_almacen=? AND ap.id=? AND e.cantidad>0
						                    GROUP BY e.id
						                    ORDER BY e.vencimiento ASC ", $rsm);
                $query->setParameter(1, $id_almacen);
                $query->setParameter(2, $movimiento['api_id']);
                $existencias = $query->getScalarResult();
                $existenciaResp = null;
                $cp = false;
                $data = [];
                $precioFinal = 0.0;
                //if ($articulo_presentacion && $cantidad  && $usuario && $existencias) {

                $contador = 0;
                $precioAcum = 0.0;
                $exit = false;
                $cantidad = $movimiento['mo_cantidad'];
                $cantUpdate = $movimiento['mo_cantidad'];
                //$em->beginTransaction();
                try {
                    foreach ($existencias as $existencia) {
                        if ($cantidad <= $existencia["e_cantidad"] && !$cp && !$exit) {
                            $existenciaToUpdate = $em->getRepository(Existencia::class)->find($existencia["e_id"]);
                            $existenciaToUpdate->setCantidad($existencia["e_cantidad"] - $cantidad);
                            $precioFinal = floatval($existencia["e_precio"]);
                            $movimientoToUpdate = $em->getRepository(Movimiento::class)->find($movimiento["mo_id"]);
                            $movimientoToUpdate->setPrecio($precioFinal);
                            $movimientoToUpdate->setConfirmado(true);
                            $em->persist($movimientoToUpdate);
                            $em->persist($existenciaToUpdate);
                            $exit = true;
                        } elseif (!$exit) {
                            $cp = true;
                            $cantidad = $cantidad - $existencia["e_cantidad"];
                            if ($cantidad > 0) {
                                $precioAcum = $precioAcum + floatval($existencia["e_precio"]);
                                $contador++;
                                $existenciaToUpdate = $em->getRepository(Existencia::class)->find($existencia["e_id"]);
                                $existenciaToUpdate->setCantidad(0);
                                $em->persist($existenciaToUpdate);
                                $precioFinal = floatval($existencia["e_precio"]);
                            } else {
                                $precioAcum = $precioAcum + floatval($existencia["e_precio"]);
                                $contador++;
                                $existenciaToUpdate = $em->getRepository(Existencia::class)->find($existencia["e_id"]);
                                $cantExistencia = $existenciaToUpdate->getCantidad();
                                $difCantidad = $cantExistencia - (-1 * $cantidad);
                                $existenciaToUpdate->setCantidad($cantExistencia - $difCantidad);
                                $precioFinal = $precioAcum / $contador;
                                $movimientoToUpdate = $em->getRepository(Movimiento::class)->find($movimiento["mo_id"]);
                                $movimientoToUpdate->setPrecio($precioFinal);
                                $movimientoToUpdate->setConfirmado(true);
                                $em->persist($movimientoToUpdate);
                                $exit = true;
                                $em->persist($existenciaToUpdate);
                            }
                        }
                    }
                    //$em->flush();
                    //$em->commit();
                } catch (\Exception $e) {
                    $data = [
                        'error' => 1,
                        'message' => $e,
                        'exception' => $e->getMessage(),
                    ];
                    //$em->close();
                    //$this->getDoctrine()->getConnection()->rollback();
                }
            }
            $estadoVale = $em->getRepository(ValeEstado::class)->findOneBy(['codigo' => 'aprobado']);
            $valeToUpdate = $em->getRepository(Vale::class)->find($id_vale);
            $valeToUpdate->setValeEstado($estadoVale);
            $em->persist($valeToUpdate);
            $data = [
                'error' => 0,
                'message' => 'Vale aprobado',
            ];
            $em->flush();
            $em->commit();
        } catch (\Exception $e) {
            $data = [
                'error' => 1,
                'message' => $e,
                'exception' => $e->getMessage(),
            ];
            //$em->close();
            //$this->getDoctrine()->getConnection()->rollback();
        }
        return new JsonResponse($data);
    }


    /**
     * @Route("/tratamientos", name="vale_alltratamientos", methods={"GET"})
     */
    public function getAllTratamientos()
    {
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();
        $query = $em->createQuery("SELECT t FROM Clinico:Tratamiento t");
        $tratamientos = $query->getScalarResult();
        return new JsonResponse($tratamientos);
    }


    /**
     * @Route("/beforeUpdate/{id_vale}/", name="vale_before_update", methods={"GET"})
     */
    public function beforeUpdateVale(int $id_vale)
    {
        $data = false;
        try {
            /** @var EntityManager $em */
            $em = $this->getDoctrine()->getManager();
            $sql = " DELETE FROM almacen.consumo WHERE id_vale=:id_vale";
            $params = array('id_vale' => $id_vale);
            $stmt = $em->getConnection()->prepare($sql);
            $stmt->execute($params);

            $sql = " DELETE FROM almacen.vale_evaluacion_dental WHERE id_vale=:id_vale";
            $params = array('id_vale' => $id_vale);

            $stmt = $em->getConnection()->prepare($sql);
            $data = true;
        } catch (DBALException $e) {
        }
        $stmt->execute($params);
        return new JsonResponse($data);

    }

}
