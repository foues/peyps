<?php

namespace App\Controller\Almacen;

use App\Entity\Administracion\Usuario;
use App\Entity\Almacen\Almacen;
use App\Entity\Almacen\Articulo;
use App\Entity\Almacen\ArticuloPresentacion;
use App\Entity\Almacen\Existencia;
use App\Entity\Almacen\Movimiento;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Query;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ORM\Query\ResultSetMapping;


/**
 * Class ExistenciaController
 * @package App\Controller\Almacen
 * @Route("/almacen/{almacen}")
 */
class ExistenciaController extends AbstractController
{
    /**
     * ExistenciaController constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->denyAccessUnlessGranted('existencia');
    }

    /**
     * @Route("/existencia", name="almacen_existencia")
     */
    public function index(Almacen $almacen)
    {
        $em = $this->getDoctrine()->getManager();
        $existencias = $em->getRepository(Existencia::class)
            ->findBy(['almacen' => $almacen]);

        $articulos = $em->getRepository(Articulo::class)
            ->findByExistenciaInAlmacen($almacen);
        $presentaciones = $em->getRepository(ArticuloPresentacion::class)
            ->findByExistenciaInAlmacen($almacen);

        return $this->render('almacen/existencia/index.html.twig', [
            'almacen' => $almacen,
            'existencias' => $existencias,
            'articulos' => $articulos,
            'presentaciones' => $presentaciones,
        ]);
    }

    /**
     * @Route("/articulos", name="almacen_articulos", methods={"GET"})
     */
    public function articulos()
    {
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();
        $articulos = $em->getRepository(Articulo::class)->findAllScalar();
        return new JsonResponse($articulos);
    }

    /**
     * @Route("/articulo/{articulo}/presentacion", name="almacen_presentaciones", methods={"GET"})
     */
    public function presentaciones(Articulo $articulo)
    {
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();
        $presentaciones = $em->getRepository(ArticuloPresentacion::class)
            ->findByArticuloScalar($articulo);
        return new JsonResponse($presentaciones);
    }

    /**
     * @Route("/movimiento/cargar", name="almacen_cargar", methods={"POST"})
     */
    public function cargar(Request $request, Almacen $almacen)
    {
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();
        $id_articulo_presentacion = $request->request->get('presentacion');
        $adquisicion = date_create_from_format('Y-m-d', $request->request->get('adquisicion'));
        $vencimiento = date_create_from_format('Y-m-d', $request->request->get('vencimiento'));
        $cantidad = $request->request->get('cantidad');
        $precio = $request->request->get('precio');

        $articulo_presentacion = $em->getRepository(ArticuloPresentacion::class)
            ->find($id_articulo_presentacion);
        $usuario = $em->getRepository(Usuario::class)
            ->findOneByUsername($this->getUser()->getUsername());

        if ($articulo_presentacion && $adquisicion && $vencimiento && $cantidad && $precio && $usuario) {
            $em->beginTransaction();
            try {
                $existencia = (new Existencia())
                    ->setAlmacen($almacen)
                    ->setArticuloPresentacion($articulo_presentacion)
                    ->setAdquisicion($adquisicion)
                    ->setVencimiento($vencimiento)
                    ->setCantidad($cantidad)
                    ->setPrecio($precio);

                $em->persist($existencia);

                $movimiento = (new Movimiento())
                    ->setAlmacen($almacen)
                    ->setArticuloPresentacion($articulo_presentacion)
                    ->setConcepto('Cargo a existencias')
                    ->setFecha(new \DateTime())
                    ->setCantidad($cantidad)
                    ->setPrecio($precio)
                    ->setConfirmado(true)
                    ->setUsuario($usuario);

                $em->persist($movimiento);

                $em->flush();
                $em->commit();
                $data = [
                    'error' => 0,
                    'message' => 'Se cargaron existencias al almacén',
                ];
            } catch (\Exception $e) {
                $data = [
                    'error' => 1,
                    'message' => 'No se pudo cargar a existencias',
                    'exception' => $e->getMessage(),
                ];
            }
        } else {
            $data = [
                'error' => 2,
                'message' => 'Parámetros incorrectos',
            ];
        }

        return new JsonResponse($data);
    }

    /**
     * @Route("/articulosEnExistencia", name="almacen_articulos_existencia", methods={"GET"})
     */
    public function getArticulosEnExistencia(Almacen $almacen)
    {
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();
        $rsm = new ResultSetMapping();
        $rsm->addEntityResult('App\Entity\Almacen\Articulo', 'a');
        $rsm->addFieldResult('a', 'id', 'id');
        $rsm->addFieldResult('a', 'nombre', 'nombre');
        $rsm->addFieldResult('a', 'codigo', 'codigo');
        $query = $em->createNativeQuery("SELECT DISTINCT a.id,a.nombre,a.codigo
                                            FROM almacen.existencia e
                                            JOIN almacen.articulo_presentacion ap on (e.id_articulo_presentacion=ap.id)
                                            JOIN almacen.articulo a on (ap.id_articulo=a.id)
                                            WHERE e.id_almacen=? AND e.cantidad>0", $rsm);
        $query->setParameter(1, $almacen->getId());
        $articulosEnExistencia = $query->getScalarResult();
        return new JsonResponse($articulosEnExistencia);
    }

    /**
     * @Route("/totalExistencia/{articulo_presentacion}", name="total_existencia", methods={"GET"})
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getTotalExistencia(Almacen $almacen, ArticuloPresentacion $articulo_presentacion)
    {
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();
        $dql = "SELECT SUM(e.cantidad) AS total FROM Almacen:Existencia e 
            JOIN e.articulo_presentacion ap
            JOIN e.almacen a
            WHERE a.id = ?1
            AND ap.id=?2";
        $total = $em->createQuery($dql)
            ->setParameter(1, $almacen->getId())
            ->setParameter(2, $articulo_presentacion->getId())
            ->getSingleScalarResult();
        return new JsonResponse($total);
    }


    /**
     * @Route("/movimiento/descargar", name="almacen_descargar", methods={"POST"})
     * @throws \Exception
     */
    public function descargar(Request $request, Almacen $almacen)
    {
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();
        $id_articulo_presentacion = $request->request->get('presentacion_desc');
        $cantidad = $request->request->get('cantidad_desc');

        $articulo_presentacion = $em->getRepository(ArticuloPresentacion::class)->find($id_articulo_presentacion);
        $usuario = $em->getRepository(Usuario::class)->findOneByUsername($this->getUser()->getUsername());

        $rsm = new ResultSetMapping();
        $rsm->addEntityResult('App\Entity\Almacen\Existencia', 'e');
        $rsm->addFieldResult('e', 'id', 'id');
        $rsm->addFieldResult('e', 'cantidad', 'cantidad');
        $rsm->addFieldResult('e', 'precio', 'precio');
        $query = $em->createNativeQuery("	SELECT e.id,e.cantidad,e.vencimiento,a.nombre,e.precio
                                            FROM almacen.existencia e
						                    JOIN almacen.articulo_presentacion ap on (e.id_articulo_presentacion=ap.id)
                                            JOIN almacen.articulo a on (ap.id_articulo=a.id)
   					                        WHERE e.id_almacen=? AND ap.id=? AND e.cantidad>0
						                    GROUP BY e.id
						                    ORDER BY e.vencimiento ASC ", $rsm);
        $query->setParameter(1, $almacen->getId());
        $query->setParameter(2, $articulo_presentacion->getId());
        $existencias = $query->getScalarResult();
        $existenciaResp = null;
        $cp = false;
        $data = [];
        //if ($articulo_presentacion && $cantidad  && $usuario && $existencias) {
        $em->beginTransaction();
        try {
            $contador = 0;
            $precioAcum = 0.0;
            $exit = false;
            $cantUpdate = $cantidad;
            foreach ($existencias as $existencia) {
                if ($cantidad <= $existencia["e_cantidad"] && !$cp && !$exit) {
                    $existenciaToUpdate = $em->getRepository(Existencia::class)->find($existencia["e_id"]);
                    $existenciaToUpdate->setCantidad($existencia["e_cantidad"] - $cantidad);
                    $precio = floatval($existencia["e_precio"]);
                    $movimiento = (new Movimiento())
                        ->setAlmacen($almacen)
                        ->setArticuloPresentacion($articulo_presentacion)
                        ->setConcepto('Descargo a existencias')
                        ->setFecha(new \DateTime())
                        ->setCantidad(-1 * $cantUpdate)
                        ->setPrecio($precio)
                        ->setConfirmado(true)
                        ->setUsuario($usuario);
                    $em->persist($movimiento);
                    $em->persist($existenciaToUpdate);
                    $exit = true;
                    $data = [
                        'error' => 0,
                        'message' => 'Se descargaron existencias del almacén',
                    ];
                } elseif (!$exit) {
                    $cp = true;
                    $cantidad = $cantidad - $existencia["e_cantidad"];
                    if ($cantidad > 0) {
                        $precioAcum = $precioAcum + floatval($existencia["e_precio"]);
                        $contador++;
                        $existenciaToUpdate = $em->getRepository(Existencia::class)->find($existencia["e_id"]);
                        $existenciaToUpdate->setCantidad(0);
                        $em->persist($existenciaToUpdate);
                    } else {
                        $precioAcum = $precioAcum + floatval($existencia["e_precio"]);
                        $contador++;
                        $existenciaToUpdate = $em->getRepository(Existencia::class)->find($existencia["e_id"]);
                        $cantExistencia = $existenciaToUpdate->getCantidad();
                        $difCantidad = $cantExistencia - (-1 * $cantidad);
                        $existenciaToUpdate->setCantidad($cantExistencia - $difCantidad);
                        $exit = true;
                        $movimiento = (new Movimiento())
                            ->setAlmacen($almacen)
                            ->setArticuloPresentacion($articulo_presentacion)
                            ->setConcepto('Descargo de existencias')
                            ->setFecha(new \DateTime())
                            ->setCantidad(-1 * $cantUpdate)
                            ->setPrecio($precioAcum / $contador)
                            ->setConfirmado(true)
                            ->setUsuario($usuario);
                        $em->persist($movimiento);
                        $em->persist($existenciaToUpdate);
                        $data = [
                            'error' => 0,
                            'message' => 'Se descargaron existencias del almacén',
                        ];
                    }
                }
            }
            $em->flush();
            $em->commit();
        } catch (\Exception $e) {
            $data = [
                'error' => 1,
                'message' => $e,
                'exception' => $e->getMessage(),
            ];
            $em->close();
            $this->getDoctrine()->getConnection()->rollback();
        }
        return new JsonResponse($data);
    }

}
