<?php

namespace App\Controller\Almacen;

use App\Entity\Almacen\Almacen;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class AlmacenController
 * @package App\Controller\Almacen
 * @Route("/almacen")
 */
class AlmacenController extends AbstractController
{
    /**
     * AlmacenController constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->denyAccessUnlessGranted('almacen');
    }

    /**
     * @Route("/", name="almacen_index")
     */
    public function index()
    {
        $em = $this->getDoctrine()->getManager();
        $almacenes = $em->getRepository(Almacen::class)
            ->findAll();
        return $this->render('almacen/almacen/index.html.twig', [
            'almacenes' => $almacenes,
        ]);
    }

    /**
     * @Route("/{almacen}", name="almacen_show", requirements={"almacen"="\d+"})
     */
    public function show(Almacen $almacen)
    {
        return $this->render('almacen/almacen/show.html.twig', [
            'almacen' => $almacen,
        ]);
    }
}
