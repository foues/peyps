<?php

namespace App\Controller\Almacen;

use App\Entity\Almacen\Almacen;
use App\Entity\Almacen\Movimiento;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class MovimientoController
 * @package App\Controller\Almacen
 * @Route("/almacen/{almacen}/movimiento")
 */
class MovimientoController extends AbstractController
{
    /**
     * MovimientoController constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->denyAccessUnlessGranted('movimiento');
    }

    /**
     * @Route("/", name="almacen_movimiento")
     */
    public function index(Almacen $almacen)
    {
        $em = $this->getDoctrine()->getManager();
        $movimientos = $em->getRepository(Movimiento::class)
            ->findBy(['almacen' => $almacen]);
        return $this->render('almacen/movimiento/index.html.twig', [
            'almacen' => $almacen,
            'movimientos' => $movimientos,
        ]);
    }
}
