<?php

namespace App\Controller\Administracion;

use App\Entity\Administracion\Menu;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class MenuController
 * @package App\Controller\Administracion
 * @Route("/menu")
 */
class MenuController extends AbstractController
{
    /**
     * @Route("/{menu}", name="menu_submenus")
     * @param Menu $menu
     * @return Response
     */
    public function submenus(Menu $menu)
    {
        $em = $this->getDoctrine();
        $submenus = $em->getRepository('Administracion:Menu')
            ->findBy(['padre' => $menu]);
        return $this->render('administracion/menu/submenus.html.twig', [
            'menu' => $menu,
            'submenus' => $submenus,
        ]);
    }
}
