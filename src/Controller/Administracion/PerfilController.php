<?php

namespace App\Controller\Administracion;

use App\Entity\Administracion\Persona;
use App\Form\Administracion\PersonaType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class PerfilController
 * @package App\Controller\Administracion
 * @Route("/perfil")
 */
class PerfilController extends AbstractController
{

    /**
     * @Route("/", name="perfil", methods="GET")
     */
    public function show(): Response
    {
        $em = $this->getDoctrine()->getManager();
        $usuario = $this->getUser();
        $persona = $em->getRepository(Persona::class)
            ->findOneByCuenta($usuario->getCuenta());

        if (!$persona) return $this->redirectToRoute('perfil_new');

        return $this->render('administracion/perfil/show.html.twig', [
            'persona' => $persona,
        ]);
    }

    /**
     * @Route("/nuevo", name="perfil_new", methods="GET|POST")
     */
    public function new(Request $request): Response
    {
        $em = $this->getDoctrine()->getManager();
        $usuario = $this->getUser();
        $persona = $em->getRepository(Persona::class)
            ->findOneByCuenta($usuario->getCuenta());

        if ($persona) return $this->redirectToRoute('perfil_edit');

        $persona = new Persona();
        $form = $this->createForm(PersonaType::class, $persona);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $persona->setCuenta($usuario->getCuenta());
            $em->persist($persona);
            $em->flush();
            $this->addFlash('success', 'Tus cambios han sido guardados!');
            return $this->redirectToRoute('perfil');
        }

        return $this->render('administracion/perfil/new.html.twig', [
            'persona' => $persona,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/editar", name="perfil_edit", methods="GET|POST")
     */
    public function edit(Request $request): Response
    {
        $em = $this->getDoctrine()->getManager();
        $usuario = $this->getUser();
        $persona = $em->getRepository(Persona::class)
            ->findOneByCuenta($usuario->getCuenta());

        if (!$persona) return $this->redirectToRoute('perfil_new');

        $form = $this->createForm(PersonaType::class, $persona);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash('success', 'Tus cambios han sido guardados!');
            return $this->redirectToRoute('perfil', ['id' => $persona->getId()]);
        }

        return $this->render('administracion/perfil/edit.html.twig', [
            'persona' => $persona,
            'form' => $form->createView(),
        ]);
    }
}
