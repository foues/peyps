<?php

namespace App\Controller\Administracion;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class SecurityController extends AbstractController
{
    /**
     * @Route("/login", name="login")
     * @Route("/login/local", name="login_local", defaults={"local"=true})
     */
    public function login(Request $request, AuthenticationUtils $authenticationUtils, $local = false)
    {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY') ||
            $this->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            return $this->redirect($this->generateUrl('/'));
        }

        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();

        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('administracion/security/login.html.twig', [
            'last_username' => $lastUsername,
            'error' => $error,
            'local' => $local,
        ]);
    }

    /**
     * @Route("/logout", name="logout")
     */
    public function logout()
    {
        throw new \RuntimeException('Esto no debería ser llamado directamente.');
    }
}
