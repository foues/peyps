<?php

namespace App\Controller\Academico;

use App\Entity\Academico\Calificacion;
use App\Entity\Academico\Curso;
use App\Entity\Academico\CursoPrograma;
use App\Entity\Academico\Estudiante;
use App\Entity\Academico\PeriodoCiclo;
use App\Entity\Academico\PlanEstudio;
use App\Entity\Academico\PlanEstudioCurso;
use App\Entity\Academico\UnidadIntegracion;
use App\Entity\Rotacion\Programa;
use App\Service\PeriodoService;
use App\Service\UsuarioService;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * Class RegistroAcademicoController
 * @package App\Controller\Academico
 * @Route("/academico")
 */
class RegistroAcademicoController extends AbstractController
{

    /**
     * @var PeriodoService $periodo_service
     */
    private $periodo_service;

    /**
     * @var UsuarioService
     */
    private $usuario_service;

    /**
     * RegistroAcademicoController constructor.
     * @param ContainerInterface $container
     * @param PeriodoService $periodo_service
     * @param UsuarioService $usuario_service
     */
    public function __construct(ContainerInterface $container, PeriodoService $periodo_service, UsuarioService $usuario_service)
    {
        $this->container = $container;
        $this->denyAccessUnlessGranted('registro_academico');
        $this->periodo_service = $periodo_service;
        $this->usuario_service = $usuario_service;
    }

    /**
     * @Route("/registro", name="registro_academico")
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function registro()
    {
        $em = $this->getDoctrine()->getManager();
        $plan_estudio = $em->getRepository(PlanEstudio::class)
            ->findOneByCarreraCodigoAndAnio('D10701', 2009);
        $estudiante = $em->getRepository(Estudiante::class)
            ->findOneByUsuario($this->getUser());
        if (!$estudiante) {
            $this->addFlash('danger', 'No posee registro como estudiante');
            return $this->redirectToRoute('/');
        }
        $estudiante_cursos = $em->getRepository('Academico:EstudianteCurso')
            ->findByEstudianteAndPlanEstudio($estudiante, $plan_estudio);

        foreach ($estudiante_cursos as $estudiante_curso) {
            $estudiante_curso->nota = $em->getRepository('Academico:Calificacion')
                ->getNotaFinal($estudiante_curso);
        }

        return $this->render('academico/registro_academico/registro.html.twig', [
            'estudiante' => $estudiante,
            'plan_estudio' => $plan_estudio,
            'estudiante_cursos' => $estudiante_cursos,
        ]);
    }

    /**
     * @Route("/consolidado/curso/{curso}/ciclo/{ciclo}", name="consolidado")
     * @throws \Exception
     */
    public function consolidado(Curso $curso, PeriodoCiclo $ciclo)
    {
        $em = $this->getDoctrine()->getManager();
        $plan_estudio = $em->getRepository(PlanEstudio::class)
            ->findOneByCarreraCodigoAndAnio('D10701', 2009);

        $plan_estudio_curso = $em->getRepository(PlanEstudioCurso::class)
            ->findOneBy(['plan_estudio' => $plan_estudio, 'curso' => $curso]);

        if (!$plan_estudio_curso) return new Response('No existe el curso especificado');

        $unidades_integracion = $em->getRepository(UnidadIntegracion::class)
            ->findBy(['plan_estudio_curso' => $plan_estudio_curso, 'periodo_ciclo' => $ciclo]);

        $estudiantes = [];
        foreach ($unidades_integracion as $unidad_integracion) {
            $notas = $em->getRepository(Calificacion::class)
                ->getNotasFinalesByUnidadIntegracion($plan_estudio_curso, $ciclo, $unidad_integracion);
            foreach ($notas as $nota) {
                $id_estudiante_curso = $nota['id_estudiante_curso'];
                $estudiantes[$id_estudiante_curso]['carnet'] = $nota['carnet'];
                $estudiantes[$id_estudiante_curso]['nombre'] = $nota['nombre'];
                $estudiantes[$id_estudiante_curso]['notas'][$unidad_integracion->getId()] = $nota;

                if (!isset($estudiantes[$id_estudiante_curso]['final']))
                    $estudiantes[$id_estudiante_curso]['final'] = 0;

                $estudiantes[$id_estudiante_curso]['final'] += $nota['nota'];
            }
        }
        return $this->render('academico/registro_academico/consolidado.html.twig', [
            'curso' => $curso,
            'ciclo' => $ciclo,
            'plan_estudio' => $plan_estudio,
            'unidades_integracion' => $unidades_integracion,
            'estudiantes' => $estudiantes,
        ]);
    }

    /**
     * @Route("/consolidado/parametros", name="consolidado_parametros")
     */
    public function consolidado_parametros()
    {
        $ciclos = $this->periodo_service->getAllCiclos();
        return $this->render('academico/registro_academico/consolidado_parametros.html.twig', [
            'ciclos' => $ciclos,
        ]);
    }

    /**
     * @Route("/consolidado/cursos/ciclo/{ciclo}", name="consolidado_cursos")
     * @param PeriodoCiclo $ciclo
     * @param SerializerInterface $serializer
     * @return JsonResponse
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function consolidado_cursos(PeriodoCiclo $ciclo, SerializerInterface $serializer)
    {
        $em = $this->getDoctrine()->getManager();
        $plan_estudio = $em->getRepository('Academico:PlanEstudio')
            ->findOneByCarreraCodigoAndAnio('D10701', 2009);
        $cursos = $em->getRepository('Academico:CursoPrograma')
            ->getCursosConProgramasByCiclo($plan_estudio, $ciclo);

        $data = $serializer->serialize($cursos, 'json');
        return JsonResponse::fromJsonString($data);
    }

    /**
     * @Route("/calificaciones", name="calificaciones")
     * @throws \Exception
     */
    public function calificaciones()
    {
        $em = $this->getDoctrine()->getManager();
        $estudiante = $em->getRepository('Academico:Estudiante')
            ->findOneByUsuario($this->getUser());
        if (!$estudiante) {
            $this->addFlash('danger', 'No posee registro como estudiante');
            return $this->redirectToRoute('/');
        }
        $plan_estudio = $em->getRepository('Academico:PlanEstudio')
            ->findOneByCarreraCodigoAndAnio('D10701', 2009);
        $ciclos = $em->getRepository('Academico:EstudianteCurso')
            ->findCiclosByEstudianteAndPlanEstudio($estudiante, $plan_estudio);

        return $this->render('academico/registro_academico/calificaciones.html.twig', [
            'plan_estudio' => $plan_estudio,
            'ciclos' => $ciclos,
            'estudiante' => $estudiante,
        ]);
    }

    /**
     * @Route("/calificaciones/ciclo/{ciclo}/curso/{curso}/programa/{programa}", name="calificaciones_notas")
     * @param PeriodoCiclo $ciclo
     * @param Curso $curso
     * @param Programa $programa
     * @return JsonResponse
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function notas(PeriodoCiclo $ciclo, Curso $curso, Programa $programa)
    {
        /** @var \Doctrine\ORM\EntityManager $em */
        $em = $this->getDoctrine()->getManager();

        $plan_estudio = $em->getRepository('Academico:PlanEstudio')
            ->findOneByCarreraCodigoAndAnio('D10701', 2009);
        $estudiante = $em->getRepository('Academico:Estudiante')
            ->findOneByUsuario($this->getUser());

        $data = $em->getRepository('Academico:Calificacion')
            ->getCalificacionesByCursoAndProgramaAndEstudiante($plan_estudio, $ciclo, $curso, $programa, $estudiante);

        return new JsonResponse($data);
    }

    /**
     * @Route("/unidad_integracion/ciclo/{ciclo}/curso/{curso}/programa/{programa}", name="calificaciones_unidad_integracion")
     * @param PeriodoCiclo $ciclo
     * @param Curso $curso
     * @param Programa $programa
     * @param SerializerInterface $serializer
     * @return JsonResponse
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function unidadIntegracion(PeriodoCiclo $ciclo, Curso $curso, Programa $programa, SerializerInterface $serializer)
    {
        $em = $this->getDoctrine()->getManager();
        $plan_estudio = $em->getRepository('Academico:PlanEstudio')
            ->findOneByCarreraCodigoAndAnio('D10701', 2009);
        $plan_estudio_curso = $em->getRepository('Academico:PlanEstudioCurso')
            ->findOneBy(['plan_estudio' => $plan_estudio, 'curso' => $curso]);
        $estudiante = $em->getRepository('Academico:Estudiante')->findOneByUsuario($this->getUser());
        $estudiante_plan_estudio = $em->getRepository('Academico:EstudiantePlanEstudio')
            ->findOneBy([
                'estudiante' => $estudiante,
                'plan_estudio' => $plan_estudio,
            ]);
        $estudiante_curso = $em->getRepository('Academico:EstudianteCurso')
            ->findOneBy([
                'estudiante_plan_estudio' => $estudiante_plan_estudio,
                'curso' => $curso,
                'periodo_ciclo' => $ciclo,
            ]);
        $unidad_integracion = $em->getRepository('Academico:UnidadIntegracion')
            ->findOneBy([
                'plan_estudio_curso' => $plan_estudio_curso,
                'periodo_ciclo' => $ciclo,
                'programa' => $programa,
            ]);

        $data = [];

        $data['unidad_integracion'] = $unidad_integracion;
        $data['nota_parcial'] = $em->getRepository('Academico:Calificacion')
            ->getNotaParcial($estudiante_curso, $unidad_integracion);

        $data = $serializer->serialize($data, 'json');
        return JsonResponse::fromJsonString($data);
    }

    /**
     * @Route("/mis_cursos/ciclo/{ciclo}", name="mis_cursos_ciclo")
     * @param PeriodoCiclo $ciclo
     * @param SerializerInterface $serializer
     * @return JsonResponse
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function misCursosPorCiclo(PeriodoCiclo $ciclo, SerializerInterface $serializer)
    {
        $em = $this->getDoctrine()->getManager();
        $plan_estudio = $em->getRepository('Academico:PlanEstudio')
            ->findOneByCarreraCodigoAndAnio('D10701', 2009);
        $estudiante = $em->getRepository('Academico:Estudiante')
            ->findOneByUsuario($this->getUser());
        $cursos = $em->getRepository('Academico:EstudianteCurso')
            ->findCursosByEstudianteAndPlanEstudioAndCiclo($estudiante, $plan_estudio, $ciclo);

        $data = $serializer->serialize($cursos, 'json');
        return JsonResponse::fromJsonString($data);
    }

    /**
     * @Route("/mis_cursos/{curso}/programas", name="mis_cursos_programas")
     * @param Curso $curso
     * @param SerializerInterface $serializer
     * @return JsonResponse
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getProgramas(Curso $curso, SerializerInterface $serializer)
    {
        $em = $this->getDoctrine()->getManager();
        $plan_estudio = $em->getRepository('Academico:PlanEstudio')
            ->findOneByCarreraCodigoAndAnio('D10701', 2009);
        $programas = $em->getRepository(CursoPrograma::class)
            ->getProgramasByPlanEstudioAndCurso($plan_estudio, $curso);

        $data = $serializer->serialize($programas, 'json');
        return JsonResponse::fromJsonString($data);
    }
}
