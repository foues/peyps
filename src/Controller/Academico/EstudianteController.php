<?php

namespace App\Controller\Academico;

use App\Entity\Academico\Carrera;
use App\Entity\Academico\Curso;
use App\Entity\Academico\CursoPrograma;
use App\Entity\Academico\Estudiante;
use App\Entity\Academico\EstudianteCurso;
use App\Entity\Academico\EstudianteEstado;
use App\Entity\Academico\EstudiantePlanEstudio;
use App\Entity\Academico\PeriodoCiclo;
use App\Entity\Academico\PlanEstudio;
use App\Entity\Administracion\Cuenta;
use App\Entity\Administracion\Persona;
use App\Service\EstudianteService;
use App\Service\PeriodoService;
use App\Service\RotacionService;
use App\Service\UsuarioService;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints\File;

/**
 * Class EstudianteController
 * @package App\Controller\Academico
 */
class EstudianteController extends AbstractController
{

    /**
     * @var EstudianteService
     */
    private $estudiante_service;

    /**
     * @var RotacionService
     */
    private $rotacion_service;

    /**
     * @var PeriodoService
     */
    private $periodo_service;

    /**
     * @var UsuarioService
     */
    private $usuario_service;

    /**
     * EstudianteController constructor.
     * @param ContainerInterface $container
     * @param EstudianteService $estudiante_service
     * @param RotacionService $rotacion_service
     * @param PeriodoService $periodoService
     */
    public function __construct(
        ContainerInterface $container,
        EstudianteService $estudiante_service,
        RotacionService $rotacion_service,
        PeriodoService $periodoService,
        UsuarioService $usuario_service
    )
    {
        $this->container = $container;
        $this->denyAccessUnlessGranted('nomina');
        $this->estudiante_service = $estudiante_service;
        $this->rotacion_service = $rotacion_service;
        $this->periodo_service = $periodoService;
        $this->usuario_service = $usuario_service;
    }

    /**
     * @Route("/estudiantes", name="estudiantes_list", methods="GET|POST")
     */
    public function estudiantes(Request $request)
    {
        if ($request->isMethod("POST")) {
            $em = $this->getDoctrine()->getManager();
            $estado = $this->estudiante_service->getEstadoOne($request->get('id_estado'));
            $plan_estudio = $this->estudiante_service->getPlanEstudioOne($request->get('id_plan_estudio'));
            $est = $this->estudiante_service->getEstudiantesPlanEstudioAll($estado, $plan_estudio);
            $estado = $this->estudiante_service->getEstadosPlan();
            $cursos = $em->getRepository(CursoPrograma::class)
                ->getPlanEstudioCursosConProgramas($plan_estudio);
            return $this->render('academico/estudiante/estudiante.html.twig', [
                'estudiantesPlan' => $est,
                'estados' => $estado,
                'cursos' => $cursos,
            ]);
        } else
            return $this->redirectToRoute('estudiantes_parametros');
    }

    /**
     * @Route("/estudiantes/parametros", name="estudiantes_parametros")
     */
    public function estudiantes_parametros()
    {
        $estados_plan = $this->estudiante_service->getEstadosPlan();
        //dd($estudiantes);
        $plan_estudio = $this->estudiante_service->getPlanEstudioAll();

        return $this->render('academico/estudiante/parametros.html.twig', [
            'estados' => $estados_plan,
            'planes_estudios' => $plan_estudio,
        ]);
    }

    /**
     * @Route("/estudiante/create", name="create_estudiante", methods="GET|POST")
     */
    public function estudiante_create(Request $request)
    {

        $em = $this->getDoctrine()->getManager();
        $perido_actual = $this->periodo_service->getCicloActual();//Cambiar por servicio de Nelson
        $estudiante_dui = $em->getRepository(Persona::class)
            ->findBy(array('dui' => $request->get('dui_est')));
        $estudiante_carnet = $em->getRepository(Estudiante::class)
            ->findBy(array('carnet' => $request->get('carnet')));
        if ($estudiante_carnet) {
            $this->addFlash('danger', 'Ya existe un estudiante con este Carnet!!');
            return $this->redirect($request->server->get('HTTP_REFERER'));
        }
        if ($estudiante_dui) {
            $this->addFlash('danger ', 'Ya existe una persona con este DUI!!');
            return $this->redirect($request->server->get('HTTP_REFERER'));
        }
        if ($request->isMethod("POST")) {
            $cuenta = new Cuenta();
            $em->persist($cuenta);
            $em->flush();

            $persona = new Persona();
            $persona->setNombre($request->get('nombre_est'));
            $persona->setApellido($request->get('apellido_est'));
            $persona->setDui($request->get('dui_est'));
            $persona->setNit($request->get('nit_est'));
            $persona->setNacimiento(new \DateTime($request->get('nacimiento_est')));
            $persona->setCuenta($em->getRepository(Cuenta::class)->find($cuenta->getId()));
            $em->persist($persona);
            $em->flush();

            $estudiante = new Estudiante();
            $estudiante->setCarnet($request->get('carnet'));
            $estudiante->setPersona($em->getRepository(Persona::class)->find($persona->getId()));
            $em->persist($estudiante);
            $em->flush();

            $estudiante_plan_estudio = new EstudiantePlanEstudio();
            $plan_estudio = $em->getRepository(PlanEstudio::class)->findOneBy(['activo' => true]);
//            dd($plan_estudio);
            $estudiante_plan_estudio->setEstudiante($em->getRepository(Estudiante::class)->find($estudiante->getId()));
            $estudiante_plan_estudio->setPlanEstudio($plan_estudio);
            $estudiante_plan_estudio->setEstudianteEstado($em->getRepository(EstudianteEstado::class)->find($request->get('estado')));
            if ($request->get('cum') == "") {
                $estudiante_plan_estudio->setCum(0);
            } else {
                $estudiante_plan_estudio->setCum($request->get('cum'));
            }
            $em->persist($estudiante_plan_estudio);
            $em->flush();

            $estudiante_curso = new EstudianteCurso();
            $estudiante_curso->setPeriodoCiclo($perido_actual);
            $estudiante_curso->setEstudiantePlanEstudio($estudiante_plan_estudio);
            $estudiante_curso->setEstudianteEstado($em->getRepository(EstudianteEstado::class)->findOneBy(['codigo' => 'estudiante']));
            $estudiante_curso->setCurso($em->getRepository(Curso::class)->find($request->get('curso')));
            $em->persist($estudiante_curso);
            $em->flush();

            $this->addFlash('success', 'Estudiante Agregado Exitosamente!!');
            return $this->redirectToRoute('estudiantes_parametros');
        }

    }

    /**
     * @Route("/estudiante/nomina", name="estudiante_nomina", methods="GET")
     */
    public function estudiantes_nomina(): Response
    {
        $em = $this->getDoctrine()->getManager();
        $carrera = $em->getRepository(Carrera::class)
            ->findOneBy(['codigo' => 'D10701']);
        $plan_estudio = $em->getRepository(PlanEstudio::class)
            ->findOneBy(['carrera' => $carrera, 'anio' => 2009]);
        $cursos = $em->getRepository(CursoPrograma::class)->getCursosConProgramas($plan_estudio);
        return $this->render('academico/estudiante/nomina.html.twig', [
            'cursos' => $cursos,
        ]);
    }

    /**
     * @Route("/estudiante/gestion", name="estudiante_gestion", methods="GET|POST")
     */
    public function estudiantes_gestion(Request $request)
    {
        if ($request->isMethod("POST")) {
            $estudiante_curso = $this->estudiante_service->getEstudiantesCursosAll($request->get('id_curso'), $request->get('periodo_ciclo'));
            $periodo = $this->getDoctrine()->getRepository("Academico:PeriodoCiclo")->find($request->get('periodo_ciclo'));
            $curso = $this->estudiante_service->getCurso($request->get('id_curso'));
            //dump($this->periodo_service->getCicloActual());
            //dump($periodo->getPeriodo()->getNombre());
            //die();
            $estados = $this->estudiante_service->getEstadosCurso();
            //dd($estados);

            return $this->render('academico/estudiante/gestion.html.twig', [
                'estudiantes_periodos' => $estudiante_curso,
                'curso' => $curso,
                'estados' => $estados,
                'periodo' => $periodo
            ]);
        } else
            return $this->redirectToRoute("estudiante_nomina");
    }

    /**
     * @Route("/estudiante/estado/{id}/{periodo}", name="estudiante_estado",defaults={"id"=0,"periodo"=0},methods="GET|POST")
     */
    public function estudiantes_estado(Request $request, $id, $periodo)
    {
        $em = $this->getDoctrine()->getManager();

        //Para el boton de aprobar a todos
        if ($id != 0) {

            $estudiantes_curso = $this->estudiante_service->getEstudiantesCurso($id, $em->getRepository("Academico:PeriodoCiclo")->find($periodo));

            foreach ($estudiantes_curso as $estudiante) {
                $estudiante->setEstudianteEstado(
                    $em->getRepository(EstudianteEstado::class)->findOneBy(array('codigo' => 'aprobado'))
                );
                $em->flush();
            }
            //verifico si habian alumnos con estado estudiante en el curso
            if (count($estudiantes_curso) > 0) {
                $estados = $this->estudiante_service->getEstadosCurso();


                return $this->render('academico/estudiante/gestion.html.twig', [
                    'estudiantes_periodos' => $em->getRepository("Academico:EstudianteCurso")
                        ->findBy(array('curso' => $estudiantes_curso[0]->getCurso(), 'periodo_ciclo' => $estudiantes_curso[0]->getPeriodoCiclo())), //$this->estudiante_service->getEstudianteCurso($estudiantes_curso[0]->getCurso(),$this->rotacion_service->obtenerPeriodoCiclo()),
                    'curso' => $estudiantes_curso[0]->getCurso(),
                    'estados' => $estados,
                    'periodo' => $estudiantes_curso[0]->getPeriodoCiclo()
                ]);
            }
        }

        // Para poner estado individualmente
        if ($request->isMethod("POST")) {
            $estudiante_curso = $this->estudiante_service->getEstudianteCursoOne($request->get('id_estudiante_curso'));
            $estudiante_curso->setEstudianteEstado($this->estudiante_service->getEstadoOne($request->get("id_estado")));
            $em->flush();


            //para renderizacion  de twig
            $curso = $estudiante_curso->getCurso();
            $estudiantes_periodo = $em->getRepository("Academico:EstudianteCurso")
                ->findBy(array('curso' => $curso, 'periodo_ciclo' => $estudiante_curso->getPeriodoCiclo()));//$this->estudiante_service->getEstudianteCurso($curso, $estudiante_curso->getPeriodoCiclo());
            $estados = $this->estudiante_service->getEstadosCurso();

            //dump($estudiante_curso->getPeriodoCiclo());
            //die();
            return $this->render('academico/estudiante/gestion.html.twig', [
                'estudiantes_periodos' => $estudiantes_periodo,
                'curso' => $curso,
                'estados' => $estados,
                'periodo' => $estudiante_curso->getPeriodoCiclo()
            ]);

        }

        return $this->redirectToRoute("estudiante_nomina");

    }

    /**
     * @Route("/estudiante/edit/{id}",defaults={"id"=1},name="estudiante_edit", methods="GET|POST")
     */
    public function estudiante_edit($id, Request $request)
    {

        $estudiante = $this->estudiante_service->getEstudiantesPlanEstudioOne($id);
        $estado = $this->estudiante_service->getEstadosPlan();
        //$cursos = $this->estudiante_service->getCursos();

        if ($request->isMethod("POST")) {
            //dd(strlen($request->get("dui_est")));
            $dui = $request->get("dui_est");
            $dui = strlen($dui) ? $dui : null;
            $nit = $request->get("nit_est");
            $nit = strlen($nit) ? $nit : null;
            //dd( $request->get('estado'));
            $this->estudiante_service->setPersonaOne(
                $estudiante->getEstudiante()->getPersona(),
                $request->get("nombre_est"),
                $request->get("apellido_est"),
                $dui,
                $nit,
                $request->get("nacimiento_est")
            );
            $this->estudiante_service->setEstudiantePlanEstudioOne(
                $estudiante->getId(),
                $request->get('cum'),
                $request->get('estado')
            );
            //$this->estudiante_service->setEstudianteCursoOne($id,$request->get('curso'));

            $this->addFlash('success', 'Cambio Realizado Exitosamente!');
            return $this->redirectToRoute('estudiantes_parametros');
        }

        return $this->render('academico/estudiante/estudiante_edit.html.twig', [
            'estudiante' => $estudiante,
            'estados' => $estado
            //'cursos' => $cursos
        ]);
    }

    /**
     * @Route("/estudiante/inscripcion", name="estudiante_inscripcion", methods="GET|POST")
     */
    public function estudiantes_inscripcion(Request $request)
    {
        $estudiantes_cursos = $this->estudiante_service->getEstudiantesCursosAll($request->get('id_curso'), $request->get('periodo_ciclo'));
        $estudiante_curso = $this->estudiante_service->getEstudianteCurso($request->get('id_curso'), $request->get('periodo_ciclo'));
        //dd($estudiante_curso);
        $curso = $this->estudiante_service->getCurso($request->get('id_curso'));

        $est = $this->getDoctrine()->getManager();
        if ($estudiante_curso != null) {
            $siguiente_curso = $est->getRepository('Academico:CursoPrograma')->getSiguienteCursoConPrograma($estudiante_curso);
        } else {
            $siguiente_curso = null;
        }//dd($siguiente_curso);
        //dd($estados);
        $periodos = $this->estudiante_service->getPeriodoCiclo();

        return $this->render('academico/estudiante/inscripcion.html.twig', [
            'estudiantes_periodos' => $estudiantes_cursos,
            'curso' => $curso,
            'estudiante_curso' => $estudiante_curso,
            'siguiente_curso' => $siguiente_curso,
            'periodos' => $periodos,
        ]);
    }

    /**
     * @Route("/estudiante/inscribir/curso", name="estudiantes_inscribir",methods="GET|POST")
     */
    public function inscribir_esudiantes(Request $request)
    {
        $estudiantes_cursos = $this->estudiante_service->getEstudiantesCursosAll($request->get('id_curso_actual'), $request->get('id_periodo_actual'));
        $estudiante_curso = $this->estudiante_service->getEstudianteCurso($request->get('id_curso_actual'), $request->get('id_periodo_actual'));
        $curso = $this->estudiante_service->getCurso($request->get('id_curso_actual'));
        $est = $this->getDoctrine()->getManager();
        if ($estudiante_curso != null) {
            $siguiente_curso = $est->getRepository('Academico:CursoPrograma')->getSiguienteCursoConPrograma($estudiante_curso);
        } else {
            $siguiente_curso = null;
        }
        $em = $this->getDoctrine()->getManager();
        $periodos = $this->estudiante_service->getPeriodoCiclo();

        if ($request->isMethod("POST")) {
            //$curso_actual= $this->estudiante_service->getCurso($request->get('id_curso_actual'));
            $periodo_actual = $this->estudiante_service->getPeriodoCicloOne($request->get('id_periodo_actual'));
            $curso_siguiente = $this->estudiante_service->getCurso($request->get('id_siguiente_curso'));
            /** @var PeriodoCiclo $periodo_siguiente */
            $periodo_siguiente = $this->periodo_service->getCicloSiguiente($periodo_actual);
            $estudiantes_cursos = $this->estudiante_service->getEstudiantesCursosAll($request->get('id_curso_actual'), $request->get('id_periodo_actual'));

            //dd($estudiantes_cursos);
            /** @var EstudianteCurso $estudiantes_curso */
            foreach ($estudiantes_cursos as $estudiantes_curso) {

                if ($estudiantes_curso->getEstudianteEstado()->getCodigo() == 'aprobado' || $estudiantes_curso->getEstudianteEstado()->getCodigo() == 'resolucion') {

                    if ($estudiantes_curso->getPeriodoCiclo()->getPeriodo()->getNombre() == $periodo_actual->getPeriodo()->getNombre()) {
                        if ($periodo_siguiente) {
                            $estudiante_plan = $this->estudiante_service->getEstudiantesPlanEstudioOne($estudiantes_curso->getId());
                            $est_curso_next_aprob = $this->estudiante_service->getEstudianteCursoAprobado($curso_siguiente, $periodo_siguiente, $estudiante_plan);
                            if ($est_curso_next_aprob) {
                                //El estudiante ya se encuentra aprobado en el curso y periodo siguiente
                            } else {
                                $est_curso_next_resol = $this->estudiante_service->getEstudianteCursoResolucion($curso_siguiente, $periodo_siguiente, $estudiante_plan);
                                if ($est_curso_next_resol) {
                                    //El estudiante se encuentra en resolución en el curso y periodo siguiente
                                } else {
                                    $est_curso_next_est = $this->estudiante_service->getEstudianteCursoEstudiante($curso_siguiente, $periodo_siguiente, $estudiante_plan);
                                    if ($est_curso_next_est) {
                                        //El esudiante de encuentra en activo en el curso y periodo actual
                                    } else {
                                        $estudiante_curso = new EstudianteCurso();
                                        $estudiante_curso->setPeriodoCiclo($periodo_siguiente);
                                        $estudiante_plan = $this->estudiante_service->getEstudiantesPlanEstudioOne($estudiantes_curso->getEstudiantePlanEstudio()->getId());
                                        $estudiante_curso->setEstudiantePlanEstudio($estudiante_plan);
                                        $estudiante_curso->setEstudianteEstado($em->getRepository(EstudianteEstado::class)->findOneBy(['codigo' => 'estudiante']));
                                        $estudiante_curso->setCurso($curso_siguiente);
                                        $em->persist($estudiante_curso);
                                        $em->flush();
                                    }
                                }
                            }
                        } else {
                            $this->addFlash('danger', 'No existe un periodo válido para inscribir!!');
                            $cursos = $this->estudiante_service->getCursos();
                            $periodo_ciclo = $this->estudiante_service->getPeriodoCiclo();
                            return $this->render('academico/estudiante/nomina.html.twig', [
                                'cursos' => $cursos,
                                'periodos' => $periodo_ciclo,
                            ]);
                        }
                    } else {
                        //Validando que no se encuentra en el periodo actual
                        dd("Validando que no se encuentra en el periodo actual");
                    }

                }

            }
            $this->addFlash('success', 'Estudiantes Inscritos Exitosamente!!');
            return $this->redirectToRoute('estudiante_nomina');

        }

        return $this->render('academico/estudiante/inscripcion.html.twig', [
            'estudiantes_periodos' => $estudiantes_cursos,
            'curso' => $curso,
            'estudiante_curso' => $estudiante_curso,
            'siguiente_curso' => $siguiente_curso,
            'periodos' => $periodos,
        ]);

    }

    /**
     * @Route("estudiantes/cargar", name="estudiantes_cargar")
     * @param Request $request
     * @param PeriodoService $periodo_service
     * @return Response
     * @throws \Exception
     */
    public function cargar(Request $request, PeriodoService $periodo_service)
    {
        $ciclo = $periodo_service->getCicloActual();
        $ciclos = $periodo_service->getAllCiclos();
        $form = $this->createFormBuilder()
            ->add('ciclo', EntityType::class, [
                'class' => PeriodoCiclo::class,
                'choices' => $ciclos,
                'data' => $ciclo,
            ])
            ->add('nomina', FileType::class, [
                'label' => 'Nómina',
                'attr' => ['placeholder' => 'Seleccionar archivo', 'accept' => '.csv'],
                'constraints' => [new File(['mimeTypes' => ['text/csv', 'text/plain']])]
            ])
            ->getForm();

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            $data = $form->getData();
            /** @var UploadedFile $nomina */
            $nomina = $data['nomina'];
            $csv = fopen($nomina->getRealPath(), 'r');

            $first_line = fgetcsv($csv);
            if ($this->validarArchivo($first_line)) {
                $em = $this->getDoctrine()->getManager();

                $em->beginTransaction();
                try {
                    $lines = [];
                    while (!feof($csv)) {
                        $line = fgetcsv($csv);
                        if (isset($line[0])) $lines[$line[0]] = $line;
                    }

                    $carrera = $em->getRepository(Carrera::class)
                        ->findOneBy(['codigo' => 'D10701']);
                    $plan_estudio = $em->getRepository(PlanEstudio::class)
                        ->findOneBy(['carrera' => $carrera, 'anio' => 2009]);
                    $estudiante_estado = $em->getRepository(EstudianteEstado::class)
                        ->findOneBy(['tipo' => 'plan_estudio', 'codigo' => 'activo']);

                    $carnets = array_keys($lines);
                    $estudiantes = $em->getRepository(Estudiante::class)
                        ->findByCarnets($carnets);

                    $usuarios = [];
                    $cantidad = 0;
                    $diff = array_diff($carnets, array_keys($estudiantes));
                    foreach ($diff as $carnet) {
                        $line = $lines[$carnet];
                        $usuario = $this->usuario_service->getUserByUsername($carnet);
                        $estudiante = $this->estudiante_service->createEstudiante($carnet, $line[1], $line[2], $usuario->getCuenta());

                        $cantidad++;
                        $usuarios[] = $usuario;
                        $estudiantes[$carnet] = $estudiante;
                    }
                    $em->flush();

                    foreach ($usuarios as $usuario) {
                        $this->usuario_service->setUserRoles($usuario, ['ROLE_USER', 'ROLE_ESTUDIANTE']);
                    }
                    $em->flush();

                    $_estudiantes_plan_estudio = $em->getRepository(EstudiantePlanEstudio::class)
                        ->findByCarnetsAndPlanEstudio($carnets, $plan_estudio);
                    $estudiantes_plan_estudio = [];
                    foreach ($_estudiantes_plan_estudio as $estudiante_plan_estudio) {
                        $carnet = $estudiante_plan_estudio->getEstudiante()->getCarnet();
                        $estudiantes_plan_estudio[$carnet] = $estudiante_plan_estudio;
                    }

                    $diff = array_diff($carnets, array_keys($estudiantes_plan_estudio));
                    foreach ($diff as $carnet) {
                        $estudiante = $estudiantes[$carnet];
                        $estudiante_plan_estudio = (new EstudiantePlanEstudio())
                            ->setEstudiante($estudiante)
                            ->setPlanEstudio($plan_estudio)
                            ->setCum($lines[$carnet][3])
                            ->setEstudianteEstado($estudiante_estado);
                        $em->persist($estudiante_plan_estudio);

                        $estudiantes_plan_estudio[$carnet] = $estudiante_plan_estudio;
                    }
                    $em->flush();

                    $inscritos = 0;
                    $cursos = [];
                    foreach ($lines as $line) {
                        $curso = null;
                        if (!empty($line[4])) {
                            $curso = isset($cursos[$line[4]]) ? $cursos[$line[4]] :
                                $em->getRepository(Curso::class)->findOneBy(['codigo' => $line[4]]);
                        }

                        if ($curso) {
                            $cursos[$line[4]] = $curso;
                            $carnet = $line[0];
                            $estudiante_plan_estudio = $estudiantes_plan_estudio[$carnet];
                            $estudiante_curso = $em->getRepository(EstudianteCurso::class)
                                ->findOneByEstudiantePlanEstudioAndCurso($estudiante_plan_estudio, $curso, $ciclo);
                            if (!$estudiante_curso) {
                                $estudiante_curso = (new EstudianteCurso())
                                    ->setEstudiantePlanEstudio($estudiante_plan_estudio)
                                    ->setCurso($curso)
                                    ->setPeriodoCiclo($ciclo)
                                    ->setEstudianteEstado($estudiante_estado);
                                $em->persist($estudiante_curso);
                                $inscritos++;
                            }
                        }
                    }
                    $em->flush();

                    $em->commit();

                    $this->addFlash(
                        'success',
                        "$cantidad nuevos estudiantes registrados. $inscritos estudiantes inscritos."
                    );
                } catch (\Exception $e) {
                    $this->addFlash(
                        'danger',
                        'Ocurrió un error'
                    );
                    $em->rollback();
                }

            } else {
                $this->addFlash(
                    'danger',
                    'Formato de archivo inválido'
                );
            }

            fclose($csv);
        }

        return $this->render('academico/estudiante/cargar.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @param array $first_line
     * @return bool
     */
    private function validarArchivo(array $first_line)
    {
        if (empty($first_line[0]) || $first_line[0] != 'CARNET')
            return false;
        if (empty($first_line[1]) || $first_line[1] != 'NOMBRE')
            return false;
        if (empty($first_line[2]) || $first_line[2] != 'APELLIDO')
            return false;
        if (empty($first_line[3]) || $first_line[3] != 'CUM')
            return false;
        if (empty($first_line[4]) || $first_line[4] != 'CURSO')
            return false;

        return true;
    }
}
