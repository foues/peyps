<?php

namespace App\Controller\Academico;

use App\Entity\Academico\Carrera;
use App\Entity\Academico\CursoPrograma;
use App\Entity\Academico\Evaluacion;
use App\Entity\Academico\PeriodoCiclo;
use App\Entity\Academico\PlanEstudio;
use App\Entity\Academico\PlanEstudioCurso;
use App\Entity\Academico\UnidadIntegracion;
use App\Entity\Rotacion\Programa;
use App\Form\Academico\EvaluacionType;
use App\Form\Academico\UnidadIntegracionType;
use App\Service\PeriodoService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

// Include JSON Response
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * @Route("/academico")
 */
class ConfiguracionEstructuraController extends AbstractController
{

    /**
     * @var PeriodoService $periodo_service
     */
    private $periodo_service;

    /**
     * ConfiguracionEstructuraController constructor.
     * @param ContainerInterface $container
     * @param PeriodoService $periodo_service
     */
    public function __construct(ContainerInterface $container, PeriodoService $periodo_service)
    {
        $this->container = $container;
        $this->denyAccessUnlessGranted('evaluacion');
        $this->periodo_service = $periodo_service;
    }

    /**
     * @Route("/", name="academico_index")
     */
    public function index()
    {
        $em = $this->getDoctrine()->getManager();
        $carrera = $em->getRepository(Carrera::class)->findOneBy(['codigo' => 'D10701']);
        $plan_estudio = $em->getRepository(PlanEstudio::class)->findOneBy([
            'carrera' => $carrera,
            'anio' => '2009',
        ]);

        $cursos = $em->getRepository(CursoPrograma::class)
            ->getPlanEstudioCursosConProgramas($plan_estudio);

        return $this->render('academico/index.html.twig', [
            'plan_estudio' => $plan_estudio,
            'cursos' => $cursos,
        ]);
    }

    /**
     * @Route("/programas/{plan_estudio_curso}",name="programasByCurso")
     */
    public function getProgramas(PlanEstudioCurso $plan_estudio_curso)
    {
        $em = $this->getDoctrine()->getManager();
        $programas = $em->getRepository(CursoPrograma::class)
            ->getProgramasByPlanEstudioCurso($plan_estudio_curso);
        $responseArray = array();
        /** @var Programa $programa */
        foreach ($programas as $programa) {
            $responseArray[] = array(
                'id' => $programa->getId(),
                'name' => $programa->getNombre()
            );
        }

        // Return array with structure of the neighborhoods of the providen city id
        return new JsonResponse($responseArray);
    }

    /**
     * @Route("/ciclos/{plan_estudio_curso}",name="ciclosByCurso")
     */
    public function getCiclos(PlanEstudioCurso $plan_estudio_curso)
    {
        $em = $this->getDoctrine()->getManager();
        $ciclos = $em->getRepository(PlanEstudioCurso::class)
            ->findPeriodoCiclosByPlanEstudioCurso($plan_estudio_curso);
        $responseArray = array();
        /** @var PeriodoCiclo $ciclo */
        foreach ($ciclos as $ciclo) {
            $responseArray[] = array(
                'id' => $ciclo->getId(),
                'name' => $ciclo->getPeriodo()->getNombre()
            );
        }
        // Return array with structure of the neighborhoods of the providen city id
        return new JsonResponse($responseArray);
    }

    /**
     * @Route("/unidadIntegracion",name="unidadIntegracion")
     */
    public function getUnidadIntegracion(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $programa = $request->query->get('programa');
        $planEstudioCurso = $request->query->get('planEstudioCurso');
        $periodoCiclo = $request->query->get('periodoCiclo');

        $unidadIngregacion = $em->getRepository(UnidadIntegracion::class)
            ->findOneBy(array('programa' => $programa, 'plan_estudio_curso' => $planEstudioCurso, 'periodo_ciclo' => $periodoCiclo));

        $responseArray = array();
        if ($unidadIngregacion != null) {
            $responseArray[] = array(
                "id" => "unidad",
                "value" => array(
                    "idUnidadIntegracion" => $unidadIngregacion->getId(),
                ),
                "label" => $unidadIngregacion->getNombre(),
                "ponderacion" => $unidadIngregacion->getPonderacion(),
                "expanded" => true,
                "items" => $this->getEvaluacionPorUnidadIntegracion($unidadIngregacion->getId())
            );
        }

        return new JsonResponse($responseArray);
    }

    /**
     * @Route("/evaluaciones",name="evaluaciones")
     */
    public function getEvaluacion(Request $request)
    {
        $parent = $request->query->get("idPadre");
        $unidadIntegracion = $request->query->get("idUnidadIntegracion");

        if ($parent == '') {
            $parent = null;
        }

        $data = $this->getEvaluaciones($parent, $unidadIntegracion);
        return new JsonResponse($data);
    }

    public function getEvaluacionPorUnidadIntegracion($unidadIntegracion)
    {
        $em = $this->getDoctrine()->getManager();
        $evaluaciones = $em->getRepository(Evaluacion::class)
            ->findBy(
                array('unidad_integracion' => $unidadIntegracion, 'padre' => null));
        $responseArray = array();
        foreach ($evaluaciones as $evaluacion) {
            $responseArray[] = array(
                "id" => $evaluacion->getId(),
                "value" => array(
                    "idUnidadIntegracion" => $evaluacion->getUnidadIntegracion()->getId(),
                    "tipoEvaluacion" => $evaluacion->getEvaluacionTipo()->getCodigo()
                ),
                "label" => $evaluacion->getNombre(),
                "ponderacion" => $evaluacion->getPonderacion(),
                "expanded" => false,
                "tipo" => $evaluacion->getEvaluacionTipo()->getNombre(),
                "items" => $this->getEvaluaciones($evaluacion->getId(), $unidadIntegracion)
            );
        }

        return $responseArray;
    }

    /**
     * @param $idPadre
     * @param $idUnidadIntegracion
     * @return array
     */
    private function getEvaluaciones($idPadre, $idUnidadIntegracion)
    {
        $em = $this->getDoctrine()->getManager();
        $evaluaciones = $em->getRepository(Evaluacion::class)
            ->findByIdUnidadIntegracionAndIdPadre($idUnidadIntegracion, $idPadre);
        $responseArray = array();
        foreach ($evaluaciones as $evaluacion) {
            $responseArray[] = array(
                "id" => $evaluacion->getId(),
                "value" => [
                    "idUnidadIntegracion" => $evaluacion->getUnidadIntegracion()->getId(),
                    "tipoEvaluacion" => $evaluacion->getEvaluacionTipo()->getCodigo(),
                ],
                "label" => $evaluacion->getNombre(),
                "ponderacion" => $evaluacion->getPonderacion(),
                "expanded" => false,
                "tipo" => $evaluacion->getEvaluacionTipo()->getNombre(),
                "items" => $this->getEvaluaciones($evaluacion->getId(), $idUnidadIntegracion)
            );
        }

        return $responseArray;
    }

    /**
     * @Route("/add",name="add_evaluacion")
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function add(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $evaluacion = new Evaluacion();
        $idEvaluacionPadre = $request->query->get('idEvaluacionPadre');
        $unidadIntegracion = $request->query->get("idUnidadIntegracion");

        $form = $this->createForm(EvaluacionType::class, $evaluacion, array(
            'action' => $this->generateUrl('add_evaluacion') . "?idEvaluacionPadre=" . $idEvaluacionPadre . "&idUnidadIntegracion=" . $unidadIntegracion . "&submited=1",
        ));
        $form->handleRequest($request);
        if ($request->isXmlHttpRequest() && $request->query->get("submited")) {
            if (!$form->isSubmitted()) {
                $form->submit($request->request->get($form->getName()));
            }

            $response = new JsonResponse();
            $response->setStatusCode(200);


            if ($form->isValid()) {
                if ($idEvaluacionPadre != '') {
                    $evaluacionPadre = $em->getRepository(Evaluacion::class)->find($idEvaluacionPadre);
                    if ($evaluacionPadre) {
                        $evaluacion->setPadre($evaluacionPadre);
                    }

                }
                $unidadIntegracion = $em->getRepository(UnidadIntegracion::class)->find($unidadIntegracion);
                $evaluacion->setUnidadIntegracion($unidadIntegracion);


                if ($this->validar($evaluacion, $unidadIntegracion)) {
                    $em->persist($evaluacion);
                    $em->flush();
                    $response->setData(array(
                        'status' => 'success',
                        'message' => 'Agregado exitosamente'
                    ));
                } else {
                    $response->setData(array(
                        'status' => 'error',
                        'message' => 'La suma de las ponderaciones supera el 100%'
                    ));
                }
            } else {
                $response->setData(array(
                    'status' => 'error',
                    'message' => 'Ocurrió un error'
                ));
            }
            return $response;
        }
        return $this->render('academico/add.html.twig',
            [
                'form' => $form->createView()

            ]);
    }

    /**
     * @Route("/unidadIntegacion/add",name="add_unidadIntegracion")
     */
    function createUnidadIntegracion(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $unidadIntegracion = new UnidadIntegracion();

        $programa = $request->query->get('programa');
        $planEstudioCurso = $request->query->get('planEstudioCurso');
        $periodoCiclo = $request->query->get('periodoCiclo');


        $form = $this->createForm(UnidadIntegracionType::class, $unidadIntegracion, array(

            'action' => $this->generateUrl('add_unidadIntegracion') . '?programa=' . $programa . '&periodoCiclo=' . $periodoCiclo . '&planEstudioCurso=' . $planEstudioCurso . '&submited=1',
        ));
        $form->handleRequest($request);
        if ($request->isXmlHttpRequest() && $request->query->get("submited")) {
            if (!$form->isSubmitted()) {
                $form->submit($request->request->get($form->getName()));
            }
            $response = new JsonResponse();
            $response->setStatusCode(200);
            if ($form->isValid()) {
                $programa = $em->getRepository(Programa::class)->find($programa);
                $planEstudioCurso = $em->getRepository(PlanEstudioCurso::class)->find($planEstudioCurso);
                $periodoCiclo = $em->getRepository(PeriodoCiclo::class)->find($periodoCiclo);
                $unidadIntegracion->setPrograma($programa);
                $unidadIntegracion->setPlanEstudioCurso($planEstudioCurso);
                $unidadIntegracion->setPeriodoCiclo($periodoCiclo);
                $em->persist($unidadIntegracion);
                $em->flush();
                $response->setData(array(
                    'status' => 'success',
                    'message' => 'Agregado exitosamente'
                ));
            } else {
                $response->setData(array(
                    'status' => 'error',
                    'message' => 'Ocurrió un error'
                ));
            }
            return $response;
        }
        return $this->render('academico/addUnidadIntegracion.html.twig',
            [
                'form' => $form->createView()
            ]);
    }

    /**
     * @Route("/edit/{id}",name="edit_evaluacion")
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function edit(Evaluacion $evaluacion, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $id = $evaluacion->getId();
        $form = $this->createForm(EvaluacionType::class, $evaluacion, array(
            'action' => $this->generateUrl('edit_evaluacion', array('id' => $id)),
        ));
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $response = new JsonResponse();
            $response->setStatusCode(200);
            if ($this->validar($evaluacion, $evaluacion->getUnidadIntegracion())) {
                $em->persist($evaluacion);
                $em->flush();
                $response->setData(array(
                    'status' => 'success',
                    'message' => 'Registro actualizado exitosamente'
                ));
            } else {
                $response->setData(array(
                    'status' => 'error',
                    'message' => 'La suma de las ponderaciones supera el 100%'
                ));
            }

            return $response;
        } else {
            return $this->render('academico/add.html.twig',
                [
                    'form' => $form->createView()
                ]);
        }

    }

    /**
     * @Route("/remove/{id}",name="remove_evaluacion")
     */
    public function remove(Evaluacion $evaluacion)
    {
        $em = $this->getDoctrine()->getManager();
        $response = new JsonResponse();
        $response->setStatusCode(200);
        try {
            $em->remove($evaluacion);
            $em->flush();
        } catch (\Exception $e) {
            $response->setData(array(
                'status' => 'error',
                'message' => 'Esta evaluación no puede ser eliminada, tiene otras que dependen de ella',
                'exception' => $e->getMessage(),
            ));
            return $response;
        }


        $response->setData(array(
            'status' => 'success',
            'message' => 'Eliminado exitosamente'
        ));

        return $response;
    }

    /**
     * Verifica que la suma de los porcentajes no supere el 100%
     * @param Evaluacion $evaluacion
     * @param UnidadIntegracion $unidad_integracion
     * @return bool
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    private function validar(Evaluacion $evaluacion, UnidadIntegracion $unidad_integracion)
    {
        /** @var \Doctrine\ORM\EntityManager $em */
        $em = $this->getDoctrine()->getManager();

        $ponderacion = $em->getRepository('Academico:Evaluacion')
            ->getPonderacionByEvaluacionAndUnidadIntegracion($evaluacion, $unidad_integracion);

        //Sumatoria incluyendo la nueva ponderacion
        $suma = $ponderacion + $evaluacion->getPonderacion();

        return $suma <= 100;
    }
}
