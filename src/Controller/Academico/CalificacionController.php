<?php

namespace App\Controller\Academico;

use App\Entity\Academico\Calificacion;
use App\Entity\Academico\CalificacionGrupo;
use App\Entity\Academico\CalificacionPenalizacion;
use App\Entity\Academico\Carrera;
use App\Entity\Academico\Curso;
use App\Entity\Academico\CursoPrograma;
use App\Entity\Academico\EstudianteCurso;
use App\Entity\Academico\Evaluacion;
use App\Entity\Academico\Penalizacion;
use App\Entity\Academico\PeriodoCiclo;
use App\Entity\Academico\PlanEstudio;
use App\Entity\Rotacion\Grupo;
use App\Entity\Rotacion\GrupoEstudiante;
use App\Service\PeriodoService;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class CalificacionController
 * @package App\Controller\Academico
 * @Route("/academico")
 */
class CalificacionController extends AbstractController
{
    /**
     * @var PeriodoService $periodo_service
     */
    private $periodo_service;

    /**
     * CalificacionController constructor.
     * @param ContainerInterface $container
     * @param PeriodoService $periodo_service
     */
    public function __construct(ContainerInterface $container, PeriodoService $periodo_service)
    {
        $this->container = $container;
        $this->denyAccessUnlessGranted('calificacion');
        $this->periodo_service = $periodo_service;
    }

    /**
     * @Route("/notas/", name="registro_notas")
     */
    public function registroNotas()
    {

        $em = $this->getDoctrine()->getManager();
        $carrera = $em->getRepository(Carrera::class)->findOneBy(['codigo' => 'D10701']);
        $plan_estudio = $em->getRepository(PlanEstudio::class)->findOneBy([
            'carrera' => $carrera,
            'anio' => '2009',
        ]);

        $cursos = $em->getRepository(CursoPrograma::class)
            ->getPlanEstudioCursosConProgramas($plan_estudio);
        $periodos = $em->getRepository(PeriodoCiclo::class)
            ->findAll();

        return $this->render('academico/evaluaciones/index.html.twig', [
            'cursos' => $cursos, 'periodos' => $periodos,
        ]);
    }


    /**
     * @Route("/notas/unica/view/{id}", name="registro_notas_unica")
     */
    public function registroNotasUnica(Evaluacion $evaluacion)
    {
        $em = $this->getDoctrine()->getManager();

        $estudiantes = $em->getRepository(EstudianteCurso::class)->findBy([
            'periodo_ciclo' => $evaluacion->getUnidadIntegracion()->getPeriodoCiclo(),
            'curso' => $evaluacion->getUnidadIntegracion()->getPlanEstudioCurso()->getCurso()
        ]);
        $calificaciones = [];
        foreach ($estudiantes as $estudianteCurso) {
            $calificacion = $em->getRepository(Calificacion::class)->findOneBy([
                'evaluacion' => $evaluacion->getId(),
                'estudiante_curso' => $estudianteCurso->getId()
            ]);

            if (!$calificacion) {
                $calificacion = new Calificacion();
                $calificacion->setNota(0);
                $calificacion->setEstudianteCurso($estudianteCurso);
            }

            $calificaciones[] = $calificacion;
        }


        return $this->render('academico/evaluaciones/evaluacion_unica.html.twig', [
            'calificaciones' => $calificaciones,
            'evaluacion' => $evaluacion,
        ]);
    }

    /**
     * @Route("/notas/unica/update/{id}", name="registro_notas_unica_update")
     */
    public function actualizarNotas(Evaluacion $evaluacion, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $data = json_decode($request->query->get("data"));
        $response = new JsonResponse();
        $response->setStatusCode(200);
        foreach ($data as $dato) {
            //dato[2] Almacena la nota
            //dato[3] Almacena el id de estudianteCurso
            $estudiante = $em->getRepository(EstudianteCurso::class)->find($dato[3]);
            $calificacion = $em->getRepository(Calificacion::class)->findOneBy([
                'evaluacion' => $evaluacion->getId(),
                'estudiante_curso' => $estudiante->getId(),
            ]);

            if (!$calificacion) {
                $calificacion = new Calificacion();
                $calificacion->setEvaluacion($evaluacion);
                $calificacion->setEstudianteCurso($estudiante);
                $calificacion->setFecha(new \DateTime());
            }
            $calificacion->setNota($dato[2]);
            $em->persist($calificacion);
        }
        try {
            $em->flush();
        } catch (\Exception $e) {
            $response->setData([
                'status' => 'error',
                'message' => 'No se pudieron guardar los datos',
                'exception' => $e->getMessage(),
            ]);
            return $response;
        }


        $response->setData([
            'status' => 'success',
            'message' => 'Registros actualizados exitosamente',
        ]);
        return $response;
    }

    /**
     * @Route("/notas/diaria", name="registro_notas_diaria")
     * @throws \Exception
     */
    public function registroNotasDiaria(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $fecha = $request->get('fecha') ? \DateTime::createFromFormat('Y-m-d', $request->get('fecha')) : null;

        //Mostrar o no las calificaciones a detalle realizadas por fecha específica
        $mostrar = $request->get('mostrar');
        if (!$mostrar) {
            return new Response('Seleccione la evaluación ascendiente para calificar a detalle');
        }

        if ($fecha) {
            $ciclo = $em->getRepository(PeriodoCiclo::class)->find($request->get('periodoCiclo'));
            $curso = $em->getRepository(Curso::class)->find($request->get('curso'));
            $estudiantes = $em->getRepository(EstudianteCurso::class)->findBy([
                'periodo_ciclo' => $ciclo,
                'curso' => $curso,
            ]);
        } else {
            $estudiantes = [];
        }

        return $this->render('academico/evaluaciones/evaluacion_diaria.html.twig', [
            'fecha' => $fecha,
            'estudiantes' => $estudiantes,
        ]);
    }

    /**
     * Pantalla de registro de nota grupal calculada con penalizaciones
     * @Route("/notas/calculada/{id}", name="registro_notas_calculada")
     */
    public function registroNotasCalculada(Evaluacion $evaluacion)
    {
        $em = $this->getDoctrine()->getManager();
        $ciclo = $evaluacion->getUnidadIntegracion()->getPeriodoCiclo();
        $plan_estudio_curso = $evaluacion->getUnidadIntegracion()->getPlanEstudioCurso();

        $subcalculadas = $em->getRepository('Academico:Evaluacion')
            ->findByPadreAndTipo($evaluacion, 'calculada');

        if (count($subcalculadas) > 0) {
            return new Response('Seleccione una evaluación descendiente para calificar');
        }

        $grupos = null;
        if ($evaluacion->isGrupal()) {
            $grupos = $em->getRepository(Grupo::class)->findByCursoAndCicloAndCalificable($plan_estudio_curso, $ciclo);
        }

        $estudiantes = $em->getRepository(EstudianteCurso::class)->findBy([
            'periodo_ciclo' => $evaluacion->getUnidadIntegracion()->getPeriodoCiclo(),
            'curso' => $plan_estudio_curso->getCurso(),
        ]);
        $calificaciones = [];
        foreach ($estudiantes as $estudianteCurso) {
            $calificacion = $em->getRepository(Calificacion::class)->findOneBy([
                'evaluacion' => $evaluacion->getId(),
                'estudiante_curso' => $estudianteCurso->getId()
            ]);

            if (!$calificacion) {
                $calificacion = new Calificacion();
                $calificacion->setNota(0);
                $calificacion->setEstudianteCurso($estudianteCurso);
            }

            $calificaciones[] = $calificacion;
        }

        return $this->render('academico/evaluaciones/evaluacion_calculada.html.twig', [
            'calificaciones' => $calificaciones,
            'evaluacion' => $evaluacion,
            'grupos' => $grupos,
        ]);
    }


    /**
     * @Route("/notas/calculada/individual/{id}", name="registro_notas_calculada_invididual")
     */
    public function registroNotasCalculadaIndividual(Evaluacion $evaluacion, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $fecha = new \DateTime($request->query->get('fecha'));

        $estudiantes = $em->getRepository(EstudianteCurso::class)->findBy([
            'periodo_ciclo' => $evaluacion->getUnidadIntegracion()->getPeriodoCiclo(),
            'curso' => $evaluacion->getUnidadIntegracion()->getPlanEstudioCurso()->getCurso(),
        ]);
        $calificaciones = [];
        foreach ($estudiantes as $estudianteCurso) {
            $calificacion = $this->getOrCreateCalificacion($fecha, $evaluacion, $estudianteCurso, null);
            $calificaciones[] = $calificacion;
        }

        return $this->render('academico/evaluaciones/evaluacion_calculada_individual.html.twig', [
            'calificaciones' => $calificaciones,
            'evaluacion' => $evaluacion,
        ]);
    }

    /**
     * @Route("/notas/calculada/detalle/{id}", name="registro_notas_calculada_detalle")
     */
    public function registroNotasCalculadaDetalle(Evaluacion $evaluacion, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $fecha = new \DateTime($request->query->get('fecha'));
        $estudiante = $em->getRepository(EstudianteCurso::class)->find($request->query->get('estudiante'));

        //Obtenemos todos los criterios de evaluacion
        $criterios = $em->getRepository(Evaluacion::class)
            ->findBy(['padre' => $evaluacion, 'unidad_integracion' => $evaluacion->getUnidadIntegracion()]);

        $calificacionPadre = $this->getOrCreateCalificacion($fecha, $evaluacion, $estudiante, null);


        //Procesamos los criterios
        $calificacionesCriterios = [];
        foreach ($criterios as $criterio) {
            $padre = null;
            if ($calificacionPadre->getId()) {
                $padre = $calificacionPadre;
            }
            $calificacion = $this->getOrCreateCalificacion($fecha, $criterio, $estudiante, $padre);
            $calificacionesCriterios[] = $calificacion;
        }


        return $this->render('academico/evaluaciones/evaluacion_calculada_nota_estudiante.html.twig', [
            'criterios' => $calificacionesCriterios,
            'calificacionPadre' => $calificacionPadre
        ]);
    }

    /**
     * @Route("/estudiantesGrupo", name="estudiantes_curso")
     */
    public function getEstudiantesGrupo(Request $request, Grupo $grupo)
    {
        $em = $this->getDoctrine()->getManager();
        $evaluacion = $em->getRepository(Evaluacion::class)->find($request->query->get('evaluacion'));
        $fecha = new \DateTime($request->query->get('fecha'));

        //Obtenemos todos los criterios de evaluacion
        $criterios = $em->getRepository(Evaluacion::class)
            ->findBy(['padre' => $evaluacion, 'unidad_integracion' => $evaluacion->getUnidadIntegracion()]);


        $calificacionGrupo = $this->getOrCreateCalificacionGrupo($grupo, $evaluacion, $fecha, null);
        // private function getOrCreateCalificacionGrupo($grupo,$evaluacion,$fecha,$padre){

        //Procesamos los criterios
        $calificacionesCriterios = [];
        foreach ($criterios as $criterio) {
            $calificacionGrupoCriterio = $this->getOrCreateCalificacionGrupo($grupo, $criterio, $fecha, $calificacionGrupo);
            $calificacionesCriterios[] = $calificacionGrupoCriterio;
        }

        //Obteniendo los estudiantes del curso

        $estudiantes = $em->getRepository(GrupoEstudiante::class)->findBy([
            'grupo' => $grupo,
        ]);
        $calificacionesEstudiante = [];
        foreach ($estudiantes as $estudiante) {
            $calificacion = $this->getOrCreateCalificacionFinalEstudianteGrupo($calificacionGrupo, $evaluacion, $estudiante, $fecha);
            $calificacionesEstudiante[] = $calificacion;
        }


        return $this->render('academico/evaluaciones/evaluacion_calculada_nota_grupo.html.twig', [
            'grupo' => $calificacionGrupo,
            'criterios' => $calificacionesCriterios,
            'calificaciones' => $calificacionesEstudiante
        ]);
    }


    /**
     * @Route("/notas/calificacionGrupo/{id}", name="calificacion_grupo")
     *
     */
    public function getCalificacionGrupo(Request $request, Grupo $grupo)
    {
        $em = $this->getDoctrine()->getManager();
        $idEvaluacion = $request->query->get('evaluacion');
        $evaluacion = $em->getRepository(Evaluacion::class)->find($idEvaluacion);
        $fecha = new \DateTime($request->query->get('fecha'));


        //Obtenemos todos los criterios de evaluacion
        $criterios = $em->getRepository(Evaluacion::class)
            ->findBy(['padre' => $evaluacion, 'unidad_integracion' => $evaluacion->getUnidadIntegracion()]);


        $calificacionGrupo = $this->getOrCreateCalificacionGrupo($grupo, $evaluacion, $fecha, null);
        // private function getOrCreateCalificacionGrupo($grupo,$evaluacion,$fecha,$padre){

        //Procesamos los criterios
        $calificacionesCriterios = [];
        foreach ($criterios as $criterio) {
            $calificacionGrupoCriterio = $this->getOrCreateCalificacionGrupo($grupo, $criterio, $fecha, $calificacionGrupo);
            $calificacionesCriterios[] = $calificacionGrupoCriterio;
        }

        $estudiantes = $em->getRepository(GrupoEstudiante::class)->findByGrupoAndPadre($grupo);
        $calificacionesEstudiante = [];
        foreach ($estudiantes as $estudiante) {
            $calificacion = $this->getOrCreateCalificacionFinalEstudianteGrupo($calificacionGrupo, $evaluacion, $estudiante->getEstudianteCurso(), $fecha);
            $calificacionesEstudiante[] = $calificacion;
        }

        $penalizaciones = $em->getRepository(Penalizacion::class)->findAll();


        return $this->render('academico/evaluaciones/evaluacion_calculada_nota_grupo.html.twig', [
            'grupo' => $calificacionGrupo,
            'criterios' => $calificacionesCriterios,
            'calificaciones' => $calificacionesEstudiante,
            'penalizaciones' => $penalizaciones,
            'nuevo' => ($calificacionGrupo->getId() == null),
            'estudiantes' => $estudiantes,
        ]);


    }

    /**
     * @Route("/notas/calculada/update/{grupo}", name="registro_notas_calculada_update")
     * @param Request $request
     * @param Grupo $grupo
     * @param LoggerInterface $logger
     * @return JsonResponse
     */
    public function actualizarNotasCalculadas(Request $request, Grupo $grupo, LoggerInterface $logger)
    {
        /** @var \Doctrine\ORM\EntityManager $em */
        $em = $this->getDoctrine()->getManager();
        $idEvaluacion = $request->query->get('evaluacion');
        $evaluacion = $em->getRepository(Evaluacion::class)->find($idEvaluacion);
        $fecha = new \DateTime($request->query->get('fecha'));

        $datos = json_decode($request->query->get("criterios"));

        $em->beginTransaction();
        try {

            $calificacionGrupo = $this->getOrCreateCalificacionGrupo($grupo, $evaluacion, $fecha, null);
            $em->persist($calificacionGrupo);
            $notaGrupo = 0.0;
            foreach ($datos as $dato) {
                if (!$dato[0]) {
                    continue;
                }
                //dato[3] Almacena el id de la evaluacion
                $evaluacionCriterio = $em->getRepository(Evaluacion::class)->find($dato[3]);
                $calificacionGrupoCriterio = $this->getOrCreateCalificacionGrupo($grupo, $evaluacionCriterio, $fecha, $calificacionGrupo);
                $calificacionGrupoCriterio->setNota($dato[1]);
                $em->persist($calificacionGrupoCriterio);
                $notaGrupo += ($calificacionGrupoCriterio->getNota() * $calificacionGrupoCriterio->getEvaluacion()->getPonderacion());
            }

            //Calculando
            $calificacionGrupo->setNota($notaGrupo / 100);
            $em->persist($calificacionGrupo);


            //Registrando la nota de los estudiantes
            $estudiantes = $em->getRepository(GrupoEstudiante::class)->findBy([
                'grupo' => $grupo,
            ]);
            foreach ($estudiantes as $estudiante) {
                $calificacion = $this->getOrCreateCalificacionFinalEstudianteGrupo($calificacionGrupo, $evaluacion, $estudiante->getEstudianteCurso(), $fecha);
                if ($calificacion->getId()) {
                    $nota = $calificacionGrupo->getNota();
                    /** @var CalificacionPenalizacion $penalizacion */
                    foreach ($calificacion->getPenalizaciones() as $penalizacion) {
                        $nota -= $penalizacion->getPenalizacion()->getPuntos();
                        if ($nota < 0) {
                            $nota = 0;
                        }
                    }
                    $calificacion->setNota($nota);
                }
                $em->persist($calificacion);
            }
            $em->flush();

            foreach ($estudiantes as $estudiante) {
                $calificacion = $this->getOrCreateCalificacionFinalEstudianteGrupo($calificacionGrupo, $evaluacion, $estudiante->getEstudianteCurso(), $fecha);
                $evaluacion_padre = $calificacion->getEvaluacion()->getPadre();
                $estudiante_curso = $estudiante->getEstudianteCurso();
                if ($evaluacion_padre) {
                    $this->calificarCalculadas($evaluacion_padre, $estudiante_curso);
                }
            }

            $em->flush();
            $em->commit();

            $data = [
                'status' => 'success',
                'message' => 'Registros actualizados exitosamente'
            ];
        } catch (\Exception $e) {
            $em->rollback();
            $logger->error($e);
            $data = [
                'status' => 'error',
                'message' => 'No se pudieron guardar los datos',
                'exception' => $e->getMessage(),
            ];
        }

        return new JsonResponse($data);
    }

    /**
     * @Route("/notas/calculada/individual/update/{id}", name="registro_notas_calculada_individual_update")
     */
    public function actualizarNotasCalculadasIndividual(Evaluacion $evaluacion, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $fecha = new \DateTime($request->query->get('fecha'));
        $estudiante = $em->getRepository(EstudianteCurso::class)->find($request->query->get('estudiante'));
        $data = json_decode($request->query->get("criterios"));
        $response = new JsonResponse();
        $response->setStatusCode(200);

        $calificacionPadre = $this->getOrCreateCalificacion($fecha, $evaluacion, $estudiante, null);
        $em->persist($calificacionPadre);
        $notaGrupo = 0.0;
        foreach ($data as $dato) {
            if (!$dato[0]) {
                continue;
            }
            //dato[3] Almacena el id de la evaluacion
            $evaluacionCriterio = $em->getRepository(Evaluacion::class)->find($dato[3]);
            $calificacion = $this->getOrCreateCalificacion($fecha, $evaluacionCriterio, $estudiante, $calificacionPadre);
            $calificacion->setNota($dato[1]);
            $em->persist($calificacion);
            $notaGrupo += ($calificacion->getNota() * $calificacion->getEvaluacion()->getPonderacion());
        }

        //Calculando
        $calificacionPadre->setNota($notaGrupo / 100);
        $em->persist($calificacionPadre);

        $evaluacion_padre = $calificacionPadre->getEvaluacion()->getPadre();
        $estudiante_curso = $estudiante;
        if ($evaluacion_padre) {
            $this->calificarCalculadas($evaluacion_padre, $estudiante_curso);
        }

        try {
            $em->flush();
        } catch (\Exception $e) {
            $response->setData([
                'status' => 'error',
                'message' => 'No se pudieron guardar los datos',
                'exception' => $e->getMessage(),
            ]);
            return $response;
        }

        $response->setData([
            'status' => 'success',
            'message' => 'Registros actualizados exitosamente'
        ]);
        return $response;
    }

    /**
     * @param Evaluacion $evaluacion
     * @param EstudianteCurso $estudiante_curso
     */
    public function calificarCalculadas(Evaluacion $evaluacion, EstudianteCurso $estudiante_curso)
    {
        $em = $this->getDoctrine()->getManager();

        while ($evaluacion) {
            $calificacion = $this->getOrCreateCalificacionCalculada($evaluacion, $estudiante_curso, null);
            $nota = $em->getRepository('Academico:Calificacion')
                ->getNotaCalculada($evaluacion, $estudiante_curso);
            $calificacion->setNota($nota);
            $calificacion->setFecha(new \DateTime());
            $em->persist($calificacion);
            $evaluacion = $calificacion->getEvaluacion()->getPadre();
        }
    }

    /**
     * @Route("/notas/calculada/penalizacion/{id}", name="calificacion_penalizacion")
     */
    public function addPenalizacion(Request $request, Calificacion $calificacion)
    {
        $em = $this->getDoctrine()->getManager();
        $penalizacion = $em->getRepository(Penalizacion::class)->find($request->query->get('penalizacion'));

        $calificacionPenalizacion = $em->getRepository(CalificacionPenalizacion::class)->findOneBy([
            'calificacion' => $calificacion,
            'penalizacion' => $penalizacion
        ]);
        $response = new JsonResponse();
        if ($calificacionPenalizacion) {
            $response->setData([
                'status' => 'error',
                'message' => 'No se pudieron guardar los datos',
            ]);
            return $response;
        }


        $calificacionPenalizacion = new CalificacionPenalizacion();
        $calificacionPenalizacion->setCalificacion($calificacion);
        $calificacionPenalizacion->setPenalizacion($penalizacion);
        $calificacionPenalizacion->setCreado(new \DateTime());


        $calificacion->setNota($calificacion->getNota() - $penalizacion->getPuntos());
        if ($calificacion->getNota() < 0) {
            $calificacion->setNota(0);
        }
        $em->persist($calificacionPenalizacion);
        $em->persist($calificacion);
        $em->flush();

        $evaluacion_padre = $calificacion->getEvaluacion()->getPadre();
        $estudiante_curso = $calificacion->getEstudianteCurso();
        if ($evaluacion_padre) {
            $this->calificarCalculadas($evaluacion_padre, $estudiante_curso);
        }

        $em->flush();

        $response->setData([
            'status' => 'success',
            'message' => 'Registros actualizados exitosamente',
        ]);
        return $response;
    }

    /**
     * @Route("/notas/penalizacion/remove/{id}", name="penalizacion_remove")
     */
    public function removePenalizacion(CalificacionPenalizacion $calificacionPenalizacion)
    {
        $em = $this->getDoctrine()->getManager();
        $calificacion = $calificacionPenalizacion->getCalificacion();
        $penalizacion = $calificacionPenalizacion->getPenalizacion();

        $nota = $calificacion->getCalificacionGrupo()->getNota();
        /** @var CalificacionPenalizacion $penalizacion1 */
        foreach ($calificacion->getPenalizaciones() as $penalizacion1) {
            $nota -= $penalizacion1->getPenalizacion()->getPuntos();
        }
        //Se le agregan los puntos porque aun no se ha eliminado
        $nota += $penalizacion->getPuntos();
        if ($nota < 0) {
            $nota = 0;
        }
        $calificacion->setNota($nota);

        $response = new JsonResponse();
        $response->setStatusCode(200);
        try {
            $em->persist($calificacion);
            $em->remove($calificacionPenalizacion);
            $em->flush();

            $evaluacion_padre = $calificacion->getEvaluacion()->getPadre();
            $estudiante_curso = $calificacion->getEstudianteCurso();
            if ($evaluacion_padre) {
                $this->calificarCalculadas($evaluacion_padre, $estudiante_curso);
            }

            $em->flush();
        } catch (\Exception $e) {
            $response->setData([
                'status' => 'error',
                'message' => 'No se eliminar',
                'exception' => $e->getMessage(),
            ]);
            return $response;
        }


        $response->setData([
            'status' => 'success',
            'message' => 'Eliminado exitosamente'
        ]);

        return $response;
    }

    /**
     * @param $grupo
     * @param $evaluacion
     * @param $fecha
     * @param $padre
     * @return CalificacionGrupo
     */
    private function getOrCreateCalificacionGrupo($grupo, $evaluacion, $fecha, $padre)
    {
        $em = $this->getDoctrine()->getManager();
        //Obtenemos la calificacion del grupo
        $calificacionGrupo = $em->getRepository(CalificacionGrupo::class)->findOneBy([
            'grupo' => $grupo,
            'evaluacion' => $evaluacion,
            'fecha' => $fecha,
            'padre' => $padre
        ]);
        //Sino existe la creamos para que la vista sea consistente
        if (!$calificacionGrupo) {
            $calificacionGrupo = new CalificacionGrupo();
            $calificacionGrupo->setEvaluacion($evaluacion);
            $calificacionGrupo->setNota(0);
            $calificacionGrupo->setGrupo($grupo);
            $calificacionGrupo->setFecha($fecha);
            if ($padre) {
                $calificacionGrupo->setPadre($padre);
            }
        }
        return $calificacionGrupo;
    }

    /**
     * @param CalificacionGrupo $calificacionGrupo
     * @param $evaluacion
     * @param $estudiante
     * @param $fecha
     * @return Calificacion
     */
    private function getOrCreateCalificacionFinalEstudianteGrupo(CalificacionGrupo $calificacionGrupo, $evaluacion, $estudiante, $fecha)
    {
        $em = $this->getDoctrine()->getManager();

        $calificacion = $em->getRepository(Calificacion::class)->findOneBy([
            'calificacion_grupo' => $calificacionGrupo,
            'evaluacion' => $evaluacion,
            'estudiante_curso' => $estudiante,
            'fecha' => $fecha
        ]);

        if (!$calificacion) {
            $calificacion = new Calificacion();
            $calificacion->setEvaluacion($evaluacion);
            $calificacion->setCalificacionGrupo($calificacionGrupo);
            $calificacion->setEstudianteCurso($estudiante);
            $calificacion->setFecha($fecha);
            $calificacion->setNota($calificacionGrupo->getNota());
        }
        return $calificacion;
    }

    /**
     * @param \DateTime $fecha
     * @param Evaluacion $evaluacion
     * @param EstudianteCurso $estudianteCurso
     * @param $padre
     * @return Calificacion
     */
    private function getOrCreateCalificacion(\DateTime $fecha, Evaluacion $evaluacion, EstudianteCurso $estudianteCurso, $padre)
    {
        $em = $this->getDoctrine()->getManager();
        $calificacion = $em->getRepository(Calificacion::class)->findOneBy([
            'evaluacion' => $evaluacion,
            'estudiante_curso' => $estudianteCurso,
            'fecha' => $fecha,
            'padre' => $padre
        ]);

        if (!$calificacion) {
            $calificacion = new Calificacion();
            if ($padre) {
                $calificacion->setPadre($padre);
            }
            $calificacion->setNota(0);
            $calificacion->setEvaluacion($evaluacion);
            $calificacion->setEstudianteCurso($estudianteCurso);
            $calificacion->setFecha($fecha);
        }
        return $calificacion;
    }

    /**
     * @param \DateTime $fecha
     * @param Evaluacion $evaluacion
     * @param EstudianteCurso $estudianteCurso
     * @param $padre
     * @return Calificacion
     */
    private function getOrCreateCalificacionCalculada(Evaluacion $evaluacion, EstudianteCurso $estudianteCurso, $padre)
    {
        $em = $this->getDoctrine()->getManager();
        $calificaciones = $em->getRepository(Calificacion::class)->findBy([
            'evaluacion' => $evaluacion,
            'estudiante_curso' => $estudianteCurso,
            'padre' => $padre
        ]);

        if (count($calificaciones) > 1) {
            foreach ($calificaciones as $calificacion) {
                $em->remove($calificacion);
            }
            $em->flush();

            $calificacion = null;
        } else {
            $calificacion = $em->getRepository(Calificacion::class)->findOneBy([
                'evaluacion' => $evaluacion,
                'estudiante_curso' => $estudianteCurso,
                'padre' => $padre
            ]);
        }

        if (!$calificacion) {
            $calificacion = new Calificacion();
            if ($padre) {
                $calificacion->setPadre($padre);
            }
            $calificacion->setNota(0);
            $calificacion->setEvaluacion($evaluacion);
            $calificacion->setEstudianteCurso($estudianteCurso);
        }
        return $calificacion;
    }

}
