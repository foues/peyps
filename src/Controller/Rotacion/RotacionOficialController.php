<?php

namespace App\Controller\Rotacion;


use App\Service\EmailService;
use App\Service\RotacionService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Dompdf\Dompdf;
use Dompdf\Options;
use App\Service\DqlRotacionUtilService;

/**
 * @Route("/rotacion")
 */
class RotacionOficialController extends AbstractController
{
    private $DqlUtilService;
    /**
     * FichaController constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container, DqlRotacionUtilService $DqlUtilService)
    {
        $this->container = $container;
        $this->denyAccessUnlessGranted('rotacion_oficial');
        $this->DqlUtilService = $DqlUtilService;
    }

    /**
     * @Route("/aceptar",name="aceptar")
     */
    public function aceptarRotacion(RotacionService $rotacionService)
    {

        $em = $this->getDoctrine()->getManager();
        //Obteniendo la Planificacion de Rotacion
        $planificacion = $em->getRepository('Rotacion:Planificacion')
            ->findOneBy(array('periodo_ciclo' => $rotacionService->obtenerPeriodoCiclo()->getId(), 'confirmado' => 0));

        //confirmando las planificacion de rotaciones
        if ($planificacion) {
            $planificacion->setConfirmado(1);
            $em->flush();
            $this->addFlash('success', 'Planificacion de Rotacion Confirmada!');
            $this->DqlUtilService->CrearSubGruposPPE();
           return $this->redirectToRoute('rotacion_preliminar');
        }
        /*
        $rotaciones=$em->getRepository("Rotacion:Rotacion")
            ->findBy(array('planificacion'=>$planificacion));

        //confirmando las rotaciones
        if($rotaciones) {
            $planificacion->setConfirmado(1);
            $em->flush();
            //enviando mensaje
            $this->addFlash('success','Rotaciones Confirmadas!');
        }
        */
        $this->addFlash('danger', 'No existe Planificacion de Rotacion a Confirmar!!');
        return $this->redirectToRoute('/');
    }

    /**
     * @Route("/email",name="email")
     */
    public function email(\Swift_Mailer $mailer, EmailService $emailService, RotacionService $rotacionService)
    {
        /*
        $periodo_ciclo=$rotacionService->obtenerPeriodoCiclo()->getId();
        //escogiendo los destinatarios
        $destinatarios=array("fernando.ebolanos@gmail.com","fer143monster@gmail.com","fh.menendez@outlook.com");
        //recorriendo los cursos que tienen rotacion para el ciclo
        foreach ($rotacionService->obtenerCursosRotando($periodo_ciclo) as $cursos){
            foreach ($rotacionService->obtenerCorreosRotacion($periodo_ciclo,$cursos["cursos"]) as $email){
                //insertando los alumnos rotando en la los destinatarios del correo
                //array_push($destinatarios,$email["carnet"]."@ues.edu.sv");
            }
            //enviando el correo
            $emailService->send('Rotacion PEYPS',$destinatarios,
                ['rotaciones'=>$rotacionService->obtenerResumenRotacionEmail($periodo_ciclo,$cursos["cursos"])]
                ,'email/rotacion_oficial.html.twig');
            //reiniciando el array
            $destinatarios=array("fernando.ebolanos@gmail.com","fer143monster@gmail.com","fh.menendez@outlook.com");
        };
        $this->addFlash('success','Publicacion de Rotacion Realizada!');
        return $this->redirectToRoute('/');
        */

        $periodo_ciclo = $rotacionService->obtenerPeriodoCiclo()->getId();
        //escogiendo los destinatarios
        $destinatarios_A = array("rd12004@ues.edu.sv", "fer143monster@gmail.com");
        $destinatarios_B = array("rd12004@ues.edu.sv", "fer143monster@gmail.com");
        //recorriendo los cursos que tienen rotacion para el ciclo
        foreach ($rotacionService->obtenerCursosRotando($periodo_ciclo) as $cursos) {
            foreach ($rotacionService->obtenerCorreosRotacion($periodo_ciclo, $cursos["cursos"]) as $email) {
                //insertando los alumnos rotando en la los destinatarios del correo
                //array_push($destinatarios,$email["carnet"]."@ues.edu.sv");
            }
            //$r=$rotacionService->obtenerResumenRotacionEmail($periodo_ciclo,$cursos["cursos"],'A');
            //$r=reset($r);
            //dump($rotacionService->obtenerResumenRotacionEmail($periodo_ciclo,$cursos["cursos"],'A'));
            if ($cursos["cursos"] == 'ODONTOLOGÍA PREVENTIVA COMUNITARÍA E INVESTIGACIÓN V'
                or $cursos["cursos"] == 'ODONTOLOGÍA PREVENTIVA COMUNITARÍA E INVESTIGACIÓN VI'
                or $cursos["cursos"] == 'ODONTOLOGÍA PREVENTIVA COMUNITARÍA E INVESTIGACIÓN VII') {
                $emailService->send('Rotacion PEYPS', $destinatarios_A,
                    ['rotaciones' => $rotacionService->obtenerResumenRotacionEmail($periodo_ciclo, $cursos["cursos"], 'A'), 'tipo' => 'A']
                    , 'email/rotacion_oficial2.html.twig');

                $emailService->send('Rotacion PEYPS', $destinatarios_B,
                    ['rotaciones' => $rotacionService->obtenerResumenRotacionEmail($periodo_ciclo, $cursos["cursos"], 'B'), 'tipo' => 'B']
                    , 'email/rotacion_oficial2.html.twig');
//                dump('Entre 1');
            } else {
                $emailService->send('Rotacion PEYPS', $destinatarios_B,
                    ['rotaciones' => $rotacionService->obtenerResumenRotacionEmail($periodo_ciclo, $cursos["cursos"], 'B'), 'tipo' => 'C']
                    , 'email/rotacion_oficial2.html.twig');
//                dump('Entre 2');
            }

            //reiniciando el array
            $destinatarios_A = array("rd12004@ues.edu.sv", "fer143monster@gmail.com");
            $destinatarios_B = array("rd12004@ues.edu.sv", "fer143monster@gmail.com");
        };

        $this->addFlash('success', 'Publicacion de Rotacion Realizada!');

        return $this->redirectToRoute('/');
    }

    /**
     * @Route("/individual/{carnet}",name="rotacion_individual")
     */
    public function RotacionIndividual(RotacionService $rotacionService, $carnet)
    {

        //$carnet=$this->getUser()->getUserName();
        //dd($rotacionService->obtenerResumenRotacionAlumno(4,$carnet));
        //$carnet="HH05045";
        return $this->render('rotacion/rotacion_individual.html.twig',
            array('rotaciones' => $rotacionService->obtenerResumenRotacionAlumno(null, $carnet)));

        /*
        $result=$dqlRotacionUtilService
            ->getRotacionConsolidad(null,null,null,null,
                null,null,null,null,'PV16008');
        dd($result);
        */
    }

    /**
     * @Route("/test")
     */
    public function test()
    {
        $f = $this->getDoctrine()->getRepository("Clinico:Ficha")->find(1);
        $f->setEmbarazada(false);
        dd($f);
        return new Response();
    }

    /**
     * @Route("/reporte",name="reporte")
     */
    public function reporte()
    {
        // Configure Dompdf according to your needs
        $pdfOptions = new Options();
        $pdfOptions->set('defaultFont', 'Arial');

        // Instantiate Dompdf with our options
        $dompdf = new Dompdf($pdfOptions);

        // Retrieve the HTML generated in our twig file
        $html = $this->renderView('reportes/estadisticos.html.twig', [
            'title' => "Bienvenido al sistema SIGEPREX"

        ]);

        // Load HTML to Dompdf
        $dompdf->loadHtml($html);

        // (Optional) Setup the paper size and orientation 'portrait' or 'portrait'
        $dompdf->setPaper('A4', 'portrait');

        // Render the HTML as PDF
        $dompdf->render();

        // Output the generated PDF to Browser (force download)
        $dompdf->stream("peyps.pdf", [
            "Attachment" => false
        ]);
    }
}