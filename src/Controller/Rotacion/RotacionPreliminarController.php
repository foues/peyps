<?php

namespace App\Controller\Rotacion;

use App\Entity\Rotacion\ProgramaCalendario;
use App\Service\RotacionService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/rotacion")
 */
class RotacionPreliminarController extends AbstractController
{
    /**
     * @var RotacionService
     */
    private $servicio_rotacion;

    /**
     * RotacionPreliminarController constructor.
     * @param ContainerInterface $container
     * @param RotacionService $servicio_rotacion
     */
    public function __construct(ContainerInterface $container, RotacionService $servicio_rotacion)
    {
        $this->container = $container;
        $this->denyAccessUnlessGranted('rotacion_preliminar');
        $this->servicio_rotacion = $servicio_rotacion;
    }

    /**
     * @Route("/parametrizacion", name="parametrizacion",methods="GET|POST"))
     */
    public function parametrizacion(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $programas_calendarios = $em->getRepository('Rotacion:ProgramaCalendario')->findBy(array('periodo_ciclo' => $this->servicio_rotacion->obtenerPeriodoCiclo()));
        if (!$programas_calendarios) {
            $this->addFlash('danger', 'Porfavor establecer una calendarizacion del ciclo!');
            return $this->redirectToRoute('calendarizacion');
        }

        $especiales = $em->getRepository('Rotacion:ProgramaCalendario')->findBy(array('tipo' => 'ESPECIAL'));
        $instituciones = $em->getRepository('Rotacion:ProgramaInstitucion')->findAll();

        return $this->render('rotacion/parametrizacioncopy.html.twig', array('especiales' => $especiales, 'instituciones' => $instituciones));

    }

    /**
     * @Route("/parametrizacionEspecial/{id}", defaults={"id"=0},name="parametrizacionEspecial"))
     */
    public function parametrizacionEspecial(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $especiales = $em->getRepository('Rotacion:ProgramaCalendario')->findBy(array('tipo' => 'ESPECIAL'));
        $instituciones = $em->getRepository('Rotacion:ProgramaInstitucion')->findAll();

        if ($id != 0) {
            $em->remove($em->getRepository('Rotacion:ProgramaCalendario')->find($id));
            $em->flush();
            $this->addFlash('danger', 'Fecha Especial Eliminada!');
            $this->redirectToRoute('parametrizacion');
        }

        if ($request->isMethod("POST")) {
            $programa2 = new ProgramaCalendario();
            $programa2->setPrograma($em->getRepository('Rotacion:Programa')->findOneBy(array('codigo' => 'ppe')));
            $programa2->setPeriodoCiclo($this->servicio_rotacion->obtenerPeriodoCiclo());
            $programa2->setGrupo($em->getRepository('Rotacion:Grupo')
                ->findOneBy(array('periodo_ciclo' => $this->servicio_rotacion->obtenerPeriodoCiclo(), 'nombre' => $request->get("grupo"))));
            $programa2->setTipo('ESPECIAL');
            $programa2->setDescripcion($request->get('motivo'));
            $programa2->setFecha(new \DateTime($request->get('fecha')));
            $programa2->setCreado(new \DateTime('now'));
            $em->persist($programa2);
            $em->flush();
            $this->addFlash('success', 'Fecha Especial Agregada Exitosamente!');
            $this->redirectToRoute('parametrizacion');
        }


        return $this->redirectToRoute('parametrizacion');
    }

    /**
     * @Route("/parametrizacionInstitucion", defaults={"id"=0},name="parametrizacionInstitucion",methods="POST"))
     */
    public function parametrizacionInstitucion(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        if ($request->isMethod("POST")) {

            $institucion = $em->getRepository("Rotacion:ProgramaInstitucion")->find($request->get("id_institucion"));
            $institucion->setCantidadEstudiante($request->get('cantidad'));
            $em->flush($institucion);
            $this->addFlash('success', 'Cambio Realizado Exitosamente!');
            return $this->redirectToRoute('parametrizacion');
        }


        return $this->redirectToRoute('parametrizacion');
    }

    /**
     * @Route("/calendarizacion",name="calendarizacion")
     */
    public function calendarizacion(Request $request)
    {

        $perido_actual = $this->servicio_rotacion->obtenerPeriodoCiclo();//sustituir por servicio de nelson

        if (!$perido_actual) { //sino hay ciclo actual
            $this->addFlash('danger', 'Porfavor establecer un periodo!');
            return $this->redirectToRoute('/');
        }

        $em = $this->getDoctrine()->getManager();
        $grupos = $em->getRepository('Rotacion:Grupo')->findBy(array('periodo_ciclo' => $perido_actual));
        $peridos_ciclos = $em->getRepository("Rotacion:ProgramaCalendario")->findBy(array('periodo_ciclo' => $perido_actual));

        if ($request->isMethod("POST")) {
            //verifca si existe calendarizacion para el ciclo en curso
            if (!$peridos_ciclos) {
                //si no existen grupos los creo
                if (!$grupos) {
                    //creando grupos
                    $this->servicio_rotacion->crearGruposRotacion($perido_actual);
                    //guardando si no hay errores
                    $em->flush();
                }

                //PPE
                $this->servicio_rotacion->crearCalendarizacionDoble($request, $perido_actual, 'ppe', 1);

                //CESA
                $this->servicio_rotacion->crearCalendarizacionDoble($request, $perido_actual, 'cesa', 6);

                //CSNS
                $this->servicio_rotacion->crearCalendarizacionDoble($request, $perido_actual, 'csns', 6);

                //HNR
                $this->servicio_rotacion->crearCalendarizacionDoble($request, $perido_actual, 'hnr', 6);

                //ULC
                $this->servicio_rotacion->crearCalendarizacionUnica($request, $perido_actual, 'ulc', 6);

                //CLC
                $this->servicio_rotacion->crearCalendarizacionUnica($request, $perido_actual, 'clc', 6);

                //guardando si no hay errores
                $em->flush();

                //redirigiendo al resumen con mensaje de exito
                $this->addFlash('success', 'Calendarizacion guardada!');
                return $this->redirectToRoute('calendarizacionResumen');
            } else {
                //Actualizacion de calendarizacion

                //PPE
                $this->servicio_rotacion->updateCalendarizacionDoble($request, $perido_actual, 'ppe', 6);

                //CESA
                $this->servicio_rotacion->updateCalendarizacionDoble($request, $perido_actual, 'cesa', 6);

                //CSNS
                $this->servicio_rotacion->updateCalendarizacionDoble($request, $perido_actual, 'csns', 6);

                //HNR
                $this->servicio_rotacion->updateCalendarizacionDoble($request, $perido_actual, 'hnr', 6);

                //ULC
                $this->servicio_rotacion->updateCalendarizacionUnica($request, $perido_actual, 'ulc', 6);

                //CLC
                $this->servicio_rotacion->updateCalendarizacionUnica($request, $perido_actual, 'clc', 6);

                //actualizacion fechas rotacion
                foreach ($this->servicio_rotacion->obtenerResumenCalendarizacion() as $r) {

                    $rotacion = $em->getRepository('Rotacion:Rotacion')->findOneBy(array('grupo' => $r["id_grupo"]));
                    if ($rotacion) {
                        $rotacion->setInicio(new \DateTime($r["INICIO"]));
                        $rotacion->setFin(new \DateTime($r["FIN"]));
                    }

                };

                //guardando si no hay errores
                $em->flush();

                $this->addFlash('success', 'Calendarizacion actualizada!');
            }
        }
        //dd($this->servicio_rotacion->obtenerResumenCalendarizacion());
        if ($peridos_ciclos)
            return $this->render('rotacion/calendarizaciondinamico.html.twig', array('resumen' => $this->servicio_rotacion->obtenerResumenCalendarizacion()));
        else
            return $this->render('rotacion/calendarizaciondinamico.html.twig');
    }

    /**
     * @Route("/calendarizacion/resumen",name="calendarizacionResumen")
     */
    public function calendarizacionResumen()
    {
        $em = $this->getDoctrine()->getManager();
        //comprobando si existe calendarizacion del ciclo
        if ($em->getRepository("Rotacion:ProgramaCalendario")->findBy(array('periodo_ciclo' => $this->servicio_rotacion->obtenerPeriodoCiclo())))
            return $this->render('rotacion/calendarizacion_resumen.html.twig', array('resumen' => $this->servicio_rotacion->obtenerResumenCalendarizacion()));
        else
            return $this->render('rotacion/calendarizacion_resumen.html.twig');
    }

    /**
     * @Route("/estudiante/{carnet}",defaults={"carnet"=null},name="rotacionEstudiante")
     */
    public function rotacionEstudiante($carnet)
    {
        if ($carnet) {
            $rotacion_alumno =
                $this->servicio_rotacion->obtenerResumenRotacionAlumno(null, $carnet);
            $especiales = $this->getDoctrine()->getRepository("Rotacion:ProgramaCalendario")
                ->findBy(array('tipo' => 'ESPECIAL'), array('fecha' => 'ASC'));
            return $this->render('rotacion/rotacion_estudiante.html.twig',
                array('alumno' => $rotacion_alumno, 'especiales' => $especiales)
            );
        } else return $this->redirectToRoute('/');
    }
}
