<?php

namespace App\Controller\Rotacion;

use App\Entity\Rotacion\Grupo;
use App\Entity\Rotacion\GrupoEstudiante;
use App\Entity\Rotacion\Rotacion;
use App\Entity\Rotacion\RotacionEstudiante;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Routing\Annotation\Route;
use App\Service\DqlRotacionUtilService;
use App\Service\RotacionService;
use App\Service\PeriodoService;


/**
 * @Route("/rotacionPrevisualizacion")
 */
class RotacionPrevisualizacion extends AbstractController
{
    /**
     * @var RotacionService
     */
    private $DqlUtilService;
    private $servicio_rotacion;
    private $gruposRotables = 6;
    private $periodo_service;
    private $materias = "''";
    private $Prod = true; // para que guarde en la base true

    /**
     * RotacionPrevisualizacion constructor.
     * @param ContainerInterface $container
     * @param DqlRotacionUtilService $DqlUtilService
     * @param RotacionService $servicio_rotacion
     * @param PeriodoService $periodo_service
     */
    public function __construct(ContainerInterface $container, DqlRotacionUtilService $DqlUtilService, RotacionService $servicio_rotacion, PeriodoService $periodo_service)
    {
        $this->container = $container;
        $this->denyAccessUnlessGranted('rotacion_previsualizacion');
        $this->DqlUtilService = $DqlUtilService;
        $this->servicio_rotacion = $servicio_rotacion;
        $this->periodo_service = $periodo_service;
    }


    /**
     * @Route("/", name="rotacion_preliminar")
     * @throws \Exception
     */
    public function indexRotacionPre()
    {

        $em = $this->getDoctrine()->getManager();

        $em->beginTransaction();

        try {
            $usuario = $this->getUser();
            $usuario->getCuenta();

//++++++++++modulo que realizan la rotacion
            $this->rotacionProces();
            $this->rotacionEstudiantes();


//            ++++++++ para los filtros
            $rotacionEstudianteLocal = $this->DqlUtilService->getRotacionConsolidad();
            $instituciones = $this->DqlUtilService->executeNativeSql("SELECT DISTINCT idPrograma,codigoPrograma,nombrePrograma FROM rotacion.institucion_programa");
            $programaXinstitucion = $this->DqlUtilService->executeNativeSql("SELECT * FROM rotacion.institucion_programa");
            $gruposRotables = $this->DqlUtilService->getRotacion();
            //dd($gruposRotables); // aca deno de llenar el listBox con A 1 2 3 B 1 2 3

            $CursosDeCicloPerido = $this->DqlUtilService->getCursoPeridoCiclo();

            $em->commit();
        } catch (\Exception $e) {
            $em->rollBack();
            throw $e;
        }

        //verifica si existen rotaciones por confirmar
        //$planificacion=$em->getRepository('Rotacion:Planificacion')->findOneBy(array('confirmado'=>0));
        $planificacion = $em->getRepository('Rotacion:Planificacion')
            ->findOneBy(array('periodo_ciclo' => $this->servicio_rotacion->obtenerPeriodoCiclo()->getId(), 'confirmado' => 0));

        $rotaciones = ($em->getRepository("Rotacion:Rotacion")
            ->findBy(array('planificacion' => $planificacion))) ? true : false;
        //dd($rotacionEstudianteLocal);
        return $this->render('rotacion/rotacion_previsualizacion.html.twig', [
            "rotacioEstudiante" => []
            , "rotacioEstudianteLocal" => $rotacionEstudianteLocal
            , "instituciones" => $instituciones
            , "programaXinstitucion" => $programaXinstitucion
            , "gruposRotables" => $gruposRotables
            , "cursosDeCicloPerido" => $CursosDeCicloPerido
            , 'rotaciones' => $rotaciones
        ]);

    }

//*********************************************************************************************************************

    /**
     * Grupo estudiante -> Se llena la tablar previa a la rotacion
     * @Route("/proceso_rotacion", name="proceso_rotacion")
     * @throws \Exception
     */
    public function rotacionProces()
    {
        $algo12 = null;
        $CursosDeCicloPerido = $this->DqlUtilService->getCursoPeridoCiclo();
//dd($CursosDeCicloPerido);
        if (!$this->DqlUtilService->existeGrupoEstudianteEnPeriodo()) {// Oficial
            foreach ($CursosDeCicloPerido as $keyCDP => $valueCDP) {
                $this->materias = $valueCDP->getcodigo();
//            $this->materias = null;

//        if (true) { // para testing
                $periodoCicloAnterior = $this->periodo_service->getCicloAnterior($this->periodo_service->getCicloActual())->getId();
                /**
                 * se esta asumientod que un estudiante solo puede estar cursando una materia extramural
                 * es decir que jamas podra llevar estomatologia 1 con prenventiva 2
                 */

                $em = $this->getDoctrine()->getManager();

                $periodoCiclo = $this->periodo_service->getCicloActual()->getid();


                /**
                 * inicia el proceso de rotacion
                 */

                //en este segmento puedo yo enviar los estudiantes para el curso
                $estudianteRotables = $this->DqlUtilService->estudianteRotables(null, $this->materias);
//             dd($estudianteRotables);
                $CuentaEstudiantesRotables = count($estudianteRotables);
                $gruposRotables = $this->gruposRotables;
                $tamañoGrupo = (int)round($CuentaEstudiantesRotables / $gruposRotables);
                $grupoEstudiantes =
                    $em->getRepository('Rotacion:Grupo')
                        ->findBy(array('periodo_ciclo' => $this->servicio_rotacion->obtenerPeriodoCiclo(), "rotable" => true));

                if (!$this->Prod) echo("#  TEST de estudiantes  :" . $CuentaEstudiantesRotables . "<br>");
                if (!$this->Prod) echo("# TEST estudiante por grupo (MAX) :" . $tamañoGrupo . "<br>");
//dd($grupoEstudiantes);
                /**
                 * forma para declara un array de arrays
                 *        $GrupoConEstudiante["0"][] = "arbol";
                 *        $GrupoConEstudiante["0"][] =   "rasengasn";
                 */
                $GrupoConEstudiante = [];

//            $algo11=$red= null;
//            $algo11 [] = $estudianteRotables;
                if ($this->verificaMateriaRotacion($this->materias)) {
                    foreach ($estudianteRotables as $key => $value) {
                        $EstActual = $value->getEstudiantePlanEstudio()->getEstudiante()->getid();
                        $rotacionEstudianteAnterior = $this->DqlUtilService->getRotacionConsolidad($periodoCicloAnterior, null, null, $EstActual, null, null, null, "nombreGrupo");

                        $algo12[count($rotacionEstudianteAnterior)][] = count($rotacionEstudianteAnterior);


                        //  $algo11[$key][]=$rotacionEstudianteAnterior;

                        //si en caso no tiene grupo de rotacion pasada se le crearan Excluyendo al ppe
                        if (count($rotacionEstudianteAnterior) < 2) {
//                    if (true) {
                            //  $algo11["F"][]=$rotacionEstudianteAnterior;
                            $grupoB = 3;
                            $grupoA = 0;


                            if ((($key + 1) % 2) == 0)
                                for ($i = 0; $i < $gruposRotables; $i++) {

                                    if ((($i + 1) % 2) == 0) {
                                        $GrupoConEstudiante[$grupoB][] = new GrupoEstudiante($value, $grupoEstudiantes [($grupoB)], new \DateTime('now'));
                                        $grupoB++;
                                    }
                                }
                            else for ($i = 0; $i < $gruposRotables; $i++) {
//
                                if (!(($i + 1) % 2) == 0) {
                                    $GrupoConEstudiante[$grupoA][] = new GrupoEstudiante($value, $grupoEstudiantes [($grupoA)], new \DateTime('now'));
                                    $grupoA++;
                                }
                            }

                        } else {
                            // $algo11 ["D"][]= $rotacionEstudianteAnterior;
                            //segmento donde pondre el mismo grupo que la rotacion pasada
                            foreach ($rotacionEstudianteAnterior as $keyGEX => $valueGEX) {
                                $grupoViejo = $valueGEX["nombreGrupo"] - 1;
//                            if($valueGEX["nombreGrupo"]==4) dd($valueGEX,$rotacionEstudianteAnterior);
                                if ($keyGEX != 3) {
//                            $algo12[]= $grupoViejo;
                                    $algo5 [] = new GrupoEstudiante($value, $grupoEstudiantes [($grupoViejo)], new \DateTime('now'));
                                    $em->persist(new GrupoEstudiante($value, $grupoEstudiantes [($grupoViejo)], new \DateTime('now')));
                                }
                            }


                        }

                    }

                }

                // Segmento donde se ponen los GurposEstudiantes que son unicamente de los estudiantes actos para rotar
                foreach ($estudianteRotables as $key => $value) {
                    $GrupoConEstudiante[7][] = new GrupoEstudiante($value, $grupoEstudiantes [(6)], new \DateTime('now'));
                }

                $algo3 [] = $this->materias;
                $algo3 [] = $GrupoConEstudiante;

//  dd($grupoEstudiantes,$estudianteRotables,$GrupoConEstudiante);


//dd($GrupoConEstudiante);
                /**
                 * recorremos el grupo al que vamos a insertar, se separa de la logica porque asi este mecanismo guardara indepediente
                 * de los filtro o cambios que hayan , eso a partir de el juego de arreglos
                 * $key -> es el grupo
                 * $arregloEstudiante -> es el arreglo de estudiantes perteniecientes a ese grupo
                 */
                foreach ($GrupoConEstudiante as $key => $arregloEstudiante) {
                    /**
                     * $key -> pierde valor ya que solo es correlativo de estudiante en el arreglo
                     * $estu -> estudiante a insertar en la base
                     */
                    foreach ($arregloEstudiante as $key => $estu) {
                        $em->persist($estu);
                        $algo4 [] = $estu;
                    }
                }
                if ($this->Prod) $em->flush();
//            $this->addFlash('success', 'GrupoEstudainte  creados!');
            }


        }
        //else
//            $this->addFlash('error', 'Ya existe GrupoEstudainte!');
        if (!$this->Prod)
            return $this->render('rotacion/printWTF.twig', [
                "uno" => $algo12
                , "dos" => []//$algo5//  $mapaCupo["Institucion"]
                , "tres" => $algo4 // $grupoEstudiantesDel
                , "algoTable" => []// $algo3

            ]);
//        return 0;
    }

//*********************************************************************************************************************

    /**
     * Grupo estudiante ->
     * @Route("/rotacion_estudiantes_prev", name="rotacionEstudiantes")
     * @throws \Exception
     */
    public function rotacionEstudiantes()
    {
        //  $this->DqlUtilService->existeGrupoEnPeriodo();
        $AsigEstu = [];
        $CursosDeCicloPerido = $this->DqlUtilService->getCursoPeridoCiclo();

        $iterate = 0;
        //dd($CursosDeCicloPerido);
//        dd($this->periodo_service->getCicloActual()->getid());
        if (!$this->DqlUtilService->existeRotacionEnPeriodo()) {  // Oficial
            foreach ($CursosDeCicloPerido as $keyCDP => $valueCDP) {
                $iterate++;
//      if ( true) {  // para testing

                $this->materias = $valueCDP->getcodigo();

                $periodoCiclo = $this->periodo_service->getCicloActual()->getId();
                $periodoCicloAnterior = $this->periodo_service->getCicloAnterior($this->periodo_service->getCicloActual())->getId();
//dd($periodoCicloAnterior);
                $em = $this->getDoctrine()->getManager();

//+++++++++++++++++++++++++++++CREACION DE GRUPOS ++++++++++++++++++++++++++++++

                $ResumenCalendarizacion = $this->servicio_rotacion->obtenerResumenCalendarizacion();
                $planificacion = $this->servicio_rotacion->obtenerPlanificacionActual();
//          dd($ResumenCalendarizacion);
//        verificamos que los grupos existan sino los creamos s
//            dd($ResumenCalendarizacion);
                if (!$this->DqlUtilService->existeGrupoEnPeriodo()) {
                    foreach ($ResumenCalendarizacion as $value) {
                        $grupo = $em->getRepository('Rotacion:Grupo')->findOneBy(["nombre" => $value["rotacion"], "periodo_ciclo" => $periodoCiclo]);
//dd($grupo);
                        //Se guarda la rotacion y  a partir de los grupos -> no se guarda con las fechas correctas habra que corregirlar !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                        $em->persist(new Rotacion($grupo, $value["rotacion"], $planificacion, new \DateTime($value["inicio_cesa"]), new \DateTime($value["inicio_clc"])));
                    }
                    $em->flush();
                }
// fin del guardado de las grupo--------------------------------------------------
//============================================================================
                $GrupoEstudiante = [];
                $GrupoEstudianteUnicos = [];
// los estudiantes con su respectivo grupo -> gurpo1 tiene 16 / grupo2 -> 16
                if ($this->verificaMateriaRotacion($this->materias)) {
                    $GrupoEstudianteUnicos = $this->DqlUtilService->getEstudiantesRotablesUnicos(null, null, null, null, $this->materias);
                    $GrupoEstudiante = $this->DqlUtilService->getGrupoEstudiante(null, null, null, null, $this->materias);
//         dd($GrupoEstudiante);
                }


                $GrupoEstudiantePPE = $this->DqlUtilService->getGrupoEstudiante(null, null, "7", null, $this->materias);
//         dd($this->materias,$GrupoEstudiantePPE,$GrupoEstudiante);
//          $GrupoEstudianteDataTest[] = $this->materias;
//          $GrupoEstudianteDataTest[] = $GrupoEstudiante;


                $cuposInstitucion = $this->DqlUtilService->getCuposInstitucion();
                $rotacion = $this->DqlUtilService->getRotacion();
//           dd($rotacion , $GrupoEstudiante  );


//            dd($GrupoEstudiante);
//+++++++++++++++++++++++++++++++++++++++++++CLONACION de la rotacion anterior++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                $mapaCupos = $this->DqlUtilService->getCuposInstitucionConsolidado();
                /**
                 * CLONACION de la rotacion anterior
                 * ojo los nombre de los programa no pueden cambiar es decir 1,2,3,4,5,6 siempre
                 */

                $rotacionEstudianteAnterior = $this->DqlUtilService->getRotacionConsolidad($periodoCicloAnterior);
                //$GrupoEs3 = $em->getRepository("Rotacion:rtoacion")->find($valueEA["idGrupoEstudiante"] );
//      dd(  "actual"  , $GrupoEstudiante  , "vieja" , $rotacionEstudianteAnterior);
                if ($iterate == 2)
                    foreach ($GrupoEstudianteUnicos as $keyMRA => $valueMRA) {

                        $EstActual = $valueMRA->getEstudianteCurso()->getEstudiantePlanEstudio()->getEstudiante()->getid();
                        $algo10["valueMRA"] = ($valueMRA);
                        $algo10["EstActual"] = ($EstActual);

                        $rotacionEstudianteAnterior = $this->DqlUtilService->getRotacionConsolidad($periodoCicloAnterior, null, null, $EstActual);
//                dd($mapaCupos);
                        foreach ($rotacionEstudianteAnterior as $keyRAN => $valueRAN) {

                            $nombreGrupo = $valueRAN["nombreGrupo"];
                            $rotacionNueva = $this->DqlUtilService->getRotacion(null, $nombreGrupo)[0];
                            $algo10["rotacionNueva"][$nombreGrupo][] = ($rotacionNueva);


                            $idInstitucion = $valueRAN["programaInstitucion"];


                            $GrupoEstudiantePAST = $em->getRepository("Rotacion:GrupoEstudiante")->find($valueRAN["idGrupoEstudiante"]);
                            $grupoEstudiante = $GrupoEstudiantePAST->getGrupo()->getnombre();
                            $idEstudianteCursoUNI = $valueMRA->getEstudianteCurso()->getID();
                            $rotacionEstudiate = $this->DqlUtilService->getRotacion(null, $grupoEstudiante)[0];
                            $GrupoEstudianteUnic = $this->DqlUtilService->getGrupoEstudiante(null, $idEstudianteCursoUNI, $rotacionEstudiate->getGrupo()->getNombre());

                            $ProgramaInstitucion = $em->getRepository('Rotacion:ProgramaInstitucion')->find($idInstitucion);
//dd($valueRAN["programaInstitucion"],$planificacion);


                            if ((!empty($GrupoEstudianteUnic)) && (!empty($rotacionNueva) && (!empty($ProgramaInstitucion)))) {
                                $RotacionEstudiante = new RotacionEstudiante();
                                $RotacionEstudiante->setGrupoEstudiante($GrupoEstudianteUnic[0]);
//                                $RotacionEstudiante->setGrupoEstudiante($GrupoEstuObect[0]);
                                $RotacionEstudiante->setRotacion($rotacionNueva);
                                $RotacionEstudiante->setProgramaInstitucion($ProgramaInstitucion);

                                $AsigEstu[] = $RotacionEstudiante;
//                       if ($iterate=2) $algo10["ExM"][$iterate][0] = $RotacionEstudiante;
//


                                foreach ($GrupoEstudiante as $keyGEDE => $valueGEDE) {
                                    if ($valueGEDE->getID() == $GrupoEstudianteUnic[0]->getID()) {

                                        unset($GrupoEstudiante[$keyGEDE]);
//                                dd($valueGEDE, $GrupoEstudianteUnic[0]);

                                    }

                                }
                            }


                        }
                    }

//                $rotacionEstudianteAnterior2= $this->DqlUtilService->getRotacionConsolidad($periodoCicloAnterior,null,null, 165);
//
//             dd($valueMRA,$rotacionEstudianteAnterior,$rotacionEstudianteAnterior2);
//
//
//
//
//                foreach ($rotacionEstudianteAnterior as $keyEA => $valueEA) {
//                    $EstActual = $valueMRA->getEstudianteCurso()->getEstudiantePlanEstudio()->getEstudiante()->getid();
//                    $GruEstActual = $valueMRA->getGrupo()->getnombre();
//
//
////                    if(count($rotacionEstudianteActual) != 0)
//                    if (
//                       ($valueEA["idEstudiante"] == $EstActual)
////                     &&
////                       ($valueEA["codigoPrograma"] != "ppe")
//                    ) {
////                        $algo10["filtro"][$iterate][] = $EstActual;
//                        //obtengo los datos del estudiante de la rotacion pasada
////                        $GrupoEstudiantePAST = $this->DqlUtilService->getGrupoEstudiante($periodoCicloAnterior,null, null,null,null,486);
//                        $GrupoEstudiantePAST = $em->getRepository("Rotacion:GrupoEstudiante")->find($valueEA["idGrupoEstudiante"] );
//                        //      dd(  "valueEA"  , $valueEA  , "GrupoEstudiante" , $GrupoEstudiante);
//                        //este segmento superior solo deberia de traer un solo registro d
//
////                        if(count($GrupoEstudiantePAST)!=0)
////                        dd(  "GrupoEstudiantePAST",$GrupoEstudiantePAST,$EstActual,"actual"  , $valueMRA  , "vieja" , $valueEA);
//                        //recorro el objeto de grupoEstudiante
//
//
//                            $estudianteID = $GrupoEstudiantePAST->getEstudianteCurso()->getEstudiantePlanEstudio()->getEstudiante()->getid();
//                            $grupoEstudiante = $GrupoEstudiantePAST->getGrupo()->getnombre();
//
////                            dd($GrupoEstudiantePAST);
//                          //  $GrupoEstuObect = $this->DqlUtilService->getGrupoEstudiante(null, $estudianteID, $grupoEstudiante); // obtengo el grupoEstudiante a insertar desde los datos viejos
////                  $rotacionEstudiate = $em->getRepository("Rotacion:Rotacion")->findOneBy(["grupo" =>$valueGE->getGrupo()->getnombre(),"periodo_ciclo"=> ]);
//
//
//                        $idEstudianteCursoUNI = $valueMRA->getEstudianteCurso()->getID();
//
//
//                        $rotacionEstudiate = $this->DqlUtilService->getRotacion(null, $grupoEstudiante)[0];
//                            $programaInstituEstudiate = $em->getRepository("Rotacion:ProgramaInstitucion")->findOneBy(["institucion" => $valueEA["idInstitucion"]]);
//                            $GrupoEstudianteUnic = $this->DqlUtilService->getGrupoEstudiante(null,$idEstudianteCursoUNI,$rotacionEstudiate->getGrupo()->getNombre());
//
//
//                        if ((empty($GrupoEstudianteUnic)))
////                            dd($GrupoEstudiantePAST,$valueMRA,$idEstudianteCursoUNI,$rotacionEstudiate,$GrupoEstudianteUnic);
//
//
//
//                          //   dd($valueEA,   $rotacionEstudiate[0], $programaInstituEstudiate);
//                            if ((!empty($GrupoEstudianteUnic)) && (!empty($rotacionEstudiate) && (!empty($programaInstituEstudiate)))) {
//
//
//                                if ((!empty($GrupoEstudianteUnic)))
//                                if ($GrupoEstudianteUnic[0]->getEstudianteCurso()->getEstudiantePlanEstudio()->getEstudiante()->getCarnet() == "PL15002")
//                                {   $algo10 []=($valueMRA);
//                                    $algo10 [1]=($rotacionEstudiate);
//                                    $algo10 [2]=($GrupoEstudianteUnic);
//                               }
//
//                                $RotacionEstudiante = new RotacionEstudiante();
//                                $RotacionEstudiante->setGrupoEstudiante($GrupoEstudianteUnic[0]);
////                                $RotacionEstudiante->setGrupoEstudiante($GrupoEstuObect[0]);
//                                $RotacionEstudiante->setRotacion($rotacionEstudiate);
//                                $RotacionEstudiante->setProgramaInstitucion($programaInstituEstudiate);
//
//                                $AsigEstu[] = $RotacionEstudiante;
//
//                            }
//
//
//                        unset($GrupoEstudiante[$keyMRA]);
//
//                    }
//
//                }
////                break;
//              if ($iterate ==2 ){   $AsigEstu1=$AsigEstu; printf($this->materias);}

//            dd($GrupoEstudiante, $AsigEstu1);
                //  $GrupoEstudiante = array_values($GrupoEstudiante);

                $AsigEstuRespeto = $AsigEstu;

//====================================================================================================================================================================================

//         dd($AsigEstuRespeto,$GrupoEstudiante);
//dd($rotacion);
//+++++++++++++++++Este es el modulo que me organiza los estudiantes y me los divide en 2 grupos de 19 y 18 y a su vez este en grupos de tres
//            $GrupoEstudianteNuevos =    $this->DqlUtilService->getGrupoEstudiante($this->periodo_service->getCicloActual()->getid(),null,null,"= 'EST117");

//dd($GrupoEstudianteNuevos);


//            Justamente aca tenemos que reorganizar para que si solo hay 42 estudiantes
//            de igual manera los rote, por que ahorita solo saca dos grupos


//dd($GrupoEstudiante);
                $conta = 0;
                $GrupoEstudianteFormat = [];
                foreach ($GrupoEstudiante as $key => $value) { // AQUI inicia la distribucion de los estudiantes
                    // =============Verificar del commit         foreach ($GrupoEstudianteNuevos as $key => $value) {

                    if (isset($grupoAGuardar))
                        if ($grupoAGuardar != $value->getGrupo()->getNombre()) {
                            $conta = 0;
                        }
                    $conta++;
                    // print $conta;
                    if ($conta == 4) $conta = 1;

                    $grupoAGuardar = $value->getGrupo()->getNombre();
                    $GrupoEstudianteFormat[$grupoAGuardar][$conta][] = $value;
                }
//dd($GrupoEstudianteFormat);
//---------------------------------------

                foreach ($rotacion as $key => $value) {
                    $grupoAGuardar = $value->getNumero();
                    $rotacionFormat[$grupoAGuardar] = $value;
                }

// segmento donde se piensa guar la rotactionEstudiante  ++++++++++++++++++++++++++++++++++++++++++++++++++

//            $mapaCupos = $this->DqlUtilService->getCuposInstitucionConsolidado(); Original

                //  dd($GrupoEstudianteFormat,count($GrupoEstudianteFormat),$GrupoEstudiante);

                /**
                 * Inicio de toda la magia de distribucion en 6 Sin PPE
                 */
                // $AsigEstu = []; // no se debe de limpiar por que se definio logica arriba
//            if (count($GrupoEstudianteFormat) >= 6)
                if (true)
                    for ($keyGrupo = 1; $keyGrupo <= 6; $keyGrupo++) {
//                    dd($GrupoEstudiante);
                        if (array_key_exists($keyGrupo, $GrupoEstudianteFormat)) {
                            $grupoOrigen = $GrupoEstudianteFormat[$keyGrupo];
                            $algo [] = $GrupoEstudianteFormat[$keyGrupo];

//********csns*****************************csns*********************
                            $AsigEstu = array_merge($AsigEstu, $this->CrearRotacionEstudiente($mapaCupos, $grupoOrigen, $keyGrupo, $rotacionFormat, "csns"));

//******cesa******************************cesa**************
//
                            $keyGrupo2 = $keyGrupo;
                            if ($keyGrupo2 == 1) {
                                $keyGrupo2 = 2;
                            } elseif ($keyGrupo2 == 2) {
                                $keyGrupo2 = 3;
                            }// el primero es el grupo A o B (columna)
                            elseif ($keyGrupo2 == 3) {
                                $keyGrupo2 = 1;
                            }// el segundo es el g1,g2,g3..... (filas)
                            elseif ($keyGrupo2 == 4) {
                                $keyGrupo2 = 5;
                            } elseif ($keyGrupo2 == 5) {
                                $keyGrupo2 = 6;
                            } elseif ($keyGrupo2 == 6) {
                                $keyGrupo2 = 4;
                            }
                            $AsigEstu = array_merge($AsigEstu, $this->CrearRotacionEstudiente($mapaCupos, $grupoOrigen, $keyGrupo2, $rotacionFormat, "cesa"));


//*****hnr******************hnr************

                            if (($this->materias == "OPC517") || ($this->materias == "OPC617")) { // las unicas materias para las que funciona el HNR
                                $keyGrupo3 = $keyGrupo;
                                if ($keyGrupo3 == 1) {
                                    $keyGrupo3 = 3;
                                } elseif ($keyGrupo3 == 2) {
                                    $keyGrupo3 = 1;
                                } elseif ($keyGrupo3 == 3) {
                                    $keyGrupo3 = 2;
                                } elseif ($keyGrupo3 == 4) {
                                    $keyGrupo3 = 6;
                                } elseif ($keyGrupo3 == 5) {
                                    $keyGrupo3 = 4;
                                } elseif ($keyGrupo3 == 6) {
                                    $keyGrupo3 = 5;
                                }
                                $AsigEstu = array_merge($AsigEstu, $this->CrearRotacionEstudiente($mapaCupos, $grupoOrigen, $keyGrupo3, $rotacionFormat, "hnr"));

                            }
//********************ulc*******ulc************ulc******
                            $keyGrupo4 = $keyGrupo;
//            if($keyGrupo4 == 1){$keyGrupo4=1;}  // NO necesita por que es igual que le primero CSNS
//            elseif ($keyGrupo4 == 2){$keyGrupo4=1;}
//            elseif ($keyGrupo4 == 3){$keyGrupo4=2;}
//            elseif ($keyGrupo4 == 4){$keyGrupo4=6;}
//            elseif ($keyGrupo4 == 5){$keyGrupo4=2;}
//            elseif ($keyGrupo4 == 6){$keyGrupo4=2;}

                            $AsigEstu = array_merge($AsigEstu, $this->CrearRotacionEstudiente($mapaCupos, $grupoOrigen, $keyGrupo4, $rotacionFormat, "ulc"));


//****clc**************************clc**************************clc******
                            $g5 = $grupoOrigen;
                            $keyGrupo5 = $keyGrupo;
                            if ($keyGrupo5 > 3) {
                                $keyGrupo5 -= 3;
                            }
                            if ($keyGrupo5 == 1) {
                                $g5Double [$keyGrupo5] = array_merge($g5[1], $g5[2]);
                            }
                            if ($keyGrupo5 == 2) {
                                $g5Double [$keyGrupo5] = array_merge($g5[2], $g5[3]);
                            }
                            if ($keyGrupo5 == 3) {
                                $g5Double [$keyGrupo5] = array_merge($g5[3], $g5[1]);
                            }
                            if ($keyGrupo5 == 4) {
                                $g5Double [$keyGrupo5] = array_merge($g5[1], $g5[2]);
                            }
                            if ($keyGrupo5 == 5) {
                                $g5Double [$keyGrupo5] = array_merge($g5[2], $g5[3]);
                            }
                            if ($keyGrupo5 == 6) {
                                $g5Double [$keyGrupo5] = array_merge($g5[3], $g5[1]);
                            }
                            $AsigEstu = array_merge($AsigEstu, $this->CrearRotacionEstudiente($mapaCupos, $g5Double, $keyGrupo5, $rotacionFormat, "clc"));


//****ppe*********************ppe****************ppe***************
//            $g6= $grupoOrigen;
//            $keyGrupo6 = $keyGrupo;
//            if($keyGrupo6>3){$keyGrupo6-=3;}
//            if($keyGrupo6 == 1){ $g6Double [$keyGrupo6] = array_merge($g6[1], $g6[2], $g6[3]);}
//            if($keyGrupo6 == 2){ $g6Double [$keyGrupo6] = array_merge($g6[2], $g6[3], $g6[1]);}
//            if($keyGrupo6 == 3){ $g6Double [$keyGrupo6] = array_merge($g6[3], $g6[1], $g6[2]);}
//            if($keyGrupo6 == 4){ $g6Double [$keyGrupo6] = array_merge($g6[3], $g6[1], $g6[2]);}
//            if($keyGrupo6 == 5){ $g6Double [$keyGrupo6] = array_merge($g6[3], $g6[1], $g6[2]);}
//            if($keyGrupo6 == 6){ $g6Double [$keyGrupo6] = array_merge($g6[3], $g6[1], $g6[2]);}
//            $AsigEstu = array_merge( $AsigEstu,$this->CrearRotacionEstudiente($mapaCupos,$g6Double,$keyGrupo6,$rotacionFormat,"ppe"));

//***************************************************
                        }
                    }
                /**
                 * Segemento donde se crea la distribucion de ppe
                 */

                //Segmento donde se crea la Rotacion de Los estudiantes PPE /-> No necesita modificarse
                $rotacionPPE = $this->CrearRotacionEstudientePPE($mapaCupos, $GrupoEstudiantePPE, 7, $rotacionFormat, "ppe");

                $AsigEstu = array_merge($AsigEstu, $rotacionPPE);

//dd($AsigEstu);

                /**
                 * Fin de toda la magia de distribuccion
                 */

                //   dd($GrupoEstudiante,$AsigEstuRespeto,$AsigEstu);


                if ($this->Prod) $this->DqlUtilService->saveArray($AsigEstu);
//---------------------------Fin del guardado de la tabla rotactionEstudiante---------------------------------

            }
        }


        if (!$this->Prod) return $this->render('rotacion/printWTF.twig', [
            "uno" => []//$algo10// $GrupoEstudianteDataTest //[]
            , "dos" => []//`$GrupoEstudiantePPE//  $mapaCupo["Institucion"]
            , "tres" => []//$AsigEstu[0]
            , "algoTable" => $AsigEstu// $algo3

        ]);
//return 0;
    }

//*********************************FUNCIONESS LOCALES******************************************

    /**
     * @param array $mapaCupos
     * @param array $g3
     * @param int $keyGrupo3
     * @param array $rotacionFormat
     * @param string $programa
     * @return array
     * @throws \Exception
     */
    private function CrearRotacionEstudiente(array $mapaCupos, array $g3, int $keyGrupo3, array $rotacionFormat, string $programa)
    {
        $RotacionEstudiante = [];
        if ($keyGrupo3 > 3) {
            $keyGrupo3 -= 3;
        }
// dd($g3 , $keyGrupo3);
        foreach ($mapaCupos as $keymap => $mapaCupo) {

            if ($keymap == $programa) {
                foreach ($g3 as $keyestudiante => $estudiante) {
                    while (count($g3[$keyGrupo3]) != 0) {
                        foreach ($mapaCupo["Institucion"] as $keyInstitucion => $cupoInstitucion) {
                            // $algo4[] = count($g3[1]);
                            if (count($g3[$keyGrupo3]) != 0) {
                                if ($mapaCupo["Institucion"][$keyInstitucion]["cupo"] > 0) {// validamos que haya cupo para ser asiganado
                                    $grupoEstu = array_pop($g3[$keyGrupo3]);
                                    $RotacionEstudiante[] = new RotacionEstudiante($grupoEstu, $rotacionFormat[$grupoEstu->getGrupo()->getNombre()], $cupoInstitucion["ProgramaInstitucion"]);
                                    $mapaCupo["Institucion"][$keyInstitucion]["cupo"] -= 1;
                                }
                            }
                        }
                    }
                }
            }
        }
        return $RotacionEstudiante;
    }

    /**
     * Este a parte de generar la rotacion para el ppe ,hace los grupos
     * @param array $mapaCupos
     * @param array $g3 arreglo [1] GrupoEstudiantes
     * @param int $keyGrupo3 llave distintica
     * @param array $rotacionFormat
     * @param string $programa
     * @return array
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    private function CrearRotacionEstudientePPE(array $mapaCupos, array $g3, int $keyGrupo3, array $rotacionFormat, string $programa)
    {
        $RotacionEstudiante = [];

// dd( $mapaCupos,  $g3,  $keyGrupo3,  $rotacionFormat,  $programa);
        foreach ($mapaCupos as $keymap => $mapaCupo) {


            foreach ($g3 as $keyestudiante => $estudiante) {
                while (count($g3) != 0) {
                    foreach ($mapaCupo["Institucion"] as $keyInstitucion => $cupoInstitucion) {
                        //  $algo4[] = count($g3[1]);
                        if (count($g3) != 0) {
                            if ($mapaCupo["Institucion"][$keyInstitucion]["cupo"] > 0) {// validamos que haya cupo para ser asiganado
                                $grupoEstu = array_pop($g3);
                                $RotacionEstudiante[] = new RotacionEstudiante($grupoEstu, $rotacionFormat[7], $cupoInstitucion["ProgramaInstitucion"]);
                                $mapaCupo["Institucion"][$keyInstitucion]["cupo"] -= 1;
                            }
                        }
                    }
                }
            }

        }

//
//        $instituRotacioEstudia = null;
//
//        //Organizo un arreglo con las instituciones que continenen estudiantes, para luego hacer un registro de grupo por cada una
//        foreach ($RotacionEstudiante as $keymapRE => $valueRE) {
//            $GrupEstuCodiInstitu = $valueRE->getProgramaInstitucion()->getInstitucion()->getCodigo();
//            $instituRotacioEstudia[$GrupEstuCodiInstitu][] = $valueRE;
//        }
//        //dd($instituRotacioEstudia);
////++++++++++++++++++++++++++++++Guarda en la base a partir de las instituciones ubicadas en el segnmento anterior
//        $count = 0;
//        foreach ($instituRotacioEstudia as $keymapGIT => $valueGIT) {
//            $count++;
//            $GrupEstuCodiCurso = $valueGIT[0]->getGrupoEstudiante()->getEstudianteCurso()->getCurso()->getCodigo();
//            $InstituEstuNombre = $valueGIT[0]->getProgramaInstitucion()->getInstitucion ()->getnombre();
//            $GrupEstuCodigrupo = $valueGIT[0]->getGrupoEstudiante()->getGrupo();
//            $nombreGrupo = substr($GrupEstuCodiCurso, 0, 20) . "/G". $count. " - ".$InstituEstuNombre ;
//
////            dd($keymapGIT,$valueGIT,$instituRotacioEstudia, $nombreGrupo);
//            $grupoPPE = new Grupo($GrupEstuCodigrupo, $this->periodo_service->getCicloActual(), $nombreGrupo, false,true);
//            $grupoPPE->setCalificable(true);
//            $gruposPPEfinales[] = $grupoPPE;
////        dd($valueGIT);
//
//
////+++++++++++++++++++++++++++++++++++++++++++segemento donde guardo en grupo_estudiante
//// id_estudiante_curso  id_grupo
//            foreach ($valueGIT as $keymapGRE => $valueGRE) {
////              ya tenemos al grupo $grupoPPE
//                $GrupEstuCurso = $valueGRE->getGrupoEstudiante()->getEstudianteCurso();
//
//                $GrupoConEstudiante[] = new GrupoEstudiante($GrupEstuCurso, $grupoPPE, new \DateTime('now'));
//            }
////-Fin  ----------  grupo_estudiante-------------------------------
//        }
//
////        dd($GrupoConEstudiante);
//        if ($this->Prod) $this->DqlUtilService->saveArray($gruposPPEfinales);// primero se deben de guardar el grupo
//        if ($this->Prod) $this->DqlUtilService->saveArray($GrupoConEstudiante);// primero se deben de guardar el grupo
//
//
////- Fin----------------------------------------------------------------------------------------------------
       // $this->DqlUtilService->CrearSubGruposPPE();

//dd($instituRotacioEstudia, $algo3, $algo4);

        return ($RotacionEstudiante);
    }

    /**
     * @param array|null $arreglo
     * @param string|null $clave1
     * @param string|null $valor1
     * @param string|null $clave2
     * @param string|null $valor2
     * @return null
     */
    private function buscarValorEnArray(array $arreglo = null, string $clave1 = null, string $valor1 = null,
                                        string $clave2 = null, string $valor2 = null)
    {
        $resultado = null;

        if (!is_null($clave2)) {
            foreach ($arreglo as $key => $value) {
                if (($value[$clave1] == $valor1) && ($value[$clave2] == $valor2)) { //validamos que los dos parametros esten
                    $resultado[] = $value;// le asignamos el valor
                }
            }
        } else {
            foreach ($arreglo as $key => $value) {
                if (($value[$clave1] == $valor1)) { //validamos que los dos parametros esten
                    $resultado[] = $value;// le asignamos el valor
                }
            }
        }

        return ($resultado);

    }

    /**
     * estas materias solo rotan en el ciclo 4 - 8 en el pp, por lo que se deberian de solo agregar aquellas que no ro-
     * taran en todos los programas
     * @param String $materia
     * @return bool
     */
    private function verificaMateriaRotacion(String $materia)
    {

        if (
            ($materia != "EST417")
            && ($materia != "OPC117")
            && ($materia != "OPC217")
            && ($materia != "OPC317")
            && ($materia != "OPC417")

        ) {
            return true;
        } else
            return false;
    }

    /**
     * funcion en contruccion que me eliminaria toda una rotacion hasta, pero no estoy seguro que sea necesario los grupos
     * @param int|null $periodoCiclo
     * @throws \Exception
     */
    private function eliminarGrupos(int $periodoCiclo = null)
    {
        /**
         * segmento donde borro la agrupacion de estudiantes actual ,Valido que no exista paa crearlo
         */
        if (is_null($periodoCiclo)) $periodoCiclo = $this->periodo_service->getCicloActual()->getid();

        if ($this->DqlUtilService->existeGrupoEstudianteEnPeriodo()) {
            $grupoEstudiantesDel = $this->em->createQuery(
                'SELECT grup FROM Rotacion:GrupoEstudiante grup, Rotacion:Grupo c
                where grup.grupo = c.id and c.periodo_ciclo =' . $periodoCiclo
            )->getSQL();

            dd($grupoEstudiantesDel);
            foreach ($grupoEstudiantesDel as $key => $value) {
                $this->em->remove($value);
            }
//                dd($this->DqlUtilService->existeGrupoEstudianteEnPeriodo(),$CursosDeCicloPerido);
//                if ($this->Prod) $em->flush();
        }
    }


    /**
     * @Route("/cupos_rotacion", name="cupos_rotacion")
     */
    public function indexCuposRotacion()
    {

        $em = $this->getDoctrine()->getManager();

        $em->beginTransaction();

        try {
            $db = $em->getConnection();
            $query = "select *
                    from rotacion.vw_cupos_por_rotacion
                    where cupos_max-activos>1";
            $stmt = $db->prepare($query);
            //dd($periodoCicloID,$id_programa,$id_rotacion);
            // $params = array('periodo_ciclo'=>$periodoCicloID,'programa'=>$id_programa,'rotacion'=>$id_rotacion);
            $stmt->execute();
            $result = $stmt->fetchAll();
            $em->rollBack();

//            dd($ciclos);

        } catch (\Exception $e) {
            $em->rollBack();
            throw $e;
        }

        foreach ($result as $key => $value) {

            $intitu[$value["programa"]] = $value;

        }
//dd($intitu);
//        $cuposPrograma =   json_encode($result);
//        dd($cuposPrograma);
        return $this->render('rotacion/cupos_institu_rotacion.twig', [
                "cuposPrograma" => $result
                , "intitu" => $intitu


            ]
        );

    }


    /**
     * @Route("/eliminar_rotacion",name="eliminar_rotacion")
     */
    public function EliminarRotacion(int $periodoCiclo = null)
    {
        $periodoCiclo = $this->periodo_service->getCicloActual()->getid();
        $this->DqlUtilService->EliminarRot($periodoCiclo);
        $this->addFlash('success', 'Rotación eliminada');

        return $this->render('default.html.twig' );
    }




}