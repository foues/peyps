<?php

namespace App\Controller\Rotacion;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Service\DqlRotacionUtilService;
use App\Service\RotacionService;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/rotacion_intercambio")
 */
class RotacionIntercambio extends AbstractController
{
    /**
     * @var RotacionService
     */
    private $DqlUtilService;
    private $servicio_rotacion;
    private $gruposRotables = 6;

    /**
     * RotacionPreliminarController constructor.
     */
    public function __construct(ContainerInterface $container, DqlRotacionUtilService $DqlUtilService, RotacionService $servicio_rotacion)
    {
        $this->container = $container;
        $this->denyAccessUnlessGranted('rotacion_intercambio');
        $this->DqlUtilService = $DqlUtilService;
        $this->servicio_rotacion = $servicio_rotacion;
    }

    /**
     * @Route("/grupo/{grupoEstudiante}/programa/{programaIsntitucion}/rotacion/{id_rotacion}/programa/{id_programa}", name="intercambio")
     */
    public function indexRotacionPre($grupoEstudiante = null, $programaIsntitucion = null, $id_rotacion = null, $id_programa = null)
    {
        //dd($id_rotacion,$id_programa);
        $estudianteSelecto = $this->DqlUtilService->getRotacionConsolidad(null, $grupoEstudiante, $programaIsntitucion);
        $rotacionEstudianteLocal = $this->DqlUtilService->getRotacionConsolidad(null, null, null, null, $id_programa, $id_rotacion);
        $instituciones = $this->DqlUtilService->executeNativeSql("SELECT DISTINCT idPrograma,codigoPrograma,nombrePrograma FROM rotacion.institucion_programa");
        $gruposRotables = $this->DqlUtilService->getRotacion();
        $institucionesCupos = $this->servicio_rotacion->obtenerCuposInstitucion(null, $id_programa, $id_rotacion);
        //dd($institucionesCupos);

        return $this->render('rotacion/rotacion_intercambio.twig', [
            "estudiante" => $rotacionEstudianteLocal
            , "rotacioEstudianteLocal" => $rotacionEstudianteLocal
            , "estudianteSelecto" => $estudianteSelecto[0]
            , "instituciones" => $instituciones
            , "gruposRotables" => $gruposRotables
            , "institucionesCupos" => $institucionesCupos

        ]);
    }

    /**
     * @Route("/procesarIntercambio",name="p_intercambio")
     */
    public function procesarIntercambio(Request $request)
    {
        if ($request->isMethod("POST")) {
            $em = $this->getDoctrine()->getManager();

            $estudiante1 = $em->getRepository("Rotacion:RotacionEstudiante")
                ->findOneBy(array("grupo_estudiante" => $em->getRepository("Rotacion:GrupoEstudiante")->find($request->get("estudiante1"))));
            $estudiante2 = $em->getRepository("Rotacion:RotacionEstudiante")
                ->findOneBy(array("grupo_estudiante" => $em->getRepository("Rotacion:GrupoEstudiante")->find($request->get("estudiante2"))));
            //intercambio
            $institucion_intercambiable = $estudiante1->getProgramaInstitucion();
            //dump($estudiante1,$estudiante2);
            $estudiante1->setProgramaInstitucion($estudiante2->getProgramaInstitucion());
            $estudiante2->setProgramaInstitucion($institucion_intercambiable);
            //dump($estudiante1,$estudiante2);
            $em->flush();

            $this->addFlash('sucess', 'Intercambio Existoso');
            return $this->redirectToRoute('rotacion_preliminar');
        }
    }

    /**
     * @Route("/procesarMovimiento",name="p_movimiento")
     */
    public function procesarMovimiento(Request $request)
    {
        if ($request->isMethod("POST")) {
            $em = $this->getDoctrine()->getManager();

            $estudiante = $em->getRepository("Rotacion:RotacionEstudiante")
                ->findOneBy(array("grupo_estudiante" => $em->getRepository("Rotacion:GrupoEstudiante")->find($request->get("estudiante_grupo"))));
            //Determinando nueva Institucion
            $programaInstitucion = $em->getRepository("Rotacion:ProgramaInstitucion")->findOneBy(
                array(
                    'activo' => 1,
                    'institucion' => $em->getRepository("Administracion:Institucion")->find($request->get("nva_institucion"))
                )
            );
            $estudiante->setProgramaInstitucion($programaInstitucion);
            $em->flush();
            $this->addFlash('sucess', 'Intercambio Existoso');
            return $this->redirectToRoute('rotacion_preliminar');
        }
    }

}