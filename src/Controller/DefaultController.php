<?php

namespace App\Controller;

use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use App\Service\PeriodoService;


class DefaultController extends AbstractController
{

    /**
     * @var PeriodoService
     */
    private $periodo_service;

    /**
     * @var SessionInterface
     */
    private $session;

    /**
     * DefaultController constructor.
     * @param PeriodoService $periodo_service
     * @param SessionInterface $session
     */
    public function __construct(PeriodoService $periodo_service, SessionInterface $session)
    {
        $this->periodo_service = $periodo_service;
        $this->session = $session;
    }

    /**
     * @Route("/", name="/")
     * @throws Exception
     */
    public function default()
    {
        $em = $this->getDoctrine()->getManager();

        $periodoCiclo = $this->periodo_service->getCicloActual()->getPeriodo()->getNombre();
        $this->session->set('periodoCiclo', $periodoCiclo);

        if (empty($this->session->get('menu'))) {
            $menu = $em->getRepository('Administracion:Menu')->getMenuJerarquicoByUsuario($this->getUser());
            $this->session->set('menu', $menu);
        }
        return $this->render('default.html.twig');
    }
}
