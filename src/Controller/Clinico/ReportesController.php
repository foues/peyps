<?php

namespace App\Controller\Clinico;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Clinico\Ficha;
use App\Entity\Clinico\ExpedienteClinico;
use App\Service\Clinico\TablaFichaService;
use App\Service\Clinico\ReportesService;

class ReportesController extends AbstractController
{

  private $tablaFichaService;
  private $reportesService;

    /**
     * ReportesController constructor.
     * @param ContainerInterface $container
     * @param TablaFichaService $tablaFichaService
     * @param ReportesService $reportesService
     */
    public function __construct(ContainerInterface $container, TablaFichaService $tablaFichaService, ReportesService $reportesService)
    {
        $this->container = $container;
        $this->denyAccessUnlessGranted('reporte_clinico');
        $this->tablaFichaService = $tablaFichaService;
        $this->reportesService = $reportesService;
    }

  /**
   * @Route("/clinico/expedientes/{expedienteId}/fichas/{fichaId}/reportes", name="reportes", methods="GET")
   */
  public function show(ExpedienteClinico $expedienteId, $fichaId): Response
  {
    $fichaInfo = $this->tablaFichaService->getTableInfo($expedienteId->getId(), $fichaId);

    return $this->render('clinico/reportes/list.html.twig', [
      'expedienteId' => $expedienteId->getId(),
      'expediente' => $expedienteId,
      'fichaId' => $fichaId,
      'fichaInfo' => $fichaInfo,
    ]);
  }

  /**
   * @Route("/clinico/expedientes/{expedienteId}/fichas/{fichaId}/reporte_indices", name="reporte_indices", methods="GET")
   */
  public function indices($fichaId, $expedienteId, Request $request): Response
  {
    $criterio =  $request->query->get('criterio');
    $em = $this->getDoctrine()->getManager();
    $usuario = $this->getUser();
    $ficha = $em->getRepository(Ficha::class)->findOneBy(['id' => $fichaId]);
    $expediente = $em->getRepository(ExpedienteClinico::class)->findOneBy(['id' => $expedienteId]);

    if($criterio!='ICDAS' && $criterio!='OMS') $criterio = 'ICDAS';

    if(is_null($ficha) || is_null($expediente)){
      $this->addFlash('warning', 'Solicitud incorrecta');
      return $this->redirectToRoute('expedientes');
    }

    $fichaInfo = $this->tablaFichaService->getTableInfo($expedienteId, $fichaId);

    $cod_criterio = 0;
    if($criterio=='OMS') $cod_criterio = 1;

    $indices = $this->reportesService->getIndices($fichaId, $cod_criterio);


    return $this->render('clinico/reportes/indices.html.twig', [
        'fichaInfo' => $fichaInfo,
        'indices' => $indices,
        'fichaId' => $fichaId,
        'expedienteId' => $expedienteId,
        'expediente' => $expediente,
        'criterio' => $criterio
      ]);

  }

  /**
   * @Route("/clinico/expedientes/{expedienteId}/fichas/{fichaId}/reporte_tratamientos", name="reporte_tratamientos", methods="GET")
   */
  public function tratamientos($fichaId, $expedienteId, Request $request)
  {
    $criterio =  $request->query->get('criterio');
    $em = $this->getDoctrine()->getManager();
    $usuario = $this->getUser();
    $ficha = $em->getRepository(Ficha::class)->findOneBy(['id' => $fichaId]);
    $expediente = $em->getRepository(ExpedienteClinico::class)->findOneBy(['id' => $expedienteId]);

    if($criterio!='ICDAS' && $criterio!='OMS') $criterio = 'ICDAS';

    if(is_null($ficha) || is_null($expediente)){
    $this->addFlash('warning', 'Solicitud incorrecta');
      return $this->redirectToRoute('expedientes');
    }

    $fichaInfo = $this->tablaFichaService->getTableInfo($expedienteId, $fichaId);

    $cod_criterio = 0;
    if($criterio=='OMS') $cod_criterio = 1;

    $tratamientos = $this->reportesService->getNecesidadesTratamiento($fichaId, $cod_criterio);

    return $this->render('clinico/reportes/tratamientos.html.twig', [
        'fichaInfo' => $fichaInfo,
        'tratamientos' => $tratamientos,
        'fichaId' => $fichaId,
        'expedienteId' => $expedienteId,
        'expediente' => $expediente,
        'criterio' => $criterio
      ]);
  }

  /**
   * @Route("/clinico/expedientes/{expedienteId}/fichas/{fichaId}/reporte_severidades", name="reporte_severidades", methods="GET")
   */
  public function severidades($fichaId, $expedienteId, Request $request)
  {

    $em = $this->getDoctrine()->getManager();
    $usuario = $this->getUser();
    $ficha = $em->getRepository(Ficha::class)->findOneBy(['id' => $fichaId]);
    $expediente = $em->getRepository(ExpedienteClinico::class)->findOneBy(['id' => $expedienteId]);

    if(is_null($ficha) || is_null($expediente)){
      $this->addFlash('warning', 'Solicitud incorrecta');
      return $this->redirectToRoute('expedientes');
    }

    $fichaInfo = $this->tablaFichaService->getTableInfo($expedienteId, $fichaId);


    $severidades = $this->reportesService->getSeveridades($fichaId);

    return $this->render('clinico/reportes/severidades.html.twig', [
        'fichaInfo' => $fichaInfo,
        'severidades' => $severidades,
        'fichaId' => $fichaId,
        'expedienteId' => $expedienteId,
        'expediente' => $expediente,
      ]);
  }

}
