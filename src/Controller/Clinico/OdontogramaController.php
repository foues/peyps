<?php

namespace App\Controller\Clinico;

use App\Entity\Clinico\ExpedienteClinico;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Clinico\Ficha;
use App\Entity\Clinico\RiesgoDetalle;
use App\Entity\Clinico\FichaCatalogo;
use App\Entity\Clinico\Tratamiento;
use App\Entity\Clinico\EvaluacionDental;
use App\Entity\Clinico\Lesion;
use App\Form\Clinico\riesgos\CrearRiesgo;
use App\Service\Clinico\TablaFichaService;
use App\Service\Clinico\RiesgoService;

class OdontogramaController extends AbstractController
{

  private $tablaFichaService;

    /**
     * OdontogramaController constructor.
     * @param ContainerInterface $container
     * @param TablaFichaService $tablaFichaService
     * @param RiesgoService $riesgoService
     */
    public function __construct(ContainerInterface $container, TablaFichaService $tablaFichaService, RiesgoService $riesgoService)
    {
        $this->container = $container;
        $this->denyAccessUnlessGranted('odontograma');
        $this->tablaFichaService = $tablaFichaService;
        $this->riesgoService = $riesgoService;
    }

  /**
   * @Route("/clinico/expedientes/{expedienteId}/fichas/{fichaId}/icdas", name="odontograma_icdas", methods="GET")
   */
  public function icdas(ExpedienteClinico $expedienteId, $fichaId): Response
  {
      $em = $this->getDoctrine()->getManager();

      $f_repository = $em->getRepository(FichaCatalogo::class);
      $cavities = $f_repository->findByCodigoTipo('icdas_caries');
      $restorations = $f_repository->findByCodigoTipo('icdas_restauracion');
      $fullmouth_states = $f_repository->findByCodigoTipo('icdas_estados_bc');
      $complete_states = $f_repository->findByCodigoTipoAndCodigo('icdas_estados', '9_');

      $t_repository = $em->getRepository(Tratamiento::class);
      $treatments = $t_repository->findAll();

      $fichaInfo = $this->tablaFichaService->getTableInfo($expedienteId->getId(), $fichaId);

      return $this->render('clinico/odontogramas/icdas.html.twig', [
        'expedienteId' => $expedienteId->getId(),
        'expediente' => $expedienteId,
        'fichaId' => $fichaId,
        'fichaInfo' => $fichaInfo,
        'odontogram' => $this->getOdontogram($fichaId),
        'bc' => $this->getToothObject($fichaId, 'BC'),
        'cavities' => $cavities,
        'restorations' => $restorations,
        'complete_states' => $complete_states,
        'fullmouth_states' => $fullmouth_states,
        'treatments' => $treatments,
        'allowEdit' => $this->getUser()->getCuenta()->getId() === $fichaInfo['id'] && !$fichaInfo['finalizada'] ? 1 : 0
      ]);
  }

  /**
   * @Route("/clinico/tratamientos/{state}", name="tratamientos_estado", methods="GET")
   */
  public function getTratamientosByState($state): Response
  {
      $em = $this->getDoctrine()->getManager();
      $repository = $em->getRepository(Tratamiento::class);
      $treatments = $repository->findByState($state);
      return $this->json($treatments);
  }

  /**
   * @Route("/clinico/tratamientos/byName/{name}", name="tratamientos_nombre", methods="GET")
   */
  public function getTratamientosByName($name): Response
  {
      $em = $this->getDoctrine()->getManager();
      $repository = $em->getRepository(Tratamiento::class);
      $treatments = $repository->findByName($name);
      return $this->json($treatments);
  }


  /**
   * @Route("/clinico/icdas/nuevo", name="icdas_nuevo", methods="POST")
   */
  public function create_icdas(Request $request): Response
  {
      $em = $this->getDoctrine()->getManager();

      $fichaId = $request->request->get('fichaId');
      $tooth =  $request->request->get('tooth');
      $treatments = $request->request->get('treatments', array() );

      $keeps = array();

      $stored_treatments = $this->getTreatmentsByTooth($fichaId, $tooth);

      foreach($treatments as $treatment){
        if($treatment['id'] != "") array_push($keeps, $treatment['id']);
        else{
          $surface = $treatment['surface'];
          $state = $treatment['state'];
          $need = $treatment['need'];

          $evaluation = $this->createEvaluation($fichaId, $tooth, $surface, $state, $need);

          $em->persist($evaluation);
        }
      }

      foreach ($stored_treatments as $store_treatment){

        if(!in_array(strval($store_treatment->getId()), $keeps)){
          $em->remove($store_treatment);
        }
      }

      $em->flush();

      /*Recalcular indice de riesgo cariogenico */
      $ficha = $em->getRepository(Ficha::class)->findOneBy(['id' => $fichaId]);
      $riesgoDetalle = $em->getRepository(RiesgoDetalle::class)->findOneBy(['ficha' => $ficha]);

      if(!is_null($riesgoDetalle)){
        $riesgo = new CrearRiesgo();
        $this->riesgoService->getIndices($riesgo, $fichaId);
        $total_caries = $this->riesgoService->calcInd($riesgo);
        $total_placa = $riesgoDetalle->getTotalPlacaBacteriana();
        $total_azucar = $riesgoDetalle->getTotalIngestaAzucares();
        $diagnostico = $this->riesgoService->calcRisk($total_caries, $total_placa, $total_azucar);

        $riesgoDetalle->setTotalIndiceCpoCeo($total_caries);
        $tipo_riesgo = $em->getRepository(FichaCatalogo::class)->findOneBy(['nombre' => $diagnostico]);
        $ficha->setIndiceRiesgoCariogenico($tipo_riesgo);
      }

      $em->flush();

      $tooth_obj = $this->getToothObject($fichaId, $tooth);

      $response = array(
        'number' => $tooth_obj->number,
        'ids' => $tooth_obj->ids,
        'states' => $tooth_obj->states,
        'surfaces' => $tooth_obj->surfaces,
        'needs' => $tooth_obj->needs,
        'class' => $tooth_obj->class
      );

      return $this->json($response);
  }

  private function getOdontogram($fichaId): array
  {
    $fila1 = [55, 54, 53, 52, 51, 61, 62, 63, 64, 65];
    $fila2 = [18, 17, 16, 15, 14, 13, 12, 11, 21, 22, 23, 24, 25, 26, 27, 28];
    $fila3 = [48, 47, 46, 45, 44, 43, 42, 41, 31, 32, 33, 34, 35, 36, 37, 38];
    $fila4 = [85, 84, 83, 82, 81, 71, 72, 73, 74, 75];

    $odontogram_numbers = [$fila1, $fila2, $fila3, $fila4];

    $odontogram = array();

    foreach($odontogram_numbers as $row){
      $odontogram_row = array();
      foreach($row as $tooth){
        array_push($odontogram_row, $this->getToothObject($fichaId, $tooth));
      }

      array_push($odontogram, $odontogram_row);
    }

    return $odontogram;
  }

  private function getTreatmentsByTooth($fichaId, $tooth){
    $em = $this->getDoctrine()->getManager();

    $ed_repository = $em->getRepository(EvaluacionDental::class);
    $f_repository = $em->getRepository(Ficha::class);
    $fc_repository = $em->getRepository(FichaCatalogo::class);

    $ficha = $f_repository->find($fichaId);
    return $ed_repository->findBy(['ficha'=>$ficha, 'pieza_dental'=>$tooth]);
  }

  private function getToothObject($fichaId, $tooth){
    $treatments = $this->getTreatmentsByTooth($fichaId, $tooth);

    $tooth_obj = (object) array(
      'number' => $tooth,
      'ids' => "",
      'states' => "",
      'surfaces' => "",
      'needs' => "",
      'class' => ""
    );

    foreach($treatments as $treatment){
      $tooth_obj->ids .= $treatment->getId() . " ";
      $tooth_obj->surfaces .= $treatment->getSuperficieDental() . " ";
      $tooth_obj->states .= $treatment->getPiezaDentalEstado()->getCodigo() . " ";
      $tooth_obj->needs .= $treatment->getTratamiento()->getId() . " ";
      $tooth_obj->class .= $treatment->getPiezaDentalEstado()->getDescripcion() . " ";
    }

    return $tooth_obj;
  }

  private function createEvaluation($fichaId, $tooth, $surface, $state, $need){
    $em = $this->getDoctrine()->getManager();

    $fc_repository = $em->getRepository(FichaCatalogo::class);
    $f_repository = $em->getRepository(Ficha::class);
    $t_repository = $em->getRepository(Tratamiento::class);

    $ficha = $f_repository->find($fichaId);
    if(strlen($state) == 2)
      $estado = $fc_repository->findOneByCodigoTipoAndCodigo('icdas_estados', $state);
    else
      $estado = $fc_repository->findOneByCodigoTipoAndCodigo('icdas_estados_bc', $state);
    $tratamiento = $t_repository->find($need);

    $evaluation = new EvaluacionDental();
    $evaluation->setFicha($ficha);
    $evaluation->setPiezaDental($tooth);
    $evaluation->setSuperficieDental($surface);
    $evaluation->setPiezaDentalEstado($estado);
    $evaluation->setTratamiento($tratamiento);

    return $evaluation;
  }





  /**
   * @Route("/clinico/expedientes/{expedienteId}/fichas/{fichaId}/lesiones", name="odontograma_lesiones", methods="GET")
   */
  public function lesiones(ExpedienteClinico $expedienteId, $fichaId): Response
  {
    $fichaInfo = $this->tablaFichaService->getTableInfo($expedienteId->getId(), $fichaId);
    $injuries = $this->getInjuries();
    $odontogram = $this->getOdontogramInjury($fichaId);

    return $this->render('clinico/odontogramas/lesiones.html.twig', [
      'expedienteId' => $expedienteId->getId(),
      'expediente' => $expedienteId,
      'fichaId' => $fichaId,
      'fichaInfo' => $fichaInfo,
      'injuries' => $injuries,
      'odontogram' => $odontogram,
      'allowEdit' => $this->getUser()->getCuenta()->getId() === $fichaInfo['id'] && !$fichaInfo['finalizada'] ? 1 : 0
    ]);
  }

  private function getInjuriesByTooth($fichaId, $tooth){
    $em = $this->getDoctrine()->getManager();

    $l_repository = $em->getRepository(Lesion::class);
    $f_repository = $em->getRepository(Ficha::class);

    $ficha = $f_repository->find($fichaId);
    return $l_repository->findBy(['ficha'=>$ficha, 'pieza_dental'=>$tooth]);
  }

  private function getToothObjectInjury($fichaId, $tooth){
    $injuries = $this->getInjuriesByTooth($fichaId, $tooth);

    $tooth_obj = (object) array(
      'number' => $tooth,
      'ids' => "",
      'injuries' => ""
    );

    foreach($injuries as $injury){
      $tooth_obj->ids .= $injury->getId() . " ";
      $tooth_obj->injuries .= $injury->getLesionValorTipo()->getId() . " ";
    }

    return $tooth_obj;
  }

  private function getOdontogramInjury($fichaId): array
  {
    $fila1 = [55, 54, 53, 52, 51, 61, 62, 63, 64, 65];
    $fila2 = [18, 17, 16, 15, 14, 13, 12, 11, 21, 22, 23, 24, 25, 26, 27, 28];
    $fila3 = [48, 47, 46, 45, 44, 43, 42, 41, 31, 32, 33, 34, 35, 36, 37, 38];
    $fila4 = [85, 84, 83, 82, 81, 71, 72, 73, 74, 75];

    $odontogram_numbers = [$fila1, $fila2, $fila3, $fila4];

    $odontogram = array();

    foreach($odontogram_numbers as $row){
      $odontogram_row = array();
      foreach($row as $tooth){
        array_push($odontogram_row, $this->getToothObjectInjury($fichaId, $tooth));
      }

      array_push($odontogram, $odontogram_row);
    }

    return $odontogram;
  }

  private function getInjuries()
  {
      $em = $this->getDoctrine()->getManager();
      $fc_repository = $em->getRepository(FichaCatalogo::class);

      $injuries_types = $fc_repository->findByCodigoLike('lesion___');

      $injuries = array();

      foreach($injuries_types as $_type){
        $type = array(
          'id' => $_type->getId(),
          'nombre' => $_type->getNombre(),
          'members' => array()
        );

        $type_members = $fc_repository->findBy(['ficha_catalogo_tipo' => $_type]);

        foreach($type_members as $_injury){
          $injury = array(
            'id' => $_injury->getId(),
            'nombre' => $_injury->getNombre()
          );

          array_push($type['members'], $injury);
        }

        array_push($injuries, $type);
      }

      return $injuries;
  }

  /**
   * @Route("/clinico/lesion/nuevo", name="lesion_nuevo", methods="POST")
   */
  public function create_lesion(Request $request): Response
  {
      $em = $this->getDoctrine()->getManager();

      $fichaId = $request->request->get('fichaId');
      $tooth =  $request->request->get('tooth');
      $injuries = $request->request->get('injuries', array() );

      $keeps = array();

      $stored_injuries = $this->getInjuriesByTooth($fichaId, $tooth);

      foreach($injuries as $_injury){
        if($_injury['id'] != "") array_push($keeps, $_injury['id']);
        else{
          $injury = $this->createInjury($fichaId, $tooth, $_injury['injury']);

          $em->persist($injury);
        }
      }

      foreach ($stored_injuries as $store_injury){

        if(!in_array(strval($store_injury->getId()), $keeps)){
          $em->remove($store_injury);
        }
      }

      $em->flush();

      $tooth_obj = $this->getToothObjectInjury($fichaId, $tooth);

      $response = array(
        'number' => $tooth_obj->number,
        'ids' => $tooth_obj->ids,
        'injuries' => $tooth_obj->injuries
      );

      return $this->json($response);
  }

  private function createInjury($fichaId, $tooth, $injuryId){
    $em = $this->getDoctrine()->getManager();

    $f_repository = $em->getRepository(Ficha::class);
    $fc_repository = $em->getRepository(FichaCatalogo::class);

    $ficha = $f_repository->find($fichaId);
    $injuryType = $fc_repository->find($injuryId);

    $injury = new Lesion();
    $injury->setFicha($ficha);
    $injury->setPiezaDental($tooth);
    $injury->setLesionValorTipo($injuryType);

    return $injury;
  }
}
