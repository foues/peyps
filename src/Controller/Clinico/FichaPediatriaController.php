<?php

namespace App\Controller\Clinico;

use App\Entity\Administracion\PersonaAtributo;
use App\Entity\Clinico\DatosClinicos;
use App\Form\Clinico\datosclinicos_odontoped\DatosPedFlow;
use App\Form\Clinico\fichas\FichaFlow;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use App\Form\Clinico\datosclinicos_odontoped\CrearDatosPed;
use App\Form\Clinico\fichas\CrearFicha;
use App\Entity\Clinico\ExpedienteClinico;
use App\Entity\Clinico\Ficha;
use App\Entity\Clinico\FichaAtributo;
use App\Entity\Administracion\Persona;
use App\Entity\Academico\Curso;
use App\Entity\Academico\PlanEstudioCiclo;
use App\Service\Clinico\TablaFichaService;

use Datetime;

use App\Entity\Clinico\Pregunta;
use App\Entity\Clinico\FichaRespuestas;
use App\Entity\Clinico\OpcionesPregunta;
use App\Entity\Clinico\Respuesta;
use App\Entity\Clinico\Categoria;


class FichaPediatriaController extends AbstractController
{

  private $tablaFichaService;

    /**
     * FichaController constructor.
     * @param ContainerInterface $container
     * @param TablaFichaService $tablaFichaService
     */
    public function __construct(ContainerInterface $container, TablaFichaService $tablaFichaService)
    {
        $this->container = $container;
        $this->denyAccessUnlessGranted('ficha');
        $this->tablaFichaService = $tablaFichaService;
    }


  /**
   * @Route("/clinico/expedientes/{expedienteId}/fichas_ped", name="fichas_ped", methods="GET")
   */
  public function list($expedienteId): Response
  {

      $em = $this->getDoctrine()->getManager();
      $expediente = $em->getRepository(ExpedienteClinico::class)->findOneBy(['id' => $expedienteId]);
      $persona = $expediente->getPersona();
      $fnacimiento = $expediente->getPersona()->getNacimiento();
      $edad_actual = null;
      if ($fnacimiento) $edad_actual = (new Datetime)->diff($fnacimiento)->y;
      $fichas = $em->getRepository(Ficha::class)->findBy(['expediente_clinico' => $expediente]);
      $estudiantes = [];
      $edades = [];
      foreach ($fichas as $f) {
        $estudiante = $em->getRepository(Persona::class)->findOneBy(['cuenta' => $f->getCuenta()])->getNombreCompleto();
        $creado = $f->getFechaControl();
        $edad = null;
        if ($fnacimiento) $edad = $creado->diff($fnacimiento)->y;
        array_push($estudiantes, $estudiante);
        array_push($edades, $edad);
      }

      $fichas_t = array_map(null, $fichas, $estudiantes, $edades);

      return $this->render('clinico/fichas/list.html.twig', [
        'expedienteId' => $expedienteId,
        'expediente' => $expediente,
        'codigo_expediente' => $expediente->getCodigoExpediente(),
        'persona' => $persona,
        'fichas' => $fichas_t,
        'edad' => $edad_actual,
      ]);
  }

  /**
   * @Route("/clinico/expedientes/{expedienteId}/fichas_ped/nueva", name="ficha_ped_nueva", methods="GET|POST")
   */
  public function new($expedienteId, FichaFlow $flow): Response
  {
      $em = $this->getDoctrine()->getManager();
      $usuario = $this->getUser();
      $expediente = $em->getRepository(ExpedienteClinico::class)->findOneBy(['id' => $expedienteId]);
      $fnacimiento = $expediente->getPersona()->getNacimiento();
      $fhoy = new \DateTime();

      # Validar que no se cree una ficha si no se encuentran todas finalizadas
      $fichas_incompletas = $em->getRepository(Ficha::class)->findBy(['expediente_clinico'=>$expedienteId, 'finalizada'=>0]);

      // if(sizeof($fichas_incompletas)>0){
      //   $this->addFlash('warning', 'No se puede abrir una nueva ficha mientras otra este incompleta!');
      //   return $this->redirectToRoute('fichas', ['expedienteId' => $expedienteId]);
      // }

      $ficha = new CrearFicha();
//      $flow = $this->get('app.form.flow.fichaFlow');
      $flow->bind($ficha);
      $edad = $fhoy->diff($fnacimiento)->y;

      $form = $submittedForm = $flow->createForm();

      if ($flow->isValid($submittedForm)){
        $flow->saveCurrentStepData($form);

        if($flow->nextStep()){
          $form = $flow->createForm();
        } else {
          //Save


          $fichaNueva = new Ficha();
          $fichaNueva->setFechaControl($ficha->fecha);
          $fichaNueva->setMotivoConsulta($ficha->motivo);
          $fichaNueva->setHistoriaEnfermedad($ficha->historia);
          $fichaNueva->setVisitaOdontologo(boolval($ficha->visita_odontologo));
          $fichaNueva->setEmbarazada(boolval($ficha->embarazada));
          $fichaNueva->setReaccionesAdversas(boolval($ficha->reacciones_adversas));
          $fichaNueva->setCurso($ficha->curso->getCodigo());
          $fichaNueva->setCiclo($ficha->ciclo->getTipo() . ' ' . $ficha->ciclo->getPeriodo()->getInicio()->format('Y'));
          if(boolval($ficha->reacciones_adversas)) $fichaNueva->setTipoReaccion($ficha->tipo_reaccion);
          $fichaNueva->setTraumaDentoalveolar(boolval($ficha->trauma_dento));
          if(boolval($ficha->trauma_dento)) $fichaNueva->setPieza($ficha->pieza_trauma);

          $fichaNueva->setFrecuenciaCepillado($ficha->frecuencia_cepillado);

          //Atributos

          $atributos = array_merge($ficha->aditamientos_higiene->toArray(), $ficha->eval_sistemica->toArray(), $ficha->habitos_bucales->toArray());

          foreach ($atributos as $attr) {
            $nuevoAttr = new FichaAtributo();
            $nuevoAttr->setFicha($fichaNueva);
            $nuevoAttr->setAtributo($attr);
            $em->persist($nuevoAttr);
          }


          $fichaNueva->setCuenta($usuario->getCuenta());
          $fichaNueva->setExpedienteClinico($expediente);
          $fichaNueva->setFinalizada(0);
          $em->persist($fichaNueva);

          $datosClinicos = new DatosClinicos();
          $datosClinicos->setFicha($fichaNueva);
          $em->persist($datosClinicos);

          $em->flush();


          $flow->reset();
          $this->addFlash('success', 'Tus cambios fueron guardados');
          return $this->redirectToRoute('fichas', ['expedienteId' => $expedienteId]);
        }

      }

      return $this->render('clinico/fichas/new.html.twig', [
        'form' => $form->createView(),
        'flow' => $flow,
        'new' => true,
        'expedienteId' => $expedienteId,
        'expediente' => $expediente,
        'edad' => $edad
      ]);
  }

  /**
   * @Route("/clinico/expedientes/{expedienteId}/fichas_ped/{fichaId}", name="ficha_ped_detalle", methods="GET")
   */
  public function show(ExpedienteClinico $expedienteId, $fichaId): Response
  {
    $fichaInfo = $this->tablaFichaService->getTableInfo($expedienteId->getId(), $fichaId);
    $em = $this->getDoctrine()->getManager();
    $usuario = $this->getUser();
    $ficha = $em->getRepository(Ficha::Class)->findOneBy(['id' => $fichaId]);
    $canEdit = $this->tablaFichaService->allowEdit($usuario, $ficha);
    $canOpen = $ficha->getFinalizada();

    return $this->render('clinico/fichas/show.html.twig', [
      'expedienteId' => $expedienteId->getId(),
      'expediente' =>$expedienteId,
      'fichaId' => $fichaId,
      'fichaInfo' => $fichaInfo,
      'editable' => $canEdit,
      'openable' => $canOpen,
    ]);
  }

  /**
   * @Route("/clinico/expedientes/{expedienteId}/fichas_ped/{fichaId}/editar", name="ficha_ped_editar", methods="GET|POST")
   */
  public function edit($expedienteId, $fichaId, DatosPedFlow $flow): Response
  {
    $fichaInfo = $this->tablaFichaService->getTableInfo($expedienteId, $fichaId);
    $em = $this->getDoctrine()->getManager();
    $usuario = $this->getUser();
    $expediente = $em->getRepository(ExpedienteClinico::class)->findOneBy(['id' => $expedienteId]);
    $fnacimiento = $expediente->getPersona()->getNacimiento();
    $fhoy = new \DateTime();

    $ficha = $em->getRepository(Ficha::Class)->findOneBy(['id' => $fichaId]);
    $ficha_respuestas = $em->getRepository(FichaRespuestas::class)->findBy(['ficha'=>$fichaId]);
    $datosClinicos = $em->getRepository(DatosClinicos::Class)->findOneBy(['ficha' => $fichaId]);

    $canEdit = $this->tablaFichaService->allowEdit($usuario, $ficha);
    //Verificar que sea el creador
    if(!$canEdit){
      $this->addFlash('warning', 'No esta autorizado para editar esa ficha');
      return $this->redirectToRoute('fichas', ['expedienteId' => $expedienteId]);
    }
    // $repoFichaAttr = $em->getRepository(FichaAtributo::Class);
    $fichaEditable = new CrearDatosPed();

    $fichaEditable->fecha = $ficha->getFechaControl();

    $motivo = $this->getOneByNombreAndPreguntaCompleja($em, 'Motivo de Consulta', $ficha, '0' );
    if($motivo) $fichaEditable->motivo = $motivo->getOpcionesPregunta();

    // $fichaEditable->motivo = $ficha->getMotivoConsulta();
    $fichaEditable->historia = $ficha->getHistoriaEnfermedad();

    $fichaEditable->curso = $em->getRepository(Curso::class)->findOneBy(['codigo'=>$ficha->getCurso()]);
    $fichaEditable->ciclo = $em->getRepository(PlanEstudioCiclo::class)->findOneBy(['romano'=>$ficha->getCiclo()]);

    $edad = $fhoy->diff($fnacimiento)->y;

    $antecf = $this->getByNombrePregunta($em, 'Antecedentes Familiares', $ficha);
    if(sizeof($antecf)>0){$fichaEditable->antecedentesFam =array_map(function($val){return $val->getOpcionesPregunta();}, $antecf);}

    $antec = $this->getByNombrePregunta($em, 'Antecedentes Odontologicos', $ficha);
    if(sizeof($antec)>0){$fichaEditable->antecedentes =array_map(function($val){return $val->getOpcionesPregunta();}, $antec);}


    // $medicamentos = $this->getValorEscrito($em, 'Medicamentos tomados durante el embarazo', $ficha);
    // if($medicamentos) $fichaEditable->medicamentos_durante_embarazo = $medicamentos->getDescripcion();


    // if($parto)  = $parto->getDescripcion();



    $enf_emb = $this->getByNombrePregunta($em, 'Enfermedades durante el embarazo', $ficha);
    if(sizeof($enf_emb)>0){$fichaEditable->Enfermedades_Durante_el_Embarazo =array_map(function($val){return $val->getOpcionesPregunta();}, $enf_emb);}
    $medicamentos = $this->getByNombrePregunta($em, 'Medicamentos tomados durante el embarazo', $ficha);
    if(sizeof($medicamentos)>0){$fichaEditable->medicamentos_durante_embarazo =array_map(function($val){return $val->getOpcionesPregunta();}, $medicamentos);}
    // $parto = $this->getOneByNombreAndPreguntaCompleja($em, 'Parto', $ficha, '1' );
    // if($parto) $fichaEditable->PartoNumero = $parto->getOpcionesPregunta();
    $parto_opts = $this->getByNombreAndPreguntaCompleja($em, 'Parto', $ficha, '0' );
    if($parto_opts) $fichaEditable->Valores = array_map(function($val){return $val->getOpcionesPregunta();}, $parto_opts);
    // $peso = $this->getOneByNombreAndPreguntaCompleja($em, 'Peso al nacer', $ficha, '0');
    // if($peso){$fichaEditable->PesoAlNacer =$peso->getOpcionesPregunta();}
    // $talla = $this->getOneByNombreAndPreguntaCompleja($em, 'Talla al nacer',$ficha, '0');
    // if($talla){$fichaEditable->TallaAlNacer = $talla->getOpcionesPregunta();}
    // $peso = 'TESTING';
    // $talla = 'TESTING';


    $dif_neonat = $this->getByNombreAndPreguntaCompleja($em, 'Dificultades Neonatales', $ficha, '0');
    if(sizeof($dif_neonat)>0){$fichaEditable->DificultadesNeonatales =array_map(function($val){return $val->getOpcionesPregunta();}, $dif_neonat);}
    $alim_mat = $this->getByNombreAndPreguntaCompleja($em, 'Alimentacion Materna', $ficha, '1');
    if(sizeof($alim_mat)>0){$fichaEditable->AlimentacionMaterna =array_map(function($val){return $val->getOpcionesPregunta();}, $alim_mat);}
    // $duracion = 'TESTING';

    $fichaEditable->OtrosAntec = $this->getValorEscrito($em, 'Antecedentes Odontologicos', $ficha, '127');
    $fichaEditable->OtrosAntecFam = $this->getValorEscrito($em, 'Antecedentes Familiares', $ficha);
    $fichaEditable->otras_Enfermedades_Durante_el_Embarazo = $this->getValorEscrito($em, 'Enfermedades durante el embarazo', $ficha);
    $fichaEditable->otros_medicamentos_durante_embarazo = $this->getValorEscrito($em, 'Medicamentos tomados durante el embarazo', $ficha);
    $fichaEditable->PartoNumero =  $this->getValorEscrito($em, 'Parto', $ficha);
    $fichaEditable->PesoAlNacer =  $this->getValorEscrito($em, 'Peso al nacer', $ficha);
    $fichaEditable->PesoAlNacer_units = $this->getUnidades($em, 'Peso al nacer', $ficha);

    $fichaEditable->TallaAlNacer = $this->getValorEscrito($em, 'Talla al nacer', $ficha);
    $fichaEditable->ExpliqueDifNeoNat = $this->getValorEscrito($em, 'Dificultades Neonatales', $ficha);
    $fichaEditable->Duracion = $this->getValorEscrito($em, 'Alimentacion Materna', $ficha);
    $fichaEditable->Duracion_units = $this->getUnidades($em, 'Alimentacion Materna', $ficha);

    $fichaEditable->EdadLecheCompleta = $this->getValorEscrito($em, 'Leche completa', $ficha);
    $fichaEditable->EdadLecheCompleta_units = $this->getUnidades($em, 'Leche completa', $ficha);

    $fichaEditable->EdadFormula = $this->getValorEscrito($em, 'Formula', $ficha);
    $fichaEditable->EdadFormula_units = $this->getUnidades($em, 'Formula', $ficha);

    $fichaEditable->EdadAlimentosSolidos = $this->getValorEscrito($em, 'Alimentos solidos', $ficha);
    $fichaEditable->EdadAlimentosSolidos_units = $this->getUnidades($em, 'Alimentos solidos', $ficha);


    $fichaEditable->EdadEnQueGateo = $this->getValorEscrito($em, 'Desarrollo fisicomotor', $ficha);
    $fichaEditable->EdadEnQueCamino = $this->getValorEscrito($em, 'Desarrollo fisicomotor', $ficha, '126');
    $fichaEditable->EdadEnQueHablo = $this->getValorEscrito($em, 'Desarrollo fisicomotor', $ficha, '125');

    $fichaEditable->EdadEnQueGateo_units = $this->getUnidades($em, 'Desarrollo fisicomotor', $ficha);
    $fichaEditable->EdadEnQueCamino_units = $this->getUnidades($em, 'Desarrollo fisicomotor', $ficha, '126');
    $fichaEditable->EdadEnQueHablo_units = $this->getUnidades($em, 'Desarrollo fisicomotor', $ficha, '125');

    $fichaEditable->OtrasEnfermeInf = $this->getValorEscrito($em, 'Enfermedades Infecciosas Padecidas', $ficha); //23
    $fichaEditable->PorCualMotivo = $this->getValorEscrito($em, 'Ha asistido a consulta medica en los ultimos 6 meses?', $ficha); //24
    $fichaEditable->MotivoDeTratamiento = $this->getValorEscrito($em, 'Se encuentra bajo tratamiento medico?', $ficha); //25
    $fichaEditable->MedicamentosQueTomaActualmente = $this->getValorEscrito($em, 'Medicamentos que toma actualmente', $ficha);//26
    $fichaEditable->MotivoDeIntervencion = $this->getValorEscrito($em, 'Se le han practicado intervenciones quirurgicas?', $ficha);//27
    $fichaEditable->MotivoHospitalizacion = $this->getValorEscrito($em, 'Ha estado hospitalizado?', $ficha); //28
    $fichaEditable->OtrasVacunas = $this->getValorEscrito($em, 'Vacunas Recibidas', $ficha); //29

    $edad_erup_lec = $this->getOneByNombreAndPreguntaCompleja($em, 'Edad de erupcion de los dientes primarios', $ficha, '0' ); //32
    if($edad_erup_lec) $fichaEditable->EdadErupcionDeDientesPrimarios = $edad_erup_lec->getOpcionesPregunta();
    // // $fichaEditable->EdadErupcionDeDientesPermanentes = $this->getValorEscrito($em, 'Edad de erupcion de los dientes permanentes', $ficha); //33
    $fichaEditable->EdadTraumatismoDentario = $this->getValorEscrito($em, 'Traumatismos dentarios', $ficha); //35
    $fichaEditable->EdadTraumatismoDentario_units = $this->getUnidades($em, 'Traumatismos dentarios', $ficha);
    // $fichaEditable->TratamientoRecibido = $this->getValorEscrito($em, 'Traumatismos dentarios', $ficha, '126'); //35
    // $fichaEditable->VecesAlDiaQueSeCepilla = $this->getValorEscrito($em, 'Cepillado', $ficha); //37
    // $fichaEditable->QuienLoCepilla = $this->getValorEscrito($em, 'Cepillado', $ficha, '126'); //37

    $tipo_trauma = $this->getOneByNombreAndPreguntaCompleja($em, 'Tipo de Traumatismo', $ficha, '0');
    if($tipo_trauma) $fichaEditable->TipoTraumatismo = $tipo_trauma->getOpcionesPregunta();

    $tratam_recibido = $this->getOneByNombreAndPreguntaCompleja($em, 'Traumatismos dentarios', $ficha, '0');
    if($tratam_recibido) $fichaEditable->TratamientoRecibido = $tratam_recibido->getOpcionesPregunta();

    $veces_cepilla = $this->getOneByNombreAndPreguntaCompleja($em, 'Cepillado', $ficha, '1');
    if($veces_cepilla) $fichaEditable->VecesAlDiaQueSeCepilla = $veces_cepilla->getOpcionesPregunta();

    $quien_cepilla = $this->getOneByNombreAndPreguntaCompleja($em, 'Cepillado', $ficha, '3');
    if($quien_cepilla )$fichaEditable->QuienLoCepilla = $quien_cepilla->getOpcionesPregunta();

    $crema_dent = $this->getOneByNombreAndPreguntaCompleja($em, '¿Cuál crema dental utiliza?', $ficha, '0');
    if($crema_dent) $fichaEditable->CremaDentalQueUtiliza = $crema_dent->getOpcionesPregunta();



    $CuantasMeriendasHaceEntreComidas = $this->getOneByNombreAndPreguntaCompleja($em, 'Cuantas meriendas hace el ninio entre comidas?' , $ficha, '0');
    if($CuantasMeriendasHaceEntreComidas) $fichaEditable->CuantasMeriendasHaceEntreComidas = $CuantasMeriendasHaceEntreComidas->getOpcionesPregunta();
    $fichaEditable->QueCome = $this->getValorEscrito($em, '¿Que come?' ,$ficha);
    $fichaEditable->CualesAlimentosBlandos = $this->getValorEscrito($em, '¿Prefiere alimentos blandos?' ,$ficha);
    $fichaEditable->AlimentosSolidosQuePrefiere = $this->getValorEscrito($em, '¿Cuáles alimentos sólidos Prefiere?' ,$ficha);

    $CuantosVasos = $this->getOneByNombreAndPreguntaCompleja($em, 'Leche' , $ficha, '1');
    if($CuantosVasos)$fichaEditable->CuantosVasos = $CuantosVasos->getOpcionesPregunta();

    $fichaEditable->DesayunoHabitual = $this->getValorEscrito($em, 'Alimentación habitual del niño' ,$ficha); //127
    $fichaEditable->MeriendaHabitual = $this->getValorEscrito($em, 'Alimentación habitual del niño' ,$ficha, '126'); //126
    $fichaEditable->AlmuerzoHabitual = $this->getValorEscrito($em, 'Alimentación habitual del niño' ,$ficha, '125'); //125
    $fichaEditable->MeriendaHabitualTarde = $this->getValorEscrito($em, 'Alimentación habitual del niño' ,$ficha, '124'); //124
    $fichaEditable->CenaHabitual = $this->getValorEscrito($em, 'Alimentación habitual del niño' ,$ficha, '123'); //123


    $fichaEditable->TallaActual = $this->getValorEscrito($em, 'Talla actual', $ficha);
    $fichaEditable->PesoActual = $this->getValorEscrito($em, 'Peso actual', $ficha);

    $fichaEditable->TallaActual_units = $this->getUnidades($em, 'Talla actual', $ficha);
    $fichaEditable->PesoActual_units = $this->getUnidades($em, 'Peso actual', $ficha);


    $inser_orejas = $this->getOneByNombreAndPreguntaCompleja($em, 'Insercion de orejas' , $ficha, '0');
    if($inser_orejas) $fichaEditable->InsercionDeOrejas =  $inser_orejas->getOpcionesPregunta();


    $fichaEditable->Descripcion = $this->getValorEscrito($em, 'Observaciones', $ficha);

    $fichaEditable->DescEncia = $this->getValorEscrito($em, 'Examen clinico intraoral', $ficha);
    $fichaEditable->DescMucosaBucal = $this->getValorEscrito($em, 'Examen clinico intraoral', $ficha, '126');
    $fichaEditable->DescPisoBucal = $this->getValorEscrito($em, 'Examen clinico intraoral', $ficha, '125');
    $fichaEditable->DescPaladarDuro = $this->getValorEscrito($em, 'Examen clinico intraoral', $ficha, '124');
    $fichaEditable->DescPaladarBlando = $this->getValorEscrito($em, 'Examen clinico intraoral', $ficha, '123');
    $fichaEditable->DescAmigdalas = $this->getValorEscrito($em, 'Examen clinico intraoral', $ficha, '122');


    $desv_atm = $this->getByNombreAndPreguntaCompleja($em, 'Examen funcional', $ficha, '0');
    if(sizeof($desv_atm)>0){$fichaEditable->DesviacionATM =array_map(function($val){return $val->getOpcionesPregunta();}, $desv_atm);}

    $apertura = $this->getByNombreAndPreguntaCompleja($em, 'Examen funcional', $ficha, '1');
    if(sizeof($apertura)>0){$fichaEditable->Apertura =array_map(function($val){return $val->getOpcionesPregunta();}, $apertura);}

    $cierre = $this->getByNombreAndPreguntaCompleja($em, 'Examen funcional', $ficha, '2');
    if(sizeof($cierre)>0){$fichaEditable->Cierre =array_map(function($val){return $val->getOpcionesPregunta();}, $cierre);}

    $ruidos = $this->getByNombreAndPreguntaCompleja($em, 'Ruidos', $ficha, '0');
    if(sizeof($ruidos)>0){$fichaEditable->Ruidos =array_map(function($val){return $val->getOpcionesPregunta();}, $ruidos);}

    $saltos = $this->getByNombreAndPreguntaCompleja($em, 'Saltos', $ficha, '0');
    if(sizeof($saltos)>0){$fichaEditable->Saltos =array_map(function($val){return $val->getOpcionesPregunta();}, $saltos);}

    $fichaEditable->ExpliqueHabitosParafuncionales = $this->getValorEscrito($em, 'Habitos parafuncionales', $ficha);

    $duracion = $this->getOneByNombreAndPreguntaCompleja($em, 'Habitos parafuncionales' , $ficha, '3');
    if($duracion)$fichaEditable->DuracionHabitosParaf = $duracion->getOpcionesPregunta();

    $intensidad = $this->getOneByNombreAndPreguntaCompleja($em, 'Habitos parafuncionales' , $ficha, '2');
    if($intensidad)$fichaEditable->IntensidadHabitosParaf = $intensidad->getOpcionesPregunta();

    $frecuencia = $this->getOneByNombreAndPreguntaCompleja($em, 'Habitos parafuncionales' , $ficha, '1');
    if($frecuencia)$fichaEditable->FrecuenciaHabitosParaf = $frecuencia->getOpcionesPregunta();

    $fichaEditable->Overjet = $this->getValorEscrito($em, 'OVERJET', $ficha);
    $fichaEditable->Overbite = $this->getValorEscrito($em, 'OVERBITE', $ficha);
    $fichaEditable->LineaMediaSuperiorOpts = $this->getValorEscrito($em, 'LINEA MEDIA SUPERIOR', $ficha);
    $fichaEditable->LineaMediaInferiorOpts = $this->getValorEscrito($em, 'LINEA MEDIA INFERIOR', $ficha);
    $fichaEditable->Diastemas = $this->getValorEscrito($em, 'Diastemas', $ficha);
    $fichaEditable->PerdidasPrematura = $this->getValorEscrito($em, 'Perdidas Prematuras', $ficha);
    $fichaEditable->OtrasObservaciones = $this->getValorEscrito($em, 'Otras Observaciones', $ficha);


    //  $this->getOneByNombreAndPreguntaCompleja($em, 'Alimentacion Materna', $ficha, '0' );
    // if($duracion) $fichaEditable->Duracion = $duracion->getOpcionesPregunta();
    $leche_c = $this->getByNombreAndPreguntaCompleja($em, 'Leche completa', $ficha, '1');
    if(sizeof($leche_c)>0){$fichaEditable->LecheCompleta =array_map(function($val){return $val->getOpcionesPregunta();}, $leche_c);}
    // $edad_lechec  = 'TESTING';
    // $this->getOneByNombreAndPreguntaCompleja($em, 'Leche completa', $ficha, '0' );
    // if($edad_lechec) $fichaEditable->EdadLecheCompleta = $edad_lechec->getOpcionesPregunta();
    $formula = $this->getByNombreAndPreguntaCompleja($em, 'Formula', $ficha, '1');
    if(sizeof($formula)>0){$fichaEditable->Formula =array_map(function($val){return $val->getOpcionesPregunta();}, $formula);}
    // $edad_formula = 'TESTING';
    // $this->getOneByNombreAndPreguntaCompleja($em, 'Formula', $ficha, '0' );
    // if($edad_formula) $fichaEditable->EdadFormula = $edad_formula->getOpcionesPregunta();
    $alim_solid = $this->getByNombreAndPreguntaCompleja($em, 'Alimentos solidos', $ficha, '1');
    if(sizeof($alim_solid)>0){$fichaEditable->AlimentosSolidos =array_map(function($val){return $val->getOpcionesPregunta();}, $alim_solid);}
    // $desa_fis = $this->getByNombrePregunta($em, 'Desarrollo fisicomotor');
    // if(sizeof($desa_fis)>0){$fichaEditable->DesarrolloFisicomotor =array_map(function($val){return $val->getOpcionesPregunta();}, $desa_fis);}
    // $edad_alimsolid = 'TESTING';
    // $this->getOneByNombreAndPreguntaCompleja($em, 'Alimentos solidos', $ficha, '0');
    // if($edad_alimsolid) $fichaEditable->EdadAlimentosSolidos = $edad_alimsolid->getOpcionesPregunta();

    // $edad_gateo = 'TESTING';
    // $this->getOneByNombreAndPreguntaCompleja($em, 'Desarrollo fisicomotor', $ficha, '0');
    // if($edad_gateo) $fichaEditable->EdadEnQueGateo = $edad_gateo->getOpcionesPregunta();
    // $edad_camino = 'TESTING';
    // $this->getOneByNombreAndPreguntaCompleja($em, 'Desarrollo fisicomotor', $ficha, '1');
    // if($edad_camino) $fichaEditable->EdadEnQueCamino = $edad_camino->getOpcionesPregunta();
    // $edad_hablo = 'TESTING';
    // $this->getOneByNombreAndPreguntaCompleja($em, 'Desarrollo fisicomotor', $ficha, '2');
    // if($edad_hablo) $fichaEditable->EdadEnQueHablo = $edad_hablo->getOpcionesPregunta();

    $sist_nervioso =$this->getByNombreAndPreguntaCompleja($em, 'Sistema Nervioso', $ficha, '0');
    if(sizeof($sist_nervioso)>0){$fichaEditable->SistemaNervioso =array_map(function($val){return $val->getOpcionesPregunta();}, $sist_nervioso);}

    $sist_cardio =$this->getByNombreAndPreguntaCompleja($em, 'Sistema Cardiovascular', $ficha, '0');
    if(sizeof($sist_cardio)>0){$fichaEditable->SistemaCardiovascular =array_map(function($val){return $val->getOpcionesPregunta();}, $sist_cardio);}

    $sist_hemato  =$this->getByNombreAndPreguntaCompleja($em, 'Sistema Hematopoyetico', $ficha, '0');
    if(sizeof($sist_hemato)>0){$fichaEditable->SistemaHematopoyetico =array_map(function($val){return $val->getOpcionesPregunta();}, $sist_hemato);}

    $sist_resp =$this->getByNombreAndPreguntaCompleja($em, 'Sistema Respiratorio', $ficha, '0');
    if(sizeof($sist_resp)>0){$fichaEditable->SistemaRespiratorio =array_map(function($val){return $val->getOpcionesPregunta();}, $sist_resp);}

    $sist_gastroi =$this->getByNombreAndPreguntaCompleja($em, 'Sistema Gastrointestinal', $ficha, '0');
    if(sizeof($sist_gastroi)>0){$fichaEditable->SistemaGastrointestinal =array_map(function($val){return $val->getOpcionesPregunta();}, $sist_gastroi);}

    $sist_endoc  =$this->getByNombreAndPreguntaCompleja($em, 'Sistema Endocrino', $ficha, '0');
    if(sizeof($sist_endoc)>0){$fichaEditable->SistemaEndocrino =array_map(function($val){return $val->getOpcionesPregunta();}, $sist_endoc);}

    $sist_genitou =$this->getByNombreAndPreguntaCompleja($em, 'Sistema Genitourinario', $ficha, '0');
    if(sizeof($sist_genitou)>0){$fichaEditable->SistemaGenitourinario =array_map(function($val){return $val->getOpcionesPregunta();}, $sist_genitou);}

    $pielyM =$this->getByNombreAndPreguntaCompleja($em, 'Piel y Mucosas', $ficha, '0');
    if(sizeof($pielyM)>0){$fichaEditable->PielYMucosa =array_map(function($val){return $val->getOpcionesPregunta();}, $pielyM);}

    $trast_senso =$this->getByNombreAndPreguntaCompleja($em, 'Trastornos Sensoriales', $ficha, '0');
    if(sizeof($trast_senso)>0){$fichaEditable->TrastornosSensoriales =array_map(function($val){return $val->getOpcionesPregunta();}, $trast_senso);}

    $alergias =$this->getByNombreAndPreguntaCompleja($em, 'Alergias', $ficha, '0');
    if(sizeof($alergias)>0){$fichaEditable->Alergias =array_map(function($val){return $val->getOpcionesPregunta();}, $alergias);}


    for($i = 60; $i <= 81; $i++)
      $fichaEditable->{'TA'.$i} = $this->getDescripcionByOpcionId($em, $ficha, $i);



    $enf_infec_pad =$this->getByNombreAndPreguntaCompleja($em, 'Enfermedades Infecciosas Padecidas', $ficha, '0');
    if(sizeof($enf_infec_pad)>0){$fichaEditable->enfermedadesInfecciosasPadecidas =array_map(function($val){return $val->getOpcionesPregunta();}, $enf_infec_pad);}
    //NEW VALUES FROM HERE!!
    $Asist_cons_ultimos6m = $this->getByNombreAndPreguntaCompleja($em, 'Ha asistido a consulta medica en los ultimos 6 meses?', $ficha, '1');
    if(sizeof($Asist_cons_ultimos6m)>0){$fichaEditable->HaAsistidoAConsultaEnLosUltimos6Meses =array_map(function($val){return $val->getOpcionesPregunta();}, $Asist_cons_ultimos6m);}

    $Asist_con_opts= 'TESTING';
    // $this->getOneByNombreAndPreguntaCompleja($em, 'Ha asistido a consulta medica en los ultimos 6 meses?', $ficha, '0');
    // if($Asist_con_opts) $fichaEditable->PorCualMotivo = $Asist_con_opts->getOpcionesPregunta()->getNombreOpcion();

    $Bajo_tratam =$this->getByNombreAndPreguntaCompleja($em, 'Se encuentra bajo tratamiento medico?', $ficha, '1');
    if(sizeof($Bajo_tratam)>0){$fichaEditable->SeEncuentraBajoTratamiento =array_map(function($val){return $val->getOpcionesPregunta();}, $Bajo_tratam);}

    $moti_trat =  'TESTING';
    // $this->getOneByNombreAndPreguntaCompleja($em, 'Se encuentra bajo tratamiento medico?', $ficha, '0');
    // if($moti_trat) $fichaEditable->MotivoDeTratamiento = $moti_trat->getOpcionesPregunta();

    $Med_actuales = 'TESTING';
    // $this->getByNombreAndPreguntaCompleja($em, 'Medicamentos que toma actualmente', $ficha, '0');
    // if(sizeof($Med_actuales)>0){$fichaEditable->MedicamentosQueTomaActualmente =array_map(function($val){return $val->getOpcionesPregunta();}, $Med_actuales);}

    $interv_q =$this->getByNombreAndPreguntaCompleja($em, 'Se le han practicado intervenciones quirurgicas?', $ficha, '1');
    if(sizeof($interv_q)>0){$fichaEditable->IntervencionesQuirurgicas =array_map(function($val){return $val->getOpcionesPregunta();}, $interv_q);}

    $moti_inter = 'TESTING';
    // $this->getOneByNombreAndPreguntaCompleja($em, 'Se le han practicado intervenciones quirurgicas?', $ficha, '0');
    // if($moti_inter) $fichaEditable->MotivoDeIntervencion = $moti_inter->getOpcionesPregunta();

    $hospitalizado =$this->getByNombreAndPreguntaCompleja($em, 'Ha estado hospitalizado?', $ficha, '1');
    if(sizeof($hospitalizado)>0){$fichaEditable->HaSidoHospitalizado =array_map(function($val){return $val->getOpcionesPregunta();}, $hospitalizado);}

    $moti_hospi =  'TESTING';
    // $this->getOneByNombreAndPreguntaCompleja($em, 'Ha estado hospitalizado?', $ficha, '0');
    // if($moti_hospi) $fichaEditable->MotivoHospitalizacion = $moti_hospi->getOpcionesPregunta();

    $vac_rec =$this->getByNombreAndPreguntaCompleja($em, 'Vacunas Recibidas', $ficha, '0');
    if(sizeof($vac_rec)>0){$fichaEditable->VacunasRecibidas =array_map(function($val){return $val->getOpcionesPregunta();}, $vac_rec);}

    $refuerzos =$this->getByNombreAndPreguntaCompleja($em, 'Ha recibido sus refuerzos hasta la fecha?', $ficha, '0');
    if(sizeof($refuerzos)>0){$fichaEditable->TodosSusRefuerzosRecibidosHastaLaFecha =array_map(function($val){return $val->getOpcionesPregunta();}, $refuerzos);}

    $esq_vac =$this->getByNombreAndPreguntaCompleja($em, 'Esquema de vacunacion', $ficha, '0');
    if(sizeof($esq_vac)>0){$fichaEditable->EsquemaDeVacunacion =array_map(function($val){return $val->getOpcionesPregunta();}, $esq_vac);}

    $fichaEditable->EsquemaIncompleto = $this->getValorEscrito($em, 'Esquema de vacunacion', $ficha);

    //COMENZAR ERUPCIONES DIENTES PRIMARIOS
    // $edad_erup_lec = 'TESTING';
    //  $this->getOneByNombreAndPreguntaCompleja($em, 'Edad de erupcion de los dientes primarios', $ficha, '0');
    // if($edad_erup_lec) $fichaEditable->EdadErupcionDeDientesPrimarios = $edad_erup_lec->getOpcionesPregunta();

    // $edad_erup_perm = 'TESTING';
    // $this->getOneByNombreAndPreguntaCompleja($em, 'Edad de erupcion de los dientes permanentes', $ficha, '0');
    // if($edad_erup_perm) $fichaEditable->EdadErupcionDeDientesPermanentes = $edad_erup_perm->getOpcionesPregunta();

    $traum_dent = $this->getByNombreAndPreguntaCompleja($em, 'Traumatismos dentarios', $ficha, '1');
    if(sizeof($traum_dent)>0){$fichaEditable->TraumatismosDentarios =array_map(function($val){return $val->getOpcionesPregunta();}, $traum_dent);}

    // $this->getOneByNombreAndPreguntaCompleja($em, 'Traumatismos dentarios', $ficha, '0');
    // if($edad_traum_den) $fichaEditable->EdadTraumatismoDentario = $edad_traum_den->getOpcionesPregunta();


    // $this->getOneByNombreAndPreguntaCompleja($em, 'Traumatismos dentarios', $ficha, '2');
    // if($tratam_recibido) $fichaEditable->TratamientoRecibido = $tratam_recibido->getOpcionesPregunta();

    $asit_odonto = $this->getByNombreAndPreguntaCompleja($em, 'Ha asistido anteriormente al odontologo?', $ficha, '1');
    if(sizeof($asit_odonto)>0){$fichaEditable->HaAsistidoAnteriormenteAlOdontologo =array_map(function($val){return $val->getOpcionesPregunta();}, $asit_odonto);}


    // $this->getOneByNombreAndPreguntaCompleja($em, 'Cepillado', $ficha, '1');
    // if($veces_cepilla) $fichaEditable->VecesAlDiaQueSeCepilla = $veces_cepilla->getOpcionesPregunta();

    $cepilla_solo = $this->getByNombreAndPreguntaCompleja($em, 'Cepillado', $ficha, '2');
    if(sizeof($cepilla_solo)>0){$fichaEditable->SeCepillaSolo =array_map(function($val){return $val->getOpcionesPregunta();}, $cepilla_solo);}

    // $this->getOneByNombreAndPreguntaCompleja($em, 'Cepillado', $ficha, '3');
    // if($quien_cepilla) $fichaEditable->QuienLoCepilla = $quien_cepilla->getOpcionesPregunta();

    // $crema_dent = $this->getOneByNombreAndPreguntaCompleja($em, 'Cual crema dental utiliza?', $ficha, '0');
    // if($crema_dent) $fichaEditable->CremaDentalQueUtiliza = $crema_dent->getOpcionesPregunta();

    $utili_seda_eng = $this->getByNombreAndPreguntaCompleja($em, 'Utiliza seda dental o enjuague bucal?', $ficha, '0');
    if(sizeof($utili_seda_eng)>0){$fichaEditable->UtilizaSedaDentalOEnjuague =array_map(function($val){return $val->getOpcionesPregunta();}, $utili_seda_eng);}


    $ing_unavezXS = $this->getByNombreAndPreguntaCompleja($em, 'Alimentos que el ninio ingiere al menos una vez a la semana', $ficha, '0');
    if(sizeof($ing_unavezXS)>0){$fichaEditable->AlimentosIngeridosAlMenosUnaVezPorSemana =array_map(function($val){return $val->getOpcionesPregunta();}, $ing_unavezXS);}

    $merien_entreCom = 'TESTING';
    // $this->getOneByNombreAndPreguntaCompleja($em, 'Cuantas meriendas hace el ninio entre comidas?', $ficha, '0');
    // if($merien_entreCom) $fichaEditable->CuantasMeriendasHaceEntreComidas = $merien_entreCom->getOpcionesPregunta();

    $quecome  = 'TESTING';
    // $this->getByNombreAndPreguntaCompleja($em, 'Cuantas meriendas hace el ninio entre comidas?', $ficha, '1');
    // if(sizeof($quecome)>0){$fichaEditable->QueCome =array_map(function($val){return $val->getOpcionesPregunta();},$quecome);}

    $alimBlandosQuePref  = 'TESTING';

    //FALTA ALIM BLANDOS QUE PREFIERE ? complex 0

    $solid_prefiere = 'TESTING';
    // $this->getByNombreAndPreguntaCompleja($em, 'Cuales alimentos solidos Prefiere?', $ficha, '0');
    // if(sizeof($solid_prefiere)>0){$fichaEditable->AlimentosSolidosQuePrefiere =array_map(function($val){return $val->getOpcionesPregunta();}, $solid_prefiere);}

    $biberon = $this->getByNombreAndPreguntaCompleja($em, 'Biberon', $ficha, '1');
    if(sizeof($biberon)>0){$fichaEditable->Biberon =array_map(function($val){return $val->getOpcionesPregunta();}, $biberon);}

    $contenido_bibe = $this->getByNombreAndPreguntaCompleja($em, 'Biberon', $ficha, '0');
    if(sizeof($contenido_bibe)>0){$fichaEditable->ContenidoDelBiberon =array_map(function($val){return $val->getOpcionesPregunta();}, $contenido_bibe);}

    $toma_leche = $this->getByNombreAndPreguntaCompleja($em, 'Leche', $ficha, '0');
    if(sizeof($toma_leche)>0){$fichaEditable->TomaLeche =array_map(function($val){return $val->getOpcionesPregunta();}, $toma_leche);}

    $cuantos_vasos = 'TESTING';
    // $this->getOneByNombreAndPreguntaCompleja($em, 'Leche', $ficha, '1');
    // if($cuantos_vasos) $fichaEditable->CuantosVasos = $cuantos_vasos->getOpcionesPregunta();

    $conque_tomal = $this->getByNombreAndPreguntaCompleja($em, 'Leche', $ficha, '2');
    if(sizeof($conque_tomal)>0){$fichaEditable->ConQueTomaSuLeche =array_map(function($val){return $val->getOpcionesPregunta();}, $conque_tomal);}

    $consume_msal = $this->getByNombreAndPreguntaCompleja($em, 'Consume mucha sal?', $ficha, '0');
    if(sizeof($consume_msal)>0){$fichaEditable->ConsumeMuchaSal =array_map(function($val){return $val->getOpcionesPregunta();}, $consume_msal);}

    $consume_mcit = $this->getByNombreAndPreguntaCompleja($em, 'Consume muchos citricos?', $ficha, '0');
    if(sizeof($consume_mcit)>0){$fichaEditable->ConsumeMuchosCitricos =array_map(function($val){return $val->getOpcionesPregunta();}, $consume_mcit);}


    // $this->getByNombreAndPreguntaCompleja($em, 'Alimentacion habitual del ninio', $ficha, '0');
    // if(sizeof($desa_habit)>0){$fichaEditable->DesayunoHabitual =array_map(function($val){return $val->getOpcionesPregunta();}, $desa_habit);}

    // $this->getByNombreAndPreguntaCompleja($em, 'Alimentacion habitual del ninio', $ficha, '1');
    // if(sizeof($merie_habit)>0){$fichaEditable->MeriendaHabitual =array_map(function($val){return $val->getOpcionesPregunta();}, $merie_habit);}

    // $this->getByNombreAndPreguntaCompleja($em, 'Alimentacion habitual del ninio', $ficha, '2');
    // if(sizeof($almuer_habit)>0){$fichaEditable->AlmuerzoHabitual =array_map(function($val){return $val->getOpcionesPregunta();}, $almuer_habit);}

    // $this->getByNombreAndPreguntaCompleja($em, 'Alimentacion habitual del ninio', $ficha, '3');
    // if(sizeof($merie2_habit)>0){$fichaEditable->MeriendaHabitualTarde =array_map(function($val){return $val->getOpcionesPregunta();}, $merie2_habit);}

    // $this->getByNombreAndPreguntaCompleja($em, 'Alimentacion habitual del ninio', $ficha, '4');
    // if(sizeof($cena_habit)>0){$fichaEditable->CenaHabitual =array_map(function($val){return $val->getOpcionesPregunta();}, $cena_habit);}

    //CLINICO EXTRAORA
    // $this->getOneByNombreAndPreguntaCompleja($em, 'Talla actual (Estatura en cm)', $ficha, '0');
    // if($talla_act) $fichaEditable->TallaActual = $talla_act->getOpcionesPregunta();

    // $this->getOneByNombreAndPreguntaCompleja($em, 'Peso actual (Kg)', $ficha, '0');
    // if($peso_act) $fichaEditable->PesoActual = $peso_act->getOpcionesPregunta();

    $perfil =$this->getOneByNombreAndPreguntaCompleja($em, 'Perfil', $ficha, '0');
    if($perfil) $fichaEditable->Perfil = $perfil->getOpcionesPregunta();

    $config =$this->getOneByNombreAndPreguntaCompleja($em, 'Configuracion', $ficha, '0');
    if($config) $fichaEditable->Configuracion = $config->getOpcionesPregunta();

    $labios =$this->getOneByNombreAndPreguntaCompleja($em, 'Labios', $ficha, '0');
    if($labios) $fichaEditable->Labios = $labios->getOpcionesPregunta();

    $nariz =$this->getOneByNombreAndPreguntaCompleja($em, 'Tamanio de la nariz', $ficha, '0');
    if($nariz) $fichaEditable->TamanioDeNariz = $nariz->getOpcionesPregunta();

    $inser_orejas =$this->getOneByNombreAndPreguntaCompleja($em, 'Insercion de orejas', $ficha, '0');
    if($inser_orejas) $fichaEditable->InsercionDeOrejas = $inser_orejas->getOpcionesPregunta();

    $observaciones =$this->getByNombreAndPreguntaCompleja($em, 'Observaciones', $ficha, '0');
    if(sizeof($observaciones)>0){$fichaEditable->Observaciones =array_map(function($val){return $val->getOpcionesPregunta();},$observaciones);}

    $encias = $this->getByNombreAndPreguntaCompleja($em, 'Examen clinico intraoral', $ficha, '0');
    if(sizeof($encias)>0){$fichaEditable->Encias =array_map(function($val){return $val->getOpcionesPregunta();},$encias);}

    $mucosa_bucal = $this->getByNombreAndPreguntaCompleja($em, 'Examen clinico intraoral', $ficha, '1');
    if(sizeof($mucosa_bucal)>0){$fichaEditable->MucosaBucal =array_map(function($val){return $val->getOpcionesPregunta();},$mucosa_bucal);}

    $lengua= $this->getByNombreAndPreguntaCompleja($em, 'Examen clinico intraoral', $ficha, '2');
    if(sizeof($lengua)>0){$fichaEditable->Lengua =array_map(function($val){return $val->getOpcionesPregunta();},$lengua);}


    $freni_lab_sup = $this->getByNombreAndPreguntaCompleja($em, 'Examen clinico intraoral', $ficha, '3');
    if(sizeof($freni_lab_sup)>0){$fichaEditable->FrenilloLabialSuperior =array_map(function($val){return $val->getOpcionesPregunta();},$freni_lab_sup);}


    $freni_lab_inf = $this->getByNombreAndPreguntaCompleja($em, 'Examen clinico intraoral', $ficha, '4');
    if(sizeof($freni_lab_inf)>0){$fichaEditable->FrenilloLabialInferior =array_map(function($val){return $val->getOpcionesPregunta();},$freni_lab_inf);}

    $freni_inf = $this->getByNombreAndPreguntaCompleja($em, 'Examen clinico intraoral', $ficha, '5');
    if(sizeof($freni_inf)>0){$fichaEditable->FrenilloInferior =array_map(function($val){return $val->getOpcionesPregunta();},$freni_inf);}

    $piso_buc = $this->getByNombreAndPreguntaCompleja($em, 'Examen clinico intraoral', $ficha, '6');
    if(sizeof($piso_buc)>0){$fichaEditable->PisoBucal =array_map(function($val){return $val->getOpcionesPregunta();},$piso_buc);}

    $exa_funcional = $this->getByNombreAndPreguntaCompleja($em, 'Examen Funcional', $ficha, '0');
    if(sizeof($exa_funcional)>0){$fichaEditable->ExamenFuncional =array_map(function($val){return $val->getOpcionesPregunta();},$exa_funcional);}

    $habit_para = $this->getByNombreAndPreguntaCompleja($em, 'Habitos Parafuncionales', $ficha, '0');
    if(sizeof($habit_para)>0){$fichaEditable->HabitosParafuncionales =array_map(function($val){return $val->getOpcionesPregunta();},$habit_para);}

    $rel_mol_der = $this->getByNombreAndPreguntaCompleja($em, 'Relacion molar derecha', $ficha, '0');
    if(sizeof($rel_mol_der)>0){$fichaEditable->RelacionMolarDerecha =array_map(function($val){return $val->getOpcionesPregunta();},$rel_mol_der);}

    $rel_mol_iz = $this->getByNombreAndPreguntaCompleja($em, 'Relacion molar izquierda', $ficha, '0');
    if(sizeof($rel_mol_iz)>0){$fichaEditable->RelacionMolarIzquierda =array_map(function($val){return $val->getOpcionesPregunta();},$rel_mol_iz);}

    $rel_can_der = $this->getByNombreAndPreguntaCompleja($em, 'Relacion canina derecha', $ficha, '0');
    if(sizeof($rel_can_der)>0){$fichaEditable->RelacionCaninaDerecha =array_map(function($val){return $val->getOpcionesPregunta();},$rel_can_der);}

    $rel_can_iz = $this->getByNombreAndPreguntaCompleja($em, 'Relacion canina izquierda', $ficha, '0');
    if(sizeof($rel_can_iz)>0){$fichaEditable->RelacionCaninaIzquierda =array_map(function($val){return $val->getOpcionesPregunta();},$rel_can_iz);}

    $mord_abierta = $this->getByNombreAndPreguntaCompleja($em, 'Mordida Abierta', $ficha, '0');
    if(sizeof($mord_abierta)>0){$fichaEditable->MordidaAbierta=array_map(function($val){return $val->getOpcionesPregunta();},$mord_abierta);}

    $mord_cruzada = $this->getByNombreAndPreguntaCompleja($em, 'Mordida Cruzada', $ficha, '0');
    if(sizeof($mord_cruzada)>0){$fichaEditable->MordidaCruzada =array_map(function($val){return $val->getOpcionesPregunta();},$mord_cruzada);}

    $mord_cruz_opts = 'TESTING';
    // $this->getOneByNombreAndPreguntaCompleja($em, 'Mordida Cruzada', $ficha, '1');
    // if($mord_cruz_opts) $fichaEditable->MordidaCruzadaOpts = $mord_cruz_opts->getOpcionesPregunta();

    $Overjet = 'TESTING';
    // $this->getOneByNombreAndPreguntaCompleja($em, 'OVERJET', $ficha, '0');
    // if($Overjet) $fichaEditable->Overjet= $Overjet->getOpcionesPregunta();

    $Overjet_opts = $this->getByNombreAndPreguntaCompleja($em, 'OVERJET', $ficha, '1');
    if(sizeof($Overjet_opts)>0){$fichaEditable->OverjetOpts =array_map(function($val){return $val->getOpcionesPregunta();},$Overjet_opts);}

    $Overbite = 'TESTING';
    // $this->getOneByNombreAndPreguntaCompleja($em, 'OVERBITE', $ficha, '0');
    // if($Overbite) $fichaEditable->Overbite  = $Overbite->getOpcionesPregunta();

    $Overbite_opts = $this->getByNombreAndPreguntaCompleja($em, 'OVERBITE', $ficha, '1');
    if(sizeof($Overbite_opts)>0){$fichaEditable->OverbiteOpts=array_map(function($val){return $val->getOpcionesPregunta();},$Overbite_opts);}

    $linea_meds = $this->getByNombreAndPreguntaCompleja($em, 'LINEA MEDIA SUPERIOR', $ficha, '0');
    if(sizeof($linea_meds)>0){$fichaEditable->LineaMediaSuperior=array_map(function($val){return $val->getOpcionesPregunta();},$linea_meds);}

    $linea_meds_opts = 'TESTING';
    // $this->getOneByNombreAndPreguntaCompleja($em, 'LINEA MEDIA SUPERIOR', $ficha, '1');
    // if($linea_meds_opts) $fichaEditable->LineaMediaSuperiorOpts= $linea_meds_opts->getOpcionesPregunta();

    $linea_medi = $this->getByNombreAndPreguntaCompleja($em, 'LINEA MEDIA INFERIOR', $ficha, '0');
    if(sizeof($linea_medi)>0){$fichaEditable->LineaMediaInferior=array_map(function($val){return $val->getOpcionesPregunta();},$linea_medi);}

    $linea__medi_opts = 'TESTING';
    // $this->getOneByNombreAndPreguntaCompleja($em, 'LINEA MEDIA INFERIOR', $ficha, '1');
    // if($linea__medi_opts) $fichaEditable->LineaMediaInferiorOpts= $linea__medi_opts->getOpcionesPregunta();

    $diastemas = 'TESTING';
    // $this->getOneByNombreAndPreguntaCompleja($em, 'DIENTES', $ficha, '1');
    // if($diastemas) $fichaEditable->Diastemas= $diastemas->getOpcionesPregunta();

    $dientes = $this->getByNombreAndPreguntaCompleja($em, 'DIENTES', $ficha, '0');
    if(sizeof($dientes)>0){$fichaEditable->Dientes=array_map(function($val){return $val->getOpcionesPregunta();},$dientes);}

    $esp_prims = $this->getByNombreAndPreguntaCompleja($em, 'Espacio de Primate Superior', $ficha, '0');
    if(sizeof($esp_prims)>0){$fichaEditable->EspacioDePrimateSup=array_map(function($val){return $val->getOpcionesPregunta();},$esp_prims);}

    $esp_primi = $this->getByNombreAndPreguntaCompleja($em, 'Espacio de Primate Inferior', $ficha, '0');
    if(sizeof($esp_primi)>0){$fichaEditable->EspacioDePrimateInf=array_map(function($val){return $val->getOpcionesPregunta();},$esp_primi);}

    $apinamiento = $this->getByNombreAndPreguntaCompleja($em, 'Apiniamiento', $ficha, '0');
    if(sizeof($apinamiento)>0){$fichaEditable->Apiniamiento=array_map(function($val){return $val->getOpcionesPregunta();},$apinamiento);}


    $hobby = $this->getByNombreAndPreguntaCompleja($em, 'Hobby. Juegos Preferidos', $ficha, '0');
    if(sizeof($hobby)>0){$fichaEditable->Hobby=array_map(function($val){return $val->getOpcionesPregunta();},$hobby);}

    $deporte = $this->getByNombreAndPreguntaCompleja($em, 'Deportes', $ficha, '0');
      if(sizeof($deporte)>0){$fichaEditable->Deportes=array_map(function($val){return $val->getOpcionesPregunta();},$deporte);}

    $mascotas = $this->getByNombreAndPreguntaCompleja($em, 'Mascotas', $ficha, '0');
      if(sizeof($mascotas)>0){$fichaEditable->Mascotas=array_map(function($val){return $val->getOpcionesPregunta();},$mascotas);}

    $prog_tv = $this->getByNombreAndPreguntaCompleja($em, 'Programas Preferidos en TV', $ficha, '0');
      if(sizeof($prog_tv)>0){$fichaEditable->ProgramasTV=array_map(function($val){return $val->getOpcionesPregunta();},$prog_tv);}

    $musica = $this->getByNombreAndPreguntaCompleja($em, 'Musica Preferida', $ficha, '0');
      if(sizeof($musica)>0){$fichaEditable->Musica=array_map(function($val){return $val->getOpcionesPregunta();},$musica);}

    $fobias = $this->getByNombreAndPreguntaCompleja($em, 'Fobias', $ficha, '0');
      if(sizeof($fobias)>0){$fichaEditable->Fobias=array_map(function($val){return $val->getOpcionesPregunta();},$fobias);}

    $recompensas = $this->getByNombreAndPreguntaCompleja($em, 'Recompensas', $ficha, '0');
      if(sizeof($recompensas)>0){$fichaEditable->Recompensas=array_map(function($val){return $val->getOpcionesPregunta();},$recompensas);}

    $castigos = $this->getByNombreAndPreguntaCompleja($em, 'Castigos', $ficha, '0');
      if(sizeof($castigos)>0){$fichaEditable->Castigo=array_map(function($val){return $val->getOpcionesPregunta();},$castigos);}

    $otras_activi = $this->getByNombreAndPreguntaCompleja($em, 'Otras Actividades Extra Escolares', $ficha, '0');
      if(sizeof($otras_activi)>0){$fichaEditable->OtrasActividades=array_map(function($val){return $val->getOpcionesPregunta();},$otras_activi);}

    $aprendizaje = $this->getByNombreAndPreguntaCompleja($em, 'Aprendizaje', $ficha, '0');
      if(sizeof($aprendizaje)>0){$fichaEditable->Aprendizaje=array_map(function($val){return $val->getOpcionesPregunta();},$aprendizaje);}

      $fichaEditable->Hobby_exp =  $this->getValorEscrito($em, 'Hobby. Juegos Preferidos', $ficha);
      $fichaEditable->Deportes_exp =  $this->getValorEscrito($em, 'Deportes', $ficha);
      $fichaEditable->Mascotas_exp =  $this->getValorEscrito($em, 'Mascotas', $ficha);
      $fichaEditable->ProgramasTV_exp = $this->getValorEscrito($em, 'Programas Preferidos en TV', $ficha);
      $fichaEditable->Musica_exp = $this->getValorEscrito($em, 'Musica Preferida', $ficha);
      $fichaEditable->Fobias_exp = $this->getValorEscrito($em, 'Fobias', $ficha);
      $fichaEditable->Recompensas_exp = $this->getValorEscrito($em, 'Recompensas', $ficha);
      $fichaEditable->Castigo_exp = $this->getValorEscrito($em, 'Castigos', $ficha);
      $fichaEditable->OtrasActividades_exp = $this->getValorEscrito($em, 'Otras Actividades Extra Escolares', $ficha);
      $fichaEditable->Aprendizaje_exp = $this->getValorEscrito($em, 'Aprendizaje', $ficha);
      $fichaEditable->Exp_enConsulta = $this->getValorEscrito($em, 'Experiencia y Comportamiento en la consulta Odontológica', $ficha);

      $fichaEditable->DiagnosticoMedico = $this->getValorEscrito($em, 'Diagnóstico Integral', $ficha, '121');
      $fichaEditable->DiagnosticoPeriodontal = $this->getValorEscrito($em, 'Diagnóstico Integral', $ficha, '122');
      $fichaEditable->TipoDeCaries = $this->getValorEscrito($em, 'Diagnóstico Integral', $ficha, '123');
      $fichaEditable->AlteracionesPulpares = $this->getValorEscrito($em, 'Diagnóstico Integral', $ficha, '124');
      $fichaEditable->PatologiasTejBlandoDuro = $this->getValorEscrito($em, 'Diagnóstico Integral', $ficha, '125');
      $fichaEditable->Maloclusiones = $this->getValorEscrito($em, 'Diagnóstico Integral', $ficha, '126');
      $fichaEditable->RiesgoCariogenico = $this->getValorEscrito($em, 'Diagnóstico Integral', $ficha, '127');


    // $describa = $this->getOneByNombreAndPreguntaCompleja($em, 'Observaciones', $ficha, '1');
    // if($describa)
      // $fichaEditable->Descripcion= $describa->getOpcionesPregunta()->getFichaRespuestas();



    $old_atributos = array_merge($antecf,$antec, $parto_opts,$dif_neonat,$alim_mat,$leche_c,$Asist_cons_ultimos6m,$Bajo_tratam,$interv_q,
                   $formula,$alim_solid,$sist_nervioso,$sist_cardio,$sist_hemato,$sist_resp,$sist_gastroi,$sist_endoc,
                    $sist_genitou,$pielyM,$trast_senso,$alergias,$enf_infec_pad, $hospitalizado,$vac_rec,$refuerzos,$esq_vac,$asit_odonto,$utili_seda_eng,$cepilla_solo, $ing_unavezXS,
                    $biberon,$contenido_bibe,$toma_leche,$conque_tomal,$consume_msal,$consume_mcit,$observaciones,$encias,$mucosa_bucal,$lengua,$freni_lab_sup,$freni_lab_inf,$freni_inf,$piso_buc,$exa_funcional,$habit_para,
                  $rel_mol_der, $rel_mol_iz, $rel_can_der,$rel_can_iz,$mord_abierta,$mord_cruzada,$Overjet_opts,$Overbite_opts,$linea_medi,$linea_meds,$dientes,$esp_prims,$esp_primi,$apinamiento,
                  $enf_emb, $medicamentos,$traum_dent, $desv_atm, $apertura, $cierre, $ruidos, $saltos,
                    $hobby, $deporte, $mascotas, $prog_tv, $musica, $fobias, $recompensas, $castigos, $otras_activi, $aprendizaje
                  //
                );


    $old_selects = array(
      $crema_dent,
      $perfil,
      $config,$labios, $nariz, $inser_orejas,
      $motivo,
      $edad_erup_lec,
      $tipo_trauma,
      $tratam_recibido,
      $veces_cepilla,
      $quien_cepilla,
      $crema_dent,
      $CuantasMeriendasHaceEntreComidas,
      $CuantosVasos,
      $inser_orejas,
      $duracion, $intensidad, $frecuencia


                 );


     // ->findBy(['id' => $expedienteId]);
//    $flow = $this->get('app.form.flow.datosFlow');
    $flow->bind($fichaEditable);

    $form = $submittedForm = $flow->createForm();

    if ($flow->isValid($submittedForm)){
      $flow->saveCurrentStepData($form);

      if($flow->nextStep()){
        $form = $flow->createForm();
      } else {
        //Save
       if(!is_array($fichaEditable->antecedentesFam)) $fichaEditable->antecedentesFam = $fichaEditable->antecedentesFam->toArray();
       if(!is_array($fichaEditable->antecedentes)) $fichaEditable->antecedentes = $fichaEditable->antecedentes->toArray();
       if(!is_array($fichaEditable->Enfermedades_Durante_el_Embarazo)) $fichaEditable->Enfermedades_Durante_el_Embarazo = $fichaEditable->Enfermedades_Durante_el_Embarazo->toArray();
       if(!is_array($fichaEditable->medicamentos_durante_embarazo)) $fichaEditable->medicamentos_durante_embarazo = $fichaEditable->medicamentos_durante_embarazo->toArray();
       if(!is_array($fichaEditable->Valores)) $fichaEditable->Valores = $fichaEditable->Valores->toArray();
       if(!is_array($fichaEditable->DificultadesNeonatales)) $fichaEditable->DificultadesNeonatales = $fichaEditable->DificultadesNeonatales->toArray();
       if(!is_array($fichaEditable->AlimentacionMaterna)) $fichaEditable->AlimentacionMaterna = $fichaEditable->AlimentacionMaterna->toArray();
       if(!is_array($fichaEditable->LecheCompleta)) $fichaEditable->LecheCompleta = $fichaEditable->LecheCompleta->toArray();
       if(!is_array($fichaEditable->Formula)) $fichaEditable->Formula = $fichaEditable->Formula->toArray();
       if(!is_array($fichaEditable->AlimentosSolidos)) $fichaEditable->AlimentosSolidos = $fichaEditable->AlimentosSolidos->toArray();
       if(!is_array($fichaEditable->SistemaNervioso)) $fichaEditable->SistemaNervioso = $fichaEditable->SistemaNervioso->toArray();
       if(!is_array($fichaEditable->SistemaCardiovascular)) $fichaEditable->SistemaCardiovascular = $fichaEditable->SistemaCardiovascular->toArray();
       if(!is_array($fichaEditable->SistemaHematopoyetico)) $fichaEditable->SistemaHematopoyetico = $fichaEditable->SistemaHematopoyetico->toArray();
       if(!is_array($fichaEditable->SistemaRespiratorio)) $fichaEditable->SistemaRespiratorio = $fichaEditable->SistemaRespiratorio->toArray();
       if(!is_array($fichaEditable->SistemaGastrointestinal)) $fichaEditable->SistemaGastrointestinal = $fichaEditable->SistemaGastrointestinal->toArray();
       if(!is_array($fichaEditable->SistemaEndocrino)) $fichaEditable->SistemaEndocrino = $fichaEditable->SistemaEndocrino->toArray();
       if(!is_array($fichaEditable->SistemaGenitourinario)) $fichaEditable->SistemaGenitourinario = $fichaEditable->SistemaGenitourinario->toArray();
       if(!is_array($fichaEditable->PielYMucosa)) $fichaEditable->PielYMucosa = $fichaEditable->PielYMucosa->toArray();
       if(!is_array($fichaEditable->TrastornosSensoriales)) $fichaEditable->TrastornosSensoriales = $fichaEditable->TrastornosSensoriales->toArray();
       if(!is_array($fichaEditable->Alergias)) $fichaEditable->Alergias = $fichaEditable->Alergias->toArray();
       if(!is_array($fichaEditable->enfermedadesInfecciosasPadecidas)) $fichaEditable->enfermedadesInfecciosasPadecidas = $fichaEditable->enfermedadesInfecciosasPadecidas->toArray();
       if(!is_array($fichaEditable->HaAsistidoAConsultaEnLosUltimos6Meses)) $fichaEditable->HaAsistidoAConsultaEnLosUltimos6Meses = $fichaEditable->HaAsistidoAConsultaEnLosUltimos6Meses->toArray();
       if(!is_array($fichaEditable->SeEncuentraBajoTratamiento)) $fichaEditable->SeEncuentraBajoTratamiento = $fichaEditable->SeEncuentraBajoTratamiento->toArray();
       // if(!is_array($fichaEditable->MedicamentosQueTomaActualmente)) $fichaEditable->MedicamentosQueTomaActualmente = $fichaEditable->MedicamentosQueTomaActualmente->toArray();
       if(!is_array($fichaEditable->IntervencionesQuirurgicas)) $fichaEditable->IntervencionesQuirurgicas = $fichaEditable->IntervencionesQuirurgicas->toArray();
       if(!is_array($fichaEditable->HaSidoHospitalizado)) $fichaEditable->HaSidoHospitalizado = $fichaEditable->HaSidoHospitalizado->toArray();
       if(!is_array($fichaEditable->VacunasRecibidas)) $fichaEditable->VacunasRecibidas = $fichaEditable->VacunasRecibidas->toArray();
       if(!is_array($fichaEditable->TodosSusRefuerzosRecibidosHastaLaFecha)) $fichaEditable->TodosSusRefuerzosRecibidosHastaLaFecha = $fichaEditable->TodosSusRefuerzosRecibidosHastaLaFecha->toArray();
       if(!is_array($fichaEditable->EsquemaDeVacunacion)) $fichaEditable->EsquemaDeVacunacion = $fichaEditable->EsquemaDeVacunacion->toArray();
       if(!is_array($fichaEditable->TraumatismosDentarios)) $fichaEditable->TraumatismosDentarios = $fichaEditable->TraumatismosDentarios->toArray();
       if(!is_array($fichaEditable->HaAsistidoAnteriormenteAlOdontologo)) $fichaEditable->HaAsistidoAnteriormenteAlOdontologo = $fichaEditable->HaAsistidoAnteriormenteAlOdontologo->toArray();
       if(!is_array($fichaEditable->UtilizaSedaDentalOEnjuague)) $fichaEditable->UtilizaSedaDentalOEnjuague = $fichaEditable->UtilizaSedaDentalOEnjuague->toArray();
       if(!is_array($fichaEditable->SeCepillaSolo)) $fichaEditable->SeCepillaSolo = $fichaEditable->SeCepillaSolo->toArray();
       if(!is_array($fichaEditable->AlimentosIngeridosAlMenosUnaVezPorSemana)) $fichaEditable->AlimentosIngeridosAlMenosUnaVezPorSemana = $fichaEditable->AlimentosIngeridosAlMenosUnaVezPorSemana->toArray();
       // if(!is_array($fichaEditable->QueCome)) $fichaEditable->QueCome = $fichaEditable->QueCome->toArray();
       // if(!is_array($fichaEditable->AlimentosSolidosQuePrefiere)) $fichaEditable->AlimentosSolidosQuePrefiere =$fichaEditable->AlimentosSolidosQuePrefiere->toArray();
       if(!is_array($fichaEditable->Biberon)) $fichaEditable->Biberon = $fichaEditable->Biberon->toArray();
       if(!is_array($fichaEditable->ContenidoDelBiberon)) $fichaEditable->ContenidoDelBiberon = $fichaEditable->ContenidoDelBiberon->toArray();
       if(!is_array($fichaEditable->TomaLeche)) $fichaEditable->TomaLeche = $fichaEditable->TomaLeche->toArray();
       if(!is_array($fichaEditable->ConQueTomaSuLeche)) $fichaEditable->ConQueTomaSuLeche = $fichaEditable->ConQueTomaSuLeche->toArray();
       if(!is_array($fichaEditable->ConsumeMuchaSal)) $fichaEditable->ConsumeMuchaSal = $fichaEditable->ConsumeMuchaSal->toArray();
       if(!is_array($fichaEditable->ConsumeMuchosCitricos)) $fichaEditable->ConsumeMuchosCitricos = $fichaEditable->ConsumeMuchosCitricos->toArray();
       // if(!is_array($fichaEditable->DesayunoHabitual)) $fichaEditable->DesayunoHabitual = $fichaEditable->DesayunoHabitual->toArray();
       // if(!is_array($fichaEditable->MeriendaHabitual)) $fichaEditable->MeriendaHabitual = $fichaEditable->MeriendaHabitual->toArray();
       // if(!is_array($fichaEditable->AlmuerzoHabitual)) $fichaEditable->AlmuerzoHabitual = $fichaEditable->AlmuerzoHabitual->toArray();
       // if(!is_array($fichaEditable->MeriendaHabitualTarde)) $fichaEditable->MeriendaHabitualTarde = $fichaEditable->MeriendaHabitualTarde->toArray();
       // if(!is_array($fichaEditable->CenaHabitual)) $fichaEditable->CenaHabitual = $fichaEditable->CenaHabitual->toArray();
       if(!is_array($fichaEditable->Observaciones)) $fichaEditable->Observaciones = $fichaEditable->Observaciones->toArray();
       if(!is_array($fichaEditable->Encias)) $fichaEditable->Encias = $fichaEditable->Encias->toArray();
       if(!is_array($fichaEditable->MucosaBucal)) $fichaEditable->MucosaBucal = $fichaEditable->MucosaBucal->toArray();
       if(!is_array($fichaEditable->Lengua)) $fichaEditable->Lengua = $fichaEditable->Lengua->toArray();
       if(!is_array($fichaEditable->FrenilloLabialSuperior)) $fichaEditable->FrenilloLabialSuperior= $fichaEditable->FrenilloLabialSuperior->toArray();
       if(!is_array($fichaEditable->FrenilloLabialInferior)) $fichaEditable->FrenilloLabialInferior = $fichaEditable->FrenilloLabialInferior->toArray();
       if(!is_array($fichaEditable->FrenilloInferior)) $fichaEditable->FrenilloInferior= $fichaEditable->FrenilloInferior->toArray();
       if(!is_array($fichaEditable->PisoBucal)) $fichaEditable->PisoBucal = $fichaEditable->PisoBucal->toArray();
       // if(!is_array($fichaEditable->ExamenFuncional)) $fichaEditable->ExamenFuncional = $fichaEditable->ExamenFuncional->toArray();
       if(!is_array($fichaEditable->HabitosParafuncionales)) $fichaEditable->HabitosParafuncionales = $fichaEditable->HabitosParafuncionales->toArray();
       if(!is_array($fichaEditable->RelacionMolarDerecha)) $fichaEditable->RelacionMolarDerecha = $fichaEditable->RelacionMolarDerecha->toArray();
       if(!is_array($fichaEditable->RelacionMolarIzquierda)) $fichaEditable->RelacionMolarIzquierda = $fichaEditable->RelacionMolarIzquierda->toArray();
       if(!is_array($fichaEditable->RelacionCaninaDerecha)) $fichaEditable->RelacionCaninaDerecha = $fichaEditable->RelacionCaninaDerecha->toArray();
       if(!is_array($fichaEditable->RelacionCaninaIzquierda)) $fichaEditable->RelacionCaninaIzquierda = $fichaEditable->RelacionCaninaIzquierda->toArray();
       if(!is_array($fichaEditable->MordidaAbierta)) $fichaEditable->MordidaAbierta = $fichaEditable->MordidaAbierta->toArray();
       if(!is_array($fichaEditable->MordidaCruzada)) $fichaEditable->MordidaCruzada = $fichaEditable->MordidaCruzada->toArray();
       if(!is_array($fichaEditable->OverjetOpts)) $fichaEditable->OverjetOpts = $fichaEditable->OverjetOpts->toArray();
       if(!is_array($fichaEditable->OverbiteOpts)) $fichaEditable->OverbiteOpts = $fichaEditable->OverbiteOpts->toArray();
       if(!is_array($fichaEditable->LineaMediaSuperior)) $fichaEditable->LineaMediaSuperior = $fichaEditable->LineaMediaSuperior->toArray();
       if(!is_array($fichaEditable->LineaMediaInferior)) $fichaEditable->LineaMediaInferior = $fichaEditable->LineaMediaInferior->toArray();
       if(!is_array($fichaEditable->Dientes)) $fichaEditable->Dientes = $fichaEditable->Dientes->toArray();
       if(!is_array($fichaEditable->EspacioDePrimateSup)) $fichaEditable->EspacioDePrimateSup = $fichaEditable->EspacioDePrimateSup->toArray();
       if(!is_array($fichaEditable->EspacioDePrimateInf)) $fichaEditable->EspacioDePrimateInf = $fichaEditable->EspacioDePrimateInf->toArray();
       if(!is_array($fichaEditable->Apiniamiento)) $fichaEditable->Apiniamiento = $fichaEditable->Apiniamiento->toArray();

       if(!is_array($fichaEditable->DesviacionATM)) $fichaEditable->DesviacionATM = $fichaEditable->DesviacionATM->toArray();
       if(!is_array($fichaEditable->Apertura)) $fichaEditable->Apertura = $fichaEditable->Apertura->toArray();
       if(!is_array($fichaEditable->Cierre)) $fichaEditable->Cierre = $fichaEditable->Cierre->toArray();
       if(!is_array($fichaEditable->Ruidos)) $fichaEditable->Ruidos = $fichaEditable->Ruidos->toArray();
       if(!is_array($fichaEditable->Saltos)) $fichaEditable->Saltos = $fichaEditable->Saltos->toArray();

       if(!is_array($fichaEditable->Hobby)) $fichaEditable->Hobby = $fichaEditable->Hobby->toArray();
       if(!is_array($fichaEditable->Deportes)) $fichaEditable->Deportes = $fichaEditable->Deportes->toArray();
       if(!is_array($fichaEditable->Mascotas)) $fichaEditable->Mascotas = $fichaEditable->Mascotas->toArray();
       if(!is_array($fichaEditable->ProgramasTV)) $fichaEditable->ProgramasTV = $fichaEditable->ProgramasTV->toArray();
       if(!is_array($fichaEditable->Musica)) $fichaEditable->Musica = $fichaEditable->Musica->toArray();
       if(!is_array($fichaEditable->Fobias)) $fichaEditable->Fobias = $fichaEditable->Fobias->toArray();
       if(!is_array($fichaEditable->Recompensas)) $fichaEditable->Recompensas = $fichaEditable->Recompensas->toArray();
       if(!is_array($fichaEditable->Castigo)) $fichaEditable->Castigo = $fichaEditable->Castigo->toArray();
       if(!is_array($fichaEditable->OtrasActividades)) $fichaEditable->OtrasActividades = $fichaEditable->OtrasActividades->toArray();
       if(!is_array($fichaEditable->Aprendizaje)) $fichaEditable->Aprendizaje = $fichaEditable->Aprendizaje->toArray();


        $atributos =array_merge(
          $fichaEditable->antecedentesFam,
          $fichaEditable->antecedentes,
          $fichaEditable->Enfermedades_Durante_el_Embarazo,
          $fichaEditable->medicamentos_durante_embarazo,
          $fichaEditable->Valores,
          $fichaEditable->DificultadesNeonatales,
          $fichaEditable->AlimentacionMaterna,
          $fichaEditable->LecheCompleta,
          $fichaEditable->Formula,
          $fichaEditable->AlimentosSolidos,
          $fichaEditable->SistemaNervioso,
          $fichaEditable->SistemaCardiovascular,
          $fichaEditable->SistemaHematopoyetico,
          $fichaEditable->SistemaRespiratorio,
          $fichaEditable->SistemaGastrointestinal,
          $fichaEditable->SistemaEndocrino,
          $fichaEditable->SistemaGenitourinario,
          $fichaEditable->PielYMucosa,
          $fichaEditable->TrastornosSensoriales,
          $fichaEditable->Alergias,
          $fichaEditable->enfermedadesInfecciosasPadecidas,
          $fichaEditable->HaAsistidoAConsultaEnLosUltimos6Meses,
          $fichaEditable->SeEncuentraBajoTratamiento,

          $fichaEditable->IntervencionesQuirurgicas,
          $fichaEditable->HaSidoHospitalizado,
          $fichaEditable->VacunasRecibidas,
          $fichaEditable->TodosSusRefuerzosRecibidosHastaLaFecha,
          $fichaEditable->EsquemaDeVacunacion,
          $fichaEditable->TraumatismosDentarios,
          $fichaEditable->HaAsistidoAnteriormenteAlOdontologo,
          $fichaEditable->UtilizaSedaDentalOEnjuague,
          $fichaEditable->SeCepillaSolo,
          $fichaEditable->AlimentosIngeridosAlMenosUnaVezPorSemana,

          $fichaEditable->Biberon,
          $fichaEditable->ContenidoDelBiberon,
          $fichaEditable->TomaLeche,
          $fichaEditable->ConQueTomaSuLeche,
          $fichaEditable->ConsumeMuchaSal,
          $fichaEditable->ConsumeMuchosCitricos,

          $fichaEditable->Observaciones,
          $fichaEditable->Encias,
          $fichaEditable->MucosaBucal,
          $fichaEditable->Lengua,
          $fichaEditable->FrenilloLabialSuperior,
          $fichaEditable->FrenilloLabialInferior,
          $fichaEditable->FrenilloInferior,
          $fichaEditable->PisoBucal,
          // $fichaEditable->ExamenFuncional,
          $fichaEditable->HabitosParafuncionales,
          $fichaEditable->RelacionMolarDerecha,
          $fichaEditable->RelacionMolarIzquierda,
          $fichaEditable->RelacionCaninaDerecha,
          $fichaEditable->RelacionCaninaIzquierda,
          $fichaEditable->MordidaAbierta,
          $fichaEditable->MordidaCruzada,
          $fichaEditable->OverjetOpts,
          $fichaEditable->OverbiteOpts,
          $fichaEditable->LineaMediaSuperior,
          $fichaEditable->LineaMediaInferior,
          $fichaEditable->Dientes,
          $fichaEditable->EspacioDePrimateSup,
          $fichaEditable->EspacioDePrimateInf,
          $fichaEditable->Apiniamiento,
          $fichaEditable->DesviacionATM,
          $fichaEditable->Apertura,
          $fichaEditable->Cierre,
          $fichaEditable->Ruidos,
          $fichaEditable->Saltos,

          $fichaEditable->Hobby,
          $fichaEditable->Deportes,
          $fichaEditable->Mascotas,
          $fichaEditable->ProgramasTV,
          $fichaEditable->Musica,
          $fichaEditable->Fobias,
          $fichaEditable->Recompensas,
          $fichaEditable->Castigo,
          $fichaEditable->OtrasActividades,
          $fichaEditable->Aprendizaje

        );




        $atributosSelect = array(
          // $fichaEditable->PartoNumero,
          // $fichaEditable->PesoAlNacer,
          // $fichaEditable->TallaAlNacer,
          // $fichaEditable->Duracion,
          // $fichaEditable->EdadLecheCompleta,
          // $fichaEditable->EdadFormula,
          // $fichaEditable->EdadAlimentosSolidos,
          // $fichaEditable->EdadEnQueGateo,
          // $fichaEditable->EdadEnQueCamino,
          // $fichaEditable->EdadEnQueHablo,
          // $fichaEditable->PorCualMotivo,
          // $fichaEditable->MotivoDeTratamiento,
          // $fichaEditable->MotivoDeIntervencion,
          // $fichaEditable->MotivoHospitalizacion,
          // $fichaEditable->EdadErupcionDeDientesPrimarios,
          // $fichaEditable->EdadErupcionDeDientesPermanentes,
          // $fichaEditable->EdadTraumatismoDentario,
          // $fichaEditable->TratamientoRecibido,
          // $fichaEditable->VecesAlDiaQueSeCepilla,
          // $fichaEditable->QuienLoCepilla,
          // $fichaEditable->CremaDentalQueUtiliza,
          // $fichaEditable->CuantasMeriendasHaceEntreComidas,
          // $fichaEditable->CuantosVasos,
          // $fichaEditable->TallaActual,
          // $fichaEditable->PesoActual,
          $fichaEditable->Perfil,
          $fichaEditable->Configuracion,
          $fichaEditable->Labios,
          $fichaEditable->TamanioDeNariz,
          // $fichaEditable->InsercionDeOrejas,
          // $fichaEditable->MordidaCruzadaOpts,
          // $fichaEditable->Overjet,
          // $fichaEditable->Overbite,
          // $fichaEditable->LineaMediaSuperiorOpts,
          // $fichaEditable->LineaMediaInferiorOpts,
          // $fichaEditable->Diastemas,
          $fichaEditable->motivo,
          $fichaEditable->EdadErupcionDeDientesPrimarios,
          $fichaEditable->TipoTraumatismo,
          $fichaEditable->TratamientoRecibido,
          $fichaEditable->VecesAlDiaQueSeCepilla,
          $fichaEditable->QuienLoCepilla,
          $fichaEditable->CremaDentalQueUtiliza,
          $fichaEditable->CuantasMeriendasHaceEntreComidas,
          $fichaEditable->CuantosVasos,
          $fichaEditable->InsercionDeOrejas,
          $fichaEditable->DuracionHabitosParaf,
          $fichaEditable->IntensidadHabitosParaf,
          $fichaEditable->FrecuenciaHabitosParaf

        );

        $oldTextos = $this->getAllTexts($em, $ficha);

        foreach ($oldTextos as $texto) {
          if($texto)
          $em->remove($texto);
        }

        // $otros_antecf,$enf_emb,  $medicamentos, $parto, $peso, $talla, $duracion, $edad_gateo, $edad_camino,$edad_hablo,
        //$edad_lechec, $edad_formula, $edad_alimsolid, $Med_actuales, $Asist_con_opts,$moti_trat,$moti_inter,$moti_hospi,$edad_erup_lec,
        //$edad_erup_perm,$edad_traum_den,$tratam_recibido,$veces_cepilla,$quien_cepilla,$quecome,$merien_entreCom,$cuantos_vasos,$talla_act,$peso_act,
        // $desa_habit,$merie_habit,$almuer_habit,$merie2_habit,$cena_habit,$Overjet,$Overbite,$diastemas,$solid_prefiere,$mord_cruz_opts,
        //$linea_meds_opts,$linea__medi_opts,


        // $resp = new FichaRespuestas(); $resp->setFicha($ficha); $resp->setOpcionesPregunta($attr);$em->persist($resp);
        $var1 = new FichaRespuestas(); $em->persist($var1->setFicha($ficha)->setOpcionesPregunta($this->getOpcionPregunta($em, '75', '127'))->setDescripcion($fichaEditable->OtrosAntecFam));
        $var2 = new FichaRespuestas(); $em->persist($var2->setFicha($ficha)->setOpcionesPregunta($this->getOpcionPregunta($em, '1', '127'))->setDescripcion($fichaEditable->OtrosAntec));
        $var3 = new FichaRespuestas(); $em->persist($var3->setFicha($ficha)->setOpcionesPregunta($this->getOpcionPregunta($em, '2', '127'))->setDescripcion($fichaEditable->otras_Enfermedades_Durante_el_Embarazo));
        $var4 = new FichaRespuestas(); $em->persist($var4->setFicha($ficha)->setOpcionesPregunta($this->getOpcionPregunta($em, '3', '127'))->setDescripcion($fichaEditable->otros_medicamentos_durante_embarazo));
        $var1 = new FichaRespuestas(); $em->persist($var1->setFicha($ficha)->setOpcionesPregunta($this->getOpcionPregunta($em, '4', '127'))->setDescripcion($fichaEditable->PartoNumero));
        $var1 = new FichaRespuestas(); $em->persist($var1->setFicha($ficha)->setOpcionesPregunta($this->getOpcionPregunta($em, '5', '127'))->setDescripcion($fichaEditable->PesoAlNacer)->setUnidades($fichaEditable->PesoAlNacer_units));


        $var1 = new FichaRespuestas(); $em->persist($var1->setFicha($ficha)->setOpcionesPregunta($this->getOpcionPregunta($em, '6', '127'))->setDescripcion($fichaEditable->TallaAlNacer));
        $var1 = new FichaRespuestas(); $em->persist($var1->setFicha($ficha)->setOpcionesPregunta($this->getOpcionPregunta($em, '7', '127'))->setDescripcion($fichaEditable->ExpliqueDifNeoNat));
        $var1 = new FichaRespuestas(); $em->persist($var1->setFicha($ficha)->setOpcionesPregunta($this->getOpcionPregunta($em, '8', '127'))->setDescripcion($fichaEditable->Duracion)->setUnidades($fichaEditable->Duracion_units));
        $var1 = new FichaRespuestas(); $em->persist($var1->setFicha($ficha)->setOpcionesPregunta($this->getOpcionPregunta($em, '9', '127'))->setDescripcion($fichaEditable->EdadLecheCompleta)->setUnidades($fichaEditable->EdadLecheCompleta_units));
        $var1 = new FichaRespuestas(); $em->persist($var1->setFicha($ficha)->setOpcionesPregunta($this->getOpcionPregunta($em, '10', '127'))->setDescripcion($fichaEditable->EdadFormula)->setUnidades($fichaEditable->EdadFormula_units));
        $var1 = new FichaRespuestas(); $em->persist($var1->setFicha($ficha)->setOpcionesPregunta($this->getOpcionPregunta($em, '11', '127'))->setDescripcion($fichaEditable->EdadAlimentosSolidos)->setUnidades($fichaEditable->EdadAlimentosSolidos_units));
        $var1 = new FichaRespuestas(); $em->persist($var1->setFicha($ficha)->setOpcionesPregunta($this->getOpcionPregunta($em, '12', '127'))->setDescripcion($fichaEditable->EdadEnQueGateo)->setUnidades($fichaEditable->EdadEnQueGateo_units));
        $var1 = new FichaRespuestas(); $em->persist($var1->setFicha($ficha)->setOpcionesPregunta($this->getOpcionPregunta($em, '12', '126'))->setDescripcion($fichaEditable->EdadEnQueCamino)->setUnidades($fichaEditable->EdadEnQueCamino_units));
        $var1 = new FichaRespuestas(); $em->persist($var1->setFicha($ficha)->setOpcionesPregunta($this->getOpcionPregunta($em, '12', '125'))->setDescripcion($fichaEditable->EdadEnQueHablo)->setUnidades($fichaEditable->EdadEnQueHablo_units));

          $var1 = new FichaRespuestas(); $em->persist($var1->setFicha($ficha)->setOpcionesPregunta($this->getOpcionPregunta($em, '23', '127'))->setDescripcion($fichaEditable->OtrasEnfermeInf));
          $var1 = new FichaRespuestas(); $em->persist($var1->setFicha($ficha)->setOpcionesPregunta($this->getOpcionPregunta($em, '24', '127'))->setDescripcion($fichaEditable->PorCualMotivo));
          $var1 = new FichaRespuestas(); $em->persist($var1->setFicha($ficha)->setOpcionesPregunta($this->getOpcionPregunta($em, '25', '127'))->setDescripcion($fichaEditable->MotivoDeTratamiento));
          $var1 = new FichaRespuestas(); $em->persist($var1->setFicha($ficha)->setOpcionesPregunta($this->getOpcionPregunta($em, '26', '127'))->setDescripcion($fichaEditable->MedicamentosQueTomaActualmente));
          $var1 = new FichaRespuestas(); $em->persist($var1->setFicha($ficha)->setOpcionesPregunta($this->getOpcionPregunta($em, '27', '127'))->setDescripcion($fichaEditable->MotivoDeIntervencion));
          $var1 = new FichaRespuestas(); $em->persist($var1->setFicha($ficha)->setOpcionesPregunta($this->getOpcionPregunta($em, '28', '127'))->setDescripcion($fichaEditable->MotivoHospitalizacion));
          $var1 = new FichaRespuestas(); $em->persist($var1->setFicha($ficha)->setOpcionesPregunta($this->getOpcionPregunta($em, '29', '127'))->setDescripcion($fichaEditable->OtrasVacunas));

          // $var1 = new FichaRespuestas(); $em->persist($var1->setFicha($ficha)->setOpcionesPregunta($this->getOpcionPregunta($em, '32', '127'))->setDescripcion($fichaEditable->EdadErupcionDeDientesPrimarios));
          // $var1 = new FichaRespuestas(); $em->persist($var1->setFicha($ficha)->setOpcionesPregunta($this->getOpcionPregunta($em, '33', '127'))->setDescripcion($fichaEditable->EdadErupcionDeDientesPermanentes));
          $var1 = new FichaRespuestas(); $em->persist($var1->setFicha($ficha)->setOpcionesPregunta($this->getOpcionPregunta($em, '35', '127'))->setDescripcion($fichaEditable->EdadTraumatismoDentario)->setUnidades($fichaEditable->EdadTraumatismoDentario_units));
          // $var1 = new FichaRespuestas(); $em->persist($var1->setFicha($ficha)->setOpcionesPregunta($this->getOpcionPregunta($em, '35', '126'))->setDescripcion($fichaEditable->TratamientoRecibido));
          // $var1 = new FichaRespuestas(); $em->persist($var1->setFicha($ficha)->setOpcionesPregunta($this->getOpcionPregunta($em, '37', '127'))->setDescripcion($fichaEditable->VecesAlDiaQueSeCepilla));
          // $var1 = new FichaRespuestas(); $em->persist($var1->setFicha($ficha)->setOpcionesPregunta($this->getOpcionPregunta($em, '37', '126'))->setDescripcion($fichaEditable->QuienLoCepilla));
          $var1 = new FichaRespuestas(); $em->persist($var1->setFicha($ficha)->setOpcionesPregunta($this->getOpcionPregunta($em, '31', '127'))->setDescripcion($fichaEditable->EsquemaIncompleto));

          $var1 = new FichaRespuestas(); $em->persist($var1->setFicha($ficha)->setOpcionesPregunta($this->getOpcionPregunta($em, '80', '127'))->setDescripcion($fichaEditable->QueCome));
          $var1 = new FichaRespuestas(); $em->persist($var1->setFicha($ficha)->setOpcionesPregunta($this->getOpcionPregunta($em, '42', '127'))->setDescripcion($fichaEditable->CualesAlimentosBlandos));
          $var1 = new FichaRespuestas(); $em->persist($var1->setFicha($ficha)->setOpcionesPregunta($this->getOpcionPregunta($em, '43', '127'))->setDescripcion($fichaEditable->AlimentosSolidosQuePrefiere));

          $var1 = new FichaRespuestas(); $em->persist($var1->setFicha($ficha)->setOpcionesPregunta($this->getOpcionPregunta($em, '48', '127'))->setDescripcion($fichaEditable->DesayunoHabitual ));
          $var1 = new FichaRespuestas(); $em->persist($var1->setFicha($ficha)->setOpcionesPregunta($this->getOpcionPregunta($em, '48', '126'))->setDescripcion($fichaEditable->MeriendaHabitual));
          $var1 = new FichaRespuestas(); $em->persist($var1->setFicha($ficha)->setOpcionesPregunta($this->getOpcionPregunta($em, '48', '125'))->setDescripcion($fichaEditable->AlmuerzoHabitual));
          $var1 = new FichaRespuestas(); $em->persist($var1->setFicha($ficha)->setOpcionesPregunta($this->getOpcionPregunta($em, '48', '124'))->setDescripcion($fichaEditable->MeriendaHabitualTarde));
          $var1 = new FichaRespuestas(); $em->persist($var1->setFicha($ficha)->setOpcionesPregunta($this->getOpcionPregunta($em, '48', '123'))->setDescripcion($fichaEditable->CenaHabitual));

          $var1 = new FichaRespuestas(); $em->persist($var1->setFicha($ficha)->setOpcionesPregunta($this->getOpcionPregunta($em, '49', '127'))->setDescripcion($fichaEditable->TallaActual)->setUnidades($fichaEditable->TallaActual_units));
          $var1 = new FichaRespuestas(); $em->persist($var1->setFicha($ficha)->setOpcionesPregunta($this->getOpcionPregunta($em, '50', '127'))->setDescripcion($fichaEditable->PesoActual)->setUnidades($fichaEditable->PesoActual_units));

          $var1 = new FichaRespuestas(); $em->persist($var1->setFicha($ficha)->setOpcionesPregunta($this->getOpcionPregunta($em, '56', '127'))->setDescripcion($fichaEditable->Descripcion));

          $var1 = new FichaRespuestas(); $em->persist($var1->setFicha($ficha)->setOpcionesPregunta($this->getOpcionPregunta($em, '57', '127'))->setDescripcion($fichaEditable->DescEncia));
          $var1 = new FichaRespuestas(); $em->persist($var1->setFicha($ficha)->setOpcionesPregunta($this->getOpcionPregunta($em, '57', '126'))->setDescripcion($fichaEditable->DescMucosaBucal));
          $var1 = new FichaRespuestas(); $em->persist($var1->setFicha($ficha)->setOpcionesPregunta($this->getOpcionPregunta($em, '57', '125'))->setDescripcion($fichaEditable->DescPisoBucal));
          $var1 = new FichaRespuestas(); $em->persist($var1->setFicha($ficha)->setOpcionesPregunta($this->getOpcionPregunta($em, '57', '124'))->setDescripcion($fichaEditable->DescPaladarDuro));
          $var1 = new FichaRespuestas(); $em->persist($var1->setFicha($ficha)->setOpcionesPregunta($this->getOpcionPregunta($em, '57', '123'))->setDescripcion($fichaEditable->DescPaladarBlando));
          $var1 = new FichaRespuestas(); $em->persist($var1->setFicha($ficha)->setOpcionesPregunta($this->getOpcionPregunta($em, '57', '122'))->setDescripcion($fichaEditable->DescAmigdalas));

          $var1 = new FichaRespuestas(); $em->persist($var1->setFicha($ficha)->setOpcionesPregunta($this->getOpcionPregunta($em, '59', '127'))->setDescripcion($fichaEditable->ExpliqueHabitosParafuncionales));

          $var1 = new FichaRespuestas(); $em->persist($var1->setFicha($ficha)->setOpcionesPregunta($this->getOpcionPregunta($em, '66', '127'))->setDescripcion($fichaEditable->Overjet));
          $var1 = new FichaRespuestas(); $em->persist($var1->setFicha($ficha)->setOpcionesPregunta($this->getOpcionPregunta($em, '67', '127'))->setDescripcion($fichaEditable->Overbite));
          $var1 = new FichaRespuestas(); $em->persist($var1->setFicha($ficha)->setOpcionesPregunta($this->getOpcionPregunta($em, '68', '127'))->setDescripcion($fichaEditable->LineaMediaSuperiorOpts));
          $var1 = new FichaRespuestas(); $em->persist($var1->setFicha($ficha)->setOpcionesPregunta($this->getOpcionPregunta($em, '69', '127'))->setDescripcion($fichaEditable->LineaMediaInferiorOpts));
          $var1 = new FichaRespuestas(); $em->persist($var1->setFicha($ficha)->setOpcionesPregunta($this->getOpcionPregunta($em, '81', '127'))->setDescripcion($fichaEditable->Diastemas));
          $var1 = new FichaRespuestas(); $em->persist($var1->setFicha($ficha)->setOpcionesPregunta($this->getOpcionPregunta($em, '74', '127'))->setDescripcion($fichaEditable->PerdidasPrematura));
          $var1 = new FichaRespuestas(); $em->persist($var1->setFicha($ficha)->setOpcionesPregunta($this->getOpcionPregunta($em, '82', '127'))->setDescripcion($fichaEditable->OtrasObservaciones));

          $var1 = new FichaRespuestas(); $em->persist($var1->setFicha($ficha)->setOpcionesPregunta($this->getOpcionPregunta($em, '83', '127'))->setDescripcion($fichaEditable->Hobby_exp));
          $var1 = new FichaRespuestas(); $em->persist($var1->setFicha($ficha)->setOpcionesPregunta($this->getOpcionPregunta($em, '84', '127'))->setDescripcion($fichaEditable->Deportes_exp ));
          $var1 = new FichaRespuestas(); $em->persist($var1->setFicha($ficha)->setOpcionesPregunta($this->getOpcionPregunta($em, '85', '127'))->setDescripcion($fichaEditable->Mascotas_exp ));
          $var1 = new FichaRespuestas(); $em->persist($var1->setFicha($ficha)->setOpcionesPregunta($this->getOpcionPregunta($em, '86', '127'))->setDescripcion($fichaEditable->ProgramasTV_exp));
          $var1 = new FichaRespuestas(); $em->persist($var1->setFicha($ficha)->setOpcionesPregunta($this->getOpcionPregunta($em, '87', '127'))->setDescripcion($fichaEditable->Musica_exp));
          $var1 = new FichaRespuestas(); $em->persist($var1->setFicha($ficha)->setOpcionesPregunta($this->getOpcionPregunta($em, '88', '127'))->setDescripcion($fichaEditable->Fobias_exp));
          $var1 = new FichaRespuestas(); $em->persist($var1->setFicha($ficha)->setOpcionesPregunta($this->getOpcionPregunta($em, '89', '127'))->setDescripcion($fichaEditable->Recompensas_exp));
          $var1 = new FichaRespuestas(); $em->persist($var1->setFicha($ficha)->setOpcionesPregunta($this->getOpcionPregunta($em, '90', '127'))->setDescripcion($fichaEditable->Castigo_exp));
          $var1 = new FichaRespuestas(); $em->persist($var1->setFicha($ficha)->setOpcionesPregunta($this->getOpcionPregunta($em, '91', '127'))->setDescripcion($fichaEditable->OtrasActividades_exp));
          $var1 = new FichaRespuestas(); $em->persist($var1->setFicha($ficha)->setOpcionesPregunta($this->getOpcionPregunta($em, '92', '127'))->setDescripcion($fichaEditable->Aprendizaje_exp));
          $var1 = new FichaRespuestas(); $em->persist($var1->setFicha($ficha)->setOpcionesPregunta($this->getOpcionPregunta($em, '94', '127'))->setDescripcion($fichaEditable->Exp_enConsulta));

          $var1 = new FichaRespuestas(); $em->persist($var1->setFicha($ficha)->setOpcionesPregunta($this->getOpcionPregunta($em, '93', '121'))->setDescripcion($fichaEditable->DiagnosticoMedico));
          $var1 = new FichaRespuestas(); $em->persist($var1->setFicha($ficha)->setOpcionesPregunta($this->getOpcionPregunta($em, '93', '122'))->setDescripcion($fichaEditable->DiagnosticoPeriodontal));
          $var1 = new FichaRespuestas(); $em->persist($var1->setFicha($ficha)->setOpcionesPregunta($this->getOpcionPregunta($em, '93', '123'))->setDescripcion($fichaEditable->TipoDeCaries));
          $var1 = new FichaRespuestas(); $em->persist($var1->setFicha($ficha)->setOpcionesPregunta($this->getOpcionPregunta($em, '93', '124'))->setDescripcion($fichaEditable->AlteracionesPulpares));
          $var1 = new FichaRespuestas(); $em->persist($var1->setFicha($ficha)->setOpcionesPregunta($this->getOpcionPregunta($em, '93', '125'))->setDescripcion($fichaEditable->PatologiasTejBlandoDuro ));
          $var1 = new FichaRespuestas(); $em->persist($var1->setFicha($ficha)->setOpcionesPregunta($this->getOpcionPregunta($em, '93', '126'))->setDescripcion($fichaEditable->Maloclusiones));
          $var1 = new FichaRespuestas(); $em->persist($var1->setFicha($ficha)->setOpcionesPregunta($this->getOpcionPregunta($em, '93', '127'))->setDescripcion($fichaEditable->RiesgoCariogenico));

        foreach ($old_atributos as $old_attr) {
          if($old_attr)
          $em->remove($old_attr);
        }
        foreach ($old_selects as $old_attr) {
          if($old_attr)
            // $em->remove($this->getFichaRespuestasByOptions($em, $ficha, $old_attr));
            $em->remove($old_attr);
        }


        foreach ($atributos as $attr) {
          $resp = new FichaRespuestas();
          $resp->setFicha($ficha);
          $resp->setOpcionesPregunta($attr);
          if($attr->getId()>=60 and $attr->getId()<=81 ) $resp->setDescripcion($fichaEditable->{'TA'.$attr->getId()});

          $em->persist($resp);
        }



        foreach ($atributosSelect as $attr) {
          if($attr != Null){
          $resp = new FichaRespuestas();
          $resp->setFicha($ficha);
          $resp->setOpcionesPregunta($attr);
          $em->persist($resp);
          }
        }


        $em->flush();

        $flow->reset();
        $this->addFlash('success', 'Tus cambios fueron guardados');
        return $this->redirectToRoute('fichas', ['expedienteId' => $expedienteId]);
      }

    }


    return $this->render('clinico/fichas/edit.html.twig', [
      'form' => $form->createView(),
      'flow' => $flow,
      'expedienteId' => $expedienteId,
      'fichaId' => $fichaId,
      'expediente' => $expediente,
      'edad' => $edad
    ]);

  }

  /**
   * @Route("/clinico/expedientes/{expedienteId}/fichas_ped/{fichaId}/detalle", name="ficha_ped_expandida", methods="GET")
   */
  public function showExpanded($expedienteId, $fichaId): Response
  {
      $em = $this->getDoctrine()->getManager();
      $usuario = $this->getUser();
      $ficha = $em->getRepository(Ficha::class)->findOneBy(['id' => $fichaId]);
      $datosClinicos = $em->getRepository(DatosClinicos::class)->findOneBy(['ficha' => $fichaId]);
      $expediente = $em->getRepository(ExpedienteClinico::class)->findOneBy(['id' => $expedienteId]);
      $persona = $expediente->getPersona();
      $id_cuenta = $ficha->getCuenta()->getId();
      $fichaInfo = $this->tablaFichaService->getTableInfo($expedienteId, $fichaId);

      $canEdit = $this->tablaFichaService->allowEdit($usuario, $ficha);

      $fnacimiento = $persona->getNacimiento();
      $fhoy = new \DateTime();
      $edad = $fhoy->diff($fnacimiento)->y;
      $sexo = $em->getRepository(PersonaAtributo::class)->findOneByPersonaAndTipoCatalogo($persona->getId(), 'sexo')->getAtributo()->getNombre();
      if($edad > 18 && $sexo=='Femenino') $isMujer = true;

      $respuestas=$this->getRespuestasByFicha($em, $ficha);
      $opciones = $em->getRepository(OpcionesPregunta::class)->createQueryBuilder('a')->orderBy('a.pregunta_compleja')->getQuery()->execute();;
      $preguntas = $em->getRepository(Pregunta::class)->findAll();
      $categorias = $em->getRepository(Categoria::class)->findAll();

      return $this->render('clinico/fichas/expanded_ped.html.twig', [
        'expedienteId' => $expedienteId,
        'expediente' => $expediente,
        'fichaId' => $fichaId,
        'fichaInfo' => $fichaInfo,
        'ficha' => $ficha,
        'respuestas' => $respuestas,
        'opciones' => $opciones,
        'preguntas' => $preguntas,
        'categorias' => $categorias,

        'editable' => $canEdit
      ]);
  }

  function getRespuestasByFicha($em, $ficha){
    $qb = $em->createQueryBuilder('u');
    $qb->select('u')
        ->from('App\Entity\Clinico\FichaRespuestas', 'u')
        ->innerJoin('u.opciones_pregunta' , 'op')
        ->andWhere('u.ficha = :ficha')
        ->setParameter('ficha', $ficha);
    $query = $qb->getQuery();
    return $query->getResult();
  }



  /**
   * @Route("/clinico/expedientes/{expedienteId}/fichas_ped/{fichaId}/cerrar", name="ficha_ped_cerrar", methods="GET")
   */
  public function close($expedienteId, $fichaId): Response
  {
    $em = $this->getDoctrine()->getManager();
    $usuario = $this->getUser();
    $expediente = $em->getRepository(ExpedienteClinico::class)->findOneBy(['id' => $expedienteId]);
    $ficha = $em->getRepository(Ficha::Class)->findOneBy(['id' => $fichaId]);

    $canEdit = $this->tablaFichaService->allowEdit($usuario, $ficha);

    if(!$canEdit){
      $this->addFlash('warning', 'No esta autorizado para cerrar esa ficha');
      return $this->redirectToRoute('fichas', ['expedienteId' => $expedienteId]);
    }

    $ficha->setFinalizada(1);
    $em->flush();

    $this->addFlash('success', 'Ficha cerrada');
    return $this->redirectToRoute('ficha_detalle', ['expedienteId' => $expedienteId, 'fichaId' => $fichaId]);

  }

  /**
   * @Route("/clinico/expedientes/{expedienteId}/fichas_ped/{fichaId}/abrir", name="ficha_ped_abrir", methods="GET")
   * @Security("has_role('ROLE_ADMIN')")
   */
  public function open($expedienteId, $fichaId): Response{
    $em = $this->getDoctrine()->getManager();
    $usuario = $this->getUser();
    $expediente = $em->getRepository(ExpedienteClinico::class)->findOneBy(['id' => $expedienteId]);
    $ficha = $em->getRepository(Ficha::Class)->findOneBy(['id' => $fichaId]);

    $canOpen = $ficha->getFinalizada();

    if(!$canOpen){
      $this->addFlash('warning', 'La ficha no se encuentra cerrada');
      return $this->redirectToRoute('fichas', ['expedienteId' => $expedienteId]);
    }

    $ficha->setFinalizada(0);
    $em->flush();

    $this->addFlash('success', 'Ficha abierta');
    return $this->redirectToRoute('ficha_detalle', ['expedienteId' => $expedienteId, 'fichaId' => $fichaId]);
  }

function getByNombrePregunta($em, $nombre, $ficha){
  $qb = $em->createQueryBuilder('u');
  $qb->select('u')
      ->from('App\Entity\Clinico\FichaRespuestas', 'u')
      ->innerJoin('u.opciones_pregunta' , 'op')
      ->innerJoin('op.pregunta', 'p')
      ->andWhere('p.nombre_preg = :nombre')
      ->andWhere('u.ficha = :ficha')
      ->setParameter('nombre', $nombre)
      ->setParameter('ficha', $ficha);
  $query = $qb->getQuery();
  return $query->getResult();
}

function getOneByNombreAndPreguntaCompleja($em, $nombre, $ficha, $complejidad){
  $qb = $em->createQueryBuilder('u');
  $qb->select('u')
      ->from('App\Entity\Clinico\FichaRespuestas', 'u')
      ->innerJoin('u.opciones_pregunta' , 'op')
      ->innerJoin('op.pregunta', 'p')
      ->andWhere('p.nombre_preg = :nombre')
      ->andWhere('u.ficha = :ficha')
      ->andWhere('op.pregunta_compleja = :idComplex')
      ->setParameter('nombre', $nombre)
      ->setParameter('ficha', $ficha)
      ->setParameter('idComplex', $complejidad);
  $query = $qb->getQuery();
  // return $query->getSingleResult()->getOpcionesPregunta();
  return $query->getOneOrNullResult();
}

function getByNombreAndPreguntaCompleja($em, $nombre, $ficha, $complejidad){
  $qb = $em->createQueryBuilder('u');
  $qb->select('u')
      ->from('App\Entity\Clinico\FichaRespuestas', 'u')
      ->innerJoin('u.opciones_pregunta' , 'op')
      ->innerJoin('op.pregunta', 'p')
      ->andWhere('p.nombre_preg = :nombre')
      ->andWhere('u.ficha = :ficha')
      ->andWhere('op.pregunta_compleja = :idComplex')
      ->setParameter('nombre', $nombre)
      ->setParameter('ficha', $ficha)
      ->setParameter('idComplex', $complejidad);
  $query = $qb->getQuery();
  // return $query->getSingleResult()->getOpcionesPregunta();
  return $query->getResult();
}

function getRespuestaByPregunta($em, $nombre){
  $qb = $em->createQueryBuilder('u');
  $qb->select('u')
      ->from('App\Entity\Clinico\FichaRespuestas', 'u')
      ->innerJoin('u.respuesta' , 'r')
      ->innerJoin('r.opciones_pregunta', 'op')
      ->where('op.id = :nombre')
      ->setParameter('nombre', $nombre);
  $query = $qb->getQuery();
  return $query->getResult();
}

function getFichaRespuestasByOptions($em, $ficha, $opciones_pregunta){
  $qb = $em->createQueryBuilder('u');
  $qb->select('u')
      ->from('App\Entity\Clinico\FichaRespuestas', 'u')
      ->innerJoin('u.opciones_pregunta' , 'op')
      ->innerJoin('op.pregunta', 'p')
      ->andWhere('u.ficha = :ficha')
      ->andWhere('u.opciones_pregunta = :opt_preg')
      ->setParameter('opt_preg', $opciones_pregunta)
      ->setParameter('ficha', $ficha)
      ;
  $query = $qb->getQuery();
  // return $query->getSingleResult()->getOpcionesPregunta();
  return $query->getOneOrNullResult();
}

function getOpcionPregunta($em, $preg_id, $opt_complex){
  $qb = $em->createQueryBuilder('u');
  $qb->select('u')
      ->from('App\Entity\Clinico\OpcionesPregunta', 'u')
      // ->innerJoin('u.opciones_pregunta' , 'op')
      ->innerJoin('u.pregunta', 'p')
      ->andWhere('u.pregunta_compleja = :optc')
      ->andWhere('p.id = :id')
      ->setParameter('id', $preg_id)
      ->setParameter('optc', $opt_complex)
      ;
  $query = $qb->getQuery();
  // return $query->getSingleResult()->getOpcionesPregunta();
  return $query->getOneOrNullResult();
}

//Defini valor de complejidad 127 para indicar que se desea la descripcion
//La funcion asume complejidad = 127
//Auxiliar para obtener dos valores distintos luego, DEscripcion y unidades.
function getRespuestasAux($em, $nombre, $ficha, $complejidad='127'){
  $qb = $em->createQueryBuilder('u');
  $qb->select('u')
      ->from('App\Entity\Clinico\FichaRespuestas', 'u')
      ->innerJoin('u.opciones_pregunta' , 'op')
      ->innerJoin('op.pregunta', 'p')
      ->andWhere('u.ficha = :ficha')
      ->andWhere('p.nombre_preg = :nombre')
      ->andWhere('op.pregunta_compleja = :complex')
      ->setParameter('ficha', $ficha)
      ->setParameter('nombre', $nombre)
      ->setParameter('complex', $complejidad)
      ;
      return $query = $qb->getQuery();

}

function getValorEscrito($em, $nombre, $ficha, $complejidad='127'){
    $query = $this->getRespuestasAux($em, $nombre, $ficha, $complejidad);
    if($query->getOneOrNullResult()) return $query->getOneOrNullResult()->getDescripcion();
    else return null;
}

function getUnidades($em, $nombre, $ficha, $complejidad='127'){
  $query = $this->getRespuestasAux($em, $nombre, $ficha, $complejidad);
  if($query->getOneOrNullResult()) return $query->getOneOrNullResult()->getUnidades();
  else return null;
}



function getAllTexts($em, $ficha){
  $qb = $em->createQueryBuilder('u');
  $qb->select('u')
      ->from('App\Entity\Clinico\FichaRespuestas', 'u')
      ->innerJoin('u.opciones_pregunta' , 'op')
      ->andWhere('u.ficha = :ficha')
      ->andWhere('op.pregunta_compleja > 120')
      ->setParameter('ficha', $ficha)
      ;
  $query = $qb->getQuery();
  // return $query->getSingleResult()->getOpcionesPregunta();
  return $query->getResult();
}

function getDescripcionByOpcionId($em, $ficha, $i){
  $qb = $em->createQueryBuilder('u');
  $qb->select('u')
      ->from('App\Entity\Clinico\FichaRespuestas', 'u')
      ->innerJoin('u.opciones_pregunta' , 'op')
      ->andWhere('u.ficha = :ficha')
      ->andWhere('op.id = :id')
      ->setParameter('ficha', $ficha)
      ->setParameter('id', $i)
      ;
  $query = $qb->getQuery();
  // return $query->getSingleResult()->getOpcionesPregunta();
  if($query->getOneOrNullResult()) return $query->getOneOrNullResult()->getDescripcion();
  return "";
}



}
