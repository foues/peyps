<?php

namespace App\Controller\Clinico;

use App\Form\Clinico\expedientes\CrearExpediente;
use App\Entity\Administracion\Catalogo;
use App\Entity\Administracion\Persona;
use App\Entity\Administracion\Cuenta;
use App\Entity\Administracion\Telefono;
use App\Entity\Administracion\Direccion;
use App\Entity\Administracion\Email;
use App\Entity\Administracion\PersonaAtributo;
use App\Entity\Clinico\ExpedienteClinico;
use App\Form\Clinico\expedientes\ExpedienteFlow;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Clinico\FichaCatalogo;
use App\Entity\Clinico\Ficha;
use Datetime;

class ExpedienteController extends AbstractController
{
    /**
     * ExpedienteController constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->denyAccessUnlessGranted('expediente_clinico');
    }

    // /clinico/expedientes/
    /**
     * @Route("/", name="expedientes", methods="GET")
     */
    public function list(): Response
    {
      $repository = $this->getDoctrine()->getRepository(ExpedienteClinico::class);
      $expedientes = $repository->findAll();
      return $this->render('clinico/expedientes/list.html.twig', array(
          'controller_name' => 'ExpedienteController',
          'expedientes' => $expedientes,
      ));
    }

    /**
     * @Route("/clinico/expedientes/nuevo", name="expediente_nuevo", methods="GET|POST")
     */
    public function new(Request $request, ExpedienteFlow $flow): Response
    {

        $usuario = $this->getUser();
        $expediente = new CrearExpediente();
//        $flow = $this->get('app.form.flow.expedienteFlow');
        $flow->bind($expediente);


        $form = $submittedForm = $flow->createForm();

        if ($flow->isValid($submittedForm)) {
            $flow->saveCurrentStepData($form);

            if(!$this->isCodigoValid($flow->getCurrentStepNumber(), $expediente->codigo, $expediente->fecha_nacimiento)){
                $this->addFlash('warning', 'Ya existe un expediente con ese codigo');
            } else {
                if ($flow->nextStep()) {
                $form = $flow->createForm();
                } else {


                    $em = $this->getDoctrine()->getManager();
                    // Cuenta
                    $cuenta = new Cuenta();

                    // Persona
                    $persona = new Persona();
                    $persona->setNombre($expediente->nombres);
                    $persona->setApellido($expediente->apellidos);
                    $persona->setNacimiento($expediente->fecha_nacimiento);
                    if(!is_null($expediente->dui)) $persona->setDui($expediente->dui);
                    //if(!is_null($expediente->nit)) $persona->setNit($expediente->nit);

                    $persona->setCuenta($cuenta);

                    // Expediente
                    $expediente_clinico = new ExpedienteClinico();
                    $expediente_clinico->setPersona($persona);
                    $tipo_expediente = $em->getRepository(FichaCatalogo::class)->findOneBy(['nombre' => 'Preventiva']);
                    $expediente_clinico->setExpedienteClinicoTipo($tipo_expediente);
                    $expediente_clinico->setCodigoExpediente($this->createCodigo($expediente->codigo, $expediente->fecha_nacimiento));

                    $em->flush();

                    // Direccion
                    $direccion = new Direccion();
                    $direccion->setCalle($expediente->residencia);
                    $direccion->setDireccion($expediente->direccion);
                    $direccion->setDireccionArea($expediente->departamento);
                    $direccion->setDivisionPolitica($expediente->municipio);
                    $direccion->setDireccionTipo($expediente->direccion_tipo);
                    $direccion->setCuenta($cuenta);
                    $em->persist($direccion);



                    // Telefonos
                    if(!is_null($expediente->telefono_casa)){
                        $telefono_casa = new Telefono();
                        $telefono_casa->setNumero($expediente->telefono_casa);
                        $tipo_casa = $em->getRepository(Catalogo::class)->findOneBy(['nombre' => 'Telefono Fijo']);
                        $telefono_casa->setTelefonoTipo($tipo_casa);
                        $telefono_casa->setContacto('Casa');
                        $telefono_casa->setCuenta($cuenta);
                        $em->persist($telefono_casa);
                    }

                    if(!is_null($expediente->telefono_movil)){
                        $telefono_movil = new Telefono();
                        $telefono_movil->setNumero($expediente->telefono_movil);
                        $tipo_movil = $em->getRepository(Catalogo::class)->findOneBy(['nombre' => 'Telefono Movil']);
                        $telefono_movil->setTelefonoTipo($tipo_movil);
                        $telefono_movil->setContacto('Movil');
                        $telefono_movil->setCuenta($cuenta);
                        $em->persist($telefono_movil);
                    }

                    if(!is_null($expediente->telefono_trabajo) || !is_null($expediente->lugar_trabajo)){
                        $telefono_trabajo = new Telefono();
                        $telefono_trabajo->setNumero(!is_null($expediente->telefono_trabajo) ? $expediente->telefono_trabajo : '');
                        $tipo_trabajo = $em->getRepository(Catalogo::class)->findOneBy(['nombre' => 'Telefono Trabajo']);
                        $telefono_trabajo->setTelefonoTipo($tipo_trabajo);
                        $telefono_trabajo->setContacto($expediente->lugar_trabajo);
                        $telefono_trabajo->setCuenta($cuenta);
                        $em->persist($telefono_trabajo);

                    }

                    if(!is_null($expediente->telefono_apoderado) || !is_null($expediente->apoderado)){
                        $telefono_apoderado = new Telefono();
                        $telefono_apoderado->setNumero(!is_null($expediente->telefono_apoderado) ? $expediente->telefono_apoderado : '');
                        $tipo_apoderado = $em->getRepository(Catalogo::class)->findOneBy(['nombre' => 'Telefono Apoderado']);
                        $telefono_apoderado->setTelefonoTipo($tipo_apoderado);
                        $telefono_apoderado->setContacto($expediente->apoderado);
                        $telefono_apoderado->setCuenta($cuenta);
                        $em->persist($telefono_apoderado);
                    }

                    // Email
                    if(!is_null($expediente->email)){
                        $email = new Email();
                        $email->setAsignadoA("Paciente");
                        $email->setEmail($expediente->email);
                        $email->setCuenta($cuenta);
                        $em->persist($email);
                    }

                    if(!is_null($expediente->email_apoderado)){
                      $email_apoderado = new Email();
                      $email_apoderado->setAsignadoA("Apoderado");
                      $email_apoderado->setEmail($expediente->email_apoderado);
                      $email_apoderado->setCuenta($cuenta);
                      $em->persist($email_apoderado);
                    }

                    if(!is_null($expediente->dui_apoderado)){
                      $persona->setDuiApoderado($expediente->dui_apoderado);
                    }

                    if(!is_null($expediente->centro_educativo_paciente)){
                      $persona->setCentroEducativo($expediente->centro_educativo_paciente);
                    }

                    /** @var UploadedFile $archivo_consentimiento */
                    //$archivo_consentimiento = $form['CrearExpediente']->getData();
                    $archivo_consentimiento = $request->files->get('post ')['archivo_consentimiento'];

                     // this condition is needed because the field is not required
                     // so the PDF file must be processed only when a file is uploaded
                     if ($archivo_consentimiento) {
                         $originalFilename = pathinfo($archivo_consentimiento->getClientOriginalName(), PATHINFO_FILENAME);
                         // this is needed to safely include the file name as part of the URL
                         $safeFilename = transliterator_transliterate('Any-Latin; Latin-ASCII; [^A-Za-z0-9_] remove; Lower()', $originalFilename);
                         $safe_archivo_consentimiento = $safeFilename.'-'.uniqid().'.'.$brochureFile->guessExtension();

                         // Move the file to the directory where brochures are stored
                         try {
                             $archivo_consentimiento->move(
                                 $this->getParameter('consentimientos_directory'),
                                 $safe_archivo_consentimiento
                             );
                         } catch (FileException $e) {
                             // ... handle exception if something happens during file upload
                         }

                         // updates the 'Filename' property to store the PDF file name
                         // instead of its contents
                         $persona->setArchivoConsentimiento($safe_archivo_consentimiento);
                     }

                    // Atributos

                    // Tipo de paciente
                    // $attr_tipo = new PersonaAtributo();


                    // $tipo;
                    // if($edad < 12) $tipo = 'Niño';
                    // else if($edad < 18) $tipo = 'Adolescente';
                    // else $tipo = 'Adulto';

                    // $attr_tipo->setAtributo($em->getRepository(Catalogo::class)->findOneBy(['nombre' => $tipo]));
                    // $attr_tipo->setPersona($persona);
                    // $em->persist($attr_tipo);

                    // Sexo del paciente
                    $attr_sexo = new PersonaAtributo();
                    $attr_sexo->setAtributo($expediente->sexo);
                    $attr_sexo->setPersona($persona);
                    $em->persist($attr_sexo);

                    // Estado civil
                    if(!is_null($expediente->estado_civil)){
                        $attr_civil = new PersonaAtributo();
                        $attr_civil->setAtributo($expediente->estado_civil);
                        $attr_civil->setPersona($persona);
                        $em->persist($attr_civil);
                    }

                    // Profesion
                    if(!is_null($expediente->profesion)){
                        $attr_profesion = new PersonaAtributo();
                        $attr_profesion->setAtributo($expediente->profesion);
                        $attr_profesion->setPersona($persona);
                        $em->persist($attr_profesion);
                    }

                    // Nivel educativo
                    $attr_educativo = new PersonaAtributo();
                    $attr_educativo->setAtributo($expediente->nivel_educativo);
                    $attr_educativo->setPersona($persona);
                    $em->persist($attr_educativo);

                    // Ocupacion laboral
                    if(!is_null($expediente->ocupacion)){
                        $attr_ocupacion = new PersonaAtributo();
                        $attr_ocupacion->setAtributo($expediente->ocupacion);
                        $attr_ocupacion->setPersona($persona);
                        $em->persist($attr_ocupacion);
                    }


                    // Fin atributos

                    $em->persist($cuenta);
                    $em->persist($persona);
                    $em->persist($expediente_clinico);

                    $em->flush();

                    $flow->reset();
                    $this->addFlash('success', 'Tus cambios han sido guardados!');
                    return $this->redirectToRoute('expedientes');
                }
            }
        }

        return $this->render('clinico/expedientes/new.html.twig', [
          'form' => $form->createView(),
          'flow' => $flow
        ]);
    }

    /**
     * @Route("/clinico/expedientes/{expedienteId}", name="expediente_detalle", methods="GET")
     */
    public function show($expedienteId): Response
    {
        $repository = $this->getDoctrine()->getRepository(ExpedienteClinico::class);
        $expediente = $repository->find($expedienteId);

        $repoExpediente= $this->getDoctrine()->getRepository(PersonaAtributo::class);
        $atributos = $repoExpediente->findBy(array('persona'=>$expediente->getPersona()));

        $repoTelefono = $this->getDoctrine()->getRepository(Telefono::class);
        $telefonos = $repoTelefono->findBy(array('cuenta'=>$expediente->getPersona()->getCuenta()));

        $repoDir = $this->getDoctrine()->getRepository(Direccion::class);
        $direccion = $repoDir->findOneBy(array('cuenta'=>$expediente->getPersona()->getCuenta()));

        $repoEmail = $this->getDoctrine()->getRepository(Email::class);
        $emails = $repoEmail->findBy(array('cuenta'=>$expediente->getPersona()->getCuenta()));

        //Agregados
        $email = new Email();
        $email_apoderado = new Email();

        foreach ($emails as $e) {
          if($e->getAsignadoA() == "Paciente")
            $email = $e;
          else
            $email_apoderado =  $e;
        }


        $repoFicha = $this->getDoctrine()->getRepository(Ficha::class);
        $fichas_expediente = $repoFicha->findBy(['expediente_clinico' => $expediente]);

        $fichasTrabajadas = sizeof($fichas_expediente);

        $estudiantes = [];
        $fechas = [];
        foreach ($fichas_expediente as $f) {
            $estudiante = $this->getDoctrine()->getRepository(Persona::class)->findOneBy(['cuenta' => $f->getCuenta()])->getNombreCompleto();
            $creado = $f->getFechaControl();
            array_push($estudiantes, $estudiante);
            array_push($fechas, $creado);
        }

        $fichas = array_map(null, $estudiantes, $fechas);



        return $this->render('clinico/expedientes/show.html.twig', array(
            'expedienteId' => $expedienteId,
            'nacimiento' => $expediente->getPersona()->getNacimiento(),
            'expediente' => $expediente,
            'atributos' => $atributos,
            'telefonos'=> $telefonos,
            'direccion'=> $direccion,
            'email'=> $email,
            'email_apoderado'=> $email_apoderado,
            'fichas_trabajadas' => $fichasTrabajadas,
            'fichas' => $fichas,
        ));
    }

    /**
     * @Route("/clinico/expedientes/{expedienteId}/editar", name="expediente_editar", methods="GET|POST")
     */
    public function edit($expedienteId, ExpedienteFlow $flow): Response
    {
        $usuario = $this->getUser();
        $em = $this->getDoctrine()->getManager();
        $repoAtributo = $em->getRepository(PersonaAtributo::class);
        $repoTelefono = $em->getRepository(Telefono::class);

        $expediente = $em->getRepository(ExpedienteClinico::class)->find($expedienteId);
        $persona = $expediente->getPersona();

        $expEditable = new CrearExpediente();
        $expEditable->codigo = substr($expediente->getCodigoExpediente(), 3);
        $expEditable->nombres = $persona->getNombre();
        $expEditable->apellidos = $persona->getApellido();
        $expEditable->fecha_nacimiento = $persona->getNacimiento();
        //$expEditable->nit = $persona->getNit();
        $sexo = $repoAtributo->findOneByPersonaAndTipoCatalogo($persona->getId(), 'sexo');
        if(!is_null($sexo)) $expEditable->sexo = $sexo->getAtributo();
        $telefono_casa = $repoTelefono->findOneByCuentaAndTipoTelefono($persona->getCuenta()->getId(), 'fijo');
        if(!is_null($telefono_casa)) $expEditable->telefono_casa = $telefono_casa->getNumero();
        $telefono_movil = $repoTelefono->findOneByCuentaAndTipoTelefono($persona->getCuenta()->getId(), 'movil');
        if(!is_null($telefono_movil)) $expEditable->telefono_movil = $telefono_movil->getNumero();
        $emails = $em->getRepository(Email::class)->findBy(['cuenta'=>$persona->getCuenta()]);

        //Agregados
        $email = new Email();
        $email_apoderado = new Email();

        foreach ($emails as $e) {
          if($e->getAsignadoA() == "Paciente"){
            $email = $e;
            $expEditable->email = $email->getEmail();
          }
           else {
            $email_apoderado = $e;
            $expEditable->email_apoderado = $email_apoderado->getEmail();
           }
        }


        //Nivel educativo
        $nivel_educativo = $repoAtributo->findOneByPersonaAndTipoCatalogo($persona->getId(), 'nivel_academico');
        if(!is_null($nivel_educativo)) $expEditable->nivel_educativo = $nivel_educativo->getAtributo();


        //Direccion
        $direccion = $em->getRepository(Direccion::class)->findOneBy(['cuenta'=>$persona->getCuenta()]);
        $expEditable->departamento = $direccion->getDireccionArea();
        $expEditable->municipio = $direccion->getDivisionPolitica();
        $expEditable->direccion = $direccion->getDireccion();
        $expEditable->residencia = $direccion->getCalle();
        $expEditable->direccion_tipo = $direccion->getDireccionTipo();


        $estado_civil = NULL;
        $profesion = NULL;
        $ocupacion = NULL;
        $telefono_trabajo = NULL;
        $telefono_apoderado = NULL;
        $dui_apoderado = NULL;
        $centro_educativo_paciente = NULL;

        if($persona->getTipoPaciente()=='Adulto'){
            //Es un adulto
            $expEditable->dui = $persona->getDui();
            $estado_civil = $repoAtributo->findOneByPersonaAndTipoCatalogo($persona->getId(), 'estado_civil');
            if(!is_null($estado_civil)) $expEditable->estado_civil = $estado_civil->getAtributo();
            $profesion = $repoAtributo->findOneByPersonaAndTipoCatalogo($persona->getId(), 'profesion');
            if(!is_null($profesion)) $expEditable->profesion = $profesion->getAtributo();
            $ocupacion = $repoAtributo->findOneByPersonaAndTipoCatalogo($persona->getId(), 'ocupacion');
            if(!is_null($ocupacion)) $expEditable->ocupacion = $ocupacion->getAtributo();
            $telefono_trabajo = $repoTelefono->findOneByCuentaAndTipoTelefono($persona->getCuenta()->getId(), 'teltr');
            if(!is_null($telefono_trabajo)){
                $expEditable->telefono_trabajo = $telefono_trabajo->getNumero();
                $expEditable->lugar_trabajo = $telefono_trabajo->getContacto();
            }


        } else{
            $telefono_apoderado = $repoTelefono->findOneByCuentaAndTipoTelefono($persona->getCuenta()->getId(), 'telap');
            if(!is_null($telefono_apoderado)){
                $expEditable->telefono_apoderado = $telefono_apoderado->getNumero();
                $expEditable->apoderado = $telefono_apoderado->getContacto();
            }

            if(!is_null($persona->getDuiApoderado())) $expEditable->dui_apoderado = $persona->getDuiApoderado();
            if(!is_null($persona->getDui())) $expEditable->dui = $persona->getDui();
            if(!is_null($persona->getCentroEducativo())) $expEditable->centro_educativo_paciente = $persona->getCentroEducativo();
        }

//        $flow = $this->get('app.form.flow.expedienteFlow');
        $flow->bind($expEditable);
        $form = $submittedForm = $flow->createForm();

        if ($flow->isValid($submittedForm)) {
            $flow->saveCurrentStepData($form);

            if(!$this->isCodigoValid($flow->getCurrentStepNumber(), $expEditable->codigo, $expEditable->fecha_nacimiento, $expedienteId)){
                $this->addFlash('warning', 'Ya existe un expediente con ese codigo');
            } else {
                if ($flow->nextStep()) {
                    $form = $flow->createForm();
                } else {
                    //Guardar cambios hechos al perfil

                    //Expediente
                    $expediente->setCodigoExpediente($this->createCodigo($expEditable->codigo, $expEditable->fecha_nacimiento));

                    // Persona
                    $persona->setNombre($expEditable->nombres);
                    $persona->setApellido($expEditable->apellidos);
                    $persona->setNacimiento($expEditable->fecha_nacimiento);
                    //$persona->setNit($expEditable->nit);

                    //Agregados
                    $persona->setDuiApoderado($expEditable->dui_apoderado);
                    $persona->setCentroEducativo($expEditable->centro_educativo_paciente);

                    // Direccion
                    $direccion->setCalle($expEditable->residencia);
                    $direccion->setDireccion($expEditable->direccion);
                    $direccion->setDireccionArea($expEditable->departamento);
                    $direccion->setDivisionPolitica($expEditable->municipio);
                    $direccion->setDireccionTipo($expEditable->direccion_tipo);

                    if($persona->getTipoPaciente()=='Adulto'){
                        //Es un adulto
                        $persona->setDui($expEditable->dui);
                        //Estado civil
                        if(!is_null($expEditable->estado_civil)){
                            //CREATE OR UPDATE
                            if(is_null($estado_civil)){
                                //CREATE
                                $attr_civil = new PersonaAtributo();
                                $attr_civil->setAtributo($expEditable->estado_civil);
                                $attr_civil->setPersona($persona);
                                $em->persist($attr_civil);
                            } else {
                                //UPDATE
                                $estado_civil->setAtributo($expEditable->estado_civil);
                            }

                        } else {
                            //REMOVE IT
                            if(!is_null($estado_civil)) $em->remove($estado_civil);
                        }

                        // Profesion
                        if(!is_null($expEditable->profesion)){
                            //CREATE OR UPDATE
                            if(is_null($profesion)){
                                //CREATE
                                $attr_profesion = new PersonaAtributo();
                                $attr_profesion->setAtributo($expEditable->profesion);
                                $attr_profesion->setPersona($persona);
                                $em->persist($attr_profesion);
                            } else {
                                //UPDATE
                                $profesion->setAtributo($expEditable->profesion);
                            }
                        }
                        else {
                            //REMOVE IT
                            if(!is_null($profesion)) $em->remove($profesion);
                        }

                        // Ocupacion laboral
                        if(!is_null($expEditable->ocupacion)){
                            //CREATE OR UPDATE
                            if(is_null($ocupacion)){
                                //CREATE
                                $attr_ocupacion = new PersonaAtributo();
                                $attr_ocupacion->setAtributo($expEditable->ocupacion);
                                $attr_ocupacion->setPersona($persona);
                                $em->persist($attr_ocupacion);
                            } else {
                                //UPDATE
                                $ocupacion->setAtributo($expEditable->ocupacion);
                            }

                        } else {
                            //REMOVE IT
                            if(!is_null($ocupacion)) $em->remove($ocupacion);
                        }


                        //Telefono trabajo
                        if(!is_null($expEditable->telefono_trabajo) || !is_null($expEditable->lugar_trabajo)){
                            //CREATE OR UPDATE
                            if(is_null($telefono_trabajo)){
                                //CREATE
                                $telefono_trabajo = new Telefono();
                                $telefono_trabajo->setNumero(!is_null($expEditable->telefono_trabajo) ? $expEditable->telefono_trabajo : '');
                                $telefono_trabajo->setContacto($expEditable->lugar_trabajo);
                                $tipo_trabajo = $em->getRepository(Catalogo::class)->findOneBy(['nombre' => 'Telefono Trabajo']);
                                $telefono_trabajo->setTelefonoTipo($tipo_trabajo);
                                $telefono_trabajo->setCuenta($persona->getCuenta());
                                $em->persist($telefono_trabajo);
                            } else {
                                //UPDATE
                                $telefono_trabajo->setNumero(!is_null($expEditable->telefono_trabajo) ? $expEditable->telefono_trabajo : '');
                                $telefono_trabajo->setContacto($expEditable->lugar_trabajo);
                            }

                        } else {
                            //REMOVE IT
                            if(!is_null($telefono_trabajo)) $em->remove($telefono_trabajo);

                        }

                        //Removing Apoderado if he/she had it
                        if(!is_null($telefono_apoderado)) $em->remove($telefono_apoderado);

                    } else{

                         //Telefono apoderado
                        if(!is_null($expEditable->telefono_apoderado) || !is_null($expEditable->apoderado)){
                            //CREATE OR UPDATE
                            if(is_null($telefono_apoderado)){
                                //CREATE
                                $telefono_apoderado = new Telefono();
                                $telefono_apoderado->setNumero(!is_null($expEditable->telefono_apoderado) ? $expEditable->telefono_apoderado : '');
                                $telefono_apoderado->setContacto($expEditable->apoderado);
                                $tipo_apoderado = $em->getRepository(Catalogo::class)->findOneBy(['nombre' => 'Telefono Apoderado']);
                                $telefono_apoderado->setTelefonoTipo($tipo_apoderado);
                                $telefono_apoderado->setCuenta($persona->getCuenta());
                                $em->persist($telefono_apoderado);
                            } else {
                                //UPDATE
                                $telefono_apoderado->setNumero(!is_null($expEditable->telefono_apoderado) ? $expEditable->telefono_apoderado : '');
                                $telefono_apoderado->setContacto($expEditable->apoderado);
                            }

                        } else {
                            //REMOVE IT
                            if(!is_null($telefono_apoderado)) $em->remove($telefono_apoderado);

                        }

                        //TODO: Nombre de apoderado

                        //Removing Dui, Estado Civil, Profesion, Ocupacion, Trabajo if he/she had it
                        $persona->setDui(NULL);
                        if(!is_null($estado_civil)) $em->remove($estado_civil);
                        if(!is_null($profesion)) $em->remove($profesion);
                        if(!is_null($ocupacion)) $em->remove($ocupacion);
                        if(!is_null($telefono_trabajo)) $em->remove($telefono_trabajo);
                        //TODO: Remove lugar de trabajo
                    }



                    // Telefonos

                    //Telefono Casa
                    if(!is_null($expEditable->telefono_casa)){
                        //CREATE OR UPDATE
                        if(is_null($telefono_casa)){
                            //CREATE
                            $telefono_casa = new Telefono();
                            $telefono_casa->setNumero($expEditable->telefono_casa);
                            $tipo_casa = $em->getRepository(Catalogo::class)->findOneBy(['nombre' => 'Telefono Fijo']);
                            $telefono_casa->setTelefonoTipo($tipo_casa);
                            $telefono_casa->setCuenta($persona->getCuenta());
                            $em->persist($telefono_casa);
                        } else {
                            //UPDATE
                            $telefono_casa->setNumero($expEditable->telefono_casa);
                        }

                    } else {
                        //REMOVE IT
                        if(!is_null($telefono_casa)) $em->remove($telefono_casa);

                    }

                    //Telefono movil
                    if(!is_null($expEditable->telefono_movil)){
                        //CREATE OR UPDATE
                        if(is_null($telefono_movil)){
                            //CREATE
                            $telefono_movil = new Telefono();
                            $telefono_movil->setNumero($expEditable->telefono_movil);
                            $tipo_movil = $em->getRepository(Catalogo::class)->findOneBy(['nombre' => 'Telefono Movil']);
                            $telefono_movil->setTelefonoTipo($tipo_movil);
                            $telefono_movil->setCuenta($persona->getCuenta());
                            $em->persist($telefono_movil);
                        } else {
                            //UPDATE
                            $telefono_movil->setNumero($expEditable->telefono_movil);
                        }

                    } else {
                        //REMOVE IT
                        if(!is_null($telefono_movil)) $em->remove($telefono_movil);

                    }

                    //Email
                    if(!is_null($expEditable->email)){
                        //CREATE OR UPDATE
                        if(is_null($email)){
                            //CREATE
                            $email = new Email();
                            $email->setEmail($expEditable->email);
                            $email->setCuenta($persona->getCuenta());
                            $em->persist($email);
                        } else {
                            //UPDATE
                            $email->setEmail($expEditable->email);
                        }

                    } else {
                        //REMOVE IT
                        if(!is_null($email)) $em->remove($email);
                    }

                    //Email apoderado
                    if(!is_null($expEditable->email_apoderado)){
                        //CREATE OR UPDATE
                        if(is_null($email_apoderado)){
                            //CREATE
                            $email_apoderado = new Email();
                            $email_apoderado->setEmail($expEditable->email_apoderado);
                            $email_apoderado->setCuenta($persona->getCuenta());
                            $em->persist($email_apoderado);
                        } else {
                            //UPDATE
                            $email_apoderado->setEmail($expEditable->email_apoderado);
                        }

                    } else {
                        //REMOVE IT
                        if(!is_null($email_apoderado)) $em->remove($email_apoderado);
                    }

                    // Atributos

                    // Sexo del paciente
                    //UPDATE
                    $sexo->setAtributo($expEditable->sexo);

                    // Nivel educativo
                    $nivel_educativo->setAtributo($expEditable->nivel_educativo);

                    $em->flush();

                    $flow->reset();
                    $this->addFlash('success', 'Tus cambios han sido guardados!');
                    return $this->redirectToRoute('expedientes');

                }
            }

        }

        return $this->render('clinico/expedientes/edit.html.twig', [
            'form' => $form->createView(),
            'flow' => $flow,
            'expedienteId' => $expedienteId,
            'expediente' => $expediente,
          ]);
    }

    private function isCodigoValid($stepNumber, $codigo, $fecha_nacimiento, $expedienteId=0)
    {
        if($stepNumber != 1) return true;

        $expedienteRepo = $this->getDoctrine()->getRepository(ExpedienteClinico::class);
        $codigo_expediente = $this->createCodigo($codigo, $fecha_nacimiento);
        $repetidos = $expedienteRepo->createQueryBuilder('exp')
        ->andWhere('exp.codigo_expediente = :codigo_expediente')
        ->setParameter('codigo_expediente', $codigo_expediente)
        ->andWhere('exp.id <> :id')
        ->setParameter('id', $expedienteId)
        ->getQuery()
        ->execute();

        if(sizeof($repetidos) > 0){
            return false;
        }
        else{
            return true;
        }
    }

    private function createCodigo($codigo, $fecha_nacimiento)
    {
        $edad = (new DateTime())->diff($fecha_nacimiento)->y;
        $cod_edad = ($edad < 18) ? 'II' : 'AA';
        $codigo_nuevo = $cod_edad . '-' . $codigo;

        return $codigo_nuevo;
    }
}
