<?php

namespace App\Controller\Clinico;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Clinico\ExpedienteClinico;
use App\Entity\Clinico\SeguimientoConductual;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use App\Entity\Clinico\Ficha;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;

class SeguimientoController extends AbstractController
{
    /**
     * ExpedienteController constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->denyAccessUnlessGranted('ficha');
    }


    /**
     * @Route("/clinico/expedientes/{expedienteId}/fichas_ped/{fichaId}/seguimiento_conductual", name="seguimiento_conductual", methods="POST|GET")
     */
    public function SeguimientoShow($expedienteId, $fichaId, Request $request){
      $em = $this->getDoctrine()->getManager();
      $expediente = $em->getRepository(ExpedienteClinico::class)->findOneBy(['id' => $expedienteId]);
      $fnacimiento = $expediente->getPersona()->getNacimiento();
      $seguimiento_data = $em->getRepository(SeguimientoConductual::class)->findBy(['ficha_fk' => $fichaId]);
      $fhoy = new \DateTime();
      $edad = $fhoy->diff($fnacimiento)->y;
      $seg = new SeguimientoConductual();

      $form = $this->createFormBuilder($seg)
          ->add('fecha', DateType::class, [  'widget' => 'single_text', 'data' => new \DateTime("now")])
          ->add('procedimientoRealizado', TextType::class)
          ->add('comportamientoDemostrado', TextType::class)
          ->add('clave', ChoiceType::class, [
            'choices'=> [
              'Tipo1: Definitivamente Negativo(--)'=>'Tipo1: Definitivamente Negativo(--)',
              'Tipo2: Negativo(-)'=>'Tipo2: Negativo(-)',
              'Tipo3: Positivo(+)'=>'Tipo3: Positivo(+)',
              'Tipo4: Definitivamente Positivo(++)'=>'Tipo4: Definitivamente Positivo(++)'
            ],
            'label'=>'Clave (Escala de Frankl)'
          ])
          ->add('save', SubmitType::class, ['label' => 'Guardar Registro'])
          ->getForm();

      $formEdit = $this->createFormBuilder()
              ->setAction($this->generateUrl('seguimiento_conductual_edit', ['expedienteId'=>$expedienteId, 'fichaId'=>$fichaId]))
              ->add('idE', HiddenType::class, ['attr'=> ['style'=>'display:none']])
              ->add('fechaE', DateType::class, [  'widget' => 'single_text', 'data' => new \DateTime("now")])
              ->add('procedimientoRealizadoE', TextType::class)
              ->add('comportamientoDemostradoE', TextType::class)
              ->add('claveE', ChoiceType::class, [
                'choices'=> [
                  'Tipo1: Definitivamente Negativo(--)'=>'Tipo1: Definitivamente Negativo(--)',
                  'Tipo2: Negativo(-)'=>'Tipo2: Negativo(-)',
                  'Tipo3: Positivo(+)'=>'Tipo3: Positivo(+)',
                  'Tipo4: Definitivamente Positivo(++)'=>'Tipo4: Definitivamente Positivo(++)'
                ],
                'label'=>'Clave (Escala de Frankl)'
              ])
              ->add('save', SubmitType::class, ['label' => 'Guardar Registro'])
              ->getForm();

      $formDelete = $this->createFormBuilder()
              ->setAction($this->generateUrl('seguimiento_conductual_borrar', ['expedienteId'=>$expedienteId, 'fichaId'=>$fichaId]))
              ->add('idD', HiddenType::class)
              ->add('save', SubmitType::class, ['label' => 'Borrar', 'attr'=> ['class'=>'btn btn-danger']])
              ->getForm();

      $form->handleRequest($request);

      if ($form->isSubmitted() && $form->isValid()) {
          $seg = $form->getData();
          $seg->setFicha($em->getRepository(Ficha::class)->findOneBy(['id' => $fichaId]));
          $em->persist($seg);
          $em->flush();
          return $this->redirectToRoute('seguimiento_conductual', ['expedienteId'=>$expedienteId, 'fichaId'=>$fichaId]);
      }



      return $this->render('clinico/seguimiento_conductual/seguimiento.html.twig', [
        'expedienteId' => $expedienteId,
        'expediente' => $expediente,
        'fichaId' => $fichaId,
        'seguimiento_data' => $seguimiento_data,
        'edad' => $edad,
        'form' => $form->createView(),
        'formEdit' => $formEdit->createView(),
        'formDelete' => $formDelete->createView()
      ]);
    }

    /**
     * @Route("/clinico/expedientes/{expedienteId}/fichas_ped/{fichaId}/seguimiento_conductual/edit", name="seguimiento_conductual_edit", methods="POST|GET")
     */
    public function SeguimientoEdit($expedienteId, $fichaId, Request $request){
      $em = $this->getDoctrine()->getManager();

      $form = $this->createFormBuilder()
              ->setAction($this->generateUrl('seguimiento_conductual_edit', ['expedienteId'=>$expedienteId, 'fichaId'=>$fichaId]))
              ->add('idE', HiddenType::class)
              ->add('fechaE', DateType::class, [  'widget' => 'single_text', 'data' => new \DateTime("now")])
              ->add('procedimientoRealizadoE', TextType::class)
              ->add('comportamientoDemostradoE', TextType::class)
              ->add('claveE', ChoiceType::class, [
                'choices'=> [
                  'Tipo1: Definitivamente Negativo(--)'=>'Tipo1: Definitivamente Negativo(--)',
                  'Tipo2: Negativo(-)'=>'Tipo2: Negativo(-)',
                  'Tipo3: Positivo(+)'=>'Tipo3: Positivo(+)',
                  'Tipo4: Definitivamente Positivo(++)'=>'Tipo4: Definitivamente Positivo(++)'
                ],
                'label'=>'Clave (Escala de Frankl)'
              ])
              ->add('save', SubmitType::class, ['label' => 'Guardar Registro'])
              ->getForm();

      $form->handleRequest($request);

      if ($form->isSubmitted() && $form->isValid()) {
          $seg = $em->getRepository(SeguimientoConductual::class)->findOneBy(['id' => $form->getData()['idE']]);
        // dd($form->getData()['idE']);
          // $seg->setId($form->getData()['idE']);
          $seg->setFecha($form->getData()['fechaE']);
          $seg->setComportamientoDemostrado($form->getData()['comportamientoDemostradoE']);
          $seg->setProcedimientoRealizado($form->getData()['procedimientoRealizadoE']);
          $seg->setClave($form->getData('claveE')['claveE']);
          $seg->setFicha($em->getRepository(Ficha::class)->findOneBy(['id' => $fichaId]));
          $em->persist($seg);
          $em->flush();
          return $this->redirectToRoute('seguimiento_conductual', ['expedienteId'=>$expedienteId, 'fichaId'=>$fichaId]);
      }

      return $this->redirectToRoute('seguimiento_conductual', ['expedienteId'=>$expedienteId, 'fichaId'=>$fichaId]);

    }

    /**
     * @Route("/clinico/expedientes/{expedienteId}/fichas_ped/{fichaId}/borrar/", name="seguimiento_conductual_borrar", methods="POST")
     */
    public function SeguimientoBorrar($expedienteId, $fichaId, Request $request){
      $em = $this->getDoctrine()->getManager();

      $form = $this->createFormBuilder()
              ->setAction($this->generateUrl('seguimiento_conductual_borrar', ['expedienteId'=>$expedienteId, 'fichaId'=>$fichaId]))
              ->add('idD', HiddenType::class)
              ->add('save', SubmitType::class, ['label' => 'Borrar', 'attr'=> ['class'=>'btn btn-danger']])
              ->getForm();

      $form->handleRequest($request);

      if ($form->isSubmitted() && $form->isValid()) {
          $seg = $em->getRepository(SeguimientoConductual::class)->findOneBy(['id' => $form->getData()['idD']]);
        // dd($form->getData()['idE']);

          $em->remove($seg);
          $em->flush();
        // dd($request);
      }
      return $this->redirectToRoute('seguimiento_conductual', ['expedienteId'=>$expedienteId, 'fichaId'=>$fichaId]);

    }

}
