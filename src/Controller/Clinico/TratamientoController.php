<?php

namespace App\Controller\Clinico;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Clinico\ExpedienteClinico;
use App\Entity\Clinico\Ficha;
use App\Entity\Clinico\FichaTratamiento;
use App\Entity\Clinico\EvaluacionDental;
use App\Entity\Administracion\Persona;

use App\Service\Clinico\TablaFichaService;

class TratamientoController extends AbstractController
{

  private $tablaFichaService;

    /**
     * TratamientoController constructor.
     * @param ContainerInterface $container
     * @param TablaFichaService $tablaFichaService
     */
    public function __construct(ContainerInterface $container, TablaFichaService $tablaFichaService)
    {
        $this->container = $container;
        $this->denyAccessUnlessGranted('tratamiento');
        $this->tablaFichaService = $tablaFichaService;
    }

  /**
   * @Route("/clinico/expedientes/{expedienteId}/fichas/{fichaId}/plan", name="tratamiento_plan", methods="GET|POST")
   */
  public function plan(Request $request, $expedienteId, $fichaId): Response
  {
      $em = $this->getDoctrine()->getManager();
      $usuario = $this->getUser();
      $expediente = $em->getRepository(ExpedienteClinico::class)->findOneBy(['id' => $expedienteId]);
      $ficha = $em->getRepository(Ficha::class)->findOneBy(['id' => $fichaId]);
      $canEdit = $this->tablaFichaService->allowEdit($usuario, $ficha);
      $fichaInfo = $this->tablaFichaService->getTableInfo($expedienteId, $fichaId);

      if(sizeof($em->getRepository(EvaluacionDental::class)->findBy(['ficha' => $fichaId])) == 0){
        $this->addFlash('warning', 'Aun no hay odontograma');
        return $this->redirectToRoute('ficha_detalle', ['expedienteId' => $expedienteId, 'fichaId' => $fichaId]);
      }

      $tratamientos_array = $em->getRepository(EvaluacionDental::class)
            ->createQueryBuilder('a')
            ->select('a.id, e.nombre as clasificacion, a.pieza_dental as pieza, d.nombre as tratamiento, a.superficie_dental as superficie, a.prioridad as prioridad')
            ->where('a.ficha = :fichaId')
            ->setParameter('fichaId', $fichaId)
            ->innerJoin('a.tratamiento', 'd')
            ->innerJoin('d.tratamiento_area', 'e')
            ->orderBy('clasificacion', 'ASC')
            ->getQuery()
            ->execute();
      
      $tratamientos = array();
      foreach($tratamientos_array as $val){
         $tratamientos[$val['clasificacion']][] = (array)$val;
      }
      $tratamientos_keys = array_keys($tratamientos);
      $orden_requerido = array('Endodónticos', 'Operatorio dental', 'Periodontal', 'Quirúrgicos', 'Sellantes o resinas preventivas',
      'Educación y motivación', 'Fluorterapia', 'Control de dieta', 'Control de hábitos', 'Otras terapias complementarias');
      //Ordenando
      $ordenado = array_intersect($orden_requerido, $tratamientos_keys);

      return $this->render('clinico/tratamientos/plan.html.twig', [
        'expedienteId' => $expedienteId,
        'expediente' => $expediente,
        'fichaId' => $fichaId,
        'fichaInfo' => $fichaInfo,
        'tratamientos' => $tratamientos,
        'tratamientos_tipos' => $ordenado,
        'editable' => $canEdit,
      ]);
  }

   /**
     * @Route("/clinico/fichas/{fichaId}/tratamientos/prioridad", name="tratamiento_prioridad", methods="POST")
     */
    public function cambiar_prioridad(Request $request)
    {
      $evaluacionId =  $request->request->get('evaluacion_id');
      $em = $this->getDoctrine()->getManager();
      $evaluacion = $em->getRepository(EvaluacionDental::class)->findOneBy(['id'=>$evaluacionId]);

      $evaluacion->setPrioridad(!$evaluacion->getPrioridad());
      $em->flush();

      $response = array(
        'estado' => $evaluacion->getPrioridad(),
      );

      return $this->json($response);
    }

  /**
   * @Route("/clinico/expedientes/{expedienteId}/fichas/{fichaId}/control", name="tratamiento_control", methods="GET")
   */
  public function control($fichaId, ExpedienteClinico $expedienteId): Response
  {
      $repository = $this->getDoctrine()->getRepository(EvaluacionDental::class);
      $tratamiento = $repository->findBy(array('ficha'=>$fichaId, 'prioridad'=>1));
      $usuario = $this->getUser();
      $ficha = $this->getDoctrine()->getRepository(Ficha::class)->findOneBy(['id' => $fichaId]);
      $canEdit = $this->tablaFichaService->allowEdit($usuario, $ficha);
      $fichaInfo = $this->tablaFichaService->getTableInfo($expedienteId->getId(), $fichaId);

      return $this->render('clinico/tratamientos/control.html.twig', array(
          'tratamientos' => $tratamiento,
          'expedienteId' => $expedienteId->getId(),
          'expediente' => $expedienteId,
          'fichaId' => $fichaId,
          'fichaInfo' => $fichaInfo,
          'editable' => $canEdit
      ));
  }

  /**
     * @Route("/clinico/fichas/{fichaId}/tratamientos/realizado", name="tratamiento_realizado", methods="POST")
     */
    public function cambiar_realizado(Request $request)
    {
      $evaluacionId =  $request->request->get('evaluacion_id');
      $em = $this->getDoctrine()->getManager();
      $evaluacion = $em->getRepository(EvaluacionDental::class)->findOneBy(['id'=>$evaluacionId]);
      $usuario = $this->getUser();
      $evaluacion->setRealizado(!$evaluacion->getRealizado());  //Se cambia el estado

      /* Se agrega o elimina dependiendo de su estado*/

      if($evaluacion->getRealizado()){
        $tratamiento = new FichaTratamiento();
        $tratamiento->setEvaluacionDental($evaluacion);
        $tratamiento->setCuenta($usuario->getCuenta());
        $em->persist($tratamiento);
      } else {
        if($evaluacion->getRetratamiento()){
          $this->addFlash('warning', 'El tratamiento ya fue realizado multiples veces');
          return $this->redirectToRoute('retratamiento_listar', ['expedienteId' => $expedienteId, 'fichaId' => $fichaId, 'tratamientoId' => $evaluaco]);
        } else {
          $old_tratamiento = $em->getRepository(FichaTratamiento::class)->findOneBy(['evaluacion_dental'=>$evaluacion]);
          if(!is_null($old_tratamiento)) $em->remove($old_tratamiento);
        }
      }

      $em->flush();

      $response = array(
        'estado' => $evaluacion->getRealizado(),
      );

      return $this->json($response);
    }

  /**
  * @Route("/clinico/expedientes/{expedienteId}/fichas/{fichaId}/tratamiento/{tratamientoId}", name="retratamiento_listar", methods="GET")
  */
  public function retratamiento($expedienteId, $fichaId, $tratamientoId): Response
  {
      $em = $this->getDoctrine()->getManager();
      $repoEva = $this->getDoctrine()->getRepository(EvaluacionDental::class);
      $repoTratamientos = $this->getDoctrine()->getRepository(FichaTratamiento::class);
      $evaluacion = $repoEva->findOneBy(['id' => $tratamientoId]);
      $usuario = $this->getUser();
      $ficha = $this->getDoctrine()->getRepository(Ficha::class)->findOneBy(['id' => $fichaId]);
      $fichaInfo = $this->tablaFichaService->getTableInfo($expedienteId, $fichaId);
      $expediente = $em->getRepository(ExpedienteClinico::class)->findOneBy(['id' => $expedienteId]);

      if(is_null($evaluacion) || !$evaluacion->getRealizado()){
        $this->addFlash('warning', 'El tratamiento aun no ha sido realizado');
        return $this->redirectToRoute('ficha_detalle', ['expedienteId' => $expedienteId, 'fichaId' => $fichaId]);
      }

      $tratamientos = $repoTratamientos->findBy(['evaluacion_dental' => $tratamientoId]);
      $usuarios = [];
      $editables = [];
      foreach ($tratamientos as $trat) {
        $autor = $this->getDoctrine()->getRepository(Persona::class)->findOneBy(['cuenta' => $trat->getCuenta()]);
        if($usuario->getCuenta()->getId() == $trat->getCuenta()->getId()) $canEdit = true;
        else $canEdit = false;

        array_push($usuarios, $autor);
        array_push($editables, $canEdit);
      }

      return $this->render('clinico/tratamientos/historial.html.twig', array(
          'expedienteId' => $expedienteId,
          'expediente' => $expediente,
          'fichaId' => $fichaId,
          'fichaInfo' => $fichaInfo,
          'tratamientoId' => $tratamientoId,
          'evaluacion_dental' => $evaluacion,
          'tratamientos' => $tratamientos,
          'usuarios' => $usuarios,
          'editables' => $editables
      ));
  }

  /**
  * @Route("/clinico/expedientes/{expedienteId}/fichas/{fichaId}/tratamiento/{tratamientoId}", name="retratamiento_nuevo", methods="POST")
  */
  public function agregar_tratamiento(Request $request): Response
  {
      $usuario = $this->getUser();
      $evaluacionId =  $request->request->get('evaluacion_id');
      $em = $this->getDoctrine()->getManager();
      $evaluacion = $em->getRepository(EvaluacionDental::class)->findOneBy(['id'=>$evaluacionId]);
      
      if($evaluacion->getRealizado()){
        $tratamiento = new FichaTratamiento();
        $tratamiento->setEvaluacionDental($evaluacion);
        $tratamiento->setCuenta($usuario->getCuenta());
        $em->persist($tratamiento);
      }

      $evaluacion->setRetratamiento(1);
      $em->flush();

      $persona = $this->getDoctrine()->getRepository(Persona::class)->findOneBy(['cuenta' => $usuario->getCuenta()]);

      $response = array(
        'estado' => $evaluacion->getRealizado(),
        'tratamiento_id' => $tratamiento->getId(),
        'tratamiento_usuario' => $persona->getNombreCompleto(),
        'tratamiento_fecha' => $tratamiento->getCreado(),
      );

      return $this->json($response);
  }

  /**
  * @Route("/clinico/expedientes/{expedienteId}/fichas/{fichaId}/tratamiento/{tratamientoId}", name="retratamiento_borrar", methods="DELETE")
  */
  public function remover_retratamiento($tratamientoId): Response
  {
      $em = $this->getDoctrine()->getManager();
      $usuario = $this->getUser();

      $old_tratamiento = $em->getRepository(FichaTratamiento::class)->findOneBy(['id'=>$tratamientoId]);
      if(!is_null($old_tratamiento)){
        if($old_tratamiento->getCuenta()->getId() == $usuario->getCuenta()->getId()) $em->remove($old_tratamiento);
      }

      $tratamientos = $em->getRepository(FichaTratamiento::class)->findBy(['evaluacion_dental' => $old_tratamiento->getEvaluacionDental()]);

      if(sizeof($tratamientos)==1){
        $evaluacion = $em->getRepository(EvaluacionDental::class)->findOneBy(['id'=>$old_tratamiento->getEvaluacionDental()]);
        $evaluacion->setRetratamiento(0);
      }

      $em->flush();

      $response = array(
        'estado' => 'Deleted'
      );

      return $this->json($response);
  }






}
