<?php

namespace App\Controller\Clinico;

use App\Form\Clinico\riesgos\RiesgoFlow;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Clinico\Ficha;
use App\Form\Clinico\riesgos\CrearRiesgo;
use App\Entity\Clinico\ExpedienteClinico;
use App\Entity\Clinico\FichaAtributo;
use App\Entity\Clinico\FichaCatalogo;
use App\Entity\Clinico\EvaluacionDental;

use App\Service\Clinico\TablaFichaService;
use App\Entity\Clinico\RiesgoDetalle;
use App\Service\Clinico\RiesgoService;

class RiesgoController extends AbstractController
{

  private $tablaFichaService;
  private $riesgoService;

    /**
     * RiesgoController constructor.
     * @param ContainerInterface $container
     * @param TablaFichaService $tablaFichaService
     * @param RiesgoService $riesgoService
     */
    public function __construct(ContainerInterface $container, TablaFichaService $tablaFichaService, RiesgoService $riesgoService)
    {
        $this->container = $container;
        $this->denyAccessUnlessGranted('riesgo_cariogenico');
        $this->tablaFichaService = $tablaFichaService;
        $this->riesgoService = $riesgoService;
    }

  /**
   * @Route("/clinico/expedientes/{expedienteId}/fichas/{fichaId}/riesgo", name="riesgos_cariogenicos", methods="POST|GET")
   */
  public function riesgos($expedienteId, $fichaId, RiesgoFlow $flow): Response
  {
    $em = $this->getDoctrine()->getManager();
    $usuario = $this->getUser();
    $expediente = $em->getRepository(ExpedienteClinico::class)->findOneBy(['id' => $expedienteId]);
    $ficha = $em->getRepository(Ficha::class)->findOneBy(['id' => $fichaId]);
    $riesgoDetalle = $em->getRepository(RiesgoDetalle::class)->findOneBy(['ficha' => $ficha]);

    $canEdit = $this->tablaFichaService->allowEdit($usuario, $ficha);

    $fichaInfo = $this->tablaFichaService->getTableInfo($expedienteId, $fichaId);

    if(sizeof($em->getRepository(EvaluacionDental::class)->findBy(['ficha' => $fichaId])) == 0){
      $this->addFlash('warning', 'Aun no hay odontograma');
      return $this->redirectToRoute('ficha_detalle', ['expedienteId' => $expedienteId, 'fichaId' => $fichaId]);
    }

    if (!is_null($ficha) && !is_null($riesgoDetalle)){
      $persona = $expediente->getPersona();
      $ingestas = $em->getRepository(FichaAtributo::class)->findByFichaAndCatalogoTipo($fichaId, 'historia_ingesta');
      //TODO: Add boolean to show edit button depending on the user
      return $this->render('clinico/riesgos/show.html.twig', [
        'ficha' => $ficha,
        'riesgo_detalle' => $riesgoDetalle,
        'fichaId' => $fichaId,
        'fichaInfo' => $fichaInfo,
        'expedienteId' => $expedienteId,
        'expediente' => $expediente,
        'persona' => $persona,
        'ingestas' => $ingestas,
        'editable' => $canEdit,
      ]);
    }

    if(!$canEdit){
      $this->addFlash('warning', 'Aun no se ha agregado el riesgo cariogenico');
      return $this->redirectToRoute('ficha_detalle', ['expedienteId' => $expedienteId, 'fichaId' => $fichaId]);
    }

    $riesgoDetalle = new RiesgoDetalle();
    $riesgoDetalle->setFicha($ficha);

    $riesgo = new CrearRiesgo();
//    $flow = $this->get('app.form.flow.riesgoFlow');
    //TODO: Fill riesgo cpo, ceo with a function
    $this->riesgoService->getIndices($riesgo, $fichaId);
    $flow->bind($riesgo);

    $form = $submittedForm = $flow->createForm();

    if ($flow->isValid($submittedForm)){

      $flow->saveCurrentStepData($form);

      if($flow->nextStep()){

        if ($flow->getCurrentStepNumber() == 4){
          //Do the calculations for risk
          $total_caries = $this->riesgoService->calcInd($riesgo);
          $total_placa = $this->riesgoService->calcPlaque($riesgo);
          $total_azucar = sizeof($riesgo->ingesta_azucar);
          $riesgo->total_ingesta = $total_azucar;
          $riesgo->diagnostico = $this->riesgoService->calcRisk($total_caries, $total_placa, $total_azucar);

        }

        $form = $flow->createForm();

      } else {

        //Save
        $riesgo->total_indice = $this->riesgoService->calcInd($riesgo);
        $riesgo->total_placa = $this->riesgoService->calcPlaque($riesgo);
        //Indice CPO/ceo
        $riesgoDetalle->setTotalIndiceCpoCeo($riesgo->total_indice);
        $riesgoDetalle->setObservaciones($riesgo->observaciones_indice);

        //Placa bacteriana
        $riesgoDetalle->setPlaca1655($riesgo->placa_1655);
        $riesgoDetalle->setPlaca1151($riesgo->placa_1151);
        $riesgoDetalle->setPlaca2665($riesgo->placa_2665);
        $riesgoDetalle->setPlaca3675($riesgo->placa_3675);
        $riesgoDetalle->setPlaca3171($riesgo->placa_3171);
        $riesgoDetalle->setPlaca4685($riesgo->placa_4685);
        $riesgoDetalle->setTotalPlacaBacteriana($riesgo->total_placa);
        $riesgoDetalle->setComentarios($riesgo->comentarios_placa);

        //Atributos

        //Ingesta unicamente
        //$atributos = array_merge($ficha->ingesta_azucar->toArray());
        $atributos = $riesgo->ingesta_azucar;

        foreach($atributos as $attr){
          $nuevoAttr = new FichaAtributo();
          $nuevoAttr->setFicha($ficha);
          $nuevoAttr->setAtributo($attr);
          $em->persist($nuevoAttr);
        }

        $riesgoDetalle->setTotalIngestaAzucares(sizeof($riesgo->ingesta_azucar));

        $riesgoDetalle->setOtrasCondiciones($riesgo->condiciones);
        $tipo_riesgo = $em->getRepository(FichaCatalogo::class)->findOneBy(['nombre' => $riesgo->diagnostico]);
        $ficha->setIndiceRiesgoCariogenico($tipo_riesgo);
        //Riesgo asignado por el usuario
        $ficha->setIndiceRiesgoReal($riesgo->riesgo_real);

        $em->persist($riesgoDetalle);
        $em->flush();

        $flow->reset();
        $this->addFlash('success', 'Se guardo el indice de riesgo cariogenico!');


        return $this->redirectToRoute('ficha_detalle', ['expedienteId' => $expedienteId, 'fichaId' => $fichaId]);
      }

    }

    return $this->render('clinico/riesgos/new.html.twig', [
      'form' => $form->createView(),
      'flow' => $flow,
      'fichaInfo' => $fichaInfo,
      'new' => true,
      'fichaId' => $fichaId,
      'expedienteId' => $expedienteId,
        'expediente' => $expediente,
    ]);

  }

  /**
   * @Route("/clinico/expedientes/{expedienteId}/fichas/{fichaId}/riesgo/editar", name="riesgos_cariogenicos_editar", methods="POST|GET")
   */
  public function riesgos_editar($expedienteId, $fichaId, RiesgoFlow $flow): Response
  {
    $em = $this->getDoctrine()->getManager();
    $repoFichaAttr = $em->getRepository(FichaAtributo::class);
    $usuario = $this->getUser();
    $expediente = $em->getRepository(ExpedienteClinico::class)->findOneBy(['id' => $expedienteId]);
    $ficha = $em->getRepository(Ficha::class)->findOneBy(['id' => $fichaId]);
    $riesgoDetalle = $em->getRepository(RiesgoDetalle::class)->findOneBy(['ficha' => $ficha]);

    //Verificar que sea el creador
    $canEdit = $this->tablaFichaService->allowEdit($usuario, $ficha);
    if(!$canEdit){
      $this->addFlash('warning', 'No esta autorizado para editar el riesgo cariogenico');
      return $this->redirectToRoute('riesgos_cariogenicos', ['expedienteId' => $expedienteId, 'fichaId' => $fichaId]);
    }

    $fichaInfo = $this->tablaFichaService->getTableInfo($expedienteId, $fichaId);

    if(sizeof($em->getRepository(EvaluacionDental::class)->findBy(['ficha' => $fichaId])) == 0){
      $this->addFlash('warning', 'Primero debe generar el odontograma!');
      return $this->redirectToRoute('ficha_detalle', ['expedienteId' => $expedienteId, 'fichaId' => $fichaId]);
    }

    if(is_null($riesgoDetalle)){
      $this->addFlash('warning', 'No se ha creado aun!');
      return $this->redirectToRoute('ficha_detalle', ['expedienteId' => $expedienteId, 'fichaId' => $fichaId]);
    }

    $riesgoEditable = new CrearRiesgo();
//    $flow = $this->get('app.form.flow.riesgoFlow');
    //TODO: Fill riesgo cpo, ceo with a function
    $this->riesgoService->getIndices($riesgoEditable, $fichaId);
    $riesgoEditable->observaciones_indice = $riesgoDetalle->getObservaciones();

    //Placa bacteriana
    $riesgoEditable->placa_1655 = $riesgoDetalle->getPlaca1655();
    $riesgoEditable->placa_1151 = $riesgoDetalle->getPlaca1151();
    $riesgoEditable->placa_2665 = $riesgoDetalle->getPlaca2665();
    $riesgoEditable->placa_3675 = $riesgoDetalle->getPlaca3675();
    $riesgoEditable->placa_3171 = $riesgoDetalle->getPlaca3171();
    $riesgoEditable->placa_4685 = $riesgoDetalle->getPlaca4685();
    $riesgoEditable->comentarios_placa = $riesgoDetalle->getComentarios();
    //Ingesta de azucar
    $ingesta_azucar = $repoFichaAttr->findByFichaAndCatalogoTipo($fichaId, 'historia_ingesta');
    if(sizeof($ingesta_azucar)>0){
      $riesgoEditable->ingesta_azucar = array_map(function($ingesta){return $ingesta->getAtributo();}, $ingesta_azucar);
    }
    $riesgoEditable->condiciones = $riesgoDetalle->getOtrasCondiciones();
    $riesgoEditable->riesgo_real = $ficha->getIndiceRiesgoReal();
    $flow->bind($riesgoEditable);

    $form = $submittedForm = $flow->createForm();

    if ($flow->isValid($submittedForm)){

      $flow->saveCurrentStepData($form);

      if($flow->nextStep()){

        if ($flow->getCurrentStepNumber() == 4){
          //Do the calculations for risk
          $total_caries = $this->riesgoService->calcInd($riesgoEditable);
          $total_placa = $this->riesgoService->calcPlaque($riesgoEditable);
          $total_azucar = sizeof($riesgoEditable->ingesta_azucar);
          $riesgoEditable->total_ingesta = $total_azucar;
          $riesgoEditable->diagnostico = $this->riesgoService->calcRisk($total_caries, $total_placa, $total_azucar);

        }

        $form = $flow->createForm();

      } else {


        //Save
        $riesgoEditable->total_indice = $this->riesgoService->calcInd($riesgoEditable);
        $riesgoEditable->total_placa = $this->riesgoService->calcPlaque($riesgoEditable);
        //Indice CPO/ceo
        $riesgoDetalle->setTotalIndiceCpoCeo($riesgoEditable->total_indice);

        $riesgoDetalle->setObservaciones($riesgoEditable->observaciones_indice);

        //Placa bacteriana
        $riesgoDetalle->setPlaca1655($riesgoEditable->placa_1655);
        $riesgoDetalle->setPlaca1151($riesgoEditable->placa_1151);
        $riesgoDetalle->setPlaca2665($riesgoEditable->placa_2665);
        $riesgoDetalle->setPlaca3675($riesgoEditable->placa_3675);
        $riesgoDetalle->setPlaca3171($riesgoEditable->placa_3171);
        $riesgoDetalle->setPlaca4685($riesgoEditable->placa_4685);
        $riesgoDetalle->setTotalPlacaBacteriana($riesgoEditable->total_placa);
        $riesgoDetalle->setComentarios($riesgoEditable->comentarios_placa);

        foreach ($ingesta_azucar as $ingesta) {
          $em->remove($ingesta);
        }

        foreach($riesgoEditable->ingesta_azucar as $ingesta){
          $nuevoAttr = new FichaAtributo();
          $nuevoAttr->setFicha($ficha);
          $nuevoAttr->setAtributo($ingesta);
          $em->persist($nuevoAttr);
        }
        $riesgoDetalle->setTotalIngestaAzucares(sizeof($riesgoEditable->ingesta_azucar));
        $riesgoDetalle->setOtrasCondiciones($riesgoEditable->condiciones);
        $tipo_riesgo = $em->getRepository(FichaCatalogo::class)->findOneBy(['nombre' => $riesgoEditable->diagnostico]);
        $ficha->setIndiceRiesgoCariogenico($tipo_riesgo);
         //Riesgo asignado por el usuario
        $ficha->setIndiceRiesgoReal($riesgoEditable->riesgo_real);

        $em->flush();
        $flow->reset();
        $this->addFlash('success', 'Se guardaron los cambios en indice de riesgo cariogenico!');


        return $this->redirectToRoute('ficha_detalle', ['expedienteId' => $expedienteId, 'fichaId' => $fichaId]);
      }

    }

    return $this->render('clinico/riesgos/new.html.twig', [
      'form' => $form->createView(),
      'flow' => $flow,
      'fichaInfo' => $fichaInfo,
      'new' => true,
      'fichaId' => $fichaId,
      'expedienteId' => $expedienteId,
        'expediente' => $expediente,
    ]);


  }

}
