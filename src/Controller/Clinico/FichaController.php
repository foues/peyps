<?php

namespace App\Controller\Clinico;

use App\Entity\Administracion\PersonaAtributo;
use App\Entity\Clinico\DatosClinicos;
use App\Form\Clinico\datosclinicos\DatosFlow;
use App\Form\Clinico\fichas\FichaFlow;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use App\Form\Clinico\datosclinicos\CrearDatos;
use App\Form\Clinico\fichas\CrearFicha;
use App\Entity\Clinico\ExpedienteClinico;
use App\Entity\Clinico\Ficha;
use App\Entity\Clinico\FichaAtributo;
use App\Entity\Administracion\Persona;
use App\Entity\Academico\Curso;
use App\Entity\Academico\PlanEstudioCiclo;
use App\Service\Clinico\TablaFichaService;
use Datetime;

use App\Entity\Clinico\OpcionesPregunta;

use App\Entity\Clinico\FichaRespuestas;


class FichaController extends AbstractController
{

  private $tablaFichaService;

    /**
     * FichaController constructor.
     * @param ContainerInterface $container
     * @param TablaFichaService $tablaFichaService
     */
    public function __construct(ContainerInterface $container, TablaFichaService $tablaFichaService)
    {
        $this->container = $container;
        $this->denyAccessUnlessGranted('ficha');
        $this->tablaFichaService = $tablaFichaService;
    }


  /**
   * @Route("/clinico/expedientes/{expedienteId}/fichas", name="fichas", methods="GET")
   */
  public function list($expedienteId): Response
  {

      $em = $this->getDoctrine()->getManager();
      $expediente = $em->getRepository(ExpedienteClinico::class)->findOneBy(['id' => $expedienteId]);
      $persona = $expediente->getPersona();
      $fnacimiento = $expediente->getPersona()->getNacimiento();
      $edad_actual = null;
      if ($fnacimiento) $edad_actual = (new Datetime)->diff($fnacimiento)->y;
      $fichas = $em->getRepository(Ficha::class)->findBy(['expediente_clinico' => $expediente]);
      $estudiantes = [];
      $edades = [];
      foreach ($fichas as $f) {
        $estudiante = $em->getRepository(Persona::class)->findOneBy(['cuenta' => $f->getCuenta()])->getNombreCompleto();
        $creado = $f->getFechaControl();
        $edad = null;
        if ($fnacimiento) $edad = $creado->diff($fnacimiento)->y;
        array_push($estudiantes, $estudiante);
        array_push($edades, $edad);
      }

      $fichas_t = array_map(null, $fichas, $estudiantes, $edades);

      return $this->render('clinico/fichas/list.html.twig', [
        'expedienteId' => $expedienteId,
        'expediente' => $expediente,
        'codigo_expediente' => $expediente->getCodigoExpediente(),
        'persona' => $persona,
        'fichas' => $fichas_t,
        'edad' => $edad_actual
      ]);
  }

  /**
   * @Route("/clinico/expedientes/{expedienteId}/fichas/nueva", name="ficha_nueva", methods="GET|POST")
   */
  public function new($expedienteId, FichaFlow $flow): Response
  {
      $em = $this->getDoctrine()->getManager();
      $usuario = $this->getUser();
      $expediente = $em->getRepository(ExpedienteClinico::class)->findOneBy(['id' => $expedienteId]);
      $fnacimiento = $expediente->getPersona()->getNacimiento();
      $fhoy = new \DateTime();

      # Validar que no se cree una ficha si no se encuentran todas finalizadas
      $fichas_incompletas = $em->getRepository(Ficha::class)->findBy(['expediente_clinico'=>$expedienteId, 'finalizada'=>0]);

      // if(sizeof($fichas_incompletas)>0){
      //   $this->addFlash('warning', 'No se puede abrir una nueva ficha mientras otra este incompleta!');
      //   return $this->redirectToRoute('fichas', ['expedienteId' => $expedienteId]);
      // }

      $ficha = new CrearFicha();
//      $flow = $this->get('app.form.flow.fichaFlow');
      $flow->bind($ficha);
      $edad = $fhoy->diff($fnacimiento)->y;

      $form = $submittedForm = $flow->createForm();

      if ($flow->isValid($submittedForm)){
        $flow->saveCurrentStepData($form);

        if($flow->nextStep()){
          $form = $flow->createForm();
        } else {
          //Save


          $fichaNueva = new Ficha();
          $fichaNueva->setFechaControl($ficha->fecha);
          // $fichaNueva->setMotivoConsulta($ficha->motivo);
          $fichaNueva->setHistoriaEnfermedad($ficha->historia);
          $fichaNueva->setVisitaOdontologo(boolval($ficha->visita_odontologo));
          $fichaNueva->setEmbarazada(boolval($ficha->embarazada));
          $fichaNueva->setReaccionesAdversas(boolval($ficha->reacciones_adversas));
          $fichaNueva->setCurso($ficha->curso->getCodigo());
          $fichaNueva->setCiclo($ficha->ciclo->getTipo() . ' ' . $ficha->ciclo->getPeriodo()->getInicio()->format('Y'));
          if(boolval($ficha->reacciones_adversas)) $fichaNueva->setTipoReaccion($ficha->tipo_reaccion);
          $fichaNueva->setTraumaDentoalveolar(boolval($ficha->trauma_dento));
          if(boolval($ficha->trauma_dento)) $fichaNueva->setPieza($ficha->pieza_trauma);

          $fichaNueva->setFrecuenciaCepillado($ficha->frecuencia_cepillado);

          //Atributos

          $atributos = array_merge($ficha->aditamientos_higiene->toArray(), $ficha->eval_sistemica->toArray(), $ficha->habitos_bucales->toArray());

          foreach ($atributos as $attr) {
            $nuevoAttr = new FichaAtributo();
            $nuevoAttr->setFicha($fichaNueva);
            $nuevoAttr->setAtributo($attr);
            $em->persist($nuevoAttr);
          }

            if($ficha->motivo != Null){
            $resp = new FichaRespuestas();
            $resp->setFicha($fichaNueva);
            $resp->setOpcionesPregunta($ficha->motivo);
            $em->persist($resp);
            }

            

          $fichaNueva->setCuenta($usuario->getCuenta());
          $fichaNueva->setExpedienteClinico($expediente);
          $fichaNueva->setFinalizada(0);
          $em->persist($fichaNueva);

          $datosClinicos = new DatosClinicos();
          $datosClinicos->setFicha($fichaNueva);
          $em->persist($datosClinicos);

          $em->flush();


          $flow->reset();
          $this->addFlash('success', 'Tus cambios fueron guardados');
          return $this->redirectToRoute('fichas', ['expedienteId' => $expedienteId]);
        }

      }

      return $this->render('clinico/fichas/new.html.twig', [
        'form' => $form->createView(),
        'flow' => $flow,
        'new' => true,
        'expedienteId' => $expedienteId,
        'expediente' => $expediente,
        'edad' => $edad
      ]);
  }

  /**
   * @Route("/clinico/expedientes/{expedienteId}/fichas/{fichaId}", name="ficha_detalle", methods="GET")
   */
  public function show(ExpedienteClinico $expedienteId, $fichaId): Response
  {
    $fichaInfo = $this->tablaFichaService->getTableInfo($expedienteId->getId(), $fichaId);
    $em = $this->getDoctrine()->getManager();
    $usuario = $this->getUser();
    $ficha = $em->getRepository(Ficha::Class)->findOneBy(['id' => $fichaId]);
    $canEdit = $this->tablaFichaService->allowEdit($usuario, $ficha);
    $canOpen = $ficha->getFinalizada();

    return $this->render('clinico/fichas/show.html.twig', [
      'expedienteId' => $expedienteId->getId(),
      'expediente' =>$expedienteId,
      'fichaId' => $fichaId,
      'fichaInfo' => $fichaInfo,
      'editable' => $canEdit,
      'openable' => $canOpen,
    ]);
  }

  /**
   * @Route("/clinico/expedientes/{expedienteId}/fichas/{fichaId}/editar", name="ficha_editar", methods="GET|POST")
   */
  public function edit($expedienteId, $fichaId, DatosFlow $flow): Response
  {
    $fichaInfo = $this->tablaFichaService->getTableInfo($expedienteId, $fichaId);
    $em = $this->getDoctrine()->getManager();
    $usuario = $this->getUser();
    $expediente = $em->getRepository(ExpedienteClinico::class)->findOneBy(['id' => $expedienteId]);
    $fnacimiento = $expediente->getPersona()->getNacimiento();
    $fhoy = new \DateTime();


    $ficha = $em->getRepository(Ficha::Class)->findOneBy(['id' => $fichaId]);
    $datosClinicos = $em->getRepository(DatosClinicos::Class)->findOneBy(['ficha' => $fichaId]);

    $canEdit = $this->tablaFichaService->allowEdit($usuario, $ficha);
    //Verificar que sea el creador
    if(!$canEdit){
      $this->addFlash('warning', 'No esta autorizado para editar esa ficha');
      return $this->redirectToRoute('fichas', ['expedienteId' => $expedienteId]);
    }

    $repoFichaAttr = $em->getRepository(FichaAtributo::Class);
    $fichaEditable = new CrearDatos();

    $fichaEditable->fecha = $ficha->getFechaControl();
    $fichaEditable->motivo = $ficha->getMotivoConsulta();
    $fichaEditable->historia = $ficha->getHistoriaEnfermedad();
    $fichaEditable->visita_odontologo = $ficha->getVisitaOdontologo();
    $fichaEditable->embarazada = $ficha->getEmbarazada();
    $fichaEditable->reacciones_adversas = $ficha->getReaccionesAdversas();
    if(boolval($ficha->getReaccionesAdversas())) $fichaEditable->tipo_reaccion = $ficha->getTipoReaccion();
    $fichaEditable->trauma_dento = $ficha->getTraumaDentoalveolar();
    if(boolval($ficha->getTraumaDentoalveolar())) $fichaEditable->pieza_trauma = $ficha->getPieza();
    $fichaEditable->frecuencia_cepillado = $ficha->getFrecuenciaCepillado();

    $fichaEditable->curso = $em->getRepository(Curso::class)->findOneBy(['codigo'=>$ficha->getCurso()]);
    $fichaEditable->ciclo = $em->getRepository(PlanEstudioCiclo::class)->findOneBy(['romano'=>$ficha->getCiclo()]);


    //Aditamientos
    $aditamientos_higiene = $repoFichaAttr->findByFichaAndCatalogoTipo($fichaId, 'aditamentos');
    if(sizeof($aditamientos_higiene)>0){
      $fichaEditable->aditamientos_higiene = array_map(function($aditamiento){return $aditamiento->getAtributo();}, $aditamientos_higiene);
    }

    //Evaluacion sistemica
    $eval_sistemica = $repoFichaAttr->findByFichaAndCatalogoTipo($fichaId, 'evaluaciones');
    if(sizeof($eval_sistemica)>0){
      $fichaEditable->eval_sistemica = array_map(function($eval){return $eval->getAtributo();}, $eval_sistemica);
    }
    //Habitos bucales
    $habitos_bucales = $repoFichaAttr->findByFichaAndCatalogoTipo($fichaId, 'habitos_bucales');
    if(sizeof($habitos_bucales)>0){
      $fichaEditable->habitos_bucales = array_map(function($habito){return $habito->getAtributo();}, $habitos_bucales);
    }

    //Padecimientos bucales
    $padecimiento_bucal = $repoFichaAttr->findByFichaAndCatalogoTipo($fichaId, 'padecimiento_bucal');
    if(sizeof($padecimiento_bucal)>0){
      $fichaEditable->padecimiento_bucal = array_map(function($padecimiento){return $padecimiento->getAtributo();}, $padecimiento_bucal);
    }

    //Padecimientos generales
    $padecimientos_generales = $repoFichaAttr->findByFichaAndCatalogoTipo($fichaId, 'enfermedades_ficha');
    if(sizeof($padecimientos_generales)>0){
      $fichaEditable->padecimientos_generales = array_map(function($padecimiento){return $padecimiento->getAtributo();}, $padecimientos_generales);
    }

    //Operaciones
    $operaciones = $repoFichaAttr->findByFichaAndCatalogoTipo($fichaId, 'traumatismos');
    if(sizeof($operaciones)>0){
      $fichaEditable->operaciones = array_map(function($operacion){return $operacion->getAtributo();}, $operaciones);
    }

    //Medicamentos
    $medicamentos = $repoFichaAttr->findByFichaAndCatalogoTipo($fichaId, 'medicamentos');
    if(sizeof($medicamentos)>0){
      $fichaEditable->medicamentos = array_map(function($medicamento){return $medicamento->getAtributo();}, $medicamentos);
    }

    //Productos
    $productos = $repoFichaAttr->findByFichaAndCatalogoTipo($fichaId, 'alergia');
    if(sizeof($productos)>0){
      $fichaEditable->productos = array_map(function($producto){return $producto->getAtributo();}, $productos);
    }

    //Sustancias
    $sustancias = $repoFichaAttr->findByFichaAndCatalogoTipo($fichaId, 'sustancias');
    if(sizeof($sustancias)>0){
      $fichaEditable->sustancias = array_map(function($sustancia){return $sustancia->getAtributo();}, $sustancias);
    }

    $fichaEditable->transfusiones = $ficha->getTransfusiones();
    $fichaEditable->orientacion_sexual = $ficha->getOrientacionSexual();

    /* Datos especificos de genero */
    //Solo si es mujer adulta
    $edad = $fhoy->diff($fnacimiento)->y;
    $sexo = $em->getRepository(PersonaAtributo::class)->findOneByPersonaAndTipoCatalogo($expediente->getPersona()->getId(), 'sexo')->getAtributo()->getNombre();
    if($edad > 18 && $sexo=='Femenino'){
      $mujeres = $repoFichaAttr->findByFichaAndCatalogoTipo($fichaId, 'en_mujeres');
      if(sizeof($mujeres)>0){
        $fichaEditable->mujeres = array_map(function($mujer){return $mujer->getAtributo();}, $mujeres);
      }
      $fichaEditable->semanas_embarazo = $datosClinicos->getSemanasEmbarazo();
      $fichaEditable->numero_embarazos = $datosClinicos->getNumeroEmbarazos();
      $fichaEditable->numero_abortos = $datosClinicos->getNumeroAbortos();
      $fichaEditable->fur = $datosClinicos->getFur();
      $fichaEditable->isMujer = true;
    } else {
      $fichaEditable->isMujer = false;
      $mujeres = [];
      $fichaEditable->mujeres = [];
    }


    //Enfermedades
    $enfermedades = $repoFichaAttr->findByFichaAndCatalogoTipo($fichaId, 'historial_enfermedades');
    if(sizeof($enfermedades)>0){
      $fichaEditable->enfermedades = array_map(function($enfermedad){return $enfermedad->getAtributo();}, $enfermedades);
    }

    //Familiares
    $familiares = $repoFichaAttr->findByFichaAndCatalogoTipo($fichaId, 'historial_enfermedades_familiares');
    if(sizeof($familiares)>0){
      $fichaEditable->familiares = array_map(function($familiar){return $familiar->getAtributo();}, $familiares);
    }

    //Molestias
    $molestias = $repoFichaAttr->findByFichaAndCatalogoTipo($fichaId, 'molestias');
    if(sizeof($molestias)>0){
      $fichaEditable->molestias = array_map(function($molestia){return $molestia->getAtributo();}, $molestias);
    }

    /* Datos de vivienda */
    $fichaEditable->material = $ficha->getMaterialCasa();
    $fichaEditable->cuartos = $datosClinicos->getCuartos();
    $fichaEditable->habitantes = $datosClinicos->getHabitantes();
    $fichaEditable->personas = $datosClinicos->getPersonas();

    $servicios = $repoFichaAttr->findByFichaAndCatalogoTipo($fichaId, 'vivienda_servicios');
    if(sizeof($servicios)>0){
      $fichaEditable->servicios = array_map(function($ser){return $ser->getAtributo();}, $servicios);
    }

    /* Datos de alimentacion */

    $fichaEditable->leche = $datosClinicos->getLeche();
    $fichaEditable->huevo = $datosClinicos->getHuevo();
    $fichaEditable->carne = $datosClinicos->getCarne();
    $fichaEditable->frutas = $datosClinicos->getFrutas();
    $fichaEditable->verduras = $datosClinicos->getVerduras();
    $fichaEditable->legumbres = $datosClinicos->getLegumbres();
    $fichaEditable->cereales = $datosClinicos->getCereales();

    /* Datos de higiene */

    $fichaEditable->bano = $datosClinicos->getBano();
    $fichaEditable->ropa = $datosClinicos->getRopa();
    $fichaEditable->cepillo = $datosClinicos->getCepillo();
    $fichaEditable->hilo = $datosClinicos->getHilo();
    $fichaEditable->enjuague = $datosClinicos->getEnjuague();

    /* Lesiones */
    $fichaEditable->dolor_boca = $ficha->getLesion();
    $fichaEditable->tipo_lesion = $ficha->getTipoLesion();
    $fichaEditable->consistencia = $ficha->getConsistenciaLesion();
    $fichaEditable->localizacion_lesion = $datosClinicos->getLocalizacionLesion();
    $fichaEditable->color_lesion = $datosClinicos->getColorLesion();
    $fichaEditable->tamano_lesion = $datosClinicos->getTamanoLesion();

    /* Exploracion */
    $fichaEditable->pulso = $datosClinicos->getPulso();
    $fichaEditable->frecuencia_respiratoria = $datosClinicos->getFrecuenciaRespiratoria();
    $fichaEditable->frecuencia_cardiaca = $datosClinicos->getFrecuenciaCardiaca();
    $fichaEditable->presion_arterial = $datosClinicos->getPresionArterial();
    $fichaEditable->peso = $datosClinicos->getPeso();
    $fichaEditable->estatura = $datosClinicos->getEstatura();


//    $flow = $this->get('app.form.flow.datosFlow');
    $flow->bind($fichaEditable);

    $form = $submittedForm = $flow->createForm();

    if ($flow->isValid($submittedForm)){
      $flow->saveCurrentStepData($form);

      if($flow->nextStep()){
        $form = $flow->createForm();
      } else {
        //Save

        $ficha->setFechaControl($fichaEditable->fecha);
        $ficha->setMotivoConsulta($fichaEditable->motivo);
        $ficha->setHistoriaEnfermedad($fichaEditable->historia);
        $ficha->setVisitaOdontologo(boolval($fichaEditable->visita_odontologo));
        $ficha->setEmbarazada(boolval($fichaEditable->embarazada));
        $ficha->setReaccionesAdversas(boolval($fichaEditable->reacciones_adversas));
        $ficha->setCurso($fichaEditable->curso->getCodigo());
        $ficha->setCiclo($fichaEditable->ciclo->getRomano());
        if(boolval($fichaEditable->reacciones_adversas)) $ficha->setTipoReaccion($fichaEditable->tipo_reaccion);
        else $ficha->setTipoReaccion('');
        $ficha->setTraumaDentoalveolar(boolval($fichaEditable->trauma_dento));
        if(boolval($fichaEditable->trauma_dento)) $ficha->setPieza($fichaEditable->pieza_trauma);
        else $ficha->setPieza('');
        $ficha->setFrecuenciaCepillado($fichaEditable->frecuencia_cepillado);

        /* Datos clinicos */
        $ficha->setTransfusiones($fichaEditable->transfusiones);
        $ficha->setOrientacionSexual($fichaEditable->orientacion_sexual);
        $datosClinicos->setSemanasEmbarazo($fichaEditable->semanas_embarazo);
        $datosClinicos->setNumeroEmbarazos($fichaEditable->numero_embarazos);
        $datosClinicos->setNumeroAbortos($fichaEditable->numero_abortos);
        $datosClinicos->setFur($fichaEditable->fur);
        $ficha->setMaterialCasa($fichaEditable->material);
        $datosClinicos->setCuartos($fichaEditable->cuartos);
        $datosClinicos->setHabitantes($fichaEditable->habitantes);
        $datosClinicos->setPersonas($fichaEditable->personas);
        $datosClinicos->setLeche($fichaEditable->leche);
        $datosClinicos->setHuevo($fichaEditable->huevo);
        $datosClinicos->setCarne($fichaEditable->carne);
        $datosClinicos->setFrutas($fichaEditable->frutas);
        $datosClinicos->setVerduras($fichaEditable->verduras);
        $datosClinicos->setLegumbres($fichaEditable->legumbres);
        $datosClinicos->setCereales($fichaEditable->cereales);
        $datosClinicos->setBano($fichaEditable->bano);
        $datosClinicos->setRopa($fichaEditable->ropa);
        $datosClinicos->setCepillo($fichaEditable->cepillo);
        $datosClinicos->setHilo($fichaEditable->hilo);
        $datosClinicos->setEnjuague($fichaEditable->enjuague);
        $ficha->setLesion($fichaEditable->dolor_boca);
        $ficha->setTipoLesion($fichaEditable->tipo_lesion);
        $ficha->setConsistenciaLesion($fichaEditable->consistencia);
        $datosClinicos->setLocalizacionLesion($fichaEditable->localizacion_lesion);
        $datosClinicos->setColorLesion($fichaEditable->color_lesion);
        $datosClinicos->setTamanoLesion($fichaEditable->tamano_lesion);
        $datosClinicos->setPulso($fichaEditable->pulso);
        $datosClinicos->setFrecuenciaRespiratoria($fichaEditable->frecuencia_respiratoria);
        $datosClinicos->setFrecuenciaCardiaca($fichaEditable->frecuencia_cardiaca);
        $datosClinicos->setPresionArterial($fichaEditable->presion_arterial);
        $datosClinicos->setPeso($fichaEditable->peso);
        $datosClinicos->setEstatura($fichaEditable->estatura);


        //Atributos
        $old_atributos = array_merge($aditamientos_higiene, $eval_sistemica, $habitos_bucales, $padecimiento_bucal, $padecimientos_generales, $operaciones,
      $medicamentos, $productos, $sustancias, $mujeres, $enfermedades, $familiares, $molestias, $servicios);
        if(!is_array($fichaEditable->aditamientos_higiene)) $fichaEditable->aditamientos_higiene = $fichaEditable->aditamientos_higiene->toArray();
        if(!is_array($fichaEditable->eval_sistemica)) $fichaEditable->eval_sistemica = $fichaEditable->eval_sistemica->toArray();
        if(!is_array($fichaEditable->habitos_bucales)) $fichaEditable->habitos_bucales = $fichaEditable->habitos_bucales->toArray();
        if(!is_array($fichaEditable->padecimiento_bucal)) $fichaEditable->padecimiento_bucal = $fichaEditable->padecimiento_bucal->toArray();
        if(!is_array($fichaEditable->padecimientos_generales)) $fichaEditable->padecimientos_generales = $fichaEditable->padecimientos_generales->toArray();
        if(!is_array($fichaEditable->operaciones)) $fichaEditable->operaciones = $fichaEditable->operaciones->toArray();
        if(!is_array($fichaEditable->medicamentos)) $fichaEditable->medicamentos = $fichaEditable->medicamentos->toArray();
        if(!is_array($fichaEditable->productos)) $fichaEditable->productos = $fichaEditable->productos->toArray();
        if(!is_array($fichaEditable->sustancias)) $fichaEditable->sustancias = $fichaEditable->sustancias->toArray();
        if(!is_array($fichaEditable->mujeres)) $fichaEditable->mujeres = $fichaEditable->mujeres->toArray();
        if(!is_array($fichaEditable->enfermedades)) $fichaEditable->enfermedades = $fichaEditable->enfermedades->toArray();
        if(!is_array($fichaEditable->familiares)) $fichaEditable->familiares = $fichaEditable->familiares->toArray();
        if(!is_array($fichaEditable->molestias)) $fichaEditable->molestias = $fichaEditable->molestias->toArray();
        if(!is_array($fichaEditable->servicios)) $fichaEditable->servicios = $fichaEditable->servicios->toArray();


        $atributos = array_merge($fichaEditable->aditamientos_higiene, $fichaEditable->eval_sistemica, $fichaEditable->habitos_bucales,
        $fichaEditable->padecimiento_bucal, $fichaEditable->padecimientos_generales, $fichaEditable->operaciones, $fichaEditable->medicamentos,
        $fichaEditable->productos, $fichaEditable->sustancias, $fichaEditable->mujeres, $fichaEditable->enfermedades, $fichaEditable->familiares,
        $fichaEditable->molestias, $fichaEditable->servicios);

        foreach ($old_atributos as $old_attr) {
          $em->remove($old_attr);
        }

        foreach ($atributos as $attr) {
          $nuevoAttr = new FichaAtributo();
          $nuevoAttr->setFicha($ficha);
          $nuevoAttr->setAtributo($attr);
          $em->persist($nuevoAttr);
        }

        $em->flush();

        $flow->reset();
        $this->addFlash('success', 'Tus cambios fueron guardados');
        return $this->redirectToRoute('fichas', ['expedienteId' => $expedienteId]);
      }

    }

    return $this->render('clinico/fichas/edit.html.twig', [
      'form' => $form->createView(),
      'flow' => $flow,
      'expedienteId' => $expedienteId,
      'fichaId' => $fichaId,
      'expediente' => $expediente,
      'edad' => $edad
    ]);

  }

  /**
   * @Route("/clinico/expedientes/{expedienteId}/fichas/{fichaId}/detalle", name="ficha_expandida", methods="GET")
   */
  public function showExpanded($expedienteId, $fichaId): Response
  {
      $em = $this->getDoctrine()->getManager();
      $usuario = $this->getUser();
      $ficha = $em->getRepository(Ficha::class)->findOneBy(['id' => $fichaId]);
      $datosClinicos = $em->getRepository(DatosClinicos::class)->findOneBy(['ficha' => $fichaId]);
      $expediente = $em->getRepository(ExpedienteClinico::class)->findOneBy(['id' => $expedienteId]);
      $persona = $expediente->getPersona();
      $id_cuenta = $ficha->getCuenta()->getId();
      $estudiante = $em->getRepository(Persona::class)->findOneBy(['cuenta' => $id_cuenta])->getNombreCompleto();
      $aditamientos = $em->getRepository(FichaAtributo::class)->findByFichaAndCatalogoTipo($fichaId, 'aditamentos');
      $evaluaciones = $em->getRepository(FichaAtributo::class)->findByFichaAndCatalogoTipo($fichaId, 'evaluaciones');
      $habitos = $em->getRepository(FichaAtributo::class)->findByFichaAndCatalogoTipo($fichaId, 'habitos_bucales');
      $padecimientos = $em->getRepository(FichaAtributo::class)->findByFichaAndCatalogoTipo($fichaId, 'padecimiento_bucal');
      $enfermedades = $em->getRepository(FichaAtributo::class)->findByFichaAndCatalogoTipo($fichaId, 'enfermedades_ficha');
      $medicamentos = $em->getRepository(FichaAtributo::class)->findByFichaAndCatalogoTipo($fichaId, 'medicamentos');
      $alergias = $em->getRepository(FichaAtributo::class)->findByFichaAndCatalogoTipo($fichaId, 'alergia');
      $traumatismos = $em->getRepository(FichaAtributo::class)->findByFichaAndCatalogoTipo($fichaId, 'traumatismos');
      $sustancias = $em->getRepository(FichaAtributo::class)->findByFichaAndCatalogoTipo($fichaId, 'sustancias');
      $mujeres = $em->getRepository(FichaAtributo::class)->findByFichaAndCatalogoTipo($fichaId, 'en_mujeres');
      $historial = $em->getRepository(FichaAtributo::class)->findByFichaAndCatalogoTipo($fichaId, 'historial_enfermedades');
      $familiares = $em->getRepository(FichaAtributo::class)->findByFichaAndCatalogoTipo($fichaId, 'historial_enfermedades_familiares');
      $molestias = $em->getRepository(FichaAtributo::class)->findByFichaAndCatalogoTipo($fichaId, 'molestias');
      $servicios = $em->getRepository(FichaAtributo::class)->findByFichaAndCatalogoTipo($fichaId, 'vivienda_servicios');



      $fichaInfo = $this->tablaFichaService->getTableInfo($expedienteId, $fichaId);
      $canEdit = $this->tablaFichaService->allowEdit($usuario, $ficha);

      $isMujer = false;
      $fnacimiento = $persona->getNacimiento();
      $fhoy = new \DateTime();
      $edad = $fhoy->diff($fnacimiento)->y;
      $sexo = $em->getRepository(PersonaAtributo::class)->findOneByPersonaAndTipoCatalogo($persona->getId(), 'sexo')->getAtributo()->getNombre();
      if($edad > 18 && $sexo=='Femenino') $isMujer = true;


      return $this->render('clinico/fichas/expanded.html.twig', [
        'expedienteId' => $expedienteId,
        'expediente' => $expediente,
        'fichaId' => $fichaId,
        'fichaInfo' => $fichaInfo,
        'ficha' => $ficha,
        'clinicos' => $datosClinicos,
        'estudiante' => $estudiante,
        'persona' => $persona,
        'aditamientos' => $aditamientos,
        'evaluaciones' => $evaluaciones,
        'habitos' => $habitos,
        'padecimientos' => $padecimientos,
        'enfermedades' => $enfermedades,
        'medicamentos' => $medicamentos,
        'alergias' => $alergias,
        'traumatismos' => $traumatismos,
        'sustancias' => $sustancias,
        'mujeres' => $mujeres,
        'historial' => $historial,
        'familiares' => $familiares,
        'molestias' => $molestias,
        'servicios' => $servicios,
        'isMujer' => $isMujer,
        'editable' => $canEdit
      ]);
  }

  /**
   * @Route("/clinico/expedientes/{expedienteId}/fichas/{fichaId}/cerrar", name="ficha_cerrar", methods="GET")
   */
  public function close($expedienteId, $fichaId): Response
  {
    $em = $this->getDoctrine()->getManager();
    $usuario = $this->getUser();
    $expediente = $em->getRepository(ExpedienteClinico::class)->findOneBy(['id' => $expedienteId]);
    $ficha = $em->getRepository(Ficha::Class)->findOneBy(['id' => $fichaId]);

    $canEdit = $this->tablaFichaService->allowEdit($usuario, $ficha);

    if(!$canEdit){
      $this->addFlash('warning', 'No esta autorizado para cerrar esa ficha');
      return $this->redirectToRoute('fichas', ['expedienteId' => $expedienteId]);
    }

    $ficha->setFinalizada(1);
    $em->flush();

    $this->addFlash('success', 'Ficha cerrada');
    return $this->redirectToRoute('ficha_detalle', ['expedienteId' => $expedienteId, 'fichaId' => $fichaId]);

  }

  /**
   * @Route("/clinico/expedientes/{expedienteId}/fichas/{fichaId}/abrir", name="ficha_abrir", methods="GET")
   * @Security("has_role('ROLE_ADMIN')")
   */
  public function open($expedienteId, $fichaId): Response{
    $em = $this->getDoctrine()->getManager();
    $usuario = $this->getUser();
    $expediente = $em->getRepository(ExpedienteClinico::class)->findOneBy(['id' => $expedienteId]);
    $ficha = $em->getRepository(Ficha::Class)->findOneBy(['id' => $fichaId]);

    $canOpen = $ficha->getFinalizada();

    if(!$canOpen){
      $this->addFlash('warning', 'La ficha no se encuentra cerrada');
      return $this->redirectToRoute('fichas', ['expedienteId' => $expedienteId]);
    }

    $ficha->setFinalizada(0);
    $em->flush();

    $this->addFlash('success', 'Ficha abierta');
    return $this->redirectToRoute('ficha_detalle', ['expedienteId' => $expedienteId, 'fichaId' => $fichaId]);
  }



}
