<?php

namespace App\Security;

use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;

class AuthenticationListener
{
    /**
     * @param InteractiveLoginEvent $event
     */
    public function handle(InteractiveLoginEvent $event)
    {
        $request = $event->getRequest();

        $username = $request->request->get('_username');
        $password = $request->request->get('_password');
    }
}