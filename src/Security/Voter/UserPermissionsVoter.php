<?php


namespace App\Security\Voter;

use App\Service\UsuarioService;
use Symfony\Component\Security\Core\Authorization\AccessDecisionManagerInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class UserPermissionsVoter extends Voter
{
    /**
     * @var AccessDecisionManagerInterface
     */
    private $decision_manager;

    /**
     * @var UsuarioService
     */
    private $usuario_service;

    /**
     * @var array
     */
    private $permissions = [
        'calificacion', //registrar calificaciones
        'evaluacion', //definir evaluaciones
        'nomina', //gestionar nominas de estudiantes
        'registro_academico', //obtener calificaciones y consolidados
        'almacen', //acceder a almacenes
        'existencia', //visualizar existencias en almacén
        'movimiento', //visualizar movimientos en almacén
        'vale', //gestión de vales de insumos
        'expediente_clinico', //gestión de expediente clínico
        'ficha', //gestión de ficha clínica
        'odontograma', //definición de necesidades de tratamientos
        'reporte_clinico', //reportes individuales por ficha
        'riesgo_cariogenico', //determinar el nivel de riesgo cariogénico
        'tratamiento', //gestión de tratamientos pririotarios y ejecutados
        'reporte', //acceso a reportes generales
        'rotacion_intercambio', //intercambio en rotaciones de estudiantes
        'rotacion_oficial', //publicación de rotaciones
        'rotacion_preliminar', //genera las rotaciones
        'rotacion_previsualizacion', //visualiza rotaciones antes de confirmar
    ];

    /**
     * UserPermissionsVoter constructor.
     * @param AccessDecisionManagerInterface $decision_manager
     */
    public function __construct(AccessDecisionManagerInterface $decision_manager, UsuarioService $usuario_service)
    {
        $this->decision_manager = $decision_manager;
        $this->usuario_service = $usuario_service;
    }

    /**
     * {@inheritdoc}
     */
    protected function supports($attribute, $subject)
    {
        if (!in_array($attribute, $this->permissions)) {
            return false;
        }

        return true;
    }

    /**
     * {@inheritdoc}
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {

        $user = $token->getUser();

        if (!$user instanceof UserInterface) {
            return false;
        }

        return $this->usuario_service->isGranted($user, $attribute);
    }
}