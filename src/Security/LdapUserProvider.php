<?php

namespace App\Security;

use App\Entity\Administracion\Usuario;
use App\Service\UsuarioService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Ldap\LdapInterface;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\User\User;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Ldap\Entry;
use Symfony\Component\Security\Core\User\LdapUserProvider as BaseLdapUserProvider;

class LdapUserProvider extends BaseLdapUserProvider
{
    /**
     * @var EntityManagerInterface
     */
    private $manager;

    /**
     * @var UsuarioService
     */
    private $usuario_service;

    /**
     * LdapUserProvider constructor.
     * @param EntityManagerInterface $manager
     * @param UsuarioService $usuario_service
     * @param LdapInterface $ldap
     * @param string $baseDn
     * @param string|null $searchDn
     * @param string|null $searchPassword
     * @param array $defaultRoles
     * @param null|string $uidKey
     * @param string $filter
     * @param string|null $passwordAttribute
     */
    public function __construct(EntityManagerInterface $manager, UsuarioService $usuario_service, LdapInterface $ldap, string $baseDn, string $searchDn = null, string $searchPassword = null, array $defaultRoles = array(), ?string $uidKey = null, string $filter = '({uid_key}={username})', string $passwordAttribute = null)
    {
        parent::__construct($ldap, $baseDn, $searchDn, $searchPassword, $defaultRoles, $uidKey, $filter, $passwordAttribute);
        $this->manager = $manager;
        $this->usuario_service = $usuario_service;
    }

    /**
     * {@inheritdoc}
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function refreshUser(UserInterface $user)
    {
        if (!$user instanceof Usuario) {
            throw new UnsupportedUserException(sprintf('Instances of "%s" are not supported.', get_class($user)));
        }

        return $this->manager->getRepository(Usuario::class)->loadUserByUsername($user->getUsername());
    }

    /**
     * {@inheritdoc}
     */
    public function supportsClass($class)
    {
        return $class === Usuario::class;
    }

    /**
     * {@inheritdoc}
     * @throws \Exception
     */
    protected function loadUser($username, Entry $entry)
    {
        $user = parent::loadUser($username, $entry);

        if ($user instanceof User) {
            $_user = $this->usuario_service->loadUserByUsername($user->getUsername());
            if (!$_user) $_user = $this->usuario_service->createUserFromCoreUser($user);
            $user = $_user;
        }

        return $user;
    }
}
