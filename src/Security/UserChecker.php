<?php

namespace App\Security;

use App\Entity\Administracion\Usuario;
use App\Service\UsuarioService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Exception\DisabledException;
use Symfony\Component\Security\Core\User\UserCheckerInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class UserChecker implements UserCheckerInterface
{

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var UsuarioService
     */
    private $usuario_service;

    /**
     * UserCheckerService constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em, UsuarioService $usuario_service)
    {
        $this->em = $em;
        $this->usuario_service = $usuario_service;
    }

    /**
     * Checks the user account before authentication.
     *
     * @param UserInterface $user
     * @throws \Exception
     */
    public function checkPreAuth(UserInterface $user)
    {
        if (!$user instanceof Usuario) {
            return;
        }

        // user is disabled.
        if (!$user->isEnabled()) {
            throw new DisabledException();
        }
    }

    /**
     * Checks the user account after authentication.
     *
     * @throws
     */
    public function checkPostAuth(UserInterface $user)
    {
        if ($user instanceof Usuario) {
            /** @var Usuario $user */
            $user->setLastLogin(new \DateTime());
            $this->em->flush();
        }
    }
}