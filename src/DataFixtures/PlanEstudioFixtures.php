<?php

namespace App\DataFixtures;

use App\Entity\Academico\Carrera;
use App\Entity\Academico\PlanEstudio;
use App\Entity\Academico\PlanEstudioCurso;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class PlanEstudioFixtures extends Fixture implements DependentFixtureInterface, FixtureGroupInterface
{
    /**
     * @param ObjectManager $manager
     * @throws \Doctrine\ORM\ORMException
     */
    public function load(ObjectManager $manager)
    {
        $carrera = (new Carrera())
            ->setCodigo('D10701')
            ->setNombre('Doctorado en Cirugía Dental')
            ->setDescripcion('Título que confiere la Facultad de Odontología al profesional del área de la salud' .
                'que se ocupa de estudiar, diagnosticar, prevenir y tratar las enfermedades que involucran el sistema Estomatognático.');

        $manager->persist($carrera);

        $plan_estudio = (new PlanEstudio())
            ->setCarrera($carrera)
            ->setAnio(2009)
            ->setActivo(true);

        $manager->persist($plan_estudio);

        $plan_estudio_cursos = [
            ['codigo' => 'EST117', 'ciclo' => 1],
            ['codigo' => 'MFF117', 'ciclo' => 1],
            ['codigo' => 'PAT117', 'ciclo' => 1],
            ['codigo' => 'TTA117', 'ciclo' => 1],
            ['codigo' => 'EST217', 'ciclo' => 2],
            ['codigo' => 'MFF217', 'ciclo' => 2],
            ['codigo' => 'PAT217', 'ciclo' => 2],
            ['codigo' => 'TTA217', 'ciclo' => 2],
            ['codigo' => 'EST317', 'ciclo' => 3],
            ['codigo' => 'MFF317', 'ciclo' => 3],
            ['codigo' => 'PAT317', 'ciclo' => 3],
            ['codigo' => 'TTA317', 'ciclo' => 3],
            ['codigo' => 'EST417', 'ciclo' => 4],
            ['codigo' => 'MFF417', 'ciclo' => 4],
            ['codigo' => 'PAT417', 'ciclo' => 4],
            ['codigo' => 'TTA417', 'ciclo' => 4],
            ['codigo' => 'EST517', 'ciclo' => 5],
            ['codigo' => 'MFF517', 'ciclo' => 5],
            ['codigo' => 'PAT517', 'ciclo' => 5],
            ['codigo' => 'TTA517', 'ciclo' => 5],
            ['codigo' => 'COP117', 'ciclo' => 5],
            ['codigo' => 'OPC117', 'ciclo' => 5],
            ['codigo' => 'EST617', 'ciclo' => 6],
            ['codigo' => 'MFF617', 'ciclo' => 6],
            ['codigo' => 'PAT617', 'ciclo' => 6],
            ['codigo' => 'TTA617', 'ciclo' => 6],
            ['codigo' => 'CDO117', 'ciclo' => 6],
            ['codigo' => 'OPC217', 'ciclo' => 6],
            ['codigo' => 'DPC117', 'ciclo' => 7],
            ['codigo' => 'EPC117', 'ciclo' => 7],
            ['codigo' => 'CDP117', 'ciclo' => 7],
            ['codigo' => 'ODI117', 'ciclo' => 7],
            ['codigo' => 'ORT117', 'ciclo' => 7],
            ['codigo' => 'CDO217', 'ciclo' => 7],
            ['codigo' => 'RPC117', 'ciclo' => 7],
            ['codigo' => 'OPC317', 'ciclo' => 7],
            ['codigo' => 'CDD117', 'ciclo' => 8],
            ['codigo' => 'CDE117', 'ciclo' => 8],
            ['codigo' => 'CDP217', 'ciclo' => 8],
            ['codigo' => 'CIM117', 'ciclo' => 8],
            ['codigo' => 'ODI217', 'ciclo' => 8],
            ['codigo' => 'ORT217', 'ciclo' => 8],
            ['codigo' => 'CDR117', 'ciclo' => 8],
            ['codigo' => 'RPC217', 'ciclo' => 8],
            ['codigo' => 'OPC417', 'ciclo' => 8],
            ['codigo' => 'CDD217', 'ciclo' => 9],
            ['codigo' => 'CDE217', 'ciclo' => 9],
            ['codigo' => 'CDP317', 'ciclo' => 9],
            ['codigo' => 'CMB117', 'ciclo' => 9],
            ['codigo' => 'COI117', 'ciclo' => 9],
            ['codigo' => 'COR117', 'ciclo' => 9],
            ['codigo' => 'CIR117', 'ciclo' => 9],
            ['codigo' => 'OPC517', 'ciclo' => 9],
            ['codigo' => 'CDD317', 'ciclo' => 10],
            ['codigo' => 'CDE317', 'ciclo' => 10],
            ['codigo' => 'CDP417', 'ciclo' => 10],
            ['codigo' => 'CMB217', 'ciclo' => 10],
            ['codigo' => 'COI217', 'ciclo' => 10],
            ['codigo' => 'COR217', 'ciclo' => 10],
            ['codigo' => 'CIR217', 'ciclo' => 10],
            ['codigo' => 'OPC617', 'ciclo' => 10],
            ['codigo' => 'CDD417', 'ciclo' => 11],
            ['codigo' => 'CDE417', 'ciclo' => 11],
            ['codigo' => 'COI317', 'ciclo' => 11],
            ['codigo' => 'COR317', 'ciclo' => 11],
            ['codigo' => 'CIR317', 'ciclo' => 11],
            ['codigo' => 'OPC717', 'ciclo' => 11],
            ['codigo' => 'CIA117', 'ciclo' => 12],
            ['codigo' => 'CII117', 'ciclo' => 12],
            ['codigo' => 'IEC117', 'ciclo' => 12],
            ['codigo' => 'CIA217', 'ciclo' => 13],
            ['codigo' => 'CII217', 'ciclo' => 13],
            ['codigo' => 'IEC217', 'ciclo' => 13],
            ['codigo' => 'PDP117', 'ciclo' => 14],
            ['codigo' => 'IEC317', 'ciclo' => 14],
        ];

        $plan_estudio_curso_repository = $manager->getRepository(PlanEstudioCurso::class);
        foreach ($plan_estudio_cursos as $_plan_estudio_curso) {
            $plan_estudio_curso_repository
                ->createFromPlanEstudioAndCursoCodigo(
                    $plan_estudio,
                    $_plan_estudio_curso['codigo'],
                    $_plan_estudio_curso['ciclo']
                );
        }

        $manager->flush();
    }

    /**
     * @return array
     */
    public function getDependencies()
    {
        return [
            CursoFixtures::class,
            PlanEstudioCicloFixtures::class,
        ];
    }

    /**
     * @return array
     */
    public static function getGroups(): array
    {
        return ['dev', 'prod'];
    }

}
