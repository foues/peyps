<?php

namespace App\DataFixtures;

use App\Entity\Administracion\Permiso;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\Persistence\ObjectManager;

class PermisoFixtures extends Fixture implements FixtureGroupInterface
{
    public function load(ObjectManager $manager)
    {
        $permisos = [
            [
                'codigo' => 'administracion',
                'nombre' => 'administracion',
                'descripcion' => ''
            ],
            [
                'codigo' => 'calificacion',
                'nombre' => 'calificacion',
                'descripcion' => ''
            ],
            [
                'codigo' => 'evaluacion',
                'nombre' => 'evaluacion',
                'descripcion' => ''
            ],
            [
                'codigo' => 'nomina',
                'nombre' => 'nomina',
                'descripcion' => ''
            ],
            [
                'codigo' => 'registro_academico',
                'nombre' => 'registro_academico',
                'descripcion' => ''
            ],
            [
                'codigo' => 'almacen',
                'nombre' => 'almacen',
                'descripcion' => ''
            ],
            [
                'codigo' => 'existencia',
                'nombre' => 'existencia',
                'descripcion' => ''
            ],
            [
                'codigo' => 'movimiento',
                'nombre' => 'movimiento',
                'descripcion' => ''
            ],
            [
                'codigo' => 'vale',
                'nombre' => 'vale',
                'descripcion' => ''
            ],
            [
                'codigo' => 'expediente_clinico',
                'nombre' => 'expediente_clinico',
                'descripcion' => ''
            ],
            [
                'codigo' => 'ficha',
                'nombre' => 'ficha',
                'descripcion' => ''
            ],
            [
                'codigo' => 'odontograma',
                'nombre' => 'odontograma',
                'descripcion' => ''
            ],
            [
                'codigo' => 'reporte_clinico',
                'nombre' => 'reporte_clinico',
                'descripcion' => ''
            ],
            [
                'codigo' => 'riesgo_cariogenico',
                'nombre' => 'riesgo_cariogenico',
                'descripcion' => ''
            ],
            [
                'codigo' => 'tratamiento',
                'nombre' => 'tratamiento',
                'descripcion' => ''
            ],
            [
                'codigo' => 'reporte',
                'nombre' => 'reporte',
                'descripcion' => ''
            ],
            [
                'codigo' => 'rotacion_intercambio',
                'nombre' => 'rotacion_intercambio',
                'descripcion' => ''
            ],
            [
                'codigo' => 'rotacion_oficial',
                'nombre' => 'rotacion_oficial',
                'descripcion' => ''
            ],
            [
                'codigo' => 'rotacion_preliminar',
                'nombre' => 'rotacion_preliminar',
                'descripcion' => ''
            ],
            [
                'codigo' => 'rotacion_previsualizacion',
                'nombre' => 'rotacion_previsualizacion',
                'descripcion' => ''
            ],
        ];

        foreach ($permisos as $permiso) {
            $rol = (new Permiso())
                ->setCodigo($permiso['codigo'])
                ->setNombre($permiso['nombre'])
                ->setDescripcion($permiso['descripcion']);

            $manager->persist($rol);
        }

        $manager->flush();
    }

    /**
     * @return array
     */
    public static function getGroups(): array
    {
        return ['dev', 'prod'];
    }

}
