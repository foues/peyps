<?php

namespace App\DataFixtures;

use App\Entity\Administracion\Rol;
use App\Entity\Administracion\RolPermiso;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class RolFixtures extends Fixture implements DependentFixtureInterface, FixtureGroupInterface
{
    public function load(ObjectManager $manager)
    {

        $_roles = [
            [
                'codigo' => 'ROLE_USER',
                'nombre' => 'Usuario',
                'descripcion' => '',
                'permisos' => [
                ],
            ],
            [
                'codigo' => 'ROLE_ADMIN',
                'nombre' => 'Administrador',
                'descripcion' => '',
                'permisos' => [
                    'administracion',
                    'calificacion',
                    'evaluacion',
                    'nomina',
                    'registro_academico',
                    'almacen',
                    'existencia',
                    'movimiento',
                    'vale',
                    'expediente_clinico',
                    'ficha',
                    'odontograma',
                    'reporte_clinico',
                    'riesgo_cariogenico',
                    'tratamiento',
                    'reporte',
                    'rotacion_intercambio',
                    'rotacion_oficial',
                    'rotacion_preliminar',
                    'rotacion_previsualizacion',
                ],
            ],
            [
                'codigo' => 'ROLE_JEFE',
                'nombre' => 'Jefatura',
                'descripcion' => '',
                'permisos' => [
                    'calificacion',
                    'evaluacion',
                    'nomina',
                    'registro_academico',
                    'almacen',
                    'existencia',
                    'movimiento',
                    'vale',
                    'expediente_clinico',
                    'ficha',
                    'odontograma',
                    'reporte_clinico',
                    'riesgo_cariogenico',
                    'tratamiento',
                    'reporte',
                    'rotacion_intercambio',
                    'rotacion_oficial',
                    'rotacion_preliminar',
                    'rotacion_previsualizacion',
                ],
            ],
            [
                'codigo' => 'ROLE_ASISTENTE',
                'nombre' => 'Asistente administrativo',
                'descripcion' => '',
                'permisos' => [
                    'calificacion',
                    'nomina',
                    'registro_academico',
                    'almacen',
                    'existencia',
                    'movimiento',
                    'vale',
                    'expediente_clinico',
                    'ficha',
                    'odontograma',
                    'reporte_clinico',
                    'riesgo_cariogenico',
                    'tratamiento',
                    'reporte',
                ],
            ],
            [
                'codigo' => 'ROLE_ESTUDIANTE',
                'nombre' => 'Estudiante',
                'descripcion' => '',
                'permisos' => [
                    'calificacion',
                    'registro_academico',
                    'almacen',
                    'vale',
                    'expediente_clinico',
                    'ficha',
                    'odontograma',
                    'reporte_clinico',
                    'riesgo_cariogenico',
                    'tratamiento',
                ],
            ],
            [
                'codigo' => 'ROLE_TUTOR',
                'nombre' => 'Tutor',
                'descripcion' => '',
                'permisos' => [
                    'calificacion',
                    'registro_academico',
                    'expediente_clinico',
                    'ficha',
                    'odontograma',
                    'reporte_clinico',
                    'riesgo_cariogenico',
                    'tratamiento',
                ],
            ],
            [
                'codigo' => 'ROLE_COORDINADOR',
                'nombre' => 'Coordinador de curso',
                'descripcion' => '',
                'permisos' => [
                    'calificacion',
                    'evaluacion',
                    'nomina',
                    'registro_academico',
                    'expediente_clinico',
                    'ficha',
                    'odontograma',
                    'reporte_clinico',
                    'riesgo_cariogenico',
                    'tratamiento',
                ],
            ],
        ];

        $roles = [];
        foreach ($_roles as $_rol) {
            $rol = (new Rol())
                ->setCodigo($_rol['codigo'])
                ->setNombre($_rol['nombre'])
                ->setDescripcion($_rol['descripcion']);

            $manager->persist($rol);

            $roles[$rol->getCodigo()] = $rol;
        }

        $manager->flush();

        $permisos = [];
        foreach ($_roles as $_rol) {
            $codigo = $_rol['codigo'];
            $rol = $roles[$codigo];
            foreach ($_rol['permisos'] as $_permiso) {
                if (isset($permisos[$_permiso])) {
                    $permiso = $permisos[$_permiso];
                } else {
                    $permiso = $manager->getRepository('Administracion:Permiso')
                        ->findOneBy(['codigo' => $_permiso]);
                    $permisos[$_permiso] = $permiso;
                }

                $rol_permiso = (new RolPermiso())
                    ->setRol($rol)
                    ->setPermiso($permiso);

                $manager->persist($rol_permiso);
            }
        }

        $manager->flush();
    }

    /**
     * @return array
     */
    public function getDependencies()
    {
        return [
            PermisoFixtures::class,
        ];
    }

    /**
     * @return array
     */
    public static function getGroups(): array
    {
        return ['dev', 'prod'];
    }

}
