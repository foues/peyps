<?php

namespace App\DataFixtures;

use App\Entity\Academico\Curso;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\Persistence\ObjectManager;

class CursoFixtures extends Fixture implements FixtureGroupInterface
{
    public function load(ObjectManager $manager)
    {
        $cursos = [
            ['codigo' => 'EST117', 'nombre' => 'ESTOMATOLOGÍA I'],
            ['codigo' => 'MFF117', 'nombre' => 'MORFOFUNCION I'],
            ['codigo' => 'PAT117', 'nombre' => 'PATOLOGÍA I'],
            ['codigo' => 'TTA117', 'nombre' => 'TRATAMIENTOS I'],
            ['codigo' => 'EST217', 'nombre' => 'ESTOMATOLOGIA II'],
            ['codigo' => 'MFF217', 'nombre' => 'MORFOFUNCION II'],
            ['codigo' => 'PAT217', 'nombre' => 'PATOLOGÍA II'],
            ['codigo' => 'TTA217', 'nombre' => 'TRATAMIENTOS II'],
            ['codigo' => 'EST317', 'nombre' => 'ESTOMATOLOGÍA III'],
            ['codigo' => 'MFF317', 'nombre' => 'MORFOFUNCIÓN III'],
            ['codigo' => 'PAT317', 'nombre' => 'PATOLOGÍA III'],
            ['codigo' => 'TTA317', 'nombre' => 'TRATAMIENTOS III'],
            ['codigo' => 'EST417', 'nombre' => 'ESTOMATOLOGÍA IV'],
            ['codigo' => 'MFF417', 'nombre' => 'MORFOFUNCIÓN IV'],
            ['codigo' => 'PAT417', 'nombre' => 'PATOLOGÍA IV'],
            ['codigo' => 'TTA417', 'nombre' => 'TRATAMIENTOS IV'],
            ['codigo' => 'EST517', 'nombre' => 'ESTOMATOLOGÍA V'],
            ['codigo' => 'MFF517', 'nombre' => 'MORFOFUNCIÓN V'],
            ['codigo' => 'PAT517', 'nombre' => 'PATOLOGÍA V'],
            ['codigo' => 'TTA517', 'nombre' => 'TRATAMIENTOS V'],
            ['codigo' => 'COP117', 'nombre' => 'CLÍNICA DE ODONTOLOGÍA PREVENTIVA'],
            ['codigo' => 'OPC117', 'nombre' => 'ODONTOLOGÍA PREVENTIVA COMUNITARÍA E INVESTIGACIÓN I'],
            ['codigo' => 'EST617', 'nombre' => 'ESTOMATOLOGÍA VI'],
            ['codigo' => 'MFF617', 'nombre' => 'MORFOFUNCIÓN VI'],
            ['codigo' => 'PAT617', 'nombre' => 'PATOLOGÍA VI'],
            ['codigo' => 'TTA617', 'nombre' => 'TRATAMIENTOS VI'],
            ['codigo' => 'CDO117', 'nombre' => 'CLÍNICA DE OPERATORÍA I'],
            ['codigo' => 'OPC217', 'nombre' => 'ODONTOLOGÍA PREVENTIVA COMUNITARÍA E INVESTIGACIÓN II'],
            ['codigo' => 'DPC117', 'nombre' => 'DIAGNÓSTICO PRECLÍNICO'],
            ['codigo' => 'EPC117', 'nombre' => 'ENDODONCIA PRECLÍNICA'],
            ['codigo' => 'CDP117', 'nombre' => 'CLÍNICA DE PERIODONCIA I'],
            ['codigo' => 'ODI117', 'nombre' => 'ODONTOLOGÍA INFANTIL I'],
            ['codigo' => 'ORT117', 'nombre' => 'ORTOPEDÍA/ORTODONCIA I'],
            ['codigo' => 'CDO217', 'nombre' => 'CLÍNICA DE OPERATORÍA II'],
            ['codigo' => 'RPC117', 'nombre' => 'RESTAURATIVA PRECLÍNICA I'],
            ['codigo' => 'OPC317', 'nombre' => 'ODONTOLOGÍA PREVENTIVA COMUNITARÍA E INVESTIGACIÓN III'],
            ['codigo' => 'CDD117', 'nombre' => 'CLÍNICA DE DIAGNÓSTICO I'],
            ['codigo' => 'CDE117', 'nombre' => 'CLÍNICA DE ENDODONCIA I'],
            ['codigo' => 'CDP217', 'nombre' => 'CLÍNICA DE PERIODONCIA II'],
            ['codigo' => 'CIM117', 'nombre' => 'CIRUGÍA MAXILOFACIAL'],
            ['codigo' => 'ODI217', 'nombre' => 'ODONTOLOGÍA INFANTIL II'],
            ['codigo' => 'ORT217', 'nombre' => 'ORTOPEDÍA/ORTODONCIA II'],
            ['codigo' => 'CDR117', 'nombre' => 'CLÍNICA DE RESTAURATIVA'],
            ['codigo' => 'RPC217', 'nombre' => 'RESTAURATIVA PRECLÍNICA II'],
            ['codigo' => 'OPC417', 'nombre' => 'ODONTOLOGÍA PREVENTIVA COMUNITARÍA E INVESTIGACIÓN IV'],
            ['codigo' => 'CDD217', 'nombre' => 'CLÍNICA DE DIAGNÓSTICO II'],
            ['codigo' => 'CDE217', 'nombre' => 'CLÍNICA DE ENDODONCIA II'],
            ['codigo' => 'CDP317', 'nombre' => 'CLÍNICA DE PERIODONCIA III'],
            ['codigo' => 'CMB117', 'nombre' => 'CLÍNICA DE MEDICINA BUCAL/CIRUGÍA MAXILOFACIAL I'],
            ['codigo' => 'COI117', 'nombre' => 'CLÍNICA DE ODONTOLOGÍA INFANTIL I'],
            ['codigo' => 'COR117', 'nombre' => 'CLÍNICA DE ORTOPEDÍA/ORTODONCIA I'],
            ['codigo' => 'CIR117', 'nombre' => 'CLÍNICA INTEGRAL DE RESTAURATIVA I'],
            ['codigo' => 'OPC517', 'nombre' => 'ODONTOLOGÍA PREVENTIVA COMUNITARÍA E INVESTIGACIÓN V'],
            ['codigo' => 'CDD317', 'nombre' => 'CLÍNICA DE DIAGNÓSTICO III'],
            ['codigo' => 'CDE317', 'nombre' => 'CLÍNICA DE ENDODONCIA III'],
            ['codigo' => 'CDP417', 'nombre' => 'CLÍNICA DE PERIODONCIA IV'],
            ['codigo' => 'CMB217', 'nombre' => 'CLÍNICA DE MEDICINA BUCAL/CIRUGÍA MAXILOFACIAL II'],
            ['codigo' => 'COI217', 'nombre' => 'CLÍNICA DE ODONTOLOGÍA INFANTIL II'],
            ['codigo' => 'COR217', 'nombre' => 'CLÍNICA DE ORTOPEDIA/ORTODONCIA II'],
            ['codigo' => 'CIR217', 'nombre' => 'CLÍNICA INTEGRAL DE RESTAURATIVA II'],
            ['codigo' => 'OPC617', 'nombre' => 'ODONTOLOGÍA PREVENTIVA COMUNITARÍA E INVESTIGACIÓN VI'],
            ['codigo' => 'CDD417', 'nombre' => 'CLÍNICA DE DIAGNÓSTICO IV'],
            ['codigo' => 'CDE417', 'nombre' => 'CLÍNICA DE ENDODONCIA IV'],
            ['codigo' => 'COI317', 'nombre' => 'CLÍNICA DE ODONTOLOGÍA INFANTIL III'],
            ['codigo' => 'COR317', 'nombre' => 'CLÍNICA DE ORTOPEDIA/ORTODONCIA III'],
            ['codigo' => 'CIR317', 'nombre' => 'CLÍNICA INTEGRAL DE RESTAURATIVA III'],
            ['codigo' => 'OPC717', 'nombre' => 'ODONTOLOGÍA PREVENTIVA COMUNITARIA E INVESTIGACIÓN VII'],
            ['codigo' => 'CIA117', 'nombre' => 'CLÍNICA INTEGRAL DEL PACIENTE ADULTO (PRÁCTICA PREPROFESIONAL INTEGRADA ADULTO) I'],
            ['codigo' => 'CII117', 'nombre' => 'CLÍNICA INTEGRAL DE ODONTOLOGIA INFANTIL Y ORTODONCIA I'],
            ['codigo' => 'IEC117', 'nombre' => 'INVESTIGACIÓN EPIDEMIOLÓGICA CLÍNICA I'],
            ['codigo' => 'CIA217', 'nombre' => 'CLÍNICA INTEGRAL DEL PACIENTE ADULTO (PRÁCTICA PREPROFESIONAL INTEGRADA ADULTO) II'],
            ['codigo' => 'CII217', 'nombre' => 'CLÍNICA INTEGRAL DE ODONTOLOGÍA INFANTIL Y ORTODONCIA II'],
            ['codigo' => 'IEC217', 'nombre' => 'INVESTIGACIÓN EPIDEMIOLÓGICA CLÍNICA II'],
            ['codigo' => 'PDP117', 'nombre' => 'PRÁCTICA DISCIPLINAR PROFUNDIZADA'],
            ['codigo' => 'IEC317', 'nombre' => 'INVESTIGACIÓN EPIDEMIOLÓGICA CLÍNICA III'],
        ];

        foreach ($cursos as $_curso) {
            $curso = (new Curso())
                ->setCodigo($_curso['codigo'])
                ->setNombre($_curso['nombre']);
            $manager->persist($curso);
        }
        $manager->flush();
    }

    /**
     * @return array
     */
    public static function getGroups(): array
    {
        return ['dev', 'prod'];
    }

}
