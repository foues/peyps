<?php

namespace App\DataFixtures;

use App\Entity\Clinico\Tratamiento;
use App\Entity\Clinico\FichaCatalogo;
use App\Entity\Clinico\TratamientoEstado;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

class TratamientoFixtures extends Fixture implements DependentFixtureInterface, FixtureGroupInterface
{

    public function getDependencies()
    {
        return array(
            ClinicoCatalogoFixtures::class,
        );
    }

    public function load(ObjectManager $manager)
    {
        $tratamientos = [
            [
                'codigo' => '01',
                'area' => 'area10',
                'nombre' => 'Adaptación de márgenes + Resina fluida en zona de lesión 03',
                'estados' => ['73']
            ],
            [
                'codigo' => '02',
                'area' => 'area10',
                'nombre' => 'Adaptación de márgenes + Sellante preventivo en fosa y/o fisura en riesgo',
                'estados' => ['70']
            ],
            [
                'codigo' => '03',
                'area' => 'area10',
                'nombre' => 'Adaptación de márgenes + Sellante terapéutico en zona de lesión 01',
                'estados' => ['71']
            ],
            [
                'codigo' => '04',
                'area' => 'area10',
                'nombre' => 'Adaptación de márgenes + Sellante terapéutico en zona de lesión 02',
                'estados' => ['72']
            ],
            [
                'codigo' => '05',
                'area' => 'area9',
                'nombre' => 'Evaluar retratamiento',
                'estados' => ['50']
            ],
            [
                'codigo' => '06',
                'area' => 'area8',
                'nombre' => 'Exodoncia',
                'estados' => ['05', '06', '15', '16', '25', '26', '35', '36', '45', '46', '64', '65', '66', '74', '75', '76', '84', '85', '86']
            ],
            [
                'codigo' => '07',
                'area' => 'area6',
                'nombre' => 'Mantenedor de espacio',
                'estados' => ['97', '98']
            ],
            [
                'codigo' => '08',
                'area' => 'area10',
                'nombre' => 'No aplica',
                'estados' => ['51', '52', '53', '54', '55', '56']
            ],
            [
                'codigo' => '09',
                'area' => 'area3',
                'nombre' => 'Obturación',
                'estados' => ['04', '05', '06', '14', '15', '16', '24', '25', '26', '34', '35', '36', '44', '45', '46']
            ],
            [
                'codigo' => '10',
                'area' => 'area10',
                'nombre' => 'Obturación con resina fluida + Observación clínica + Radiográfica',
                'estados' => ['03']
            ],
            [
                'codigo' => '11',
                'area' => 'area5',
                'nombre' => 'Obturación con resina fluida limitada a la lesión',
                'estados' => ['13', '23', '33', '43', '63']
            ],
            [
                'codigo' => '12',
                'area' => 'area7',
                'nombre' => 'Obturación mínimamente invasiva',
                'estados' => ['63']
            ],
            [
                'codigo' => '13',
                'area' => 'area8',
                'nombre' => 'Obturación preventiva',
                'estados' => ['04', '05', '06', '14', '15', '16', '24', '25', '26', '34', '35', '36', '44', '45', '46', '80', '81', '82', '83', '84', '85']
            ],
            [
                'codigo' => '14',
                'area' => 'area9',
                'nombre' => 'Plan básico preventivo (profilaxis + flúor barniz + técnicas de higiene oral)',
                'estados' => ['00', '10', '20', '30', '40', '80'],
            ],
            [
                'codigo' => '15',
                'area' => 'area3',
                'nombre' => 'Prótesis',
                'estados' => ['97', '98']
            ],
            [
                'codigo' => '16',
                'area' => 'area1',
                'nombre' => 'Radiografía',
                'estados' => ['99']
            ],
            [
                'codigo' => '17',
                'area' => 'area2',
                'nombre' => 'Reconstrucción',
                'estados' => ['06', '16', '26', '36', '46']
            ],
            [
                'codigo' => '18',
                'area' => 'area3',
                'nombre' => 'Reemplazo por obturación definitiva',
                'estados' => ['80', '81', '82', '83', '84', '85', '86']
            ],
            [
                'codigo' => '19',
                'area' => 'area5',
                'nombre' => 'Reemplazo por obturación definitiva + Remineralización con flúor barniz',
                'estados' => ['81', '82']
            ],
            [
                'codigo' => '20',
                'area' => 'area9',
                'nombre' => 'Remineralización con flúor barniz',
                'estados' => ['01', '02', '11', '12', '21', '22', '31', '32', '41', '42', '61', '62', '71', '72']
            ],
            [
                'codigo' => '21',
                'area' => 'area10',
                'nombre' => 'Remineralización con flúor barniz + Observación radiográfica',
                'estados' => ['03']
            ],
            [
                'codigo' => '22',
                'area' => 'area9',
                'nombre' => 'Retratamiento',
                'estados' => ['60']
            ],
            [
                'codigo' => '23',
                'area' => 'area8',
                'nombre' => 'Retratamiento completo de obturación',
                'estados' => ['70', '71', '72']
            ],
            [
                'codigo' => '24',
                'area' => 'area7',
                'nombre' => 'Retratamiento completo de obturación + Resina fluida en zona de lesión 03',
                'estados' => ['73']
            ],
            [
                'codigo' => '25',
                'area' => 'area6',
                'nombre' => 'Retratamiento completo de sellante de fosas y fisuras terapéutico + Observación clínica y radiográfica',
                'estados' => ['13']
            ],
            [
                'codigo' => '26',
                'area' => 'area5',
                'nombre' => 'Retratamiento con obturación mínimamente invasiva',
                'estados' => ['33', '43']
            ],
            [
                'codigo' => '27',
                'area' => 'area3',
                'nombre' => 'Retratamiento con obturación preventiva',
                'estados' => ['70', '71', '72']
            ],
            [
                'codigo' => '28',
                'area' => 'area2',
                'nombre' => 'Retratamiento de obturación',
                'estados' => ['74', '75', '76']
            ],
            [
                'codigo' => '29',
                'area' => 'area1',
                'nombre' => 'Retratamiento parcial de obturación',
                'estados' => ['70', '71', '72']
            ],
            [
                'codigo' => '30',
                'area' => 'area2',
                'nombre' => 'Retratamiento parcial de obturación + Resina fluida en zona de lesión 03',
                'estados' => ['73']
            ],
            [
                'codigo' => '31',
                'area' => 'area3',
                'nombre' => 'Retratamiento parcial de sellante de fosas y fisuras',
                'estados' => ['10', '11', '12']
            ],
            [
                'codigo' => '32',
                'area' => 'area4',
                'nombre' => 'Retratamiento parcial de sellante de fosas y fisuras + Observación clínica y radiográfica',
                'estados' => ['13']
            ],
            [
                'codigo' => '33',
                'area' => 'area5',
                'nombre' => 'Retratamiento protéstico',
                'estados' => ['61', '62', '63', '64', '65', '66']
            ],
            [
                'codigo' => '34',
                'area' => 'area6',
                'nombre' => 'Retratamiento total de sellante de fosas y fisuras preventivo',
                'estados' => ['10']
            ],
            [
                'codigo' => '35',
                'area' => 'area7',
                'nombre' => 'Retratamiento total de sellante de fosas y fisuras terapéutico',
                'estados' => ['11', '12']
            ],
            [
                'codigo' => '36',
                'area' => 'area9',
                'nombre' => 'Sellante de fosas y fisuras preventivo',
                'estados' => ['00']
            ],
            [
                'codigo' => '37',
                'area' => 'area8',
                'nombre' => 'Sellante de fosas y fisuras preventivo limitado a fosa y fisura sana',
                'estados' => ['30', '40']
            ],
            [
                'codigo' => '38',
                'area' => 'area10',
                'nombre' => 'Sellante de fosas y fisuras terapéutico',
                'estados' => ['01', '02', '03', '23', '71', '72']
            ],
            [
                'codigo' => '39',
                'area' => 'area5',
                'nombre' => 'Sellante de fosas y fisuras terapéutico limitado a la lesión',
                'estados' => ['21', '22', '31', '32', '33', '41', '42', '43', '63']
            ],
            [
                'codigo' => '40',
                'area' => 'area4',
                'nombre' => 'Técnicas de higiene oral',
                'estados' => ['00', '10', '20', '30', '40', '50', '60', '80', '96', '97', '98', '99']
            ],
            [
                'codigo' => '41',
                'area' => 'area7',
                'nombre' => 'Tratamiento pulpar',
                'estados' => ['04', '05', '06', '14', '15', '16', '24', '25', '26', '34', '35', '36', '44', '45', '46', '64', '65', '66', '74', '75', '76', '80', '81', '82', '83', '84', '85', '86']
            ],
            [
                'codigo' => '42',
                'area' => 'area4',
                'nombre' => 'Ejemplo de tratamiento completo 1',
                'estados' => ['BC05', 'BC02', 'BC03', 'BC04']
            ],
            [
                'codigo' => '43',
                'area' => 'area6',
                'nombre' => 'Ejemplo de tratamiento completo 2',
                'estados' => ['BC01', 'BC02', 'BC05', 'BC04']
            ],
            [
                'codigo' => '44',
                'area' => 'area2',
                'nombre' => 'Ejemplo de tratamiento completo 3',
                'estados' => ['BC01', 'BC05', 'BC03', 'BC04']
            ],
            [
                'codigo' => '45',
                'area' => 'area1',
                'nombre' => 'Ejemplo de tratamiento completo 4',
                'estados' => ['BC01', 'BC02', 'BC03', 'BC05']
            ],
            [
                'codigo' => '46',
                'area' => 'area5',
                'nombre' => 'Ejemplo de tratamiento completo 5',
                'estados' => ['BC05', 'BC04', 'BC03']
            ],
            [
                'codigo' => '47',
                'area' => 'area9',
                'nombre' => 'Ejemplo de tratamiento completo 6',
                'estados' => ['BC01']
            ],
        ];

        $fc_repository = $manager->getRepository(FichaCatalogo::class);

        foreach ($tratamientos as $_tratamiento) {
            $area = $fc_repository->findOneByCodigoTipoAndCodigo('area_tratamiento', $_tratamiento['area']);

            $tratamiento = new Tratamiento();
            $tratamiento->setTratamientoArea($area);
            $tratamiento->setCodigo($_tratamiento['codigo']);
            $tratamiento->setNombre($_tratamiento['nombre']);
            $tratamiento->setPrecio(0);
            $tratamiento->setDescripcion('');

            $manager->persist($tratamiento);

            foreach ($_tratamiento['estados'] as $_estado) {
                if (strlen($_estado) == 2)
                    $estado = $fc_repository->findOneByCodigoTipoAndCodigo('icdas_estados', $_estado);
                else
                    $estado = $fc_repository->findOneByCodigoTipoAndCodigo('icdas_estados_bc', $_estado);

                $tratamiento_estado = new TratamientoEstado();
                $tratamiento_estado->setTratamiento($tratamiento);
                $tratamiento_estado->setEstado($estado);

                $manager->persist($tratamiento_estado);
            }
        }

        $manager->flush();

    }

    /**
     * @return array
     */
    public static function getGroups(): array
    {
        return ['dev', 'prod'];
    }

}
