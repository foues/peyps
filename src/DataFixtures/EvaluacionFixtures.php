<?php

namespace App\DataFixtures;

use App\Entity\Academico\Carrera;
use App\Entity\Academico\Evaluacion;
use App\Entity\Academico\EvaluacionTipo;
use App\Entity\Academico\PlanEstudio;
use App\Entity\Academico\UnidadIntegracion;
use App\Service\PeriodoService;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class EvaluacionFixtures extends Fixture implements DependentFixtureInterface, FixtureGroupInterface
{
    /**
     * @var PeriodoService
     */
    private $periodo_service;

    /**
     * RotacionFixture constructor.
     * @param PeriodoService $periodo_service
     */
    public function __construct(PeriodoService $periodo_service)
    {
        $this->periodo_service = $periodo_service;
    }

    /**
     * @param ObjectManager $manager
     * @throws \Exception
     */
    public function load(ObjectManager $manager)
    {
        $ciclo = $this->periodo_service->getCicloActual();

        $carrera = $manager->getRepository(Carrera::class)
            ->findOneBy(['codigo' => 'D10701']);
        $plan_estudio = $manager->getRepository(PlanEstudio::class)
            ->findOneBy(['carrera' => $carrera, 'anio' => 2009]);

        $unidad_integracion_repository = $manager->getRepository(UnidadIntegracion::class);
        $evaluacion_tipo_repository = $manager->getRepository(EvaluacionTipo::class);
        $evaluaciones = [];

        $csv = fopen(__DIR__ . '/Resources/evaluaciones.csv', 'r');

        while (!feof($csv)) {
            $line = fgetcsv($csv);
            $id = $line[0];
            $curso_codigo = $line[1];
            $programa_codigo = $line[2];
            $tipo = $line[3];
            $id_padre = $line[4];
            $nombre = $line[5];
            $ponderacion = $line[6];
            $grupal = $line[7];

            if (isset($id)) {
                $unidad_integracion = $unidad_integracion_repository
                    ->findOneByCursoAndCicloAndPrograma($plan_estudio, $curso_codigo, $ciclo, $programa_codigo);
                $evaluacion_tipo = $evaluacion_tipo_repository->findOneBy(['codigo' => $tipo]);

                $evaluacion = (new Evaluacion())
                    ->setUnidadIntegracion($unidad_integracion)
                    ->setEvaluacionTipo($evaluacion_tipo)
                    ->setNombre($nombre)
                    ->setPonderacion($ponderacion)
                    ->setGrupal($grupal);

                if (!empty($id_padre)) {
                    $evaluacion->setPadre($evaluaciones[$id_padre]);
                }

                $manager->persist($evaluacion);

                $evaluaciones[$id] = $evaluacion;
            }
        }

        $manager->flush();
    }

    /**
     * @return array
     */
    public function getDependencies()
    {
        return [
            UnidadIntegracionFixtures::class,
        ];
    }

    /**
     * @return array
     */
    public static function getGroups(): array
    {
        return ['dev'];
    }

}
