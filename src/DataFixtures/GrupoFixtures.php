<?php

namespace App\DataFixtures;

use App\Entity\Academico\PeriodoCiclo;
use App\Entity\Rotacion\Grupo;
use App\Service\PeriodoService;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class GrupoFixtures extends Fixture implements DependentFixtureInterface, FixtureGroupInterface
{
    /**
     * @var PeriodoService
     */
    private $periodo_service;

    /**
     * RotacionFixture constructor.
     * @param PeriodoService $periodo_service
     */
    public function __construct(PeriodoService $periodo_service)
    {
        $this->periodo_service = $periodo_service;
    }

    /**
     * @param ObjectManager $manager
     * @throws \Exception
     */
    public function load(ObjectManager $manager)
    {
        $manager->getConnection()->getConfiguration()->setSQLLogger(null);

        $grupo_repository = $manager->getRepository(Grupo::class);
        $ciclo_repository = $manager->getRepository(PeriodoCiclo::class);

        $csv = fopen(__DIR__ . '/Resources/grupos.csv', 'r');

        while (!feof($csv)) {
            $line = fgetcsv($csv);
            $nombre = $line[0];
            $rotable = $line[2];
            $anio = $line[3];
            $ciclo_tipo = $line[4];

            if (isset($nombre)) {
                $ciclo = $ciclo_repository->findOneByAnioAndTipo($anio, $ciclo_tipo);

                $grupo = (new Grupo())
                    ->setNombre($nombre)
                    ->setPeriodoCiclo($ciclo)
                    ->setRotable($rotable);

                $manager->persist($grupo);
            }
        }

        fclose($csv);
        $manager->flush();

        $csv = fopen(__DIR__ . '/Resources/grupos.csv', 'r');

        while (!feof($csv)) {
            $line = fgetcsv($csv);
            $nombre = $line[0];
            $nombre_padre = $line[1];
            $anio = $line[3];
            $ciclo_tipo = $line[4];

            if (isset($nombre_padre) && !empty($nombre_padre)) {
                $grupo = $grupo_repository->findOneByNombreAndCicloAnioTipo($nombre, $anio, $ciclo_tipo);
                $padre = $grupo_repository->findOneByNombreAndCicloAnioTipo($nombre_padre, $anio, $ciclo_tipo);
                $grupo->setPadre($padre);

                $manager->merge($grupo);
            }
        }

        fclose($csv);
        $manager->flush();
    }

    /**
     * This method must return an array of fixtures classes
     * on which the implementing class depends on
     *
     * @return array
     */
    public function getDependencies()
    {
        return [
            EstudianteCursoFixtures::class,
        ];
    }

    /**
     * @return array
     */
    public static function getGroups(): array
    {
        return ['dev'];
    }

}
