<?php

namespace App\DataFixtures;

use App\Entity\Administracion\Cuenta;
use App\Entity\Administracion\Institucion;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class InstitucionFixtures extends Fixture implements DependentFixtureInterface, FixtureGroupInterface
{
    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $instituciones = [
            ['codigo' => 'ues', 'nombre' => 'Universidad de El Salvador'],
            ['codigo' => 'fjccss', 'nombre' => 'Facultad de Jurisprudencia y Ciencias Sociales'],
            ['codigo' => 'fm', 'nombre' => 'Facultad de Medicina'],
            ['codigo' => 'fqf', 'nombre' => 'Facultad de Química y Farmacia'],
            ['codigo' => 'fce', 'nombre' => 'Facultad de Ciencias Económicas'],
            ['codigo' => 'fo', 'nombre' => 'Facultad de Odontología'],
            ['codigo' => 'fcchh', 'nombre' => 'Facultad de Ciencias y Humanidades'],
            ['codigo' => 'fia', 'nombre' => 'Facultad de Ingeniería y Arquitectura'],
            ['codigo' => 'fccaa', 'nombre' => 'Facultad de Ciencias Agronómicas'],
            ['codigo' => 'fccnnm', 'nombre' => 'Facultad de Ciencias Naturales y Matemática'],
            ['codigo' => 'fmocc', 'nombre' => 'Facultad Multidisciplinaria de Occidente'],
            ['codigo' => 'fmo', 'nombre' => 'Facultad Multidisciplinaria Oriental'],
            ['codigo' => 'fmp', 'nombre' => 'Facultad Multidisciplinaria Paracentral'],
            ['codigo' => 'csns_zacamil', 'nombre' => 'CSNS Zacamil'],
            ['codigo' => 'csns_cuscatancingo', 'nombre' => 'CSNS Cuscatancingo'],
            ['codigo' => 'csns_concepcion', 'nombre' => 'CSNS Concepción'],
            ['codigo' => 'csns_soyapango', 'nombre' => 'CSNS Soyapango'],
            ['codigo' => 'csns_san_marcos', 'nombre' => 'CSNS San Marcos'],
            ['codigo' => 'csns_san_martin', 'nombre' => 'CSNS San Martín'],
            ['codigo' => 'hnr', 'nombre' => 'Hospiral Nacional Rosales'],
            ['codigo' => 'cesa', 'nombre' => 'Clínica Extramural Santa Ana'],
            ['codigo' => 'ce_peru', 'nombre' => 'C.E. República del Perú'],
            ['codigo' => 'ce_dominicana', 'nombre' => 'C.E. República Dominicana'],
            ['codigo' => 'ce_espania', 'nombre' => 'C.E. España'],
            ['codigo' => 'ce_hogar_ninio', 'nombre' => 'C.E. Católico Hogar del Niño'],
            ['codigo' => 'ce_romero', 'nombre' => 'C.E. Dr. Humberto Romero Alvergue'],
            ['codigo' => 'ee_san_jacinto', 'nombre' => 'Escuela Especial San Jacinto'],
            ['codigo' => 'sello_oro', 'nombre' => 'Productos Alimenticios Sello de Oro S.A. de C.V.'],
            ['codigo' => 'cascada', 'nombre' => 'Embotelladora La Cascada S.A'],
        ];

        $institucion_repository = $manager->getRepository(Institucion::class);

        foreach ($instituciones as $_institucion) {

            $cuenta = new Cuenta();
            $manager->persist($cuenta);

            $institucion = (new Institucion())
                ->setCodigo($_institucion['codigo'])
                ->setNombre($_institucion['nombre'])
                ->setCuenta($cuenta);

            if (isset($_institucion['padre'])) {
                $padre = $institucion_repository->findOneBy(
                    ['codigo' => $_institucion['padre']]
                );
                $institucion->setPadre($padre);
            }

            $manager->persist($institucion);
        }

        $manager->flush();

        $institucion_padres = [
            ['institucion' => 'fjccss', 'padre' => 'ues'],
            ['institucion' => 'fm', 'padre' => 'ues'],
            ['institucion' => 'fqf', 'padre' => 'ues'],
            ['institucion' => 'fce', 'padre' => 'ues'],
            ['institucion' => 'fo', 'padre' => 'ues'],
            ['institucion' => 'fcchh', 'padre' => 'ues'],
            ['institucion' => 'fia', 'padre' => 'ues'],
            ['institucion' => 'fccaa', 'padre' => 'ues'],
            ['institucion' => 'fccnnm', 'padre' => 'ues'],
            ['institucion' => 'fmocc', 'padre' => 'ues'],
            ['institucion' => 'fmo', 'padre' => 'ues'],
            ['institucion' => 'fmp', 'padre' => 'ues'],
        ];

        foreach ($institucion_padres as $institucion_padre) {
            $institucion = $institucion_repository
                ->findOneBy(['codigo' => $institucion_padre['institucion']]);
            $padre = $institucion_repository
                ->findOneBy(['codigo' => $institucion_padre['padre']]);

            $institucion->setPadre($padre);

            $manager->merge($institucion);
        }

        $manager->flush();
    }

    /**
     * @return array
     */
    public function getDependencies()
    {
        return [
            ExpedienteFixtures::class,
        ];
    }

    /**
     * @return array
     */
    public static function getGroups(): array
    {
        return ['dev', 'prod'];
    }

}
