<?php

namespace App\DataFixtures;

use App\Entity\Administracion\Cuenta;
use App\Entity\Administracion\Usuario;
use App\Entity\Administracion\UsuarioRol;
use App\Service\UsuarioService;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UsuarioFixtures extends Fixture implements DependentFixtureInterface, FixtureGroupInterface
{
    /**
     * @var UserPasswordEncoderInterface
     */
    private $encoder;

    /**
     * @var UsuarioService
     */
    private $usuario_service;

    /**
     * UsuarioFixtures constructor.
     * @param UserPasswordEncoderInterface $encoder
     * @param UsuarioService $usuario_service
     */
    public function __construct(UserPasswordEncoderInterface $encoder, UsuarioService $usuario_service)
    {
        $this->encoder = $encoder;
        $this->usuario_service = $usuario_service;
    }

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $_usuarios = [
            [
                'username' => 'bc11023',
                'email' => 'bc11023@ues.edu.sv',
                'password' => '123456'
            ],
            [
                'username' => 'ma10043',
                'email' => 'ma10043@ues.edu.sv',
                'password' => '123456'
            ],
            [
                'username' => 'mm11221',
                'email' => 'mm11221@ues.edu.sv',
                'password' => '123456'
            ],
            [
                'username' => 'rd12004',
                'email' => 'rd12004@ues.edu.sv',
                'password' => '123456'
            ],
            [
                'username' => 'lisset.lopez',
                'email' => 'lisset.lopez@ues.edu.sv',
                'password' => '123456'
            ],
            [
                'username' => 'rafael.burgos',
                'email' => 'rafael.burgos@ues.edu.sv',
                'password' => '123456'
            ],
            [
                'username' => 'rudy.chicas',
                'email' => 'rudy.chicas@ues.edu.sv',
                'password' => '123456'
            ]
        ];

        $usuarios = [];
        foreach ($_usuarios as $_usuario) {
            $usuario = new Usuario();
            $usuario->setUsername($_usuario['username']);
            $usuario->setEmail($_usuario['email']);
            $password = $this->encoder->encodePassword($usuario, $_usuario['password']);
            $usuario->setPassword($password);
            $cuenta = new Cuenta();
            $manager->persist($cuenta);
            $usuario->setCuenta($cuenta);
            $manager->persist($usuario);

            $usuarios[] = $usuario;
        }

        $manager->flush();

        foreach ($usuarios as $usuario) {
            $this->usuario_service->setUserRoles($usuario, ['ROLE_USER', 'ROLE_ADMIN']);
        }

        $manager->flush();
    }

    /**
     * @return array
     */
    public function getDependencies()
    {
        return [
            RolFixtures::class,
            ExpedienteFixtures::class,
        ];
    }

    /**
     * @return array
     */
    public static function getGroups(): array
    {
        return ['dev', 'prod'];
    }

}
