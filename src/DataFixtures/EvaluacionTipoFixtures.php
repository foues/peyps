<?php

namespace App\DataFixtures;

use App\Entity\Academico\EvaluacionTipo;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\Persistence\ObjectManager;

class EvaluacionTipoFixtures extends Fixture implements FixtureGroupInterface
{
    public function load(ObjectManager $manager)
    {
        $evaluaciones_tipos = [
            [
                'codigo' => 'calculada',
                'nombre' => 'Calculada',
                'descripcion' => 'Evaluaciones que poseen subevaluaciones de tipo detalle. 
                Estas evaluaciones no poseen una calificación directa, ya que se calcula con el promedio de las calificaciones detalle.
                Pueden corresponder a calificaciones grupales o individuales.',
            ],
            [
                'codigo' => 'unica',
                'nombre' => 'Única',
                'descripcion' => 'Evaluaciones que no poseen sub evaluaciones y para la cual cualquier estudiante solo puede tener una calificación correspondiente. 
                Generalmente corresponden a calificaciones individuales.',
            ],
            [
                'codigo' => 'detalle',
                'nombre' => 'Detalle',
                'descripcion' => 'Evaluaciones que se realizan a diario o semana a semana.
                Pueden corresponder a calificaciones grupales o individuales.',
            ],
        ];

        foreach ($evaluaciones_tipos as $_evaluacion_tipo) {
            $evaluacion_tipo = (new EvaluacionTipo())
                ->setCodigo($_evaluacion_tipo['codigo'])
                ->setNombre($_evaluacion_tipo['nombre'])
                ->setDescripcion($_evaluacion_tipo['descripcion']);
            $manager->persist($evaluacion_tipo);
        }

        $manager->flush();
    }

    /**
     * @return array
     */
    public static function getGroups(): array
    {
        return ['dev', 'prod'];
    }

}
