<?php

namespace App\DataFixtures;

use App\Entity\Administracion\Institucion;
use App\Entity\Rotacion\Programa;
use App\Entity\Rotacion\ProgramaInstitucion;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class ProgramaFixtures extends Fixture implements DependentFixtureInterface, FixtureGroupInterface
{
    public function load(ObjectManager $manager)
    {
        $programas = [
            ['codigo' => 'ppe', 'nombre' => 'Programa Preventivo Escolar'],
            ['codigo' => 'ulc', 'nombre' => 'Universidad Libre de Caries'],
            ['codigo' => 'clc', 'nombre' => 'Comunidad Libre de Caries'],
            ['codigo' => 'cesa', 'nombre' => 'Clínica Extramural Santa Ana'],
            ['codigo' => 'csns', 'nombre' => 'Clinicas Sistema Nacional de Salud'],
            ['codigo' => 'hnr', 'nombre' => 'Hospital Nacional Rosales'],
        ];

        foreach ($programas as $_programa) {
            $programa = (new Programa())
                ->setCodigo($_programa['codigo'])
                ->setNombre($_programa['nombre']);

            $manager->persist($programa);
        }

        $manager->flush();

        $programa_instituciones = [
            /**
             * Las cantidades máximas para cada una de las instituciones de los programas son las siguientes:
             * ppe: 44
             * csns: 4
             * hnr: 10
             * cesa: 18
             * ulc: 18
             * clc: 36
             *
             * Temporalmente se asignan otras cantidades hasta que se corrijan las rotaciones
             * TODO: corregir distribución y cantidad de estudiantes por grupos de rotación
             */
            ['programa' => 'ppe', 'institucion' => 'ce_peru', 'cantidad' => 44],
            ['programa' => 'ppe', 'institucion' => 'ce_dominicana', 'cantidad' => 44],
            ['programa' => 'ppe', 'institucion' => 'ce_espania', 'cantidad' => 44],
            ['programa' => 'ppe', 'institucion' => 'ce_hogar_ninio', 'cantidad' => 44],
            ['programa' => 'ppe', 'institucion' => 'ce_romero', 'cantidad' => 44],
            ['programa' => 'ppe', 'institucion' => 'ee_san_jacinto', 'cantidad' => 44],
            ['programa' => 'csns', 'institucion' => 'csns_zacamil', 'cantidad' => 8],
            ['programa' => 'csns', 'institucion' => 'csns_cuscatancingo', 'cantidad' => 8],
            ['programa' => 'csns', 'institucion' => 'csns_concepcion', 'cantidad' => 8],
            ['programa' => 'csns', 'institucion' => 'csns_soyapango', 'cantidad' => 8],
            ['programa' => 'csns', 'institucion' => 'csns_san_marcos', 'cantidad' => 8],
            ['programa' => 'csns', 'institucion' => 'csns_san_martin', 'cantidad' => 8],
            ['programa' => 'hnr', 'institucion' => 'hnr', 'cantidad' => 50],
            ['programa' => 'cesa', 'institucion' => 'cesa', 'cantidad' => 48],
            ['programa' => 'ulc', 'institucion' => 'fccnnm', 'cantidad' => 50],
            ['programa' => 'clc', 'institucion' => 'sello_oro', 'cantidad' => 50],
            ['programa' => 'clc', 'institucion' => 'cascada', 'cantidad' => 50],
        ];

        $programa_repository = $manager->getRepository(Programa::class);
        $institucion_repository = $manager->getRepository(Institucion::class);

        foreach ($programa_instituciones as $_programa_institucion) {
            $programa = $programa_repository
                ->findOneBy(['codigo' => $_programa_institucion['programa']]);
            $institucion = $institucion_repository
                ->findOneBy(['codigo' => $_programa_institucion['institucion']]);

            $programa_institucion = (new ProgramaInstitucion())
                ->setPrograma($programa)
                ->setInstitucion($institucion)
                ->setCantidadEstudiante($_programa_institucion['cantidad'])
                ->setActivo(true);

            $manager->persist($programa_institucion);
        }

        $manager->flush();
    }

    /**
     * @return array
     */
    public function getDependencies()
    {
        return [
            InstitucionFixtures::class,
        ];
    }

    /**
     * @return array
     */
    public static function getGroups(): array
    {
        return ['dev', 'prod'];
    }

}
