<?php

namespace App\DataFixtures;

use App\Entity\Academico\PeriodoCiclo;
use App\Entity\Rotacion\Grupo;
use App\Entity\Rotacion\Planificacion;
use App\Entity\Rotacion\Programa;
use App\Entity\Rotacion\ProgramaCalendario;
use App\Service\PeriodoService;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class ProgramaCalendarioFixture extends Fixture implements DependentFixtureInterface, FixtureGroupInterface
{
    /**
     * @var PeriodoService
     */
    private $periodo_service;

    /**
     * RotacionFixture constructor.
     * @param PeriodoService $periodo_service
     */
    public function __construct(PeriodoService $periodo_service)
    {
        $this->periodo_service = $periodo_service;
    }

    /**
     * @param ObjectManager $manager
     * @throws \Exception
     */
    public function load(ObjectManager $manager)
    {
        $ciclo = $this->periodo_service->getCicloActual();

        $planificacion = (new Planificacion())
            ->setPeriodoCiclo($ciclo);

        $manager->persist($planificacion);

        $manager->getConnection()->getConfiguration()->setSQLLogger(null);

        $grupo_repository = $manager->getRepository(Grupo::class);
        $ciclo_repository = $manager->getRepository(PeriodoCiclo::class);
        $programa_repository = $manager->getRepository(Programa::class);

        $csv = fopen(__DIR__ . '/Resources/calendario.csv', 'r');

        while (!feof($csv)) {
            $line = fgetcsv($csv);
            $programa_codigo = $line[0];
            $anio = $line[1];
            $ciclo_tipo = $line[2];
            $grupo_padre_nombre = $line[3];
            $grupo_nombre = $line[4];
            $tipo = $line[5];
            $fecha = $line[6];

            if (isset($programa_codigo)) {
                $grupo = $grupo_repository->findOneByNombreAndCicloAnioTipo($grupo_nombre, $anio, $ciclo_tipo, $grupo_padre_nombre);
                $programa = $programa_repository->findOneBy(['codigo' => $programa_codigo]);
                $ciclo = $ciclo_repository->findOneByAnioAndTipo($anio, $ciclo_tipo);
                $fecha = \DateTime::createFromFormat('Y-m-d', $fecha);

                $programa_calendario = (new ProgramaCalendario())
                    ->setPeriodoCiclo($ciclo)
                    ->setPrograma($programa)
                    ->setGrupo($grupo)
                    ->setTipo($tipo)
                    ->setFecha($fecha);

                $manager->persist($programa_calendario);
            }
        }

        $manager->flush();
    }

    /**
     * This method must return an array of fixtures classes
     * on which the implementing class depends on
     *
     * @return array
     */
    public function getDependencies()
    {
        return [
            GrupoFixtures::class,
        ];
    }

    /**
     * @return array
     */
    public static function getGroups(): array
    {
        return ['dev'];
    }

}
