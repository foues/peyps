<?php

namespace App\DataFixtures;

use App\Entity\Administracion\Catalogo;
use App\Entity\Administracion\CatalogoTipo;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\Persistence\ObjectManager;

class AdministracionCatalogoFixtures extends Fixture implements FixtureGroupInterface
{
    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {

        $catalogos = [
            [
                'tipo' => [
                    'codigo' => 'division_politica',
                    'nombre' => 'Division Politica',
                ],
                'items' => [
                    [
                        'codigo' => 'div_1',
                        'nombre' => 'San Marcos',
                    ],
                    [
                        'codigo' => 'div_2',
                        'nombre' => 'San Salvador',
                    ],
                ]
            ],
            [
                'tipo' => [
                    'codigo' => 'estado_civil',
                    'nombre' => 'Estado Civil',
                    'descripcion' => 'Estados Civiles'
                ],
                'items' => [
                    [
                        'codigo' => 'solte',
                        'nombre' => 'Soltero',
                    ],
                    [
                        'codigo' => 'casad',
                        'nombre' => 'Casado',
                    ],
                    [
                        'codigo' => 'viudo',
                        'nombre' => 'Viudo',
                    ],
                    [
                        'codigo' => 'divor',
                        'nombre' => 'Divorciado',
                    ],
                ]
            ],
            [
                'tipo' => [
                    'codigo' => 'nivel_academico',
                    'nombre' => 'Nivel Academico',
                ],
                'items' => [
                    [
                        'codigo' => 'unive',
                        'nombre' => 'Universitario',
                    ],
                    [
                        'codigo' => 'media',
                        'nombre' => 'Educacion Media',
                    ],
                    [
                        'codigo' => 'basic',
                        'nombre' => 'Educacion Basica',
                    ],
                ]
            ],
            [
                'tipo' => [
                    'codigo' => 'ocupacion',
                    'nombre' => 'Ocupacion',
                ],
                'items' => [
                    [
                        'codigo' => 'ocu_1',
                        'nombre' => 'Ingeniero Industrial',
                    ],
                    [
                        'codigo' => 'ocu_2',
                        'nombre' => 'Ingeniero en Sistemas',
                    ],
                ]
            ],
            [
                'tipo' => [
                    'codigo' => 'profesion',
                    'nombre' => 'Profesion',
                ],
                'items' => [
                    [
                        'codigo' => 'pro_1',
                        'nombre' => 'Ingeniero',
                    ],
                    [
                        'codigo' => 'pro_2',
                        'nombre' => 'Contador',
                    ],
                ]
            ],
            [
                'tipo' => [
                    'codigo' => 'referencia',
                    'nombre' => 'Referencia',
                ],
                'items' => [
                    [
                        'codigo' => 'priva',
                        'nombre' => 'Privada',
                    ],
                    [
                        'codigo' => 'udes',
                        'nombre' => 'UDES',
                    ],
                    [
                        'codigo' => 'hospi',
                        'nombre' => 'Hospital',
                    ],
                    [
                        'codigo' => 'sanmi',
                        'nombre' => 'Sanidad Militar',
                    ],
                    [
                        'codigo' => 'pnc',
                        'nombre' => 'PNC',
                    ],
                    [
                        'codigo' => 'otros',
                        'nombre' => 'Otros',
                    ],
                ]
            ],
            [
                'tipo' => [
                    'codigo' => 'sexo',
                    'nombre' => 'Sexo',
                    'descripcion' => 'Sexo Persona'
                ],
                'items' => [
                    [
                        'codigo' => 'sex_m',
                        'nombre' => 'Masculino',
                    ],
                    [
                        'codigo' => 'sex_f',
                        'nombre' => 'Femenino',
                    ],
                ]
            ],
            [
                'tipo' => [
                    'codigo' => 'tipo_area',
                    'nombre' => 'Tipo de Area',
                ],
                'items' => [
                    [
                        'codigo' => 'are_1',
                        'nombre' => 'San Salvador',
                    ],
                    [
                        'codigo' => 'are_2',
                        'nombre' => 'Santa Ana',
                    ],
                    [
                        'codigo' => 'are_3',
                        'nombre' => 'La libertad',
                    ],
                ]
            ],
            [
                'tipo' => [
                    'codigo' => 'tipo_direccion',
                    'nombre' => 'Tipo de Direccion',
                ],
                'items' => [
                    [
                        'codigo' => 'urban',
                        'nombre' => 'Urbano',
                    ],
                    [
                        'codigo' => 'rural',
                        'nombre' => 'Rural',
                    ],
                ]
            ],
            [
                'tipo' => [
                    'codigo' => 'tipo_paciente',
                    'nombre' => 'Tipo de Paciente',
                ],
                'items' => [
                    [
                        'codigo' => 'adult',
                        'nombre' => 'Adulto',
                    ],
                    [
                        'codigo' => 'adole',
                        'nombre' => 'Adolescente',
                    ],
                    [
                        'codigo' => 'niño',
                        'nombre' => 'Niño',
                    ],
                ]
            ],
            [
                'tipo' => [
                    'codigo' => 'tipo_telefono',
                    'nombre' => 'Tipo de Telefono',
                ],
                'items' => [
                    [
                        'codigo' => 'fijo',
                        'nombre' => 'Telefono Fijo',
                    ],
                    [
                        'codigo' => 'movil',
                        'nombre' => 'Telefono Movil',
                    ],
                    [
                        'codigo' => 'teltr',
                        'nombre' => 'Telefono Trabajo',
                    ],
                    [
                        'codigo' => 'telap',
                        'nombre' => 'Telefono Apoderado',
                    ],
                ]
            ],
        ];

        foreach ($catalogos as $catalogo) {
            $_tipo = $catalogo['tipo'];

            $tipo = new CatalogoTipo();
            $tipo->setCodigo($_tipo['codigo']);
            $tipo->setNombre($_tipo['nombre']);
            if (array_key_exists('descripcion', $_tipo)) $tipo->setDescripcion($_tipo['descripcion']);
            else $tipo->setDescripcion('');

            $manager->persist($tipo);

            foreach ($catalogo['items'] as $_item) {
                $item = new Catalogo();
                $item->setCatalogoTipo($tipo);
                $item->setCodigo($_item['codigo']);
                $item->setNombre($_item['nombre']);

                $manager->persist($item);
            }
        }

        $manager->flush();

    }

    /**
     * @return array
     */
    public static function getGroups(): array
    {
        return ['dev'];
    }
}
