<?php

namespace App\DataFixtures;

use App\Entity\Academico\Carrera;
use App\Entity\Academico\Estudiante;
use App\Entity\Academico\EstudianteEstado;
use App\Entity\Academico\EstudiantePlanEstudio;
use App\Entity\Academico\PlanEstudio;
use App\Entity\Administracion\Persona;
use App\Entity\Administracion\Usuario;
use App\Service\UsuarioService;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class EstudianteFixtures extends Fixture implements DependentFixtureInterface, FixtureGroupInterface
{
    /**
     * @var UsuarioService
     */
    private $usuario_service;

    /**
     * EstudianteFixtures constructor.
     * @param UsuarioService $usuario_service
     */
    public function __construct(UsuarioService $usuario_service)
    {
        $this->usuario_service = $usuario_service;
    }

    /**
     * @param ObjectManager $manager
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Exception
     */
    public function load(ObjectManager $manager)
    {
        $manager->getConnection()->getConfiguration()->setSQLLogger(null);

        $csv = fopen(__DIR__ . '/Resources/nomina.csv', 'r');

        $carrera = $manager->getRepository(Carrera::class)
            ->findOneBy(['codigo' => 'D10701']);
        $plan_estudio = $manager->getRepository(PlanEstudio::class)
            ->findOneBy(['carrera' => $carrera, 'anio' => 2009]);
        $estudiante_estado = $manager->getRepository(EstudianteEstado::class)
            ->findOneBy(['tipo' => 'plan_estudio', 'codigo' => 'activo']);
        $usuarios = [];

        $i = 0;

        while (!feof($csv)) {
            $line = fgetcsv($csv);
            $carnet = $line[0];

            if (isset($carnet)) {
                $usuario = $manager->getRepository(Usuario::class)
                    ->loadUserByUsername($carnet);

                if ($usuario) {
                    $cuenta = $usuario->getCuenta();
                } else {
                    $usuario = $this->usuario_service->createUserFromUsername(strtolower($carnet));
                    $cuenta = $usuario->getCuenta();
                }

                $usuarios[] = $usuario;

                $persona = (new Persona())
                    ->setCuenta($cuenta)
                    ->setNombre($line[1])
                    ->setApellido($line[2]);

                $manager->persist($persona);

                $estudiante = (new Estudiante())
                    ->setPersona($persona)
                    ->setCarnet($carnet);

                $manager->persist($estudiante);

                $estudiante_plan_estudio = (new EstudiantePlanEstudio())
                    ->setEstudiante($estudiante)
                    ->setPlanEstudio($plan_estudio)
                    ->setEstudianteEstado($estudiante_estado)
                    ->setCum($line[3]);

                $manager->persist($estudiante_plan_estudio);

                $i = $i + 1;

                if ($i % 100 == 0) {
                    $manager->flush();
                    //$manager->clear();
                    echo 'Registrando 100 estudiantes', PHP_EOL;
                }
            }
        }

        fclose($csv);
        $manager->flush();

        foreach ($usuarios as $usuario) {
            $this->usuario_service->setUserRoles($usuario, ['ROLE_USER', 'ROLE_ESTUDIANTE']);
        }

        $manager->flush();
    }

    /**
     * @return array
     */
    public function getDependencies()
    {
        return [
            UsuarioFixtures::class,
            PlanEstudioFixtures::class,
            EstudianteEstadoFixtures::class,
        ];
    }

    /**
     * @return array
     */
    public static function getGroups(): array
    {
        return ['dev'];
    }

}
