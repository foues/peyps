<?php

namespace App\DataFixtures;

use App\Entity\Administracion\Menu;
use App\Entity\Administracion\MenuPermiso;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class MenuFixtures extends Fixture implements DependentFixtureInterface, FixtureGroupInterface
{
    public function load(ObjectManager $manager)
    {

        $menus = [
            [
                'nombre' => 'Rotaciones',
                'descripcion' => '',
                'url' => '#',
                'icon' => 'fa fa-sync-alt fa-fw',
                'enabled' => true,
                'permisos' => ['rotacion_preliminar', 'rotacion_previsualizacion'],
                'submenus' => [
                    [
                        'nombre' => 'Calendarización',
                        'descripcion' => '',
                        'url' => '/rotacion/calendarizacion',
                        'icon' => '',
                        'enabled' => true,
                        'permisos' => ['rotacion_preliminar'],
                    ], [
                        'nombre' => 'Parametrización',
                        'descripcion' => '',
                        'url' => '/rotacion/parametrizacion',
                        'icon' => '',
                        'enabled' => true,
                        'permisos' => ['rotacion_preliminar'],
                    ], [
                        'nombre' => 'Resumen',
                        'descripcion' => '',
                        'url' => '/rotacion/calendarizacion/resumen',
                        'icon' => '',
                        'enabled' => true,
                        'permisos' => ['rotacion_preliminar'],
                    ], [
                        'nombre' => 'Rotación preliminar',
                        'descripcion' => '',
                        'url' => '/rotacionPrevisualizacion/',
                        'icon' => '',
                        'enabled' => true,
                        'permisos' => ['rotacion_previsualizacion'],
                    ], [
                        'nombre' => 'Cupos por institución',
                        'descripcion' => '',
                        'url' => '/rotacionPrevisualizacion/cupos_rotacion',
                        'icon' => '',
                        'enabled' => true,
                        'permisos' => ['rotacion_previsualizacion'],
                    ],
                ],
            ], [
                'nombre' => 'Académico',
                'descripcion' => '',
                'url' => '#',
                'icon' => 'fa fa-user-graduate fa-fw',
                'enabled' => true,
                'permisos' => ['nomina', 'evaluacion', 'calificacion', 'registro_academico'],
                'submenus' => [
                    [
                        'nombre' => 'Nómina general',
                        'descripcion' => '',
                        'url' => '/estudiantes/parametros',
                        'icon' => '',
                        'enabled' => true,
                        'permisos' => ['nomina'],
                    ], [
                        'nombre' => 'Nómina oficial',
                        'descripcion' => '',
                        'url' => '/estudiante/nomina',
                        'icon' => '',
                        'enabled' => true,
                        'permisos' => ['nomina'],
                    ], [
                        'nombre' => 'Carga nómina',
                        'descripcion' => '',
                        'url' => '/estudiantes/cargar',
                        'icon' => '',
                        'enabled' => true,
                        'permisos' => ['nomina'],
                    ], [
                        'nombre' => 'Evaluaciones',
                        'descripcion' => '',
                        'url' => '/academico',
                        'icon' => '',
                        'enabled' => true,
                        'permisos' => ['evaluacion'],
                    ], [
                        'nombre' => 'Calificar',
                        'descripcion' => '',
                        'url' => '/academico/notas',
                        'icon' => '',
                        'enabled' => true,
                        'permisos' => ['calificacion'],
                    ], [
                        'nombre' => 'Calificaciones',
                        'descripcion' => '',
                        'url' => '/academico/calificaciones',
                        'icon' => '',
                        'enabled' => true,
                        'permisos' => ['registro_academico'],
                    ], [
                        'nombre' => 'Registro',
                        'descripcion' => '',
                        'url' => '/academico/registro',
                        'icon' => '',
                        'enabled' => true,
                        'permisos' => ['registro_academico'],
                    ], [
                        'nombre' => 'Consolidado de notas',
                        'descripcion' => '',
                        'url' => '/academico/consolidado/parametros',
                        'icon' => '',
                        'enabled' => true,
                        'permisos' => ['registro_academico'],
                    ],
                ],
            ], [
                'nombre' => 'Expedientes clínicos',
                'descripcion' => '',
                'url' => '/clinico/expedientes',
                'icon' => 'fa fa-notes-medical fa-fw',
                'enabled' => true,
                'permisos' => ['expediente_clinico'],
                'submenus' => [
                ],
            ], [
                'nombre' => 'Almacenes',
                'descripcion' => '',
                'url' => '/almacen',
                'icon' => 'fa fa-box-open fa-fw',
                'enabled' => true,
                'permisos' => ['almacen'],
                'submenus' => [
                ],
            ], [
                'nombre' => 'Administración',
                'descripcion' => '',
                'url' => '#',
                'icon' => 'fa fa-cog fa-fw',
                'enabled' => true,
                'permisos' => ['administracion', 'reporte'],
                'submenus' => [
                    [
                        'nombre' => 'Panel',
                        'descripcion' => 'Panel de administración',
                        'url' => '/admin/dashboard',
                        'icon' => '',
                        'enabled' => true,
                        'permisos' => ['administracion'],
                    ], [
                        'nombre' => 'Reportes',
                        'descripcion' => '',
                        'url' => '/reportes/index',
                        'icon' => '',
                        'enabled' => true,
                        'permisos' => ['reporte'],
                    ],
                ],
            ],
        ];

        foreach ($menus as $_menu) {
            $menu = (new Menu())
                ->setNombre($_menu['nombre'])
                ->setDescripcion($_menu['descripcion'])
                ->setUrl($_menu['url'])
                ->setIcon($_menu['icon'])
                ->setEnabled($_menu['enabled']);

            $manager->persist($menu);

            foreach ($_menu['submenus'] as $_submenu) {
                $submenu = (new Menu())
                    ->setNombre($_submenu['nombre'])
                    ->setDescripcion($_submenu['descripcion'])
                    ->setUrl($_submenu['url'])
                    ->setIcon($_submenu['icon'])
                    ->setEnabled($_submenu['enabled'])
                    ->setPadre($menu);

                $manager->persist($submenu);

                $this->setPermisos($manager, $submenu, $_submenu['permisos']);
            }

            $this->setPermisos($manager, $menu, $_menu['permisos']);
        }

        $manager->flush();
    }

    /**
     * @return array
     */
    public function getDependencies()
    {
        return [
            PermisoFixtures::class,
        ];
    }

    /**
     * @return array
     */
    public static function getGroups(): array
    {
        return ['dev', 'menu'];
    }

    /**
     * @param ObjectManager $em
     * @param Menu $menu
     * @param array $_permisos
     */
    private function setPermisos(ObjectManager $em, Menu $menu, array $_permisos)
    {
        foreach ($_permisos as $_permiso) {
            $permiso = $em->getRepository('Administracion:Permiso')
                ->findOneBy(['nombre' => $_permiso]);

            $menu_permiso = (new MenuPermiso())
                ->setMenu($menu)
                ->setPermiso($permiso);

            $em->persist($menu_permiso);
        }
    }
}
