<?php

namespace App\DataFixtures;

use App\Entity\Clinico\FichaCatalogo;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\Persistence\ObjectManager;

class ClinicoCatalogoFixtures extends Fixture implements FixtureGroupInterface
{
    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $catalogos = [
            [
                'tipo' => [
                    'codigo' => 'aditamentos',
                    'nombre' => 'Aditamentos de Higiene Bucal',
                ],
                'items' => [
                    [
                        'codigo' => 'adita_cepillos',
                        'nombre' => 'Cepillo',
                    ],
                    [
                        'codigo' => 'adita_pasta',
                        'nombre' => 'Pasta Dental',
                    ],
                    [
                        'codigo' => 'adita_hilo',
                        'nombre' => 'Hilo Dental',
                    ],
                    [
                        'codigo' => 'adita_enjuague',
                        'nombre' => 'Enjuage Bucal',
                    ],
                ]
            ],
            [
                'tipo' => [
                    'codigo' => 'icdas_caries',
                    'nombre' => 'Código ICDAS para caries',
                ],
                'items' => [
                    [
                        'codigo' => '0',
                        'nombre' => '0',
                        'descripcion' => 'No evidencia de caries',
                    ],
                    [
                        'codigo' => '1',
                        'nombre' => '1',
                        'descripcion' => 'Primer cambio visible en el esmalte',
                    ],
                    [
                        'codigo' => '2',
                        'nombre' => '2',
                        'descripcion' => 'Lesión de caries observada cuando está húmedo',
                    ],
                    [
                        'codigo' => '3',
                        'nombre' => '3',
                        'descripcion' => 'Ruptura localizada del esmalte debida a caries sin dentina visible',
                    ],
                    [
                        'codigo' => '4',
                        'nombre' => '4',
                        'descripcion' => 'Sombra oscura subyacente de dentina',
                    ],
                    [
                        'codigo' => '5',
                        'nombre' => '5',
                        'descripcion' => 'Cavidad detectable con detina visible',
                    ],
                    [
                        'codigo' => '6',
                        'nombre' => '6',
                        'descripcion' => 'Cavidad detectable extensa con dentina visible',
                    ],
                ]
            ],
            [
                'tipo' => [
                    'codigo' => 'icdas_estados',
                    'nombre' => 'Código ICDAS para una pieza o superficie',
                ],
                'items' => [
                    [
                        'codigo' => '00',
                        'nombre' => 'Sano, no obturado ni sellado',
                        'descripcion' => 'diente-sano'
                    ],
                    [
                        'codigo' => '01',
                        'nombre' => 'Cambio en el esmalte, no obturado ni sellado',
                        'descripcion' => 'diente-amarillo'
                    ],
                    [
                        'codigo' => '02',
                        'nombre' => 'Lesión, no obturado ni sellado',
                        'descripcion' => 'diente-amarillo'
                    ],
                    [
                        'codigo' => '03',
                        'nombre' => 'Ruptura, no obturado ni sellado',
                        'descripcion' => 'diente-naranja'
                    ],
                    [
                        'codigo' => '04',
                        'nombre' => 'Sombra oscura, no obturado ni sellado',
                        'descripcion' => 'diente-naranja'
                    ],
                    [
                        'codigo' => '05',
                        'nombre' => 'Cavidad, no obturado ni sellado',
                        'descripcion' => 'diente-rojo'
                    ],
                    [
                        'codigo' => '06',
                        'nombre' => 'Cavidad extensa, no obturado ni sellado',
                        'descripcion' => 'diente-rojo'
                    ],
                    [
                        'codigo' => '10',
                        'nombre' => 'Sano, con sellante parcial',
                        'descripcion' => 'diente-marcado'
                    ],
                    [
                        'codigo' => '11',
                        'nombre' => 'Cambio en el esmalte, con sellante parcial',
                        'descripcion' => 'diente-marcado'
                    ],
                    [
                        'codigo' => '12',
                        'nombre' => 'Lesión, con sellante parcial',
                        'descripcion' => 'diente-marcado'
                    ],
                    [
                        'codigo' => '13',
                        'nombre' => 'Ruptura, con sellante parcial',
                        'descripcion' => 'diente-marcado'
                    ],
                    [
                        'codigo' => '14',
                        'nombre' => 'Sombra oscura, con sellante parcial',
                        'descripcion' => 'diente-marcado'
                    ],
                    [
                        'codigo' => '15',
                        'nombre' => 'Cavidad, con sellante parcial',
                        'descripcion' => 'diente-marcado'
                    ],
                    [
                        'codigo' => '16',
                        'nombre' => 'Cavidad extensa, con sellante parcial',
                        'descripcion' => 'diente-marcado'
                    ],
                    [
                        'codigo' => '20',
                        'nombre' => 'Sano, con sellante completo',
                        'descripcion' => 'diente-marcado'
                    ],
                    [
                        'codigo' => '21',
                        'nombre' => 'Cambio en el esmalte, con sellante completo',
                        'descripcion' => 'diente-marcado'
                    ],
                    [
                        'codigo' => '22',
                        'nombre' => 'Lesión, con sellante completo',
                        'descripcion' => 'diente-marcado'
                    ],
                    [
                        'codigo' => '23',
                        'nombre' => 'Ruptura, con sellante completo',
                        'descripcion' => 'diente-marcado'
                    ],
                    [
                        'codigo' => '24',
                        'nombre' => 'Sombra oscura, con sellante completo',
                        'descripcion' => 'diente-marcado'
                    ],
                    [
                        'codigo' => '25',
                        'nombre' => 'Cavidad, con sellante completo',
                        'descripcion' => 'diente-marcado'
                    ],
                    [
                        'codigo' => '26',
                        'nombre' => 'Cavidad extensa, con sellante completo',
                        'descripcion' => 'diente-marcado'
                    ],
                    [
                        'codigo' => '30',
                        'nombre' => 'Sano, con obturación de color de diente',
                        'descripcion' => 'diente-obturado-sano'
                    ],
                    [
                        'codigo' => '31',
                        'nombre' => 'Cambio en el esmalte, con obturación de color de diente',
                        'descripcion' => 'diente-obturado-caries'
                    ],
                    [
                        'codigo' => '32',
                        'nombre' => 'Lesión, con obturación de color de diente',
                        'descripcion' => 'diente-obturado-caries'
                    ],
                    [
                        'codigo' => '33',
                        'nombre' => 'Ruptura, con obturación de color de diente',
                        'descripcion' => 'diente-obturado-caries'
                    ],
                    [
                        'codigo' => '34',
                        'nombre' => 'Sombra oscura, con obturación de color de diente',
                        'descripcion' => 'diente-obturado-caries'
                    ],
                    [
                        'codigo' => '35',
                        'nombre' => 'Cavidad, con obturación de color de diente',
                        'descripcion' => 'diente-obturado-caries'
                    ],
                    [
                        'codigo' => '36',
                        'nombre' => 'Cavidad extensa, con obturación de color de diente',
                        'descripcion' => 'diente-obturado-caries'
                    ],
                    [
                        'codigo' => '40',
                        'nombre' => 'Sano, con obturación en amalgama',
                        'descripcion' => 'diente-obturado-sano'
                    ],
                    [
                        'codigo' => '41',
                        'nombre' => 'Cambio en el esmalte, con obturación en amalgama',
                        'descripcion' => 'diente-obturado-caries'
                    ],
                    [
                        'codigo' => '42',
                        'nombre' => 'Lesión, con obturación en amalgama',
                        'descripcion' => 'diente-obturado-caries'
                    ],
                    [
                        'codigo' => '43',
                        'nombre' => 'Ruptura, con obturación en amalgama',
                        'descripcion' => 'diente-obturado-caries'
                    ],
                    [
                        'codigo' => '44',
                        'nombre' => 'Sombra oscura, con obturación en amalgama',
                        'descripcion' => 'diente-obturado-caries'
                    ],
                    [
                        'codigo' => '45',
                        'nombre' => 'Cavidad, con obturación en amalgama',
                        'descripcion' => 'diente-obturado-caries'
                    ],
                    [
                        'codigo' => '46',
                        'nombre' => 'Cavidad extensa, con obturación en amalgama',
                        'descripcion' => 'diente-obturado-caries'
                    ],
                    [
                        'codigo' => '50',
                        'nombre' => 'Sano, con corona de acero',
                        'descripcion' => 'diente-marcado'
                    ],
                    [
                        'codigo' => '51',
                        'nombre' => 'Cambio en el esmalte, con corona de acero',
                        'descripcion' => 'diente-marcado'
                    ],
                    [
                        'codigo' => '52',
                        'nombre' => 'Lesión, con corona de acero',
                        'descripcion' => 'diente-marcado'
                    ],
                    [
                        'codigo' => '53',
                        'nombre' => 'Ruptura, con corona de acero',
                        'descripcion' => 'diente-marcado'
                    ],
                    [
                        'codigo' => '54',
                        'nombre' => 'Sombra oscura, con corona de acero',
                        'descripcion' => 'diente-marcado'
                    ],
                    [
                        'codigo' => '55',
                        'nombre' => 'Cavidad, con corona de acero',
                        'descripcion' => 'diente-marcado'
                    ],
                    [
                        'codigo' => '56',
                        'nombre' => 'Cavidad extensa, con corona de acero',
                        'descripcion' => 'diente-marcado'
                    ],
                    [
                        'codigo' => '60',
                        'nombre' => 'Sano, con corona en porcelana u otro metal',
                        'descripcion' => 'diente-marcado'
                    ],
                    [
                        'codigo' => '61',
                        'nombre' => 'Cambio en el esmalte, con corona en porcelana u otro metal',
                        'descripcion' => 'diente-marcado'
                    ],
                    [
                        'codigo' => '62',
                        'nombre' => 'Lesión, con corona en porcelana u otro metal',
                        'descripcion' => 'diente-marcado'
                    ],
                    [
                        'codigo' => '63',
                        'nombre' => 'Ruptura, con corona en porcelana u otro metal',
                        'descripcion' => 'diente-marcado'
                    ],
                    [
                        'codigo' => '64',
                        'nombre' => 'Sombra oscura, con corona en porcelana u otro metal',
                        'descripcion' => 'diente-marcado'
                    ],
                    [
                        'codigo' => '65',
                        'nombre' => 'Cavidad, con corona en porcelana u otro metal',
                        'descripcion' => 'diente-marcado'
                    ],
                    [
                        'codigo' => '66',
                        'nombre' => 'Cavidad extensa, con corona en porcelana u otro metal',
                        'descripcion' => 'diente-marcado'
                    ],
                    [
                        'codigo' => '70',
                        'nombre' => 'Sano, con obturación perdida o fracturada',
                        'descripcion' => 'obturacion-perdida'
                    ],
                    [
                        'codigo' => '71',
                        'nombre' => 'Cambio en el esmalte, con obturación perdida o fracturada',
                        'descripcion' => 'diente-marcado'
                    ],
                    [
                        'codigo' => '72',
                        'nombre' => 'Lesión, con obturación perdida o fracturada',
                        'descripcion' => 'diente-marcado'
                    ],
                    [
                        'codigo' => '73',
                        'nombre' => 'Ruptura, con obturación perdida o fracturada',
                        'descripcion' => 'diente-marcado'
                    ],
                    [
                        'codigo' => '74',
                        'nombre' => 'Sombra oscura, con obturación perdida o fracturada',
                        'descripcion' => 'diente-marcado'
                    ],
                    [
                        'codigo' => '75',
                        'nombre' => 'Cavidad, con obturación perdida o fracturada',
                        'descripcion' => 'diente-marcado'
                    ],
                    [
                        'codigo' => '76',
                        'nombre' => 'Cavidad extensa, con obturación perdida o fracturada',
                        'descripcion' => 'diente-marcado'
                    ],
                    [
                        'codigo' => '80',
                        'nombre' => 'Sano, con obturación temporal',
                        'descripcion' => 'obturacion-perdida'
                    ],
                    [
                        'codigo' => '81',
                        'nombre' => 'Cambio en el esmalte, con obturación temporal',
                        'descripcion' => 'diente-marcado'
                    ],
                    [
                        'codigo' => '82',
                        'nombre' => 'Lesión, con obturación temporal',
                        'descripcion' => 'diente-marcado'
                    ],
                    [
                        'codigo' => '83',
                        'nombre' => 'Ruptura, con obturación temporal',
                        'descripcion' => 'diente-marcado'
                    ],
                    [
                        'codigo' => '84',
                        'nombre' => 'Sombra oscura, con obturación temporal',
                        'descripcion' => 'diente-marcado'
                    ],
                    [
                        'codigo' => '85',
                        'nombre' => 'Cavidad, con obturación temporal',
                        'descripcion' => 'diente-marcado'
                    ],
                    [
                        'codigo' => '86',
                        'nombre' => 'Cavidad extensa, con obturación temporal',
                        'descripcion' => 'diente-marcado'
                    ],
                    [
                        'codigo' => '96',
                        'nombre' => 'Excluido',
                        'descripcion' => 'diente-marcado'
                    ],
                    [
                        'codigo' => '97',
                        'nombre' => 'Perdido por caries',
                        'descripcion' => 'diente-perdido-caries'
                    ],
                    [
                        'codigo' => '98',
                        'nombre' => 'Perdido por otras causas',
                        'descripcion' => 'diente-perdido-otros'
                    ],
                    [
                        'codigo' => '99',
                        'nombre' => 'No erupcionado',
                        'descripcion' => 'diente-marcado'
                    ]
                ]
            ],
            [
                'tipo' => [
                    'codigo' => 'icdas_estados_bc',
                    'nombre' => 'Código ICDAS para una pieza completa',
                ],
                'items' => [
                    [
                        'codigo' => 'BC01',
                        'nombre' => 'Ejemplo estado BC 1',
                        'descripcion' => 'Estado de la boca completa 1'
                    ],
                    [
                        'codigo' => 'BC02',
                        'nombre' => 'Ejemplo estado BC 2',
                        'descripcion' => 'Estado de la boca completa 2'
                    ],
                    [
                        'codigo' => 'BC03',
                        'nombre' => 'Ejemplo estado BC 3',
                        'descripcion' => 'Estado de la boca completa 3'
                    ],
                    [
                        'codigo' => 'BC04',
                        'nombre' => 'Ejemplo estado BC 4',
                        'descripcion' => 'Estado de la boca completa 4'
                    ],
                    [
                        'codigo' => 'BC05',
                        'nombre' => 'Ejemplo estado BC 5',
                        'descripcion' => 'Estado de la boca completa 5'
                    ],
                ]
            ],
            [
                'tipo' => [
                    'codigo' => 'icdas_restauracion',
                    'nombre' => 'Código ICDAS para restauraciones y sellantes',
                ],
                'items' => [
                    [
                        'codigo' => '000',
                        'nombre' => '0',
                        'descripcion' => 'No sellado, no restaurado'
                    ],
                    [
                        'codigo' => '001',
                        'nombre' => '1',
                        'descripcion' => 'Sellante, Parcial',
                    ],
                    [
                        'codigo' => '002',
                        'nombre' => '2',
                        'descripcion' => 'Sellante, Completo',
                    ],
                    [
                        'codigo' => '003',
                        'nombre' => '3',
                        'descripcion' => 'Restauración color del diente',
                    ],
                    [
                        'codigo' => '004',
                        'nombre' => '4',
                        'descripcion' => 'Restauración en amalgama',
                    ],
                    [
                        'codigo' => '005',
                        'nombre' => '5',
                        'descripcion' => 'Corona de acero inoxidable',
                    ],
                    [
                        'codigo' => '006',
                        'nombre' => '6',
                        'descripcion' => 'Corona o Carilla en porcelona, oro o metal porcelana',
                    ],
                    [
                        'codigo' => '007',
                        'nombre' => '7',
                        'descripcion' => 'Restauración perdida o fracturada',
                    ],
                    [
                        'codigo' => '008',
                        'nombre' => '8',
                        'descripcion' => 'Restauración Temporal',
                    ],
                ]
            ],
            [
                'tipo' => [
                    'codigo' => 'evaluaciones',
                    'nombre' => 'Evaluaciones Sistemicas',
                ],
                'items' => [
                    [
                        'codigo' => 'eval_sist_hiperte',
                        'nombre' => 'Hipertension',
                    ],
                    [
                        'codigo' => 'eval_sist_diabetes',
                        'nombre' => 'Diabetes',
                    ],
                    [
                        'codigo' => 'eval_sist_angina',
                        'nombre' => 'Angina de Pecho',
                    ],
                    [
                        'codigo' => 'eval_sist_soplo',
                        'nombre' => 'Soplo Cardiaco',
                    ],
                    [
                        'codigo' => 'eval_sist_arritmia',
                        'nombre' => 'Arritmia',
                    ],
                    [
                        'codigo' => 'eval_sist_colesterol',
                        'nombre' => 'Colesterol Alto',
                    ],
                    [
                        'codigo' => 'eval_sist_obesidad',
                        'nombre' => 'Obesidad',
                    ],
                    [
                        'codigo' => 'eval_sist_hipertirio',
                        'nombre' => 'Hipertirioidismo',
                    ],
                ]
            ],
            [
                'tipo' => [
                    'codigo' => 'frecuencia_cepillado',
                    'nombre' => 'Frecuencia Cepillado',
                ],
                'items' => [
                    [
                        'codigo' => 'frec_cepi_3',
                        'nombre' => '3 veces',
                    ],
                    [
                        'codigo' => 'frec_cepi_2',
                        'nombre' => '2 veces',
                    ],
                    [
                        'codigo' => 'frec_cepi_1',
                        'nombre' => '1 vez',
                    ],
                    [
                        'codigo' => 'frec_cepi_0',
                        'nombre' => 'Ninguna',
                    ],
                ]
            ],
            [
                'tipo' => [
                    'codigo' => 'habitos_bucales',
                    'nombre' => 'Habitos Bucales',
                ],
                'items' => [
                    [
                        'codigo' => 'hab_buc_respirador',
                        'nombre' => 'Respirador Bucal',
                    ],
                    [
                        'codigo' => 'hab_buc_onico',
                        'nombre' => 'Onicofagia',
                    ],
                    [
                        'codigo' => 'hab_buc_bruxismo',
                        'nombre' => 'Bruxismo',
                    ],
                    [
                        'codigo' => 'hab_buc_fumar',
                        'nombre' => 'Fumar',
                    ],
                    [
                        'codigo' => 'hab_buc_succion',
                        'nombre' => 'Succion del Dedo',
                    ],
                    [
                        'codigo' => 'hab_buc_deglu',
                        'nombre' => 'Deglucion Atipica',
                    ],
                ]
            ],
            [
                'tipo' => [
                    'codigo' => 'historia_odontologica',
                    'nombre' => 'Historia Odontologica',
                ],
                'items' => [
                    [
                        'codigo' => 'hist_odon_visita',
                        'nombre' => 'Visitas al Odontologo',
                    ],
                    [
                        'codigo' => 'hist_odon_trauma',
                        'nombre' => 'Trauma DentoAlveolar',
                    ],
                    [
                        'codigo' => 'hist_odon_reacciones',
                        'nombre' => 'Reacciones Adversas',
                    ],
                ]
            ],
            [
                'tipo' => [
                    'codigo' => 'historia_ingesta',
                    'nombre' => 'Historia de Ingesta',
                ],
                'items' => [
                    [
                        'codigo' => 'hist_inges_desa',
                        'nombre' => 'Desayuno',
                    ],
                    [
                        'codigo' => 'hist_inges_desa_alm',
                        'nombre' => 'Entre Des y Almuerzo',
                    ],
                    [
                        'codigo' => 'hist_inges_alm',
                        'nombre' => 'Almuerzo',
                    ],
                    [
                        'codigo' => 'hist_inges_alm_cena',
                        'nombre' => 'Entre Alm y Cena',
                    ],
                    [
                        'codigo' => 'hist_inges_cena',
                        'nombre' => 'Cena',
                    ],
                    [
                        'codigo' => 'hist_inges_jarabes',
                        'nombre' => 'Jarabes en la Noche',
                    ],
                    [
                        'codigo' => 'hist_inges_desp_cena',
                        'nombre' => 'Despues de Cena',
                    ],
                    [
                        'codigo' => 'hist_inges_noche',
                        'nombre' => 'Por la Noche',
                    ],
                ]
            ],
            [
                'tipo' => [
                    'codigo' => 'lesion_02',
                    'nombre' => 'Lesión radiolúcida distal',
                ],
                'items' => [
                    [
                        'codigo' => 'lesion_021',
                        'nombre' => 'R1',
                    ],
                    [
                        'codigo' => 'lesion_022',
                        'nombre' => 'R2',
                    ],
                    [
                        'codigo' => 'lesion_023',
                        'nombre' => 'R3',
                    ],
                    [
                        'codigo' => 'lesion_024',
                        'nombre' => 'R4',
                    ],
                    [
                        'codigo' => 'lesion_025',
                        'nombre' => 'R5',
                    ],
                ]
            ],
            [
                'tipo' => [
                    'codigo' => 'lesion_09',
                    'nombre' => 'Ligamento periodontal (Continuidad)',
                ],
                'items' => [
                    [
                        'codigo' => 'lesion_091',
                        'nombre' => 'Continuo',
                    ],
                    [
                        'codigo' => 'lesion_092',
                        'nombre' => 'Discontinuo',
                    ],
                ]
            ],
            [
                'tipo' => [
                    'codigo' => 'lesion_08',
                    'nombre' => 'Ligamento periodontal (grosor)',
                ],
                'items' => [
                    [
                        'codigo' => 'lesion_081',
                        'nombre' => 'Aumento',
                    ],
                    [
                        'codigo' => 'lesion_082',
                        'nombre' => 'Disminución',
                    ],
                ]
            ],
            [
                'tipo' => [
                    'codigo' => 'lesion_01',
                    'nombre' => 'Lesión radiolúcida mesial',
                ],
                'items' => [
                    [
                        'codigo' => 'lesion_011',
                        'nombre' => 'R1',
                    ],
                    [
                        'codigo' => 'lesion_012',
                        'nombre' => 'R2',
                    ],
                    [
                        'codigo' => 'lesion_013',
                        'nombre' => 'R3',
                    ],
                    [
                        'codigo' => 'lesion_014',
                        'nombre' => 'R4',
                    ],
                    [
                        'codigo' => 'lesion_015',
                        'nombre' => 'R5',
                    ],
                ]
            ],
            [
                'tipo' => [
                    'codigo' => 'lesion_05',
                    'nombre' => 'Obliteración de conductos',
                ],
                'items' => [
                    [
                        'codigo' => 'lesion_051',
                        'nombre' => 'Sí',
                    ],
                    [
                        'codigo' => 'lesion_052',
                        'nombre' => 'No',
                    ],
                ]
            ],
            [
                'tipo' => [
                    'codigo' => 'lesion_03',
                    'nombre' => 'Lesión radiolúcida oclusal',
                ],
                'items' => [
                    [
                        'codigo' => 'lesion_031',
                        'nombre' => 'Esmalte',
                    ],
                    [
                        'codigo' => 'lesion_032',
                        'nombre' => 'Dentrina',
                    ],
                    [
                        'codigo' => 'lesion_033',
                        'nombre' => 'Pulpa',
                    ],
                ]
            ],
            [
                'tipo' => [
                    'codigo' => 'lesion_04',
                    'nombre' => 'Lesión radiolúcida',
                ],
                'items' => [
                    [
                        'codigo' => 'lesion_041',
                        'nombre' => 'Periapicales',
                    ],
                    [
                        'codigo' => 'lesion_042',
                        'nombre' => 'Furca',
                    ],
                    [
                        'codigo' => 'lesion_043',
                        'nombre' => 'Periapicales y furca',
                    ],
                ]
            ],
            [
                'tipo' => [
                    'codigo' => 'lesion_10',
                    'nombre' => 'Lesiones radiopacas',
                ],
                'items' => [
                    [
                        'codigo' => 'lesion_101',
                        'nombre' => 'Corona',
                    ],
                    [
                        'codigo' => 'lesion_102',
                        'nombre' => 'Raiz',
                    ],
                ]
            ],
            [
                'tipo' => [
                    'codigo' => 'lesion_11',
                    'nombre' => 'Lesiones radiopacas',
                ],
                'items' => [
                    [
                        'codigo' => 'lesion_111',
                        'nombre' => 'Periápice',
                    ],
                    [
                        'codigo' => 'lesion_112',
                        'nombre' => 'Hueso',
                    ],
                ]
            ],
            [
                'tipo' => [
                    'codigo' => 'lesion_06',
                    'nombre' => 'Reabsorciones',
                ],
                'items' => [
                    [
                        'codigo' => 'lesion_061',
                        'nombre' => 'Lámina dura',
                    ],
                    [
                        'codigo' => 'lesion_062',
                        'nombre' => 'Cresta alveolar',
                    ],
                ]
            ],
            [
                'tipo' => [
                    'codigo' => 'lesion_07',
                    'nombre' => 'Reabsorciones',
                ],
                'items' => [
                    [
                        'codigo' => 'lesion_071',
                        'nombre' => 'Radicular',
                    ],
                    [
                        'codigo' => 'lesion_072',
                        'nombre' => 'Periaplical',
                    ],
                ]
            ],
            [
                'tipo' => [
                    'codigo' => 'planes_preventivos',
                    'nombre' => 'Planes Preventivos',
                ],
                'items' => [
                    [
                        'codigo' => 'plan_prev_periodo',
                        'nombre' => 'Tratamiento Periodental',
                    ],
                    [
                        'codigo' => 'plan_prev_endo',
                        'nombre' => 'Tratamiento Endodontico',
                    ],
                    [
                        'codigo' => 'plan_prev_quirur',
                        'nombre' => 'Tratamiento Quirurgico',
                    ],
                    [
                        'codigo' => 'plan_prev_oper',
                        'nombre' => 'Operatoria Dental',
                    ],
                    [
                        'codigo' => 'plan_prev_infecc',
                        'nombre' => 'Control de Infeccion',
                    ],
                    [
                        'codigo' => 'plan_prev_educa',
                        'nombre' => 'Educacion y Motivacion',
                    ],
                    [
                        'codigo' => 'plan_prev_fluor',
                        'nombre' => 'Fluorterapia',
                    ],
                    [
                        'codigo' => 'plan_prev_sella',
                        'nombre' => 'Sellantes',
                    ],
                ]
            ],
            [
                'tipo' => [
                    'codigo' => 'riesgo_cariogenico',
                    'nombre' => 'Riesgo Cariogenico',
                ],
                'items' => [
                    [
                        'codigo' => 'riesgo_alto',
                        'nombre' => 'Alto',
                    ],
                    [
                        'codigo' => 'riesgo_moderado',
                        'nombre' => 'Moderado',
                    ],
                    [
                        'codigo' => 'riesgo_bajo',
                        'nombre' => 'Bajo',
                    ],
                ]
            ],
            [
                'tipo' => [
                    'codigo' => 'tipo_expediente',
                    'nombre' => 'Tipo de Expediente',
                ],
                'items' => [
                    [
                        'codigo' => 'expediente_preventiva',
                        'nombre' => 'Preventiva',
                    ],
                    [
                        'codigo' => 'expediente_extramural',
                        'nombre' => 'Extramural',
                    ],
                ]
            ],
            [
                'tipo' => [
                    'codigo' => 'area_tratamiento',
                    'nombre' => 'Código ICDAS para áreas de tratamiento',
                ],
                'items' => [
                    [
                        'codigo' => 'area1',
                        'nombre' => 'Periodontal',
                    ],
                    [
                        'codigo' => 'area2',
                        'nombre' => 'Endodónticos',
                    ],
                    [
                        'codigo' => 'area3',
                        'nombre' => 'Quirúrgicos',
                    ],
                    [
                        'codigo' => 'area4',
                        'nombre' => 'Operatorio dental',
                    ],
                    [
                        'codigo' => 'area5',
                        'nombre' => 'Educación y motivación',
                    ],
                    [
                        'codigo' => 'area6',
                        'nombre' => 'Fluorterapia',
                    ],
                    [
                        'codigo' => 'area7',
                        'nombre' => 'Sellantes o resinas preventivas',
                    ],
                    [
                        'codigo' => 'area8',
                        'nombre' => 'Control de dieta',
                    ],
                    [
                        'codigo' => 'area9',
                        'nombre' => 'Control de hábitos',
                    ],
                    [
                        'codigo' => 'area10',
                        'nombre' => 'Otras terapias complementarias',
                    ],
                ]
            ],
            [
                'tipo' => [
                    'codigo' => 'alergia',
                    'nombre' => 'Alergias',
                ],
                'items' => [
                    [
                        'codigo' => 'aler_penici',
                        'nombre' => 'Penicilinas',
                    ],
                    [
                        'codigo' => 'aler_sulfas',
                        'nombre' => 'Sulfas',
                    ],
                    [
                        'codigo' => 'aler_pari',
                        'nombre' => 'Parizolonas',
                    ],
                    [
                        'codigo' => 'aler_aspi',
                        'nombre' => 'Aspirina',
                    ],
                    [
                        'codigo' => 'aler_acetam',
                        'nombre' => 'Acetaminofén',
                    ],
                    [
                        'codigo' => 'aler_sedantes',
                        'nombre' => 'Sedantes',
                    ],
                    [
                        'codigo' => 'aler_anest',
                        'nombre' => 'Anestésico local',
                    ],
                    [
                        'codigo' => 'aler_alimen',
                        'nombre' => 'Alimentos',
                    ],
                    [
                        'codigo' => 'aler_polen',
                        'nombre' => 'Polen',
                    ],
                    [
                        'codigo' => 'aler_pelo',
                        'nombre' => 'Pelo o pluma de animales',
                    ],
                    [
                        'codigo' => 'aler_otro',
                        'nombre' => 'Otra',
                    ],
                ]
            ],
            [
                'tipo' => [
                    'codigo' => 'enfermedades_ficha',
                    'nombre' => 'Padecimiento actual de enfermedades',
                ],
                'items' => [
                    [
                        'codigo' => 'enfer_hipertension',
                        'nombre' => 'Hipertensión arterial',
                    ],
                    [
                        'codigo' => 'enfer_diabetes',
                        'nombre' => 'Diabetes Mellitus',
                    ],
                    [
                        'codigo' => 'enfer_angina',
                        'nombre' => 'Angia de pecho',
                    ],
                    [
                        'codigo' => 'enfer_soplo',
                        'nombre' => 'Soplo cardíaco',
                    ],
                    [
                        'codigo' => 'enfer_arritmia',
                        'nombre' => 'Arritmia',
                    ],
                    [
                        'codigo' => 'enfer_colesterol',
                        'nombre' => 'Colesterol alto',
                    ],
                    [
                        'codigo' => 'enfer_obesidad',
                        'nombre' => 'Obesidad',
                    ],
                    [
                        'codigo' => 'enfer_hipertiroidimos',
                        'nombre' => 'Hipertiroidismo',
                    ],
                    [
                        'codigo' => 'enfer_hipotiroidismo',
                        'nombre' => 'Hipotiroidismo',
                    ],
                    [
                        'codigo' => 'enfer_rinitis',
                        'nombre' => 'Rinitis alérgica',
                    ],
                    [
                        'codigo' => 'enfer_asma',
                        'nombre' => 'Asma bronquial',
                    ],
                    [
                        'codigo' => 'enfer_migraña',
                        'nombre' => 'Migraña',
                    ],
                    [
                        'codigo' => 'enfer_epilepsia',
                        'nombre' => 'Epilepsia',
                    ],
                    [
                        'codigo' => 'enfer_parkinson',
                        'nombre' => 'Parkinson',
                    ],
                    [
                        'codigo' => 'enfer_alzheimer',
                        'nombre' => 'Alzheimer',
                    ],
                    [
                        'codigo' => 'enfer_depresion',
                        'nombre' => 'Depresión',
                    ],
                    [
                        'codigo' => 'enfer_gastritis',
                        'nombre' => 'Gastritis',
                    ],
                    [
                        'codigo' => 'enfer_ulcera',
                        'nombre' => 'Úlcera péptica',
                    ],
                    [
                        'codigo' => 'enfer_colitis',
                        'nombre' => 'Colitis nerviosa',
                    ],
                    [
                        'codigo' => 'enfer_hemorroides',
                        'nombre' => 'Hemorroides',
                    ],
                    [
                        'codigo' => 'enfer_anemia',
                        'nombre' => 'Anemia',
                    ],
                    [
                        'codigo' => 'enfer_vih',
                        'nombre' => 'VIH+ o SIDA',
                    ],
                    [
                        'codigo' => 'enfer_cancer',
                        'nombre' => 'Cáncer',
                    ],
                    [
                        'codigo' => 'enfer_vitigilo',
                        'nombre' => 'Vitigilo',
                    ],
                    [
                        'codigo' => 'enfer_dermatitis',
                        'nombre' => 'Dermatitis atópica',
                    ],
                    [
                        'codigo' => 'enfer_artritis',
                        'nombre' => 'Artritis reumatoide',
                    ],
                    [
                        'codigo' => 'enfer_osteoartritis',
                        'nombre' => 'Osteoartritis',
                    ],
                    [
                        'codigo' => 'enfer_osteoporosis',
                        'nombre' => 'Osteoporosis',
                    ],
                    [
                        'codigo' => 'enfer_hiperuricemia',
                        'nombre' => 'Hiperuricemia',
                    ],
                    [
                        'codigo' => 'enfer_mastopatia',
                        'nombre' => 'Mastopatia fibroquistica',
                    ],
                    [
                        'codigo' => 'enfer_quistes',
                        'nombre' => 'Quistes ováricos',
                    ],
                    [
                        'codigo' => 'enfer_urinaria',
                        'nombre' => 'Infección urinaria',
                    ],
                    [
                        'codigo' => 'enfer_calculos',
                        'nombre' => 'Cálculos',
                    ],
                    [
                        'codigo' => 'enfer_otros',
                        'nombre' => 'Otros',
                    ],
                ]
            ],
            [
                'tipo' => [
                    'codigo' => 'historial_enfermedades',
                    'nombre' => 'Historial de enfermedades',
                ],
                'items' => [
                    [
                        'codigo' => 'hist_enfer_sarampion',
                        'nombre' => 'Sarampión',
                    ],
                    [
                        'codigo' => 'hist_enfer_varicela',
                        'nombre' => 'Varicela',
                    ],
                    [
                        'codigo' => 'hist_enfer_fiebre',
                        'nombre' => 'Fiebre reumática',
                    ],
                    [
                        'codigo' => 'hist_enfer_hepatitis',
                        'nombre' => 'Hepatitis',
                    ],
                    [
                        'codigo' => 'hist_enfer_infarto',
                        'nombre' => 'Infarto al Miocardio',
                    ],
                    [
                        'codigo' => 'hist_enfer_cancer',
                        'nombre' => 'Cáncer',
                    ],
                    [
                        'codigo' => 'hist_enfer_tuberculosis',
                        'nombre' => 'Tuberculosis',
                    ],
                    [
                        'codigo' => 'hist_enfer_sifilis',
                        'nombre' => 'Sífilis',
                    ],
                    [
                        'codigo' => 'hist_enfer_ulcera',
                        'nombre' => 'Ulcera péptica',
                    ],
                    [
                        'codigo' => 'hist_enfer_anemia',
                        'nombre' => 'Anemia',
                    ],
                    [
                        'codigo' => 'hist_enfer_otra',
                        'nombre' => 'Otra',
                    ],
                ]
            ],
            [
                'tipo' => [
                    'codigo' => 'historial_enfermedades_familiares',
                    'nombre' => 'Historial de enfermedades familiares',
                ],
                'items' => [
                    [
                        'codigo' => 'hist_enfer_fam_hiper',
                        'nombre' => 'Hipertensión',
                    ],
                    [
                        'codigo' => 'hist_enfer_fam_diabetes',
                        'nombre' => 'Diabetes Mellitus',
                    ],
                    [
                        'codigo' => 'hist_enfer_fam_infarto',
                        'nombre' => 'Infarto al Miocardio',
                    ],
                    [
                        'codigo' => 'hist_enfer_fam_embolia',
                        'nombre' => 'Embolia cerebral',
                    ],
                    [
                        'codigo' => 'hist_enfer_fam_esquizofrenia',
                        'nombre' => 'Esquizofrenia',
                    ],
                    [
                        'codigo' => 'hist_enfer_fam_migraña',
                        'nombre' => 'Migraña',
                    ],
                    [
                        'codigo' => 'hist_enfer_fam_alcoholismo',
                        'nombre' => 'Alcoholismo',
                    ],
                    [
                        'codigo' => 'hist_enfer_fam_adiccion',
                        'nombre' => 'Adicción a drogas',
                    ],
                    [
                        'codigo' => 'hist_enfer_fam_cancer',
                        'nombre' => 'Cáncer',
                    ],
                    [
                        'codigo' => 'hist_enfer_fam_asma',
                        'nombre' => 'Asma',
                    ],
                    [
                        'codigo' => 'hist_enfer_fam_artritis',
                        'nombre' => 'Artritis reumatoide',
                    ],
                    [
                        'codigo' => 'hist_enfer_fam_hipertiro',
                        'nombre' => 'Hipertiroidismo',
                    ],
                    [
                        'codigo' => 'hist_enfer_fam_hipotiro',
                        'nombre' => 'Hipotiroidismo',
                    ],
                    [
                        'codigo' => 'hist_enfer_fam_obesidad',
                        'nombre' => 'Obesidad',
                    ],
                    [
                        'codigo' => 'hist_enfer_fam_vih',
                        'nombre' => 'VIH+ o SIDA',
                    ],
                    [
                        'codigo' => 'hist_enfer_fam_malfor',
                        'nombre' => 'Malformación congénita',
                    ],
                    [
                        'codigo' => 'hist_enfer_fam_otra',
                        'nombre' => 'Otra',
                    ],
                ]
            ],
            [
                'tipo' => [
                    'codigo' => 'lesiones_tejidos',
                    'nombre' => 'Lesión en tejido blando',
                ],
                'items' => [
                    [
                        'codigo' => 'lesion_tej_blan_si',
                        'nombre' => 'Sí',
                    ],
                    [
                        'codigo' => 'lesion_tej_blan_no',
                        'nombre' => 'No',
                    ],
                    [
                        'codigo' => 'lesion_tej_blan_exudado',
                        'nombre' => 'Exudado',
                    ],
                    [
                        'codigo' => 'lesion_tej_blan_sangrado',
                        'nombre' => 'Sangramiento',
                    ],
                ]
            ],
            [
                'tipo' => [
                    'codigo' => 'lesiones_tejido_consistencia',
                    'nombre' => 'Consitencia de la lesión de tejido blando',
                ],
                'items' => [
                    [
                        'codigo' => 'lesion_tej_blan_blanda',
                        'nombre' => 'Blanda',
                    ],
                    [
                        'codigo' => 'lesion_tej_blan_firme',
                        'nombre' => 'Firme',
                    ],
                    [
                        'codigo' => 'lesion_tej_blan_dura',
                        'nombre' => 'Dura',
                    ],
                ]
            ],
            [
                'tipo' => [
                    'codigo' => 'lesiones_tejido_tipo',
                    'nombre' => 'Tipo de la lesión de tejido blando',
                ],
                'items' => [
                    [
                        'codigo' => 'lesion_tej_blan_ulcera',
                        'nombre' => 'Úlcera',
                    ],
                    [
                        'codigo' => 'lesion_tej_blan_mancha',
                        'nombre' => 'Mancha',
                    ],
                    [
                        'codigo' => 'lesion_tej_blan_vesicula',
                        'nombre' => 'Vesícula',
                    ],
                    [
                        'codigo' => 'lesion_tej_blan_fisura',
                        'nombre' => 'Fisura',
                    ],
                ]
            ],
            [
                'tipo' => [
                    'codigo' => 'medicamentos',
                    'nombre' => 'Medicamentos',
                ],
                'items' => [
                    [
                        'codigo' => 'medica_aine',
                        'nombre' => 'AINE',
                    ],
                    [
                        'codigo' => 'medica_acido',
                        'nombre' => 'Antiácido',
                    ],
                    [
                        'codigo' => 'medica_arritmico',
                        'nombre' => 'Antiarrítmico',
                    ],
                    [
                        'codigo' => 'medica_biotico',
                        'nombre' => 'Antibiótico',
                    ],
                    [
                        'codigo' => 'medica_coagulante',
                        'nombre' => 'Anticoagulante',
                    ],
                    [
                        'codigo' => 'medica_conceptivo',
                        'nombre' => 'Anticonceptivo',
                    ],
                    [
                        'codigo' => 'medica_convulsivo',
                        'nombre' => 'Anticonvulsivo',
                    ],
                    [
                        'codigo' => 'medica_depresivo',
                        'nombre' => 'Antidepresivo',
                    ],
                    [
                        'codigo' => 'medica_diarreico',
                        'nombre' => 'Antidiarreico',
                    ],
                    [
                        'codigo' => 'medica_hipertensivo',
                        'nombre' => 'Antihipertensivo',
                    ],
                    [
                        'codigo' => 'medica_histaminico',
                        'nombre' => 'Antihistamínico',
                    ],
                    [
                        'codigo' => 'medica_migrañoso',
                        'nombre' => 'Antimigrañoso',
                    ],
                    [
                        'codigo' => 'medica_neoplasico',
                        'nombre' => 'Antineoplásico',
                    ],
                    [
                        'codigo' => 'medica_parkinsonimso',
                        'nombre' => 'Antiparkinsonimso',
                    ],
                    [
                        'codigo' => 'medica_psicotico',
                        'nombre' => 'Antipsicótico',
                    ],
                    [
                        'codigo' => 'medica_tiroideo',
                        'nombre' => 'Antitiroideo',
                    ],
                    [
                        'codigo' => 'medica_bronco',
                        'nombre' => 'Broncodilatador',
                    ],
                    [
                        'codigo' => 'medica_cortico',
                        'nombre' => 'Corticoesteroide',
                    ],
                    [
                        'codigo' => 'medica_digitalico',
                        'nombre' => 'Digitálico',
                    ],
                    [
                        'codigo' => 'medica_hipnotico',
                        'nombre' => 'Hipnótico',
                    ],
                    [
                        'codigo' => 'medica_gluceminante',
                        'nombre' => 'Hipogluceminante',
                    ],
                    [
                        'codigo' => 'medica_hormona',
                        'nombre' => 'Hormona tiroidea',
                    ],
                    [
                        'codigo' => 'medica_otro',
                        'nombre' => 'Otro',
                    ],
                ]
            ],
            [
                'tipo' => [
                    'codigo' => 'molestias',
                    'nombre' => 'Molestias actuales',
                ],
                'items' => [
                    [
                        'codigo' => 'moles_poliuria',
                        'nombre' => 'Poliuria',
                    ],
                    [
                        'codigo' => 'moles_polidipsia',
                        'nombre' => 'Polidipsia',
                    ],
                    [
                        'codigo' => 'moles_polifagia',
                        'nombre' => 'Polifagia',
                    ],
                    [
                        'codigo' => 'moles_peso',
                        'nombre' => 'Pérdida de peso',
                    ],
                    [
                        'codigo' => 'moles_sequedad',
                        'nombre' => 'Sequedad bucal',
                    ],
                    [
                        'codigo' => 'moles_nicturia',
                        'nombre' => 'Nicturia',
                    ],
                    [
                        'codigo' => 'moles_disnea',
                        'nombre' => 'Disnea',
                    ],
                    [
                        'codigo' => 'moles_tos',
                        'nombre' => 'Tos',
                    ],
                    [
                        'codigo' => 'moles_expectoracion',
                        'nombre' => 'Expectoración',
                    ],
                    [
                        'codigo' => 'moles_rinorrea',
                        'nombre' => 'Rinorrea',
                    ],
                    [
                        'codigo' => 'moles_precordial',
                        'nombre' => 'Dolor precordial',
                    ],
                    [
                        'codigo' => 'moles_palpitaciones',
                        'nombre' => 'Palpitaciones',
                    ],
                    [
                        'codigo' => 'moles_edema',
                        'nombre' => 'Edema',
                    ],
                    [
                        'codigo' => 'moles_mareo',
                        'nombre' => 'Mareo o vértigo',
                    ],
                    [
                        'codigo' => 'moles_fatiga',
                        'nombre' => 'Fatiga',
                    ],
                    [
                        'codigo' => 'moles_desmayos',
                        'nombre' => 'Desmayos',
                    ],
                    [
                        'codigo' => 'moles_fiebre',
                        'nombre' => 'Fiebre',
                    ],
                    [
                        'codigo' => 'moles_diaforesis',
                        'nombre' => 'Diaforesis',
                    ],
                    [
                        'codigo' => 'moles_intolerancia',
                        'nombre' => 'Intolerancia al frio o calor',
                    ],
                    [
                        'codigo' => 'moles_piel',
                        'nombre' => 'Sequedad piel',
                    ],
                    [
                        'codigo' => 'moles_cefalea',
                        'nombre' => 'Cefalea',
                    ],
                    [
                        'codigo' => 'moles_otalgia',
                        'nombre' => 'Otalgia',
                    ],
                    [
                        'codigo' => 'moles_parestesia',
                        'nombre' => 'Parestesia',
                    ],
                    [
                        'codigo' => 'moles_paresia',
                        'nombre' => 'Paresia',
                    ],
                    [
                        'codigo' => 'moles_nerviosismo',
                        'nombre' => 'Nerviosismo',
                    ],
                    [
                        'codigo' => 'moles_temblores',
                        'nombre' => 'Temblores',
                    ],
                    [
                        'codigo' => 'moles_diplopia',
                        'nombre' => 'Diplopia',
                    ],
                    [
                        'codigo' => 'moles_fosfenos',
                        'nombre' => 'Fosfenos',
                    ],
                    [
                        'codigo' => 'moles_escotomas',
                        'nombre' => 'Escotomas',
                    ],
                    [
                        'codigo' => 'moles_hipoacusia',
                        'nombre' => 'Hipoacusia',
                    ],
                    [
                        'codigo' => 'moles_acufenos',
                        'nombre' => 'Acúfenos',
                    ],
                    [
                        'codigo' => 'moles_disgeusia',
                        'nombre' => 'Disgeusia',
                    ],
                    [
                        'codigo' => 'moles_nausea',
                        'nombre' => 'Náusea',
                    ],
                    [
                        'codigo' => 'moles_vomito',
                        'nombre' => 'Vómito',
                    ],
                    [
                        'codigo' => 'moles_hiporexia',
                        'nombre' => 'Hiporexia',
                    ],
                    [
                        'codigo' => 'moles_diarrea',
                        'nombre' => 'Diarrea',
                    ],
                    [
                        'codigo' => 'moles_abdominal',
                        'nombre' => 'Dolor abdominal',
                    ],
                    [
                        'codigo' => 'moles_melena',
                        'nombre' => 'Melena',
                    ],
                    [
                        'codigo' => 'moles_petequias',
                        'nombre' => 'Petequias',
                    ],
                    [
                        'codigo' => 'moles_equimosis',
                        'nombre' => 'Equimosis',
                    ],
                    [
                        'codigo' => 'moles_epistaxis',
                        'nombre' => 'Epistaxis',
                    ],
                    [
                        'codigo' => 'moles_hematuria',
                        'nombre' => 'Hematuria',
                    ],
                    [
                        'codigo' => 'moles_coluria',
                        'nombre' => 'Coluria',
                    ],
                    [
                        'codigo' => 'moles_disuria',
                        'nombre' => 'Disuria',
                    ],
                    [
                        'codigo' => 'moles_prurito',
                        'nombre' => 'Prurito',
                    ],
                    [
                        'codigo' => 'moles_cutanea',
                        'nombre' => 'Erupción cutánea',
                    ],
                    [
                        'codigo' => 'moles_cambios',
                        'nombre' => 'Cambios color de piel',
                    ],
                    [
                        'codigo' => 'moles_artralgias',
                        'nombre' => 'Artralgias',
                    ],
                    [
                        'codigo' => 'moles_otros',
                        'nombre' => 'Otra',
                    ],
                ]
            ],
            [
                'tipo' => [
                    'codigo' => 'en_mujeres',
                    'nombre' => 'En mujeres',
                ],
                'items' => [
                    [
                        'codigo' => 'muje_embarazo',
                        'nombre' => 'Embarazo',
                    ],
                    [
                        'codigo' => 'muje_toxemia',
                        'nombre' => 'Toxemia embarazo',
                    ],
                    [
                        'codigo' => 'muje_disminorrea',
                        'nombre' => 'Disminorrea',
                    ],
                    [
                        'codigo' => 'muje_fur',
                        'nombre' => 'FUR',
                    ],
                    [
                        'codigo' => 'muje_lactancia',
                        'nombre' => 'Lactancia',
                    ],
                    [
                        'codigo' => 'muje_sangrado',
                        'nombre' => 'Sangrado anormal',
                    ],
                    [
                        'codigo' => 'muje_menopausia',
                        'nombre' => 'Menopausia',
                    ],
                    [
                        'codigo' => 'muje_papani',
                        'nombre' => 'Papanicolau',
                    ],
                    [
                        'codigo' => 'muje_menorrea',
                        'nombre' => 'Menorrea',
                    ],
                    [
                        'codigo' => 'muje_sexual',
                        'nombre' => 'Actividad sexual',
                    ],
                ]
            ],
            [
                'tipo' => [
                    'codigo' => 'orientacion_sexual',
                    'nombre' => 'Orientación sexual',
                ],
                'items' => [
                    [
                        'codigo' => 'orienta_hetero',
                        'nombre' => 'Heterosexual',
                    ],
                    [
                        'codigo' => 'orienta_homo',
                        'nombre' => 'Homosexual',
                    ],
                    [
                        'codigo' => 'orienta_bi',
                        'nombre' => 'Bisexual',
                    ],
                ]
            ],
            [
                'tipo' => [
                    'codigo' => 'padecimiento_bucal',
                    'nombre' => 'Padecimientos Bucales',
                ],
                'items' => [
                    [
                        'codigo' => 'pad_buc_revision',
                        'nombre' => 'Revisión',
                    ],
                    [
                        'codigo' => 'pad_buc_rehabilitacion',
                        'nombre' => 'Rehabilitación',
                    ],
                    [
                        'codigo' => 'pad_buc_estetica',
                        'nombre' => 'Estética dental',
                    ],
                    [
                        'codigo' => 'pad_buc_movilidad',
                        'nombre' => 'Movilidad dental',
                    ],
                    [
                        'codigo' => 'pad_buc_perdida',
                        'nombre' => 'Pérdida de dientes',
                    ],
                    [
                        'codigo' => 'pad_buc_maloclusion',
                        'nombre' => 'Dientes en maloclusión',
                    ],
                    [
                        'codigo' => 'pad_buc_caries',
                        'nombre' => 'Caries',
                    ],
                    [
                        'codigo' => 'pad_buc_dolor_dental',
                        'nombre' => 'Dolor dental',
                    ],
                    [
                        'codigo' => 'pad_buc_dolor_ATM',
                        'nombre' => 'Dolor de ATM',
                    ],
                    [
                        'codigo' => 'pad_buc_trismus',
                        'nombre' => 'Trismus',
                    ],
                    [
                        'codigo' => 'pad_buc_orofacial',
                        'nombre' => 'Dolor orofacial',
                    ],
                    [
                        'codigo' => 'pad_buc_dentoalveolar',
                        'nombre' => 'Trauma dentoalveolar',
                    ],
                    [
                        'codigo' => 'pad_buc_sangrado',
                        'nombre' => 'Sangrado de encía',
                    ],
                    [
                        'codigo' => 'pad_buc_agrandamiento',
                        'nombre' => 'Agrandamiento',
                    ],
                    [
                        'codigo' => 'pad_buc_halitosis',
                        'nombre' => 'Halitosis',
                    ],
                    [
                        'codigo' => 'pad_buc_limpieza',
                        'nombre' => 'Limpieza dental',
                    ],
                    [
                        'codigo' => 'pad_buc_tratamientos',
                        'nombre' => 'Continuar tratamientos',
                    ],
                    [
                        'codigo' => 'pad_buc_otros',
                        'nombre' => 'Otros',
                    ],
                ]
            ],
            [
                'tipo' => [
                    'codigo' => 'referencia',
                    'nombre' => 'Tipo de referencia',
                ],
                'items' => [
                    [
                        'codigo' => 'ref_priv',
                        'nombre' => 'Privada',
                    ],
                    [
                        'codigo' => 'ref_udes',
                        'nombre' => 'UDES',
                    ],
                    [
                        'codigo' => 'ref_hosp',
                        'nombre' => 'Hospital',
                    ],
                    [
                        'codigo' => 'ref_sani',
                        'nombre' => 'Sanidad Militar',
                    ],
                    [
                        'codigo' => 'ref_pnc',
                        'nombre' => 'PNC',
                    ],
                    [
                        'codigo' => 'ref_otros',
                        'nombre' => 'Otros',
                    ],
                ]
            ],
            [
                'tipo' => [
                    'codigo' => 'sustancias',
                    'nombre' => 'Historial del consumo de sustancias',
                ],
                'items' => [
                    [
                        'codigo' => 'sustan_alcohol',
                        'nombre' => 'Alcohol',
                    ],
                    [
                        'codigo' => 'sustan_tabaco',
                        'nombre' => 'Tabaco',
                    ],
                    [
                        'codigo' => 'sustan_marihuana',
                        'nombre' => 'Marihuana',
                    ],
                    [
                        'codigo' => 'sustan_cocaina',
                        'nombre' => 'Cocaína',
                    ],
                    [
                        'codigo' => 'sustan_anfetaminas',
                        'nombre' => 'Anfetaminas',
                    ],
                    [
                        'codigo' => 'sustan_otra',
                        'nombre' => 'Otra',
                    ],
                ]
            ],
            [
                'tipo' => [
                    'codigo' => 'transfuciones',
                    'nombre' => 'Historial de transfuciones sanguíneas',
                ],
                'items' => [
                    [
                        'codigo' => 'transfuciones_si',
                        'nombre' => 'Sí',
                    ],
                    [
                        'codigo' => 'transfuciones_no',
                        'nombre' => 'No',
                    ],
                ]
            ],
            [
                'tipo' => [
                    'codigo' => 'traumatismos',
                    'nombre' => 'Historial de operaciones o traumatismos',
                ],
                'items' => [
                    [
                        'codigo' => 'trauma_extracc',
                        'nombre' => 'Extracción dental',
                    ],
                    [
                        'codigo' => 'trauma_amigda',
                        'nombre' => 'Amigdaloctemía',
                    ],
                    [
                        'codigo' => 'trauma_apendi',
                        'nombre' => 'Apendicectomia',
                    ],
                    [
                        'codigo' => 'trauma_cesar',
                        'nombre' => 'Cesárea',
                    ],
                    [
                        'codigo' => 'trauma_histere',
                        'nombre' => 'Histerectomía',
                    ],
                    [
                        'codigo' => 'trauma_salpin',
                        'nombre' => 'Salpingoclasia',
                    ],
                    [
                        'codigo' => 'trauma_colecis',
                        'nombre' => 'Colecistectomia',
                    ],
                    [
                        'codigo' => 'trauma_fracturas',
                        'nombre' => 'Fracturas',
                    ],
                    [
                        'codigo' => 'trauma_quema',
                        'nombre' => 'Quemaduras',
                    ],
                    [
                        'codigo' => 'trauma_herida',
                        'nombre' => 'Heridas de bala o punzantes',
                    ],
                    [
                        'codigo' => 'trauma_otro',
                        'nombre' => 'Otros',
                    ],
                ]
            ],
            [
                'tipo' => [
                    'codigo' => 'vivienda_material',
                    'nombre' => 'Material de la vivienda',
                ],
                'items' => [
                    [
                        'codigo' => 'vivi_mat_concreto',
                        'nombre' => 'Concreto',
                    ],
                    [
                        'codigo' => 'vivi_mat_adobe',
                        'nombre' => 'Adobe',
                    ],
                    [
                        'codigo' => 'vivi_mat_madera',
                        'nombre' => 'Madera',
                    ],
                    [
                        'codigo' => 'vivi_mat_lamina',
                        'nombre' => 'Lámina',
                    ],
                    [
                        'codigo' => 'vivi_mat_otro',
                        'nombre' => 'Otro',
                    ],
                ]
            ],
            [
                'tipo' => [
                    'codigo' => 'vivienda_servicios',
                    'nombre' => 'Servicios en la vivienda',
                ],
                'items' => [
                    [
                        'codigo' => 'vivi_ser_agua',
                        'nombre' => 'Agua',
                    ],
                    [
                        'codigo' => 'vivi_ser_drenaje',
                        'nombre' => 'Drenaje',
                    ],
                    [
                        'codigo' => 'vivi_ser_ventilacion',
                        'nombre' => 'Ventilación',
                    ],
                ]
            ],
        ];


        foreach ($catalogos as $catalogo) {
            $_tipo = $catalogo['tipo'];

            $tipo = new FichaCatalogo();
            $tipo->setCodigo($_tipo['codigo']);
            $tipo->setNombre($_tipo['nombre']);
            if (array_key_exists('descripcion', $_tipo)) $tipo->setDescripcion($_tipo['descripcion']);

            $manager->persist($tipo);

            foreach ($catalogo['items'] as $_item) {
                $item = new FichaCatalogo();
                $item->setFichaCatalogoTipo($tipo);
                $item->setCodigo($_item['codigo']);
                $item->setNombre($_item['nombre']);
                if (array_key_exists('descripcion', $_item)) $item->setDescripcion($_item['descripcion']);

                $manager->persist($item);
            }
        }

        $manager->flush();

    }

    /**
     * @return array
     */
    public static function getGroups(): array
    {
        return ['dev', 'prod'];
    }

}
