<?php

namespace App\DataFixtures;

use App\Entity\Academico\Periodo;
use App\Entity\Academico\PeriodoCiclo;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\Persistence\ObjectManager;

class PeriodoFixtures extends Fixture implements FixtureGroupInterface
{
    public function load(ObjectManager $manager)
    {
        $periodos = [
            [
                'nombre' => 'Ciclo I 2017',
                'inicio' => '2017-02-20',
                'fin' => '2017-07-31',
                'activo' => false,
                'tipo' => 'Impar',
            ],
            [
                'nombre' => 'Ciclo P 2017',
                'inicio' => '2017-08-01',
                'fin' => '2017-12-15',
                'activo' => false,
                'tipo' => 'Par',
            ],
            [
                'nombre' => 'Ciclo I 2018',
                'inicio' => '2018-02-19',
                'fin' => '2018-07-31',
                'activo' => false,
                'tipo' => 'Impar',
            ],
            [
                'nombre' => 'Ciclo P 2018',
                'inicio' => '2018-08-01',
                'fin' => '2018-12-14',
                'activo' => true,
                'tipo' => 'Par',
            ],
            [
                'nombre' => 'Ciclo I 2019',
                'inicio' => '2019-02-18',
                'fin' => '2019-07-31',
                'activo' => false,
                'tipo' => 'Impar',
            ],
            [
                'nombre' => 'Ciclo P 2019',
                'inicio' => '2019-08-01',
                'fin' => '2019-12-13',
                'activo' => false,
                'tipo' => 'Par',
            ],
        ];

        foreach ($periodos as $_periodo) {
            $periodo = (new Periodo())
                ->setNombre($_periodo['nombre'])
                ->setInicio(\DateTime::createFromFormat('Y-m-d', $_periodo['inicio']))
                ->setFin(\DateTime::createFromFormat('Y-m-d', $_periodo['fin']))
                ->setActivo($_periodo['activo']);
            $manager->persist($periodo);
            $ciclo = (new PeriodoCiclo())
                ->setPeriodo($periodo)
                ->setTipo($_periodo['tipo']);
            $manager->persist($ciclo);
        }

        $manager->flush();
    }

    /**
     * @return array
     */
    public static function getGroups(): array
    {
        return ['dev', 'prod'];
    }

}
