<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class ExpedienteFixtures extends Fixture implements DependentFixtureInterface, FixtureGroupInterface
{
    /**
     * @param ObjectManager $manager
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function load(ObjectManager $manager)
    {
        /** @var \Doctrine\ORM\EntityManager $manager */
        /** @var \Doctrine\DBAL\Connection $conn */
        $conn = $manager->getConnection();
        $qb = new \Doctrine\DBAL\Query\QueryBuilder($conn);

        $qb->insert('administracion.cuenta')
            ->values(['id' => 1]);
        $qb->execute();
        $qb->resetQueryParts();

        $qb->insert('administracion.persona')
            ->values([
                'id' => 1,
                'id_cuenta' => 1,
                'nombre' => "'Indefinido'",
                'apellido' => "'Indefinido'",
            ]);
        $qb->execute();
        $qb->resetQueryParts();

        $expediente_clinico_tipo = $manager->getRepository('Clinico:FichaCatalogo')
            ->findOneBy(['codigo' => 'expediente_extramural']);

        $qb->insert('clinico.expediente_clinico')
            ->values([
                'id' => 1,
                'id_persona' => 1,
                'id_expediente_clinico_tipo' => $expediente_clinico_tipo->getId(),
                'codigo_expediente' => 1,
            ]);
        $qb->execute();
        $qb->resetQueryParts();

        $fecuencia_cepillado = $manager->getRepository('Clinico:FichaCatalogo')
            ->findOneBy(['codigo' => 'frec_cepi_0']);

        $fecha = date('Y-m-d');
        $qb->insert('clinico.ficha')
            ->values([
                'id' => 1,
                'id_expediente_clinico' => 1,
                'id_cuenta' => 1,
                'id_frecuencia_cepillado' => $fecuencia_cepillado->getId(),
                'fecha_control' => "'$fecha'",
                'visita_odontologo' => 0,
                'trauma_dentoalveolar' => 0,
                'reacciones_adversas' => 0,
                'motivo_consulta' => "'Indefinido'",
                'historia_enfermedad' => "'Indefinido'",
                'creado' => "'$fecha'",
                'actualizado' => "'$fecha'",
            ]);
        $qb->execute();
        $qb->resetQueryParts();

        $manager->flush();
    }

    /**
     * @return array
     */
    public function getDependencies()
    {
        return [
            TratamientoFixtures::class,
        ];
    }

    /**
     * @return array
     */
    public static function getGroups(): array
    {
        return ['dev', 'prod'];
    }

}
