<?php

namespace App\DataFixtures;

use App\Entity\Almacen\ValeEstado;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\Persistence\ObjectManager;

class ValeEstadoFixtures extends Fixture implements FixtureGroupInterface
{
    public function load(ObjectManager $manager)
    {
        $vales_estados = [
            [
                'codigo' => 'pendiente',
                'nombre' => 'Pendiente',
                'descripcion' => 'Vales en espera de revisión para su respectiva aprobación o denegación',
            ],
            [
                'codigo' => 'aprobado',
                'nombre' => 'Aprobado',
                'descripcion' => 'Vales que han sido revisados y aprobados para la entrega de insumos',
            ],
            [
                'codigo' => 'denegado',
                'nombre' => 'Denegado',
                'descripcion' => 'Vales que han sido revisados y se les deniega la entrega de insumos.',
            ],
        ];

        foreach ($vales_estados as $_vale_estado) {
            $vale_estado = (new ValeEstado())
                ->setCodigo($_vale_estado['codigo'])
                ->setNombre($_vale_estado['nombre'])
                ->setDescripcion($_vale_estado['descripcion']);
            $manager->persist($vale_estado);
        }

        $manager->flush();
    }

    /**
     * @return array
     */
    public static function getGroups(): array
    {
        return ['dev', 'prod'];
    }

}
