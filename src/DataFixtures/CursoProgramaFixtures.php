<?php

namespace App\DataFixtures;

use App\Entity\Academico\Carrera;
use App\Entity\Academico\CursoPrograma;
use App\Entity\Academico\PlanEstudio;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class CursoProgramaFixtures extends Fixture implements DependentFixtureInterface, FixtureGroupInterface
{
    /**
     * @param ObjectManager $manager
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\ORM\ORMException
     */
    public function load(ObjectManager $manager)
    {
        $carrera = $manager->getRepository(Carrera::class)
            ->findOneBy(['codigo' => 'D10701']);
        $plan_estudio = $manager->getRepository(PlanEstudio::class)
            ->findOneBy(['carrera' => $carrera, 'anio' => 2009]);
        $curso_programas = [
            ['curso_codigo' => 'est417', 'programa_codigo' => 'ppe', 'correlativo' => 1],
            ['curso_codigo' => 'opc117', 'programa_codigo' => 'ppe', 'correlativo' => 2],
            ['curso_codigo' => 'opc217', 'programa_codigo' => 'ppe', 'correlativo' => 3],
            ['curso_codigo' => 'opc317', 'programa_codigo' => 'ppe', 'correlativo' => 4],
            ['curso_codigo' => 'opc417', 'programa_codigo' => 'ppe', 'correlativo' => 5],
            ['curso_codigo' => 'opc517', 'programa_codigo' => 'ppe', 'correlativo' => 6],
            ['curso_codigo' => 'opc517', 'programa_codigo' => 'cesa', 'correlativo' => 6],
            ['curso_codigo' => 'opc517', 'programa_codigo' => 'ulc', 'correlativo' => 6],
            ['curso_codigo' => 'opc517', 'programa_codigo' => 'clc', 'correlativo' => 6],
            ['curso_codigo' => 'opc517', 'programa_codigo' => 'csns', 'correlativo' => 6],
            ['curso_codigo' => 'opc517', 'programa_codigo' => 'hnr', 'correlativo' => 6],
            ['curso_codigo' => 'opc617', 'programa_codigo' => 'ppe', 'correlativo' => 7],
            ['curso_codigo' => 'opc617', 'programa_codigo' => 'cesa', 'correlativo' => 7],
            ['curso_codigo' => 'opc617', 'programa_codigo' => 'ulc', 'correlativo' => 7],
            ['curso_codigo' => 'opc617', 'programa_codigo' => 'clc', 'correlativo' => 7],
            ['curso_codigo' => 'opc617', 'programa_codigo' => 'csns', 'correlativo' => 7],
            ['curso_codigo' => 'opc617', 'programa_codigo' => 'hnr', 'correlativo' => 7],
            ['curso_codigo' => 'opc717', 'programa_codigo' => 'ppe', 'correlativo' => 8],
            ['curso_codigo' => 'opc717', 'programa_codigo' => 'cesa', 'correlativo' => 8],
            ['curso_codigo' => 'opc717', 'programa_codigo' => 'ulc', 'correlativo' => 8],
            ['curso_codigo' => 'opc717', 'programa_codigo' => 'clc', 'correlativo' => 8],
            ['curso_codigo' => 'opc717', 'programa_codigo' => 'csns', 'correlativo' => 8],
        ];

        foreach ($curso_programas as $_curso_programa) {
            $manager->getRepository(CursoPrograma::class)
                ->createFromCursoCodigoAndProgramaCodigo(
                    $plan_estudio,
                    $_curso_programa['curso_codigo'],
                    $_curso_programa['programa_codigo'],
                    $_curso_programa['correlativo']
                );
        }

        $manager->flush();
    }

    /**
     * @return array
     */
    public function getDependencies()
    {
        return [
            PlanEstudioFixtures::class,
            ProgramaFixtures::class,
        ];
    }

    /**
     * @return array
     */
    public static function getGroups(): array
    {
        return ['dev', 'prod'];
    }

}
