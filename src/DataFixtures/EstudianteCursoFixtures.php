<?php

namespace App\DataFixtures;

use App\Entity\Academico\Carrera;
use App\Entity\Academico\CursoPrograma;
use App\Entity\Academico\Estudiante;
use App\Entity\Academico\EstudianteCurso;
use App\Entity\Academico\EstudianteEstado;
use App\Entity\Academico\EstudiantePlanEstudio;
use App\Entity\Academico\PlanEstudio;
use App\Service\PeriodoService;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class EstudianteCursoFixtures extends Fixture implements DependentFixtureInterface, FixtureGroupInterface
{
    /**
     * @var PeriodoService
     */
    private $periodo_service;

    /**
     * EstudianteCursoFixtures constructor.
     * @param PeriodoService $periodo_service
     */
    public function __construct(PeriodoService $periodo_service)
    {
        $this->periodo_service = $periodo_service;
    }

    /**
     * @param ObjectManager $manager
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Exception
     */
    public function load(ObjectManager $manager)
    {
        $manager->getConnection()->getConfiguration()->setSQLLogger(null);

        $csv = fopen(__DIR__ . '/Resources/inscripcion.csv', 'r');

        $carrera = $manager->getRepository(Carrera::class)
            ->findOneBy(['codigo' => 'D10701']);
        $plan_estudio = $manager->getRepository(PlanEstudio::class)
            ->findOneBy(['carrera' => $carrera, 'anio' => 2009]);
        $estudiante_estado = $manager->getRepository(EstudianteEstado::class)
            ->findOneBy(['tipo' => 'curso', 'codigo' => 'estudiante']);
        $ciclo = $this->periodo_service->getCicloActual();

        $i = 0;

        while (!feof($csv)) {
            $line = fgetcsv($csv);
            $carnet = $line[0];

            if (isset($carnet)) {
                $estudiante_plan_estudio = $manager->getRepository(EstudiantePlanEstudio::class)
                    ->findOneByCarnetAndPlanEstudio($carnet, $plan_estudio);
                $ciclo_numero = $line[4];
                $cursos = $manager->getRepository(CursoPrograma::class)
                    ->getCursosConProgramasByNumero($plan_estudio, $ciclo_numero);

                foreach ($cursos as $curso) {
                    $estudiante_curso = (new EstudianteCurso())
                        ->setEstudiantePlanEstudio($estudiante_plan_estudio)
                        ->setCurso($curso)
                        ->setPeriodoCiclo($ciclo)
                        ->setEstudianteEstado($estudiante_estado);
                    $manager->persist($estudiante_curso);
                }

                $i = $i + 1;

                if ($i % 100 == 0) {
                    $manager->flush();
                    //$manager->clear();
                    echo 'Inscribiendo 100 estudiantes', PHP_EOL;
                }
            }
        }

        fclose($csv);
        $manager->flush();
    }

    /**
     * @return array
     */
    public function getDependencies()
    {
        return [
            EstudianteFixtures::class,
            PeriodoFixtures::class,
            CursoProgramaFixtures::class,
        ];
    }

    /**
     * @return array
     */
    public static function getGroups(): array
    {
        return ['dev'];
    }

}
