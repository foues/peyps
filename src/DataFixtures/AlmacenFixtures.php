<?php

namespace App\DataFixtures;

use App\Entity\Almacen\Almacen;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class AlmacenFixtures extends Fixture implements DependentFixtureInterface, FixtureGroupInterface
{
    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $cesa = $manager->getRepository('Administracion:Institucion')
            ->findOneBy(['codigo' => 'cesa']);

        $almacen = (new Almacen())
            ->setCodigo('cesa')
            ->setNombre('Almacén de la Clínica Extramural de Santa Ana')
            ->setInstitucion($cesa);


        $manager->persist($almacen);
        $manager->flush();
    }

    /**
     * @return array
     */
    public function getDependencies()
    {
        return [
            InstitucionFixtures::class,
            ArticuloFixtures::class,
        ];
    }

    /**
     * @return array
     */
    public static function getGroups(): array
    {
        return ['dev'];
    }

}
