<?php

namespace App\DataFixtures;

use App\Entity\Almacen\Articulo;
use App\Entity\Almacen\ArticuloPresentacion;
use App\Entity\Almacen\UnidadMedida;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\Persistence\ObjectManager;

class ArticuloFixtures extends Fixture implements FixtureGroupInterface
{
    public function load(ObjectManager $manager)
    {
        $unidades = [];

        $csv = fopen(__DIR__ . '/Resources/articulos.csv', 'r');

        while (!feof($csv)) {
            $line = fgetcsv($csv);
            $articulo_codigo = $line[0];
            $articulo_nombre = $line[1];
            $unidad_nombre = $line[2];
            $subunidad_nombre = $line[3];
            $cantidad = $line[4];
            $marca = $line[5];

            if (isset($articulo_codigo)) {
                $unidad_codigo = strtolower($unidad_nombre);
                $subunidad_codigo = strtolower($subunidad_nombre);

                if (isset($unidades[$unidad_codigo])) {
                    $unidad_medida = $unidades[$unidad_codigo];
                } else {
                    $unidad_medida = (new UnidadMedida())
                        ->setCodigo($unidad_codigo)
                        ->setNombre($unidad_nombre);
                    $manager->persist($unidad_medida);
                    $unidades[$unidad_codigo] = $unidad_medida;
                }
                if (isset($unidades[$subunidad_codigo])) {
                    $subunidad_medida = $unidades[$subunidad_codigo];
                } else {
                    $subunidad_medida = (new UnidadMedida())
                        ->setCodigo($subunidad_codigo)
                        ->setNombre($subunidad_nombre);
                    $manager->persist($subunidad_medida);
                    $unidades[$subunidad_codigo] = $subunidad_medida;
                }

                $articulo = (new Articulo())
                    ->setCodigo($articulo_codigo)
                    ->setNombre($articulo_nombre)
                    ->setMarca($marca);

                $manager->persist($articulo);

                $articulo_presentacion = (new ArticuloPresentacion())
                    ->setArticulo($articulo)
                    ->setUnidadMedida($unidad_medida)
                    ->setSubunidadMedida($subunidad_medida)
                    ->setCantidad($cantidad);

                $manager->persist($articulo_presentacion);
            }
        }

        $manager->flush();
    }

    /**
     * @return array
     */
    public static function getGroups(): array
    {
        return ['dev'];
    }

}
