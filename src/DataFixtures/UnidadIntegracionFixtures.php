<?php

namespace App\DataFixtures;

use App\Entity\Academico\Carrera;
use App\Entity\Academico\PlanEstudio;
use App\Entity\Academico\PlanEstudioCurso;
use App\Entity\Academico\UnidadIntegracion;
use App\Entity\Rotacion\Programa;
use App\Service\PeriodoService;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class UnidadIntegracionFixtures extends Fixture implements DependentFixtureInterface, FixtureGroupInterface
{
    /**
     * @var PeriodoService
     */
    private $periodo_service;

    /**
     * RotacionFixture constructor.
     * @param PeriodoService $periodo_service
     */
    public function __construct(PeriodoService $periodo_service)
    {
        $this->periodo_service = $periodo_service;
    }

    /**
     * @param ObjectManager $manager
     * @throws \Exception
     */
    public function load(ObjectManager $manager)
    {
        $ciclo = $this->periodo_service->getCicloActual();

        $carrera = $manager->getRepository(Carrera::class)
            ->findOneBy(['codigo' => 'D10701']);
        $plan_estudio = $manager->getRepository(PlanEstudio::class)
            ->findOneBy(['carrera' => $carrera, 'anio' => 2009]);

        $plan_estudio_curso_repository = $manager->getRepository(PlanEstudioCurso::class);
        $programa_repository = $manager->getRepository(Programa::class);

        $csv = fopen(__DIR__ . '/Resources/unidad_integracion.csv', 'r');

        while (!feof($csv)) {
            $line = fgetcsv($csv);
            $curso_codigo = $line[0];
            $programa_codigo = $line[1];
            $nombre = $line[2];
            $ponderacion = $line[3];

            if (isset($nombre)) {
                $plan_estudio_curso = $plan_estudio_curso_repository
                    ->findOneByPlanEstudioAndCursoCodigo($plan_estudio, $curso_codigo);
                $programa = $programa_repository->findOneBy(['codigo' => $programa_codigo]);

                $unidad_integracion = (new UnidadIntegracion())
                    ->setPlanEstudioCurso($plan_estudio_curso)
                    ->setPrograma($programa)
                    ->setPeriodoCiclo($ciclo)
                    ->setNombre($nombre)
                    ->setPonderacion($ponderacion);

                $manager->persist($unidad_integracion);
            }
        }

        $manager->flush();
    }

    /**
     * @return array
     */
    public function getDependencies()
    {
        return [
            PlanEstudioFixtures::class,
            ProgramaFixtures::class,
        ];
    }

    /**
     * @return array
     */
    public static function getGroups(): array
    {
        return ['dev'];
    }

}
