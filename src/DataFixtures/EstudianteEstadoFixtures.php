<?php

namespace App\DataFixtures;

use App\Entity\Academico\EstudianteEstado;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\Persistence\ObjectManager;

class EstudianteEstadoFixtures extends Fixture implements FixtureGroupInterface
{
    public function load(ObjectManager $manager)
    {
        $estudiante_estados = [
            ['tipo' => 'plan_estudio', 'codigo' => 'activo', 'nombre' => 'Activo'],
            ['tipo' => 'plan_estudio', 'codigo' => 'inactivo', 'nombre' => 'Inactivo'],
            ['tipo' => 'plan_estudio', 'codigo' => 'egresado', 'nombre' => 'Egresado'],
            ['tipo' => 'plan_estudio', 'codigo' => 'titulado', 'nombre' => 'Titulado'],
            ['tipo' => 'plan_estudio', 'codigo' => 'reserva', 'nombre' => 'Reserva de matrícula'],
            ['tipo' => 'plan_estudio', 'codigo' => 'abandono', 'nombre' => 'Abandono'],
            ['tipo' => 'plan_estudio', 'codigo' => 'reingreso', 'nombre' => 'Reingreso'],
            ['tipo' => 'plan_estudio', 'codigo' => 'cambio', 'nombre' => 'Cambio de carrera'],
            ['tipo' => 'curso', 'codigo' => 'estudiante', 'nombre' => 'Estudiante'],
            ['tipo' => 'curso', 'codigo' => 'retirado', 'nombre' => 'Retirado'],
            ['tipo' => 'curso', 'codigo' => 'aprobado', 'nombre' => 'Aprobado'],
            ['tipo' => 'curso', 'codigo' => 'reprobado', 'nombre' => 'Reprobado'],
            ['tipo' => 'curso', 'codigo' => 'resolución', 'nombre' => 'En resolución'],
            ['tipo' => 'curso', 'codigo' => 'anulada', 'nombre' => 'Inscripcion anulada'],
        ];

        foreach ($estudiante_estados as $_estudiante_estado) {
            $estudiante_estado = (new EstudianteEstado())
                ->setTipo($_estudiante_estado['tipo'])
                ->setCodigo($_estudiante_estado['codigo'])
                ->setNombre($_estudiante_estado['nombre']);

            $manager->persist($estudiante_estado);
        }

        $manager->flush();
    }

    /**
     * @return array
     */
    public static function getGroups(): array
    {
        return ['dev', 'prod'];
    }

}
