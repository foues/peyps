<?php

namespace App\DataFixtures;

use App\Entity\Academico\Penalizacion;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\Persistence\ObjectManager;

class PenalizacionFixtures extends Fixture implements FixtureGroupInterface
{
    public function load(ObjectManager $manager)
    {
        $penalizaciones = [
            ['codigo' => 'impuntualidad', 'nombre' => 'Impuntualidad', 'descripcion' => '', 'puntos' => 2],
            ['codigo' => 'incumplimiento_tarea', 'nombre' => 'Incumplimiento de tarea asignada', 'descripcion' => '', 'puntos' => 2],
            ['codigo' => 'indisposicion', 'nombre' => 'Indisposicion al trabajo', 'descripcion' => '', 'puntos' => 2],
            ['codigo' => 'desmotivacion', 'nombre' => 'Desmotivación', 'descripcion' => '', 'puntos' => 2],
            ['codigo' => 'desatencion_indicaciones', 'nombre' => 'Desatención a indicadores', 'descripcion' => '', 'puntos' => 2],
            ['codigo' => 'inasistencia', 'nombre' => 'Inasistencia', 'descripcion' => 'Restará la totalidad de la calificación. 
            La calificación solo puede tener un valor minimo de 0.0', 'puntos' => 10],
        ];

        foreach ($penalizaciones as $_penalizacion) {
            $penalizacion = (new Penalizacion())
                ->setCodigo($_penalizacion['codigo'])
                ->setNombre($_penalizacion['nombre'])
                ->setDescripcion($_penalizacion['descripcion'])
                ->setPuntos($_penalizacion['puntos']);
            $manager->persist($penalizacion);
        }

        $manager->flush();
    }

    /**
     * @return array
     */
    public static function getGroups(): array
    {
        return ['dev', 'prod'];
    }

}
