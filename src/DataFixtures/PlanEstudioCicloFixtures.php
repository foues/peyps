<?php

namespace App\DataFixtures;

use App\Entity\Academico\PlanEstudioCiclo;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Common\Persistence\ObjectManager;

class PlanEstudioCicloFixtures extends Fixture implements FixtureGroupInterface
{
    public function load(ObjectManager $manager)
    {
        $plan_estudio_ciclos = [
            ['numero' => 1, 'romano' => 'I'],
            ['numero' => 2, 'romano' => 'II'],
            ['numero' => 3, 'romano' => 'III'],
            ['numero' => 4, 'romano' => 'IV'],
            ['numero' => 5, 'romano' => 'V'],
            ['numero' => 6, 'romano' => 'VI'],
            ['numero' => 7, 'romano' => 'VII'],
            ['numero' => 8, 'romano' => 'VIII'],
            ['numero' => 9, 'romano' => 'IX'],
            ['numero' => 10, 'romano' => 'X'],
            ['numero' => 11, 'romano' => 'XI'],
            ['numero' => 12, 'romano' => 'XII'],
            ['numero' => 13, 'romano' => 'XIII'],
            ['numero' => 14, 'romano' => 'XIV'],
        ];

        foreach ($plan_estudio_ciclos as $_plan_estudio_ciclo) {
            $plan_estudio_ciclo = (new PlanEstudioCiclo())
                ->setNumero($_plan_estudio_ciclo['numero'])
                ->setRomano($_plan_estudio_ciclo['romano']);

            $manager->persist($plan_estudio_ciclo);
        }

        $manager->flush();
    }

    /**
     * @return array
     */
    public static function getGroups(): array
    {
        return ['dev', 'prod'];
    }

}
