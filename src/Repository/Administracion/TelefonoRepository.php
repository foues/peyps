<?php
namespace App\Repository\Administracion;

use App\Entity\Administracion\Telefono;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;


class TelefonoRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Telefono::class);
    }

    public function findOneByCuentaAndTipoTelefono($cuentaId, $codigoTelefono)
    { 
        $telefonos = $this->createQueryBuilder('a')
        ->innerJoin('a.cuenta', 'b')
        ->andWhere('b.id = :cuentaId')
        ->setParameter('cuentaId', $cuentaId)
        ->innerJoin('a.telefono_tipo', 'c')
        ->andWhere('c.codigo = :codigoTel')
        ->setParameter('codigoTel', $codigoTelefono)
        ->getQuery()
        ->execute();

        if(sizeof($telefonos)>0){
            return $telefonos[0];
        } else {
            return NULL;
        }
    }

}
