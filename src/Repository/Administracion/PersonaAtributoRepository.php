<?php
namespace App\Repository\Administracion;

use App\Entity\Administracion\PersonaAtributo;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;


class PersonaAtributoRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, PersonaAtributo::class);
    }

    public function findOneByPersonaAndTipoCatalogo($personaId, $codigoTipo)
    { 
        $atributos = $this->createQueryBuilder('a')
        ->innerJoin('a.persona', 'b')
        ->andWhere('b.id = :personaId')
        ->setParameter('personaId', $personaId)
        ->innerJoin('a.atributo', 'c')
        ->innerJoin('c.catalogo_tipo', 'd')
        ->andWhere('d.codigo = :catalogoCod')
        ->setParameter('catalogoCod', $codigoTipo)
        ->getQuery()
        ->execute();

        if(sizeof($atributos)>0){
            return $atributos[0];
        } else {
            return NULL;
        }
    }

}
