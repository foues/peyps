<?php

namespace App\Repository\Administracion;

use App\Entity\Administracion\Menu;
use App\Entity\Administracion\Usuario;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\ResultSetMappingBuilder;
use Symfony\Bridge\Doctrine\RegistryInterface;


class MenuRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Menu::class);
    }

    /**
     * @param Usuario $usuario
     * @return array
     */
    public function findByUsuarioScalar(Usuario $usuario)
    {
        $em = $this->getEntityManager();

        $sql = "
            select distinct m.* from administracion.menu m
            inner join administracion.menu_permiso mp on m.id = mp.id_menu
            inner join administracion.permiso p on mp.id_permiso = p.id
            inner join administracion.rol_permiso rp on p.id = rp.id_permiso
            inner join administracion.rol r on rp.id_rol = r.id
            inner join administracion.usuario_rol ur on r.id = ur.id_rol
            inner join administracion.usuario u on ur.id_usuario = u.id
            where u.id = ? and m.enabled = ?
            order by if(m.id_padre is null, 0, 1), m.id
        ";

        $rsm = new ResultSetMappingBuilder($em);
        $rsm->addScalarResult('id', 'id', 'integer');
        $rsm->addScalarResult('nombre', 'nombre');
        $rsm->addScalarResult('descripcion', 'descripcion');
        $rsm->addScalarResult('url', 'url');
        $rsm->addScalarResult('icon', 'icon');
        $rsm->addScalarResult('enabled', 'enabled', 'boolean');
        $rsm->addScalarResult('id_padre', 'id_padre', 'integer');
        $rsm->addScalarResult('creado', 'creado', 'datetime');

        $query = $em->createNativeQuery($sql, $rsm);

        $query->setParameter(1, $usuario->getId());
        $query->setParameter(2, true);

        $_menus = $query->getResult();

        $menus = [];
        foreach ($_menus as $_menu) {
            $menus[$_menu['id']] = $_menu;
        }

        return $menus;
    }

    /**
     * @param Usuario $usuario
     * @return array
     */
    public function getMenuJerarquicoByUsuario(Usuario $usuario)
    {
        $_menus = $this->findByUsuarioScalar($usuario);
        $menus = [];
        foreach ($_menus as $id => &$_menu) {
            $id_padre = $_menu['id_padre'];
            if ($id_padre) {
                if (isset($_menus[$id_padre])) {
                    $_menus[$id_padre]['submenus'][$id] = &$_menu;
                } else {
                    unset($_menus[$id]);
                }
            } else {
                $menus[$id] = &$_menu;
            }
        }

        return $menus;
    }
}
