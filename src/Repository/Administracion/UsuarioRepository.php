<?php

namespace App\Repository\Administracion;

use App\Entity\Administracion\Usuario;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Symfony\Bridge\Doctrine\Security\User\UserLoaderInterface;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @method Usuario|null find($id, $lockMode = null, $lockVersion = null)
 * @method Usuario|null findOneBy(array $criteria, array $orderBy = null)
 * @method Usuario[]    findAll()
 * @method Usuario[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 * @method Usuario|null findOneByUsername(array $criteria, array $orderBy = null)
 */
class UsuarioRepository extends ServiceEntityRepository implements UserLoaderInterface
{
    /**
     * UsuarioRepository constructor.
     * @param RegistryInterface $registry
     */
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Usuario::class);
    }

    /**
     * {@inheritdoc}
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function loadUserByUsername($username)
    {
        return $this->createQueryBuilder('u')
            ->where('u.username = :username')
            ->setParameter('username', $username)
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * @param Usuario $usuario
     * @return mixed
     */
    public function findRolesDiffByUser(Usuario $usuario, array $roles)
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder()
            ->select('r')
            ->from('Administracion:Rol', 'r')
            ->leftJoin('Administracion:UsuarioRol', 'ur', 'WITH', 'r.id = ur.rol AND ur.usuario = :usuario')
            ->where('r.codigo IN (:roles)')
            ->andWhere('ur.id IS NULL')
            ->setParameters([
                'usuario' => $usuario,
                'roles' => $roles,
            ]);

        return $qb->getQuery()->getResult();
    }

    /**
     * @param UserInterface $usuario
     * @param string $permiso
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findPermissionByUser(UserInterface $usuario, $permiso)
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder()
            ->select('p')
            ->from('Administracion:Permiso', 'p')
            ->innerJoin('Administracion:RolPermiso', 'rp', 'WITH', 'p.id = rp.permiso')
            ->innerJoin('Administracion:Rol', 'r', 'WITH', 'rp.rol = r.id')
            ->innerJoin('Administracion:UsuarioRol', 'ur', 'WITH', 'r.id = ur.rol')
            ->innerJoin('Administracion:Usuario', 'u', 'WITH', 'ur.usuario = u.id')
            ->where('u.id = :usuario')
            ->andWhere('p.codigo = :permiso')
            ->groupBy('p.id')
            ->setMaxResults(1)
            ->setParameters([
                'usuario' => $usuario,
                'permiso' => $permiso,
            ]);

        return $qb->getQuery()->getOneOrNullResult();
    }

    /**
     * @param Usuario $usuario
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findPasswordByUser(Usuario $usuario)
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder()
            ->select('u.password')
            ->from('Administracion:Usuario', 'u')
            ->where('u.id = :usuario')
            ->setMaxResults(1)
            ->setParameters([
                'usuario' => $usuario,
            ]);

        return $qb->getQuery()->getSingleScalarResult();
    }

}
