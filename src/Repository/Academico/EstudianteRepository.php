<?php

namespace App\Repository\Academico;

use App\Entity\Academico\Estudiante;
use App\Entity\Administracion\Usuario;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Estudiante|null find($id, $lockMode = null, $lockVersion = null)
 * @method Estudiante|null findOneBy(array $criteria, array $orderBy = null)
 * @method Estudiante[]    findAll()
 * @method Estudiante[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EstudianteRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Estudiante::class);
    }

    /**
     * @param Usuario $usuario
     * @return Estudiante|null
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findOneByUsuario(Usuario $usuario): ?Estudiante
    {
        return $this->createQueryBuilder('e')
            ->select('e, p')
            ->innerJoin('e.persona', 'p')
            ->innerJoin('p.cuenta', 'c')
            ->innerJoin('Administracion:Usuario', 'u', 'WITH', 'c.id = u.cuenta')
            ->where('u.id = :usuario')
            ->setParameter('usuario', $usuario)
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * @param array $carnets
     * @return Estudiante[]
     */
    public function findByCarnets(array $carnets)
    {
        $qb = $this->createQueryBuilder('e', 'e.carnet')
            ->innerJoin('e.persona', 'p')
            ->innerJoin('p.cuenta', 'c')
            ->where('e.carnet IN (:carnets)')
            ->setParameter('carnets', $carnets);

        return $qb->getQuery()->getResult();
    }
}
