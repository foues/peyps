<?php

namespace App\Repository\Academico;

use App\Entity\Academico\Curso;
use App\Entity\Academico\PeriodoCiclo;
use App\Entity\Academico\PlanEstudio;
use App\Entity\Academico\PlanEstudioCiclo;
use App\Entity\Academico\PlanEstudioCurso;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method PlanEstudioCurso|null find($id, $lockMode = null, $lockVersion = null)
 * @method PlanEstudioCurso|null findOneBy(array $criteria, array $orderBy = null)
 * @method PlanEstudioCurso[]    findAll()
 * @method PlanEstudioCurso[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PlanEstudioCursoRepository extends ServiceEntityRepository
{
    /**
     * PlanEstudioCursoRepository constructor.
     * @param RegistryInterface $registry
     */
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, PlanEstudioCurso::class);
    }

    /**
     * @param PlanEstudio $plan_estudio
     * @param $curso_codigo
     * @param $ciclo_numero
     * @return PlanEstudioCurso
     * @throws \Doctrine\ORM\ORMException
     */
    public function createFromPlanEstudioAndCursoCodigo(PlanEstudio $plan_estudio, $curso_codigo, $ciclo_numero)
    {
        $em = $this->getEntityManager();
        $curso = $em->getRepository(Curso::class)
            ->findOneBy(['codigo' => $curso_codigo]);
        $plan_estudio_ciclo = $em->getRepository(PlanEstudioCiclo::class)
            ->findOneBy(['numero' => $ciclo_numero]);
        $plan_estudio_curso = (new PlanEstudioCurso())
            ->setPlanEstudio($plan_estudio)
            ->setCurso($curso)
            ->setPlanEstudioCiclo($plan_estudio_ciclo);
        $em->persist($plan_estudio_curso);
        return $plan_estudio_curso;
    }

    /**
     * @param PlanEstudioCurso $plan_estudio_curso
     * @return array
     */
    public function findPeriodoCiclosByPlanEstudioCurso(PlanEstudioCurso $plan_estudio_curso)
    {
        $em = $this->getEntityManager();
        $numero = $plan_estudio_curso->getPlanEstudioCiclo()->getNumero();
        if ($numero % 2 == 0)
            $tipo = 'Par';
        else
            $tipo = 'Impar';

        $ciclos = $em->getRepository(PeriodoCiclo::class)
            ->findByTipo($tipo);

        return $ciclos;
    }

    /**
     * @param PlanEstudio $plan_estudio
     * @param $curso_codigo
     * @return PlanEstudioCurso|null
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findOneByPlanEstudioAndCursoCodigo(PlanEstudio $plan_estudio, $curso_codigo)
    {
        $em = $this->getEntityManager();
        $qb = $em->createQueryBuilder()
            ->select('pec')
            ->from('Academico:PlanEstudioCurso', 'pec')
            ->innerJoin('pec.plan_estudio', 'pe')
            ->innerJoin('pec.curso', 'c')
            ->where('pe.id = :plan_estudio')
            ->andWhere('c.codigo = :curso_codigo')
            ->setParameters([
                'plan_estudio' => $plan_estudio,
                'curso_codigo' => $curso_codigo,
            ]);

        return $qb->getQuery()->getOneOrNullResult();
    }
}
