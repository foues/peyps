<?php

namespace App\Repository\Academico;

use App\Entity\Academico\PeriodoCiclo;
use App\Entity\Academico\PlanEstudio;
use App\Entity\Academico\UnidadIntegracion;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method UnidadIntegracion|null find($id, $lockMode = null, $lockVersion = null)
 * @method UnidadIntegracion|null findOneBy(array $criteria, array $orderBy = null)
 * @method UnidadIntegracion[]    findAll()
 * @method UnidadIntegracion[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UnidadIntegracionRepository extends ServiceEntityRepository
{
    /**
     * UnidadIntegracionRepository constructor.
     * @param RegistryInterface $registry
     */
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, UnidadIntegracion::class);
    }

    /**
     * @param PlanEstudio $plan_estudio
     * @param $curso_codigo
     * @param PeriodoCiclo $ciclo
     * @param $programa_codigo
     * @return UnidadIntegracion|null
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findOneByCursoAndCicloAndPrograma(
        PlanEstudio $plan_estudio,
        $curso_codigo,
        PeriodoCiclo $ciclo,
        $programa_codigo
    ): ?UnidadIntegracion
    {
        $qb = $this->createQueryBuilder('ui')
            ->innerJoin('ui.periodo_ciclo', 'pc')
            ->innerJoin('ui.programa', 'programa')
            ->innerJoin('ui.plan_estudio_curso', 'pec')
            ->innerJoin('pec.plan_estudio', 'pe')
            ->innerJoin('pec.curso', 'curso')
            ->where('pe.id = :plan_estudio')
            ->andWhere('curso.codigo = :curso_codigo')
            ->andWhere('pc.id = :ciclo')
            ->andWhere('programa.codigo = :programa_codigo')
            ->setParameters([
                'plan_estudio' => $plan_estudio,
                'curso_codigo' => $curso_codigo,
                'ciclo' => $ciclo,
                'programa_codigo' => $programa_codigo,
            ]);

        return $qb->getQuery()->getOneOrNullResult();
    }
}
