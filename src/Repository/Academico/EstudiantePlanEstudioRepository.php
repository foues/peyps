<?php

namespace App\Repository\Academico;

use App\Entity\Academico\EstudianteEstado;
use App\Entity\Academico\EstudiantePlanEstudio;
use App\Entity\Academico\PlanEstudio;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method EstudiantePlanEstudio|null find($id, $lockMode = null, $lockVersion = null)
 * @method EstudiantePlanEstudio|null findOneBy(array $criteria, array $orderBy = null)
 * @method EstudiantePlanEstudio[]    findAll()
 * @method EstudiantePlanEstudio[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EstudiantePlanEstudioRepository extends ServiceEntityRepository
{
    /**
     * EstudiantePlanEstudioRepository constructor.
     * @param RegistryInterface $registry
     */
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, EstudiantePlanEstudio::class);
    }

    /**
     * @param PlanEstudio $plan_estudio
     * @param EstudianteEstado $estudiante_estado
     * @return EstudiantePlanEstudio[]
     */
    public function getAllByPlanEstudioAndEstado(PlanEstudio $plan_estudio, EstudianteEstado $estudiante_estado)
    {
        $em = $this->getEntityManager();
        $query = $em->createQuery("
            SELECT epe, e, p, pe, ee FROM Academico:EstudiantePlanEstudio epe
            INNER JOIN epe.estudiante e
            INNER JOIN e.persona p
            INNER JOIN epe.plan_estudio pe
            INNER JOIN epe.estudiante_estado ee 
            WHERE pe.id = :plan_estudio AND ee.id = :estudiante_estado ")
            ->setParameters([
                'plan_estudio' => $plan_estudio,
                'estudiante_estado' => $estudiante_estado,
            ]);

        return $query->getResult();
    }

    /**
     * @param string $carnet
     * @param PlanEstudio $plan_estudio
     * @return EstudiantePlanEstudio|null
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findOneByCarnetAndPlanEstudio(string $carnet, PlanEstudio $plan_estudio)
    {
        $em = $this->getEntityManager();
        $query = $em->createQuery("
            SELECT epe, e FROM Academico:EstudiantePlanEstudio epe
            INNER JOIN epe.estudiante e
            INNER JOIN epe.plan_estudio pe
            WHERE e.carnet = :carnet AND pe.id = :plan_estudio
        ")
            ->setParameters([
                'carnet' => $carnet,
                'plan_estudio' => $plan_estudio,
            ]);

        return $query->getOneOrNullResult();
    }

    /**
     * @param array $carnets
     * @param PlanEstudio $plan_estudio
     * @return EstudiantePlanEstudio[]
     */
    public function findByCarnetsAndPlanEstudio(array $carnets, PlanEstudio $plan_estudio)
    {
        $qb = $this->createQueryBuilder('epe')
            ->innerJoin('epe.estudiante', 'e')
            ->innerJoin('epe.plan_estudio', 'pe')
            ->where('e.carnet IN (:carnets)')
            ->andWhere('pe.id = :plan_estudio')
            ->setParameters([
                'carnets' => $carnets,
                'plan_estudio' => $plan_estudio,
            ]);

        return $qb->getQuery()->getResult();
    }
}
