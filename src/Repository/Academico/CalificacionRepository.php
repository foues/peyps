<?php

namespace App\Repository\Academico;

use App\Entity\Academico\Calificacion;
use App\Entity\Academico\Curso;
use App\Entity\Academico\Estudiante;
use App\Entity\Academico\EstudianteCurso;
use App\Entity\Academico\Evaluacion;
use App\Entity\Academico\PeriodoCiclo;
use App\Entity\Academico\PlanEstudio;
use App\Entity\Academico\PlanEstudioCurso;
use App\Entity\Academico\UnidadIntegracion;
use App\Entity\Rotacion\Programa;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Calificacion|null find($id, $lockMode = null, $lockVersion = null)
 * @method Calificacion|null findOneBy(array $criteria, array $orderBy = null)
 * @method Calificacion[]    findAll()
 * @method Calificacion[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CalificacionRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Calificacion::class);
    }

    /**
     * @param PlanEstudio $plan_estudio
     * @param PeriodoCiclo $ciclo
     * @param Curso $curso
     * @param Programa $programa
     * @return array
     */
    public function getCalificacionesByCursoAndProgramaAndEstudiante(
        PlanEstudio $plan_estudio,
        PeriodoCiclo $ciclo,
        Curso $curso,
        Programa $programa,
        Estudiante $estudiante
    )
    {
        $em = $this->getEntityManager();

        $query = $em->createQuery("
            SELECT e.id, e.nombre, e.ponderacion, e2.id padre, AVG(calificacion.nota) nota, calificacion.fecha
            FROM Academico:Evaluacion e INDEX BY e.id
            INNER JOIN Academico:UnidadIntegracion ui WITH e.unidad_integracion = ui.id
            INNER JOIN Academico:PeriodoCiclo pc WITH ui.periodo_ciclo = pc.id
            INNER JOIN Academico:PlanEstudioCurso pec WITH ui.plan_estudio_curso = pec.id
            INNER JOIN Academico:PlanEstudio pe WITH pec.plan_estudio = pe.id
            INNER JOIN Academico:Curso c WITH pec.curso = c.id
            INNER JOIN Rotacion:Programa p WITH ui.programa = p.id
            
            INNER JOIN Academico:EstudiantePlanEstudio epe WITH pe.id = epe.plan_estudio
            INNER JOIN Academico:Estudiante estudiante WITH epe.estudiante = estudiante.id
            INNER JOIN Academico:EstudianteCurso ec WITH epe.id = ec.estudiante_plan_estudio AND c.id = ec.curso  
            LEFT JOIN Academico:Calificacion calificacion WITH e.id = calificacion.evaluacion AND ec.id = calificacion.estudiante_curso
            
            LEFT JOIN Academico:Evaluacion e2 WITH e.padre = e2.id
            WHERE pe.id = :plan_estudio 
            AND pc.id = :ciclo
            AND c.id = :curso 
            AND p.id = :programa
            AND estudiante.id = :estudiante
            GROUP BY e.id
        ")
            ->setParameters([
                'plan_estudio' => $plan_estudio,
                'ciclo' => $ciclo,
                'curso' => $curso,
                'programa' => $programa,
                'estudiante' => $estudiante,
            ]);

        return $query->getScalarResult();
    }


    /**
     * @param Evaluacion $evaluacion
     * @param EstudianteCurso $estudiante_curso
     * @return mixed
     */
    public function getNotaCalculada(Evaluacion $evaluacion, EstudianteCurso $estudiante_curso)
    {
        $em = $this->getEntityManager();

        $query = $em->createQuery("
            SELECT e.id, e.nombre, e.ponderacion, e2.id padre, AVG(calificacion.nota) promedio, 
            (AVG(calificacion.nota)*e.ponderacion/100) nota, calificacion.fecha
            FROM Academico:Evaluacion e INDEX BY e.id
            INNER JOIN Academico:Calificacion calificacion WITH e.id = calificacion.evaluacion
            INNER JOIN Academico:EstudianteCurso ec WITH calificacion.estudiante_curso = ec.id 
            INNER JOIN Academico:Evaluacion e2 WITH e.padre = e2.id
            WHERE e2.id = :evaluacion
            AND ec.id = :estudiante_curso
            GROUP BY e.id
        ")
            ->setParameters([
                'evaluacion' => $evaluacion,
                'estudiante_curso' => $estudiante_curso,
            ]);

        $notas = $query->getResult();
        $notas = array_column($notas, 'nota');
        $nota = array_sum($notas);
        return $nota;
    }

    /**
     * @param EstudianteCurso $estudiante_curso
     * @param UnidadIntegracion $unidad_integracion
     * @return array|null
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getNotaParcial(EstudianteCurso $estudiante_curso, UnidadIntegracion $unidad_integracion)
    {
        $em = $this->getEntityManager();

        $nota_parcial = $em->createQuery("
                SELECT SUM(calificacion.nota) promedio, (SUM(calificacion.nota)*ui.ponderacion/100) nota
                FROM Academico:Evaluacion e INDEX BY e.id
                INNER JOIN Academico:CalificacionPromedio calificacion WITH e.id = calificacion.evaluacion
                INNER JOIN Academico:EstudianteCurso ec WITH calificacion.estudiante_curso = ec.id
                INNER JOIN Academico:UnidadIntegracion ui WITH e.unidad_integracion = ui.id
                WHERE e.padre IS NULL
                AND ec.id = :estudiante_curso
                AND ui.id = :unidad_integracion
                GROUP BY ec.id
            ")
            ->setParameters([
                'estudiante_curso' => $estudiante_curso,
                'unidad_integracion' => $unidad_integracion,
            ])
            ->setMaxResults(1)
            ->getOneOrNullResult();

        return $nota_parcial;
    }

    /**
     * @param EstudianteCurso $estudiante_curso
     * @return float|int
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getNotaFinal(EstudianteCurso $estudiante_curso)
    {
        $em = $this->getEntityManager();

        $unidades_integracion = $em->createQuery("
            SELECT ui FROM Academico:UnidadIntegracion ui
            INNER JOIN ui.periodo_ciclo ciclo
            INNER JOIN ui.plan_estudio_curso pec
            INNER JOIN pec.plan_estudio pe
            INNER JOIN pec.curso c
            INNER JOIN Academico:EstudiantePlanEstudio epe WITH pe.id = epe.plan_estudio
            INNER JOIN Academico:EstudianteCurso ec WITH epe.id = ec.estudiante_plan_estudio AND c.id = ec.curso AND ciclo.id = ec.periodo_ciclo
            WHERE ec.id = :estudiante_curso
        ")
            ->setParameter('estudiante_curso', $estudiante_curso)
            ->getResult();

        $notas = [];

        /** @var UnidadIntegracion $unidad_integracion */
        foreach ($unidades_integracion as $unidad_integracion) {
            $nota_parcial = $this->getNotaParcial($estudiante_curso, $unidad_integracion);
            if ($nota_parcial) $notas[] = $nota_parcial['nota'];
        }

        $nota = array_sum($notas);
        return $nota;
    }

    /**
     * @param PlanEstudioCurso $plan_estudio_curso
     * @param PeriodoCiclo $ciclo
     * @param UnidadIntegracion $unidad_integracion
     * @return mixed
     */
    public function getNotasFinalesByUnidadIntegracion(PlanEstudioCurso $plan_estudio_curso, PeriodoCiclo $ciclo, UnidadIntegracion $unidad_integracion)
    {
        $em = $this->getEntityManager();

        $query = $em->createQuery("
            SELECT ec.id id_estudiante_curso, estudiante.carnet carnet, CONCAT(p.nombre,' ',p.apellido) nombre, 
            ui.id id_unidad_integracion, SUM(cp.nota) promedio, (SUM(cp.nota)*ui.ponderacion/100) nota
            FROM Academico:CalificacionPromedio cp
            INNER JOIN cp.evaluacion e
            INNER JOIN e.unidad_integracion ui
            INNER JOIN cp.estudiante_curso ec
            INNER JOIN ec.estudiante_plan_estudio epe
            INNER JOIN ec.curso curso
            INNER JOIN ec.periodo_ciclo ciclo
            INNER JOIN epe.estudiante estudiante
            INNER JOIN estudiante.persona p
            INNER JOIN epe.plan_estudio pe
            INNER JOIN Academico:PlanEstudioCurso pec WITH pe.id = pec.plan_estudio AND curso.id = pec.curso
            WHERE pe.id = :plan_estudio
            AND curso.id = :curso
            AND ciclo = :ciclo
            AND ui.id = :unidad_integracion
            AND e.padre IS NULL
            GROUP BY ec.id
        ")
            ->setParameters([
                'plan_estudio' => $plan_estudio_curso->getPlanEstudio(),
                'curso' => $plan_estudio_curso->getCurso(),
                'ciclo' => $ciclo,
                'unidad_integracion' => $unidad_integracion,
            ]);

        return $query->getResult();
    }
}
