<?php

namespace App\Repository\Academico;

use App\Entity\Academico\PlanEstudio;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method PlanEstudio|null find($id, $lockMode = null, $lockVersion = null)
 * @method PlanEstudio|null findOneBy(array $criteria, array $orderBy = null)
 * @method PlanEstudio[]    findAll()
 * @method PlanEstudio[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PlanEstudioRepository extends ServiceEntityRepository
{
    /**
     * PlanEstudioRepository constructor.
     * @param RegistryInterface $registry
     */
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, PlanEstudio::class);
    }

    /**
     * @param string $carrera_codigo
     * @param int $anio
     * @return PlanEstudio|null
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findOneByCarreraCodigoAndAnio($carrera_codigo, $anio): ?PlanEstudio
    {
        return $this->createQueryBuilder('pe')
            ->select('pe, c')
            ->innerJoin('pe.carrera', 'c')
            ->where('c.codigo = :carrera_codigo')
            ->andWhere('pe.anio = :anio')
            ->setParameters([
                'carrera_codigo' => $carrera_codigo,
                'anio' => $anio,
            ])
            ->getQuery()
            ->getOneOrNullResult();
    }

}
