<?php

namespace App\Repository\Academico;

use App\Entity\Academico\PeriodoCiclo;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method PeriodoCiclo|null find($id, $lockMode = null, $lockVersion = null)
 * @method PeriodoCiclo|null findOneBy(array $criteria, array $orderBy = null)
 * @method PeriodoCiclo[]    findAll()
 * @method PeriodoCiclo[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PeriodoCicloRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, PeriodoCiclo::class);
    }

    /**
     * @return PeriodoCiclo|null
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findCicloActual()
    {
        $em = $this->getEntityManager();
        $fecha_actual = date('Y-m-d H:i:s');

        $dql = "
            SELECT ciclo, p, CASE WHEN p.fin > :fecha_actual THEN 1 ELSE 0 END HIDDEN ord
            FROM Academico:PeriodoCiclo ciclo
            INNER JOIN ciclo.periodo p
            WHERE (p.activo = true AND p.inicio <= :fecha_actual AND p.fin >= :fecha_actual)
            OR (p.activo = true) OR (p.inicio <= :fecha_actual AND p.fin >= :fecha_actual) 
            ORDER BY p.activo DESC, ord desc, p.inicio ASC
        ";

        $ciclo = $em->createQuery($dql)
            ->setParameter('fecha_actual', $fecha_actual)
            ->setMaxResults(1)
            ->getOneOrNullResult();

        return $ciclo;
    }

    /**
     * @param PeriodoCiclo $ciclo
     * @return PeriodoCiclo|null
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findCicloSiguiente(PeriodoCiclo $ciclo)
    {
        $inicio = $ciclo->getPeriodo()->getInicio();

        $em = $this->getEntityManager();

        $dql = "
            SELECT ciclo, p FROM Academico:PeriodoCiclo ciclo
            INNER JOIN ciclo.periodo p
            WHERE p.inicio > :inicio
            ORDER BY p.inicio ASC
        ";

        $ciclo_siguiente = $em->createQuery($dql)
            ->setParameter('inicio', $inicio)
            ->setMaxResults(1)
            ->getOneOrNullResult();

        return $ciclo_siguiente;
    }

    /**
     * @param PeriodoCiclo $ciclo
     * @return PeriodoCiclo|null
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findCicloAnterior(PeriodoCiclo $ciclo)
    {
        $inicio = $ciclo->getPeriodo()->getInicio();

        $em = $this->getEntityManager();

        $dql = "
            SELECT ciclo, p FROM Academico:PeriodoCiclo ciclo
            INNER JOIN ciclo.periodo p
            WHERE p.inicio < :inicio
            ORDER BY p.inicio DESC
        ";

        $ciclo_anterior = $em->createQuery($dql)
            ->setParameter('inicio', $inicio)
            ->setMaxResults(1)
            ->getOneOrNullResult();

        return $ciclo_anterior;
    }

    /**
     * @param $anio
     * @param $tipo
     * @return PeriodoCiclo|null
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findOneByAnioAndTipo($anio, $tipo)
    {
        $em = $this->getEntityManager();

        $anio_siguiente = $anio + 1;
        $anio = "$anio-01-01";
        $anio_siguiente = "$anio_siguiente-01-01";

        $dql = "
            SELECT ciclo FROM Academico:PeriodoCiclo ciclo
            INNER JOIN ciclo.periodo p
            WHERE p.inicio >= :anio AND p.fin <= :anio_siguiente AND ciclo.tipo = :tipo
        ";

        $ciclo = $em->createQuery($dql)
            ->setParameters([
                'anio' => $anio,
                'anio_siguiente' => $anio_siguiente,
                'tipo' => $tipo,
            ])
            ->setMaxResults(1)
            ->getOneOrNullResult();

        return $ciclo;
    }

    /**
     * @param $tipo
     * @return PeriodoCiclo[]
     */
    public function findByTipo($tipo)
    {
        $em = $this->getEntityManager();

        $dql = "
            SELECT ciclo, p FROM Academico:PeriodoCiclo ciclo
            INNER JOIN ciclo.periodo p
            WHERE ciclo.tipo = :tipo
            ORDER BY p.activo DESC, p.inicio DESC
        ";

        $ciclos = $em->createQuery($dql)
            ->setParameter('tipo', $tipo)
            ->getResult();

        return $ciclos;
    }

    /**
     * @return PeriodoCiclo[]
     */
    public function findAllWithPeriodo()
    {
        $em = $this->getEntityManager();

        $dql = "
            SELECT ciclo, p FROM Academico:PeriodoCiclo ciclo
            INNER JOIN ciclo.periodo p
            ORDER BY p.activo DESC, p.inicio DESC
        ";

        $ciclos = $em->createQuery($dql)
            ->getResult();

        return $ciclos;
    }
}
