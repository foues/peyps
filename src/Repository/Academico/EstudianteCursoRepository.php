<?php

namespace App\Repository\Academico;

use App\Entity\Academico\Curso;
use App\Entity\Academico\Estudiante;
use App\Entity\Academico\EstudianteCurso;
use App\Entity\Academico\EstudiantePlanEstudio;
use App\Entity\Academico\PeriodoCiclo;
use App\Entity\Academico\PlanEstudio;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method EstudianteCurso|null find($id, $lockMode = null, $lockVersion = null)
 * @method EstudianteCurso|null findOneBy(array $criteria, array $orderBy = null)
 * @method EstudianteCurso[]    findAll()
 * @method EstudianteCurso[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EstudianteCursoRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, EstudianteCurso::class);
    }

    /**
     * @param Estudiante $estudiante
     * @param PlanEstudio $plan_estudio
     * @return EstudianteCurso[]
     */
    public function findByEstudianteAndPlanEstudio(Estudiante $estudiante, PlanEstudio $plan_estudio)
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder()
            ->select('ec, curso, ciclo, periodo, estado')
            ->from('Academico:EstudianteCurso', 'ec', 'ec.id')
            ->innerJoin('ec.curso', 'curso')
            ->innerJoin('ec.periodo_ciclo', 'ciclo')
            ->innerJoin('ciclo.periodo', 'periodo')
            ->innerJoin('ec.estudiante_plan_estudio', 'epe')
            ->innerJoin('epe.estudiante', 'e')
            ->innerJoin('epe.plan_estudio', 'pe')
            ->innerJoin('ec.estudiante_estado', 'estado')
            ->where('e.id = :estudiante')
            ->andWhere('pe.id = :plan_estudio')
            ->orderBy('ciclo.id, curso.id')
            ->setParameters([
                'estudiante' => $estudiante,
                'plan_estudio' => $plan_estudio,
            ]);

        return $qb->getQuery()->getResult();
    }

    /**
     * @param EstudiantePlanEstudio $estudiante_plan_estudio
     * @param Curso $curso
     * @param PeriodoCiclo $ciclo
     * @return EstudianteCurso
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findOneByEstudiantePlanEstudioAndCurso(
        EstudiantePlanEstudio $estudiante_plan_estudio,
        Curso $curso,
        PeriodoCiclo $ciclo
    )
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder()
            ->select('ec, curso, ciclo, periodo, estado')
            ->from('Academico:EstudianteCurso', 'ec', 'ec.id')
            ->innerJoin('ec.curso', 'curso')
            ->innerJoin('ec.periodo_ciclo', 'ciclo')
            ->innerJoin('ciclo.periodo', 'periodo')
            ->innerJoin('ec.estudiante_plan_estudio', 'epe')
            ->innerJoin('epe.estudiante', 'e')
            ->innerJoin('epe.plan_estudio', 'pe')
            ->innerJoin('ec.estudiante_estado', 'estado')
            ->where('epe.id = :estudiante_plan_estudio')
            ->andWhere('curso.id = :curso')
            ->andWhere('ciclo.id = :ciclo')
            ->orderBy('ciclo.id, curso.id')
            ->setMaxResults(1)
            ->setParameters([
                'estudiante_plan_estudio' => $estudiante_plan_estudio,
                'curso' => $curso,
                'ciclo' => $ciclo,
            ]);

        return $qb->getQuery()->getOneOrNullResult();
    }

    /**
     * @param Estudiante $estudiante
     * @param PlanEstudio $plan_estudio
     * @return PeriodoCiclo[]
     */
    public function findCiclosByEstudianteAndPlanEstudio(Estudiante $estudiante, PlanEstudio $plan_estudio)
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder()
            ->select('ciclo')
            ->from('Academico:PeriodoCiclo', 'ciclo')
            ->innerJoin('ciclo.periodo', 'p')
            ->innerJoin('Academico:EstudianteCurso', 'ec', 'WITH', 'ciclo.id = ec.periodo_ciclo')
            ->innerJoin('ec.estudiante_plan_estudio', 'epe')
            ->innerJoin('epe.estudiante', 'e')
            ->innerJoin('epe.plan_estudio', 'pe')
            ->where('e.id = :estudiante')
            ->andWhere('pe.id = :plan_estudio')
            ->orderBy('p.activo')
            ->setParameters([
                'estudiante' => $estudiante,
                'plan_estudio' => $plan_estudio,
            ]);

        return $qb->getQuery()->getResult();
    }

    /**
     * @param Estudiante $estudiante
     * @param PlanEstudio $plan_estudio
     * @param PeriodoCiclo $ciclo
     * @return mixed
     */
    public function findCursosByEstudianteAndPlanEstudioAndCiclo(Estudiante $estudiante, PlanEstudio $plan_estudio, PeriodoCiclo $ciclo)
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder()
            ->select('curso')
            ->from('Academico:Curso', 'curso')
            ->innerJoin('Academico:EstudianteCurso', 'ec', 'WITH', 'curso.id = ec.curso')
            ->innerJoin('ec.periodo_ciclo', 'ciclo')
            ->innerJoin('ec.estudiante_plan_estudio', 'epe')
            ->innerJoin('epe.estudiante', 'e')
            ->innerJoin('epe.plan_estudio', 'pe')
            ->where('e.id = :estudiante')
            ->andWhere('pe.id = :plan_estudio')
            ->andWhere('ciclo.id = :ciclo')
            ->orderBy('curso.id')
            ->setParameters([
                'estudiante' => $estudiante,
                'plan_estudio' => $plan_estudio,
                'ciclo' => $ciclo,
            ]);

        return $qb->getQuery()->getResult();
    }
}
