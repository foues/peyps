<?php

namespace App\Repository\Academico;

use App\Entity\Academico\Curso;
use App\Entity\Academico\CursoPrograma;
use App\Entity\Academico\EstudianteCurso;
use App\Entity\Academico\PeriodoCiclo;
use App\Entity\Academico\PlanEstudio;
use App\Entity\Academico\PlanEstudioCurso;
use App\Entity\Rotacion\Programa;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method CursoPrograma|null find($id, $lockMode = null, $lockVersion = null)
 * @method CursoPrograma|null findOneBy(array $criteria, array $orderBy = null)
 * @method CursoPrograma[]    findAll()
 * @method CursoPrograma[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CursoProgramaRepository extends ServiceEntityRepository
{
    /**
     * CursoProgramaRepository constructor.
     * @param RegistryInterface $registry
     */
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, CursoPrograma::class);
    }

    /**
     * @param PlanEstudio $plan_estudio
     * @param $curso_codigo
     * @param $programa_codigo
     * @return CursoPrograma
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\ORM\ORMException
     */
    public function createFromCursoCodigoAndProgramaCodigo(PlanEstudio $plan_estudio, $curso_codigo, $programa_codigo, $correlativo = null)
    {
        $em = $this->getEntityManager();

        $plan_estudio_curso = $em->createQuery("
            SELECT pec FROM Academico:PlanEstudioCurso pec
            INNER JOIN pec.curso c
            INNER JOIN pec.plan_estudio pe
            WHERE c.codigo = :curso_codigo and pe.id = :plan_estudio ")
            ->setParameters([
                'curso_codigo' => $curso_codigo,
                'plan_estudio' => $plan_estudio,
            ])
            ->setMaxResults(1)
            ->getOneOrNullResult();

        $programa = $em->getRepository(Programa::class)
            ->findOneBy(['codigo' => $programa_codigo]);

        $curso_programa = (new CursoPrograma())
            ->setPlanEstudioCurso($plan_estudio_curso)
            ->setPrograma($programa)
            ->setCorrelativo($correlativo);

        $em->persist($curso_programa);

        return $curso_programa;
    }

    /**
     * @param EstudianteCurso $estudiante_curso
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getSiguienteCursoConPrograma(EstudianteCurso $estudiante_curso)
    {
        $em = $this->getEntityManager();

        $plan_estudio_ciclo = $em->createQuery("
            SELECT ciclo FROM Academico:PlanEstudioCiclo ciclo
            INNER JOIN Academico:PlanEstudioCurso pec WITH ciclo.id = pec.plan_estudio_ciclo
            INNER JOIN pec.curso c
            INNER JOIN pec.plan_estudio pe
            WHERE c.id = :curso_actual
            AND pe.id = :plan_estudio ")
            ->setParameters([
                'curso_actual' => $estudiante_curso->getCurso(),
                'plan_estudio' => $estudiante_curso->getEstudiantePlanEstudio()->getPlanEstudio(),
            ])
            ->getOneOrNullResult();

        $curso = $em->createQuery("
            SELECT c FROM Academico:Curso c
            INNER JOIN Academico:PlanEstudioCurso pec WITH c.id = pec.curso
            INNER JOIN pec.plan_estudio_ciclo ciclo
            INNER JOIN Academico:CursoPrograma cp WITH pec.id = cp.plan_estudio_curso
            WHERE ciclo.numero = :numero + 1
            AND pec.plan_estudio = :plan_estudio
            ORDER BY ciclo.numero, pec.id ")
            ->setParameters([
                'plan_estudio' => $estudiante_curso->getEstudiantePlanEstudio()->getPlanEstudio(),
                'numero' => $plan_estudio_ciclo->getNumero(),
            ])
            ->setMaxResults(1)
            ->getOneOrNullResult();

        return $curso;
    }

    /**
     * @param PlanEstudio $plan_estudio
     * @return array
     */
    public function getPlanEstudioCursosConProgramas(PlanEstudio $plan_estudio)
    {
        $em = $this->getEntityManager();

        $cursos = $em->createQuery("      
            SELECT pec, c FROM Academico:PlanEstudioCurso pec
            INNER JOIN pec.curso c WITH c.id = pec.curso
            INNER JOIN pec.plan_estudio_ciclo ciclo
            INNER JOIN Academico:CursoPrograma cp WITH pec.id = cp.plan_estudio_curso
            WHERE pec.plan_estudio = :plan_estudio
            ORDER BY ciclo.numero, pec.id ")
            ->setParameter('plan_estudio', $plan_estudio)
            ->getResult();

        return $cursos;
    }

    /**
     * @param PlanEstudio $plan_estudio
     * @return array
     */
    public function getCursosConProgramas(PlanEstudio $plan_estudio)
    {
        $em = $this->getEntityManager();

        $cursos = $em->createQuery("      
            SELECT c FROM Academico:Curso c
            INNER JOIN Academico:PlanEstudioCurso pec WITH c.id = pec.curso
            INNER JOIN pec.plan_estudio_ciclo ciclo
            INNER JOIN Academico:CursoPrograma cp WITH pec.id = cp.plan_estudio_curso
            WHERE pec.plan_estudio = :plan_estudio
            ORDER BY ciclo.numero, pec.id ")
            ->setParameters([
                'plan_estudio' => $plan_estudio,
            ])
            ->getResult();

        return $cursos;
    }

    /**
     * @param PlanEstudio $plan_estudio
     * @param int $ciclo_numero
     * @return array
     */
    public function getCursosConProgramasByNumero(PlanEstudio $plan_estudio, int $ciclo_numero)
    {
        $em = $this->getEntityManager();

        $cursos = $em->createQuery("      
            SELECT c FROM Academico:Curso c
            INNER JOIN Academico:PlanEstudioCurso pec WITH c.id = pec.curso
            INNER JOIN pec.plan_estudio_ciclo ciclo
            INNER JOIN Academico:CursoPrograma cp WITH pec.id = cp.plan_estudio_curso
            WHERE ciclo.numero = :ciclo_numero
            AND pec.plan_estudio = :plan_estudio
            ORDER BY ciclo.numero, pec.id ")
            ->setParameters([
                'plan_estudio' => $plan_estudio,
                'ciclo_numero' => $ciclo_numero,
            ])
            ->getResult();

        return $cursos;
    }

    /**
     * @param PlanEstudio $plan_estudio
     * @param PeriodoCiclo $periodo_ciclo
     * @return mixed
     */
    public function getCursosConProgramasByCiclo(PlanEstudio $plan_estudio, PeriodoCiclo $periodo_ciclo)
    {
        $em = $this->getEntityManager();

        if ($periodo_ciclo->getTipo() == 'Par') $numero = 0;
        elseif ($periodo_ciclo->getTipo() == 'Impar') $numero = 1;
        else $numero = null;

        $cursos = $em->createQuery("      
            SELECT c FROM Academico:Curso c
            INNER JOIN Academico:PlanEstudioCurso pec WITH c.id = pec.curso
            INNER JOIN pec.plan_estudio_ciclo ciclo
            INNER JOIN Academico:CursoPrograma cp WITH pec.id = cp.plan_estudio_curso
            WHERE MOD(ciclo.numero, 2) = :numero
            AND pec.plan_estudio = :plan_estudio
            ORDER BY ciclo.numero, pec.id ")
            ->setParameters([
                'plan_estudio' => $plan_estudio,
                'numero' => $numero,
            ])
            ->getResult();

        return $cursos;
    }

    /**
     * @param PlanEstudioCurso $plan_estudio_curso
     * @return array
     */
    public function getProgramasByPlanEstudioCurso(PlanEstudioCurso $plan_estudio_curso)
    {
        $em = $this->getEntityManager();

        $programas = $em->createQuery("      
            SELECT p FROM Rotacion:Programa p
            INNER JOIN Academico:CursoPrograma cp WITH p.id = cp.programa
            INNER JOIN Academico:PlanEstudioCurso pec WITH cp.plan_estudio_curso = pec.id
            WHERE pec = :plan_estudio_curso
            ORDER BY p.id ")
            ->setParameters([
                'plan_estudio_curso' => $plan_estudio_curso,
            ])
            ->getResult();

        return $programas;
    }

    /**
     * @param PlanEstudio $plan_estudio
     * @param Curso $curso
     * @return Programa[]
     */
    public function getProgramasByPlanEstudioAndCurso(PlanEstudio $plan_estudio, Curso $curso)
    {
        $em = $this->getEntityManager();

        $programas = $em->createQuery("      
            SELECT p FROM Rotacion:Programa p
            INNER JOIN Academico:CursoPrograma cp WITH p.id = cp.programa
            INNER JOIN Academico:PlanEstudioCurso pec WITH cp.plan_estudio_curso = pec.id
            INNER JOIN pec.curso c
            INNER JOIN pec.plan_estudio pe
            WHERE pe = :plan_estudio and c = :curso
            ORDER BY p.id ")
            ->setParameters([
                'plan_estudio' => $plan_estudio,
                'curso' => $curso
            ])
            ->getResult();

        return $programas;
    }
}
