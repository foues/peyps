<?php

namespace App\Repository\Academico;

use App\Entity\Academico\Evaluacion;
use App\Entity\Academico\UnidadIntegracion;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Evaluacion|null find($id, $lockMode = null, $lockVersion = null)
 * @method Evaluacion|null findOneBy(array $criteria, array $orderBy = null)
 * @method Evaluacion[]    findAll()
 * @method Evaluacion[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EvaluacionRepository extends ServiceEntityRepository
{
    /**
     * EvaluacionRepository constructor.
     * @param RegistryInterface $registry
     */
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Evaluacion::class);
    }

    /**
     * @param $id_unidad_integracion
     * @param $id_padre
     * @return Evaluacion[]
     */
    public function findByIdUnidadIntegracionAndIdPadre($id_unidad_integracion, $id_padre = null)
    {
        $qb = $this->createQueryBuilder('e')
            ->select('e, ui, et')
            ->innerJoin('e.unidad_integracion', 'ui')
            ->innerJoin('e.evaluacion_tipo', 'et')
            ->where('ui.id = :id_unidad_integracion')
            ->setParameter('id_unidad_integracion', $id_unidad_integracion)
            ->orderBy('e.id', 'ASC');

        if ($id_padre) {
            $qb->andWhere('e.padre = :id_padre')
                ->setParameter('id_padre', $id_padre);
        } else {
            $qb->andWhere('e.padre is null');
        }

        return $qb->getQuery()->getResult();
    }

    /**
     * @param Evaluacion $padre
     * @param $tipo
     * @return Evaluacion[]
     */
    public function findByPadreAndTipo(Evaluacion $padre, $tipo)
    {
        $qb = $this->createQueryBuilder('e')
            ->innerJoin('e.evaluacion_tipo', 'et')
            ->where('e.padre = :padre')
            ->andWhere('et.codigo = :tipo')
            ->setParameters([
                'padre' => $padre,
                'tipo' => $tipo,
            ]);

        return $qb->getQuery()->getResult();
    }

    /**
     * @param Evaluacion $evaluacion
     * @param UnidadIntegracion $unidad_integracion
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getPonderacionByEvaluacionAndUnidadIntegracion(Evaluacion $evaluacion, UnidadIntegracion $unidad_integracion)
    {
        $em = $this->getEntityManager();

        $qb = $em->createQueryBuilder()
            ->select('SUM(e.ponderacion)')
            ->from('Academico:Evaluacion', 'e')
            ->where('e.unidad_integracion = :unidad_integracion')
            ->setParameter('unidad_integracion', $unidad_integracion);

        //Verificando si es nueva o edicion
        if ($evaluacion->getId()) {
            //Sumatoria de los hermanos sin incluir la evaluacion actual
            $qb->andWhere('e.id <> :evaluacion')
                ->setParameter('evaluacion', $evaluacion);
        }

        //Verificando si es de nivel superior
        if ($evaluacion->getPadre()) {
            $qb->andWhere('e.padre = :padre')
                ->setParameter('padre', $evaluacion->getPadre());
        } else {
            $qb->andWhere('e.padre IS NULL');
        }

        return $qb->getQuery()->getSingleScalarResult();
    }
}
