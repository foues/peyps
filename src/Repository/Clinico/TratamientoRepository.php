<?php
namespace App\Repository\Clinico;

use App\Entity\Clinico\Tratamiento;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

class TratamientoRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Tratamiento::class);
    }

    public function findByState($state)
    {
      $em = $this->getEntityManager();

      if(strlen($state) == 2)
        $catalogo = "icdas_estados";
      else
        $catalogo = "icdas_estados_bc";

      $query = $em->createQuery('select t.id, t.nombre from App\Entity\Clinico\Tratamiento t where t.id in (select IDENTITY(te.tratamiento) from App\Entity\Clinico\TratamientoEstado te where te.estado = (select e.id from App\Entity\Clinico\FichaCatalogo e where e.ficha_catalogo_tipo = (select fct.id from App\Entity\Clinico\FichaCatalogo fct where fct.codigo = :catalogo) and e.codigo = :state))')
            ->setParameter('state', $state)
            ->setParameter('catalogo', $catalogo);

        return $query->execute();
    }

    public function findByName($name)
    {
      $em = $this->getEntityManager();
      $query = $em->createQuery('select t.id, t.nombre from App\Entity\Clinico\Tratamiento t where t.nombre like :name')
            ->setParameter('name', '%'.$name.'%')
            ->setMaxResults(5);

        return $query->execute();
    }
}
