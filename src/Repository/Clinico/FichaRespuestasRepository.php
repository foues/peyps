<?php
namespace App\Repository\Clinico;

use App\Entity\Clinico\FichaRespuestas;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

class FichaRespuestasRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, FichaRespuestas::class);
    }

    public function findByCodigoTipo($codigo_tipo): array
    {
      $tipo = $this->findOneByCodigo($codigo_tipo);
      return $this->findBy(['ficha_catalogo_tipo'=>$tipo]);
    }

    public function findByRespuesta($codigo)
    {
      // $tipo = $this->findOneByCodigo($codigo_tipo);
      return $this->createQueryBuilder('t')
            ->innerJoin('t.opciones_pregunta', 'op')
            ->innerJoin('op.pregunta', 'p')
            ->andWhere('sdfsdfgo_tipo = :id_tipo')
            ->setParameter('codigo', $codigo)
            // ->setParameter('id_tipo', $tipo->getId())
            ->getQuery()
            ->execute();

    }

    public function findOneByCodigoTipoAndCodigo($codigo_tipo, $codigo)
    {
      return ($this->findByCodigoTipoAndCodigo($codigo_tipo, $codigo))[0];
    }

    public function findByCodigoLike($codigo)
    {
      $qb = $this->createQueryBuilder('cat')
            ->andWhere('cat.codigo like :codigo')
            ->orderBy('cat.codigo', 'ASC')
            ->setParameter('codigo', $codigo)
            ->getQuery();

        return $qb->execute();
    }
}
