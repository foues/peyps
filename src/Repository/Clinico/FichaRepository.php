<?php

namespace App\Repository\Clinico;

use App\Entity\Clinico\Ficha;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Ficha|null find($id, $lockMode = null, $lockVersion = null)
 * @method Ficha|null findOneBy(array $criteria, array $orderBy = null)
 * @method Ficha[]    findAll()
 * @method Ficha[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FichaRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Ficha::class);
    }

    /**
     * @param $codigo_expediente
     * @return mixed
     */
    function findByCodigoExpediente($codigo_expediente)
    {
        $qb = $this->createQueryBuilder('f')
            ->select('f', 'e', 'p')
            ->innerJoin('f.expediente_clinico', 'e')
            ->innerJoin('e.persona', 'p')
            ->where('e.codigo_expediente = :codigo_expediente')
            ->orderBy('f.id', 'DESC')
            ->setParameter('codigo_expediente', $codigo_expediente);

        return $qb->getQuery()->getResult();
    }
}
