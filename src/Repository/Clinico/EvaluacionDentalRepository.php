<?php

namespace App\Repository\Clinico;

use App\Entity\Clinico\EvaluacionDental;
use App\Entity\Clinico\Ficha;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method EvaluacionDental|null find($id, $lockMode = null, $lockVersion = null)
 * @method EvaluacionDental|null findOneBy(array $criteria, array $orderBy = null)
 * @method EvaluacionDental[]    findAll()
 * @method EvaluacionDental[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EvaluacionDentalRepository extends ServiceEntityRepository
{
    /**
     * EvaluacionDentalRepository constructor.
     * @param RegistryInterface $registry
     */
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, EvaluacionDental::class);
    }

    /**
     * @param Ficha $ficha
     * @return EvaluacionDental[]
     */
    public function findPrioritariosByFicha(Ficha $ficha)
    {
        $qb = $this->createQueryBuilder('e')
            ->select('e, t')
            ->innerJoin('e.tratamiento', 't')
            ->innerJoin('e.ficha', 'f')
            ->where('f.id = :ficha')
            ->andWhere('e.prioridad = :prioridad')
            ->setParameters([
                'ficha' => $ficha,
                'prioridad' => true,
            ]);

        return $qb->getQuery()->getResult();
    }

}
