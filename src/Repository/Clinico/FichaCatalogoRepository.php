<?php
namespace App\Repository\Clinico;

use App\Entity\Clinico\FichaCatalogo;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

class FichaCatalogoRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, FichaCatalogo::class);
    }

    public function findByCodigoTipo($codigo_tipo): array
    {
      $tipo = $this->findOneByCodigo($codigo_tipo);
      return $this->findBy(['ficha_catalogo_tipo'=>$tipo]);
    }

    public function findByCodigoTipoAndCodigo($codigo_tipo, $codigo)
    {
      $tipo = $this->findOneByCodigo($codigo_tipo);
      $qb = $this->createQueryBuilder('cat')
            ->andWhere('cat.codigo like :codigo')
            ->andWhere('cat.ficha_catalogo_tipo = :id_tipo')
            ->setParameter('codigo', $codigo)
            ->setParameter('id_tipo', $tipo->getId())
            ->getQuery();

        return $qb->execute();
    }

    public function findOneByCodigoTipoAndCodigo($codigo_tipo, $codigo)
    {
      return ($this->findByCodigoTipoAndCodigo($codigo_tipo, $codigo))[0];
    }

    public function findByCodigoLike($codigo)
    {
      $qb = $this->createQueryBuilder('cat')
            ->andWhere('cat.codigo like :codigo')
            ->orderBy('cat.codigo', 'ASC')
            ->setParameter('codigo', $codigo)
            ->getQuery();

        return $qb->execute();
    }
}
