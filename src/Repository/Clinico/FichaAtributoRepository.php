<?php
namespace App\Repository\Clinico;

use App\Entity\Clinico\FichaAtributo;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;


class FichaAtributoRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, FichaAtributo::class);
    }



    public function findByFichaAndCatalogoTipo($fichaId, $codigoTipo)
    { 
        return $this->createQueryBuilder('a')
                ->andWhere('a.ficha = :ficha')
                ->setParameter('ficha', $fichaId)
                ->innerJoin('a.atributo', 'b')
                ->innerJoin('b.ficha_catalogo_tipo', 'c')
                ->andWhere('c.codigo = :codigo')
                ->setParameter('codigo', $codigoTipo)
                ->getQuery()
                ->execute();
    }

}
