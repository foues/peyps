<?php

namespace App\Repository\Rotacion;

use App\Entity\Rotacion\Grupo;
use App\Entity\Rotacion\GrupoEstudiante;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method GrupoEstudiante|null find($id, $lockMode = null, $lockVersion = null)
 * @method GrupoEstudiante|null findOneBy(array $criteria, array $orderBy = null)
 * @method GrupoEstudiante[]    findAll()
 * @method GrupoEstudiante[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class GrupoEstudianteRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, GrupoEstudiante::class);
    }

    /**
     * @return GrupoEstudiante[] Returns an array of GrupoEstudiante objects
     */
    public function findByGrupoAndPadre(Grupo $grupo)
    {
        return $this->createQueryBuilder('ge')
            ->innerJoin('ge.grupo', 'g')
            ->innerJoin('g.padre', 'g2')
            ->innerJoin('Rotacion:GrupoEstudiante', 'ge2', 'WITH', 'g2.id = ge2.grupo')
            ->innerJoin('Rotacion:RotacionEstudiante','re', 'WITH', 'ge2.id = re.grupo_estudiante')
            ->innerJoin('re.rotacion', 'r')
            ->where('g.id = :grupo')
            ->setParameter('grupo', $grupo)
            ->getQuery()
            ->getResult();
    }
}
