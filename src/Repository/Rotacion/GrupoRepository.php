<?php

namespace App\Repository\Rotacion;

use App\Entity\Academico\PeriodoCiclo;
use App\Entity\Academico\PlanEstudioCurso;
use App\Entity\Rotacion\Grupo;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Grupo|null find($id, $lockMode = null, $lockVersion = null)
 * @method Grupo|null findOneBy(array $criteria, array $orderBy = null)
 * @method Grupo[]    findAll()
 * @method Grupo[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class GrupoRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Grupo::class);
    }

    /**
     * @param $nombre
     * @param $anio
     * @param $ciclo_tipo
     * @param null $padre
     * @return Grupo|null
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findOneByNombreAndCicloAnioTipo($nombre, $anio, $ciclo_tipo, $padre = null): ?Grupo
    {
        $em = $this->getEntityManager();

        $anio_siguiente = $anio + 1;
        $anio = "$anio-01-01";
        $anio_siguiente = "$anio_siguiente-01-01";

        $qb = $em->createQueryBuilder()
            ->select('g')
            ->from('Rotacion:Grupo', 'g')
            ->innerJoin('g.periodo_ciclo', 'c')
            ->innerJoin('c.periodo', 'p')
            ->where('g.nombre = :nombre')
            ->andWhere('p.inicio >= :anio ')
            ->andWhere('p.fin <= :anio_siguiente')
            ->andWhere('c.tipo = :ciclo_tipo')
            ->setParameters([
                'nombre' => $nombre,
                'anio' => $anio,
                'anio_siguiente' => $anio_siguiente,
                'ciclo_tipo' => $ciclo_tipo,
            ])
            ->setMaxResults(1);

        if ($padre) {
            $qb->innerJoin('g.padre', 'padre')
                ->andWhere('padre.nombre = :padre')
                ->setParameter('padre', $padre);
        }

        return $qb->getQuery()->getOneOrNullResult();
    }

    /**
     * @param PlanEstudioCurso $plan_estudio_curso
     * @param PeriodoCiclo $ciclo
     * @return Grupo[]
     */
    public function findByCursoAndCicloAndCalificable(PlanEstudioCurso $plan_estudio_curso, PeriodoCiclo $ciclo)
    {
        return $this->createQueryBuilder('g')
            ->innerJoin('g.periodo_ciclo', 'c')
            ->innerJoin('Rotacion:GrupoEstudiante', 'ge', 'WITH', 'g.id = ge.grupo')
            ->innerJoin('ge.estudiante_curso', 'ec')
            ->innerJoin('ec.estudiante_plan_estudio', 'epe')
            ->innerJoin('epe.plan_estudio', 'pe')
            ->innerJoin('Academico:PlanEstudioCurso', 'pec', 'WITH', 'pe.id = pec.plan_estudio')
            ->innerJoin('pec.curso', 'curso', 'WITH', 'pec.curso = curso.id AND ec.curso = curso.id')
            ->where('pec.id = :plan_estudio_curso')
            ->andWhere('c.id = :ciclo')
            ->andWhere('g.calificable = :calificable')
            ->orderBy('g.nombre', 'ASC')
            ->setParameters([
                'plan_estudio_curso' => $plan_estudio_curso,
                'ciclo' => $ciclo,
                'calificable' => true,
            ])
            ->getQuery()
            ->getResult();
    }
}
