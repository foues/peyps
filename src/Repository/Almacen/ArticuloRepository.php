<?php

namespace App\Repository\Almacen;

use App\Entity\Almacen\Almacen;
use App\Entity\Almacen\Articulo;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Articulo|null find($id, $lockMode = null, $lockVersion = null)
 * @method Articulo|null findOneBy(array $criteria, array $orderBy = null)
 * @method Articulo[]    findAll()
 * @method Articulo[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ArticuloRepository extends ServiceEntityRepository
{
    /**
     * ArticuloRepository constructor.
     * @param RegistryInterface $registry
     */
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Articulo::class);
    }

    /**
     * @return array
     */
    function findAllScalar()
    {
        $em = $this->getEntityManager();
        $query = $em->createQuery("SELECT a.id, a.codigo, a.nombre FROM Almacen:Articulo a");
        return $query->getResult(\Doctrine\ORM\Query::HYDRATE_SCALAR);
    }

    /**
     * @param Almacen $almacen
     * @return Articulo[]
     */
    public function findByExistenciaInAlmacen(Almacen $almacen)
    {
        $em = $this->getEntityManager();
        $query = $em->createQuery("
            SELECT articulo FROM Almacen:Articulo articulo
            INNER JOIN Almacen:ArticuloPresentacion p WITH articulo.id = p.articulo
            INNER JOIN Almacen:Existencia e WITH p.id = e.articulo_presentacion
            INNER JOIN Almacen:Almacen a WITH e.almacen = a.id
            WHERE a.id = :almacen
        ")
            ->setParameter('almacen', $almacen);

        return $query->getResult();
    }
}
