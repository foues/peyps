<?php

namespace App\Repository\Almacen;

use App\Entity\Almacen\Consumo;
use App\Entity\Almacen\Vale;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Consumo|null find($id, $lockMode = null, $lockVersion = null)
 * @method Consumo|null findOneBy(array $criteria, array $orderBy = null)
 * @method Consumo[]    findAll()
 * @method Consumo[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ConsumoRepository extends ServiceEntityRepository
{
    /**
     * ConsumoRepository constructor.
     * @param RegistryInterface $registry
     */
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Consumo::class);
    }

    /**
     * @return Consumo[] Returns an array of Consumo objects
     */
    public function findWithTratamientosByValeScalar(Vale $vale)
    {
        $qb = $this->createQueryBuilder('c')
            ->select('c, m, ap, a, su, v, ved, ve, t')
            ->innerJoin('c.movimiento', 'm')
            ->innerJoin('m.articulo_presentacion', 'ap')
            ->innerJoin('ap.articulo', 'a')
            ->innerJoin('ap.subunidad_medida', 'su')
            ->innerJoin('c.vale', 'v')
            ->innerJoin('Almacen:ValeEvaluacionDental', 'ved', 'WITH', 'v.id = ved.vale')
            ->innerJoin('ved.evaluacion_dental', 've')
            ->innerJoin('ve.tratamiento', 't')
            ->where('v.id = :vale')
            ->setParameter('vale', $vale)
            ->groupBy('m.id');

        return $qb->getQuery()->getScalarResult();
    }

}
