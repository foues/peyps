<?php

namespace App\Repository\Almacen;

use App\Entity\Almacen\Almacen;
use App\Entity\Almacen\ArticuloPresentacion;
use App\Entity\Almacen\Articulo;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method ArticuloPresentacion|null find($id, $lockMode = null, $lockVersion = null)
 * @method ArticuloPresentacion|null findOneBy(array $criteria, array $orderBy = null)
 * @method ArticuloPresentacion[]    findAll()
 * @method ArticuloPresentacion[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ArticuloPresentacionRepository extends ServiceEntityRepository
{
    /**
     * ArticuloPresentacionRepository constructor.
     * @param RegistryInterface $registry
     */
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ArticuloPresentacion::class);
    }

    /**
     * @param Articulo $articulo
     * @return array
     */
    function findByArticuloScalar(Articulo $articulo)
    {
        $em = $this->getEntityManager();
        $query = $em->createQuery("
            SELECT presentacion, unidad, subunidad FROM Almacen:ArticuloPresentacion presentacion
            INNER JOIN presentacion.articulo articulo
            INNER JOIN presentacion.unidad_medida unidad
            INNER JOIN presentacion.subunidad_medida subunidad
            WHERE articulo.id = :articulo
        ")
            ->setParameter('articulo', $articulo);

        return $query->getResult(\Doctrine\ORM\Query::HYDRATE_SCALAR);
    }

    /**
     * @param Almacen $almacen
     * @return ArticuloPresentacion[]
     */
    public function findByExistenciaInAlmacen(Almacen $almacen)
    {
        $em = $this->getEntityManager();
        $query = $em->createQuery("
            SELECT p FROM Almacen:ArticuloPresentacion p
            INNER JOIN Almacen:Existencia e WITH p.id = e.articulo_presentacion
            INNER JOIN Almacen:Almacen a WITH e.almacen = a.id
            WHERE a.id = :almacen
        ")
            ->setParameter('almacen', $almacen);

        return $query->getResult();
    }
}
