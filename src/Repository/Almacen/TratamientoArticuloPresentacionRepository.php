<?php

namespace App\Repository\Almacen;

use App\Entity\Almacen\TratamientoArticuloPresentacion;
use App\Entity\Clinico\Tratamiento;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method TratamientoArticuloPresentacion|null find($id, $lockMode = null, $lockVersion = null)
 * @method TratamientoArticuloPresentacion|null findOneBy(array $criteria, array $orderBy = null)
 * @method TratamientoArticuloPresentacion[]    findAll()
 * @method TratamientoArticuloPresentacion[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TratamientoArticuloPresentacionRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, TratamientoArticuloPresentacion::class);
    }

    /**
     * @return TratamientoArticuloPresentacion[]
     */
    public function findByTratamiento(Tratamiento $tratamiento)
    {
        $em = $this->getEntityManager();
        $query = $em->createQuery("
            SELECT tap, t, ap, a, u, su FROM Almacen:TratamientoArticuloPresentacion tap
            INNER JOIN tap.tratamiento t
            INNER JOIN tap.articulo_presentacion ap
            INNER JOIN ap.articulo a
            INNER JOIN ap.unidad_medida u
            INNER JOIN ap.subunidad_medida su
            WHERE t.id = :tratamiento
        ")
            ->setParameter('tratamiento', $tratamiento);

        return $query->getResult();
    }
}
