<?php

namespace App\Admin\Administracion;

use Knp\Menu\ItemInterface;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Admin\AdminInterface;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ProxyQueryInterface;
use Sonata\AdminBundle\Form\FormMapper;

class MenuAdmin extends AbstractAdmin
{

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper->add('nombre');
        $formMapper->add('descripcion');
        $formMapper->add('url');
        $formMapper->add('icon');
        $formMapper->add('enabled');
        $formMapper->add('padre');
    }

    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('id');
        $datagridMapper->add('nombre');
        $datagridMapper->add('descripcion');
        $datagridMapper->add('url');
        $datagridMapper->add('icon');
        $datagridMapper->add('enabled');
        $datagridMapper->add('padre');
        $datagridMapper->add('creado');
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('id');
        $listMapper->add('nombre');
        $listMapper->add('descripcion');
        $listMapper->add('url');
        $listMapper->add('icon');
        $listMapper->add('enabled');
        $listMapper->add('padre');
        $listMapper->add('creado', null, [
            'format' => 'Y-m-d H:i:s'
        ]);
        $listMapper->add('_action', null, [
            'actions' => [
                'edit' => [],
                'registros' => [
                    'template' => 'admin/administracion/list__action_submenus.html.twig'
                ],
            ]
        ]);
    }

    /**
     * @param ItemInterface $menu
     * @param $action
     * @param AdminInterface|null $childAdmin
     * @return mixed|void
     */
    protected function configureSideMenu(ItemInterface $menu, $action, AdminInterface $childAdmin = null)
    {
        if (!$childAdmin && !in_array($action, ['edit', 'show'])) {
            return;
        }

        $admin = $this->isChild() ? $this->getParent() : $this;
        $id = $admin->getRequest()->get('id');

        if ($this->isGranted('LIST')) {
            $menu->addChild('Menús', [
                'uri' => $admin->generateUrl('list')
            ]);
            $menu->addChild('Listar submenus', [
                'uri' => $admin->generateUrl('admin.submenu.list', ['id' => $id])
            ]);
            $menu->addChild('Listar permisos', [
                'uri' => $admin->generateUrl('admin.menu_permiso.list', ['id' => $id])
            ]);
        }
    }

    /**
     * @param string $context
     * @return \Sonata\AdminBundle\Datagrid\ProxyQueryInterface
     */
    public function createQuery($context = 'list')
    {
        /** @var ProxyQueryInterface $query */
        $query = parent::createQuery($context);
        $query->andWhere(
            $query->expr()->isNull($query->getRootAliases()[0] . '.padre')
        );
        return $query;
    }
}