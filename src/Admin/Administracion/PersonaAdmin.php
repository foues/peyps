<?php

namespace App\Admin\Administracion;

use App\Entity\Administracion\Cuenta;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Form\Type\ModelType;
use Symfony\Component\Form\Extension\Core\Type\DateType;

class PersonaAdmin extends AbstractAdmin
{
    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper->add('cuenta', ModelType::class, [
            'class' => Cuenta::class,
        ]);
        $formMapper->add('nombre');
        $formMapper->add('apellido');
        $formMapper->add('dui');
        $formMapper->add('nit');
        $formMapper->add('nacimiento', DateType::class, [
            'widget' => 'single_text',
        ]);
    }

    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('id');
        $datagridMapper->add('nombre');
        $datagridMapper->add('apellido');
        $datagridMapper->add('dui');
        $datagridMapper->add('nit');
        $datagridMapper->add('nacimiento');
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('id');
        $listMapper->addIdentifier('nombre');
        $listMapper->addIdentifier('apellido');
        $listMapper->addIdentifier('dui');
        $listMapper->addIdentifier('nit');
        $listMapper->addIdentifier('nacimiento', null, [
            'format' => 'Y-m-d'
        ]);
        $listMapper->addIdentifier('creado', null, [
            'format' => 'Y-m-d H:i:s'
        ]);
    }
}