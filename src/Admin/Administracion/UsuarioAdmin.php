<?php

namespace App\Admin\Administracion;

use App\Entity\Administracion\Cuenta;
use App\Entity\Administracion\Usuario;
use App\Service\UsuarioService;
use Knp\Menu\ItemInterface;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Admin\AdminInterface;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Form\Type\ModelType;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;

class UsuarioAdmin extends AbstractAdmin
{

    /**
     * @var UsuarioService
     */
    private $usuario_service;

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper->add('cuenta', ModelType::class, [
            'class' => Cuenta::class,
        ]);
        $formMapper->add('username');
        $formMapper->add('email');
        $formMapper->add('password', PasswordType::class, [
            'always_empty' => false,
            'required' => false,
        ]);
        $formMapper->add('enabled');
        $formMapper->remove('_delete');
    }

    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('id');
        $datagridMapper->add('username');
        $datagridMapper->add('email');
        $datagridMapper->add('enabled');
        $datagridMapper->add('creado');
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('id');
        $listMapper->add('cuenta.id');
        $listMapper->add('username');
        $listMapper->add('email');
        $listMapper->add('enabled', null, ['editable' => true, 'link' => false]);
        $listMapper->add('last_login', null, [
            'format' => 'Y-m-d H:i:s'
        ]);
        $listMapper->add('creado', null, [
            'format' => 'Y-m-d H:i:s'
        ]);
        $listMapper->add('_action', null, [
            'actions' => [
                'show' => [],
                'edit' => [],
                'roles' => [
                    'template' => 'admin/administracion/list__action_roles.html.twig'
                ],
            ]
        ]);
    }

    public function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
//            ->tab('Usuario')// the tab call is optional
            ->with('Usuario', [
                'class' => 'col-md-5',
                'box_class' => 'box box-solid box-primary',
                'description' => 'Lorem ipsum',
            ])
            ->add('cuenta')
            ->add('username')
            ->add('email')
            ->add('enabled')
            ->add('last_login', null, [
                'format' => 'Y-m-d H:i:s'
            ])
            ->add('creado', null, [
                'format' => 'Y-m-d H:i:s'
            ])
            ->end()
            ->with('Persona', [
                'class' => 'col-md-7',
                'box_class' => 'box box-solid box-primary',
                'description' => 'Lorem ipsum',
            ])
            ->end();
    }

    /**
     * @param ItemInterface $menu
     * @param $action
     * @param AdminInterface|null $childAdmin
     * @return mixed|void
     */
    protected function configureSideMenu(ItemInterface $menu, $action, AdminInterface $childAdmin = null)
    {
        if (!$childAdmin && !in_array($action, ['edit', 'show'])) {
            return;
        }

        $admin = $this->isChild() ? $this->getParent() : $this;
        $id = $admin->getRequest()->get('id');

        if ($this->isGranted('LIST')) {
            $menu->addChild('Listar roles del usuario', [
                'uri' => $admin->generateUrl('admin.usuario_rol.list', ['id' => $id])
            ]);
        }
    }

    /**
     * @param $usuario
     * @throws \Exception
     */
    public function preValidate($usuario)
    {
        /** @var Usuario $usuario */
        $usuario = $this->usuario_service->setUserPassword($usuario, $usuario->getPassword());
        parent::preValidate($usuario);
    }

    /**
     * @param UsuarioService $usuario_service
     */
    public function setUsuarioService(UsuarioService $usuario_service)
    {
        $this->usuario_service = $usuario_service;
    }

    /**
     * @return UsuarioService
     */
    public function getUsuarioService()
    {
        return $this->usuario_service;
    }
}