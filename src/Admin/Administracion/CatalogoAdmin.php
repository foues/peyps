<?php

namespace App\Admin\Administracion;

use App\Entity\Administracion\Catalogo;
use App\Entity\Administracion\CatalogoTipo;
use Doctrine\ORM\EntityManager;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Form\Type\ModelType;
use Sonata\AdminBundle\Route\RouteCollection;

class CatalogoAdmin extends AbstractAdmin
{

    /**
     * @var string
     */
    protected $parentAssociationMapping = 'catalogo_tipo';

    /**
     * @param RouteCollection $collection
     */
    protected function configureRoutes(RouteCollection $collection)
    {
        if ($this->isChild()) {
            return;
        }

        // This is the route configuration as a parent
        $collection->clear();

    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        if ($this->isChild()) {
            $parent = $this->getParent();
            $id_catalogo_tipo = $parent->getRequest()->get('id');
            /** @var EntityManager $em */
            $em = $this->modelManager->getEntityManager(Catalogo::class);
            $catalogo_tipo = $em->getRepository(CatalogoTipo::class)
                ->find($id_catalogo_tipo);
            $catalogo_tipo_padre = $catalogo_tipo->getPadre();
            if ($catalogo_tipo_padre)
                $query = $em->createQueryBuilder()
                    ->select('c')
                    ->from('Administracion:Catalogo', 'c')
                    ->innerJoin('c.catalogo_tipo', 'ct')
                    ->where('ct.id = :id_catalogo_tipo')
                    ->setParameter('id_catalogo_tipo', $catalogo_tipo->getPadre());
        }

        $formMapper->add('catalogo_tipo', null, [
            'label' => 'Tipo',
            'disabled' => true,
        ]);

        if (isset($query) && $catalogo_tipo_padre) {
            $formMapper->add('padre', ModelType::class, [
                'query' => $query,
                'label' => $catalogo_tipo_padre->getNombre()
            ]);
        }

        $formMapper->add('codigo');
        $formMapper->add('nombre');
        $formMapper->add('descripcion');
    }

    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('id');
        $datagridMapper->add('codigo');
        $datagridMapper->add('nombre');
        $datagridMapper->add('catalogo_tipo');
        $datagridMapper->add('padre');
        $datagridMapper->add('descripcion');
        $datagridMapper->add('creado');
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('id');
        $listMapper->add('codigo');
        $listMapper->add('nombre');
        $listMapper->add('catalogo_tipo');
        $listMapper->add('padre');
        $listMapper->add('descripcion');
        $listMapper->add('creado', null, [
            'format' => 'Y-m-d H:i:s'
        ]);
    }
}