<?php

namespace App\Admin\Administracion;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;

class SubmenuAdmin extends AbstractAdmin
{

    /**
     * @var string
     */
    protected $parentAssociationMapping = 'padre';

    /**
     * @param RouteCollection $collection
     */
    protected function configureRoutes(RouteCollection $collection)
    {
        if ($this->isChild()) {
            return;
        }

        // This is the route configuration as a parent
        $collection->clear();

    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper->add('nombre');
        $formMapper->add('descripcion');
        $formMapper->add('url');
        $formMapper->add('icon');
        $formMapper->add('enabled');
        $formMapper->add('padre');
    }

    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('id');
        $datagridMapper->add('nombre');
        $datagridMapper->add('descripcion');
        $datagridMapper->add('url');
        $datagridMapper->add('icon');
        $datagridMapper->add('enabled');
        $datagridMapper->add('padre');
        $datagridMapper->add('creado');
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('id');
        $listMapper->add('nombre');
        $listMapper->add('descripcion');
        $listMapper->add('url');
        $listMapper->add('icon');
        $listMapper->add('enabled');
        $listMapper->add('padre');
        $listMapper->add('creado', null, [
            'format' => 'Y-m-d H:i:s'
        ]);
        $listMapper->add('_action', null, [
            'actions' => [
                'edit' => [],
                'registros' => [
                    'template' => 'admin/administracion/list__action_submenus.html.twig'
                ],
            ]
        ]);
    }
}