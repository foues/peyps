<?php

namespace App\Admin\Almacen;

use App\Entity\Almacen\UnidadMedida;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Form\Type\ModelType;
use Sonata\AdminBundle\Route\RouteCollection;

class ArticuloPresentacionAdmin extends AbstractAdmin
{

    /**
     * @var string
     */
    protected $parentAssociationMapping = 'articulo';

    /**
     * @param RouteCollection $collection
     */
    protected function configureRoutes(RouteCollection $collection)
    {
        if ($this->isChild()) {
            return;
        }

        // This is the route configuration as a parent
        $collection->clear();

    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper->add('articulo');
        $formMapper->add('unidad_medida', ModelType::class);
        $formMapper->add('subunidad_medida', ModelType::class, [
            'class' => UnidadMedida::class,
            'placeholder' => 'Ninguno',
            'required' => false,
        ]);
        $formMapper->add('cantidad');
    }

    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('id');
        $datagridMapper->add('articulo');
        $datagridMapper->add('unidad_medida');
        $datagridMapper->add('subunidad_medida');
        $datagridMapper->add('cantidad');
        $datagridMapper->add('creado');
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('id');
        $listMapper->add('articulo');
        $listMapper->add('unidad_medida');
        $listMapper->add('subunidad_medida');
        $listMapper->add('cantidad');
        $listMapper->add('creado', null, [
            'format' => 'Y-m-d H:i:s'
        ]);
        $listMapper->add('_action', null, [
            'actions' => [
                'edit' => [],
            ]
        ]);
    }
}