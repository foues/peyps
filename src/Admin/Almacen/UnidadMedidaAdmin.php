<?php

namespace App\Admin\Almacen;

use App\Entity\Almacen\UnidadMedida;
use Doctrine\ORM\EntityManager;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class UnidadMedidaAdmin extends AbstractAdmin
{
    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        /** @var EntityManager $em */
        $em = $this->modelManager->getEntityManager(UnidadMedida::class);
        $subunidades = $em->createQueryBuilder()
            ->select('um.codigo, um.nombre')
            ->from('Almacen:UnidadMedida', 'um')
            ->getQuery()
            ->getScalarResult();
        $subunidades = array_column($subunidades, 'codigo', 'nombre');

        $formMapper->add('codigo');
        $formMapper->add('nombre');
        $formMapper->add('subunidades', ChoiceType::class, [
            'choices' => $subunidades,
            'multiple' => true,
            'required' => false,
        ]);
    }

    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('id');
        $datagridMapper->add('codigo');
        $datagridMapper->add('nombre');
        $datagridMapper->add('subunidades');
        $datagridMapper->add('creado');
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('id');
        $listMapper->add('codigo');
        $listMapper->add('nombre');
        $listMapper->add('subunidadesAsString', 'string', [
            'label' => 'Subunidades'
        ]);
        $listMapper->add('creado', null, [
            'format' => 'Y-m-d H:i:s'
        ]);
        $listMapper->add('_action', null, [
            'actions' => [
                'edit' => [],
            ]
        ]);
    }
}