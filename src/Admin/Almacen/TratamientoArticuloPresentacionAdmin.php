<?php

namespace App\Admin\Almacen;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class TratamientoArticuloPresentacionAdmin extends AbstractAdmin
{
    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper->add('tratamiento');
        $formMapper->add('articulo_presentacion');
        $formMapper->add('cantidad');
    }

    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('id');
        $datagridMapper->add('tratamiento');
        $datagridMapper->add('articulo_presentacion');
        $datagridMapper->add('cantidad');
        $datagridMapper->add('creado');
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('id');
        $listMapper->add('tratamiento');
        $listMapper->add('articulo_presentacion');
        $listMapper->add('cantidad');
        $listMapper->add('creado', null, [
            'format' => 'Y-m-d H:i:s'
        ]);
        $listMapper->add('_action', null, [
            'actions' => [
                'edit' => [],
            ]
        ]);
    }

}