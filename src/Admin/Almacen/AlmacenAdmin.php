<?php

namespace App\Admin\Almacen;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class AlmacenAdmin extends AbstractAdmin
{
    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper->add('institucion');
        $formMapper->add('codigo');
        $formMapper->add('nombre');
        $formMapper->add('descripcion');
    }

    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('id');
        $datagridMapper->add('institucion');
        $datagridMapper->add('codigo');
        $datagridMapper->add('nombre');
        $datagridMapper->add('descripcion');
        $datagridMapper->add('creado');
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('id');
        $listMapper->add('institucion');
        $listMapper->add('codigo');
        $listMapper->add('nombre');
        $listMapper->add('descripcion');
        $listMapper->add('creado', null, [
            'format' => 'Y-m-d H:i:s'
        ]);
        $listMapper->add('_action', null, [
            'actions' => [
                'edit' => [],
            ]
        ]);
    }
}