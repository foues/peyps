<?php

namespace App\Admin\Academico;

use App\Entity\Academico\Curso;
use App\Entity\Academico\PlanEstudio;
use App\Entity\Academico\PlanEstudioCiclo;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Form\Type\ModelType;

class PlanEstudioCursoAdmin extends AbstractAdmin
{
    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper->add('plan_estudio', ModelType::class, [
            'class' => PlanEstudio::class,
            'property' => 'carrera.nombre'
        ]);
        $formMapper->add('curso', ModelType::class, [
            'class' => Curso::class
        ]);
        $formMapper->add('plan_estudio_ciclo', ModelType::class, [
            'class' => PlanEstudioCiclo::class
        ]);
    }

    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('id');
        $datagridMapper->add('curso');
        $datagridMapper->add('plan_estudio.carrera');
        $datagridMapper->add('plan_estudio_ciclo');
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('id');
        $listMapper->addIdentifier('plan_estudio.carrera');
        $listMapper->addIdentifier('curso');
        $listMapper->addIdentifier('plan_estudio_ciclo');
        $listMapper->addIdentifier('creado', null, [
            'format' => 'Y-m-d H:i:s'
        ]);
    }
}