<?php

namespace App\Admin\Academico;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class EstudianteEstadoAdmin extends AbstractAdmin
{
    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper->add('tipo', ChoiceType::class, [
            'choices' => ['Plan de estudio' => 'plan_estudio', 'Curso' => 'curso']
        ]);
        $formMapper->add('codigo');
        $formMapper->add('nombre');
        $formMapper->add('descripcion');
    }

    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('id');
        $datagridMapper->add('tipo');
        $datagridMapper->add('codigo');
        $datagridMapper->add('nombre');
        $datagridMapper->add('descripcion');
        $datagridMapper->add('creado');
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('id');
        $listMapper->addIdentifier('tipo');
        $listMapper->addIdentifier('codigo');
        $listMapper->addIdentifier('nombre');
        $listMapper->addIdentifier('descripcion');
        $listMapper->addIdentifier('creado', null, [
            'format' => 'Y-m-d H:i:s'
        ]);
    }
}