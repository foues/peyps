<?php

namespace App\Admin\Academico;

use App\Entity\Academico\PeriodoCiclo;
use App\Entity\Academico\PlanEstudioCurso;
use App\Entity\Rotacion\Programa;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Form\Type\ModelType;

class UnidadIntegracionAdmin extends AbstractAdmin
{
    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper->add('plan_estudio_curso', ModelType::class, [
            'class' => PlanEstudioCurso::class,
//            'property' => 'carrera.nombre'
        ]);
        $formMapper->add('programa', ModelType::class, [
            'class' => Programa::class
        ]);
        $formMapper->add('periodo_ciclo', ModelType::class, [
            'class' => PeriodoCiclo::class
        ]);
        $formMapper->add('nombre');
        $formMapper->add('ponderacion');
    }

    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('id');
        $datagridMapper->add('plan_estudio_curso');
        $datagridMapper->add('programa');
        $datagridMapper->add('periodo_ciclo');
        $datagridMapper->add('nombre');
        $datagridMapper->add('ponderacion');
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('id');
        $listMapper->add('plan_estudio_curso');
        $listMapper->add('programa');
        $listMapper->add('periodo_ciclo');
        $listMapper->add('nombre');
        $listMapper->add('ponderacion');
        $listMapper->add('creado', null, [
            'format' => 'Y-m-d H:i:s'
        ]);
        $listMapper->add('_action', null, [
            'actions' => [
                'edit' => [],
            ]
        ]);
    }
}