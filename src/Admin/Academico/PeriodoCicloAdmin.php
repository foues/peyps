<?php

namespace App\Admin\Academico;

use App\Entity\Academico\Periodo;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Form\Type\ModelType;

use Symfony\Component\Form\Extension\Core\Type\ChoiceType;


class PeriodoCicloAdmin extends AbstractAdmin
{
    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper->add('periodo', ModelType::class, [
            'class' => Periodo::class
        ]);
        $formMapper->add('tipo', ChoiceType::class, [
            'choices' => [
                'Par' => 'Par',
                'Impar' => 'Impar',
            ],
            'placeholder' => 'Seleccione una opción...',
            'required' => true
        ]);
    }

    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('id');
        $datagridMapper->add('periodo');
        $datagridMapper->add('tipo');
        $datagridMapper->add('creado');
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('id');
        $listMapper->addIdentifier('periodo');
        $listMapper->addIdentifier('tipo');
        $listMapper->addIdentifier('periodo.inicio', null, [
            'format' => 'Y-m-d'
        ]);
        $listMapper->addIdentifier('periodo.fin', null, [
            'format' => 'Y-m-d'
        ]);
        $listMapper->addIdentifier('creado', null, [
            'format' => 'Y-m-d H:i:s'
        ]);
    }


}