<?php

namespace App\Admin\Academico;

use App\Entity\Academico\PlanEstudioCurso;
use App\Entity\Rotacion\Programa;
use Doctrine\ORM\EntityManager;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Form\Type\ModelType;

class CursoProgramaAdmin extends AbstractAdmin
{
    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        /** @var EntityManager $em */
        $em = $this->modelManager->getEntityManager(PlanEstudioCurso::class);
        $query = $em->createQueryBuilder()
            ->select('pec, pe, c')
            ->from('Academico:PlanEstudioCurso', 'pec')
            ->innerJoin('pec.plan_estudio', 'pe')
            ->innerJoin('pec.curso', 'c');

        $formMapper->add('plan_estudio_curso', ModelType::class, [
            'class' => PlanEstudioCurso::class,
            'query' => $query,
        ]);
        $formMapper->add('programa', ModelType::class, [
            'class' => Programa::class
        ]);
    }

    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('id');
        $datagridMapper->add('plan_estudio_curso.curso');
        $datagridMapper->add('programa');
        $datagridMapper->add('creado');
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('id');
        $listMapper->addIdentifier('plan_estudio_curso.curso');
        $listMapper->addIdentifier('programa');
        $listMapper->addIdentifier('creado', null, [
            'format' => 'Y-m-d H:i:s'
        ]);
    }
}