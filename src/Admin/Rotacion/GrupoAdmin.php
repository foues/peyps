<?php

namespace App\Admin\Rotacion;

use App\Entity\Academico\PeriodoCiclo;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Form\Type\ModelType;


class GrupoAdmin extends AbstractAdmin
{
    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper->add('periodo_ciclo', ModelType::class,['class' => PeriodoCiclo::class])
                   ->add('nombre')
                   ->add('rotable');
    }

    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('id');
        $datagridMapper->add('periodo_ciclo');
        $datagridMapper->add('nombre');
        $datagridMapper->add('rotable');
        $datagridMapper->add('creado');
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('id');
        $listMapper->addIdentifier('periodo_ciclo');
        $listMapper->addIdentifier('nombre');
        $listMapper->addIdentifier('rotable');
        $listMapper->addIdentifier('creado', null, [
            'format' => 'Y-m-d H:i:s'
        ]);
    }
}