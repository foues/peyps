<?php

namespace App\Admin\Rotacion;

use Knp\Menu\ItemInterface;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Admin\AdminInterface;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class ProgramaAdmin extends AbstractAdmin
{
    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper->add('codigo');
        $formMapper->add('nombre');
        $formMapper->add('descripcion');
    }

    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('id');
        $datagridMapper->add('codigo');
        $datagridMapper->add('nombre');
        $datagridMapper->add('descripcion');
        $datagridMapper->add('creado');
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('id');
        $listMapper->addIdentifier('codigo');
        $listMapper->addIdentifier('nombre');
        $listMapper->addIdentifier('descripcion');
        $listMapper->addIdentifier('creado', null, [
            'format' => 'Y-m-d H:i:s'
        ]);
    }

    /**
     * @param ItemInterface $menu
     * @param $action
     * @param AdminInterface|null $childAdmin
     * @return mixed|void
     */
    protected function configureSideMenu(ItemInterface $menu, $action, AdminInterface $childAdmin = null)
    {
        if (!$childAdmin && !in_array($action, ['edit', 'show'])) {
            return;
        }

        $admin = $this->isChild() ? $this->getParent() : $this;
        $id = $admin->getRequest()->get('id');

        if ($this->isGranted('LIST')) {
            $menu->addChild('Listar instituciones del programa', [
                'uri' => $admin->generateUrl('admin.programa_institucion.list', ['id' => $id])
            ]);
        }
    }

}