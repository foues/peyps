<?php

namespace App\Admin\Rotacion;


use App\Entity\Academico\PeriodoCiclo;
use App\Entity\Rotacion\Grupo;
use App\Entity\Rotacion\Programa;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Form\typeFormMapper;
use Sonata\AdminBundle\Form\Type\ModelType;
use Symfony\Component\Form\Extension\Core\Type\DateType;


class ProgramaCalendarioAdmin extends AbstractAdmin
{
    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {

        $query = $this->modelManager->getEntityManager('Rotacion:Grupo')
                                    ->createQueryBuilder()
            ->add('select', 'u')
            ->add('from', 'Rotacion:Grupo u')
            ->add('where','u.rotable = 1');



        $formMapper->add('programa', ModelType::class,['class' => Programa::class])
                   ->add('periodoCiclo', ModelType::class,['class' => PeriodoCiclo::class])
                   ->add('grupo', ModelType::class,['class' => Grupo::class, 'query'=> $query])
                   ->add('tipo')
                   ->add('fecha' ,DateType::class,array('widget' => 'single_text','format' =>'yyyy-MM-dd'))
                   ->add('descripcion')
        ;
    }

    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('id');
        $datagridMapper->add('programa');
        $datagridMapper->add('grupo');
        $datagridMapper->add('tipo');
        $datagridMapper->add('fecha');
        $datagridMapper->add('descripcion');
        $datagridMapper->add('creado');
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('id');
        $listMapper->addIdentifier('programa');
        $listMapper->addIdentifier('grupo');
        $listMapper->addIdentifier('tipo');
        $listMapper->addIdentifier('fecha');
        $listMapper->addIdentifier('descripcion');
        $listMapper->addIdentifier('creado', null, [
            'format' => 'Y-m-d H:i:s'
        ]);
    }
}