<?php
/**
 * Created by PhpStorm.
 * User: Dell
 * Date: 14/07/2018
 * Time: 20:30
 */

namespace App\Service;


use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use App\Service\PeriodoService;
use App\Entity\Rotacion\Grupo;
use App\Entity\Rotacion\GrupoEstudiante;

class DqlRotacionUtilService
{
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var RotacionService
     */
    private $rotacion_service;


    private  $periodo_service;

    /**
     * RotacionService constructor.
     * @param EntityManager $em
     */
    public function __construct(EntityManagerInterface $em, RotacionService $rotacion_service,PeriodoService $periodo_service)
    {
        $this->em = $em;
        $this->rotacion_service = $rotacion_service;
        $this->periodo_service = $periodo_service;
    }

    /**
     * retorna un array de objertos ProgramaInstitucion
     * @return ProgramaInstitucion
     */
    public function getCuposInstitucion()
    {
        return $this->em->createQuery(
            "SELECT p,pr,ins FROM Rotacion:ProgramaInstitucion p
                INNER JOIN p.programa pr
                INNER JOIN p.institucion ins 
            ")->getResult();
    }

    /**
     * @param int|null $periodoCiclo
     * @param int|null $estuCurso id del estudiante Plan de estudidos
     * @param int $estuGrupo nombre del grupo
     * @param string|null $curso
     * @param string|null $materia nomres de las materias "'OPC117'"
     * @return mixed $curso vaidacion del curso puede ser != o = y valor  Eje: "!= 'EST117"
     * @throws \Exception
     */
  public function getGrupoEstudiante(int $periodoCiclo = null, int $estuCurso = null,int $estuGrupo = null, string $curso = null, String $materia = null, String $id =null )
    {
        if (is_null($periodoCiclo)) $periodoCiclo = $this->periodo_service->getCicloActual()->getid();


                $query=  'SELECT p,cr,estc,estuPlan,estuDian  FROM Rotacion:GrupoEstudiante p
                INNER JOIN  p.grupo cr       
                INNER JOIN  p.estudiante_curso estc       
                INNER JOIN  estc.curso cu       
                INNER JOIN estc.estudiante_estado  c       
                INNER JOIN  estc.estudiante_plan_estudio estuPlan       
                INNER JOIN  estuPlan.estudiante estuDian   
                where cr.periodo_ciclo =' . $periodoCiclo . ' and 
                  c.tipo =\'curso\' and c.codigo =\'estudiante\' ';

         if(!is_null($id))$query = $query.' and p.id ='.$id;
         if(!is_null($estuCurso))$query = $query.' and p.estudiante_curso ='.$estuCurso;
         if(!is_null($estuGrupo))$query = $query.' and cr.nombre ='.$estuGrupo;
         if(!is_null($curso))$query = $query.' and cu.codigo'.$curso.'\'';
         if(!is_null($materia))$query = $query.' and cu.codigo IN (\''.$materia.'\') ';

       $query = $query.' order by  p.grupo,p.estudiante_curso';
//        print( "<br><br>" );
//        print( $materia. "   0000  " );
//        print( "<br><br>" );
//         dd( $id, $this->em->createQuery($query)->getSQL() );
        return(  $this->em->createQuery($query)->getResult() );
    }


    /** se optienelos estuiantes rotables del PERIODO DE TIEMPO DEL CICLO ACTUAL!!!!!
     * @param int|null $periodoCiclo
     * @param String|null $materia
     * @return mixed
     * @throws \Exception
     */
    public function estudianteRotables(int $periodoCiclo = null, String $materia = null)
    {
        if (is_null($periodoCiclo)) $periodoCiclo = $this->periodo_service->getCicloActual()->getid();

        $query =
            'SELECT est,c,pl,estu FROM Academico:EstudianteCurso est 
               inner join est.estudiante_estado c    
               inner join est.estudiante_plan_estudio pl    
               inner join pl.estudiante estu   
               inner join est.curso cu  
                where   c.tipo =\'curso\' and c.codigo =\'estudiante\' 
                and est.periodo_ciclo ='.$periodoCiclo  ;


        if(!is_null($materia))$query = $query.' and cu.codigo IN (\''.$materia.'\') ';
                 $query = $query. ' order by pl.cum DESC';


        return ($this->em->createQuery($query)->getResult());
    }

    /**Obtiene la rotacion (fechas de inicio y fin ) de un ciclo una OficilaRotacion
     * @param int $periodoCicloID
     * @return mixed
     * @throws \Exception
     */
   public function getRotacion($periodoCicloID = null,$nombre = null)
    {
        if (is_null($periodoCicloID)) $periodoCicloID = $this->periodo_service->getCicloActual()->getid();

        $query = 'SELECT ro,g,h FROM Rotacion:Rotacion ro
             inner join ro.grupo g
             inner join g.padre h
             where g.periodo_ciclo =' . $periodoCicloID . ' ';

        if(!is_null($nombre))$query = $query.' and ro.numero='.$nombre. ' ';

        return($this->em->createQuery($query)->getResult());
//        return($this->em->createQuery($query)->setMaxResults(1)->getOneOrNullResult());
    }

    /**guarda un arrelgo de entidades
     * @param array $arrayEntities
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function saveArray(array $arrayEntities){

        foreach ($arrayEntities as  $value) {
            $this->em->persist($value);
        }
        $this->em->flush();
    }

    /**retorna un arreglo modificado que contiene todos los cupos y obejtos necesarios para rotar
     * @return mixed
     */
    public function getCuposInstitucionConsolidado()
    {

        foreach ($this->getCuposInstitucion() as $value) {
            $programa = $value->getPrograma()->getCodigo();
            $cantidEstuPrograma = $value->getCantidadEstudiante();
            $nombreIns = $value->getInstitucion()->getnombre();

            // le pongo los vealores dependiento del programa intitucion
            $mapaCupos[$programa]["Institucion"][$nombreIns]["cupo"] = $cantidEstuPrograma;
            $mapaCupos[$programa]["Institucion"][$nombreIns]["ProgramaInstitucion"] = $value;
// saco los valores totales o sumas necesarios
            if (isset ($mapaCupos[$programa]["cantidadTotal"]))
                $mapaCupos[$programa]["cantidadTotal"] += $cantidEstuPrograma;
            else $mapaCupos[$programa]["cantidadTotal"] = $mapaCupos[$programa]["Institucion"][$nombreIns]["cupo"];
        }
        return $mapaCupos;
    }


    /**
     * mandar el id del periodo, si en caso se envia vacio se tomara el actual
     * @param int $periodoCicloID
     * @return bool
     * @throws \Exception
     */
    public function existeRotacionEnPeriodo($periodoCicloID = null)
    {
        $resultado = false;
        if($periodoCicloID== null)$periodoCicloID =  $this->periodo_service->getCicloActual()->getid();
        $resultadoSQL= ($this->em->createQuery(
            'SELECT re FROM Rotacion:RotacionEstudiante re
            inner join re.grupo_estudiante ge
            inner join ge.grupo g
            where g.periodo_ciclo ='.$periodoCicloID.' '

        )->setMaxResults(1)->getResult());

        if(count($resultadoSQL)!=0){$resultado=true;}

        return ($resultado);
    }

    /**
     * mandar el id del periodo, si en caso se envia vacio se tomara el actual
     * @param int $periodoCicloID
     * @return bool
     * @throws \Exception
     */
    public function existeGrupoEnPeriodo($periodoCicloID = null)
    {
        $resultado = false;
        if($periodoCicloID== null)$periodoCicloID =  $this->periodo_service->getCicloActual()->getId();
        $resultadoSQL= ($this->em->createQuery(
            'SELECT ro FROM Rotacion:Rotacion ro
            inner join ro.grupo g
            where g.periodo_ciclo ='.$periodoCicloID.' '
        )->setMaxResults(1)->getResult());
        if(count($resultadoSQL)!=0){$resultado=true;}
        return ($resultado);
    }

     public function existeGrupoEstudianteEnPeriodo($periodoCicloID = null)
    {
        $resultado = false;
        if($periodoCicloID == null)$periodoCicloID =  $this->periodo_service->getCicloActual()->getId();
        $resultadoSQL= ($this->em->createQuery(
            'SELECT grup FROM Rotacion:GrupoEstudiante grup, Rotacion:Grupo c
                where grup.grupo = c.id and c.periodo_ciclo =' . $periodoCicloID
        )->setMaxResults(1)->getResult());
        if(count($resultadoSQL)!=0){$resultado=true;}
        return ($resultado);
    }

    /**
     * @param null $periodoCicloID
     * @param null $grupo_estudiante
     * @param null $programa_institucion
     * @param null $id_estudiante
     * @param null $id_programa
     * @param null $id_rotacion
     * @return array
     * @throws \Doctrine\DBAL\DBALException
     */
    public function  getRotacionConsolidad($periodoCicloID = null, $grupo_estudiante= null, $programa_institucion= null,$id_estudiante=null,$id_programa=null,$id_rotacion=null,$curso = null,$groupBY=null){
        if(is_null($periodoCicloID))$periodoCicloID =  $this->periodo_service->getCicloActual()->getid();
        $db = $this->em->getConnection();
        $query = "SELECT 
     rotacion.rotacion.id AS idRotacion
    ,estudiante.id  AS idEstudiante
    ,grupo_estudiante.id AS idGrupoEstudiante
    ,programa_institucion.id AS programaInstitucion
    ,rotacion.rotacion.inicio
    ,rotacion.rotacion.fin
    ,academico.estudiante.carnet
    ,rotacion.grupo.nombre AS nombreGrupo
    ,rotacion.grupo.id_periodo_ciclo AS periodoCiclo
    ,academico.periodo.nombre nombre_ciclo
    ,academico.estudiante_plan_estudio.cum
    ,rotacion.programa_institucion.cantidad_estudiante
    ,administracion.institucion.nombre AS nombreInstitucion
    ,administracion.institucion.id AS idInstitucion
    ,rotacion.programa.nombre AS nombrePrograma
    ,rotacion.programa.codigo AS codigoPrograma
    ,academico.curso.codigo AS cursoCodigo
    ,academico.curso.nombre AS cursoNombre
    ,CONCAT(administracion.persona.nombre ,\" \",administracion.persona.apellido) AS nombreCompleto
   FROM rotacion.rotacion_estudiante   
    INNER JOIN  rotacion.grupo_estudiante ON (rotacion.rotacion_estudiante.id_grupo_estudiante = rotacion.grupo_estudiante.id)
    INNER JOIN  rotacion.rotacion ON (rotacion.rotacion_estudiante.id_rotacion = rotacion.rotacion.id)
    INNER JOIN  rotacion.grupo ON (rotacion.grupo_estudiante.id_grupo = grupo.id)
    INNER JOIN  academico.estudiante_curso ON (grupo_estudiante.id_estudiante_curso = estudiante_curso.id)
    INNER JOIN  academico.estudiante_plan_estudio ON (estudiante_curso.id_estudiante_plan_estudio = estudiante_plan_estudio.id)
    INNER JOIN  academico.estudiante ON (estudiante_plan_estudio.id_estudiante = estudiante.id)
    INNER JOIN  rotacion.programa_institucion ON (rotacion.rotacion_estudiante.id_programa_institucion = programa_institucion.id)
    INNER JOIN  administracion.institucion ON (programa_institucion.id_institucion = institucion.id)
    INNER JOIN  rotacion.programa ON (rotacion.programa.id = rotacion.programa_institucion.id_programa)
    INNER JOIN  administracion.persona ON (persona.id = estudiante.id_persona)
    INNER JOIN  academico.periodo_ciclo ON (periodo_ciclo.id = grupo.id_periodo_ciclo)
    INNER JOIN  academico.periodo ON (periodo_ciclo.id_periodo = periodo.id)
    INNER JOIN  academico.curso ON  curso.id = estudiante_curso.id_curso
    WHERE grupo.id_periodo_ciclo =".$periodoCicloID;

        if(!is_null($grupo_estudiante))$query = $query.' and grupo_estudiante.id ='.$grupo_estudiante;
        if(!is_null($programa_institucion ))$query =$query. ' and programa_institucion.id ='.$programa_institucion;
        if(!is_null($id_estudiante ))$query =$query. ' and estudiante.id ='.$id_estudiante;
        if(!is_null($id_programa ))$query =$query. " and programa.codigo ='".$id_programa."' ";
        if(!is_null($id_rotacion ))$query =$query. ' and rotacion.id ='.$id_rotacion;
        if(!is_null($curso ))$query =$query. "  and curso.codigo ='" .$curso."' ";
        if(!is_null($groupBY ))$query =$query.  '  GROUP BY  '.$groupBY;
        //dd($query);
        $stmt = $db->prepare($query);
        $params = array();
        $stmt->execute();
        $rotacionEstudianteLocal = $stmt->fetchAll();
        return ($rotacionEstudianteLocal);
    }


    /**
     * Ejecuta una consulta nativa con solo enviarle la query y permite agregarle parametros
     * @param String $query
     * @param array $params
     * @param int $periodoCicloID
     * @return array
     * @throws \Doctrine\DBAL\DBALException
     */
    public function executeNativeSql(String $query,Array $params=[]){

        $db = $this->em->getConnection();
        $stmt = $db->prepare($query);

        $stmt->execute($params);
        $rotacionEstudianteLocal = $stmt->fetchAll();
        return ($rotacionEstudianteLocal);


    }

    public function getRotacionEstudiante(int $periodoCiclo = null, int $estuPlan = null)
    {
        if (is_null($periodoCiclo)) $periodoCiclo = $this->periodo_service->getCicloActual()->getid();

                $query=  'SELECT p,cr,estc,estuPlan,estuDian  FROM Rotacion:GrupoEstudiante p
                INNER JOIN  p.grupo cr       
                INNER JOIN  p.estudiante_curso estc       
                INNER JOIN estc.estudiante_estado  c       
                INNER JOIN  estc.estudiante_plan_estudio estuPlan       
                INNER JOIN  estuPlan.estudiante estuDian   
                where cr.periodo_ciclo =' . $periodoCiclo . ' and 
                  c.tipo =\'curso\' and c.codigo =\'estudiante\'';

        if(!is_null($estuPlan))$query = $query.' and estuPlan.estudiante ='.$estuPlan;

        $query = $query.' order by  p.grupo,p.estudiante_curso';
        return(  $this->em->createQuery($query)->getResult() );
    }


    /**
     * Modulo que obtiene las materias impartidas  en un ciclo especifico
     * @param int|null $periodoCiclo
     * @return mixed
     * @throws \Exception
     */
    public function getCursoPeridoCiclo(int $periodoCiclo = null)
    {
        if (is_null($periodoCiclo)) $periodoCiclo = $this->periodo_service->getCicloActual()->getid();

        $query='SELECT cp,pec,cur  FROM Academico:CursoPrograma cp
                INNER JOIN  cp.plan_estudio_curso pec       
                INNER JOIN  pec.curso cur       
                INNER JOIN  cp.programa  c       
                INNER JOIN  Academico:EstudianteCurso ec 
                WITH  cur.id = ec.curso 
                WHERE ec.periodo_ciclo ='.$periodoCiclo.'
                GROUP BY cur.nombre';

//        if(!is_null($estuPlan))$query = $query.' and estuPlan.estudiante ='.$estuPlan; por si se necesita mas parametros

        //$query = $query.' order by  p.grupo,p.estudiante_curso';
        $CursoPrograma= $this->em->createQuery($query)->getResult();

        foreach ($CursoPrograma as $key => $value) {
                $curso[] = $value->getPlanEstudioCurso()->getCurso();
        }
        return($curso);
    }
//
//where cr.periodo_ciclo =' . $periodoCiclo . ' and
//c.tipo =\'curso\' and c.codigo =\'estudiante\'

    /** Optiene los estudiante listos para rotar Sin repetir ninguno de ellos
     * @param int|null $periodoCiclo
     * @param int|null $estuPlan
     * @param int|null $estuGrupo
     * @param string|null $curso
     * @param String|null $materia
     * @param String|null $id
     * @return mixed
     * @throws \Exception
     */
    public function getEstudiantesRotablesUnicos(int $periodoCiclo = null, int $estuPlan = null,int $estuGrupo = null, string $curso = null, String $materia = null, String $id =null )
    {
        if (is_null($periodoCiclo)) $periodoCiclo = $this->periodo_service->getCicloActual()->getid();


        $query=  'SELECT p,cr,estc,estuPlan,estuDian  FROM Rotacion:GrupoEstudiante p
                INNER JOIN  p.grupo cr       
                INNER JOIN  p.estudiante_curso estc       
                INNER JOIN  estc.curso cu       
                INNER JOIN estc.estudiante_estado  c       
                INNER JOIN  estc.estudiante_plan_estudio estuPlan       
                INNER JOIN  estuPlan.estudiante estuDian   
                where cr.periodo_ciclo =' . $periodoCiclo . ' and 
                  c.tipo =\'curso\' and c.codigo =\'estudiante\' ';

        if(!is_null($id))$query = $query.' and p.id ='.$id;
        if(!is_null($estuPlan))$query = $query.' and estuPlan.estudiante ='.$estuPlan;
        if(!is_null($estuGrupo))$query = $query.' and cr.nombre ='.$estuGrupo;
        if(!is_null($curso))$query = $query.' and cu.codigo'.$curso.'\'';
        if(!is_null($materia))$query = $query.' and cu.codigo IN (\''.$materia.'\') ';

        $query = $query.' GROUP BY estuDian.carnet   order by  p.grupo,p.estudiante_curso';
//        print( "<br><br>" );
//        print( $materia. "   0000  " );
//        print( "<br><br>" );
//       dd( $id, $this->em->createQuery($query)->getSQL() );
        return(  $this->em->createQuery($query)->getResult() );
    }


    public function EliminarRot(int $periodoCiclo = null)
    {
        if (is_null($periodoCiclo)) $periodoCiclo = $this->periodo_service->getCicloActual()->getid();

        $db = $this->em->getConnection();

        $query = ("SELECT  rotacion.rotacion_estudiante.id AS idRotacionEstudiante,
        rotacion.rotacion.id AS idRotacion,
        rotacion.grupo_estudiante.id AS idGrupoEstudiante 
        FROM
        rotacion.rotacion_estudiante 
        INNER JOIN  rotacion.rotacion  
            ON ( rotacion_estudiante . id_rotacion  =  rotacion . id )
        INNER JOIN rotacion.grupo_estudiante 
            ON (rotacion_estudiante.id_grupo_estudiante = grupo_estudiante.id)
        INNER JOIN rotacion.grupo 
            ON (rotacion.id_grupo = grupo.id) 
        INNER JOIN academico.periodo_ciclo
            ON (grupo.id_periodo_ciclo = periodo_ciclo.id)
        WHERE periodo_ciclo.id = ".$periodoCiclo);
        $stmt = $db->prepare($query);
        $stmt->execute();
        $arrayToDelete = $stmt->fetchAll();
//dd($arrayToDelete);

    foreach ( $arrayToDelete as $ToDelete){

        $db->prepare("DELETE FROM rotacion.rotacion_estudiante where id = ".$ToDelete["idRotacionEstudiante"].";")->execute();
    }foreach ( $arrayToDelete as $ToDelete){

        $db->prepare("DELETE FROM rotacion.rotacion  where id = ".$ToDelete["idRotacion"].";")->execute();
    }foreach ( $arrayToDelete as $ToDelete){

        $db->prepare("DELETE FROM rotacion.grupo_estudiante  where id = ".$ToDelete["idGrupoEstudiante"].";")->execute();
    }


     //   $db->prepare("DELETE FROM rotacion.grupo WHERE calificable = 1 and id_periodo_ciclo = ".$periodoCiclo." ;")->execute();

        return 0 ;

    }


    public function CrearSubGruposPPE(int $periodoCiclo = null)
    {
        if (is_null($periodoCiclo)) $periodoCiclo = $this->periodo_service->getCicloActual()->getid();
        $db = $this->em->getConnection();

        $CursosDeCicloPerido = $this->getCursoPeridoCiclo();
        foreach ($CursosDeCicloPerido as $keyCDP => $valueCDP) {

         $materias = $valueCDP->getId();
//dd($CursosDeCicloPerido);

        $query= 'Select re,ge,gr from Rotacion:RotacionEstudiante re
        INNER JOIN re.grupo_estudiante ge
        INNER JOIN ge.grupo gr
        INNER JOIN ge.estudiante_curso ec
        where gr.nombre= 7 and
        ec.curso  ='. $materias .' and  
        gr.periodo_ciclo ='. $periodoCiclo ;

        $RotacionEstudiante =   $this->em->createQuery($query)->getResult() ;
        $instituRotacioEstudia = null;

        //Organizo un arreglo con las instituciones que continenen estudiantes, para luego hacer un registro de grupo por cada una
        foreach ($RotacionEstudiante as $keymapRE => $valueRE) {
            $GrupEstuCodiInstitu = $valueRE->getProgramaInstitucion()->getInstitucion()->getCodigo();
            $instituRotacioEstudia[$GrupEstuCodiInstitu][] = $valueRE;
        }
        //dd($instituRotacioEstudia);
//++++++++++++++++++++++++++++++Guarda en la base a partir de las instituciones ubicadas en el segnmento anterior
        $count = 0;
        foreach ($instituRotacioEstudia as $keymapGIT => $valueGIT) {
            $count++;
            $GrupEstuCodiCurso = $valueGIT[0]->getGrupoEstudiante()->getEstudianteCurso()->getCurso()->getCodigo();
            $InstituEstuNombre = $valueGIT[0]->getProgramaInstitucion()->getInstitucion ()->getnombre();
            $GrupEstuCodigrupo = $valueGIT[0]->getGrupoEstudiante()->getGrupo();
            $nombreGrupo = substr($GrupEstuCodiCurso, 0, 20) . "/G". $count. " - ".$InstituEstuNombre ;

//            dd($keymapGIT,$valueGIT,$instituRotacioEstudia, $nombreGrupo);
            $grupoPPE = new Grupo($GrupEstuCodigrupo, $this->periodo_service->getCicloActual(), $nombreGrupo, false,true);
            $grupoPPE->setCalificable(true);
            $gruposPPEfinales[] = $grupoPPE;
//        dd($valueGIT);

//+++++++++++++++++++++++++++++++++++++++++++segemento donde guardo en grupo_estudiante
// id_estudiante_curso  id_grupo
            foreach ($valueGIT as $keymapGRE => $valueGRE) {
//              ya tenemos al grupo $grupoPPE
                $GrupEstuCurso = $valueGRE->getGrupoEstudiante()->getEstudianteCurso();

                $GrupoConEstudiante[] = new GrupoEstudiante($GrupEstuCurso, $grupoPPE, new \DateTime('now'));
            }
//-Fin  ----------  grupo_estudiante-------------------------------
        }
//        dd($GrupoConEstudiante);
       $this->saveArray($gruposPPEfinales);// primero se deben de guardar el grupo
        $this->saveArray($GrupoConEstudiante);// primero se deben de guardar el grupo
//- Fin----------------------------------------------------------------------------------------------------
        }

    }






}