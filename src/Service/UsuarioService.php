<?php

namespace App\Service;

use App\Entity\Administracion\Cuenta;
use App\Entity\Administracion\Rol;
use App\Entity\Administracion\Usuario;
use App\Entity\Administracion\UsuarioRol;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\User\User;
use Symfony\Component\Security\Core\User\UserInterface;

class UsuarioService
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var UserPasswordEncoderInterface
     */
    private $encoder;

    /**
     * @var string
     */
    private $random_password;

    /**
     * @var string
     */
    private $email_domain = 'ues.edu.sv';

    /**
     * UsuarioService constructor.
     * @param EntityManagerInterface $em
     * @param UserPasswordEncoderInterface $encoder
     * @throws \Exception
     */
    public function __construct(EntityManagerInterface $em, UserPasswordEncoderInterface $encoder)
    {
        $this->em = $em;
        $this->encoder = $encoder;
    }

    /**
     * @param string $username
     * @return mixed|null|\Symfony\Component\Security\Core\User\UserInterface
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function loadUserByUsername($username)
    {
        return $this->em->getRepository(Usuario::class)->loadUserByUsername($username);
    }

    /**
     * @param $username
     * @return Usuario|UserInterface|null
     * @throws \Exception
     */
    public function getUserByUsername($username)
    {
        $usuario = $this->loadUserByUsername($username);

        if (!$usuario) {
            $usuario = $this->createUserFromUsername(strtolower($username));
        }

        return $usuario;
    }

    /**
     * @param User $user
     * @return Usuario
     * @throws \Exception
     */
    public function createUserFromCoreUser(User $user)
    {
        $cuenta = new Cuenta();
        $this->em->persist($cuenta);

        $usuario = new Usuario();
        $username = $user->getUsername();
        $email = $user->getUsername() . '@' . $this->email_domain;
        $password = $this->encoder->encodePassword($usuario, random_bytes(10));

        $usuario
            ->setCuenta($cuenta)
            ->setUsername($username)
            ->setEmail($email)
            ->setPassword($password);
        $this->em->persist($usuario);

        $rol = $this->em->getRepository(Rol::class)->findOneBy(['codigo' => 'ROLE_USER']);
        $usuario_rol = new UsuarioRol();
        $usuario_rol->setUsuario($usuario);
        $usuario_rol->setRol($rol);
        $this->em->persist($usuario_rol);

        $this->em->flush();
        $this->em->refresh($usuario);

        return $usuario;
    }

    /**
     * @param string $username
     * @return Usuario
     * @throws \Exception
     */
    public function createUserFromUsername($username)
    {
        $cuenta = new Cuenta();
        $this->em->persist($cuenta);

        $usuario = new Usuario();
        $email = $username . '@' . $this->email_domain;
        $password = $this->getRandomPassword();

        $usuario
            ->setCuenta($cuenta)
            ->setUsername($username)
            ->setEmail($email)
            ->setPassword($password);

        $this->em->persist($usuario);

        return $usuario;
    }

    /**
     * @return string
     * @throws \Exception
     */
    private function getRandomPassword()
    {
        if (!$this->random_password) {
            $_usuario = new Usuario();
            $this->random_password = $this->encoder->encodePassword($_usuario, random_bytes(10));
        }

        return $this->random_password;
    }

    /**
     * @param Usuario $usuario
     * @param $plain_password
     * @param bool $random
     * @return Usuario
     * @throws \Exception
     */
    public function setUserPassword(Usuario $usuario, $plain_password = null, $random = false)
    {
        if ($plain_password) {
            $password = $this->encoder->encodePassword($usuario, $plain_password);
        } else {
            /** @var \Doctrine\ORM\EntityManager $em */
            $em = $this->em;
            if ($usuario->getId() && !$random) {
                $password = $em->getRepository(Usuario::class)->findPasswordByUser($usuario);
            }
            if (!isset($password) || $random) {
                $password = $this->getRandomPassword();
            }
        }
        $usuario->setPassword($password);

        return $usuario;
    }

    /**
     * @param Usuario $usuario
     * @param array $roles
     */
    public function setUserRoles(Usuario $usuario, array $roles)
    {
        $roles_diff = $this->em->getRepository(Usuario::class)
            ->findRolesDiffByUser($usuario, $roles);

        foreach ($roles_diff as $rol) {
            $usuario_rol = (new UsuarioRol())
                ->setUsuario($usuario)
                ->setRol($rol);
            $this->em->persist($usuario_rol);
        }
    }

    /**
     * @param UserInterface $usuario
     * @param string $permiso
     * @return bool
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function isGranted(UserInterface $usuario, $permiso)
    {
        $granted = $this->em->getRepository(Usuario::class)
            ->findPermissionByUser($usuario, $permiso);

        return $granted ? true : false;
    }
}