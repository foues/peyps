<?php

namespace App\Service;


use App\Entity\Rotacion\Planificacion;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Rotacion\ProgramaCalendario;
use App\Entity\Rotacion\Grupo;

class RotacionService
{
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var PeriodoService
     */
    private $periodo_service;

    /**
     * RotacionService constructor.
     * @param EntityManagerInterface $em
     * @param PeriodoService $periodo_service
     */
    public function __construct(EntityManagerInterface $em, PeriodoService $periodo_service= null )
    {
        $this->em = $em;
        if(!is_null($periodo_service))$this->periodo_service = $periodo_service;
    }

    /*
     * Obtiene el ciclo en curso
     */
    public function obtenerPeriodoCiclo(){
        return ($this->periodo_service->getCicloActual());
    }

    /*
     * Crea un programa calendario
     */
    public function crearProgramaCalendario($id_programa, $id_grupo, $fecha, $tipo, $perido_actual)
    {
        $programa_calendario = new ProgramaCalendario();
        $programa_calendario->setPrograma($this->em->getRepository('Rotacion:Programa')->find($id_programa));
        $programa_calendario->setGrupo($this->em->getRepository("Rotacion:Grupo")->find($id_grupo));
        $programa_calendario->setFecha(new \DateTime($fecha));
        $programa_calendario->setTipo($tipo);//determinado si es fecha de inicio o fin
        $programa_calendario->setCreado(new \DateTime("now"));
        $programa_calendario->setDescripcion('');
        $programa_calendario->setPeriodoCiclo($perido_actual);
        //persistiendo
        $this->em->persist($programa_calendario);
        return $programa_calendario;
    }

    /*
     * Realiza la logica para agregar calendariazaciones de los programas en los que se asiste un unico dia por rotacion
     */
    public function crearCalendarizacionUnica($request, $perido_actual, $programa, $numero_inputs)
    {
        //var_dump($programa);
        for ($i = 1; $i <= $numero_inputs; $i++) {
            //obteniendo las fechas
            //$rangos = explode(" - ", $request->get($programa. $i));
            //var_dump($rangos);
            //var_dump($i);
            //Llenando fecha inicio y fin de cada input
            //for ($j = 0; $j <= 1; $j++) {
            $programa_calendario = new ProgramaCalendario();
            //PPE
            $programa_calendario->setPrograma($this->em->getRepository('Rotacion:Programa')->findOneBy(array('codigo' => $programa)));
            $programa_calendario->setGrupo($this->em->getRepository("Rotacion:Grupo")->findOneBy(array('periodo_ciclo' => $perido_actual, 'nombre' => $i, 'rotable' => 1)));
            $programa_calendario->setFecha(new \DateTime($request->get($programa . $i)));
            $programa_calendario->setTipo("DIA UNICO");//determinado si es fecha de inicio o fin
            $programa_calendario->setCreado(new \DateTime("now"));
            $programa_calendario->setDescripcion('');
            $programa_calendario->setPeriodoCiclo($perido_actual);
            //persistiendo
            $this->em->persist($programa_calendario);
            // }

        }
        //die();
    }

    /*
     * Realiza la logica para actualizar calendariazaciones de los programas en los que se asiste un unico dia por rotacion
*/
    public function updateCalendarizacionUnica($request, $perido_actual, $programa, $numero_inputs)
    {
        //var_dump($programa);
        for ($i = 1; $i <= $numero_inputs; $i++) {

            $programa_calendario = $this->em->getRepository('Rotacion:ProgramaCalendario')->findOneBy(
                array('periodo_ciclo' => $perido_actual,
                    'programa' => $this->em->getRepository('Rotacion:Programa')->findOneBy(array('codigo' => $programa)),
                    'tipo' => "DIA UNICO",
                    'grupo' => $this->em->getRepository('Rotacion:Grupo')->findOneBy(array('nombre' => $i))
                )
            );
            $programa_calendario->setFecha(new \DateTime($request->get($programa . $i)));
        }
    }

    /*
     * Realiza la logica para agregar calendariazaciones de los programas que se asiste mas de un dia por rotacion
     */
    public function crearCalendarizacionDoble($request, $perido_actual, $programa, $numero_inputs)
    {
        //var_dump($programa);
        for ($i = 1; $i <= $numero_inputs; $i++) {
            //obteniendo las fechas
            $rangos = ($programa == 'ppe') ? explode(" - ", $request->get('ppe1')) : explode(" - ", $request->get($programa . $i));
            //var_dump($rangos);
            //var_dump($i);
            //Llenando fecha inicio y fin de cada input
            for ($j = 0; $j <= 1; $j++) {
                $programa_calendario = new ProgramaCalendario();
                //PPE
                $programa_calendario->setPrograma($this->em->getRepository('Rotacion:Programa')->findOneBy(array('codigo' => $programa)));
                if($numero_inputs==1)
                    $programa_calendario->setGrupo($this->em->getRepository("Rotacion:Grupo")->findOneBy(array('periodo_ciclo' => $perido_actual, 'nombre' => 7, 'rotable' => 1)));
                else
                    $programa_calendario->setGrupo($this->em->getRepository("Rotacion:Grupo")->findOneBy(array('periodo_ciclo' => $perido_actual, 'nombre' => $i , 'rotable' => 1)));

                //$programa_calendario->setGrupo($this->em->getRepository("Rotacion:Grupo")->findOneBy(array('periodo_ciclo' => $perido_actual, 'nombre' => $i, 'rotable' => 1)));
                $programa_calendario->setFecha(new \DateTime($rangos[$j]));
                $programa_calendario->setTipo(($j == 0) ? "INICIO" : "FIN");//determinado si es fecha de inicio o fin
                $programa_calendario->setCreado(new \DateTime("now"));
                $programa_calendario->setDescripcion('');
                $programa_calendario->setPeriodoCiclo($perido_actual);
                //persistiendo
                $this->em->persist($programa_calendario);
            }

        }
        //die();
    }

    /*
     * Realiza la logica para actualizar calendariazaciones de los programas que se asiste mas de un dia por rotacion
     */
    public function updateCalendarizacionDoble($request, $perido_actual, $programa, $numero_inputs)
    {
        //var_dump($programa);
        for ($i = 1; $i <= $numero_inputs; $i++) {
            //obteniendo las fechas
            $rangos = ($programa == 'ppe') ? explode(" - ", $request->get('ppe1')) : explode(" - ", $request->get($programa . $i));;
            //Llenando fecha inicio y fin de cada input
            for ($j = 0; $j <= 1; $j++) {

                $programa_calendario = $this->em->getRepository('Rotacion:ProgramaCalendario')->findOneBy(
                    array('periodo_ciclo' => $perido_actual,
                        'programa' => $this->em->getRepository('Rotacion:Programa')->findOneBy(array('codigo' => $programa)),
                        'tipo' => ($j == 0) ? "INICIO" : "FIN",
                        //'grupo'=>$this->em->getRepository('Rotacion:Grupo')->findOneBy(array('nombre'=>($numero_inputs == 1) ? 7: $i))
                        'grupo' => $this->em->getRepository('Rotacion:Grupo')->findOneBy(array('nombre' => ($programa == 'ppe')?7:$i,'periodo_ciclo'=>$perido_actual))
                    )
                );
                //dump($programa_calendario);
                //dump($rangos[$j]);
                //dump($rangos[$j]);
                $programa_calendario->setFecha(date_create_from_format('d/m/Y',$rangos[$j]));
                //dump(date_create_from_format('d/m/Y','19/07/2018'));
                //die();
                //Modificando Fechas de Rotacion
                /*
                $rotacion=$this->em->getRepository('Rotacion:Rotacion')->findOneBy(array('grupo'=>$this->em->getRepository('Rotacion:Grupo')->findOneBy(array('nombre' => $i,'periodo_ciclo'=>$perido_actual))));
                ($j == 0) ? $rotacion->setInicio(new \DateTime($rangos[$j])) : $rotacion->setInicio(new \DateTime($rangos[$j]));
                */
            }
            //die();
        }
    }

    /*
    * Obtiene los cursos que tiene rotacion preliminar
    */
    public function obtenerCursosRotando($periodoCicloID = null)
    {
        $db = $this->em->getConnection();

        if (is_null($periodoCicloID)) $periodoCicloID = $this->obtenerPeriodoCiclo()->getID();
        $query = "select distinct curso as cursos 
                  from rotacion.rotacion_detalle_view 
                  where periodo_ciclo =:periodo;";
        $stmt = $db->prepare($query);
        $params = array('periodo'=>$periodoCicloID);
        $stmt->execute($params);
        return ($fechas = $stmt->fetchAll());
    }

    /*
    * Obtiene los correos que tienen rotacion preliminar
    */
    public function obtenerCorreosRotacion($periodoCicloID = null,$curso = null)
    {
        $db = $this->em->getConnection();

        if (is_null($periodoCicloID)) $periodoCicloID = $this->obtenerPeriodoCiclo()->getID();
        /*$query = "select distinct carnet
                  from rotacion.rotacion_detalle_view 
                  where periodo_ciclo =:periodo
                  and curso=:curso
                  ;";
        $stmt = $db->prepare($query);
        $params = array('periodo'=>$periodoCicloID,'curso'=>$curso);
        $stmt->execute($params);
        return ($fechas = $stmt->fetchAll());
        */
        $query = "select distinct carnet,tipo_rotacion
                  from rotacion.rotacion_detalle_view_completa
                  where periodo_ciclo =:periodo
                  and curso=:curso
                  ;";
        $stmt = $db->prepare($query);
        $params = array('periodo'=>$periodoCicloID,'curso'=>$curso);
        $stmt->execute($params);
        return ($fechas = $stmt->fetchAll());
    }

    /*
     * Obtiene el resumen de la calendarizacion por Rotacion
     */
    public function obtenerResumenCalendarizacion($periodoCicloID = null)
    {

        $db = $this->em->getConnection();

        if (is_null($periodoCicloID)) $periodoCicloID = $this->obtenerPeriodoCiclo()->getID();
        $query = "SELECT * from rotacion.fechas_rotacion_view where periodoCiclo = " . $periodoCicloID . " ;";
        $stmt = $db->prepare($query);
        $params = array();
        $stmt->execute($params);

        return ($fechas = $stmt->fetchAll());
    }

    /*
    * Obtiene el resumen de la calendarizacion por Rotacion
    */
    public function obtenerResumenRotacionEmail($periodoCicloID = null,$curso=null,$tipo_rotacion=null){

        $db = $this->em->getConnection();

        /*
        if(is_null($periodoCicloID))$periodoCicloID = $this->obtenerPeriodoCiclo()->getID();
        $query = "SELECT * from rotacion.rotacion_resumen_email where periodo_ciclo =:p and curso=:curso;";
        $stmt = $db->prepare($query);
        $params = array('p'=>$periodoCicloID,'curso'=>$curso);
        $stmt->execute($params);
        */


        if(is_null($periodoCicloID))$periodoCicloID = $this->obtenerPeriodoCiclo()->getID();
        $query = "
                SELECT * 
                from rotacion.rotacion_resumen_email 
                where periodo_ciclo =:p 
                and curso=:curso
                and tipo_rotacion=:tipo_rotacion
                union ALL
                SELECT * 
                from rotacion.rotacion_resumen_email 
                where periodo_ciclo =:p 
                and curso=:curso
                and tipo_rotacion='C'
                order by 4,5";
        $stmt = $db->prepare($query);
        $params = array('p'=>$periodoCicloID,'curso'=>$curso,'tipo_rotacion'=>$tipo_rotacion);
        $stmt->execute($params);

        //dd($periodoCicloID,$curso,$tipo_rotacion);

        //dd($fechas=$stmt->fetchAll());
        return ($fechas=$stmt->fetchAll());
    }

    /*
    * Obtiene el resumen de la calendarizacion por Rotacion por alumno
    */
    public function obtenerResumenRotacionAlumno($periodoCicloID = null,$carnet){

        $db = $this->em->getConnection();

        if(is_null($periodoCicloID)) $periodoCicloID = $this->obtenerPeriodoCiclo()->getID();
        $query = "SELECT carnet,nombre,curso,rotacion,periodo_ciclo,nombre_periodo_ciclo,programa,
                         institucion,fechas
                  from rotacion.rotacion_detalle_view_completa
                  where periodo_ciclo =:p 
                  and carnet=:carnet
                  order by rotacion;";
        $stmt = $db->prepare($query);
        $params = array('p'=>$periodoCicloID,'carnet'=>$carnet);
        $stmt->execute($params);

        return ($fechas=$stmt->fetchAll());
    }
    /*
     * Obtiene los cupos que hay por institucion
     */
     public function obtenerCuposInstitucion($periodoCicloID=null,$id_programa=null,$id_rotacion=null){
         $db = $this->em->getConnection();

         if(is_null($periodoCicloID))$periodoCicloID = $this->obtenerPeriodoCiclo()->getID();
         $query = "select *
                    from rotacion.vw_cupos_por_rotacion
                    where cupos_max-activos>1
                    and id_periodo_ciclo=:periodo_ciclo
                    and programa=:programa
                    and rotacion=:rotacion";
         $stmt = $db->prepare($query);
         //dd($periodoCicloID,$id_programa,$id_rotacion);
         $params = array('periodo_ciclo'=>$periodoCicloID,'programa'=>$id_programa,'rotacion'=>$id_rotacion);
         $stmt->execute($params);

         return ($fechas=$stmt->fetchAll());
     }
    /*
     * Crea un grupo de rotacion
     */
    public function crearGrupoOne($nombre, $id_padre, $rotable, $periodo_actual)
    {

        $grupo = new Grupo();
        $grupo->setNombre($nombre);
        if ($id_padre != null)
            $grupo->setPadre($id_padre);
        $grupo->setRotable($rotable);
        $grupo->setPeriodoCiclo($periodo_actual);
        $grupo->setCreado(new \DateTime('now'));

        $this->em->persist($grupo);

        return $grupo;
    }

    /*
     * Crea los grupos de Rotacion
     */
    public function crearGruposRotacion($periodo_actual)
    {

        for ($i = 1; $i <= 2; $i++) {

            //creando los grupos padre
            $padre = $this->crearGrupoOne(($i == 1) ? 'A' : 'B', null, 0, $periodo_actual);

            for ($j = 1; $j <= 3; $j++) {
                //creando los grupos hijos
                $this->crearGrupoOne(($i == 1) ? $j : $j + 3, $padre, 1, $periodo_actual);
            }
        }

        //creando el septimo grupo de PPE
        $padre = $this->crearGrupoOne('C', null, 0, $periodo_actual);
        $this->crearGrupoOne(7, $padre, 1, $periodo_actual);
    }

    /**
     * @return Planificacion
     * @throws \Exception
     */
    public function obtenerPlanificacionActual()
    {
        $ciclo_actual = $this->periodo_service->getCicloActual();
        $planificacion = $this->em->createQuery("
            SELECT p FROM Rotacion:Planificacion p
            INNER JOIN p.periodo_ciclo c
            WHERE c.id = :ciclo_actual ")
            ->setParameter('ciclo_actual', $ciclo_actual)
            ->setMaxResults(1)
            ->getOneOrNullResult();

        if (is_null($planificacion)) {
            throw new \Exception('No existe una planificación activa');
        }

        return $planificacion;
    }
}