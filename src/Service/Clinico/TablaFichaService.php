<?php

namespace App\Service\Clinico;

use Doctrine\ORM\EntityManagerInterface;

use App\Entity\Clinico\ExpedienteClinico;
use App\Entity\Clinico\Ficha;
use App\Entity\Administracion\Persona;

class TablaFichaService
{
  private $em;

  public function __construct(EntityManagerInterface $entityManager)
  {
    $this->em = $entityManager;
  }

  public function getTableInfo($expedienteId, $fichaId)
  {
    $ficha = $this->em->getRepository(Ficha::class)->findOneBy(['id' => $fichaId]);
    $expediente = $this->em->getRepository(ExpedienteClinico::class)->findOneBy(['id' => $expedienteId]);
    $persona = $expediente->getPersona();
    $id_cuenta = $ficha->getCuenta()->getId();
    $estudiante = $this->em->getRepository(Persona::class)->findOneBy(['cuenta' => $id_cuenta]);
    $creado = $ficha->getFechaControl();
    $fnacimiento = $expediente->getPersona()->getNacimiento();
    $edad = null;
    if ($fnacimiento) $edad = $creado->diff($fnacimiento)->y;

    $result = Array(
      'id' => $ficha->getCuenta()->getId(),
      'finalizada' => $ficha->getFinalizada(),
      'fecha_control' => $ficha->getFechaControl(),
      'estudiante' => $estudiante->getNombreCompleto(),
      'persona' => $persona->getNombreCompleto(),
      'edad' => $edad,
      'curso' => $ficha->getCurso(),
      'ciclo' => $ficha->getCiclo(),
      'codigo_expediente' => $expediente->getCodigoExpediente(),
    );

    return $result;
  }

  public function allowEdit($usuario, $ficha)
  {

    if(($ficha->getCuenta()->getId() == $usuario->getCuenta()->getId()) && !$ficha->getFinalizada()){
      return true;
    }

    return false;
  }
}
