<?php

namespace App\Service\Clinico;

use Doctrine\ORM\EntityManagerInterface;

use App\Entity\Clinico\ExpedienteClinico;
use App\Entity\Clinico\Ficha;
use App\Entity\Administracion\Persona;
use Doctrine\ORM\Query\ResultSetMapping;

class RiesgoService
{
  private $em;

  public function __construct(EntityManagerInterface $entityManager)
  {
    $this->em = $entityManager;
  }

  public function getIndices(&$riesgo, $fichaId){
    # IMPORTANTE: Solo funciona en MYSQL >= 5.6 o MariaDB 10, debido a funcion group_concat
    # En otros gestores utilizar funciones de agregacion y array to string
    $piezasSQL = 'select a.pieza_dental as pieza, group_concat(b.codigo) as estados from (select pieza_dental, id_pieza_dental_estado from clinico.evaluacion_dental where id_ficha=:ficha) as a inner join clinico.ficha_catalogo as b on a.id_pieza_dental_estado=b.id group by pieza;';
    $rsm = new ResultSetMapping();
    
    $rsm->addScalarResult('pieza', 'pieza');
    $rsm->addScalarResult('estados', 'estados');
    $piezas = $this->em->createNativeQuery($piezasSQL, $rsm)->setParameter('ficha', $fichaId)->getResult();


    # Contadores
    $cpo_c = 0;
    $cpo_p = 0;
    $cpo_o = 0;
    $ceo_c = 0;
    $ceo_e = 0;
    $ceo_o = 0;
    
    foreach ($piezas as $pieza) {
      
      $int_pieza = (int)$pieza['pieza'];

      # Se manejan las piezas por 3 codigos: 0=permanente, 1=permanente y molar, 2=primaria; donde 0 y 1 forman parte de CPO y 2 de ceo.
      $codigo_pieza;
      if($int_pieza > 50) $codigo_pieza = 2;
      else if($int_pieza == 16 or $int_pieza == 26 or $int_pieza == 36 or $int_pieza == 46) $codigo_pieza = 1;
      else $codigo_pieza = 0;

      # Se determina el estado de la pieza a partir de los estados de sus superficies
      $estados_superficies = explode(',', $pieza['estados']);
      $estado_pieza = -1;
      foreach ($estados_superficies as $estado_str) {
        $cod_estado = (int)$estado_str;

        #Se determina con ayuda de ICDAS el estado de las superficies: 3=cariado, 2=perdido, 1=obturado, 0=sana
        $estado;
        if($cod_estado >= 96) $estado=2;
        else if($cod_estado>=30 && $cod_estado<=86 && $cod_estado%10==0) $estado=1;
        else if($cod_estado<30 && $cod_estado>=0 && $cod_estado%10==0) $estado=0;
        else $estado=3;
        
        if($estado > $estado_pieza) $estado_pieza = $estado;
      }

      #Teniendo el codigo de la pieza y el codigo del estado de la pieza, procedemos a aumentar los contadores
      switch ($estado_pieza) {
        case 1:
          if($codigo_pieza == 2) $ceo_o++;
          else $cpo_o++;
          break;
        
        case 2:
          if($codigo_pieza == 2) $ceo_e++;
          else $cpo_p++;
          break;
        
        case 3:
          if($codigo_pieza == 2) $ceo_c++;
          else $cpo_c++;
          break;  
      }

    }

    $riesgo->cpo_c = $cpo_c;
    $riesgo->cpo_p = $cpo_p;
    $riesgo->cpo_o = $cpo_o;
    $riesgo->ceo_c = $ceo_c;
    $riesgo->ceo_e = $ceo_e;
    $riesgo->ceo_o = $ceo_o;
  }

  public function calcRisk($caries, $placa, $azucar){
    $riesgo_pond = 0;
    $riesgo_pond += $this->getStates($caries, 0);
    $riesgo_pond += $this->getStates($placa, 1);
    $riesgo_pond += $this->getStates($azucar, 2);

    $diagnostico = 'Alto';

    if($riesgo_pond == 0){
      $diagnostico = 'Bajo';
    } elseif($riesgo_pond <= 3) {
      $diagnostico = 'Moderado';
    } else {
      $diagnostico = 'Alto';
    }

    return $diagnostico;
  }

  private function getStates($medida, $tipo){

    $pond = 0;

    switch ($tipo) {

      case 0:

        if ($medida<=2) {
          $pond = 0;
        } elseif ($medida <= 4) {
          $pond = 1;
        } else {
          $pond = 2;
        }

        break;

      case 1:

        if ($medida<=20) {
          $pond = 0;
        } elseif ($medida <= 50) {
          $pond = 1;
        } else {
          $pond = 2;
        }

        break;

      case 2:

        if ($medida<=4) {
          $pond = 0;
        } elseif ($medida <= 7) {
          $pond = 1;
        } else {
          $pond = 2;
        }

        break;
    }

    return $pond;
  }

  public function calcInd($riesgo){
    return $riesgo->cpo_c + $riesgo->cpo_p + $riesgo->cpo_o + $riesgo->ceo_c + $riesgo->ceo_e + $riesgo->ceo_o;
  }

  public function calcPlaque($riesgo){
    return (($riesgo->placa_1655 + $riesgo->placa_1151 + $riesgo->placa_2665 + $riesgo->placa_3675 + $riesgo->placa_3171 + $riesgo->placa_4685)/24)*100;
  }
}
