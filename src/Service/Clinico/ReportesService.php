<?php

namespace App\Service\Clinico;

use Doctrine\ORM\EntityManagerInterface;

use App\Entity\Clinico\ExpedienteClinico;
use App\Entity\Clinico\Ficha;
use App\Entity\Administracion\Persona;
use Doctrine\ORM\Query\ResultSetMapping;

class ReportesService{

  private $em;

  public function __construct(EntityManagerInterface $entityManager)
  {
    $this->em = $entityManager;
  }

  public function getIndices($fichaId, $criterio) //$criterio: 0 = ICDAS, 1 = OMS
  {
    # IMPORTANTE: Solo funciona en MYSQL >= 5.6 o MariaDB 10, debido a funcion group_concat
    # En otros gestores utilizar funciones de agregacion y array to string
    $piezasSQL = 'select a.pieza_dental as pieza, group_concat(b.codigo) as estados from (select pieza_dental, id_pieza_dental_estado from clinico.evaluacion_dental where id_ficha=:ficha) as a inner join clinico.ficha_catalogo as b on a.id_pieza_dental_estado=b.id group by pieza;';
    $rsm = new ResultSetMapping();

    $rsm->addScalarResult('pieza', 'pieza');
    $rsm->addScalarResult('estados', 'estados');
    $piezas = $this->em->createNativeQuery($piezasSQL, $rsm)->setParameter('ficha', $fichaId)->getResult();

     # Contadores
     $cpod_c = 0;
     $cpod_p = 0;
     $cpod_o = 0;
     $cpom_c = 0;
     $cpom_p = 0;
     $cpom_o = 0;
     $ceod_c = 0;
     $ceod_p = 0;
     $ceod_o = 0;
     $cpos_c = 0;
     $cpos_p = 0;
     $cpos_o = 0;
     $cpos_s = 0;

     foreach ($piezas as $pieza) {

        $int_pieza = (int)$pieza['pieza'];
        if($int_pieza<=0) continue;

        # Se manejan las piezas por 3 codigos: 0=permanente, 1=permanente y molar, 2=primaria; donde 0 es CPOD, 1 es CPOM, 2 es ceo.
        $codigo_pieza;
        if($int_pieza > 50) $codigo_pieza = 2;
        else if($int_pieza == 16 or $int_pieza == 26 or $int_pieza == 36 or $int_pieza == 46) $codigo_pieza = 1;
        else $codigo_pieza = 0;

        # Se determina el estado de la pieza a partir de los estados de sus superficies
        $estados_superficies = explode(',', $pieza['estados']);
        $estado_pieza = -1;
        foreach ($estados_superficies as $estado_str) {
          $cod_estado = (int)$estado_str;
          $estado = $this->getEstadoElemento($cod_estado, $criterio);

          /* Llevando el conteo de CPOS */
          switch ($estado) {
              case 0:
                  $cpos_s++;
                  break;
              case 1:
                  $cpos_o++;
                  break;
              case 2:
                  $cpos_p++;
                  break;
              case 3:
                  $cpos_c++;
                  break;
          }

          # Para determinar el estado de la pieza en base a la prioridad
          if($estado > $estado_pieza) $estado_pieza = $estado;
        }

        # Se manejan las piezas por 3 codigos: 0=permanente, 1=permanente y molar, 2=primaria; donde 0 y 1 forman parte de CPO y 2 de ceo.
        #Teniendo el codigo de la pieza y el codigo del estado de la pieza, procedemos a aumentar los contadores

        switch ($estado_pieza) {
          case 1:
            if($codigo_pieza==0) $cpod_o++;
            elseif ($codigo_pieza==1) $cpom_o++;
            else $ceod_o++;
            break;

          case 2:
            if($codigo_pieza==0) $cpod_p++;
            elseif ($codigo_pieza==1) $cpom_p++;
            else $ceod_p++;
            break;

          case 3:
            if($codigo_pieza==0) $cpod_c++;
            elseif ($codigo_pieza==1) $cpom_c++;
            else $ceod_c++;
            break;
        }

      }

      $indice_cpod = ['CPOD', $cpod_c, $cpod_p, $cpod_o];
      $indice_cpom = ['CPOM', $cpom_c, $cpom_p, $cpom_o];
      $indice_ceod = ['CEOD', $ceod_c, $ceod_p, $ceod_o];
      $indice_cpos = ['CPOS', $cpos_c, $cpos_p, $cpos_o, $cpos_s];

      return [$indice_cpod, $indice_cpom, $indice_ceod, $indice_cpos];
  }


  private function getEstadoElemento($cod_estado, $criterio) //$criterio: 0 = ICDAS, 1 = OMS
  {
    $estado = -1;
    switch ($criterio) {

        case 0:
          #Se determina con ayuda de ICDAS el estado de las superficies: 3=cariado, 2=perdido, 1=obturado, 0=sana
          if($cod_estado >= 96) $estado=2;
          else if($cod_estado>=30 && $cod_estado<=86 && $cod_estado%10==0) $estado=1;
          else if($cod_estado<30 && $cod_estado>=0 && $cod_estado%10==0) $estado=0;
          else $estado=3;
          break;

        case 1:
            #Se determina con ayuda de OMS el estado de las superficies: 3=cariado, 2=perdido, 1=obturado, 0=sana
              if($cod_estado >= 96) $estado=2;
              else if($cod_estado%10>=3 && $cod_estado%10<=6) $estado=3;
              else if($cod_estado/10>=3 && $cod_estado%10>=0 && $cod_estado%10<=2) $estado=1;
              else $estado=0;
            break;
    }


    return $estado;
  }

  public function getNecesidadesTratamiento($fichaId, $criterio)
  {
    $tratamientosSQL = 'select c.pieza_dental, c.superficie_dental, c.codigo, d.nombre from (select a.pieza_dental, a.superficie_dental, b.codigo, a.id_tratamiento from (select pieza_dental, superficie_dental, id_pieza_dental_estado, id_tratamiento from clinico.evaluacion_dental where id_ficha=:ficha and pieza_dental not like "BC%") as a inner join clinico.ficha_catalogo as b on a.id_pieza_dental_estado=b.id) as c inner join clinico.tratamiento as d on c.id_tratamiento=d.id;';
    $rsm = new ResultSetMapping();

    $rsm->addScalarResult('pieza_dental', 'pieza_dental');
    $rsm->addScalarResult('superficie_dental', 'superficie_dental');
    $rsm->addScalarResult('codigo', 'codigo');
    $rsm->addScalarResult('nombre', 'nombre');
    $tratamientos = $this->em->createNativeQuery($tratamientosSQL, $rsm)->setParameter('ficha', $fichaId)->getResult();

    return $tratamientos;
  }

  public function getSeveridades($fichaId)
  {
    $severidadesSQL = 'select d.severidad as "Severidad", (count(*)-1) as "Afectados" from (select "Sano" as severidad union select "Esmalte" as severidad union select "Dentina" as severidad union select "Perdido" as severidad union all (select case when mod(c.codigo, 10)=0 then "Sano" when (mod(c.codigo, 10)>=1 and mod(c.codigo, 10)<=3) then "Esmalte" when (mod(c.codigo, 10)>=4 and mod(c.codigo, 10)<=6) then "Dentina" else "Perdido" end as severidad from (select codigo from (select id_pieza_dental_estado as estado from clinico.evaluacion_dental where id_ficha=:ficha and pieza_dental not like "BC%") as a inner join clinico.ficha_catalogo as b on a.estado=b.id) as c)) as d group by d.severidad;';
    $rsm = new ResultSetMapping();

    $rsm->addScalarResult('Severidad', 'Severidad');
    $rsm->addScalarResult('Afectados', 'Afectados');
    $severidades = $this->em->createNativeQuery($severidadesSQL, $rsm)->setParameter('ficha', $fichaId)->getResult();

    return $severidades;
  }

}
