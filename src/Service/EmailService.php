<?php

namespace App\Service;

/**
 * Class EmailService
 * @package App\Service
 */
class EmailService
{

    /**
     * @var \Swift_Mailer
     */
    private $mailer;

    /**
     * @var \Twig_Environment
     */
    private $twig;

    /**
     * EmailService constructor.
     * @param \Swift_Mailer $mailer
     * @param \Twig_Environment $twig
     */
    public function __construct(\Swift_Mailer $mailer, \Twig_Environment $twig)
    {
        $this->mailer = $mailer;
        $this->twig = $twig;
    }

    /**
     * @param $subject
     * @param $recipients
     * @param array $params
     * @param string $template
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    public function send($subject, $recipients, $params = [], $template = null)
    {
        $template = $template ?: 'email/test.html.twig';

        $body = $this->twig->render($template, $params);

        $message = (new \Swift_Message())
            ->setSubject('[PEYPS] ' . $subject)
            ->setFrom(getenv('MAILER_USER'))
            ->setTo($recipients)
            ->setBody($body, 'text/html');

        $this->mailer->send($message);
    }

}