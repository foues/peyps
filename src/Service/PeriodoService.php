<?php


namespace App\Service;


use App\Entity\Academico\PeriodoCiclo;
use Doctrine\ORM\EntityManagerInterface;

class PeriodoService
{

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * PeriodoService constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @return PeriodoCiclo
     * @throws \Exception
     */
    public function getCicloActual()
    {
        $ciclo = $this->em->getRepository('Academico:PeriodoCiclo')
            ->findCicloActual();

        if (is_null($ciclo)) {
            throw new \Exception('No existe un ciclo o periodo activo');
        }

        return $ciclo;
    }

    /**
     * @param PeriodoCiclo|null $ciclo
     * @return PeriodoCiclo|null
     * @throws \Exception
     */
    public function getCicloSiguiente(?PeriodoCiclo $ciclo)
    {
        $ciclo = $ciclo ?: $this->getCicloActual();

        $ciclo_siguiente = $this->em->getRepository('Academico:PeriodoCiclo')
            ->findCicloSiguiente($ciclo);

        return $ciclo_siguiente;
    }

    /**
     * @param PeriodoCiclo|null $ciclo
     * @return PeriodoCiclo|null
     * @throws \Exception
     */
    public function getCicloAnterior(?PeriodoCiclo $ciclo)
    {
        $ciclo = $ciclo ?: $this->getCicloActual();

        $ciclo_anterior = $this->em->getRepository('Academico:PeriodoCiclo')
            ->findCicloAnterior($ciclo);

        return $ciclo_anterior;
    }

    /**
     * @return PeriodoCiclo[]
     */
    public function getAllCiclos()
    {
        return $this->em->getRepository('Academico:PeriodoCiclo')
            ->findAllWithPeriodo();
    }
}