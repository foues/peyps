<?php
/**
 * Created by PhpStorm.
 * User: Dell
 * Date: 23/09/2018
 * Time: 16:28
 */

namespace App\Service;

use App\Entity\Almacen\Almacen;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
class ReporteService
{
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var PeriodoService
     */
    private $periodo_service;

    /**
     * RotacionService constructor.
     * @param EntityManagerInterface $em
     * @param PeriodoService $periodo_service
     */
    public function __construct(EntityManagerInterface $em, PeriodoService $periodo_service= null )
    {
        $this->em = $em;
        if(!is_null($periodo_service))$this->periodo_service = $periodo_service;
    }

    /**
     * Reporte de tratamientos ejecutados por programa y periodo. (Por programa no se puede)
     * @param null $inicio
     * @param null $fin
     * @throws \Doctrine\DBAL\DBALException
     */
    public function obtenerTratamientosEjecutados($inicio=null,$fin=null){
    $db = $this->em->getConnection();

    if (is_null($inicio)){
        $inicio = (new \DateTime('now'))->format('Y-m-d');//date("H:i:s")
        $fin=$inicio;
    }
    //dd($inicio,$fin);
    $query = "select d.nombre area,a.id_tratamiento,b.nombre tratamiento,
                    sum(
                    case 
                        when (c.curso like '%EST%' OR c.curso like '%OPC%') THEN 1
                        else 0
                    end
                    ) EXTRAMURAL,
                    sum(
                          case 
                              when (c.curso like '%EST%' OR c.curso like '%OPC%') THEN 0
                              else 1
                          end
                          ) INTRAMURAL
            from clinico.evaluacion_dental a
            join clinico.tratamiento b
            on a.id_tratamiento=b.id
            join clinico.ficha c
            on a.id_ficha=c.id 
            join clinico.ficha_catalogo d
            on b.id_tratamiento_area=d.id
            where date(a.creado) BETWEEN date(:inicio) and date(:fin)  and a.realizado=1
            GROUP by a.id_tratamiento,b.nombre,d.nombre;";
    // where a.creado BETWEEN :inicio and :fin
    $stmt = $db->prepare($query);
    $params = array('inicio'=>$inicio,'fin'=>$fin);
    $stmt->execute($params);
    return ($fechas = $stmt->fetchAll());
    }

    /**
     * Cantidad de personas atendidas en periodo, por rangos de edad y sexo.
     * @param null $inicio
     * @param null $fin
     * @throws \Doctrine\DBAL\DBALException
     */
    public function obtenerPersonasAtendidas($inicio=null,$fin=null){
        $db = $this->em->getConnection();

        if (is_null($inicio)){
            $inicio = (new \DateTime('now'))->format('dmy');//date("H:i:s")
            $fin=$inicio;
        }
        $query = "select
                   case 
                    when max(floor(DATEDIFF(sysdate(),y.nacimiento)/365)) BETWEEN 0 and 9 then '0 - 9 años'
                    when max(floor(DATEDIFF(sysdate(),y.nacimiento)/365)) BETWEEN 10 and 19 then '10 -17 años'
                    when max(floor(DATEDIFF(sysdate(),y.nacimiento)/365)) >= 20 then 'Mayores a 20 años'
                  end rango,
                  count(distinct case when b.nombre= 'Masculino' then x.id end) Masculino,
                count(distinct case when b.nombre= 'Femenino' then x.id end) Femenino
            from clinico.expediente_clinico x
            join  administracion.persona_atributo a
            on x.id_persona=a.id_persona
            join administracion.persona y
            on x.id_persona=y.id
            join clinico.ficha z
            on x.id=z.id_expediente_clinico
            join administracion.catalogo b
            on a.id_atributo=b.id
            join administracion.catalogo_tipo c
            on b.id_catalogo_tipo=c.id
            where c.nombre='Sexo'
            and date(z.creado) BETWEEN date(:inicio) and date(:fin);";
        $stmt = $db->prepare($query);
        $params = array('inicio'=>$inicio,'fin'=>$fin);
        $stmt->execute($params);
        return ($fechas = $stmt->fetchAll());
    }

    /**
     * Cantidad de mujeres embarazadas atendidas durante un periodo
     * @param null $inicio
     * @param null $fin
     * @throws \Doctrine\DBAL\DBALException
     */
    public function obtenerEmbarazadasAtendidas($inicio=null,$fin=null){
        $db = $this->em->getConnection();

        if (is_null($inicio)){
            $inicio = (new \DateTime('now'))->format('dmy');//date("H:i:s")
            $fin=$inicio;
        }
        $query = "  select
                   case 
                    when max(floor(DATEDIFF(sysdate(),y.nacimiento)/365)) BETWEEN 0 and 9 then '0 - 9 años'
                    when max(floor(DATEDIFF(sysdate(),y.nacimiento)/365)) BETWEEN 10 and 19 then '10 -17 años'
                    when max(floor(DATEDIFF(sysdate(),y.nacimiento)/365)) >= 20 then 'Mayores a 20 años'
                  end rango,
                  count(distinct x.id) atenciones
            from clinico.expediente_clinico x
            join  administracion.persona_atributo a
            on x.id_persona=a.id_persona
            join administracion.persona y
            on x.id_persona=y.id
            join clinico.ficha z
            on x.id=z.id_expediente_clinico
            join administracion.catalogo b
            on a.id_atributo=b.id
            and date(z.creado) BETWEEN date(:inicio) and date(:fin)
            and z.embarazada=1;";
        $stmt = $db->prepare($query);
        $params = array('inicio'=>$inicio,'fin'=>$fin);
        $stmt->execute($params);
        return ($fechas = $stmt->fetchAll());
    }

    /**
     * Población atendida por primera vez durante un periodo.
     * @param null $inicio
     * @param null $fin
     * @throws \Doctrine\DBAL\DBALException
     */
    public function obtenerAtendidosPrimeraVez($inicio=null,$fin=null){
        $db = $this->em->getConnection();

        if (is_null($inicio)){
            $inicio = (new \DateTime('now'))->format('dmy');//date("H:i:s")
            $fin=$inicio;
        }
        $query = "select
                   case 
                    when max(floor(DATEDIFF(sysdate(),y.nacimiento)/365)) BETWEEN 0 and 9 then '0 - 9 años'
                    when max(floor(DATEDIFF(sysdate(),y.nacimiento)/365)) BETWEEN 10 and 19 then '10 -17 años'
                    when max(floor(DATEDIFF(sysdate(),y.nacimiento)/365)) >= 20 then 'Mayores a 20 años'
                  end rango,
                  count(distinct x.id) atenciones
            from clinico.expediente_clinico x
            join  administracion.persona_atributo a
            on x.id_persona=a.id_persona
            join administracion.persona y
            on x.id_persona=y.id
            join clinico.ficha z
            on x.id=z.id_expediente_clinico
            join administracion.catalogo b
            on a.id_atributo=b.id
            and date(z.creado) BETWEEN date(:inicio) and date(:fin)
            ";
        $stmt = $db->prepare($query);
        $params = array('inicio'=>$inicio,'fin'=>$fin);
        $stmt->execute($params);
        return ($fechas = $stmt->fetchAll());
    }

    /**
     * Ingresos economicos por tratamientos
     * @param null $inicio
     * @param null $fin
     * @throws \Doctrine\DBAL\DBALException
     */
    public function obtenerIngresosPorTratamientos($inicio=null,$fin=null){
        $db = $this->em->getConnection();

        if (is_null($inicio)){
            $inicio = (new \DateTime('now'))->format('dmy');//date("H:i:s")
            $fin=$inicio;
        }
        $query = "select a.id_tratamiento,b.nombre tratamiento,count(0) tratamientos,b.precio arancel,count(0)*b.precio ingresos
                    from clinico.evaluacion_dental a
                    join clinico.tratamiento b
                    on a.id_tratamiento=b.id
                    where date(a.creado) BETWEEN date(:inicio) and date(:fin) and a.realizado=1
                    GROUP by a.id_tratamiento,b.nombre";
        $stmt = $db->prepare($query);
        $params = array('inicio'=>$inicio,'fin'=>$fin);
        $stmt->execute($params);
        return ($fechas = $stmt->fetchAll());
    }

    /**
     * Tratamientos ejecutados por procedencia
     * @param null $inicio
     * @param null $fin
     * @throws \Doctrine\DBAL\DBALException
     */
    public function obtenerTratamientosPorProcedencia($inicio=null,$fin=null){
        $db = $this->em->getConnection();

        if (is_null($inicio)){
            $inicio = (new \DateTime('now'))->format('dmy');//date("H:i:s")
            $fin=$inicio;
        }
        $query = "select x.id_referencia id_procedencia,y.nombre procedencia,c.nombre area_clinica,
                          a.id_tratamiento,b.nombre tratamiento,count(0) ejecutados
                    from clinico.ficha x
                    join administracion.catalogo y
                    on x.id_referencia=y.id
                    join clinico.evaluacion_dental a
                    on x.id=a.id_ficha
                    join clinico.tratamiento b
                    on a.id_tratamiento=b.id
                    join clinico.ficha_catalogo c
                    on b.id_tratamiento_area=c.id
                    where date(a.creado) BETWEEN date(:inicio) and date(:fin)  and a.realizado=1
                    GROUP by x.id_referencia,y.nombre,a.id_tratamiento,b.nombre,c.nombre;";
        $stmt = $db->prepare($query);
        $params = array('inicio'=>$inicio,'fin'=>$fin);
        $stmt->execute($params);
        return ($fechas = $stmt->fetchAll());
    }

    /**
     * Consumo de Materiales por Fecha
     * @param null $inicio
     * @param null $fin
     * @param null $almacen
     * @throws \Doctrine\DBAL\DBALException
     */
    public function obtenerConsumoFecha($inicio=null,$fin=null,$almacen=null){
        //dd($almacen);
        $db = $this->em->getConnection();

        if (is_null($inicio)){
            $inicio = (new \DateTime('now'))->format('dmy');//date("H:i:s")
            $fin=$inicio;
        }
        $query = "select a.fecha,x.nombre institucion,b.nombre almacen,d.nombre material,e.nombre unidad_medida,e.subunidades subunidad,ABS(sum(a.cantidad)) cantidad
                    from almacen.movimiento a
                    join almacen.almacen b
                    on a.id_almacen=b.id 
                    join almacen.articulo_presentacion c
                    on a.id_articulo_presentacion=c.id
                    join almacen.articulo d
                    on c.id_articulo=d.id
                    join almacen.unidad_medida e
                    on c.id_unidad_medida=e.id
                    join administracion.institucion x
                    on b.id_institucion=x.id
                    where date(a.fecha) BETWEEN date(:inicio) and date(:fin)
                    and a.cantidad<0 
                    and b.nombre=:almacen
                    GROUP by a.fecha,b.nombre,d.nombre,e.nombre,x.nombre;";
        $stmt = $db->prepare($query);
        $params = array('inicio'=>$inicio,'fin'=>$fin, 'almacen'=>$almacen);
        $stmt->execute($params);
        return ($fechas = $stmt->fetchAll());
    }

    public function getAlmacenAll(){
        $almacen=$this->em->getRepository(Almacen::class)->findAll();
        return $almacen;
    }

}