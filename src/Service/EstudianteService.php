<?php

namespace App\Service;

use App\Entity\Academico\Curso;
use App\Entity\Academico\Estudiante;
use App\Entity\Academico\EstudianteCurso;
use App\Entity\Academico\EstudianteEstado;
use App\Entity\Academico\EstudiantePlanEstudio;
use App\Entity\Academico\PeriodoCiclo;
use App\Entity\Academico\PlanEstudio;
use App\Entity\Academico\PlanEstudioCiclo;
use App\Entity\Administracion\Cuenta;
use App\Entity\Administracion\Persona;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use http\Env\Response;


class EstudianteService
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * UserService constructor.
     * @param EntityManager $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function getEstudiantesPlanEstudio()
    {
        $estudiantes = $this->em->getRepository(EstudiantePlanEstudio::class)->findAll();
        return $estudiantes;
    }

    public function getEstudiantesPlanEstudioOne($id)
    {
        $estudiante = $this->em->getRepository(EstudiantePlanEstudio::class)->find($id);
        return $estudiante;
    }

    /**
     * @param $estado
     * @param $plan_estudio
     * @return EstudiantePlanEstudio[]
     */
    public function getEstudiantesPlanEstudioAll($estado, $plan_estudio)
    {
        /** @var EstudiantePlanEstudio $estudiantes */
        $estudiantes = $this->em->getRepository(EstudiantePlanEstudio::class)
            ->getAllByPlanEstudioAndEstado($plan_estudio,$estado);
        return $estudiantes;
    }

    /**
     * @return EstudianteEstado[]
     */
    public function getEstados()
    {
        /** @var EstudianteEstado $estados */
        $estados = $this->em->getRepository(EstudianteEstado::class)->findAll();
        return $estados;
    }

    public function getEstadosPlan()
    {
        $estados = $this->em->getRepository(EstudianteEstado::class)->findBy(array('tipo'=>'plan_estudio'));
        return $estados;
    }
    public function getEstadosCurso()
    {
        $estados = $this->em->getRepository(EstudianteEstado::class)->findBy(array('tipo'=>'curso'));
        return $estados;
    }

    public function getEstadoOne($id)
    {
        $estado = $this->em->getRepository(EstudianteEstado::class)->find($id);
        return $estado;
    }

    public function getCursos()
    {

        $cursos = $this->em->getRepository(Curso::class)->findAll();
        return $cursos;
    }
    public function getCursosExtra(){
        $db = $this->em->getConnection();

        $query = "select *
                  from academico.curso
                  where codigo like '%EST%' or codigo like '%OPC%';";
        $stmt = $db->prepare($query);
        $params = array();
        $stmt->execute($params);
        return ($fechas = $stmt->fetchAll());
    }

    public function getEstudianteDuplicado($dui,$nit,$carnet){
        $db = $this->em->getConnection();

        $query = "select count(0) existe
                  from administracion.persona a
                  left join academico.estudiante b
                  on a.id=b.id_persona
                  where (dui=:dui or nit=:nit or lower(carnet)=:carnet);";
        $stmt = $db->prepare($query);
        $params = array('dui'=>$dui,'nit'=>$nit,'carnet'=>$carnet);
        $stmt->execute($params);
        return ($fechas = $stmt->fetch());
    }

    public function getPeriodoCiclo()
    {
        $periodo_ciclo = $this->em->getRepository(PeriodoCiclo::class)->findAll();
        //$p=new PeriodoCiclo();

        ///dd($periodo_ciclo[0]->getPeriodo()->getInicio());
        return $periodo_ciclo;
    }

    public function getPlanEstudioAll()
    {
        $plan_estudio = $this->em->getRepository(PlanEstudio::class)->findAll();
        return $plan_estudio;
    }

    public function getPlanEstudioOne($id)
    {
        $plan_estudio = $this->em->getRepository(PlanEstudio::class)->find($id);
        return $plan_estudio;
    }

    public function getEstudiantesCursos($periodo)
    {
        $est_c = $this->em->getRepository(EstudianteCurso::class)->findBy(array('periodo_ciclo'=>$periodo));
        return $est_c;
    }

    public function getEstudianteCurso($curso, $periodo)
    {
        $est_c = $this->em->getRepository(EstudianteCurso::class)->findOneBy(array('curso'=> $curso,'periodo_ciclo'=>$periodo));

        return $est_c;
    }
    public function getEstudianteCursoAprobado($curso, $periodo, $estudiante)
    {
       $est_estado=$this->em->getRepository(EstudianteEstado::class)->findOneBy(['codigo'=>'aprobado']);
        $est_c = $this->em->getRepository(EstudianteCurso::class)->findOneBy(array('curso'=> $curso,'periodo_ciclo'=>$periodo, 'estudiante_estado'=>$est_estado, 'estudiante_plan_estudio'=>$estudiante));

        return $est_c;
    }
    public function getEstudianteCursoResolucion($curso, $periodo, $estudiante)
    {
        $est_estado=$this->em->getRepository(EstudianteEstado::class)->findOneBy(['codigo'=>'resolucion']);
        $est_c = $this->em->getRepository(EstudianteCurso::class)->findOneBy(array('curso'=> $curso,'periodo_ciclo'=>$periodo, 'estudiante_estado'=>$est_estado, 'estudiante_plan_estudio'=>$estudiante));

        return $est_c;
    }
    public function getEstudianteCursoEstudiante($curso, $periodo, $estudiante)
    {
        $est_estado=$this->em->getRepository(EstudianteEstado::class)->findOneBy(['codigo'=>'estudiante']);
        $est_c = $this->em->getRepository(EstudianteCurso::class)->findOneBy(array('curso'=> $curso,'periodo_ciclo'=>$periodo, 'estudiante_estado'=>$est_estado, 'estudiante_plan_estudio'=>$estudiante));

        return $est_c;
    }

    public function getEstudiantesCursosAll($curso, $periodo){
        $estudiantes_curso= $this->em->getRepository(EstudianteCurso::class)
            ->findBy(array('curso'=>$this->getCurso($curso),
                    'periodo_ciclo'=>$periodo,
                ),
                array('estudiante_plan_estudio'=>'ASC')
            );

        return $estudiantes_curso;
    }

    public function getCurso($id)
    {
        $curso = $this->em->getRepository(Curso::class)->find($id);
        return $curso;
    }

    public function getEstudianteCursoOne($id){
        $estudiante_curso= $this->em->getRepository(EstudianteCurso::class)->find($id);
        return $estudiante_curso;
    }

    public function getEstudiantesCurso($id_curso,$periodo){
        $estudiantes_curso= $this->em->getRepository(EstudianteCurso::class)
            ->findBy(array('curso'=>$this->getCurso($id_curso),
                    'periodo_ciclo'=>$periodo,
                    'estudiante_estado'=>$this->em->getRepository(EstudianteEstado::class)
                        ->findOneBy(array('codigo'=>'estudiante'))
                )
            );
        return $estudiantes_curso;
    }

    public function getPersonaOne($id){
        $persona= $this->em->getRepository(Persona::class)->find($id);
        return $persona;
    }

    //Metodos para edicion de estudiante

    public function setPersonaOne($id,$nombre,$apellido,$dui,$nit,$nacimiento){
        $persona= $this->getPersonaOne($id);
        $persona->setNombre($nombre);
        $persona->setApellido($apellido);
        $persona->setDui($dui);
        $persona->setNit($nit);
        $persona->setNacimiento(new \DateTime($nacimiento));
        $this->em->flush();
    }

    public function setEstudiantePlanEstudioOne($id,$cum,$estado)
    {
        //dump($this->getEstadoOne($estado));
        $estudiante = $this->getEstudiantesPlanEstudioOne($id);
        $estudiante->setCum($cum);
        //dump($estudiante->getEstudianteEstado());
        $estudiante->setEstudianteEstado($this->getEstadoOne($estado));
        //dump($estudiante->getEstudianteEstado());
        //die();
        $this->em->flush();
    }

    public function setEstudianteCursoOne($id,$curso)
    {
        $estudiante = $this->getEstudianteCursoOne($id);
        $estudiante->setCurso($this->getCurso($curso));
        $this->em->flush();
    }

    public function getPeriodoCicloOne($id){
        $persona= $this->em->getRepository(PeriodoCiclo::class)->find($id);
        return $persona;
    }

    /**
     * @param $carnet
     * @param $nombre
     * @param $apellido
     * @param Cuenta|null $cuenta
     * @return Estudiante
     * @throws \Doctrine\ORM\ORMException
     */
    public function createEstudiante($carnet, $nombre, $apellido, Cuenta $cuenta = null)
    {
        $em = $this->em;

        $persona = (new Persona())
            ->setCuenta($cuenta)
            ->setNombre($nombre)
            ->setApellido($apellido);
        $em->persist($persona);

        $estudiante = (new Estudiante())
            ->setPersona($persona)
            ->setCarnet($carnet);
        $em->persist($estudiante);

        return $estudiante;
    }

}