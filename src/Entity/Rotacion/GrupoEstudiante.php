<?php

namespace App\Entity\Rotacion;

use Doctrine\ORM\Mapping as ORM;

/**
 * GrupoEstudiante
 *
 * @ORM\Table(name="rotacion.grupo_estudiante", indexes={
 *     @ORM\Index(name="fk_grupo_estudiante_curso", columns={"id_estudiante_curso"}),
 *     @ORM\Index(name="fk_grupo_estudiante", columns={"id_grupo"})
 * })
 * @ORM\Entity(repositoryClass="App\Repository\Rotacion\GrupoEstudianteRepository")
 */
class GrupoEstudiante
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="creado", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $creado;

    /**
     * @var \App\Entity\Academico\EstudianteCurso
     *
     * @ORM\ManyToOne(targetEntity="\App\Entity\Academico\EstudianteCurso")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_estudiante_curso", referencedColumnName="id")
     * })
     */
    private $estudiante_curso;

    /**
     * @var Grupo
     *
     * @ORM\ManyToOne(targetEntity="Grupo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_grupo", referencedColumnName="id")
     * })
     */
    private $grupo;

    /**
     * GrupoEstudiante constructor.
     * @param \DateTime $creado
     * @param \App\Entity\Academico\EstudianteCurso $estudiante_curso
     * @param Grupo $grupo
     */
    public function __construct(\App\Entity\Academico\EstudianteCurso $estudiante_curso, Grupo $grupo,\DateTime $creado)
    {
        $this->creado = $creado;
        $this->estudiante_curso = $estudiante_curso;
        $this->grupo = $grupo;
    }


    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return \DateTime
     */
    public function getCreado(): ?\DateTime
    {
        return $this->creado;
    }

    /**
     * @param \DateTime $creado
     * @return GrupoEstudiante
     */
    public function setCreado(\DateTime $creado): GrupoEstudiante
    {
        $this->creado = $creado;
        return $this;
    }

    /**
     * @return \App\Entity\Academico\EstudianteCurso
     */
    public function getEstudianteCurso(): ?\App\Entity\Academico\EstudianteCurso
    {
        return $this->estudiante_curso;
    }

    /**
     * @param \App\Entity\Academico\EstudianteCurso $estudiante_curso
     * @return GrupoEstudiante
     */
    public function setEstudianteCurso(\App\Entity\Academico\EstudianteCurso $estudiante_curso): GrupoEstudiante
    {
        $this->estudiante_curso = $estudiante_curso;
        return $this;
    }

    /**
     * @return Grupo
     */
    public function getGrupo(): ?Grupo
    {
        return $this->grupo;
    }

    /**
     * @param Grupo $grupo
     * @return GrupoEstudiante
     */
    public function setGrupo(Grupo $grupo): GrupoEstudiante
    {
        $this->grupo = $grupo;
        return $this;
    }

    /**
     * @return null|string
     */
    public function __toString(): ?string
    {
        return (string)$this->grupo;
    }



}
