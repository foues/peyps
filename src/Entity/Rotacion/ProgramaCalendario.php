<?php

namespace App\Entity\Rotacion;

use Doctrine\ORM\Mapping as ORM;

/**
 * ProgramaCalendario
 *
 * @ORM\Table(name="rotacion.programa_calendario", indexes={
 *     @ORM\Index(name="fk_programa_calendario", columns={"id_programa"}),
 *     @ORM\Index(name="fk_programa_periodo_ciclo", columns={"id_periodo_ciclo"}),
 *     @ORM\Index(name="fk_programa_calendario_grupo", columns={"id_grupo"})
 * })
 * @ORM\Entity
 */
class ProgramaCalendario
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="tipo", type="string", length=10, nullable=false)
     */
    private $tipo;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha", type="date", nullable=false)
     */
    private $fecha;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="string", length=500, nullable=true)
     */
    private $descripcion;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="creado", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $creado;

    /**
     * @var Programa
     *
     * @ORM\ManyToOne(targetEntity="Programa")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_programa", referencedColumnName="id")
     * })
     */
    private $programa;

    /**
     * @var \App\Entity\Academico\PeriodoCiclo
     *
     * @ORM\ManyToOne(targetEntity="\App\Entity\Academico\PeriodoCiclo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_periodo_ciclo", referencedColumnName="id")
     * })
     */
    private $periodo_ciclo;

    /**
     * @var Grupo
     *
     * @ORM\ManyToOne(targetEntity="Grupo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_grupo", referencedColumnName="id")
     * })
     */
    private $grupo;

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getTipo(): ?string
    {
        return $this->tipo;
    }

    /**
     * @param string $tipo
     * @return ProgramaCalendario
     */
    public function setTipo(string $tipo): ProgramaCalendario
    {
        $this->tipo = $tipo;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getFecha(): ?\DateTime
    {
        return $this->fecha;
    }

    /**
     * @param \DateTime $fecha
     * @return ProgramaCalendario
     */
    public function setFecha(\DateTime $fecha): ProgramaCalendario
    {
        $this->fecha = $fecha;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescripcion(): ?string
    {
        return $this->descripcion;
    }

    /**
     * @param string $descripcion
     * @return ProgramaCalendario
     */
    public function setDescripcion(string $descripcion): ProgramaCalendario
    {
        $this->descripcion = $descripcion;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreado(): ?\DateTime
    {
        return $this->creado;
    }

    /**
     * @param \DateTime $creado
     * @return ProgramaCalendario
     */
    public function setCreado(\DateTime $creado): ProgramaCalendario
    {
        $this->creado = $creado;
        return $this;
    }

    /**
     * @return Programa
     */
    public function getPrograma(): ?Programa
    {
        return $this->programa;
    }

    /**
     * @param Programa $programa
     * @return ProgramaCalendario
     */
    public function setPrograma(Programa $programa): ProgramaCalendario
    {
        $this->programa = $programa;
        return $this;
    }

    /**
     * @return \App\Entity\Academico\PeriodoCiclo
     */
    public function getPeriodoCiclo(): ?\App\Entity\Academico\PeriodoCiclo
    {
        return $this->periodo_ciclo;
    }

    /**
     * @param \App\Entity\Academico\PeriodoCiclo $periodo_ciclo
     * @return ProgramaCalendario
     */
    public function setPeriodoCiclo(\App\Entity\Academico\PeriodoCiclo $periodo_ciclo): ProgramaCalendario
    {
        $this->periodo_ciclo = $periodo_ciclo;
        return $this;
    }

    /**
     * @return Grupo
     */
    public function getGrupo(): ?Grupo
    {
        return $this->grupo;
    }

    /**
     * @param Grupo $grupo
     * @return ProgramaCalendario
     */
    public function setGrupo(Grupo $grupo): ProgramaCalendario
    {
        $this->grupo = $grupo;
        return $this;
    }

    /**
     * @return null|string
     */
    public function __toString(): ?string
    {
        return (string)$this->programa;
    }


}
