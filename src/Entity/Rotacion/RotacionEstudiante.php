<?php

namespace App\Entity\Rotacion;

use Doctrine\ORM\Mapping as ORM;

/**
 * RotacionEstudiante
 *
 * @ORM\Table(name="rotacion.rotacion_estudiante", indexes={
 *     @ORM\Index(name="fk_rotacion_estudiante", columns={"id_rotacion"}),
 *     @ORM\Index(name="fk_rotacion_estudiante_programa_institucion", columns={"id_programa_institucion"}),
 *     @ORM\Index(name="fk_rotacion_estudiante_grupo", columns={"id_grupo_estudiante"})
 * })
 * @ORM\Entity
 */
class RotacionEstudiante
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="creado", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $creado;

    /**
     * @var Rotacion
     *
     * @ORM\ManyToOne(targetEntity="Rotacion")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_rotacion", referencedColumnName="id")
     * })
     */
    private $rotacion;

    /**
     * @var ProgramaInstitucion
     *
     * @ORM\ManyToOne(targetEntity="ProgramaInstitucion")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_programa_institucion", referencedColumnName="id")
     * })
     */
    private $programa_institucion;

    /**
     * @var GrupoEstudiante
     *
     * @ORM\ManyToOne(targetEntity="GrupoEstudiante")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_grupo_estudiante", referencedColumnName="id")
     * })
     */
    private $grupo_estudiante;

    /**
     * RotacionEstudiante constructor.
     * @param GrupoEstudiante|null $grupo_estudiante
     * @param Rotacion|null $rotacion
     * @param ProgramaInstitucion|null $programa_institucion
     */
    public function __construct(GrupoEstudiante $grupo_estudiante = null, Rotacion $rotacion= null,ProgramaInstitucion $programa_institucion =null)
    {
        $this->rotacion = $rotacion;
        $this->programa_institucion = $programa_institucion;
        $this->grupo_estudiante = $grupo_estudiante;
    }

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return \DateTime
     */
    public function getCreado(): ?\DateTime
    {
        return $this->creado;
    }

    /**
     * @param \DateTime $creado
     * @return RotacionEstudiante
     */
    public function setCreado(\DateTime $creado): RotacionEstudiante
    {
        $this->creado = $creado;
        return $this;
    }

    /**
     * @return Rotacion
     */
    public function getRotacion(): ?Rotacion
    {
        return $this->rotacion;
    }

    /**
     * @param Rotacion $rotacion
     * @return RotacionEstudiante
     */
    public function setRotacion(Rotacion $rotacion): RotacionEstudiante
    {
        $this->rotacion = $rotacion;
        return $this;
    }

    /**
     * @return ProgramaInstitucion
     */
    public function getProgramaInstitucion(): ?ProgramaInstitucion
    {
        return $this->programa_institucion;
    }

    /**
     * @param ProgramaInstitucion $programa_institucion
     * @return RotacionEstudiante
     */
    public function setProgramaInstitucion(ProgramaInstitucion $programa_institucion): RotacionEstudiante
    {
        $this->programa_institucion = $programa_institucion;
        return $this;
    }

    /**
     * @return GrupoEstudiante
     */
    public function getGrupoEstudiante(): ?GrupoEstudiante
    {
        return $this->grupo_estudiante;
    }

    /**
     * @param GrupoEstudiante $grupo_estudiante
     * @return RotacionEstudiante
     */
    public function setGrupoEstudiante(GrupoEstudiante $grupo_estudiante): RotacionEstudiante
    {
        $this->grupo_estudiante = $grupo_estudiante;
        return $this;
    }

    /**
     * @return null|string
     */
    public function __toString(): ?string
    {
        return (string)$this->rotacion;
    }


}
