<?php

namespace App\Entity\Rotacion;

use Doctrine\ORM\Mapping as ORM;

/**
 * ProgramaInstitucion
 *
 * @ORM\Table(name="rotacion.programa_institucion", uniqueConstraints={
 *     @ORM\UniqueConstraint(name="unique_programa_institucion", columns={"id_programa", "id_institucion"})
 * }, indexes={
 *     @ORM\Index(name="fk_programa_institucion", columns={"id_programa"}),
 *     @ORM\Index(name="fk_institucion_programa", columns={"id_institucion"})
 * })
 * @ORM\Entity
 */
class ProgramaInstitucion
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="cantidad_estudiante", type="integer", nullable=false)
     */
    private $cantidad_estudiante;

    /**
     * @var bool
     *
     * @ORM\Column(name="activo", type="boolean", nullable=false)
     */
    private $activo = false;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="creado", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $creado;

    /**
     * @var Programa
     *
     * @ORM\ManyToOne(targetEntity="Programa")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_programa", referencedColumnName="id")
     * })
     */
    private $programa;

    /**
     * @var \App\Entity\Administracion\Institucion
     *
     * @ORM\ManyToOne(targetEntity="\App\Entity\Administracion\Institucion")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_institucion", referencedColumnName="id")
     * })
     */
    private $institucion;

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getCantidadEstudiante(): ?int
    {
        return $this->cantidad_estudiante;
    }

    /**
     * @param int $cantidad_estudiante
     * @return ProgramaInstitucion
     */
    public function setCantidadEstudiante(int $cantidad_estudiante): ProgramaInstitucion
    {
        $this->cantidad_estudiante = $cantidad_estudiante;
        return $this;
    }

    /**
     * @return bool
     */
    public function isActivo(): ?bool
    {
        return $this->activo;
    }

    /**
     * @param bool $activo
     * @return ProgramaInstitucion
     */
    public function setActivo(bool $activo): ProgramaInstitucion
    {
        $this->activo = $activo;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreado(): ?\DateTime
    {
        return $this->creado;
    }

    /**
     * @param \DateTime $creado
     * @return ProgramaInstitucion
     */
    public function setCreado(\DateTime $creado): ProgramaInstitucion
    {
        $this->creado = $creado;
        return $this;
    }

    /**
     * @return Programa
     */
    public function getPrograma(): ?Programa
    {
        return $this->programa;
    }

    /**
     * @param Programa $programa
     * @return ProgramaInstitucion
     */
    public function setPrograma(Programa $programa): ProgramaInstitucion
    {
        $this->programa = $programa;
        return $this;
    }

    /**
     * @return \App\Entity\Administracion\Institucion
     */
    public function getInstitucion(): ?\App\Entity\Administracion\Institucion
    {
        return $this->institucion;
    }

    /**
     * @param \App\Entity\Administracion\Institucion $institucion
     * @return ProgramaInstitucion
     */
    public function setInstitucion(\App\Entity\Administracion\Institucion $institucion): ProgramaInstitucion
    {
        $this->institucion = $institucion;
        return $this;
    }

    /**
     * @return null|string
     */
    public function __toString(): ?string
    {
        return (string)$this->id;
    }


}
