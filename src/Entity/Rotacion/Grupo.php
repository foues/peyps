<?php

namespace App\Entity\Rotacion;

use Doctrine\ORM\Mapping as ORM;

/**
 * Grupo
 *
 * @ORM\Table(name="rotacion.grupo", indexes={
 *     @ORM\Index(name="fk_grupo_periodo_ciclo", columns={"id_periodo_ciclo"}),
 *     @ORM\Index(name="fk_grupo_plan_estudio_curso", columns={"id_plan_estudio_curso"}),
 *     @ORM\Index(name="fk_grupo_padre", columns={"id_padre"})
 * })
 * @ORM\Entity(repositoryClass="App\Repository\Rotacion\GrupoRepository")
 */
class Grupo
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=50, nullable=false)
     */
    private $nombre;

    /**
     * @var bool
     *
     * @ORM\Column(name="rotable", type="boolean", nullable=false)
     */
    private $rotable = false;

    /**
     * @var bool
     *
     * @ORM\Column(name="calificable", type="boolean", nullable=false)
     */
    private $calificable = false;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="creado", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $creado;

    /**
     * @var \App\Entity\Academico\PeriodoCiclo
     *
     * @ORM\ManyToOne(targetEntity="\App\Entity\Academico\PeriodoCiclo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_periodo_ciclo", referencedColumnName="id")
     * })
     */
    private $periodo_ciclo;

    /**
     * @var \App\Entity\Academico\PlanEstudioCurso
     *
     * @ORM\ManyToOne(targetEntity="\App\Entity\Academico\PlanEstudioCurso")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_plan_estudio_curso", referencedColumnName="id")
     * })
     */
    private $plan_estudio_curso;

    /**
     * @var Grupo
     *
     * @ORM\ManyToOne(targetEntity="Grupo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_padre", referencedColumnName="id")
     * })
     */
    private $padre;

    /**
     * Grupo constructor.
     * @param Grupo $padre
     * @param \App\Entity\Academico\PeriodoCiclo $periodo_ciclo
     * @param string $nombre
     * @param bool $rotable
     */
    public function __construct(Grupo $padre = null, \App\Entity\Academico\PeriodoCiclo $periodo_ciclo = null, string $nombre = null, bool $rotable = null, bool $calificable = null )
    {
        if (!is_null($nombre)) $this->nombre = $nombre;
        if (!is_null($rotable)) $this->rotable = $rotable;
        if (!is_null($calificable)) $this->$calificable = $calificable;
        if (!is_null($periodo_ciclo)) $this->periodo_ciclo = $periodo_ciclo;
        if (!is_null($padre)) $this->padre = $padre;
    }

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    /**
     * @param string $nombre
     * @return Grupo
     */
    public function setNombre(string $nombre): Grupo
    {
        $this->nombre = $nombre;
        return $this;
    }

    /**
     * @return bool
     */
    public function isRotable(): ?bool
    {
        return $this->rotable;
    }

    /**
     * @param bool $rotable
     * @return Grupo
     */
    public function setRotable(bool $rotable): Grupo
    {
        $this->rotable = $rotable;
        return $this;
    }

    /**
     * @return bool
     */
    public function isCalificable(): ?bool
    {
        return $this->calificable;
    }

    /**
     * @param bool $calificable
     * @return Grupo
     */
    public function setCalificable(bool $calificable): Grupo
    {
        $this->calificable = $calificable;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreado(): ?\DateTime
    {
        return $this->creado;
    }

    /**
     * @param \DateTime $creado
     * @return Grupo
     */
    public function setCreado(\DateTime $creado): Grupo
    {
        $this->creado = $creado;
        return $this;
    }

    /**
     * @return \App\Entity\Academico\PeriodoCiclo
     */
    public function getPeriodoCiclo(): ?\App\Entity\Academico\PeriodoCiclo
    {
        return $this->periodo_ciclo;
    }

    /**
     * @param \App\Entity\Academico\PeriodoCiclo $periodo_ciclo
     * @return Grupo
     */
    public function setPeriodoCiclo(\App\Entity\Academico\PeriodoCiclo $periodo_ciclo): Grupo
    {
        $this->periodo_ciclo = $periodo_ciclo;
        return $this;
    }

    /**
     * @return \App\Entity\Academico\PlanEstudioCurso
     */
    public function getPlanEstudioCurso(): ?\App\Entity\Academico\PlanEstudioCurso
    {
        return $this->plan_estudio_curso;
    }

    /**
     * @param \App\Entity\Academico\PlanEstudioCurso $plan_estudio_curso
     * @return Grupo
     */
    public function setPlanEstudioCurso(?\App\Entity\Academico\PlanEstudioCurso $plan_estudio_curso): Grupo
    {
        $this->plan_estudio_curso = $plan_estudio_curso;
        return $this;
    }

    /**
     * @return Grupo
     */
    public function getPadre(): ?Grupo
    {
        return $this->padre;
    }

    /**
     * @param Grupo $padre
     * @return Grupo
     */
    public function setPadre(Grupo $padre): Grupo
    {
        $this->padre = $padre;
        return $this;
    }

    /**
     * @return string
     */
    public function __toString(): ?string
    {
        return (string)$this->periodo_ciclo;
    }


}
