<?php

namespace App\Entity\Rotacion;

use Doctrine\ORM\Mapping as ORM;

/**
 * Rotacion
 *
 * @ORM\Table(name="rotacion.rotacion", indexes={
 *     @ORM\Index(name="fk_rotacion_planificacion", columns={"id_planificacion"}),
 *     @ORM\Index(name="fk_rotacion_grupo", columns={"id_grupo"}),
 *     @ORM\Index(name="fk_rotacion_padre", columns={"id_padre"})
 * })
 * @ORM\Entity
 */
class Rotacion
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="numero", type="integer", nullable=false)
     */
    private $numero;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="inicio", type="date", nullable=false)
     */
    private $inicio;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fin", type="date", nullable=false)
     */
    private $fin;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="creado", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $creado;

    /**
     * @var Planificacion
     *
     * @ORM\ManyToOne(targetEntity="Planificacion")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_planificacion", referencedColumnName="id")
     * })
     */
    private $planificacion;

    /**
     * @var Grupo
     *
     * @ORM\ManyToOne(targetEntity="Grupo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_grupo", referencedColumnName="id")
     * })
     */
    private $grupo;

    /**
     * @var Rotacion
     *
     * @ORM\ManyToOne(targetEntity="Rotacion")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_padre", referencedColumnName="id")
     * })
     */
    private $padre;

    /**
     * Rotacion constructor.
     * @param int $numero
     * @param \DateTime $inicio
     * @param \DateTime $fin
     * @param Planificacion $planificacion
     */
    public function __construct(Grupo $grupo,int $numero, Planificacion $planificacion, \DateTime $inicio, \DateTime $fin)
    {
        $this->numero = $numero;
        $this->grupo = $grupo;
        $this->inicio = $inicio;
        $this->fin = $fin;
        $this->planificacion = $planificacion;
    }

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getNumero(): ?int
    {
        return $this->numero;
    }

    /**
     * @param int $numero
     * @return Rotacion
     */
    public function setNumero(int $numero): Rotacion
    {
        $this->numero = $numero;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getInicio(): ?\DateTime
    {
        return $this->inicio;
    }

    /**
     * @param \DateTime $inicio
     * @return Rotacion
     */
    public function setInicio(\DateTime $inicio): Rotacion
    {
        $this->inicio = $inicio;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getFin(): ?\DateTime
    {
        return $this->fin;
    }

    /**
     * @param \DateTime $fin
     * @return Rotacion
     */
    public function setFin(\DateTime $fin): Rotacion
    {
        $this->fin = $fin;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreado(): ?\DateTime
    {
        return $this->creado;
    }

    /**
     * @param \DateTime $creado
     * @return Rotacion
     */
    public function setCreado(\DateTime $creado): Rotacion
    {
        $this->creado = $creado;
        return $this;
    }

    /**
     * @return Planificacion
     */
    public function getPlanificacion(): ?Planificacion
    {
        return $this->planificacion;
    }

    /**
     * @param Planificacion $planificacion
     * @return Rotacion
     */
    public function setPlanificacion(Planificacion $planificacion): Rotacion
    {
        $this->planificacion = $planificacion;
        return $this;
    }

    /**
     * @return Grupo
     */
    public function getGrupo(): ?Grupo
    {
        return $this->grupo;
    }

    /**
     * @param Grupo $grupo
     * @return Rotacion
     */
    public function setGrupo(Grupo $grupo): Rotacion
    {
        $this->grupo = $grupo;
        return $this;
    }

    /**
     * @return Rotacion
     */
    public function getPadre(): ?Rotacion
    {
        return $this->padre;
    }

    /**
     * @param Rotacion $padre
     * @return Rotacion
     */
    public function setPadre(Rotacion $padre): Rotacion
    {
        $this->padre = $padre;
        return $this;
    }

    /**
     * @return string
     */
    public function __toString(): ?string
    {
        return (string)$this->numero;
    }


}
