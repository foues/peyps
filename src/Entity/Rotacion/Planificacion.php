<?php

namespace App\Entity\Rotacion;

use Doctrine\ORM\Mapping as ORM;

/**
 * Planificacion
 *
 * @ORM\Table(name="rotacion.planificacion", indexes={
 *     @ORM\Index(name="fk_planificacion_periodo_ciclo", columns={"id_periodo_ciclo"})
 * })
 * @ORM\Entity
 */
class Planificacion
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var bool
     *
     * @ORM\Column(name="confirmado", type="boolean", nullable=false)
     */
    private $confirmado = false;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="creado", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $creado;

    /**
     * @var \App\Entity\Academico\PeriodoCiclo
     *
     * @ORM\ManyToOne(targetEntity="\App\Entity\Academico\PeriodoCiclo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_periodo_ciclo", referencedColumnName="id")
     * })
     */
    private $periodo_ciclo;

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return bool
     */
    public function isConfirmado(): ?bool
    {
        return $this->confirmado;
    }

    /**
     * @param bool $confirmado
     * @return Planificacion
     */
    public function setConfirmado(bool $confirmado): Planificacion
    {
        $this->confirmado = $confirmado;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreado(): ?\DateTime
    {
        return $this->creado;
    }

    /**
     * @param \DateTime $creado
     * @return Planificacion
     */
    public function setCreado(\DateTime $creado): Planificacion
    {
        $this->creado = $creado;
        return $this;
    }

    /**
     * @return \App\Entity\Academico\PeriodoCiclo
     */
    public function getPeriodoCiclo(): ?\App\Entity\Academico\PeriodoCiclo
    {
        return $this->periodo_ciclo;
    }

    /**
     * @param \App\Entity\Academico\PeriodoCiclo $periodo_ciclo
     * @return Planificacion
     */
    public function setPeriodoCiclo(\App\Entity\Academico\PeriodoCiclo $periodo_ciclo): Planificacion
    {
        $this->periodo_ciclo = $periodo_ciclo;
        return $this;
    }

}
