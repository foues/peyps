<?php

namespace App\Entity\Academico;

use Doctrine\ORM\Mapping as ORM;

/**
 * PlanEstudioCurso
 *
 * @ORM\Table(name="odontoped_academico.plan_estudio_curso", uniqueConstraints={
 *     @ORM\UniqueConstraint(name="unique_plan_estudio_curso", columns={"id_curso", "id_plan_estudio"})
 * }, indexes={
 *     @ORM\Index(name="fk_curso_plan_estudio", columns={"id_curso"}),
 *     @ORM\Index(name="fk_plan_estudio_curso", columns={"id_plan_estudio"}),
 *     @ORM\Index(name="fk_plan_estudio_ciclo", columns={"id_plan_estudio_ciclo"})
 * })
 * @ORM\Entity(repositoryClass="App\Repository\Academico\PlanEstudioCursoRepository")
 */
class PlanEstudioCurso
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="creado", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $creado;

    /**
     * @var Curso
     *
     * @ORM\ManyToOne(targetEntity="Curso")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_curso", referencedColumnName="id")
     * })
     */
    private $curso;

    /**
     * @var PlanEstudio
     *
     * @ORM\ManyToOne(targetEntity="PlanEstudio")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_plan_estudio", referencedColumnName="id")
     * })
     */
    private $plan_estudio;

    /**
     * @var PlanEstudioCiclo
     *
     * @ORM\ManyToOne(targetEntity="PlanEstudioCiclo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_plan_estudio_ciclo", referencedColumnName="id")
     * })
     */
    private $plan_estudio_ciclo;

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return \DateTime
     */
    public function getCreado(): ?\DateTime
    {
        return $this->creado;
    }

    /**
     * @param \DateTime $creado
     * @return PlanEstudioCurso
     */
    public function setCreado(\DateTime $creado): PlanEstudioCurso
    {
        $this->creado = $creado;
        return $this;
    }

    /**
     * @return Curso
     */
    public function getCurso(): ?Curso
    {
        return $this->curso;
    }

    /**
     * @param Curso $curso
     * @return PlanEstudioCurso
     */
    public function setCurso(Curso $curso): PlanEstudioCurso
    {
        $this->curso = $curso;
        return $this;
    }

    /**
     * @return PlanEstudio
     */
    public function getPlanEstudio(): ?PlanEstudio
    {
        return $this->plan_estudio;
    }

    /**
     * @param PlanEstudio $plan_estudio
     * @return PlanEstudioCurso
     */
    public function setPlanEstudio(PlanEstudio $plan_estudio): PlanEstudioCurso
    {
        $this->plan_estudio = $plan_estudio;
        return $this;
    }

    /**
     * @return PlanEstudioCiclo
     */
    public function getPlanEstudioCiclo(): ?PlanEstudioCiclo
    {
        return $this->plan_estudio_ciclo;
    }

    /**
     * @param PlanEstudioCiclo $plan_estudio_ciclo
     * @return PlanEstudioCurso
     */
    public function setPlanEstudioCiclo(PlanEstudioCiclo $plan_estudio_ciclo): PlanEstudioCurso
    {
        $this->plan_estudio_ciclo = $plan_estudio_ciclo;
        return $this;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        if ($this->plan_estudio && $this->curso)
            return $this->curso->getCodigo()
                . ' - ' . $this->curso->getNombre()
                . ' - ' . $this->plan_estudio->getCarrera()->getCodigo()
                . ' - ' . $this->plan_estudio->getAnio();
        else
            return '';
    }

}
