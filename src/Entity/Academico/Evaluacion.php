<?php

namespace App\Entity\Academico;

use Doctrine\ORM\Mapping as ORM;

/**
 * Evaluacion
 *
 * @ORM\Table(name="odontoped_academico.evaluacion", indexes={
 *     @ORM\Index(name="fk_evaluacion_unidad_integracion", columns={"id_unidad_integracion"}),
 *     @ORM\Index(name="fk_evaluacion_tipo", columns={"id_evaluacion_tipo"}),
 *     @ORM\Index(name="fk_evaluacion_padre", columns={"id_padre"})
 * })
 * @ORM\Entity(repositoryClass="App\Repository\Academico\EvaluacionRepository")
 */
class Evaluacion
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=255, nullable=false)
     */
    private $nombre;

    /**
     * @var string|null
     *
     * @ORM\Column(name="descripcion", type="string", length=500, nullable=true)
     */
    private $descripcion;

    /**
     * @var float
     *
     * @ORM\Column(name="ponderacion", type="float", precision=10, scale=0, nullable=false)
     */
    private $ponderacion;

    /**
     * @var bool
     *
     * @ORM\Column(name="grupal", type="boolean", nullable=false, options={"default"=false})
     */
    private $grupal = false;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="creado", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $creado;

    /**
     * @var UnidadIntegracion
     *
     * @ORM\ManyToOne(targetEntity="UnidadIntegracion")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_unidad_integracion", referencedColumnName="id")
     * })
     */
    private $unidad_integracion;

    /**
     * @var EvaluacionTipo
     *
     * @ORM\ManyToOne(targetEntity="EvaluacionTipo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_evaluacion_tipo", referencedColumnName="id")
     * })
     */
    private $evaluacion_tipo;


    /**
     * @var Evaluacion
     *
     * @ORM\ManyToOne(targetEntity="Evaluacion")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_padre", referencedColumnName="id")
     * })
     */
    private $padre;

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    /**
     * @param string $nombre
     * @return Evaluacion
     */
    public function setNombre(string $nombre): Evaluacion
    {
        $this->nombre = $nombre;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getDescripcion(): ?string
    {
        return $this->descripcion;
    }

    /**
     * @param null|string $descripcion
     * @return Evaluacion
     */
    public function setDescripcion(?string $descripcion): Evaluacion
    {
        $this->descripcion = $descripcion;
        return $this;
    }

    /**
     * @return float
     */
    public function getPonderacion(): ?float
    {
        return $this->ponderacion;
    }

    /**
     * @param float $ponderacion
     * @return Evaluacion
     */
    public function setPonderacion(float $ponderacion): Evaluacion
    {
        $this->ponderacion = $ponderacion;
        return $this;
    }

    /**
     * @return bool
     */
    public function isGrupal(): ?bool
    {
        return $this->grupal;
    }

    /**
     * @param bool $grupal
     * @return Evaluacion
     */
    public function setGrupal(bool $grupal): Evaluacion
    {
        $this->grupal = $grupal;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreado(): ?\DateTime
    {
        return $this->creado;
    }

    /**
     * @param \DateTime $creado
     * @return Evaluacion
     */
    public function setCreado(\DateTime $creado): Evaluacion
    {
        $this->creado = $creado;
        return $this;
    }

    /**
     * @return UnidadIntegracion
     */
    public function getUnidadIntegracion(): ?UnidadIntegracion
    {
        return $this->unidad_integracion;
    }

    /**
     * @param UnidadIntegracion $unidad_integracion
     * @return Evaluacion
     */
    public function setUnidadIntegracion(UnidadIntegracion $unidad_integracion): Evaluacion
    {
        $this->unidad_integracion = $unidad_integracion;
        return $this;
    }

    /**
     * @return EvaluacionTipo
     */
    public function getEvaluacionTipo(): ?EvaluacionTipo
    {
        return $this->evaluacion_tipo;
    }

    /**
     * @param EvaluacionTipo $evaluacion_tipo
     * @return Evaluacion
     */
    public function setEvaluacionTipo(EvaluacionTipo $evaluacion_tipo): Evaluacion
    {
        $this->evaluacion_tipo = $evaluacion_tipo;
        return $this;
    }

    /**
     * @return Evaluacion
     */
    public function getPadre(): ?Evaluacion
    {
        return $this->padre;
    }

    /**
     * @param Evaluacion $padre
     * @return Evaluacion
     */
    public function setPadre(Evaluacion $padre): Evaluacion
    {
        $this->padre = $padre;
        return $this;
    }

    /**
     * @return string
     */
    public function __toString(): ?string
    {
        return (string)$this->nombre;
    }

}
