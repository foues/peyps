<?php

namespace App\Entity\Academico;

use Doctrine\ORM\Mapping as ORM;

/**
 * PeriodoCiclo
 *
 * @ORM\Table(name="odontoped_academico.periodo_ciclo", uniqueConstraints={
 *     @ORM\UniqueConstraint(name="id_periodo", columns={"id_periodo"})
 * })
 * @ORM\Entity(repositoryClass="App\Repository\Academico\PeriodoCicloRepository")
 */
class PeriodoCiclo
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="tipo", type="string", length=5, nullable=false)
     */
    private $tipo;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="creado", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $creado;

    /**
     * @var Periodo
     *
     * @ORM\ManyToOne(targetEntity="Periodo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_periodo", referencedColumnName="id")
     * })
     */
    private $periodo;

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getTipo(): ?string
    {
        return $this->tipo;
    }

    /**
     * @param string $tipo
     * @return PeriodoCiclo
     */
    public function setTipo(string $tipo): PeriodoCiclo
    {
        $this->tipo = $tipo;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreado(): ?\DateTime
    {
        return $this->creado;
    }

    /**
     * @param \DateTime $creado
     * @return PeriodoCiclo
     */
    public function setCreado(\DateTime $creado): PeriodoCiclo
    {
        $this->creado = $creado;
        return $this;
    }

    /**
     * @return Periodo
     */
    public function getPeriodo(): ?Periodo
    {
        return $this->periodo;
    }

    /**
     * @param Periodo $periodo
     * @return PeriodoCiclo
     */
    public function setPeriodo(Periodo $periodo): PeriodoCiclo
    {
        $this->periodo = $periodo;
        return $this;
    }

    /**
     * @return null|string
     */
    public function __toString(): ?string
    {
        return (string)$this->periodo;
    }


}
