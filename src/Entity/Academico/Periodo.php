<?php

namespace App\Entity\Academico;

use Doctrine\ORM\Mapping as ORM;

/**
 * Periodo
 *
 * @ORM\Table(name="odontoped_academico.periodo")
 * @ORM\Entity
 */
class Periodo
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=255, nullable=false)
     */
    private $nombre;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="inicio", type="date", nullable=false)
     */
    private $inicio;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fin", type="date", nullable=false)
     */
    private $fin;

    /**
     * @var bool
     *
     * @ORM\Column(name="activo", type="boolean", nullable=false, options={"default"=true})
     */
    private $activo = true;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="creado", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $creado;

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    /**
     * @param string $nombre
     * @return Periodo
     */
    public function setNombre(string $nombre): Periodo
    {
        $this->nombre = $nombre;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getInicio(): ?\DateTime
    {
        return $this->inicio;
    }

    /**
     * @param \DateTime $inicio
     * @return Periodo
     */
    public function setInicio(\DateTime $inicio): Periodo
    {
        $this->inicio = $inicio;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getFin(): ?\DateTime
    {
        return $this->fin;
    }

    /**
     * @param \DateTime $fin
     * @return Periodo
     */
    public function setFin(\DateTime $fin): Periodo
    {
        $this->fin = $fin;
        return $this;
    }

    /**
     * @return bool
     */
    public function isActivo(): bool
    {
        return $this->activo;
    }

    /**
     * @param bool $activo
     * @return Periodo
     */
    public function setActivo(bool $activo): Periodo
    {
        $this->activo = $activo;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreado(): ?\DateTime
    {
        return $this->creado;
    }

    /**
     * @param \DateTime $creado
     * @return Periodo
     */
    public function setCreado(\DateTime $creado): Periodo
    {
        $this->creado = $creado;
        return $this;
    }

    /**
     * @return string
     */
    public function __toString(): ?string
    {
        return (string)$this->nombre;
    }
}
