<?php

namespace App\Entity\Academico;

use Doctrine\ORM\Mapping as ORM;

/**
 * Calificacion
 *
 * @ORM\Table(name="odontoped_academico.calificacion", indexes={
 *     @ORM\Index(name="fk_calificacion_estudiante_curso", columns={"id_estudiante_curso"}),
 *     @ORM\Index(name="fk_calificacion_evaluacion", columns={"id_evaluacion"}),
 *     @ORM\Index(name="fk_calificacion_grupo", columns={"id_calificacion_grupo"}),
 *     @ORM\Index(name="fk_calificacion_padre", columns={"id_padre"})
 * })
 * @ORM\Entity(repositoryClass="App\Repository\Academico\CalificacionRepository")
 */
class Calificacion
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nota", type="decimal", precision=8, scale=2, nullable=false)
     */
    private $nota;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha", type="date", nullable=false)
     */
    private $fecha;

    /**
     * @var bool
     *
     * @ORM\Column(name="permiso", type="boolean", nullable=false)
     */
    private $permiso = false;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="creado", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $creado;

    /**
     * @var EstudianteCurso
     *
     * @ORM\ManyToOne(targetEntity="EstudianteCurso")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_estudiante_curso", referencedColumnName="id")
     * })
     */
    private $estudiante_curso;

    /**
     * @var Evaluacion
     *
     * @ORM\ManyToOne(targetEntity="Evaluacion")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_evaluacion", referencedColumnName="id")
     * })
     */
    private $evaluacion;

    /**
     * @var CalificacionGrupo
     *
     * @ORM\ManyToOne(targetEntity="CalificacionGrupo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_calificacion_grupo", referencedColumnName="id")
     * })
     */
    private $calificacion_grupo;

    /**
     * @var Calificacion
     *
     * @ORM\ManyToOne(targetEntity="Calificacion")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_padre", referencedColumnName="id")
     * })
     */
    private $padre;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Academico\CalificacionPenalizacion", mappedBy="calificacion")
     */
    private $penalizaciones;


    public function __construct()
    {
        $this->penalizaciones = array();
    }

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getNota(): ?string
    {
        return $this->nota;
    }

    /**
     * @param string $nota
     * @return Calificacion
     */
    public function setNota(string $nota): Calificacion
    {
        $this->nota = $nota;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getFecha(): ?\DateTime
    {
        return $this->fecha;
    }

    /**
     * @param \DateTime $fecha
     * @return Calificacion
     */
    public function setFecha(\DateTime $fecha): Calificacion
    {
        $this->fecha = $fecha;
        return $this;
    }

    /**
     * @return bool
     */
    public function isPermiso(): ?bool
    {
        return $this->permiso;
    }

    /**
     * @param bool $permiso
     * @return Calificacion
     */
    public function setPermiso(bool $permiso): Calificacion
    {
        $this->permiso = $permiso;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreado(): ?\DateTime
    {
        return $this->creado;
    }

    /**
     * @param \DateTime $creado
     * @return Calificacion
     */
    public function setCreado(\DateTime $creado): Calificacion
    {
        $this->creado = $creado;
        return $this;
    }

    /**
     * @return EstudianteCurso
     */
    public function getEstudianteCurso(): ?EstudianteCurso
    {
        return $this->estudiante_curso;
    }

    /**
     * @param EstudianteCurso $estudiante_curso
     * @return Calificacion
     */
    public function setEstudianteCurso(EstudianteCurso $estudiante_curso): Calificacion
    {
        $this->estudiante_curso = $estudiante_curso;
        return $this;
    }

    /**
     * @return Evaluacion
     */
    public function getEvaluacion(): ?Evaluacion
    {
        return $this->evaluacion;
    }

    /**
     * @param Evaluacion $evaluacion
     * @return Calificacion
     */
    public function setEvaluacion(Evaluacion $evaluacion): Calificacion
    {
        $this->evaluacion = $evaluacion;
        return $this;
    }

    /**
     * @return CalificacionGrupo
     */
    public function getCalificacionGrupo(): ?CalificacionGrupo
    {
        return $this->calificacion_grupo;
    }

    /**
     * @param CalificacionGrupo $calificacion_grupo
     * @return Calificacion
     */
    public function setCalificacionGrupo(CalificacionGrupo $calificacion_grupo): Calificacion
    {
        $this->calificacion_grupo = $calificacion_grupo;
        return $this;
    }

    /**
     * @return Calificacion
     */
    public function getPadre(): ?Calificacion
    {
        return $this->padre;
    }

    /**
     * @param Calificacion $padre
     * @return Calificacion
     */
    public function setPadre(Calificacion $padre): Calificacion
    {
        $this->padre = $padre;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPenalizaciones()
    {
        return $this->penalizaciones;
    }

    /**
     * @param mixed $penalizaciones
     */
    public function setPenalizaciones($penalizaciones): void
    {
        $this->penalizaciones = $penalizaciones;
    }


}
