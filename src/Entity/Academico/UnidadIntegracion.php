<?php

namespace App\Entity\Academico;

use Doctrine\ORM\Mapping as ORM;

/**
 * UnidadIntegracion
 *
 * @ORM\Table(name="odontoped_academico.unidad_integracion", indexes={
 *     @ORM\Index(name="fk_unidad_integracion_plan_estudio", columns={"id_plan_estudio_curso"}),
 *     @ORM\Index(name="fk_unidad_integracion_periodo_ciclo", columns={"id_periodo_ciclo"}),
 *     @ORM\Index(name="fk_unidad_integracion_programa", columns={"id_programa"})
 * })
 * @ORM\Entity(repositoryClass="App\Repository\Academico\UnidadIntegracionRepository")
 */
class UnidadIntegracion
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=255, nullable=false)
     */
    private $nombre;

    /**
     * @var float
     *
     * @ORM\Column(name="ponderacion", type="float", precision=10, scale=0, nullable=false)
     */
    private $ponderacion;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="creado", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $creado;

    /**
     * @var PlanEstudioCurso
     *
     * @ORM\ManyToOne(targetEntity="PlanEstudioCurso")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_plan_estudio_curso", referencedColumnName="id")
     * })
     */
    private $plan_estudio_curso;

    /**
     * @var PeriodoCiclo
     *
     * @ORM\ManyToOne(targetEntity="PeriodoCiclo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_periodo_ciclo", referencedColumnName="id")
     * })
     */
    private $periodo_ciclo;


    /**
     * @var \App\Entity\Rotacion\Programa
     *
     * @ORM\ManyToOne(targetEntity="\App\Entity\Rotacion\Programa")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_programa", referencedColumnName="id")
     * })
     */
    private $programa;

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    /**
     * @param string $nombre
     * @return UnidadIntegracion
     */
    public function setNombre(string $nombre): UnidadIntegracion
    {
        $this->nombre = $nombre;
        return $this;
    }

    /**
     * @return float
     */
    public function getPonderacion(): ?float
    {
        return $this->ponderacion;
    }

    /**
     * @param float $ponderacion
     * @return UnidadIntegracion
     */
    public function setPonderacion(float $ponderacion): UnidadIntegracion
    {
        $this->ponderacion = $ponderacion;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreado(): ?\DateTime
    {
        return $this->creado;
    }

    /**
     * @param \DateTime $creado
     * @return UnidadIntegracion
     */
    public function setCreado(\DateTime $creado): UnidadIntegracion
    {
        $this->creado = $creado;
        return $this;
    }

    /**
     * @return PlanEstudioCurso
     */
    public function getPlanEstudioCurso(): ?PlanEstudioCurso
    {
        return $this->plan_estudio_curso;
    }

    /**
     * @param PlanEstudioCurso $plan_estudio_curso
     * @return UnidadIntegracion
     */
    public function setPlanEstudioCurso(PlanEstudioCurso $plan_estudio_curso): UnidadIntegracion
    {
        $this->plan_estudio_curso = $plan_estudio_curso;
        return $this;
    }

    /**
     * @return PeriodoCiclo
     */
    public function getPeriodoCiclo(): ?PeriodoCiclo
    {
        return $this->periodo_ciclo;
    }

    /**
     * @param PeriodoCiclo $periodo_ciclo
     * @return UnidadIntegracion
     */
    public function setPeriodoCiclo(PeriodoCiclo $periodo_ciclo): UnidadIntegracion
    {
        $this->periodo_ciclo = $periodo_ciclo;
        return $this;
    }

    /**
     * @return \App\Entity\Rotacion\Programa
     */
    public function getPrograma(): ?\App\Entity\Rotacion\Programa
    {
        return $this->programa;
    }

    /**
     * @param \App\Entity\Rotacion\Programa $programa
     * @return UnidadIntegracion
     */
    public function setPrograma(\App\Entity\Rotacion\Programa $programa): UnidadIntegracion
    {
        $this->programa = $programa;
        return $this;
    }


}
