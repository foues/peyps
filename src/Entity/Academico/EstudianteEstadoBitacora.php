<?php

namespace App\Entity\Academico;

use Doctrine\ORM\Mapping as ORM;

/**
 * EstudianteEstadoBitacora
 *
 * @ORM\Table(name="odontoped_academico.estudiante_estado_bitacora", indexes={
 *     @ORM\Index(name="fk_estudiante_estado_bitacora_plan_estudio", columns={"id_estudiante_plan_estudio"}),
 *     @ORM\Index(name="fk_estudiante_estado_bitacora", columns={"id_estudiante_estado"})
 * })
 * @ORM\Entity
 */
class EstudianteEstadoBitacora
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="creado", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $creado;

    /**
     * @var EstudiantePlanEstudio
     *
     * @ORM\ManyToOne(targetEntity="EstudiantePlanEstudio")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_estudiante_plan_estudio", referencedColumnName="id")
     * })
     */
    private $estudiante_plan_estudio;

    /**
     * @var EstudianteEstado
     *
     * @ORM\ManyToOne(targetEntity="EstudianteEstado")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_estudiante_estado", referencedColumnName="id")
     * })
     */
    private $estudiante_estado;

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return \DateTime
     */
    public function getCreado(): ?\DateTime
    {
        return $this->creado;
    }

    /**
     * @param \DateTime $creado
     * @return EstudianteEstadoBitacora
     */
    public function setCreado(\DateTime $creado): EstudianteEstadoBitacora
    {
        $this->creado = $creado;
        return $this;
    }

    /**
     * @return EstudiantePlanEstudio
     */
    public function getEstudiantePlanEstudio(): ?EstudiantePlanEstudio
    {
        return $this->estudiante_plan_estudio;
    }

    /**
     * @param EstudiantePlanEstudio $estudiante_plan_estudio
     * @return EstudianteEstadoBitacora
     */
    public function setEstudiantePlanEstudio(EstudiantePlanEstudio $estudiante_plan_estudio): EstudianteEstadoBitacora
    {
        $this->estudiante_plan_estudio = $estudiante_plan_estudio;
        return $this;
    }

    /**
     * @return EstudianteEstado
     */
    public function getEstudianteEstado(): ?EstudianteEstado
    {
        return $this->estudiante_estado;
    }

    /**
     * @param EstudianteEstado $estudiante_estado
     * @return EstudianteEstadoBitacora
     */
    public function setEstudianteEstado(EstudianteEstado $estudiante_estado): EstudianteEstadoBitacora
    {
        $this->estudiante_estado = $estudiante_estado;
        return $this;
    }


}
