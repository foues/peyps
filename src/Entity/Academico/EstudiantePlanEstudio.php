<?php

namespace App\Entity\Academico;

use Doctrine\ORM\Mapping as ORM;

/**
 * EstudiantePlanEstudio
 *
 * @ORM\Table(name="odontoped_academico.estudiante_plan_estudio", uniqueConstraints={
 *     @ORM\UniqueConstraint(name="unique_estudiante_plan_estudio", columns={"id_estudiante", "id_plan_estudio"})
 * }, indexes={
 *     @ORM\Index(name="fk_estudiante_plan_estudio", columns={"id_estudiante"}),
 *     @ORM\Index(name="fk_plan_estudio_estudiante", columns={"id_plan_estudio"}),
 *     @ORM\Index(name="fk_estudiante_plan_estudio_estado", columns={"id_estudiante_estado"})
 * })
 * @ORM\Entity(repositoryClass="App\Repository\Academico\EstudiantePlanEstudioRepository")
 */
class EstudiantePlanEstudio
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="cum", type="decimal", precision=8, scale=2, nullable=false, options={"default"="0.00"})
     */
    private $cum = '0.00';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="creado", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $creado;

    /**
     * @var Estudiante
     *
     * @ORM\ManyToOne(targetEntity="Estudiante")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_estudiante", referencedColumnName="id")
     * })
     */
    private $estudiante;

    /**
     * @var PlanEstudio
     *
     * @ORM\ManyToOne(targetEntity="PlanEstudio")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_plan_estudio", referencedColumnName="id")
     * })
     */
    private $plan_estudio;

    /**
     * @var EstudianteEstado
     *
     * @ORM\ManyToOne(targetEntity="EstudianteEstado")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_estudiante_estado", referencedColumnName="id")
     * })
     */
    private $estudiante_estado;

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getCum(): ?string
    {
        return $this->cum;
    }

    /**
     * @param string $cum
     * @return EstudiantePlanEstudio
     */
    public function setCum(string $cum): EstudiantePlanEstudio
    {
        $this->cum = $cum;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreado(): ?\DateTime
    {
        return $this->creado;
    }

    /**
     * @param \DateTime $creado
     * @return EstudiantePlanEstudio
     */
    public function setCreado(\DateTime $creado): EstudiantePlanEstudio
    {
        $this->creado = $creado;
        return $this;
    }

    /**
     * @return Estudiante
     */
    public function getEstudiante(): ?Estudiante
    {
        return $this->estudiante;
    }

    /**
     * @param Estudiante $estudiante
     * @return EstudiantePlanEstudio
     */
    public function setEstudiante(Estudiante $estudiante): EstudiantePlanEstudio
    {
        $this->estudiante = $estudiante;
        return $this;
    }

    /**
     * @return PlanEstudio
     */
    public function getPlanEstudio(): ?PlanEstudio
    {
        return $this->plan_estudio;
    }

    /**
     * @param PlanEstudio $plan_estudio
     * @return EstudiantePlanEstudio
     */
    public function setPlanEstudio(PlanEstudio $plan_estudio): EstudiantePlanEstudio
    {
        $this->plan_estudio = $plan_estudio;
        return $this;
    }

    /**
     * @return EstudianteEstado
     */
    public function getEstudianteEstado(): ?EstudianteEstado
    {
        return $this->estudiante_estado;
    }

    /**
     * @param EstudianteEstado $estudiante_estado
     * @return EstudiantePlanEstudio
     */
    public function setEstudianteEstado(EstudianteEstado $estudiante_estado): EstudiantePlanEstudio
    {
        $this->estudiante_estado = $estudiante_estado;
        return $this;
    }

    /**
     * @return null|string
     */
    public function __toString(): ?string
    {
        return (string)$this->estudiante;
    }

}
