<?php

namespace App\Entity\Academico;

use Doctrine\ORM\Mapping as ORM;

/**
 * CalificacionPromedio
 *
 * @ORM\Table(name="odontoped_academico.calificacion_promedio_vw")
 * @ORM\Entity(readOnly=true)
 */
class CalificacionPromedio
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     */
    private $id;

    /**
     * @var float
     *
     * @ORM\Column(name="ponderacion", type="float", precision=10, scale=0, nullable=false)
     */
    private $ponderacion;

    /**
     * @var string
     *
     * @ORM\Column(name="promedio", type="decimal", precision=8, scale=2, nullable=false)
     */
    private $promedio;

    /**
     * @var string
     *
     * @ORM\Column(name="nota", type="decimal", precision=8, scale=2, nullable=false)
     */
    private $nota;

    /**
     * @var EstudianteCurso
     *
     * @ORM\ManyToOne(targetEntity="EstudianteCurso")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_estudiante_curso", referencedColumnName="id")
     * })
     */
    private $estudiante_curso;

    /**
     * @var Evaluacion
     *
     * @ORM\ManyToOne(targetEntity="Evaluacion")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_evaluacion", referencedColumnName="id")
     * })
     */
    private $evaluacion;

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return float
     */
    public function getPonderacion(): ?float
    {
        return $this->ponderacion;
    }

    /**
     * @param float $ponderacion
     * @return CalificacionPromedio
     */
    public function setPonderacion(float $ponderacion): CalificacionPromedio
    {
        $this->ponderacion = $ponderacion;
        return $this;
    }

    /**
     * @return string
     */
    public function getPromedio(): ?string
    {
        return $this->promedio;
    }

    /**
     * @param string $promedio
     * @return CalificacionPromedio
     */
    public function setPromedio(string $promedio): CalificacionPromedio
    {
        $this->promedio = $promedio;
        return $this;
    }

    /**
     * @return string
     */
    public function getNota(): ?string
    {
        return $this->nota;
    }

    /**
     * @param string $nota
     * @return CalificacionPromedio
     */
    public function setNota(string $nota): CalificacionPromedio
    {
        $this->nota = $nota;
        return $this;
    }

    /**
     * @return EstudianteCurso
     */
    public function getEstudianteCurso(): ?EstudianteCurso
    {
        return $this->estudiante_curso;
    }

    /**
     * @param EstudianteCurso $estudiante_curso
     * @return CalificacionPromedio
     */
    public function setEstudianteCurso(EstudianteCurso $estudiante_curso): CalificacionPromedio
    {
        $this->estudiante_curso = $estudiante_curso;
        return $this;
    }

    /**
     * @return Evaluacion
     */
    public function getEvaluacion(): ?Evaluacion
    {
        return $this->evaluacion;
    }

    /**
     * @param Evaluacion $evaluacion
     * @return CalificacionPromedio
     */
    public function setEvaluacion(Evaluacion $evaluacion): CalificacionPromedio
    {
        $this->evaluacion = $evaluacion;
        return $this;
    }

}
