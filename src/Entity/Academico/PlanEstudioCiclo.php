<?php

namespace App\Entity\Academico;

use Doctrine\ORM\Mapping as ORM;

/**
 * PlanEstudioCiclo
 *
 * @ORM\Table(name="odontoped_academico.plan_estudio_ciclo", uniqueConstraints={
 *     @ORM\UniqueConstraint(name="numero", columns={"numero"}),
 *     @ORM\UniqueConstraint(name="romano", columns={"romano"})
 * })
 * @ORM\Entity
 */
class PlanEstudioCiclo
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="numero", type="integer", nullable=false)
     */
    private $numero;

    /**
     * @var string
     *
     * @ORM\Column(name="romano", type="string", length=5, nullable=false)
     */
    private $romano;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="creado", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $creado;

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getNumero(): ?int
    {
        return $this->numero;
    }

    /**
     * @param int $numero
     * @return PlanEstudioCiclo
     */
    public function setNumero(int $numero): PlanEstudioCiclo
    {
        $this->numero = $numero;
        return $this;
    }

    /**
     * @return string
     */
    public function getRomano(): ?string
    {
        return $this->romano;
    }

    /**
     * @param string $romano
     * @return PlanEstudioCiclo
     */
    public function setRomano(string $romano): PlanEstudioCiclo
    {
        $this->romano = $romano;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreado(): ?\DateTime
    {
        return $this->creado;
    }

    /**
     * @param \DateTime $creado
     * @return PlanEstudioCiclo
     */
    public function setCreado(\DateTime $creado): PlanEstudioCiclo
    {
        $this->creado = $creado;
        return $this;
    }

    /**
     * @return string
     */
    public function __toString(): ?string
    {
        return (string)$this->romano;
    }

}
