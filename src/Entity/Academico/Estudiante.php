<?php

namespace App\Entity\Academico;

use Doctrine\ORM\Mapping as ORM;

/**
 * Estudiante
 *
 * @ORM\Table(name="odontoped_academico.estudiante", uniqueConstraints={
 *     @ORM\UniqueConstraint(name="carnet", columns={"carnet"})
 * }, indexes={
 *     @ORM\Index(name="fk_estudiante_persona", columns={"id_persona"})
 * })
 * @ORM\Entity(repositoryClass="App\Repository\Academico\EstudianteRepository")
 */
class Estudiante
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="carnet", type="string", length=8, nullable=false)
     */
    private $carnet;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="creado", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $creado;

    /**
     * @var \App\Entity\Administracion\Persona
     *
     * @ORM\ManyToOne(targetEntity="\App\Entity\Administracion\Persona")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_persona", referencedColumnName="id")
     * })
     */
    private $persona;

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getCarnet(): ?string
    {
        return $this->carnet;
    }

    /**
     * @param string $carnet
     * @return Estudiante
     */
    public function setCarnet(string $carnet): Estudiante
    {
        $this->carnet = $carnet;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreado(): ?\DateTime
    {
        return $this->creado;
    }

    /**
     * @param \DateTime $creado
     * @return Estudiante
     */
    public function setCreado(\DateTime $creado): Estudiante
    {
        $this->creado = $creado;
        return $this;
    }

    /**
     * @return \App\Entity\Administracion\Persona
     */
    public function getPersona(): ?\App\Entity\Administracion\Persona
    {
        return $this->persona;
    }

    /**
     * @param \App\Entity\Administracion\Persona $persona
     * @return Estudiante
     */
    public function setPersona(\App\Entity\Administracion\Persona $persona): Estudiante
    {
        $this->persona = $persona;
        return $this;
    }

    /**
     * @return null|string
     */
    public function __toString(): ?string
    {
        return (string)$this->carnet;
    }

}
