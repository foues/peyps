<?php

namespace App\Entity\Academico;

use Doctrine\ORM\Mapping as ORM;

/**
 * CalificacionPenalizacion
 *
 * @ORM\Table(name="odontoped_academico.calificacion_penalizacion", uniqueConstraints={
 *     @ORM\UniqueConstraint(name="unique_calificacion_penalizacion", columns={"id_calificacion", "id_penalizacion"})
 * }, indexes={
 *     @ORM\Index(name="fk_calificacion_penalizacion", columns={"id_calificacion"}),
 *     @ORM\Index(name="fk_penalizacion_calificacion", columns={"id_penalizacion"})
 * })
 * @ORM\Entity
 */
class CalificacionPenalizacion
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="creado", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $creado;

    /**
     * @var Calificacion
     *
     * @ORM\ManyToOne(targetEntity="Calificacion", inversedBy="penalizaciones")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_calificacion", referencedColumnName="id")
     * })
     */
    private $calificacion;

    /**
     * @var Penalizacion
     *
     * @ORM\ManyToOne(targetEntity="Penalizacion")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_penalizacion", referencedColumnName="id")
     * })
     */
    private $penalizacion;

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return \DateTime
     */
    public function getCreado(): ?\DateTime
    {
        return $this->creado;
    }

    /**
     * @param \DateTime $creado
     * @return CalificacionPenalizacion
     */
    public function setCreado(\DateTime $creado): CalificacionPenalizacion
    {
        $this->creado = $creado;
        return $this;
    }

    /**
     * @return Calificacion
     */
    public function getCalificacion(): ?Calificacion
    {
        return $this->calificacion;
    }

    /**
     * @param Calificacion $calificacion
     * @return CalificacionPenalizacion
     */
    public function setCalificacion(Calificacion $calificacion): CalificacionPenalizacion
    {
        $this->calificacion = $calificacion;
        return $this;
    }

    /**
     * @return Penalizacion
     */
    public function getPenalizacion(): ?Penalizacion
    {
        return $this->penalizacion;
    }

    /**
     * @param Penalizacion $penalizacion
     * @return CalificacionPenalizacion
     */
    public function setPenalizacion(Penalizacion $penalizacion): CalificacionPenalizacion
    {
        $this->penalizacion = $penalizacion;
        return $this;
    }


}
