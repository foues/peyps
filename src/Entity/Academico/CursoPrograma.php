<?php

namespace App\Entity\Academico;

use Doctrine\ORM\Mapping as ORM;

/**
 * CursoPrograma
 *
 * @ORM\Table(name="odontoped_academico.curso_programa", uniqueConstraints={
 *     @ORM\UniqueConstraint(name="unique_curso_programa", columns={"id_programa", "id_plan_estudio_curso"})
 * }, indexes={
 *     @ORM\Index(name="fk_curso_programa", columns={"id_programa"}),
 *     @ORM\Index(name="fk_plan_estudio_curso_programa", columns={"id_plan_estudio_curso"})
 * })
 * @ORM\Entity(repositoryClass="App\Repository\Academico\CursoProgramaRepository")
 */
class CursoPrograma
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int|null
     *
     * @ORM\Column(name="correlativo", type="integer", nullable=true)
     */
    private $correlativo;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="creado", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $creado;

    /**
     * @var \App\Entity\Rotacion\Programa
     *
     * @ORM\ManyToOne(targetEntity="\App\Entity\Rotacion\Programa")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_programa", referencedColumnName="id")
     * })
     */
    private $programa;

    /**
     * @var PlanEstudioCurso
     *
     * @ORM\ManyToOne(targetEntity="PlanEstudioCurso")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_plan_estudio_curso", referencedColumnName="id")
     * })
     */
    private $plan_estudio_curso;

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return int|null
     */
    public function getCorrelativo(): ?int
    {
        return $this->correlativo;
    }

    /**
     * @param int|null $correlativo
     * @return CursoPrograma
     */
    public function setCorrelativo(?int $correlativo): CursoPrograma
    {
        $this->correlativo = $correlativo;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreado(): ?\DateTime
    {
        return $this->creado;
    }

    /**
     * @param \DateTime $creado
     * @return CursoPrograma
     */
    public function setCreado(\DateTime $creado): CursoPrograma
    {
        $this->creado = $creado;
        return $this;
    }

    /**
     * @return \App\Entity\Rotacion\Programa
     */
    public function getPrograma(): ?\App\Entity\Rotacion\Programa
    {
        return $this->programa;
    }

    /**
     * @param \App\Entity\Rotacion\Programa $programa
     * @return CursoPrograma
     */
    public function setPrograma(\App\Entity\Rotacion\Programa $programa): CursoPrograma
    {
        $this->programa = $programa;
        return $this;
    }

    /**
     * @return PlanEstudioCurso
     */
    public function getPlanEstudioCurso(): ?PlanEstudioCurso
    {
        return $this->plan_estudio_curso;
    }

    /**
     * @param PlanEstudioCurso $plan_estudio_curso
     * @return CursoPrograma
     */
    public function setPlanEstudioCurso(PlanEstudioCurso $plan_estudio_curso): CursoPrograma
    {
        $this->plan_estudio_curso = $plan_estudio_curso;
        return $this;
    }


}
