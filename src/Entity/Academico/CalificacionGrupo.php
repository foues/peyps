<?php

namespace App\Entity\Academico;

use Doctrine\ORM\Mapping as ORM;

/**
 * CalificacionGrupo
 *
 * @ORM\Table(name="odontoped_academico.calificacion_grupo", indexes={
 *     @ORM\Index(name="fk_grupo_calificacion", columns={"id_grupo"}),
 *     @ORM\Index(name="fk_calificacion_grupo_evaluacion", columns={"id_evaluacion"}),
 *     @ORM\Index(name="fk_calificacion_grupo_padre", columns={"id_padre"})
 * })
 * @ORM\Entity
 */
class CalificacionGrupo
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nota", type="decimal", precision=8, scale=2, nullable=false)
     */
    private $nota;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha", type="date", nullable=false)
     */
    private $fecha;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="creado", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $creado;

    /**
     * @var \App\Entity\Rotacion\Grupo
     *
     * @ORM\ManyToOne(targetEntity="\App\Entity\Rotacion\Grupo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_grupo", referencedColumnName="id")
     * })
     */
    private $grupo;

    /**
     * @var Evaluacion
     *
     * @ORM\ManyToOne(targetEntity="Evaluacion")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_evaluacion", referencedColumnName="id")
     * })
     */
    private $evaluacion;

    /**
     * @var CalificacionGrupo
     *
     * @ORM\ManyToOne(targetEntity="CalificacionGrupo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_padre", referencedColumnName="id")
     * })
     */
    private $padre;

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getNota(): ?string
    {
        return $this->nota;
    }

    /**
     * @param string $nota
     * @return CalificacionGrupo
     */
    public function setNota(string $nota): CalificacionGrupo
    {
        $this->nota = $nota;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getFecha(): ?\DateTime
    {
        return $this->fecha;
    }

    /**
     * @param \DateTime $fecha
     * @return CalificacionGrupo
     */
    public function setFecha(\DateTime $fecha): CalificacionGrupo
    {
        $this->fecha = $fecha;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreado(): ?\DateTime
    {
        return $this->creado;
    }

    /**
     * @param \DateTime $creado
     * @return CalificacionGrupo
     */
    public function setCreado(\DateTime $creado): CalificacionGrupo
    {
        $this->creado = $creado;
        return $this;
    }

    /**
     * @return \App\Entity\Rotacion\Grupo
     */
    public function getGrupo(): ?\App\Entity\Rotacion\Grupo
    {
        return $this->grupo;
    }

    /**
     * @param \App\Entity\Rotacion\Grupo $grupo
     * @return CalificacionGrupo
     */
    public function setGrupo(\App\Entity\Rotacion\Grupo $grupo): CalificacionGrupo
    {
        $this->grupo = $grupo;
        return $this;
    }

    /**
     * @return Evaluacion
     */
    public function getEvaluacion(): ?Evaluacion
    {
        return $this->evaluacion;
    }

    /**
     * @param Evaluacion $evaluacion
     * @return CalificacionGrupo
     */
    public function setEvaluacion(Evaluacion $evaluacion): CalificacionGrupo
    {
        $this->evaluacion = $evaluacion;
        return $this;
    }

    /**
     * @return CalificacionGrupo
     */
    public function getPadre(): ?CalificacionGrupo
    {
        return $this->padre;
    }

    /**
     * @param CalificacionGrupo $padre
     * @return CalificacionGrupo
     */
    public function setPadre(CalificacionGrupo $padre): CalificacionGrupo
    {
        $this->padre = $padre;
        return $this;
    }


}
