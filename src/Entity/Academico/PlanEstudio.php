<?php

namespace App\Entity\Academico;

use Doctrine\ORM\Mapping as ORM;

/**
 * PlanEstudio
 *
 * @ORM\Table(name="odontoped_academico.plan_estudio", uniqueConstraints={
 *     @ORM\UniqueConstraint(name="unique_plan_estudio", columns={"id_carrera", "anio"})
 * }, indexes={
 *     @ORM\Index(name="fk_plan_estudio_carrera", columns={"id_carrera"})
 * })
 * @ORM\Entity(repositoryClass="App\Repository\Academico\PlanEstudioRepository")
 */
class PlanEstudio
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="anio", type="integer", nullable=false)
     */
    private $anio;

    /**
     * @var bool
     *
     * @ORM\Column(name="activo", type="boolean", nullable=false)
     */
    private $activo = false;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="creado", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $creado;

    /**
     * @var Carrera
     *
     * @ORM\ManyToOne(targetEntity="Carrera")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_carrera", referencedColumnName="id")
     * })
     */
    private $carrera;

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getAnio(): ?int
    {
        return $this->anio;
    }

    /**
     * @param int $anio
     * @return PlanEstudio
     */
    public function setAnio(int $anio): PlanEstudio
    {
        $this->anio = $anio;
        return $this;
    }

    /**
     * @return bool
     */
    public function isActivo(): ?bool
    {
        return $this->activo;
    }

    /**
     * @param bool $activo
     * @return PlanEstudio
     */
    public function setActivo(bool $activo): PlanEstudio
    {
        $this->activo = $activo;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreado(): ?\DateTime
    {
        return $this->creado;
    }

    /**
     * @param \DateTime $creado
     * @return PlanEstudio
     */
    public function setCreado(\DateTime $creado): PlanEstudio
    {
        $this->creado = $creado;
        return $this;
    }

    /**
     * @return Carrera
     */
    public function getCarrera(): ?Carrera
    {
        return $this->carrera;
    }

    /**
     * @param Carrera $carrera
     * @return PlanEstudio
     */
    public function setCarrera(Carrera $carrera): PlanEstudio
    {
        $this->carrera = $carrera;
        return $this;
    }

    /**
     * @return null|string
     */
    public function __toString()
    {
        if ($this->carrera)
            return "{$this->carrera} ({$this->carrera->getCodigo()}-{$this->getAnio()})";
        else
            return null;
    }

}
