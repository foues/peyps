<?php

namespace App\Entity\Academico;

use Doctrine\ORM\Mapping as ORM;

/**
 * EstudianteCurso
 *
 * @ORM\Table(name="odontoped_academico.estudiante_curso", uniqueConstraints={
 *     @ORM\UniqueConstraint(name="unique_estudiante_curso", columns={"id_estudiante_plan_estudio", "id_curso", "id_periodo_ciclo"})
 * }, indexes={
 *     @ORM\Index(name="fk_estudiante_curso_plan_estudio", columns={"id_estudiante_plan_estudio"}),
 *     @ORM\Index(name="fk_estudiante_curso", columns={"id_curso"}),
 *     @ORM\Index(name="fk_estudiante_curso_periodo_ciclo", columns={"id_periodo_ciclo"}),
 *     @ORM\Index(name="fk_estudiante_curso_estado", columns={"id_estudiante_estado"})
 * })
 * @ORM\Entity(repositoryClass="App\Repository\Academico\EstudianteCursoRepository")
 */
class EstudianteCurso
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="creado", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $creado;

    /**
     * @var EstudiantePlanEstudio
     *
     * @ORM\ManyToOne(targetEntity="EstudiantePlanEstudio")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_estudiante_plan_estudio", referencedColumnName="id")
     * })
     */
    private $estudiante_plan_estudio;

    /**
     * @var Curso
     *
     * @ORM\ManyToOne(targetEntity="Curso")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_curso", referencedColumnName="id")
     * })
     */
    private $curso;

    /**
     * @var PeriodoCiclo
     *
     * @ORM\ManyToOne(targetEntity="PeriodoCiclo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_periodo_ciclo", referencedColumnName="id")
     * })
     */
    private $periodo_ciclo;

    /**
     * @var EstudianteEstado
     *
     * @ORM\ManyToOne(targetEntity="EstudianteEstado")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_estudiante_estado", referencedColumnName="id")
     * })
     */
    private $estudiante_estado;

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return \DateTime
     */
    public function getCreado(): ?\DateTime
    {
        return $this->creado;
    }

    /**
     * @param \DateTime $creado
     * @return EstudianteCurso
     */
    public function setCreado(\DateTime $creado): EstudianteCurso
    {
        $this->creado = $creado;
        return $this;
    }

    /**
     * @return EstudiantePlanEstudio
     */
    public function getEstudiantePlanEstudio(): ?EstudiantePlanEstudio
    {
        return $this->estudiante_plan_estudio;
    }

    /**
     * @param EstudiantePlanEstudio $estudiante_plan_estudio
     * @return EstudianteCurso
     */
    public function setEstudiantePlanEstudio(EstudiantePlanEstudio $estudiante_plan_estudio): EstudianteCurso
    {
        $this->estudiante_plan_estudio = $estudiante_plan_estudio;
        return $this;
    }

    /**
     * @return Curso
     */
    public function getCurso(): ?Curso
    {
        return $this->curso;
    }

    /**
     * @param Curso $curso
     * @return EstudianteCurso
     */
    public function setCurso(Curso $curso): EstudianteCurso
    {
        $this->curso = $curso;
        return $this;
    }

    /**
     * @return PeriodoCiclo
     */
    public function getPeriodoCiclo(): ?PeriodoCiclo
    {
        return $this->periodo_ciclo;
    }

    /**
     * @param PeriodoCiclo $periodo_ciclo
     * @return EstudianteCurso
     */
    public function setPeriodoCiclo(PeriodoCiclo $periodo_ciclo): EstudianteCurso
    {
        $this->periodo_ciclo = $periodo_ciclo;
        return $this;
    }

    /**
     * @return EstudianteEstado
     */
    public function getEstudianteEstado(): ?EstudianteEstado
    {
        return $this->estudiante_estado;
    }

    /**
     * @param EstudianteEstado $estudiante_estado
     * @return EstudianteCurso
     */
    public function setEstudianteEstado(EstudianteEstado $estudiante_estado): EstudianteCurso
    {
        $this->estudiante_estado = $estudiante_estado;
        return $this;
    }

    /**
     * @return null|string
     */
    public function __toString(): ?string
    {
        return (string)$this->curso;
    }

}
