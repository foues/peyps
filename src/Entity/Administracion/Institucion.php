<?php

namespace App\Entity\Administracion;

use Doctrine\ORM\Mapping as ORM;

/**
 * Institucion
 *
 * @ORM\Table(name="odontoped_administracion.institucion", uniqueConstraints={
 *     @ORM\UniqueConstraint(name="codigo", columns={"codigo"})
 * }, indexes={
 *     @ORM\Index(name="fk_institucion_cuenta", columns={"id_cuenta"}),
 *     @ORM\Index(name="fk_institucion_padre", columns={"id_padre"})
 * })
 * @ORM\Entity
 */
class Institucion
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="codigo", type="string", length=50, nullable=false)
     */
    private $codigo;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=500, nullable=false)
     */
    private $nombre;

    /**
     * @var string|null
     *
     * @ORM\Column(name="descripcion", type="string", length=500, nullable=true)
     */
    private $descripcion;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="creado", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $creado;

    /**
     * @var Cuenta
     *
     * @ORM\ManyToOne(targetEntity="Cuenta")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_cuenta", referencedColumnName="id")
     * })
     */
    private $cuenta;

    /**
     * @var Institucion
     *
     * @ORM\ManyToOne(targetEntity="Institucion")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_padre", referencedColumnName="id")
     * })
     */
    private $padre;

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getCodigo(): ?string
    {
        return $this->codigo;
    }

    /**
     * @param string $codigo
     * @return Institucion
     */
    public function setCodigo(string $codigo): Institucion
    {
        $this->codigo = $codigo;
        return $this;
    }

    /**
     * @return string
     */
    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    /**
     * @param string $nombre
     * @return Institucion
     */
    public function setNombre(string $nombre): Institucion
    {
        $this->nombre = $nombre;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getDescripcion(): ?string
    {
        return $this->descripcion;
    }

    /**
     * @param null|string $descripcion
     * @return Institucion
     */
    public function setDescripcion(?string $descripcion): Institucion
    {
        $this->descripcion = $descripcion;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreado(): ?\DateTime
    {
        return $this->creado;
    }

    /**
     * @param \DateTime $creado
     * @return Institucion
     */
    public function setCreado(\DateTime $creado): Institucion
    {
        $this->creado = $creado;
        return $this;
    }

    /**
     * @return Cuenta
     */
    public function getCuenta(): ?Cuenta
    {
        return $this->cuenta;
    }

    /**
     * @param Cuenta $cuenta
     * @return Institucion
     */
    public function setCuenta(Cuenta $cuenta): Institucion
    {
        $this->cuenta = $cuenta;
        return $this;
    }

    /**
     * @return Institucion
     */
    public function getPadre(): ?Institucion
    {
        return $this->padre;
    }

    /**
     * @param Institucion $padre
     * @return Institucion
     */
    public function setPadre(?Institucion $padre): Institucion
    {
        $this->padre = $padre;
        return $this;
    }

    /**
     * @return string
     */
    public function __toString(): ?string
    {
        return (string)$this->nombre;
    }


}
