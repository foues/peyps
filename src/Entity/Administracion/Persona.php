<?php

namespace App\Entity\Administracion;

use Doctrine\ORM\Mapping as ORM;

/**
 * Persona
 *
 * @ORM\Table(name="odontoped_administracion.persona", uniqueConstraints={
 *     @ORM\UniqueConstraint(name="dui", columns={"dui"}),
 *     @ORM\UniqueConstraint(name="nit", columns={"nit"})
 * }, indexes={
 *     @ORM\Index(name="fk_persona_cuenta", columns={"id_cuenta"})
 * })
 * @ORM\Entity
 */
class Persona
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=50, nullable=false)
     */
    private $nombre;

    /**
     * @var string|null
     *
     * @ORM\Column(name="apellido", type="string", length=50, nullable=true)
     */
    private $apellido;

    /**
     * @var string|null
     *
     * @ORM\Column(name="dui", type="string", length=10, nullable=true)
     */
    private $dui;

    /**
     * @var string|null
     *
     * @ORM\Column(name="dui_apoderado", type="string", length=12, nullable=true)
     */
    private $dui_apoderado;

    /**
     * @var string|null
     *
     * @ORM\Column(name="centro_educativo_paciente", type="string", length=500, nullable=true)
     */
    private $centro_educativo_paciente;

    /**
     * @var string|null
     *
     * @ORM\Column(name="nit", type="string", length=17, nullable=true)
     */
    private $nit;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="nacimiento", type="date", nullable=true)
     */
    private $nacimiento;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="creado", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $creado;

    /**
     * @var Cuenta
     *
     * @ORM\ManyToOne(targetEntity="Cuenta")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_cuenta", referencedColumnName="id")
     * })
     */
    private $cuenta;

    /**
     * @var string|null
     *
     * @ORM\Column(name="archivo_consentimiento", type="string", length=999, nullable=true)
     */
    private $archivo_consentimiento;

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    /**
     * @param string $nombre
     * @return Persona
     */
    public function setNombre(string $nombre): Persona
    {
        $this->nombre = $nombre;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getApellido(): ?string
    {
        return $this->apellido;
    }

    /**
     * @param null|string $apellido
     * @return Persona
     */
    public function setApellido(?string $apellido): Persona
    {
        $this->apellido = $apellido;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getDui(): ?string
    {
        return $this->dui;
    }

    /**
     * @param null|string $dui
     * @return Persona
     */
    public function setDui(?string $dui): Persona
    {
        $this->dui = $dui;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getDuiApoderado(): ?string
    {
        return $this->dui_apoderado;
    }

    /**
     * @param null|string $dui_apoderado
     * @return Persona
     */

    public function setDuiApoderado(?string $dui_apoderado): Persona
    {
        $this->dui_apoderado = $dui_apoderado;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getCentroEducativo(): ?string
    {
        return $this->centro_educativo_paciente;
    }


    /**
     * @param null|string $centro_educativo_paciente
     * @return Persona
     */
    public function setCentroEducativo(?string $centro_educativo_paciente): Persona
    {
        $this->centro_educativo_paciente = $centro_educativo_paciente;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getNit(): ?string
    {
        return $this->nit;
    }

    /**
     * @param null|string $nit
     * @return Persona
     */
    public function setNit(?string $nit): Persona
    {
        $this->nit = $nit;
        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getNacimiento(): ?\DateTime
    {
        return $this->nacimiento;
    }

    /**
     * @param \DateTime|null $nacimiento
     * @return Persona
     */
    public function setNacimiento(?\DateTime $nacimiento): Persona
    {
        $this->nacimiento = $nacimiento;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreado(): ?\DateTime
    {
        return $this->creado;
    }

    /**
     * @param \DateTime $creado
     * @return Persona
     */
    public function setCreado(\DateTime $creado): Persona
    {
        $this->creado = $creado;
        return $this;
    }

    /**
     * @return Cuenta
     */
    public function getCuenta(): ?Cuenta
    {
        return $this->cuenta;
    }

    /**
     * @param Cuenta $cuenta
     * @return Persona
     */
    public function setCuenta(Cuenta $cuenta): Persona
    {
        $this->cuenta = $cuenta;
        return $this;
    }

    /**
     * @return string
     */
    public function getArchivoConsentimiento(): ?string
    {
        return $this->$archivo_consentimiento;
    }

    /**
     * @param string $archivo_consentimiento
     * @return Persona
     */
    public function setArchivoConsentimiento(string $archivo_consentimiento): Persona
    {
        $this->archivo_consentimiento = $archivo_consentimiento;
        return $this;
    }

    /**
     * @return string
     */
    public function getNombreCompleto(): string
    {
        return $this->nombre . ' ' . $this->apellido;
    }

    /**
     * @return string
     */
    public function __toString(): ?string
    {
        return (string)$this->getNombreCompleto();
    }

    /**
     * @return string
     */
    public function getTipoPaciente(): ?string
    {
        $tipo = null;
        if ($this->nacimiento) {
            $edad = (new \DateTime())->diff($this->nacimiento)->y;
            if ($edad < 12) $tipo = 'Niño';
            else if ($edad < 18) $tipo = 'Adolescente';
            else $tipo = 'Adulto';
        }

        return $tipo;
    }





}
