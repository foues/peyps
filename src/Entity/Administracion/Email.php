<?php

namespace App\Entity\Administracion;

use Doctrine\ORM\Mapping as ORM;

/**
 * Email
 *
 * @ORM\Table(name="odontoped_administracion.email", indexes={
 *     @ORM\Index(name="fk_email_cuenta", columns={"id_cuenta"})
 * })
 * @ORM\Entity
 */
class Email
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=50, nullable=false, options={"fixed"=true})
     */
    private $email;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="creado", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $creado;

    /**
     * @var Cuenta
     *
     * @ORM\ManyToOne(targetEntity="Cuenta")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_cuenta", referencedColumnName="id")
     * })
     */
    private $cuenta;

    /**
     * @var string
     *
     * @ORM\Column(name="asignado_a", type="string", length=20, nullable=false, options={"fixed"=true})
     */
    private $asignado_a;


    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param string $email
     * @return Email
     */
    public function setEmail(string $email): Email
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreado(): ?\DateTime
    {
        return $this->creado;
    }

    /**
     * @param \DateTime $creado
     * @return Email
     */
    public function setCreado(\DateTime $creado): Email
    {
        $this->creado = $creado;
        return $this;
    }

    /**
     * @return Cuenta
     */
    public function getCuenta(): ?Cuenta
    {
        return $this->cuenta;
    }

    /**
     * @param Cuenta $cuenta
     * @return Email
     */
    public function setCuenta(Cuenta $cuenta): Email
    {
        $this->cuenta = $cuenta;
        return $this;
    }

    /**
     * @param string $asignado_a
     * @return string
     */
    public function getAsignadoA(): ?string
    {
        return $this->asignado_a;
    }

    /**
     * @param string $asignado_a
     * @return string
     */
    public function setAsignadoA(string $asignado_a): Email
    {
        $this->asignado_a = $asignado_a;
        return $this;
    }


    /**
     * @return string
     */
    public function __toString(): ?string
    {
        return (string)$this->email;
    }




}
