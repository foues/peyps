<?php

namespace App\Entity\Administracion;

use Doctrine\ORM\Mapping as ORM;

/**
 * Submenu
 *
 * @ORM\Table(name="menu", indexes={@ORM\Index(name="fk_menu_padre", columns={"id_padre"})})
 * @ORM\Entity
 */
class Submenu
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=100, nullable=false)
     */
    private $nombre;

    /**
     * @var string|null
     *
     * @ORM\Column(name="descripcion", type="string", length=255, nullable=true)
     */
    private $descripcion;

    /**
     * @var string
     *
     * @ORM\Column(name="url", type="string", length=255, nullable=false)
     */
    private $url;

    /**
     * @var string|null
     *
     * @ORM\Column(name="icon", type="string", length=50, nullable=true)
     */
    private $icon;

    /**
     * @var bool
     *
     * @ORM\Column(name="enabled", type="boolean", nullable=false, options={"default"=true})
     */
    private $enabled = true;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="creado", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $creado;

    /**
     * @var Menu
     *
     * @ORM\ManyToOne(targetEntity="Menu")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_padre", referencedColumnName="id")
     * })
     */
    private $padre;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    /**
     * @param string $nombre
     * @return Submenu
     */
    public function setNombre(string $nombre): Submenu
    {
        $this->nombre = $nombre;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getDescripcion(): ?string
    {
        return $this->descripcion;
    }

    /**
     * @param string|null $descripcion
     * @return Submenu
     */
    public function setDescripcion(?string $descripcion): Submenu
    {
        $this->descripcion = $descripcion;
        return $this;
    }

    /**
     * @return string
     */
    public function getUrl(): ?string
    {
        return $this->url;
    }

    /**
     * @param string $url
     * @return Submenu
     */
    public function setUrl(string $url): Submenu
    {
        $this->url = $url;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getIcon(): ?string
    {
        return $this->icon;
    }

    /**
     * @param string|null $icon
     * @return Submenu
     */
    public function setIcon(?string $icon): Submenu
    {
        $this->icon = $icon;
        return $this;
    }

    /**
     * @return bool
     */
    public function isEnabled(): bool
    {
        return $this->enabled;
    }

    /**
     * @param bool $enabled
     * @return Submenu
     */
    public function setEnabled(bool $enabled): Submenu
    {
        $this->enabled = $enabled;
        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getCreado(): ?\DateTime
    {
        return $this->creado;
    }

    /**
     * @param \DateTime $creado
     * @return Submenu
     */
    public function setCreado(\DateTime $creado): Submenu
    {
        $this->creado = $creado;
        return $this;
    }

    /**
     * @return Menu
     */
    public function getPadre(): ?Menu
    {
        return $this->padre;
    }

    /**
     * @param Menu $padre
     * @return Submenu
     */
    public function setPadre(Menu $padre): Submenu
    {
        $this->padre = $padre;
        return $this;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        $string = '';
        $padre = $this->getPadre();
        while ($padre) {
            $string = $padre->getNombre() . ' / ' . $string;
            $padre = $padre->getPadre();
        }

        return $string . $this->nombre;
    }


}
