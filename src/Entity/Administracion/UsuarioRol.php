<?php

namespace App\Entity\Administracion;

use Doctrine\ORM\Mapping as ORM;

/**
 * UsuarioRol
 *
 * @ORM\Table(name="odontoped_administracion.usuario_rol", uniqueConstraints={
 *     @ORM\UniqueConstraint(name="unique_usuario_rol", columns={"id_usuario", "id_rol"})
 * }, indexes={
 *     @ORM\Index(name="fk_usuario_rol", columns={"id_usuario"}),
 *     @ORM\Index(name="fk_rol_usuario", columns={"id_rol"})
 * })
 * @ORM\Entity
 */
class UsuarioRol
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="creado", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $creado;

    /**
     * @var Usuario
     *
     * @ORM\ManyToOne(targetEntity="Usuario")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_usuario", referencedColumnName="id")
     * })
     */
    private $usuario;

    /**
     * @var Rol
     *
     * @ORM\ManyToOne(targetEntity="Rol")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_rol", referencedColumnName="id")
     * })
     */
    private $rol;

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return \DateTime
     */
    public function getCreado(): ?\DateTime
    {
        return $this->creado;
    }

    /**
     * @param \DateTime $creado
     * @return UsuarioRol
     */
    public function setCreado(\DateTime $creado): UsuarioRol
    {
        $this->creado = $creado;
        return $this;
    }

    /**
     * @return Usuario
     */
    public function getUsuario(): ?Usuario
    {
        return $this->usuario;
    }

    /**
     * @param Usuario $usuario
     * @return UsuarioRol
     */
    public function setUsuario(Usuario $usuario): UsuarioRol
    {
        $this->usuario = $usuario;
        return $this;
    }

    /**
     * @return Rol
     */
    public function getRol(): ?Rol
    {
        return $this->rol;
    }

    /**
     * @param Rol $rol
     * @return UsuarioRol
     */
    public function setRol(Rol $rol): UsuarioRol
    {
        $this->rol = $rol;
        return $this;
    }


}
