<?php

namespace App\Entity\Administracion;

use Doctrine\ORM\Mapping as ORM;

/**
 * RolPermiso
 *
 * @ORM\Table(name="odontoped_administracion.rol_permiso", uniqueConstraints={
 *     @ORM\UniqueConstraint(name="unique_rol_permiso", columns={"id_rol", "id_permiso"})
 * }, indexes={
 *     @ORM\Index(name="fk_rol_permiso", columns={"id_rol"}),
 *     @ORM\Index(name="fk_permiso_rol", columns={"id_permiso"})
 * })
 * @ORM\Entity
 */
class RolPermiso
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="creado", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $creado;

    /**
     * @var Rol
     *
     * @ORM\ManyToOne(targetEntity="Rol")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_rol", referencedColumnName="id")
     * })
     */
    private $rol;

    /**
     * @var Permiso
     *
     * @ORM\ManyToOne(targetEntity="Permiso")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_permiso", referencedColumnName="id")
     * })
     */
    private $permiso;

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return \DateTime
     */
    public function getCreado(): ?\DateTime
    {
        return $this->creado;
    }

    /**
     * @param \DateTime $creado
     * @return RolPermiso
     */
    public function setCreado(\DateTime $creado): RolPermiso
    {
        $this->creado = $creado;
        return $this;
    }

    /**
     * @return Rol
     */
    public function getRol(): ?Rol
    {
        return $this->rol;
    }

    /**
     * @param Rol $rol
     * @return RolPermiso
     */
    public function setRol(Rol $rol): RolPermiso
    {
        $this->rol = $rol;
        return $this;
    }

    /**
     * @return Permiso
     */
    public function getPermiso(): ?Permiso
    {
        return $this->permiso;
    }

    /**
     * @param Permiso $permiso
     * @return RolPermiso
     */
    public function setPermiso(Permiso $permiso): RolPermiso
    {
        $this->permiso = $permiso;
        return $this;
    }


}
