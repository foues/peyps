<?php

namespace App\Entity\Administracion;

use Doctrine\ORM\Mapping as ORM;

/**
 * Telefono
 *
 * @ORM\Table(name="odontoped_administracion.telefono", indexes={
 *     @ORM\Index(name="fk_telefono_cuenta", columns={"id_cuenta"}),
 *     @ORM\Index(name="fk_telefono_tipo", columns={"id_telefono_tipo"})
 * })
 * @ORM\Entity(repositoryClass="App\Repository\Administracion\TelefonoRepository")
 */
class Telefono
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="numero", type="string", length=25, nullable=false)
     */
    private $numero;

    /**
     * @var string
     *
     * @ORM\Column(name="contacto", type="string", length=50, nullable=true)
     */
    private $contacto;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="creado", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $creado;

    /**
     * @var Cuenta
     *
     * @ORM\ManyToOne(targetEntity="Cuenta")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_cuenta", referencedColumnName="id")
     * })
     */
    private $cuenta;

    /**
     * @var Catalogo
     *
     * @ORM\ManyToOne(targetEntity="Catalogo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_telefono_tipo", referencedColumnName="id")
     * })
     */
    private $telefono_tipo;

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getNumero(): ?string
    {
        return $this->numero;
    }

    /**
     * @param string $numero
     * @return Telefono
     */
    public function setNumero(string $numero): Telefono
    {
        $this->numero = $numero;
        return $this;
    }

    /**
     * @return string
     */
    public function getContacto(): ?string
    {
        return $this->contacto;
    }

    /**
     * @param string $contacto
     * @return Telefono
     */
    public function setContacto(?string $contacto): Telefono
    {
        $this->contacto = $contacto;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreado(): ?\DateTime
    {
        return $this->creado;
    }

    /**
     * @param \DateTime $creado
     * @return Telefono
     */
    public function setCreado(\DateTime $creado): Telefono
    {
        $this->creado = $creado;
        return $this;
    }

    /**
     * @return Cuenta
     */
    public function getCuenta(): ?Cuenta
    {
        return $this->cuenta;
    }

    /**
     * @param Cuenta $cuenta
     * @return Telefono
     */
    public function setCuenta(Cuenta $cuenta): Telefono
    {
        $this->cuenta = $cuenta;
        return $this;
    }

    /**
     * @return Catalogo
     */
    public function getTelefonoTipo(): ?Catalogo
    {
        return $this->telefono_tipo;
    }

    /**
     * @param Catalogo $telefono_tipo
     * @return Telefono
     */
    public function setTelefonoTipo(Catalogo $telefono_tipo): Telefono
    {
        $this->telefono_tipo = $telefono_tipo;
        return $this;
    }

    /**
     * @return string
     */
    public function __toString(): ?string
    {
        return (string)$this->numero;
    }


}
