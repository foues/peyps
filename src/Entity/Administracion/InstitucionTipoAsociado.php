<?php

namespace App\Entity\Administracion;

use Doctrine\ORM\Mapping as ORM;

/**
 * InstitucionTipoAsociado
 *
 * @ORM\Table(name="odontoped_administracion.institucion_tipo_asociado", uniqueConstraints={
 *     @ORM\UniqueConstraint(name="unique_institucion_tipo_asociado", columns={"id_institucion", "id_institucion_tipo"})
 * }, indexes={
 *     @ORM\Index(name="fk_institucion_tipo_asociado", columns={"id_institucion"}),
 *     @ORM\Index(name="fk_tipo_asociado_institucion", columns={"id_institucion_tipo"})
 * })
 * @ORM\Entity
 */
class InstitucionTipoAsociado
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="creado", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $creado;

    /**
     * @var Institucion
     *
     * @ORM\ManyToOne(targetEntity="Institucion")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_institucion", referencedColumnName="id")
     * })
     */
    private $institucion;

    /**
     * @var Catalogo
     *
     * @ORM\ManyToOne(targetEntity="Catalogo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_institucion_tipo", referencedColumnName="id")
     * })
     */
    private $institucion_tipo;

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return \DateTime
     */
    public function getCreado(): ?\DateTime
    {
        return $this->creado;
    }

    /**
     * @param \DateTime $creado
     * @return InstitucionTipoAsociado
     */
    public function setCreado(\DateTime $creado): InstitucionTipoAsociado
    {
        $this->creado = $creado;
        return $this;
    }

    /**
     * @return Institucion
     */
    public function getInstitucion(): ?Institucion
    {
        return $this->institucion;
    }

    /**
     * @param Institucion $institucion
     * @return InstitucionTipoAsociado
     */
    public function setInstitucion(Institucion $institucion): InstitucionTipoAsociado
    {
        $this->institucion = $institucion;
        return $this;
    }

    /**
     * @return Catalogo
     */
    public function getInstitucionTipo(): ?Catalogo
    {
        return $this->institucion_tipo;
    }

    /**
     * @param Catalogo $institucion_tipo
     * @return InstitucionTipoAsociado
     */
    public function setInstitucionTipo(Catalogo $institucion_tipo): InstitucionTipoAsociado
    {
        $this->institucion_tipo = $institucion_tipo;
        return $this;
    }


}
