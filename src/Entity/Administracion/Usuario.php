<?php

namespace App\Entity\Administracion;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Usuario
 *
 * @ORM\Table(name="odontoped_administracion.usuario", uniqueConstraints={
 *     @ORM\UniqueConstraint(name="username", columns={"username"}),
 *     @ORM\UniqueConstraint(name="email", columns={"email"})
 * }, indexes={
 *     @ORM\Index(name="fk_usuario_cuenta", columns={"id_cuenta"})
 * })
 * @ORM\Entity(repositoryClass="App\Repository\Administracion\UsuarioRepository")
 */
class Usuario implements UserInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="username", type="string", length=50, nullable=false)
     */
    private $username;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=50, nullable=false)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string", length=255, nullable=false)
     */
    private $password;

    /**
     * @var bool
     *
     * @ORM\Column(name="enabled", type="boolean", nullable=false, options={"default"=true})
     */
    private $enabled = true;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="last_login", type="datetime", nullable=true)
     */
    private $last_login;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="creado", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $creado;

    /**
     * @var Cuenta
     *
     * @ORM\ManyToOne(targetEntity="Cuenta")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_cuenta", referencedColumnName="id")
     * })
     */
    private $cuenta;

    /**
     * @var \Doctrine\Common\Collections\Collection|Rol[]
     * @ORM\ManyToMany(targetEntity="Rol")
     * @ORM\JoinTable(
     *      name="odontoped_administracion.usuario_rol",
     *      joinColumns={@ORM\JoinColumn(name="id_usuario", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="id_rol", referencedColumnName="id")}
     * )
     */
    private $roles;

    /**
     * Usuario constructor.
     */
    public function __construct()
    {
        $this->roles = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * {@inheritdoc}
     */
    public function getUsername(): ?string
    {
        return $this->username;
    }

    /**
     * @param string $username
     * @return Usuario
     */
    public function setUsername(string $username): Usuario
    {
        $this->username = $username;
        return $this;
    }

    /**
     * @return string
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param string $email
     * @return Usuario
     */
    public function setEmail(string $email): Usuario
    {
        $this->email = $email;
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getPassword(): ?string
    {
        return $this->password;
    }

    /**
     * @param string $password
     * @return Usuario
     */
    public function setPassword(?string $password): Usuario
    {
        $this->password = $password;
        return $this;
    }

    /**
     * @return bool
     */
    public function isEnabled(): ?bool
    {
        return $this->enabled;
    }

    /**
     * @param bool $enabled
     * @return Usuario
     */
    public function setEnabled(bool $enabled): Usuario
    {
        $this->enabled = $enabled;
        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getLastLogin(): ?\DateTime
    {
        return $this->last_login;
    }

    /**s
     * @param \DateTime $last_login
     * @return Usuario
     */
    public function setLastLogin(\DateTime $last_login): Usuario
    {
        $this->last_login = $last_login;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreado(): ?\DateTime
    {
        return $this->creado;
    }

    /**
     * @param \DateTime $creado
     * @return Usuario
     */
    public function setCreado(\DateTime $creado): Usuario
    {
        $this->creado = $creado;
        return $this;
    }

    /**
     * @return Cuenta
     */
    public function getCuenta(): ?Cuenta
    {
        return $this->cuenta;
    }

    /**
     * @param Cuenta $cuenta
     * @return Usuario
     */
    public function setCuenta(Cuenta $cuenta): Usuario
    {
        $this->cuenta = $cuenta;
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getRoles()
    {
        return array_column($this->roles->toArray(), 'codigo');
    }

    /**
     * {@inheritdoc}
     */
    public function getSalt()
    {
        return null;
    }

    /**
     * {@inheritdoc}
     */
    public function eraseCredentials()
    {
        null;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return (string)$this->username;
    }

}
