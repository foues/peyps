<?php

namespace App\Entity\Administracion;

use Doctrine\ORM\Mapping as ORM;

/**
 * MenuPermiso
 *
 * @ORM\Table(name="menu_permiso", indexes={
 *     @ORM\Index(name="fk_menu_permiso", columns={"id_menu"}),
 *     @ORM\Index(name="fk_permiso_menu", columns={"id_permiso"})
 * })
 * @ORM\Entity
 */
class MenuPermiso
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="asignado_por", type="string", length=50, nullable=true)
     */
    private $asignado_por;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="creado", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $creado;

    /**
     * @var Menu
     *
     * @ORM\ManyToOne(targetEntity="Menu", inversedBy="menu_permisos")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_menu", referencedColumnName="id")
     * })
     */
    private $menu;

    /**
     * @var Permiso
     *
     * @ORM\ManyToOne(targetEntity="Permiso")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_permiso", referencedColumnName="id")
     * })
     */
    private $permiso;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getAsignadoPor(): ?string
    {
        return $this->asignado_por;
    }

    /**
     * @param string|null $asignado_por
     * @return MenuPermiso
     */
    public function setAsignadoPor(?string $asignado_por): MenuPermiso
    {
        $this->asignado_por = $asignado_por;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreado(): ?\DateTime
    {
        return $this->creado;
    }

    /**
     * @param \DateTime $creado
     * @return MenuPermiso
     */
    public function setCreado(\DateTime $creado): MenuPermiso
    {
        $this->creado = $creado;
        return $this;
    }

    /**
     * @return Menu
     */
    public function getMenu(): ?Menu
    {
        return $this->menu;
    }

    /**
     * @param Menu $menu
     * @return MenuPermiso
     */
    public function setMenu(Menu $menu): MenuPermiso
    {
        $this->menu = $menu;
        return $this;
    }

    /**
     * @return Permiso
     */
    public function getPermiso(): ?Permiso
    {
        return $this->permiso;
    }

    /**
     * @param Permiso $permiso
     * @return MenuPermiso
     */
    public function setPermiso(Permiso $permiso): MenuPermiso
    {
        $this->permiso = $permiso;
        return $this;
    }

}
