<?php

namespace App\Entity\Administracion;

use Doctrine\ORM\Mapping as ORM;

/**
 * PersonaAtributo
 *
 * @ORM\Table(name="odontoped_administracion.persona_atributo", uniqueConstraints={
 *     @ORM\UniqueConstraint(name="unique_persona_atributo", columns={"id_persona", "id_atributo"})
 * }, indexes={
 *     @ORM\Index(name="fk_persona_atributo", columns={"id_persona"}),
 *     @ORM\Index(name="fk_atributo_persona", columns={"id_atributo"})
 * })
 * @ORM\Entity(repositoryClass="App\Repository\Administracion\PersonaAtributoRepository")
 */
class PersonaAtributo
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="creado", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $creado;

    /**
     * @var Persona
     *
     * @ORM\ManyToOne(targetEntity="Persona")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_persona", referencedColumnName="id")
     * })
     */
    private $persona;

    /**
     * @var Catalogo
     *
     * @ORM\ManyToOne(targetEntity="Catalogo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_atributo", referencedColumnName="id")
     * })
     */
    private $atributo;

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return \DateTime
     */
    public function getCreado(): ?\DateTime
    {
        return $this->creado;
    }

    /**
     * @param \DateTime $creado
     * @return PersonaAtributo
     */
    public function setCreado(\DateTime $creado): PersonaAtributo
    {
        $this->creado = $creado;
        return $this;
    }

    /**
     * @return Persona
     */
    public function getPersona(): ?Persona
    {
        return $this->persona;
    }

    /**
     * @param Persona $persona
     * @return PersonaAtributo
     */
    public function setPersona(Persona $persona): PersonaAtributo
    {
        $this->persona = $persona;
        return $this;
    }

    /**
     * @return Catalogo
     */
    public function getAtributo(): ?Catalogo
    {
        return $this->atributo;
    }

    /**
     * @param Catalogo $atributo
     * @return PersonaAtributo
     */
    public function setAtributo(Catalogo $atributo): PersonaAtributo
    {
        $this->atributo = $atributo;
        return $this;
    }


}
