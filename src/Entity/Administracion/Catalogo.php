<?php

namespace App\Entity\Administracion;

use Doctrine\ORM\Mapping as ORM;

/**
 * Catalogo
 *
 * @ORM\Table(name="odontoped_administracion.catalogo", uniqueConstraints={
 *     @ORM\UniqueConstraint(name="codigo", columns={"codigo"})
 * }, indexes={
 *     @ORM\Index(name="fk_catalogo_tipo", columns={"id_catalogo_tipo"}),
 *     @ORM\Index(name="fk_catalogo_padre", columns={"id_padre"})
 * })
 * @ORM\Entity
 */
class Catalogo
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="codigo", type="string", length=50, nullable=false)
     */
    private $codigo;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=255, nullable=false)
     */
    private $nombre;

    /**
     * @var string|null
     *
     * @ORM\Column(name="descripcion", type="string", length=500, nullable=true)
     */
    private $descripcion;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="creado", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $creado;

    /**
     * @var CatalogoTipo
     *
     * @ORM\ManyToOne(targetEntity="CatalogoTipo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_catalogo_tipo", referencedColumnName="id")
     * })
     */
    private $catalogo_tipo;

    /**
     * @var Catalogo
     *
     * @ORM\ManyToOne(targetEntity="Catalogo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_padre", referencedColumnName="id")
     * })
     */
    private $padre;

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getCodigo(): ?string
    {
        return $this->codigo;
    }

    /**
     * @param string $codigo
     * @return Catalogo
     */
    public function setCodigo(string $codigo): Catalogo
    {
        $this->codigo = $codigo;
        return $this;
    }

    /**
     * @return string
     */
    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    /**
     * @param string $nombre
     * @return Catalogo
     */
    public function setNombre(string $nombre): Catalogo
    {
        $this->nombre = $nombre;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getDescripcion(): ?string
    {
        return $this->descripcion;
    }

    /**
     * @param null|string $descripcion
     * @return Catalogo
     */
    public function setDescripcion(?string $descripcion): Catalogo
    {
        $this->descripcion = $descripcion;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreado(): ?\DateTime
    {
        return $this->creado;
    }

    /**
     * @param \DateTime $creado
     * @return Catalogo
     */
    public function setCreado(\DateTime $creado): Catalogo
    {
        $this->creado = $creado;
        return $this;
    }

    /**
     * @return CatalogoTipo
     */
    public function getCatalogoTipo(): ?CatalogoTipo
    {
        return $this->catalogo_tipo;
    }

    /**
     * @param CatalogoTipo $catalogo_tipo
     * @return Catalogo
     */
    public function setCatalogoTipo(CatalogoTipo $catalogo_tipo): Catalogo
    {
        $this->catalogo_tipo = $catalogo_tipo;
        return $this;
    }

    /**
     * @return Catalogo
     */
    public function getPadre(): ?Catalogo
    {
        return $this->padre;
    }

    /**
     * @param Catalogo $padre
     * @return Catalogo
     */
    public function setPadre(Catalogo $padre): Catalogo
    {
        $this->padre = $padre;
        return $this;
    }

    /**
     * @return string
     */
    public function __toString(): ?string
    {
        return (string)$this->nombre;
    }

}
