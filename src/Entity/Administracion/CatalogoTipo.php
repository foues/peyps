<?php

namespace App\Entity\Administracion;

use Doctrine\ORM\Mapping as ORM;

/**
 * CatalogoTipo
 *
 * @ORM\Table(name="odontoped_administracion.catalogo_tipo", uniqueConstraints={
 *     @ORM\UniqueConstraint(name="codigo", columns={"codigo"})
 * }, indexes={
 *     @ORM\Index(name="fk_catalogo_tipo_padre", columns={"id_padre"})
 * })
 * @ORM\Entity
 */
class CatalogoTipo
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="codigo", type="string", length=50, nullable=false)
     */
    private $codigo;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=255, nullable=false)
     */
    private $nombre;

    /**
     * @var string|null
     *
     * @ORM\Column(name="descripcion", type="string", length=500, nullable=true)
     */
    private $descripcion;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="creado", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $creado;

    /**
     * @var CatalogoTipo
     *
     * @ORM\ManyToOne(targetEntity="CatalogoTipo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_padre", referencedColumnName="id")
     * })
     */
    private $padre;

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getCodigo(): ?string
    {
        return $this->codigo;
    }

    /**
     * @param string $codigo
     * @return CatalogoTipo
     */
    public function setCodigo(string $codigo): CatalogoTipo
    {
        $this->codigo = $codigo;
        return $this;
    }

    /**
     * @return string
     */
    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    /**
     * @param string $nombre
     * @return CatalogoTipo
     */
    public function setNombre(string $nombre): CatalogoTipo
    {
        $this->nombre = $nombre;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescripcion(): ?string
    {
        return $this->descripcion;
    }

    /**
     * @param string $descripcion
     * @return CatalogoTipo
     */
    public function setDescripcion(string $descripcion): CatalogoTipo
    {
        $this->descripcion = $descripcion;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreado(): ?\DateTime
    {
        return $this->creado;
    }

    /**
     * @param \DateTime $creado
     * @return CatalogoTipo
     */
    public function setCreado(\DateTime $creado): CatalogoTipo
    {
        $this->creado = $creado;
        return $this;
    }

    /**
     * @return CatalogoTipo
     */
    public function getPadre(): ?CatalogoTipo
    {
        return $this->padre;
    }

    /**
     * @param CatalogoTipo $padre
     * @return CatalogoTipo
     */
    public function setPadre(?CatalogoTipo $padre): CatalogoTipo
    {
        $this->padre = $padre;
        return $this;
    }

    /**
     * @return string
     */
    public function __toString(): ?string
    {
        return (string)$this->nombre;
    }

}
