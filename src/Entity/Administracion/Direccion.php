<?php

namespace App\Entity\Administracion;

use Doctrine\ORM\Mapping as ORM;

/**
 * Direccion
 *
 * @ORM\Table(name="odontoped_administracion.direccion", indexes={
 *     @ORM\Index(name="fk_direccion_cuenta", columns={"id_cuenta"}),
 *     @ORM\Index(name="fk_direccion_tipo", columns={"id_direccion_tipo"}),
 *     @ORM\Index(name="fk_direccion_area", columns={"id_direccion_area"}),
 *     @ORM\Index(name="fk_division_politica", columns={"id_division_politica"})
 * })
 * @ORM\Entity
 */
class Direccion
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="calle", type="string", length=255, nullable=true)
     */
    private $calle;

    /**
     * @var string|null
     *
     * @ORM\Column(name="direccion", type="string", length=500, nullable=true)
     */
    private $direccion;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="creado", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $creado;

    /**
     * @var Cuenta
     *
     * @ORM\ManyToOne(targetEntity="Cuenta")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_cuenta", referencedColumnName="id")
     * })
     */
    private $cuenta;

    /**
     * @var Catalogo
     *
     * @ORM\ManyToOne(targetEntity="Catalogo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_direccion_tipo", referencedColumnName="id")
     * })
     */
    private $direccion_tipo;

    /**
     * @var Catalogo
     *
     * @ORM\ManyToOne(targetEntity="Catalogo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_direccion_area", referencedColumnName="id")
     * })
     */
    private $direccion_area;

    /**
     * @var Catalogo
     *
     * @ORM\ManyToOne(targetEntity="Catalogo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_division_politica", referencedColumnName="id")
     * })
     */
    private $division_politica;

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return null|string
     */
    public function getCalle(): ?string
    {
        return $this->calle;
    }

    /**
     * @param null|string $calle
     * @return Direccion
     */
    public function setCalle(?string $calle): Direccion
    {
        $this->calle = $calle;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getDireccion(): ?string
    {
        return $this->direccion;
    }

    /**
     * @param null|string $direccion
     * @return Direccion
     */
    public function setDireccion(?string $direccion): Direccion
    {
        $this->direccion = $direccion;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreado(): ?\DateTime
    {
        return $this->creado;
    }

    /**
     * @param \DateTime $creado
     * @return Direccion
     */
    public function setCreado(\DateTime $creado): Direccion
    {
        $this->creado = $creado;
        return $this;
    }

    /**
     * @return Cuenta
     */
    public function getCuenta(): ?Cuenta
    {
        return $this->cuenta;
    }

    /**
     * @param Cuenta $cuenta
     * @return Direccion
     */
    public function setCuenta(Cuenta $cuenta): Direccion
    {
        $this->cuenta = $cuenta;
        return $this;
    }

    /**
     * @return Catalogo
     */
    public function getDireccionTipo(): ?Catalogo
    {
        return $this->direccion_tipo;
    }

    /**
     * @param Catalogo $direccion_tipo
     * @return Direccion
     */
    public function setDireccionTipo(Catalogo $direccion_tipo): Direccion
    {
        $this->direccion_tipo = $direccion_tipo;
        return $this;
    }

    /**
     * @return Catalogo
     */
    public function getDireccionArea(): ?Catalogo
    {
        return $this->direccion_area;
    }

    /**
     * @param Catalogo $direccion_area
     * @return Direccion
     */
    public function setDireccionArea(Catalogo $direccion_area): Direccion
    {
        $this->direccion_area = $direccion_area;
        return $this;
    }

    /**
     * @return Catalogo
     */
    public function getDivisionPolitica(): ?Catalogo
    {
        return $this->division_politica;
    }

    /**
     * @param Catalogo $division_politica
     * @return Direccion
     */
    public function setDivisionPolitica(Catalogo $division_politica): Direccion
    {
        $this->division_politica = $division_politica;
        return $this;
    }


}
