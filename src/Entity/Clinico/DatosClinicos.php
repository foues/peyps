<?php

namespace App\Entity\Clinico;

use Doctrine\ORM\Mapping as ORM;

/**
 * DatosClinicos
 *
 * @ORM\Table(name="odontoped_clinico.datos_clinicos", indexes={@ORM\Index(name="fk_ficha_datos", columns={"id_ficha"})})
 * @ORM\Entity
 */
class DatosClinicos
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var Ficha
     *
     * @ORM\ManyToOne(targetEntity="Ficha")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_ficha", referencedColumnName="id")
     * })
     */
    private $ficha;

    /**
     * @var int
     *
     * @ORM\Column(name="semanas_embarazo", type="integer", nullable=true)
     */
    private $semanas_embarazo;

    /**
     * @var int
     *
     * @ORM\Column(name="numero_embarazos", type="integer", nullable=true)
     */
    private $numero_embarazos;

    /**
     * @var int
     *
     * @ORM\Column(name="numero_abortos", type="integer", nullable=true)
     */
    private $numero_abortos;

    /**
     * @var int
     *
     * @ORM\Column(name="fur", type="integer", nullable=true)
     */
    private $fur;

    /**
     * @var int
     *
     * @ORM\Column(name="cuartos", type="integer", nullable=true)
     */
    private $cuartos;

    /**
     * @var int
     *
     * @ORM\Column(name="habitantes", type="integer", nullable=true)
     */
    private $habitantes;

    /**
     * @var int
     *
     * @ORM\Column(name="personas", type="integer", nullable=true)
     */
    private $personas;

    /**
     * @var int
     *
     * @ORM\Column(name="leche", type="integer", nullable=true)
     */
    private $leche;

    /**
     * @var int
     *
     * @ORM\Column(name="huevo", type="integer", nullable=true)
     */
    private $huevo;

    /**
     * @var int
     *
     * @ORM\Column(name="carne", type="integer", nullable=true)
     */
    private $carne;

    /**
     * @var int
     *
     * @ORM\Column(name="frutas", type="integer", nullable=true)
     */
    private $frutas;

    /**
     * @var int
     *
     * @ORM\Column(name="verduras", type="integer", nullable=true)
     */
    private $verduras;

    /**
     * @var int
     *
     * @ORM\Column(name="legumbres", type="integer", nullable=true)
     */
    private $legumbres;

    /**
     * @var int
     *
     * @ORM\Column(name="cereales", type="integer", nullable=true)
     */
    private $cereales;

    /**
     * @var int
     *
     * @ORM\Column(name="bano", type="integer", nullable=true)
     */
    private $bano;

    /**
     * @var int
     *
     * @ORM\Column(name="ropa", type="integer", nullable=true)
     */
    private $ropa;

    /**
     * @var int
     *
     * @ORM\Column(name="cepillo", type="integer", nullable=true)
     */
    private $cepillo;

    /**
     * @var int
     *
     * @ORM\Column(name="hilo", type="integer", nullable=true)
     */
    private $hilo;

    /**
     * @var int
     *
     * @ORM\Column(name="enjuague", type="integer", nullable=true)
     */
    private $enjuague;

    /**
     * @var string
     *
     * @ORM\Column(name="localizacion_lesion", type="string", length=25, nullable=true)
     */
    private $localizacion_lesion;

    /**
     * @var string
     *
     * @ORM\Column(name="color_lesion", type="string", length=25, nullable=true)
     */
    private $color_lesion;

    /**
     * @var string
     *
     * @ORM\Column(name="tamano_lesion", type="string", length=25, nullable=true)
     */
    private $tamano_lesion;

    /**
     * @var int
     *
     * @ORM\Column(name="pulso", type="integer", nullable=true)
     */
    private $pulso;

    /**
     * @var int
     *
     * @ORM\Column(name="frecuencia_respiratoria", type="integer", nullable=true)
     */
    private $frecuencia_respiratoria;

    /**
     * @var int
     *
     * @ORM\Column(name="frecuencia_cardiaca", type="integer", nullable=true)
     */
    private $frecuencia_cardiaca;

    /**
     * @var int
     *
     * @ORM\Column(name="presion_arterial", type="integer", nullable=true)
     */
    private $presion_arterial;

    /**
     * @var int
     *
     * @ORM\Column(name="peso", type="integer", nullable=true)
     */
    private $peso;

    /**
     * @var int
     *
     * @ORM\Column(name="estatura", type="integer", nullable=true)
     */
    private $estatura;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSemanasEmbarazo(): ?int
    {
        return $this->semanas_embarazo;
    }

    public function setSemanasEmbarazo(?int $semanas_embarazo): self
    {
        $this->semanas_embarazo = $semanas_embarazo;

        return $this;
    }

    public function getNumeroEmbarazos(): ?int
    {
        return $this->numero_embarazos;
    }

    public function setNumeroEmbarazos(?int $numero_embarazos): self
    {
        $this->numero_embarazos = $numero_embarazos;

        return $this;
    }

    public function getFur(): ?int
    {
        return $this->fur;
    }

    public function setFur(?int $fur): self
    {
        $this->fur = $fur;

        return $this;
    }

    public function getCuartos(): ?int
    {
        return $this->cuartos;
    }

    public function setCuartos(?int $cuartos): self
    {
        $this->cuartos = $cuartos;

        return $this;
    }

    public function getHabitantes(): ?int
    {
        return $this->habitantes;
    }

    public function setHabitantes(?int $habitantes): self
    {
        $this->habitantes = $habitantes;

        return $this;
    }

    public function getPersonas(): ?int
    {
        return $this->personas;
    }

    public function setPersonas(?int $personas): self
    {
        $this->personas = $personas;

        return $this;
    }

    public function getLeche(): ?int
    {
        return $this->leche;
    }

    public function setLeche(?int $leche): self
    {
        $this->leche = $leche;

        return $this;
    }

    public function getHuevo(): ?int
    {
        return $this->huevo;
    }

    public function setHuevo(?int $huevo): self
    {
        $this->huevo = $huevo;

        return $this;
    }

    public function getCarne(): ?int
    {
        return $this->carne;
    }

    public function setCarne(?int $carne): self
    {
        $this->carne = $carne;

        return $this;
    }

    public function getFrutas(): ?int
    {
        return $this->frutas;
    }

    public function setFrutas(?int $frutas): self
    {
        $this->frutas = $frutas;

        return $this;
    }

    public function getVerduras(): ?int
    {
        return $this->verduras;
    }

    public function setVerduras(?int $verduras): self
    {
        $this->verduras = $verduras;

        return $this;
    }

    public function getLegumbres(): ?int
    {
        return $this->legumbres;
    }

    public function setLegumbres(?int $legumbres): self
    {
        $this->legumbres = $legumbres;

        return $this;
    }

    public function getCereales(): ?int
    {
        return $this->cereales;
    }

    public function setCereales(?int $cereales): self
    {
        $this->cereales = $cereales;

        return $this;
    }

    public function getBano(): ?int
    {
        return $this->bano;
    }

    public function setBano(?int $bano): self
    {
        $this->bano = $bano;

        return $this;
    }

    public function getRopa(): ?int
    {
        return $this->ropa;
    }

    public function setRopa(?int $ropa): self
    {
        $this->ropa = $ropa;

        return $this;
    }

    public function getCepillo(): ?int
    {
        return $this->cepillo;
    }

    public function setCepillo(?int $cepillo): self
    {
        $this->cepillo = $cepillo;

        return $this;
    }

    public function getHilo(): ?int
    {
        return $this->hilo;
    }

    public function setHilo(?int $hilo): self
    {
        $this->hilo = $hilo;

        return $this;
    }

    public function getEnjuague(): ?int
    {
        return $this->enjuague;
    }

    public function setEnjuague(?int $enjuague): self
    {
        $this->enjuague = $enjuague;

        return $this;
    }

    public function getLocalizacionLesion(): ?string
    {
        return $this->localizacion_lesion;
    }

    /**
     * @param null|string $localizacion_lesion
     * @return DatosClinicos
     */
    public function setLocalizacionLesion(?string $localizacion_lesion): self
    {
        $this->localizacion_lesion = $localizacion_lesion;

        return $this;
    }

    public function getColorLesion(): ?string
    {
        return $this->color_lesion;
    }

    /**
     * @param null|string $color_lesion
     * @return DatosClinicos
     */
    public function setColorLesion(?string $color_lesion): self
    {
        $this->color_lesion = $color_lesion;

        return $this;
    }

    public function getTamanoLesion(): ?string
    {
        return $this->tamano_lesion;
    }

    /**
     * @param null|string $tamano_lesion
     * @return DatosClinicos
     */
    public function setTamanoLesion(?string $tamano_lesion): self
    {
        $this->tamano_lesion = $tamano_lesion;

        return $this;
    }

    public function getPulso(): ?int
    {
        return $this->pulso;
    }

    public function setPulso(?int $pulso): self
    {
        $this->pulso = $pulso;

        return $this;
    }

    public function getFrecuenciaRespiratoria(): ?int
    {
        return $this->frecuencia_respiratoria;
    }

    public function setFrecuenciaRespiratoria(?int $frecuencia_respiratoria): self
    {
        $this->frecuencia_respiratoria = $frecuencia_respiratoria;

        return $this;
    }

    public function getFrecuenciaCardiaca(): ?int
    {
        return $this->frecuencia_cardiaca;
    }

    public function setFrecuenciaCardiaca(?int $frecuencia_cardiaca): self
    {
        $this->frecuencia_cardiaca = $frecuencia_cardiaca;

        return $this;
    }

    public function getPresionArterial(): ?int
    {
        return $this->presion_arterial;
    }

    public function setPresionArterial(?int $presion_arterial): self
    {
        $this->presion_arterial = $presion_arterial;

        return $this;
    }

    public function getPeso(): ?int
    {
        return $this->peso;
    }

    public function setPeso(?int $peso): self
    {
        $this->peso = $peso;

        return $this;
    }

    public function getEstatura(): ?int
    {
        return $this->estatura;
    }

    public function setEstatura(?int $estatura): self
    {
        $this->estatura = $estatura;

        return $this;
    }

    public function getFicha(): ?Ficha
    {
        return $this->ficha;
    }

    public function setFicha(?Ficha $ficha): self
    {
        $this->ficha = $ficha;

        return $this;
    }

    public function getNumeroAbortos(): ?int
    {
        return $this->numero_abortos;
    }

    public function setNumeroAbortos(?int $numero_abortos): self
    {
        $this->numero_abortos = $numero_abortos;

        return $this;
    }

}
