<?php

namespace App\Entity\Clinico;

use Doctrine\ORM\Mapping as ORM;

/**
 * Respuesta
 *
 * @ORM\Table(name="odontoped_clinico.respuesta", indexes={})
 * @ORM\Entity
 */
class Respuesta {

  /**
   * @var int
   *
   * @ORM\Column(name="id", type="integer", nullable=false)
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="IDENTITY")
   */
private $id;


/**
 * @var string
 *
 * @ORM\Column(name="nombre_resp", type="string", length=500, nullable=false)
 */
private $nombre_resp;


/**
 * @var OpcionesPregunta
 *
 * @ORM\ManyToOne(targetEntity="OpcionesPregunta")
 * @ORM\JoinColumns({
 *   @ORM\JoinColumn(name="opcion_pregunta_fk", referencedColumnName="id")
 * })
 */
 private $opciones_pregunta;

public function getId(): ?int
{
    return $this->id;
}

public function getNombreResp(): ?string
{
    return $this->nombre_resp;
}

public function setNombreResp(string $nombre_resp): self
{
    $this->nombre_resp = $nombre_resp;

    return $this;
}

public function getOpcionesPregunta(): ?OpcionesPregunta
{
    return $this->opciones_pregunta;
}

public function setOpcionesPregunta(string $opciones_pregunta): self
{
    $this->opciones_pregunta = $opciones_pregunta;
    return $this;
}



}
