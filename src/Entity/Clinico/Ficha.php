<?php

namespace App\Entity\Clinico;

use Doctrine\ORM\Mapping as ORM;

/**
 * Ficha
 *
 * @ORM\Table(name="odontoped_clinico.ficha", indexes={
 *     @ORM\Index(name="fk_ficha_cuenta", columns={"id_cuenta"}),
 *     @ORM\Index(name="fk_ficha_expediente_clinico", columns={"id_expediente_clinico"}),
 *     @ORM\Index(name="fk_referencia", columns={"id_referencia"}),
 *     @ORM\Index(name="fk_frecuencia_cepillado", columns={"id_frecuencia_cepillado"}),
 *     @ORM\Index(name="fk_indice_riesgo_cariogenico", columns={"id_indice_riesgo_cariogenico"}),
 *     @ORM\Index(name="fk_indice_riesgo_real", columns={"id_indice_riesgo_real"})
 * })
 * @ORM\Entity(repositoryClass="App\Repository\Clinico\FichaRepository")
 */
class Ficha
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha_control", type="date", nullable=false)
     */
    private $fecha_control;

    /**
     * @var bool
     *
     * @ORM\Column(name="visita_odontologo", type="boolean", nullable=false)
     */
    private $visita_odontologo = false;

    /**
     * @var bool
     *
     * @ORM\Column(name="embarazada", type="boolean", nullable=false)
     */
    private $embarazada = false;

    /**
     * @var bool
     *
     * @ORM\Column(name="trauma_dentoalveolar", type="boolean", nullable=false)
     */
    private $trauma_dentoalveolar = false;

    /**
     * @var bool
     *
     * @ORM\Column(name="reacciones_adversas", type="boolean", nullable=false)
     */
    private $reacciones_adversas = false;

    /**
     * @var string
     *
     * @ORM\Column(name="pieza", type="string", length=255, nullable=true)
     */
    private $pieza;

    /**
     * @var string
     *
     * @ORM\Column(name="tipo_reaccion", type="string", length=255, nullable=true)
     */
    private $tipo_reaccion;

    /**
     * @var string
     *
     * @ORM\Column(name="motivo_consulta", type="string", length=999, nullable=true)
     */
    private $motivo_consulta;

    /**
     * @var string
     *
     * @ORM\Column(name="historia_enfermedad", type="string", length=999, nullable=true)
     */
    private $historia_enfermedad;

    /**
     * @var string
     *
     * @ORM\Column(name="anotaciones_varias", type="string", length=999, nullable=true)
     */
    private $anotaciones_varias;

    /**
     * @ORM\Column(name="finalizada", type="boolean", options={"default":false})
     */
    private $finalizada = false;

    /**
     * @var string
     *
     * @ORM\Column(name="curso", type="string", length=10, nullable=true)
     */
    private $curso;

    /**
     * @var string
     *
     * @ORM\Column(name="ciclo", type="string", length=10, nullable=true)
     */
    private $ciclo;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="creado", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $creado;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="actualizado", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $actualizado;

    /**
     * @var \App\Entity\Administracion\Cuenta
     *
     * @ORM\ManyToOne(targetEntity="\App\Entity\Administracion\Cuenta")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_cuenta", referencedColumnName="id")
     * })
     */
    private $cuenta;

    /**
     * @var ExpedienteClinico
     *
     * @ORM\ManyToOne(targetEntity="ExpedienteClinico")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_expediente_clinico", referencedColumnName="id")
     * })
     */
    private $expediente_clinico;

    /**
     * @var \App\Entity\Administracion\Catalogo
     *
     * @ORM\ManyToOne(targetEntity="\App\Entity\Administracion\Catalogo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_referencia", referencedColumnName="id")
     * })
     */
    private $referencia;

    /**
     * @var FichaCatalogo
     *
     * @ORM\ManyToOne(targetEntity="FichaCatalogo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_frecuencia_cepillado", referencedColumnName="id")
     * })
     */
    private $frecuencia_cepillado;

    /**
     * @var FichaCatalogo
     *
     * @ORM\ManyToOne(targetEntity="FichaCatalogo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_indice_riesgo_cariogenico", referencedColumnName="id")
     * })
     */
    private $indice_riesgo_cariogenico;

    /**
     * @var FichaCatalogo
     *
     * @ORM\ManyToOne(targetEntity="FichaCatalogo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_indice_riesgo_real", referencedColumnName="id")
     * })
     */
    private $indice_riesgo_real;

    /**
     * @var FichaCatalogo
     *
     * @ORM\ManyToOne(targetEntity="FichaCatalogo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_transfusiones", referencedColumnName="id")
     * })
     */
    private $transfusiones;

    /**
     * @var FichaCatalogo
     *
     * @ORM\ManyToOne(targetEntity="FichaCatalogo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_orientacion_sexual", referencedColumnName="id")
     * })
     */
    private $orientacion_sexual;

    /**
     * @var FichaCatalogo
     *
     * @ORM\ManyToOne(targetEntity="FichaCatalogo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_material_casa", referencedColumnName="id")
     * })
     */
    private $material_casa;

    /**
     * @var FichaCatalogo
     *
     * @ORM\ManyToOne(targetEntity="FichaCatalogo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_lesion", referencedColumnName="id")
     * })
     */
    private $lesion;

    /**
     * @var FichaCatalogo
     *
     * @ORM\ManyToOne(targetEntity="FichaCatalogo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_tipo_lesion", referencedColumnName="id")
     * })
     */
    private $tipo_lesion;

    /**
     * @var FichaCatalogo
     *
     * @ORM\ManyToOne(targetEntity="FichaCatalogo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_consistencia_lesion", referencedColumnName="id")
     * })
     */
    private $consistencia_lesion;

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return \DateTime
     */
    public function getFechaControl(): ?\DateTime
    {
        return $this->fecha_control;
    }

    /**
     * @param \DateTime $fecha_control
     * @return Ficha
     */
    public function setFechaControl(\DateTime $fecha_control): Ficha
    {
        $this->fecha_control = $fecha_control;
        return $this;
    }

    /**
     * @return bool
     */
    public function getVisitaOdontologo(): ?bool
    {
        return $this->visita_odontologo;
    }

    /**
     * @param bool $visita_odontologo
     * @return Ficha
     */
    public function setVisitaOdontologo(bool $visita_odontologo): Ficha
    {
        $this->visita_odontologo = $visita_odontologo;
        return $this;
    }

    /**
     * @return bool
     */
    public function getEmbarazada(): ?bool
    {
        return $this->embarazada;
    }

    /**
     * @param bool $embarazada
     * @return Ficha
     */
    public function setEmbarazada(bool $embarazada): Ficha
    {
        $this->embarazada = $embarazada;
        return $this;
    }

    /**
     * @return bool
     */
    public function getTraumaDentoalveolar(): ?bool
    {
        return $this->trauma_dentoalveolar;
    }

    /**
     * @param bool $trauma_dentoalveolar
     * @return Ficha
     */
    public function setTraumaDentoalveolar(bool $trauma_dentoalveolar): Ficha
    {
        $this->trauma_dentoalveolar = $trauma_dentoalveolar;
        return $this;
    }

    /**
     * @return bool
     */
    public function getReaccionesAdversas(): ?bool
    {
        return $this->reacciones_adversas;
    }

    /**
     * @param bool $reacciones_adversas
     * @return Ficha
     */
    public function setReaccionesAdversas(bool $reacciones_adversas): Ficha
    {
        $this->reacciones_adversas = $reacciones_adversas;
        return $this;
    }

    /**
     * @return string
     */
    public function getPieza(): ?string
    {
        return $this->pieza;
    }

    /**
     * @param string $pieza
     * @return Ficha
     */
    public function setPieza(?string $pieza): Ficha
    {
        $this->pieza = $pieza;
        return $this;
    }

    /**
     * @return string
     */
    public function getTipoReaccion(): ?string
    {
        return $this->tipo_reaccion;
    }

    /**
     * @param string $tipo_reaccion
     * @return Ficha
     */
    public function setTipoReaccion(string $tipo_reaccion): Ficha
    {
        $this->tipo_reaccion = $tipo_reaccion;
        return $this;
    }

    /**
     * @return string
     */
    public function getMotivoConsulta(): ?string
    {
        return $this->motivo_consulta;
    }

    /**
     * @param string $motivo_consulta
     * @return Ficha
     */
    public function setMotivoConsulta(string $motivo_consulta): Ficha
    {
        $this->motivo_consulta = $motivo_consulta;
        return $this;
    }

    /**
     * @return string
     */
    public function getHistoriaEnfermedad(): ?string
    {
        return $this->historia_enfermedad;
    }

    /**
     * @param string $historia_enfermedad
     * @return Ficha
     */
    public function setHistoriaEnfermedad(string $historia_enfermedad): Ficha
    {
        $this->historia_enfermedad = $historia_enfermedad;
        return $this;
    }

    /**
     * @return string
     */
    public function getAnotacionesVarias(): ?string
    {
        return $this->anotaciones_varias;
    }

    /**
     * @param null|string $anotaciones_varias
     * @return Ficha
     */
    public function setAnotacionesVarias(?string $anotaciones_varias): Ficha
    {
        $this->anotaciones_varias = $anotaciones_varias;
        return $this;
    }

    /**
     * @return bool|null
     */
    public function getFinalizada(): ?bool
    {
        return $this->finalizada;
    }

    /**
     * @param bool $finalizada
     * @return Ficha
     */
    public function setFinalizada(bool $finalizada): Ficha
    {
        $this->finalizada = $finalizada;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getCurso(): ?string
    {
        return $this->curso;
    }

    /**
     * @param string $curso
     * @return Ficha
     */
    public function setCurso(string $curso): Ficha
    {
        $this->curso = $curso;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getCiclo(): ?string
    {
        return $this->ciclo;
    }

    /**
     * @param string $ciclo
     * @return Ficha
     */
    public function setCiclo(string $ciclo): Ficha
    {
        $this->ciclo = $ciclo;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreado(): ?\DateTime
    {
        return $this->creado;
    }

    /**
     * @param \DateTime $creado
     * @return Ficha
     */
    public function setCreado(\DateTime $creado): Ficha
    {
        $this->creado = $creado;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getActualizado(): ?\DateTime
    {
        return $this->actualizado;
    }

    /**
     * @param \DateTime $actualizado
     * @return Ficha
     */
    public function setActualizado(\DateTime $actualizado): Ficha
    {
        $this->actualizado = $actualizado;
        return $this;
    }

    /**
     * @return \App\Entity\Administracion\Cuenta
     */
    public function getCuenta(): ?\App\Entity\Administracion\Cuenta
    {
        return $this->cuenta;
    }

    /**
     * @param \App\Entity\Administracion\Cuenta $cuenta
     * @return Ficha
     */
    public function setCuenta(\App\Entity\Administracion\Cuenta $cuenta): Ficha
    {
        $this->cuenta = $cuenta;
        return $this;
    }

    /**
     * @return ExpedienteClinico
     */
    public function getExpedienteClinico(): ?ExpedienteClinico
    {
        return $this->expediente_clinico;
    }

    /**
     * @param ExpedienteClinico $expediente_clinico
     * @return Ficha
     */
    public function setExpedienteClinico(ExpedienteClinico $expediente_clinico): Ficha
    {
        $this->expediente_clinico = $expediente_clinico;
        return $this;
    }

    /**
     * @return \App\Entity\Administracion\Catalogo
     */
    public function getReferencia(): ?\App\Entity\Administracion\Catalogo
    {
        return $this->referencia;
    }

    /**
     * @param \App\Entity\Administracion\Catalogo $referencia
     * @return Ficha
     */
    public function setReferencia(\App\Entity\Administracion\Catalogo $referencia): Ficha
    {
        $this->referencia = $referencia;
        return $this;
    }

    /**
     * @return FichaCatalogo
     */
    public function getFrecuenciaCepillado(): ?FichaCatalogo
    {
        return $this->frecuencia_cepillado;
    }

    /**
     * @param FichaCatalogo $frecuencia_cepillado
     * @return Ficha
     */
    public function setFrecuenciaCepillado(?FichaCatalogo $frecuencia_cepillado): Ficha
    {
        $this->frecuencia_cepillado = $frecuencia_cepillado;
        return $this;
    }

    /**
     * @return FichaCatalogo
     */
    public function getIndiceRiesgoCariogenico(): ?FichaCatalogo
    {
        return $this->indice_riesgo_cariogenico;
    }

    /**
     * @param FichaCatalogo $indice_riesgo_cariogenico
     * @return Ficha
     */
    public function setIndiceRiesgoCariogenico(?FichaCatalogo $indice_riesgo_cariogenico): Ficha
    {
        $this->indice_riesgo_cariogenico = $indice_riesgo_cariogenico;
        return $this;
    }

    /**
     * @return FichaCatalogo|null
     */
    public function getIndiceRiesgoReal(): ?FichaCatalogo
    {
        return $this->indice_riesgo_real;
    }

    /**
     * @param FichaCatalogo|null $indice_riesgo_real
     * @return Ficha
     */
    public function setIndiceRiesgoReal(?FichaCatalogo $indice_riesgo_real): self
    {
        $this->indice_riesgo_real = $indice_riesgo_real;
        return $this;
    }

    /**
     * @return FichaCatalogo
     */
    public function getTransfusiones(): ?FichaCatalogo
    {
        return $this->transfusiones;
    }

    /**
     * @param FichaCatalogo $transfusiones
     * @return Ficha
     */
    public function setTransfusiones(?FichaCatalogo $transfusiones): Ficha
    {
        $this->transfusiones = $transfusiones;
        return $this;
    }

    /**
     * @return FichaCatalogo
     */
    public function getOrientacionSexual(): ?FichaCatalogo
    {
        return $this->orientacion_sexual;
    }

    /**
     * @param FichaCatalogo $orientacion_sexual
     * @return Ficha
     */
    public function setOrientacionSexual(?FichaCatalogo $orientacion_sexual): Ficha
    {
        $this->orientacion_sexual = $orientacion_sexual;
        return $this;
    }

    /**
     * @return FichaCatalogo
     */
    public function getMaterialCasa(): ?FichaCatalogo
    {
        return $this->material_casa;
    }

    /**
     * @param FichaCatalogo $material_casa
     * @return Ficha
     */
    public function setMaterialCasa(?FichaCatalogo $material_casa): Ficha
    {
        $this->material_casa = $material_casa;
        return $this;
    }

    /**
     * @return FichaCatalogo
     */
    public function getLesion(): ?FichaCatalogo
    {
        return $this->lesion;
    }

    /**
     * @param FichaCatalogo $lesion
     * @return Ficha
     */
    public function setLesion(?FichaCatalogo $lesion): Ficha
    {
        $this->lesion = $lesion;
        return $this;
    }

    /**
     * @return FichaCatalogo
     */
    public function getTipoLesion(): ?FichaCatalogo
    {
        return $this->tipo_lesion;
    }

    /**
     * @param FichaCatalogo $tipo_lesion
     * @return Ficha
     */
    public function setTipoLesion(?FichaCatalogo $tipo_lesion): Ficha
    {
        $this->tipo_lesion = $tipo_lesion;
        return $this;
    }

    /**
     * @return FichaCatalogo
     */
    public function getConsistenciaLesion(): ?FichaCatalogo
    {
        return $this->consistencia_lesion;
    }

    /**
     * @param FichaCatalogo $consistencia_lesion
     * @return Ficha
     */
    public function setConsistenciaLesion(?FichaCatalogo $consistencia_lesion): Ficha
    {
        $this->consistencia_lesion = $consistencia_lesion;
        return $this;
    }

}
