<?php

namespace App\Entity\Clinico;

use Doctrine\ORM\Mapping as ORM;

/**
 * Categoria
 *
 * @ORM\Table(name="odontoped_clinico.categoria", indexes={})
 * @ORM\Entity
 */
class Categoria{

  /**
   * @var int
   *
   * @ORM\Column(name="id", type="integer", nullable=false)
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="IDENTITY")
   */
private $id;


/**
 * @var string
 *
 * @ORM\Column(name="nombre_cat", type="string", length=500, nullable=false)
 */
private $nombre_cat;

public function getId(): ?int
{
    return $this->id;
}

public function getNombreCat(): ?string
{
    return $this->nombre_cat;
}

public function setNombreCat(string $nombre_cat): self
{
    $this->nombre_cat = $nombre_cat;

    return $this;
}

}
