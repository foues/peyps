<?php

namespace App\Entity\Clinico;

use Doctrine\ORM\Mapping as ORM;


/**
 * Pregunta
 *
 * @ORM\Table(name="odontoped_clinico.pregunta", indexes={
 *     @ORM\Index(name="categoria_fk", columns={"categoria_fk"})
 * })
 * @ORM\Entity
 */
class Pregunta {

  /**
   * @var int
   *
   * @ORM\Column(name="id", type="integer", nullable=false)
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="IDENTITY")
   */
private $id;

/**
 * @var string
 *
 * @ORM\Column(name="nombre_preg", type="string", length=500, nullable=false)
 */
private $nombre_preg;

/**
 * @var Categoria
 *
 * @ORM\ManyToOne(targetEntity="Categoria")
 * @ORM\JoinColumns({
 *   @ORM\JoinColumn(name="categoria_fk", referencedColumnName="id")
 * })
 */
private $categoria;

public function getId(): ?int
{
    return $this->id;
}

public function getNombrePreg(): ?string
{
    return $this->nombre_preg;
}

public function setNombrePreg(string $nombre_preg): self
{
    $this->nombre_preg = $nombre_preg;

    return $this;
}

public function getCategoria(): ?Categoria
{
    return $this->categoria;
}

public function setCategoria(?Categoria $categoria): self
{
    $this->categoria = $categoria;

    return $this;
}

}
