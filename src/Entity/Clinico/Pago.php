<?php

namespace App\Entity\Clinico;

use Doctrine\ORM\Mapping as ORM;

/**
 * Pago
 *
 * @ORM\Table(name="odontoped_clinico.pago", indexes={
 *     @ORM\Index(name="fk_pago_expediente_clinico", columns={"id_expediente_clinico"})
 * })
 * @ORM\Entity
 */
class Pago
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="numero_recibo", type="integer", nullable=false)
     */
    private $numero_recibo;

    /**
     * @var string
     *
     * @ORM\Column(name="concepto", type="string", length=255, nullable=false)
     */
    private $concepto;

    /**
     * @var float
     *
     * @ORM\Column(name="costo", type="float", precision=10, scale=0, nullable=false)
     */
    private $costo;

    /**
     * @var float
     *
     * @ORM\Column(name="abono", type="float", precision=10, scale=0, nullable=false)
     */
    private $abono;

    /**
     * @var float
     *
     * @ORM\Column(name="saldo", type="float", precision=10, scale=0, nullable=false)
     */
    private $saldo;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha", type="date", nullable=false)
     */
    private $fecha;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="creado", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $creado;

    /**
     * @var ExpedienteClinico
     *
     * @ORM\ManyToOne(targetEntity="ExpedienteClinico")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_expediente_clinico", referencedColumnName="id")
     * })
     */
    private $expediente_clinico;

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getNumeroRecibo(): ?int
    {
        return $this->numero_recibo;
    }

    /**
     * @param int $numero_recibo
     * @return Pago
     */
    public function setNumeroRecibo(int $numero_recibo): Pago
    {
        $this->numero_recibo = $numero_recibo;
        return $this;
    }

    /**
     * @return string
     */
    public function getConcepto(): ?string
    {
        return $this->concepto;
    }

    /**
     * @param string $concepto
     * @return Pago
     */
    public function setConcepto(string $concepto): Pago
    {
        $this->concepto = $concepto;
        return $this;
    }

    /**
     * @return float
     */
    public function getCosto(): ?float
    {
        return $this->costo;
    }

    /**
     * @param float $costo
     * @return Pago
     */
    public function setCosto(float $costo): Pago
    {
        $this->costo = $costo;
        return $this;
    }

    /**
     * @return float
     */
    public function getAbono(): ?float
    {
        return $this->abono;
    }

    /**
     * @param float $abono
     * @return Pago
     */
    public function setAbono(float $abono): Pago
    {
        $this->abono = $abono;
        return $this;
    }

    /**
     * @return float
     */
    public function getSaldo(): ?float
    {
        return $this->saldo;
    }

    /**
     * @param float $saldo
     * @return Pago
     */
    public function setSaldo(float $saldo): Pago
    {
        $this->saldo = $saldo;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getFecha(): ?\DateTime
    {
        return $this->fecha;
    }

    /**
     * @param \DateTime $fecha
     * @return Pago
     */
    public function setFecha(\DateTime $fecha): Pago
    {
        $this->fecha = $fecha;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreado(): ?\DateTime
    {
        return $this->creado;
    }

    /**
     * @param \DateTime $creado
     * @return Pago
     */
    public function setCreado(\DateTime $creado): Pago
    {
        $this->creado = $creado;
        return $this;
    }

    /**
     * @return ExpedienteClinico
     */
    public function getExpedienteClinico(): ?ExpedienteClinico
    {
        return $this->expediente_clinico;
    }

    /**
     * @param ExpedienteClinico $expediente_clinico
     * @return Pago
     */
    public function setExpedienteClinico(ExpedienteClinico $expediente_clinico): Pago
    {
        $this->expediente_clinico = $expediente_clinico;
        return $this;
    }


}
