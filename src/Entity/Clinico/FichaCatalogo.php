<?php

namespace App\Entity\Clinico;

use Doctrine\ORM\Mapping as ORM;

/**
 * FichaCatalogo
 *
 * @ORM\Table(name="odontoped_clinico.ficha_catalogo", uniqueConstraints={
 *     @ORM\UniqueConstraint(name="codigo", columns={"codigo"})
 * }, indexes={
 *     @ORM\Index(name="fk_ficha_catalogo_tipo", columns={"id_tipo"})
 * })
 * @ORM\Entity(repositoryClass="App\Repository\Clinico\FichaCatalogoRepository")
 */
class FichaCatalogo
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="codigo", type="string", length=50, nullable=false)
     */
    private $codigo;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=255, nullable=false)
     */
    private $nombre;

    /**
     * @var string|null
     *
     * @ORM\Column(name="descripcion", type="string", length=500, nullable=true)
     */
    private $descripcion;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="creado", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $creado;

    /**
     * @var FichaCatalogo
     *
     * @ORM\ManyToOne(targetEntity="FichaCatalogo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_tipo", referencedColumnName="id")
     * })
     */
    private $ficha_catalogo_tipo;

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getCodigo(): ?string
    {
        return $this->codigo;
    }

    /**
     * @param string $codigo
     * @return FichaCatalogo
     */
    public function setCodigo(string $codigo): FichaCatalogo
    {
        $this->codigo = $codigo;
        return $this;
    }

    /**
     * @return string
     */
    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    /**
     * @param string $nombre
     * @return FichaCatalogo
     */
    public function setNombre(string $nombre): FichaCatalogo
    {
        $this->nombre = $nombre;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getDescripcion(): ?string
    {
        return $this->descripcion;
    }

    /**
     * @param null|string $descripcion
     * @return FichaCatalogo
     */
    public function setDescripcion(?string $descripcion): FichaCatalogo
    {
        $this->descripcion = $descripcion;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreado(): ?\DateTime
    {
        return $this->creado;
    }

    /**
     * @param \DateTime $creado
     * @return FichaCatalogo
     */
    public function setCreado(\DateTime $creado): FichaCatalogo
    {
        $this->creado = $creado;
        return $this;
    }

    /**
     * @return FichaCatalogo
     */
    public function getFichaCatalogoTipo(): ?FichaCatalogo
    {
        return $this->ficha_catalogo_tipo;
    }

    /**
     * @param FichaCatalogo $ficha_catalogo_tipo
     * @return FichaCatalogo
     */
    public function setFichaCatalogoTipo(FichaCatalogo $ficha_catalogo_tipo): FichaCatalogo
    {
        $this->ficha_catalogo_tipo = $ficha_catalogo_tipo;
        return $this;
    }

    /**
     * @return string
     */
    public function __toString(): ?string
    {
        return (string)$this->nombre;
    }


}
