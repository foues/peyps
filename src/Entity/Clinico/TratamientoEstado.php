<?php

namespace App\Entity\Clinico;

use Doctrine\ORM\Mapping as ORM;

/**
 * TratamientoEstado
 *
 * @ORM\Table(name="odontoped_clinico.tratamiento_estado", uniqueConstraints={
 *     @ORM\UniqueConstraint(name="tratamiento_estado_unique", columns={"id_tratamiento", "id_estado"})
 * }, indexes={
 *     @ORM\Index(name="fk_tratamiento_estado_estado", columns={"id_estado"}),
 *     @ORM\Index(name="fk_tratamiento_estado_tratamiento", columns={"id_tratamiento"})
 * })
 * @ORM\Entity
 */
class TratamientoEstado
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="creado", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $creado;

    /**
     * @var FichaCatalogo
     *
     * @ORM\ManyToOne(targetEntity="FichaCatalogo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_estado", referencedColumnName="id")
     * })
     */
    private $estado;

    /**
     * @var Tratamiento
     *
     * @ORM\ManyToOne(targetEntity="Tratamiento")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_tratamiento", referencedColumnName="id")
     * })
     */
    private $tratamiento;

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return \DateTime
     */
    public function getCreado(): ?\DateTime
    {
        return $this->creado;
    }

    /**
     * @param \DateTime $creado
     * @return TratamientoEstado
     */
    public function setCreado(\DateTime $creado): TratamientoEstado
    {
        $this->creado = $creado;
        return $this;
    }

    /**
     * @return FichaCatalogo
     */
    public function getEstado(): ?FichaCatalogo
    {
        return $this->estado;
    }

    /**
     * @param FichaCatalogo $estado
     * @return TratamientoEstado
     */
    public function setEstado(FichaCatalogo $estado): TratamientoEstado
    {
        $this->estado = $estado;
        return $this;
    }

    /**
     * @return Tratamiento
     */
    public function getTratamiento(): ?Tratamiento
    {
        return $this->tratamiento;
    }

    /**
     * @param Tratamiento $tratamiento
     * @return TratamientoEstado
     */
    public function setTratamiento(Tratamiento $tratamiento): TratamientoEstado
    {
        $this->tratamiento = $tratamiento;
        return $this;
    }

}
