<?php

namespace App\Entity\Clinico;

use Doctrine\ORM\Mapping as ORM;

/**
 * EvaluacionDental
 *
 * @ORM\Table(name="odontoped_clinico.evaluacion_dental", uniqueConstraints={
 *     @ORM\UniqueConstraint(name="unique_evaluacion_dental", columns={"id_ficha","id_tratamiento","id_pieza_dental_estado","pieza_dental","superficie_dental"})
 * }, indexes={
 *     @ORM\Index(name="fk_evaluacion_dental_ficha", columns={"id_ficha"}),
 *     @ORM\Index(name="fk_evaluacion_dental_tratamiento", columns={"id_tratamiento"}),
 *     @ORM\Index(name="fk_evaluacion_pieza_dental_estado", columns={"id_pieza_dental_estado"})
 * })
 * @ORM\Entity(repositoryClass="App\Repository\Clinico\EvaluacionDentalRepository")
 */
class EvaluacionDental
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @ORM\Column(name="realizado", type="boolean",  nullable=true, options={"default":false})
     */
    private $realizado = false;

    /**
     * @ORM\Column(name="retratamiento", type="boolean", options={"default":false})
     */
    private $retratamiento;

    /**
     * @ORM\Column(name="prioridad", type="boolean",  nullable=true, options={"default":false})
     */
    private $prioridad = false;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="creado", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $creado;

    /**
     * @var Ficha
     *
     * @ORM\ManyToOne(targetEntity="Ficha")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_ficha", referencedColumnName="id")
     * })
     */
    private $ficha;

    /**
     * @var Tratamiento
     *
     * @ORM\ManyToOne(targetEntity="Tratamiento")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_tratamiento", referencedColumnName="id")
     * })
     */
    private $tratamiento;

    /**
     * @var string
     *
     * @ORM\Column(name="pieza_dental", type="string", length=10, nullable=false)
     */
    private $pieza_dental;

    /**
     * @var string
     *
     * @ORM\Column(name="superficie_dental", type="string", length=15, nullable=false)
     */
    private $superficie_dental;

    /**
     * @var FichaCatalogo
     *
     * @ORM\ManyToOne(targetEntity="FichaCatalogo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_pieza_dental_estado", referencedColumnName="id")
     * })
     */
    private $pieza_dental_estado;

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return bool|null
     */
    public function getRealizado(): ?bool
    {
        return $this->realizado;
    }

    /**
     * @param bool $realizado
     * @return EvaluacionDental
     */
    public function setRealizado(bool $realizado): EvaluacionDental
    {
        $this->realizado = $realizado;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getRetratamiento(): ?bool
    {
        return $this->retratamiento;
    }

    /**
     * @param mixed $retratamiento
     * @return EvaluacionDental
     */
    public function setRetratamiento($retratamiento): EvaluacionDental
    {
        $this->retratamiento = $retratamiento;
        return $this;
    }

    /**
     * @return bool|null
     */
    public function getPrioridad(): ?bool
    {
        return $this->prioridad;
    }

    /**
     * @param bool $prioridad
     * @return EvaluacionDental
     */
    public function setPrioridad(bool $prioridad): EvaluacionDental
    {
        $this->prioridad = $prioridad;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreado(): ?\DateTime
    {
        return $this->creado;
    }

    /**
     * @param \DateTime $creado
     * @return EvaluacionDental
     */
    public function setCreado(\DateTime $creado): EvaluacionDental
    {
        $this->creado = $creado;
        return $this;
    }

    /**
     * @return Ficha
     */
    public function getFicha(): ?Ficha
    {
        return $this->ficha;
    }

    /**
     * @param Ficha $ficha
     * @return EvaluacionDental
     */
    public function setFicha(Ficha $ficha): EvaluacionDental
    {
        $this->ficha = $ficha;
        return $this;
    }

    public function getTratamiento(): ?Tratamiento
    {
        return $this->tratamiento;
    }

    public function setTratamiento(?Tratamiento $tratamiento): self
    {
        $this->tratamiento = $tratamiento;

        return $this;
    }

    /**
     * @return null|string
     */
    public function getPiezaDental(): ?string
    {
        return $this->pieza_dental;
    }

    /**
     * @param string $pieza_dental
     * @return EvaluacionDental
     */
    public function setPiezaDental(string $pieza_dental): EvaluacionDental
    {
        $this->pieza_dental = $pieza_dental;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getSuperficieDental(): ?string
    {
        return $this->superficie_dental;
    }

    /**
     * @param string $superficie_dental
     * @return EvaluacionDental
     */
    public function setSuperficieDental(string $superficie_dental): EvaluacionDental
    {
        $this->superficie_dental = $superficie_dental;
        return $this;
    }

    /**
     * @return FichaCatalogo
     */
    public function getPiezaDentalEstado(): ?FichaCatalogo
    {
        return $this->pieza_dental_estado;
    }

    /**
     * @param FichaCatalogo $pieza_dental_estado
     * @return EvaluacionDental
     */
    public function setPiezaDentalEstado(FichaCatalogo $pieza_dental_estado): EvaluacionDental
    {
        $this->pieza_dental_estado = $pieza_dental_estado;
        return $this;
    }

}
