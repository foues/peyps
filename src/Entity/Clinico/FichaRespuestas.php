<?php

namespace App\Entity\Clinico;

use Doctrine\ORM\Mapping as ORM;

/**
 * FichaRespuestas
 *
 * @ORM\Table(name="odontoped_clinico.ficha_respuestas", indexes={
 *     @ORM\Index(name="opciones_respuesta_fk", columns={"opciones_respuesta_fk"}),
 *     @ORM\Index(name="respuesta_fk", columns={"respuesta_fk"}),
 *     @ORM\Index(name="ficha_fk", columns={"ficha_fk"})
 * })
 * @ORM\Entity
 */
class FichaRespuestas
{
  /**
   * @var int
   *
   * @ORM\Column(name="id", type="integer", nullable=false)
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="IDENTITY")
   */
private $id;


/**
 * @var OpcionesPregunta
 *
 * @ORM\ManyToOne(targetEntity="OpcionesPregunta")
 * @ORM\JoinColumns({
 *   @ORM\JoinColumn(name="opciones_pregunta_fk", referencedColumnName="id")
 * })
 */
private $opciones_pregunta;

/**
 * @var Respuesta
 *
 * @ORM\ManyToOne(targetEntity="Respuesta")
 * @ORM\JoinColumns({
 *   @ORM\JoinColumn(name="respuesta_fk", referencedColumnName="id")
 * })
 */
private $respuesta;

/**
 * @var Ficha
 *
 * @ORM\ManyToOne(targetEntity="Ficha")
 * @ORM\JoinColumns({
 *   @ORM\JoinColumn(name="ficha_fk", referencedColumnName="id")
 * })
 */
private $ficha;

/**
 * @var string
 *
 * @ORM\Column(name="descripcion", type="string", length=250)
 */
private $descripcion;

public function getDescripcion(): ?string
{
    return $this->descripcion;
}

public function setDescripcion($descripcion): self
{
    $this->descripcion = $descripcion;

    return $this;
}


/**
 * @var string
 *
 * @ORM\Column(name="unidades", type="string", length=30)
 */
private $unidades;

public function getUnidades(): ?string
{
    return $this->unidades;
}

public function setUnidades($unidades): self
{
    $this->unidades = $unidades;

    return $this;
}




public function getId(): ?int
{
    return $this->id;
}

public function getOpcionesPregunta(): ?OpcionesPregunta
{
    return $this->opciones_pregunta;
}

public function setOpcionesPregunta(?OpcionesPregunta $opciones_pregunta): self
{
    $this->opciones_pregunta = $opciones_pregunta;

    return $this;
}

public function getRespuesta(): ?Respuesta
{
    return $this->respuesta;
}

public function setRespuesta(?Respuesta $respuesta): self
{
    $this->respuesta = $respuesta;

    return $this;
}

public function getFicha(): ?Ficha
{
    return $this->ficha;
}

public function setFicha(?Ficha $ficha): self
{
    $this->ficha = $ficha;

    return $this;
}



}
