<?php

namespace App\Entity\Clinico;

use Doctrine\ORM\Mapping as ORM;

/**
 * ControlSellante
 *
 * @ORM\Table(name="odontoped_clinico.control_sellante", indexes={
 *     @ORM\Index(name="fk_control_sellante_ficha", columns={"id_ficha"}),
 *     @ORM\Index(name="fk_control_sellante_tratamiento", columns={"id_tratamiento"}),
 *     @ORM\Index(name="fk_sellante_estado", columns={"id_estado_sellante"})
 * })
 * @ORM\Entity
 */
class ControlSellante
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha_sellante", type="date", nullable=false)
     */
    private $fecha_sellante;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha_control", type="date", nullable=false)
     */
    private $fecha_control;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="creado", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $creado;

    /**
     * @var Ficha
     *
     * @ORM\ManyToOne(targetEntity="Ficha")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_ficha", referencedColumnName="id")
     * })
     */
    private $ficha;

    /**
     * @var Tratamiento
     *
     * @ORM\ManyToOne(targetEntity="Tratamiento")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_tratamiento", referencedColumnName="id")
     * })
     */
    private $tratamiento;

    /**
     * @var FichaCatalogo
     *
     * @ORM\ManyToOne(targetEntity="FichaCatalogo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_estado_sellante", referencedColumnName="id")
     * })
     */
    private $estado_sellante;

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return \DateTime
     */
    public function getFechaSellante(): ?\DateTime
    {
        return $this->fecha_sellante;
    }

    /**
     * @param \DateTime $fecha_sellante
     * @return ControlSellante
     */
    public function setFechaSellante(\DateTime $fecha_sellante): ControlSellante
    {
        $this->fecha_sellante = $fecha_sellante;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getFechaControl(): ?\DateTime
    {
        return $this->fecha_control;
    }

    /**
     * @param \DateTime $fecha_control
     * @return ControlSellante
     */
    public function setFechaControl(\DateTime $fecha_control): ControlSellante
    {
        $this->fecha_control = $fecha_control;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreado(): ?\DateTime
    {
        return $this->creado;
    }

    /**
     * @param \DateTime $creado
     * @return ControlSellante
     */
    public function setCreado(\DateTime $creado): ControlSellante
    {
        $this->creado = $creado;
        return $this;
    }

    /**
     * @return Ficha
     */
    public function getFicha(): ?Ficha
    {
        return $this->ficha;
    }

    /**
     * @param Ficha $ficha
     * @return ControlSellante
     */
    public function setFicha(Ficha $ficha): ControlSellante
    {
        $this->ficha = $ficha;
        return $this;
    }

    /**
     * @return Tratamiento
     */
    public function getTratamiento(): ?Tratamiento
    {
        return $this->tratamiento;
    }

    /**
     * @param Tratamiento $tratamiento
     * @return ControlSellante
     */
    public function setTratamiento(Tratamiento $tratamiento): ControlSellante
    {
        $this->tratamiento = $tratamiento;
        return $this;
    }

    /**
     * @return FichaCatalogo
     */
    public function getEstadoSellante(): ?FichaCatalogo
    {
        return $this->estado_sellante;
    }

    /**
     * @param FichaCatalogo $estado_sellante
     * @return ControlSellante
     */
    public function setEstadoSellante(FichaCatalogo $estado_sellante): ControlSellante
    {
        $this->estado_sellante = $estado_sellante;
        return $this;
    }


}
