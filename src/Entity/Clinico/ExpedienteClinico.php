<?php

namespace App\Entity\Clinico;

use Doctrine\ORM\Mapping as ORM;

/**
 * ExpedienteClinico
 *
 * @ORM\Table(name="odontoped_clinico.expediente_clinico", indexes={
 *     @ORM\Index(name="fk_expediente_clinico_persona", columns={"id_persona"}),
 *     @ORM\Index(name="fk_expediente_clinico_tipo", columns={"id_expediente_clinico_tipo"})
 * })
 * @ORM\Entity
 */
class ExpedienteClinico
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="codigo_expediente", type="string", length=25, nullable=false)
     */
    private $codigo_expediente;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="creado", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $creado;

    /**
     * @var \App\Entity\Administracion\Persona
     *
     * @ORM\ManyToOne(targetEntity="\App\Entity\Administracion\Persona")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_persona", referencedColumnName="id")
     * })
     */
    private $persona;

    /**
     * @var FichaCatalogo
     *
     * @ORM\ManyToOne(targetEntity="FichaCatalogo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_expediente_clinico_tipo", referencedColumnName="id")
     * })
     */
    private $expediente_clinico_tipo;

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return null|string
     */
    public function getCodigoExpediente(): ?string
    {
        return $this->codigo_expediente;
    }

    /**
     * @param string $codigo_expediente
     * @return ExpedienteClinico
     */
    public function setCodigoExpediente(string $codigo_expediente): ExpedienteClinico
    {
        $this->codigo_expediente = $codigo_expediente;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreado(): ?\DateTime
    {
        return $this->creado;
    }

    /**
     * @param \DateTime $creado
     * @return ExpedienteClinico
     */
    public function setCreado(\DateTime $creado): ExpedienteClinico
    {
        $this->creado = $creado;
        return $this;
    }

    /**
     * @return \App\Entity\Administracion\Persona
     */
    public function getPersona(): ?\App\Entity\Administracion\Persona
    {
        return $this->persona;
    }

    /**
     * @param \App\Entity\Administracion\Persona $persona
     * @return ExpedienteClinico
     */
    public function setPersona(\App\Entity\Administracion\Persona $persona): ExpedienteClinico
    {
        $this->persona = $persona;
        return $this;
    }

    /**
     * @return FichaCatalogo
     */
    public function getExpedienteClinicoTipo(): ?FichaCatalogo
    {
        return $this->expediente_clinico_tipo;
    }

    /**
     * @param FichaCatalogo $expediente_clinico_tipo
     * @return ExpedienteClinico
     */
    public function setExpedienteClinicoTipo(FichaCatalogo $expediente_clinico_tipo): ExpedienteClinico
    {
        $this->expediente_clinico_tipo = $expediente_clinico_tipo;
        return $this;
    }

}
