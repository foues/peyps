<?php

namespace App\Entity\Clinico;

use Doctrine\ORM\Mapping as ORM;

/**
 * FichaTratamiento
 *
 * @ORM\Table(name="odontoped_clinico.ficha_tratamiento", indexes={
 *     @ORM\Index(name="fk_ficha_tratamiento_evaluacion", columns={"id_evaluacion_dental"}),
 *     @ORM\Index(name="fk_ficha_evaluacion_cuenta", columns={"id_cuenta"})
 * })
 * @ORM\Entity
 */
class FichaTratamiento
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="acciones_realizar", type="string", length=500, nullable=false)
     */
    private $acciones_realizar;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="creado", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $creado;

    /**
     * @var EvaluacionDental
     *
     * @ORM\ManyToOne(targetEntity="EvaluacionDental")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_evaluacion_dental", referencedColumnName="id")
     * })
     */
    private $evaluacion_dental;

    /**
     * @var \App\Entity\Administracion\Cuenta
     *
     * @ORM\ManyToOne(targetEntity="\App\Entity\Administracion\Cuenta")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_cuenta", referencedColumnName="id")
     * })
     */
    private $cuenta;

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getAccionesRealizar(): ?string
    {
        return $this->acciones_realizar;
    }

    /**
     * @param string $acciones_realizar
     * @return FichaTratamiento
     */
    public function setAccionesRealizar(string $acciones_realizar): FichaTratamiento
    {
        $this->acciones_realizar = $acciones_realizar;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreado(): ?\DateTime
    {
        return $this->creado;
    }

    /**
     * @param \DateTime $creado
     * @return FichaTratamiento
     */
    public function setCreado(\DateTime $creado): FichaTratamiento
    {
        $this->creado = $creado;
        return $this;
    }

    /**
     * @return EvaluacionDental|null
     */
    public function getEvaluacionDental(): ?EvaluacionDental
    {
        return $this->evaluacion_dental;
    }

    /**
     * @param EvaluacionDental|null $evaluacion_dental
     * @return FichaTratamiento
     */
    public function setEvaluacionDental(?EvaluacionDental $evaluacion_dental): FichaTratamiento
    {
        $this->evaluacion_dental = $evaluacion_dental;
        return $this;
    }

    /**
     * @return \App\Entity\Administracion\Cuenta|null
     */
    public function getCuenta(): ?\App\Entity\Administracion\Cuenta
    {
        return $this->cuenta;
    }

    /**
     * @param \App\Entity\Administracion\Cuenta|null $cuenta
     * @return FichaTratamiento
     */
    public function setCuenta(?\App\Entity\Administracion\Cuenta $cuenta): FichaTratamiento
    {
        $this->cuenta = $cuenta;
        return $this;
    }

}
