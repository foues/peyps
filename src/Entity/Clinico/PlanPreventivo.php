<?php

namespace App\Entity\Clinico;

use Doctrine\ORM\Mapping as ORM;

/**
 * PlanPreventivo
 *
 * @ORM\Table(name="odontoped_clinico.plan_preventivo", indexes={
 *     @ORM\Index(name="fk_plan_preventivo_ficha", columns={"id_ficha"}),
 *     @ORM\Index(name="fk_plan_preventivo_tratamiento", columns={"id_tratamiento"}),
 *     @ORM\Index(name="fk_plan_preventivo_area", columns={"id_plan_preventivo_area"})
 * })
 * @ORM\Entity
 */
class PlanPreventivo
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="creado", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $creado;

    /**
     * @var Ficha
     *
     * @ORM\ManyToOne(targetEntity="Ficha")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_ficha", referencedColumnName="id")
     * })
     */
    private $ficha;

    /**
     * @var Tratamiento
     *
     * @ORM\ManyToOne(targetEntity="Tratamiento")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_tratamiento", referencedColumnName="id")
     * })
     */
    private $tratamiento;

    /**
     * @var FichaCatalogo
     *
     * @ORM\ManyToOne(targetEntity="FichaCatalogo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_plan_preventivo_area", referencedColumnName="id")
     * })
     */
    private $plan_preventivo_area;

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return \DateTime
     */
    public function getCreado(): ?\DateTime
    {
        return $this->creado;
    }

    /**
     * @param \DateTime $creado
     * @return PlanPreventivo
     */
    public function setCreado(\DateTime $creado): PlanPreventivo
    {
        $this->creado = $creado;
        return $this;
    }

    /**
     * @return Ficha
     */
    public function getFicha(): ?Ficha
    {
        return $this->ficha;
    }

    /**
     * @param Ficha $ficha
     * @return PlanPreventivo
     */
    public function setFicha(Ficha $ficha): PlanPreventivo
    {
        $this->ficha = $ficha;
        return $this;
    }

    /**
     * @return Tratamiento
     */
    public function getTratamiento(): ?Tratamiento
    {
        return $this->tratamiento;
    }

    /**
     * @param Tratamiento $tratamiento
     * @return PlanPreventivo
     */
    public function setTratamiento(Tratamiento $tratamiento): PlanPreventivo
    {
        $this->tratamiento = $tratamiento;
        return $this;
    }

    /**
     * @return FichaCatalogo
     */
    public function getPlanPreventivoArea(): ?FichaCatalogo
    {
        return $this->plan_preventivo_area;
    }

    /**
     * @param FichaCatalogo $plan_preventivo_area
     * @return PlanPreventivo
     */
    public function setPlanPreventivoArea(FichaCatalogo $plan_preventivo_area): PlanPreventivo
    {
        $this->plan_preventivo_area = $plan_preventivo_area;
        return $this;
    }


}
