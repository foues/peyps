<?php

namespace App\Entity\Clinico;

use Doctrine\ORM\Mapping as ORM;

/**
 * RiesgoDetalle
 *
 * @ORM\Table(name="odontoped_clinico.riesgo_detalle", indexes={
 *     @ORM\Index(name="fk_ficha_riesgo", columns={"id_ficha"})
 * })
 * @ORM\Entity
 */
class RiesgoDetalle
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="creado", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $creado;

    /**
     * @var Ficha
     *
     * @ORM\ManyToOne(targetEntity="Ficha")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_ficha", referencedColumnName="id")
     * })
     */
    private $ficha;

    /**
     * @var float
     *
     * @ORM\Column(name="total_ingesta_azucares", type="float", precision=10, scale=0)
     */
    private $total_ingesta_azucares;

    /**
     * @var float
     *
     * @ORM\Column(name="total_placa_bacteriana", type="float", precision=10, scale=0)
     */
    private $total_placa_bacteriana;

    /**
     * @var float
     *
     * @ORM\Column(name="total_indice_cpo_ceo", type="float", precision=10, scale=0)
     */
    private $total_indice_cpo_ceo;

    /**
     * @var string
     *
     * @ORM\Column(name="observaciones", type="string", length=999, nullable=false)
     */
    private $observaciones;

    /**
     * @var string
     *
     * @ORM\Column(name="comentarios", type="string", length=999, nullable=false)
     */
    private $comentarios;

    /**
     * @var string
     *
     * @ORM\Column(name="otras_condiciones", type="string", length=999)
     */
    private $otras_condiciones;

    /**
     * @var int
     *
     * @ORM\Column(name="placa_1655", type="integer", nullable=false)
     */
    private $placa_1655;

    /**
     * @var int
     *
     * @ORM\Column(name="placa_1151", type="integer", nullable=false)
     */
    private $placa_1151;

    /**
     * @var int
     *
     * @ORM\Column(name="placa_2665", type="integer", nullable=false)
     */
    private $placa_2665;

    /**
     * @var int
     *
     * @ORM\Column(name="placa_3675", type="integer", nullable=false)
     */
    private $placa_3675;

    /**
     * @var int
     *
     * @ORM\Column(name="placa_3171", type="integer", nullable=false)
     */
    private $placa_3171;

    /**
     * @var int
     *
     * @ORM\Column(name="placa_4685", type="integer", nullable=false)
     */
    private $placa_4685;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return \DateTimeInterface|null
     */
    public function getCreado(): ?\DateTimeInterface
    {
        return $this->creado;
    }

    /**
     * @param \DateTimeInterface $creado
     * @return RiesgoDetalle
     */
    public function setCreado(\DateTimeInterface $creado): self
    {
        $this->creado = $creado;
        return $this;
    }

    /**
     * @return Ficha|null
     */
    public function getFicha(): ?Ficha
    {
        return $this->ficha;
    }

    /**
     * @param Ficha|null $ficha
     * @return RiesgoDetalle
     */
    public function setFicha(?Ficha $ficha): self
    {
        $this->ficha = $ficha;
        return $this;
    }

    /**
     * @return float|null
     */
    public function getTotalIngestaAzucares(): ?float
    {
        return $this->total_ingesta_azucares;
    }

    /**
     * @param float $total_ingesta_azucares
     * @return RiesgoDetalle
     */
    public function setTotalIngestaAzucares(float $total_ingesta_azucares): self
    {
        $this->total_ingesta_azucares = $total_ingesta_azucares;
        return $this;
    }

    /**
     * @return float|null
     */
    public function getTotalPlacaBacteriana(): ?float
    {
        return $this->total_placa_bacteriana;
    }

    /**
     * @param float $total_placa_bacteriana
     * @return RiesgoDetalle
     */
    public function setTotalPlacaBacteriana(float $total_placa_bacteriana): self
    {
        $this->total_placa_bacteriana = $total_placa_bacteriana;
        return $this;
    }

    /**
     * @return float|null
     */
    public function getTotalIndiceCpoCeo(): ?float
    {
        return $this->total_indice_cpo_ceo;
    }

    /**
     * @param float $total_indice_cpo_ceo
     * @return RiesgoDetalle
     */
    public function setTotalIndiceCpoCeo(float $total_indice_cpo_ceo): self
    {
        $this->total_indice_cpo_ceo = $total_indice_cpo_ceo;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getObservaciones(): ?string
    {
        return $this->observaciones;
    }

    /**
     * @param null|string $observaciones
     * @return RiesgoDetalle
     */
    public function setObservaciones(?string $observaciones): self
    {
        $this->observaciones = $observaciones;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getComentarios(): ?string
    {
        return $this->comentarios;
    }

    /**
     * @param null|string $comentarios
     * @return RiesgoDetalle
     */
    public function setComentarios(?string $comentarios): self
    {
        $this->comentarios = $comentarios;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getOtrasCondiciones(): ?string
    {
        return $this->otras_condiciones;
    }

    /**
     * @param null|string $otras_condiciones
     * @return RiesgoDetalle
     */
    public function setOtrasCondiciones(?string $otras_condiciones): self
    {
        $this->otras_condiciones = $otras_condiciones;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getPlaca1655(): ?int
    {
        return $this->placa_1655;
    }

    /**
     * @param int $placa_1655
     * @return RiesgoDetalle
     */
    public function setPlaca1655(int $placa_1655): self
    {
        $this->placa_1655 = $placa_1655;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getPlaca1151(): ?int
    {
        return $this->placa_1151;
    }

    /**
     * @param int $placa_1151
     * @return RiesgoDetalle
     */
    public function setPlaca1151(int $placa_1151): self
    {
        $this->placa_1151 = $placa_1151;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getPlaca2665(): ?int
    {
        return $this->placa_2665;
    }

    /**
     * @param int $placa_2665
     * @return RiesgoDetalle
     */
    public function setPlaca2665(int $placa_2665): self
    {
        $this->placa_2665 = $placa_2665;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getPlaca3675(): ?int
    {
        return $this->placa_3675;
    }

    /**
     * @param int $placa_3675
     * @return RiesgoDetalle
     */
    public function setPlaca3675(int $placa_3675): self
    {
        $this->placa_3675 = $placa_3675;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getPlaca3171(): ?int
    {
        return $this->placa_3171;
    }

    /**
     * @param int $placa_3171
     * @return RiesgoDetalle
     */
    public function setPlaca3171(int $placa_3171): self
    {
        $this->placa_3171 = $placa_3171;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getPlaca4685(): ?int
    {
        return $this->placa_4685;
    }

    /**
     * @param int $placa_4685
     * @return RiesgoDetalle
     */
    public function setPlaca4685(int $placa_4685): self
    {
        $this->placa_4685 = $placa_4685;
        return $this;
    }

}
