<?php

namespace App\Entity\Clinico;

use Doctrine\ORM\Mapping as ORM;

/**
 * FichaAtributo
 *
 * @ORM\Table(name="odontoped_clinico.ficha_atributo", indexes={
 *     @ORM\Index(name="fk_ficha_atributo", columns={"id_ficha"}),
 *     @ORM\Index(name="fk_atributo_ficha", columns={"id_atributo"})
 * })
 * @ORM\Entity(repositoryClass="App\Repository\Clinico\FichaAtributoRepository")
 */
class FichaAtributo
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="creado", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $creado;

    /**
     * @var Ficha
     *
     * @ORM\ManyToOne(targetEntity="Ficha")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_ficha", referencedColumnName="id")
     * })
     */
    private $ficha;

    /**
     * @var FichaCatalogo
     *
     * @ORM\ManyToOne(targetEntity="FichaCatalogo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_atributo", referencedColumnName="id")
     * })
     */
    private $atributo;

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return \DateTimeInterface|null
     */
    public function getCreado(): ?\DateTimeInterface
    {
        return $this->creado;
    }

    /**
     * @param \DateTimeInterface $creado
     * @return FichaAtributo
     */
    public function setCreado(\DateTimeInterface $creado): FichaAtributo
    {
        $this->creado = $creado;
        return $this;
    }

    /**
     * @return Ficha
     */
    public function getFicha(): ?Ficha
    {
        return $this->ficha;
    }

    /**
     * @param Ficha $ficha
     * @return FichaAtributo
     */
    public function setFicha(Ficha $ficha): FichaAtributo
    {
        $this->ficha = $ficha;
        return $this;
    }

    /**
     * @return FichaCatalogo
     */
    public function getAtributo(): ?FichaCatalogo
    {
        return $this->atributo;
    }

    /**
     * @param FichaCatalogo $atributo
     * @return FichaAtributo
     */
    public function setAtributo(FichaCatalogo $atributo): FichaAtributo
    {
        $this->atributo = $atributo;
        return $this;
    }


}
