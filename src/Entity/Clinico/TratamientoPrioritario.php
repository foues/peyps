<?php

namespace App\Entity\Clinico;

use Doctrine\ORM\Mapping as ORM;

/**
 * TratamientoPrioritario
 *
 * @ORM\Table(name="odontoped_clinico.tratamiento_prioritario", indexes={
 *     @ORM\Index(name="fk_tratamiento_prioritario_ficha", columns={"id_ficha"}),
 *     @ORM\Index(name="fk_tratamiento_prioritario", columns={"id_tratamiento"})
 * })
 * @ORM\Entity
 */
class TratamientoPrioritario
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="creado", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $creado;

    /**
     * @var Ficha
     *
     * @ORM\ManyToOne(targetEntity="Ficha")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_ficha", referencedColumnName="id")
     * })
     */
    private $ficha;

    /**
     * @var Tratamiento
     *
     * @ORM\ManyToOne(targetEntity="Tratamiento")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_tratamiento", referencedColumnName="id")
     * })
     */
    private $tratamiento;

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return \DateTime
     */
    public function getCreado(): ?\DateTime
    {
        return $this->creado;
    }

    /**
     * @param \DateTime $creado
     * @return TratamientoPrioritario
     */
    public function setCreado(\DateTime $creado): TratamientoPrioritario
    {
        $this->creado = $creado;
        return $this;
    }

    /**
     * @return Ficha
     */
    public function getFicha(): ?Ficha
    {
        return $this->ficha;
    }

    /**
     * @param Ficha $ficha
     * @return TratamientoPrioritario
     */
    public function setFicha(Ficha $ficha): TratamientoPrioritario
    {
        $this->ficha = $ficha;
        return $this;
    }

    /**
     * @return Tratamiento
     */
    public function getTratamiento(): ?Tratamiento
    {
        return $this->tratamiento;
    }

    /**
     * @param Tratamiento $tratamiento
     * @return TratamientoPrioritario
     */
    public function setTratamiento(Tratamiento $tratamiento): TratamientoPrioritario
    {
        $this->tratamiento = $tratamiento;
        return $this;
    }


}
