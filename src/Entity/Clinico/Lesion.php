<?php

namespace App\Entity\Clinico;

use Doctrine\ORM\Mapping as ORM;

/**
 * Lesion
 *
 * @ORM\Table(name="odontoped_clinico.lesion", indexes={
 *     @ORM\Index(name="fk_lesion_ficha", columns={"id_ficha"}),
 *     @ORM\Index(name="fk_lesion_valor_tipo", columns={"id_lesion_valor_tipo"})
 * })
 * @ORM\Entity
 */
class Lesion
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="pieza_dental", type="string", length=10, nullable=false)
     */
    private $pieza_dental;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="creado", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $creado;

    /**
     * @var Ficha
     *
     * @ORM\ManyToOne(targetEntity="Ficha")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_ficha", referencedColumnName="id")
     * })
     */
    private $ficha;

    /**
     * @var FichaCatalogo
     *
     * @ORM\ManyToOne(targetEntity="FichaCatalogo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_lesion_valor_tipo", referencedColumnName="id")
     * })
     */
    private $lesion_valor_tipo;

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getPiezaDental(): ?string
    {
        return $this->pieza_dental;
    }

    /**
     * @param string $pieza_dental
     * @return Lesion
     */
    public function setPiezaDental(string $pieza_dental): Lesion
    {
        $this->pieza_dental = $pieza_dental;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreado(): ?\DateTime
    {
        return $this->creado;
    }

    /**
     * @param \DateTime $creado
     * @return Lesion
     */
    public function setCreado(\DateTime $creado): Lesion
    {
        $this->creado = $creado;
        return $this;
    }

    /**
     * @return Ficha
     */
    public function getFicha(): ?Ficha
    {
        return $this->ficha;
    }

    /**
     * @param Ficha $ficha
     * @return Lesion
     */
    public function setFicha(Ficha $ficha): Lesion
    {
        $this->ficha = $ficha;
        return $this;
    }

    /**
     * @return FichaCatalogo
     */
    public function getLesionValorTipo(): ?FichaCatalogo
    {
        return $this->lesion_valor_tipo;
    }

    /**
     * @param FichaCatalogo $lesion_valor_tipo
     * @return Lesion
     */
    public function setLesionValorTipo(FichaCatalogo $lesion_valor_tipo): Lesion
    {
        $this->lesion_valor_tipo = $lesion_valor_tipo;
        return $this;
    }


}
