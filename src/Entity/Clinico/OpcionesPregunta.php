<?php

namespace App\Entity\Clinico;

use Doctrine\ORM\Mapping as ORM;

/**
 * OpcionesPregunta
 *
 * @ORM\Table(name="odontoped_clinico.opciones_pregunta", indexes={
 *     @ORM\Index(name="pregunta_fk", columns={"pregunta_fk"})
 * })
 * @ORM\Entity
 */
class OpcionesPregunta {

  /**
   * @var int
   *
   * @ORM\Column(name="id", type="integer", nullable=false)
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="IDENTITY")
   */
private $id;

/**
 * @var string
 *
 * @ORM\Column(name="nombre_opcion", type="string", length=500, nullable=false)
 */
private $nombre_opcion;

/**
 * @var Pregunta
 *
 * @ORM\ManyToOne(targetEntity="Pregunta")
 * @ORM\JoinColumns({
 *   @ORM\JoinColumn(name="pregunta_fk", referencedColumnName="id")
 * })
 */
private $pregunta;

/**
 * @ORM\Column(type="integer", options={"default":"0"})
 */
private $pregunta_compleja;

/**
 * @var string|null
 *
 * @ORM\Column(name="descripcion", type="string", length=250, nullable=true)
 */
private $descripcion;

public function getId(): ?int
{
    return $this->id;
}

public function getNombreOpcion(): ?string
{
    return $this->nombre_opcion;
}

public function setNombreOpcion(string $nombre_opcion): self
{
    $this->nombre_opcion = $nombre_opcion;

    return $this;
}

public function getPregunta(): ?Pregunta
{
    return $this->pregunta;
}

public function setPregunta(?Pregunta $pregunta): self
{
    $this->pregunta = $pregunta;

    return $this;
}

/**
 * @return null|string
 */
public function getDescripcion(): ?string
{
    return $this->descripcion;
}

/**
 * @param null|string $descripcion
 * @return string
 */

public function setDescripcion(?string $descripcion): ?string
{
    $this->descripcion = $descripcion;
    return $this;
}


public function getPreguntaCompleja()
{
    return $this->pregunta_compleja;
}




}
