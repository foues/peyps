<?php

namespace App\Entity\Clinico;

use Doctrine\ORM\Mapping as ORM;

/**
 * Tratamiento
 *
 * @ORM\Table(name="odontoped_clinico.tratamiento", indexes={
 *     @ORM\Index(name="fk_tratamiento_area", columns={"id_tratamiento_area"})
 * })
 * @ORM\Entity(repositoryClass="App\Repository\Clinico\TratamientoRepository")
 */
class Tratamiento
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="codigo", type="string", length=50, nullable=false)
     */
    private $codigo;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=255, nullable=false)
     */
    private $nombre;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="string", length=500, nullable=true)
     */
    private $descripcion;

    /**
     * @var string
     *
     * @ORM\Column(name="precio", type="decimal", precision=8, scale=2, nullable=false)
     */
    private $precio;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="creado", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $creado;

    /**
     * @var FichaCatalogo
     *
     * @ORM\ManyToOne(targetEntity="FichaCatalogo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_tratamiento_area", referencedColumnName="id")
     * })
     */
    private $tratamiento_area;

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getCodigo(): ?string
    {
        return $this->codigo;
    }

    /**
     * @param string $codigo
     * @return Tratamiento
     */
    public function setCodigo(string $codigo): Tratamiento
    {
        $this->codigo = $codigo;
        return $this;
    }

    /**
     * @return string
     */
    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    /**
     * @param string $nombre
     * @return Tratamiento
     */
    public function setNombre(string $nombre): Tratamiento
    {
        $this->nombre = $nombre;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getDescripcion(): ?string
    {
        return $this->descripcion;
    }

    /**
     * @param string $descripcion
     * @return Tratamiento
     */
    public function setDescripcion(?string $descripcion): Tratamiento
    {
        $this->descripcion = $descripcion;
        return $this;
    }

    /**
     * @return string
     */
    public function getPrecio(): ?string
    {
        return $this->precio;
    }

    /**
     * @param string $precio
     * @return Tratamiento
     */
    public function setPrecio(string $precio): Tratamiento
    {
        $this->precio = $precio;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreado(): ?\DateTime
    {
        return $this->creado;
    }

    /**
     * @param \DateTime $creado
     * @return Tratamiento
     */
    public function setCreado(\DateTime $creado): Tratamiento
    {
        $this->creado = $creado;
        return $this;
    }

    /**
     * @return FichaCatalogo
     */
    public function getTratamientoArea(): ?FichaCatalogo
    {
        return $this->tratamiento_area;
    }

    /**
     * @param FichaCatalogo $tratamiento_area
     * @return Tratamiento
     */
    public function setTratamientoArea(FichaCatalogo $tratamiento_area): Tratamiento
    {
        $this->tratamiento_area = $tratamiento_area;
        return $this;
    }

    /**
     * @return string
     */
    public function __toString(): ?string
    {
        return (string)$this->nombre;
    }


}
