<?php

namespace App\Entity\Clinico;

use Doctrine\ORM\Mapping as ORM;

/**
 * Seguimiento Control
 *
 * @ORM\Table(name="odontoped_clinico.seguimiento_control", indexes={
 * @ORM\Index(name="ficha_fk", columns={"ficha_fk"}),
 * })
 * @ORM\Entity
 */
class SeguimientoConductual
{
  /**
   * @var int
   *
   * @ORM\Column(name="id", type="integer", nullable=false)
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="IDENTITY")
   */
private $id;


/**
 * @var date
 *
 * @ORM\Column(name="fecha", type="date")
 */
private $fecha;


/**
 * @var string
 *
 * @ORM\Column(name="procedimiento_realizado", type="string", length=500, nullable=false)
 */
private $procedimientoRealizado;


/**
 * @var string
 *
 * @ORM\Column(name="comportamiento_demostrado", type="string", length=500, nullable=false)
 */
private $comportamientoDemostrado;


/**
 * @var string
 *
 * @ORM\Column(name="clave", type="string", length=100, nullable=false)
 */
private $clave;

/**
 * @var ficha_fk
 *
 * @ORM\ManyToOne(targetEntity="Ficha")
 * @ORM\JoinColumns({
 *   @ORM\JoinColumn(name="ficha_fk", referencedColumnName="id")
 * })
 */
private $ficha_fk;


/**
 * @return int|null
 */
public function getId(): ?int
{
    return $this->id;
}

public function setId($id)
{
    $this->id = $id;
    return $this;
}

public function getProcedimientoRealizado(): ?string
{
    return $this->procedimientoRealizado;
}

/**
 * @param string $procedimiento
 */
public function setProcedimientoRealizado(string $procedimiento)
{
    $this->procedimientoRealizado = $procedimiento;
    return $this;
}


public function getComportamientoDemostrado(): ?string
{
    return $this->comportamientoDemostrado;
}

/**
 * @param string $comportamiento
 */
public function setComportamientoDemostrado(string $comportamiento)
{
    $this->comportamientoDemostrado = $comportamiento;
    return $this;
}

public function getClave(): ?string
{
    return $this->clave;
}

/**
 * @param string $clave
 */
public function setClave(string $clave)
{
    $this->clave = $clave;
    return $this;
}


/**
 * @param \DateTime $fecha
 */
public function setFecha(\DateTime $fecha)
{
    $this->fecha = $fecha;
    return $this;
}

/**
 * @return \DateTime
 */
public function getFecha(): ?\DateTime
{
    return $this->fecha;
}

public function getFicha(): ?Ficha
{
    return $this->ficha_fk;
}

public function setFicha(?Ficha $ficha): self
{
    $this->ficha_fk = $ficha;
    return $this;
}

}
