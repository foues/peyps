<?php

namespace App\Entity\Almacen;

use Doctrine\ORM\Mapping as ORM;

/**
 * Movimiento
 *
 * @ORM\Table(name="almacen.movimiento", indexes={
 *     @ORM\Index(name="fk_movimiento_almacen", columns={"id_almacen"}),
 *     @ORM\Index(name="fk_movimiento_articulo_presentacion", columns={"id_articulo_presentacion"}),
 *     @ORM\Index(name="fk_movimiento_usuario", columns={"id_usuario"})
 * })
 * @ORM\Entity
 */
class Movimiento
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="concepto", type="string", length=255, nullable=false)
     */
    private $concepto;

    /**
     * @var string
     *
     * @ORM\Column(name="precio", type="float", precision=10, scale=0, nullable=false)
     */
    private $precio;

    /**
     * @var int
     *
     * @ORM\Column(name="cantidad", type="integer", nullable=false)
     */
    private $cantidad;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha", type="date", nullable=false)
     */
    private $fecha;

    /**
     * @var bool
     *
     * @ORM\Column(name="confirmado", type="boolean", nullable=false)
     */
    private $confirmado = false;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="creado", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $creado;

    /**
     * @var Almacen
     *
     * @ORM\ManyToOne(targetEntity="Almacen")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_almacen", referencedColumnName="id")
     * })
     */
    private $almacen;

    /**
     * @var ArticuloPresentacion
     *
     * @ORM\ManyToOne(targetEntity="ArticuloPresentacion")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_articulo_presentacion", referencedColumnName="id")
     * })
     */
    private $articulo_presentacion;

    /**
     * @var \App\Entity\Administracion\Usuario
     *
     * @ORM\ManyToOne(targetEntity="\App\Entity\Administracion\Usuario")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_usuario", referencedColumnName="id")
     * })
     */
    private $usuario;

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getConcepto(): ?string
    {
        return $this->concepto;
    }

    /**
     * @param string $concepto
     * @return Movimiento
     */
    public function setConcepto(string $concepto): Movimiento
    {
        $this->concepto = $concepto;
        return $this;
    }

    /**
     * @return string
     */
    public function getPrecio(): ?string
    {
        return $this->precio;
    }

    /**
     * @param string $precio
     * @return Movimiento
     */
    public function setPrecio(string $precio): Movimiento
    {
        $this->precio = $precio;
        return $this;
    }

    /**
     * @return int
     */
    public function getCantidad(): ?int
    {
        return $this->cantidad;
    }

    /**
     * @param int $cantidad
     * @return Movimiento
     */
    public function setCantidad(int $cantidad): Movimiento
    {
        $this->cantidad = $cantidad;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getFecha(): ?\DateTime
    {
        return $this->fecha;
    }

    /**
     * @param \DateTime $fecha
     * @return Movimiento
     */
    public function setFecha(\DateTime $fecha): Movimiento
    {
        $this->fecha = $fecha;
        return $this;
    }

    /**
     * @return bool
     */
    public function isConfirmado(): ?bool
    {
        return $this->confirmado;
    }

    /**
     * @param bool $confirmado
     * @return Movimiento
     */
    public function setConfirmado(bool $confirmado): Movimiento
    {
        $this->confirmado = $confirmado;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreado(): ?\DateTime
    {
        return $this->creado;
    }

    /**
     * @param \DateTime $creado
     * @return Movimiento
     */
    public function setCreado(\DateTime $creado): Movimiento
    {
        $this->creado = $creado;
        return $this;
    }

    /**
     * @return Almacen
     */
    public function getAlmacen(): ?Almacen
    {
        return $this->almacen;
    }

    /**
     * @param Almacen $almacen
     * @return Movimiento
     */
    public function setAlmacen(Almacen $almacen): Movimiento
    {
        $this->almacen = $almacen;
        return $this;
    }

    /**
     * @return ArticuloPresentacion
     */
    public function getArticuloPresentacion(): ?ArticuloPresentacion
    {
        return $this->articulo_presentacion;
    }

    /**
     * @param ArticuloPresentacion $articulo_presentacion
     * @return Movimiento
     */
    public function setArticuloPresentacion(ArticuloPresentacion $articulo_presentacion): Movimiento
    {
        $this->articulo_presentacion = $articulo_presentacion;
        return $this;
    }

    /**
     * @return \App\Entity\Administracion\Usuario
     */
    public function getUsuario(): ?\App\Entity\Administracion\Usuario
    {
        return $this->usuario;
    }

    /**
     * @param \App\Entity\Administracion\Usuario $usuario
     * @return Movimiento
     */
    public function setUsuario(\App\Entity\Administracion\Usuario $usuario): Movimiento
    {
        $this->usuario = $usuario;
        return $this;
    }


}
