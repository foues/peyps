<?php

namespace App\Entity\Almacen;

use Doctrine\ORM\Mapping as ORM;

/**
 * ValeEvaluacionDental
 *
 * @ORM\Table(name="almacen.vale_evaluacion_dental", uniqueConstraints={
 *     @ORM\UniqueConstraint(name="unique_vale_evaluacion_dental", columns={"id_vale", "id_evaluacion_dental"})
 * }, indexes={
 *     @ORM\Index(name="fk_vale_ficha_tratamiento", columns={"id_vale"}),
 *     @ORM\Index(name="fk_evaluacion_dental_vale", columns={"id_evaluacion_dental"})
 * })
 * @ORM\Entity
 */
class ValeEvaluacionDental
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="creado", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $creado;

    /**
     * @var Vale
     *
     * @ORM\ManyToOne(targetEntity="Vale")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_vale", referencedColumnName="id")
     * })
     */
    private $vale;

    /**
     * @var \App\Entity\Clinico\EvaluacionDental
     *
     * @ORM\ManyToOne(targetEntity="\App\Entity\Clinico\EvaluacionDental")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_evaluacion_dental", referencedColumnName="id")
     * })
     */
    private $evaluacion_dental;

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return \DateTime
     */
    public function getCreado(): ?\DateTime
    {
        return $this->creado;
    }

    /**
     * @param \DateTime $creado
     * @return ValeEvaluacionDental
     */
    public function setCreado(\DateTime $creado): ValeEvaluacionDental
    {
        $this->creado = $creado;
        return $this;
    }

    /**
     * @return Vale
     */
    public function getVale(): ?Vale
    {
        return $this->vale;
    }

    /**
     * @param Vale $vale
     * @return ValeEvaluacionDental
     */
    public function setVale(Vale $vale): ValeEvaluacionDental
    {
        $this->vale = $vale;
        return $this;
    }

    /**
     * @return \App\Entity\Clinico\EvaluacionDental
     */
    public function getEvaluacionDental(): ?\App\Entity\Clinico\EvaluacionDental
    {
        return $this->evaluacion_dental;
    }

    /**
     * @param \App\Entity\Clinico\EvaluacionDental $evaluacion_dental
     * @return ValeEvaluacionDental
     */
    public function setEvaluacionDental(\App\Entity\Clinico\EvaluacionDental $evaluacion_dental): ValeEvaluacionDental
    {
        $this->evaluacion_dental = $evaluacion_dental;
        return $this;
    }

}
