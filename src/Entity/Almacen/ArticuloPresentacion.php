<?php

namespace App\Entity\Almacen;

use Doctrine\ORM\Mapping as ORM;

/**
 * ArticuloPresentacion
 *
 * @ORM\Table(name="almacen.articulo_presentacion", indexes={
 *     @ORM\Index(name="fk_articulo", columns={"id_articulo"}),
 *     @ORM\Index(name="fk_unidad_medida", columns={"id_unidad_medida"}),
 *     @ORM\Index(name="fk_sub_unidad_medida", columns={"id_subunidad_medida"})
 * })
 * @ORM\Entity(repositoryClass="App\Repository\Almacen\ArticuloPresentacionRepository")
 */
class ArticuloPresentacion
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="cantidad", type="integer", nullable=false)
     */
    private $cantidad;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="creado", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $creado;

    /**
     * @var Articulo
     *
     * @ORM\ManyToOne(targetEntity="Articulo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_articulo", referencedColumnName="id")
     * })
     */
    private $articulo;

    /**
     * @var UnidadMedida
     *
     * @ORM\ManyToOne(targetEntity="UnidadMedida")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_unidad_medida", referencedColumnName="id")
     * })
     */
    private $unidad_medida;

    /**
     * @var UnidadMedida
     *
     * @ORM\ManyToOne(targetEntity="UnidadMedida")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_subunidad_medida", referencedColumnName="id")
     * })
     */
    private $subunidad_medida;

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getCantidad(): ?int
    {
        return $this->cantidad;
    }

    /**
     * @param int $cantidad
     * @return ArticuloPresentacion
     */
    public function setCantidad(int $cantidad): ArticuloPresentacion
    {
        $this->cantidad = $cantidad;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreado(): ?\DateTime
    {
        return $this->creado;
    }

    /**
     * @param \DateTime $creado
     * @return ArticuloPresentacion
     */
    public function setCreado(\DateTime $creado): ArticuloPresentacion
    {
        $this->creado = $creado;
        return $this;
    }

    /**
     * @return Articulo
     */
    public function getArticulo(): ?Articulo
    {
        return $this->articulo;
    }

    /**
     * @param Articulo $articulo
     * @return ArticuloPresentacion
     */
    public function setArticulo(Articulo $articulo): ArticuloPresentacion
    {
        $this->articulo = $articulo;
        return $this;
    }

    /**
     * @return UnidadMedida
     */
    public function getUnidadMedida(): ?UnidadMedida
    {
        return $this->unidad_medida;
    }

    /**
     * @param UnidadMedida $unidad_medida
     * @return ArticuloPresentacion
     */
    public function setUnidadMedida(UnidadMedida $unidad_medida): ArticuloPresentacion
    {
        $this->unidad_medida = $unidad_medida;
        return $this;
    }

    /**
     * @return UnidadMedida
     */
    public function getSubunidadMedida(): ?UnidadMedida
    {
        return $this->subunidad_medida;
    }

    /**
     * @param UnidadMedida $subunidad_medida
     * @return ArticuloPresentacion
     */
    public function setSubunidadMedida(UnidadMedida $subunidad_medida): ArticuloPresentacion
    {
        $this->subunidad_medida = $subunidad_medida;
        return $this;
    }

    /**
     *
     */
    public function __toString()
    {
        return $this->articulo->getNombre() . ' (' . $this->subunidad_medida . ')';
    }


}
