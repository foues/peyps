<?php

namespace App\Entity\Almacen;

use Doctrine\ORM\Mapping as ORM;

/**
 * TratamientoArticuloPresentacion
 *
 * @ORM\Table(name="almacen.tratamiento_articulo_presentacion", uniqueConstraints={
 *     @ORM\UniqueConstraint(name="unique_tratamiento_articulo_presentacion", columns={"id_tratamiento", "id_articulo_presentacion"})
 * }, indexes={
 *     @ORM\Index(name="fk_articulo_presentacion_tratamiento", columns={"id_articulo_presentacion"}),
 *     @ORM\Index(name="fk_tratamiento_articulo_presentacion", columns={"id_tratamiento"})
 * })
 * @ORM\Entity(repositoryClass="App\Repository\Almacen\TratamientoArticuloPresentacionRepository")
 */
class TratamientoArticuloPresentacion
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="cantidad", type="integer", nullable=false)
     */
    private $cantidad;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="creado", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $creado;

    /**
     * @var \App\Entity\Clinico\Tratamiento
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Clinico\Tratamiento")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_tratamiento", referencedColumnName="id")
     * })
     */
    private $tratamiento;

    /**
     * @var ArticuloPresentacion
     *
     * @ORM\ManyToOne(targetEntity="ArticuloPresentacion")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_articulo_presentacion", referencedColumnName="id")
     * })
     */
    private $articulo_presentacion;

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getCantidad(): ?int
    {
        return $this->cantidad;
    }

    /**
     * @param int $cantidad
     * @return TratamientoArticuloPresentacion
     */
    public function setCantidad(int $cantidad): TratamientoArticuloPresentacion
    {
        $this->cantidad = $cantidad;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreado(): ?\DateTime
    {
        return $this->creado;
    }

    /**
     * @param \DateTime $creado
     * @return TratamientoArticuloPresentacion
     */
    public function setCreado(\DateTime $creado): TratamientoArticuloPresentacion
    {
        $this->creado = $creado;
        return $this;
    }

    /**
     * @return \App\Entity\Clinico\Tratamiento
     */
    public function getTratamiento(): ?\App\Entity\Clinico\Tratamiento
    {
        return $this->tratamiento;
    }

    /**
     * @param \App\Entity\Clinico\Tratamiento $tratamiento
     * @return TratamientoArticuloPresentacion
     */
    public function setTratamiento(\App\Entity\Clinico\Tratamiento $tratamiento): TratamientoArticuloPresentacion
    {
        $this->tratamiento = $tratamiento;
        return $this;
    }

    /**
     * @return ArticuloPresentacion
     */
    public function getArticuloPresentacion(): ?ArticuloPresentacion
    {
        return $this->articulo_presentacion;
    }

    /**
     * @param ArticuloPresentacion $articulo_presentacion
     * @return TratamientoArticuloPresentacion
     */
    public function setArticuloPresentacion(ArticuloPresentacion $articulo_presentacion): TratamientoArticuloPresentacion
    {
        $this->articulo_presentacion = $articulo_presentacion;
        return $this;
    }

}
