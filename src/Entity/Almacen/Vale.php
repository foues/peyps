<?php

namespace App\Entity\Almacen;

use Doctrine\ORM\Mapping as ORM;

/**
 * Vale
 *
 * @ORM\Table(name="almacen.vale", indexes={
 *     @ORM\Index(name="fk_vale_estado", columns={"id_vale_estado"}),
 *     @ORM\Index(name="fk_vale_usuario", columns={"id_usuario"})
 * })
 * @ORM\Entity
 */
class Vale
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="creado", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $creado;

    /**
     * @var string|null
     *
     * @ORM\Column(name="descripcion", type="string", length=500, nullable=true)
     */
    private $descripcion;

    /**
     * @var array|string
     *
     * @ORM\Column(name="datos", type="json", nullable=true)
     */
    private $datos;

    /**
     * @var \App\Entity\Almacen\ValeEstado
     *
     * @ORM\ManyToOne(targetEntity="ValeEstado")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_vale_estado", referencedColumnName="id")
     * })
     */
    private $vale_estado;

    /**
     * @var \App\Entity\Administracion\Usuario
     *
     * @ORM\ManyToOne(targetEntity="\App\Entity\Administracion\Usuario")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_usuario", referencedColumnName="id")
     * })
     */
    private $usuario;

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return null|string
     */
    public function getDescripcion(): ?string
    {
        return $this->descripcion;
    }

    /**
     * @param null|string $descripcion
     * @return Vale
     */
    public function setDescripcion(?string $descripcion): Vale
    {
        $this->descripcion = $descripcion;
        return $this;
    }

    /**
     * @return array|string
     */
    public function getDatos()
    {
        return $this->datos;
    }

    /**
     * @return null|string
     */
    public function getDatosAsString()
    {
        if (is_array($this->datos))
            return implode(', ', $this->datos);
        return null;
    }

    /**
     * @param array|string $datos
     * @return Vale
     */
    public function setDatos($datos)
    {
        $this->datos = $datos;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreado(): ?\DateTime
    {
        return $this->creado;
    }

    /**
     * @param \DateTime $creado
     * @return Vale
     */
    public function setCreado(\DateTime $creado): Vale
    {
        $this->creado = $creado;
        return $this;
    }

    /**
     * @return \App\Entity\Almacen\ValeEstado
     */
    public function getValeEstado(): ?\App\Entity\Almacen\ValeEstado
    {
        return $this->vale_estado;
    }

    /**
     * @param \App\Entity\Almacen\ValeEstado $vale_estado
     * @return Vale
     */
    public function setValeEstado(\App\Entity\Almacen\ValeEstado $vale_estado): Vale
    {
        $this->vale_estado = $vale_estado;
        return $this;
    }

    /**
     * @return \App\Entity\Administracion\Usuario
     */
    public function getUsuario(): ?\App\Entity\Administracion\Usuario
    {
        return $this->usuario;
    }

    /**
     * @param \App\Entity\Administracion\Usuario $usuario
     * @return Vale
     */
    public function setUsuario(\App\Entity\Administracion\Usuario $usuario): Vale
    {
        $this->usuario = $usuario;
        return $this;
    }

}
