<?php

namespace App\Entity\Almacen;

use Doctrine\ORM\Mapping as ORM;

/**
 * Almacen
 *
 * @ORM\Table(name="almacen.almacen", indexes={
 *     @ORM\Index(name="fk_almacen_institucion", columns={"id_institucion"})
 * })
 * @ORM\Entity(repositoryClass="App\Repository\Almacen\AlmacenRepository")
 */
class Almacen
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="codigo", type="string", length=50, nullable=false)
     */
    private $codigo;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=255, nullable=false)
     */
    private $nombre;

    /**
     * @var string|null
     *
     * @ORM\Column(name="descripcion", type="string", length=500, nullable=true)
     */
    private $descripcion;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="creado", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $creado;

    /**
     * @var \App\Entity\Administracion\Institucion
     *
     * @ORM\ManyToOne(targetEntity="\App\Entity\Administracion\Institucion")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_institucion", referencedColumnName="id")
     * })
     */
    private $institucion;

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return \DateTime
     */
    public function getCreado(): ?\DateTime
    {
        return $this->creado;
    }

    /**
     * @return string
     */
    public function getCodigo(): ?string
    {
        return $this->codigo;
    }

    /**
     * @param string $codigo
     * @return Almacen
     */
    public function setCodigo(string $codigo): Almacen
    {
        $this->codigo = $codigo;
        return $this;
    }

    /**
     * @return string
     */
    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    /**
     * @param string $nombre
     * @return Almacen
     */
    public function setNombre(string $nombre): Almacen
    {
        $this->nombre = $nombre;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getDescripcion(): ?string
    {
        return $this->descripcion;
    }

    /**
     * @param null|string $descripcion
     * @return Almacen
     */
    public function setDescripcion(?string $descripcion): Almacen
    {
        $this->descripcion = $descripcion;
        return $this;
    }

    /**
     * @param \DateTime $creado
     * @return Almacen
     */
    public function setCreado(\DateTime $creado): Almacen
    {
        $this->creado = $creado;
        return $this;
    }

    /**
     * @return \App\Entity\Administracion\Institucion
     */
    public function getInstitucion(): ?\App\Entity\Administracion\Institucion
    {
        return $this->institucion;
    }

    /**
     * @param \App\Entity\Administracion\Institucion $institucion
     * @return Almacen
     */
    public function setInstitucion(\App\Entity\Administracion\Institucion $institucion): Almacen
    {
        $this->institucion = $institucion;
        return $this;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return (string)$this->nombre;
    }

}
