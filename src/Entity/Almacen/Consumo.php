<?php

namespace App\Entity\Almacen;

use Doctrine\ORM\Mapping as ORM;

/**
 * Consumo
 *
 * @ORM\Table(name="almacen.consumo", uniqueConstraints={
 *     @ORM\UniqueConstraint(name="unique_consumo", columns={"id_vale", "id_movimiento"})
 * }, indexes={
 *     @ORM\Index(name="fk_consumo_movimiento", columns={"id_movimiento"}),
 *     @ORM\Index(name="fk_consumo_vale", columns={"id_vale"})
 * })
 * @ORM\Entity(repositoryClass="App\Repository\Almacen\ConsumoRepository")
 */
class Consumo
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="creado", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $creado;

    /**
     * @var Movimiento
     *
     * @ORM\ManyToOne(targetEntity="Movimiento")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_movimiento", referencedColumnName="id")
     * })
     */
    private $movimiento;

    /**
     * @var Vale
     *
     * @ORM\ManyToOne(targetEntity="Vale")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_vale", referencedColumnName="id")
     * })
     */
    private $vale;

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return \DateTime
     */
    public function getCreado(): ?\DateTime
    {
        return $this->creado;
    }

    /**
     * @param \DateTime $creado
     * @return Consumo
     */
    public function setCreado(\DateTime $creado): Consumo
    {
        $this->creado = $creado;
        return $this;
    }

    /**
     * @return Movimiento
     */
    public function getMovimiento(): ?Movimiento
    {
        return $this->movimiento;
    }

    /**
     * @param Movimiento $movimiento
     * @return Consumo
     */
    public function setMovimiento(Movimiento $movimiento): Consumo
    {
        $this->movimiento = $movimiento;
        return $this;
    }

    /**
     * @return Vale
     */
    public function getVale(): ?Vale
    {
        return $this->vale;
    }

    /**
     * @param Vale $vale
     * @return Consumo
     */
    public function setVale(Vale $vale): Consumo
    {
        $this->vale = $vale;
        return $this;
    }


}
