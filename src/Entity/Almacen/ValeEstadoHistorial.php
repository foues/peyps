<?php

namespace App\Entity\Almacen;

use Doctrine\ORM\Mapping as ORM;

/**
 * ValeEstado
 *
 * @ORM\Table(name="almacen.vale_estado_historial", indexes={
 *     @ORM\Index(name="fk_vale_estado_historial", columns={"id_vale"}),
 *     @ORM\Index(name="fk_historial_vale_estado", columns={"id_vale_estado"})
 * })
 * @ORM\Entity
 */
class ValeEstadoHistorial
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="descripcion", type="string", length=500, nullable=true)
     */
    private $descripcion;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="creado", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $creado;

    /**
     * @var Vale
     *
     * @ORM\ManyToOne(targetEntity="Vale")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_vale", referencedColumnName="id")
     * })
     */
    private $vale;

    /**
     * @var ValeEstado
     *
     * @ORM\ManyToOne(targetEntity="ValeEstado")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_vale_estado", referencedColumnName="id")
     * })
     */
    private $vale_estado;

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return null|string
     */
    public function getDescripcion(): ?string
    {
        return $this->descripcion;
    }

    /**
     * @param null|string $descripcion
     * @return ValeEstadoHistorial
     */
    public function setDescripcion(?string $descripcion): ValeEstadoHistorial
    {
        $this->descripcion = $descripcion;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreado(): ?\DateTime
    {
        return $this->creado;
    }

    /**
     * @param \DateTime $creado
     * @return ValeEstadoHistorial
     */
    public function setCreado(\DateTime $creado): ValeEstadoHistorial
    {
        $this->creado = $creado;
        return $this;
    }

    /**
     * @return Vale
     */
    public function getVale(): ?Vale
    {
        return $this->vale;
    }

    /**
     * @param Vale $vale
     * @return ValeEstadoHistorial
     */
    public function setVale(Vale $vale): ValeEstadoHistorial
    {
        $this->vale = $vale;
        return $this;
    }

    /**
     * @return ValeEstado
     */
    public function getValeEstado(): ?ValeEstado
    {
        return $this->vale_estado;
    }

    /**
     * @param ValeEstado $vale_estado
     * @return ValeEstadoHistorial
     */
    public function setValeEstado(ValeEstado $vale_estado): ValeEstadoHistorial
    {
        $this->vale_estado = $vale_estado;
        return $this;
    }


}
