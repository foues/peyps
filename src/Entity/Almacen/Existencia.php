<?php

namespace App\Entity\Almacen;

use Doctrine\ORM\Mapping as ORM;

/**
 * Existencia
 *
 * @ORM\Table(name="almacen.existencia", indexes={
 *     @ORM\Index(name="fk_existencia_almacen", columns={"id_almacen"}),
 *     @ORM\Index(name="fk_existencia_articulo_presentacion", columns={"id_articulo_presentacion"})
 * })
 * @ORM\Entity
 */
class Existencia
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="cantidad", type="integer", nullable=false)
     */
    private $cantidad;

    /**
     * @var string
     *
     * @ORM\Column(name="precio", type="float", precision=10, scale=0, nullable=false)
     */
    private $precio;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="adquisicion", type="date", nullable=false)
     */
    private $adquisicion;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="vencimiento", type="date", nullable=false)
     */
    private $vencimiento;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="creado", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $creado;

    /**
     * @var Almacen
     *
     * @ORM\ManyToOne(targetEntity="Almacen")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_almacen", referencedColumnName="id")
     * })
     */
    private $almacen;

    /**
     * @var ArticuloPresentacion
     *
     * @ORM\ManyToOne(targetEntity="ArticuloPresentacion")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_articulo_presentacion", referencedColumnName="id")
     * })
     */
    private $articulo_presentacion;

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getCantidad(): ?int
    {
        return $this->cantidad;
    }

    /**
     * @param int $cantidad
     * @return Existencia
     */
    public function setCantidad(int $cantidad): Existencia
    {
        $this->cantidad = $cantidad;
        return $this;
    }

    /**
     * @return string
     */
    public function getPrecio(): ?string
    {
        return $this->precio;
    }

    /**
     * @param string $precio
     * @return Existencia
     */
    public function setPrecio(string $precio): Existencia
    {
        $this->precio = $precio;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getAdquisicion(): ?\DateTime
    {
        return $this->adquisicion;
    }

    /**
     * @param \DateTime $adquisicion
     * @return Existencia
     */
    public function setAdquisicion(\DateTime $adquisicion): Existencia
    {
        $this->adquisicion = $adquisicion;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getVencimiento(): ?\DateTime
    {
        return $this->vencimiento;
    }

    /**
     * @param \DateTime $vencimiento
     * @return Existencia
     */
    public function setVencimiento(\DateTime $vencimiento): Existencia
    {
        $this->vencimiento = $vencimiento;
        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getCreado(): ?\DateTime
    {
        return $this->creado;
    }

    /**
     * @param \DateTime|null $creado
     * @return Existencia
     */
    public function setCreado(?\DateTime $creado): Existencia
    {
        $this->creado = $creado;
        return $this;
    }

    /**
     * @return Almacen
     */
    public function getAlmacen(): ?Almacen
    {
        return $this->almacen;
    }

    /**
     * @param Almacen $almacen
     * @return Existencia
     */
    public function setAlmacen(Almacen $almacen): Existencia
    {
        $this->almacen = $almacen;
        return $this;
    }

    /**
     * @return ArticuloPresentacion
     */
    public function getArticuloPresentacion(): ?ArticuloPresentacion
    {
        return $this->articulo_presentacion;
    }

    /**
     * @param ArticuloPresentacion $articulo_presentacion
     * @return Existencia
     */
    public function setArticuloPresentacion(ArticuloPresentacion $articulo_presentacion): Existencia
    {
        $this->articulo_presentacion = $articulo_presentacion;
        return $this;
    }


}
