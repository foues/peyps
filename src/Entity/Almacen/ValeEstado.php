<?php

namespace App\Entity\Almacen;

use Doctrine\ORM\Mapping as ORM;

/**
 * ValeEstado
 *
 * @ORM\Table(name="almacen.vale_estado", uniqueConstraints={
 *     @ORM\UniqueConstraint(name="codigo", columns={"codigo"})
 * })
 * @ORM\Entity
 */
class ValeEstado
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="codigo", type="string", length=50, nullable=false)
     */
    private $codigo;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=255, nullable=false)
     */
    private $nombre;

    /**
     * @var string|null
     *
     * @ORM\Column(name="descripcion", type="string", length=500, nullable=true)
     */
    private $descripcion;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="creado", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $creado;

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getCodigo(): ?string
    {
        return $this->codigo;
    }

    /**
     * @param string $codigo
     * @return ValeEstado
     */
    public function setCodigo(string $codigo): ValeEstado
    {
        $this->codigo = $codigo;
        return $this;
    }

    /**
     * @return string
     */
    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    /**
     * @param string $nombre
     * @return ValeEstado
     */
    public function setNombre(string $nombre): ValeEstado
    {
        $this->nombre = $nombre;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getDescripcion(): ?string
    {
        return $this->descripcion;
    }

    /**
     * @param null|string $descripcion
     * @return ValeEstado
     */
    public function setDescripcion(?string $descripcion): ValeEstado
    {
        $this->descripcion = $descripcion;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreado(): ?\DateTime
    {
        return $this->creado;
    }

    /**
     * @param \DateTime $creado
     * @return ValeEstado
     */
    public function setCreado(\DateTime $creado): ValeEstado
    {
        $this->creado = $creado;
        return $this;
    }

    /**
     * @return string
     */
    public function __toString(): ?string
    {
        return (string)$this->nombre;
    }


}
