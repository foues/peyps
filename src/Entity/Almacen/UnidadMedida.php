<?php

namespace App\Entity\Almacen;

use Doctrine\ORM\Mapping as ORM;

/**
 * UnidadMedida
 *
 * @ORM\Table(name="almacen.unidad_medida", uniqueConstraints={
 *     @ORM\UniqueConstraint(name="codigo", columns={"codigo"})
 * })
 * @ORM\Entity
 */
class UnidadMedida
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="codigo", type="string", length=50, nullable=false)
     */
    private $codigo;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=100, nullable=false)
     */
    private $nombre;

    /**
     * @var array|string
     *
     * @ORM\Column(name="subunidades", type="json", nullable=true)
     */
    private $subunidades;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="creado", type="datetime", nullable=false, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $creado;

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getCodigo(): ?string
    {
        return $this->codigo;
    }

    /**
     * @param string $codigo
     * @return UnidadMedida
     */
    public function setCodigo(string $codigo): UnidadMedida
    {
        $this->codigo = $codigo;
        return $this;
    }

    /**
     * @return string
     */
    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    /**
     * @param string $nombre
     * @return UnidadMedida
     */
    public function setNombre(string $nombre): UnidadMedida
    {
        $this->nombre = $nombre;
        return $this;
    }

    /**
     * @return array|string
     */
    public function getSubunidades()
    {
        return $this->subunidades;
    }

    /**
     * @return null|string
     */
    public function getSubunidadesAsString()
    {
        if (is_array($this->subunidades))
            return implode(', ', $this->subunidades);
        return null;
    }

    /**
     * @param array|string $subunidades
     * @return UnidadMedida
     */
    public function setSubunidades($subunidades): UnidadMedida
    {
        $this->subunidades = $subunidades;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreado(): ?\DateTime
    {
        return $this->creado;
    }

    /**
     * @param \DateTime $creado
     * @return UnidadMedida
     */
    public function setCreado(\DateTime $creado): UnidadMedida
    {
        $this->creado = $creado;
        return $this;
    }

    /**
     * @return string
     */
    public function __toString(): ?string
    {
        return (string)$this->nombre;
    }

}
