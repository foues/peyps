<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * @package DoctrineMigrations
 */
final class Version20180803153000 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('
CREATE OR REPLACE VIEW rotacion.cupo_intitucion_programa AS (
SELECT
  rotacion.programa_institucion.cantidad_estudiante AS cantidad_estudiante,
  rotacion.programa.codigo AS codigo,
  administracion.institucion.nombre AS nombre
FROM
  rotacion.programa_institucion
  JOIN rotacion.programa ON(rotacion.programa_institucion.id_programa = rotacion.programa.id)
  JOIN administracion.institucion ON(rotacion.programa_institucion.id_institucion = administracion.institucion.id)
)');
        $this->addSql('
CREATE OR REPLACE VIEW rotacion.estudiante_activo_materia AS (
SELECT
  academico.estudiante.carnet AS carnet,
  academico.estudiante_plan_estudio.cum AS cum,
  academico.estudiante_estado.tipo AS tipo,
  academico.estudiante_estado.codigo AS codigoEstudiante,
  academico.curso.codigo AS codigoCurso,
  academico.curso.nombre AS nombre
FROM
  academico.estudiante_plan_estudio
  JOIN academico.estudiante ON(academico.estudiante_plan_estudio.id_estudiante = academico.estudiante.id)
  JOIN academico.estudiante_curso ON(academico.estudiante_curso.id_estudiante_plan_estudio = academico.estudiante_plan_estudio.id)
  JOIN academico.curso ON(academico.estudiante_curso.id_curso = academico.curso.id)
  JOIN academico.estudiante_estado ON(academico.estudiante_plan_estudio.id_estudiante_estado = academico.estudiante_estado.id)
)');
        $this->addSql('
CREATE OR REPLACE VIEW rotacion.estudiante_activo_materia_grupo AS (
SELECT
  academico.estudiante.carnet AS carnet,
  academico.estudiante_plan_estudio.cum AS cum,
  academico.estudiante_estado.tipo AS tipo,
  academico.estudiante_estado.codigo AS codicoEstudiante,
  academico.curso.codigo AS codigoCurso,
  academico.curso.nombre AS nombre,
  rotacion.grupo.nombre AS grupoEstudiante,
  rotacion.grupo.id_periodo_ciclo AS peridoCiclo
FROM
  academico.estudiante_plan_estudio
  JOIN academico.estudiante ON(academico.estudiante_plan_estudio.id_estudiante = academico.estudiante.id)
  JOIN academico.estudiante_curso ON(academico.estudiante_curso.id_estudiante_plan_estudio = academico.estudiante_plan_estudio.id)
  JOIN academico.curso ON(academico.estudiante_curso.id_curso = academico.curso.id)
  JOIN academico.estudiante_estado ON(academico.estudiante_plan_estudio.id_estudiante_estado = academico.estudiante_estado.id)
  JOIN rotacion.grupo_estudiante ON(academico.estudiante_curso.id = rotacion.grupo_estudiante.id_estudiante_curso)
  JOIN rotacion.grupo ON(rotacion.grupo_estudiante.id_grupo = rotacion.grupo.id)
)');
        $this->addSql('
CREATE OR REPLACE VIEW rotacion.fechas_rotacion_view AS (
SELECT
  resumen.grupo AS grupo,
  resumen.rotacion AS rotacion,
  resumen.periodoCiclo AS periodoCiclo,
  str_to_date(sum(CASE WHEN (resumen.tipo = \'INICIO\' AND resumen.programa = \'CSNS\') THEN resumen.fecha ELSE NULL END),\'%Y%m%d\') AS inicio_csns,
  str_to_date(sum(CASE WHEN (resumen.tipo = \'INICIO\' AND resumen.programa = \'CESA\') THEN resumen.fecha ELSE NULL END),\'%Y%m%d\') AS inicio_cesa,
  str_to_date(sum(CASE WHEN (resumen.tipo = \'INICIO\' AND resumen.programa = \'HNR\') THEN resumen.fecha ELSE NULL END),\'%Y%m%d\') AS inicio_hnr,
  str_to_date(sum(CASE WHEN (resumen.tipo = \'DIA UNICO\' AND resumen.programa = \'ULC\') THEN resumen.fecha ELSE NULL END),\'%Y%m%d\') AS inicio_ulc,
  str_to_date(sum(CASE WHEN (resumen.tipo = \'DIA UNICO\' AND resumen.programa = \'CLC\') THEN resumen.fecha ELSE NULL END),\'%Y%m%d\') AS inicio_clc,
  str_to_date(sum(CASE WHEN (resumen.tipo = \'INICIO\' AND resumen.programa = \'PPE\') THEN resumen.fecha ELSE NULL END),\'%Y%m%d\') AS inicio_ppe,
  str_to_date(sum(CASE WHEN (resumen.tipo = \'FIN\' AND resumen.programa = \'CSNS\') THEN resumen.fecha ELSE NULL END),\'%Y%m%d\') AS fin_csns,
  str_to_date(sum(CASE WHEN (resumen.tipo = \'FIN\' AND resumen.programa = \'CESA\') THEN resumen.fecha ELSE NULL END),\'%Y%m%d\') AS fin_cesa,
  str_to_date(sum(CASE WHEN (resumen.tipo = \'FIN\' AND resumen.programa = \'HNR\') THEN resumen.fecha ELSE NULL END),\'%Y%m%d\') AS fin_hnr,
  str_to_date(sum(CASE WHEN (resumen.tipo = \'FIN\' AND resumen.programa = \'PPE\') THEN resumen.fecha ELSE NULL END),\'%Y%m%d\') AS fin_ppe
FROM (
  SELECT
    a.fecha AS fecha,
    a.tipo AS tipo,
    b.codigo AS programa,
    c.nombre AS rotacion,
    d.nombre AS grupo,
    e.id_periodo AS periodoCiclo
  FROM
    rotacion.programa_calendario a
    JOIN rotacion.programa b ON(a.id_programa = b.id)
    JOIN rotacion.grupo c ON(a.id_grupo = c.id)
    JOIN rotacion.grupo d ON(c.id_padre = d.id)
    JOIN academico.periodo_ciclo e ON(a.id_periodo_ciclo = e.id)
  ) resumen
GROUP BY resumen.periodoCiclo,resumen.rotacion,resumen.periodoCiclo
ORDER BY resumen.periodoCiclo,resumen.periodoCiclo
)');
        $this->addSql('
CREATE OR REPLACE VIEW rotacion.institucion_programa AS (
SELECT
  rotacion.programa.id AS idPrograma,
  rotacion.programa.codigo AS codigoPrograma,
  rotacion.programa.nombre AS nombrePrograma,
  administracion.institucion.id AS idInstitucion,
  administracion.institucion.nombre AS nombreInstitucion
FROM
  rotacion.programa_institucion
  JOIN rotacion.programa ON(rotacion.programa_institucion.id_programa = rotacion.programa.id)
  JOIN administracion.institucion ON(administracion.institucion.id = rotacion.programa_institucion.id_institucion)
)');
        $this->addSql('
CREATE OR REPLACE VIEW rotacion.programa_materia AS (
SELECT
  academico.curso.codigo AS codigo,
  academico.curso.nombre AS nombre,
  academico.plan_estudio_ciclo.romano AS romano,
  rotacion.programa.codigo AS codigoPrograma
FROM
  academico.plan_estudio_curso
  JOIN academico.curso ON(academico.plan_estudio_curso.id_curso = academico.curso.id)
  JOIN academico.curso_programa ON(academico.curso_programa.id_plan_estudio_curso = academico.plan_estudio_curso.id)
  JOIN academico.plan_estudio_ciclo ON(academico.plan_estudio_curso.id_plan_estudio_ciclo = academico.plan_estudio_ciclo.id)
  JOIN rotacion.programa ON(academico.curso_programa.id_programa = rotacion.programa.id)
)');
    }

    public function down(Schema $schema): void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP VIEW IF EXISTS rotacion.programa_materia');
        $this->addSql('DROP VIEW IF EXISTS rotacion.institucion_programa');
        $this->addSql('DROP VIEW IF EXISTS rotacion.fechas_rotacion_view');
        $this->addSql('DROP VIEW IF EXISTS rotacion.estudiante_activo_materia_grupo');
        $this->addSql('DROP VIEW IF EXISTS rotacion.estudiante_activo_materia');
        $this->addSql('DROP VIEW IF EXISTS rotacion.cupo_intitucion_programa');
    }
}
