<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please MODIFY to your needs!
 */
final class Version20180823061254 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please MODIFY it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        //Version20180723010905
        $this->addSql('
        CREATE TABLE clinico.tratamiento_estado(
          id             int(11) AUTO_INCREMENT,
          id_tratamiento int(11) NOT NULL,
          id_estado      int(11) NOT NULL,
          creado         timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP() ON UPDATE CURRENT_TIMESTAMP(),
          CONSTRAINT pk_tratamiento_estado PRIMARY KEY(id),
          CONSTRAINT fk_tratamiento_estado_tratamiento FOREIGN KEY(id_tratamiento) REFERENCES clinico.tratamiento(id),
          CONSTRAINT fk_tratamiento_estado_estado FOREIGN KEY(id_estado) REFERENCES clinico.ficha_catalogo(id),
          CONSTRAINT tratamiento_estado_unique UNIQUE(id_tratamiento, id_estado)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
        ');


        //Version20180723074217
        $this->addSql('ALTER TABLE clinico.evaluacion_dental ADD realizado tinyint(3) DEFAULT 0');


        //Version20180726233659
        $this->addSql('ALTER TABLE clinico.evaluacion_dental DROP FOREIGN KEY fk_evaluacion_codigo_tratamiento;');
        $this->addSql('ALTER TABLE clinico.evaluacion_dental DROP id_codigo_tratamiento');
        $this->addSql('ALTER TABLE clinico.codigo_tratamiento DROP FOREIGN KEY fk_codigo_tratamiento;');
        $this->addSql('ALTER TABLE clinico.codigo_tratamiento DROP FOREIGN KEY fk_codigo_caries;');
        $this->addSql('ALTER TABLE clinico.codigo_tratamiento DROP FOREIGN KEY fk_codigo_restauracion;');
        $this->addSql('DROP TABLE clinico.codigo_tratamiento');
        $this->addSql('ALTER TABLE clinico.evaluacion_dental ADD id_tratamiento int(11) NOT NULL');
        $this->addSql('ALTER TABLE clinico.evaluacion_dental ADD CONSTRAINT fk_evaluacion_dental_tratamiento FOREIGN KEY(id_tratamiento) REFERENCES clinico.tratamiento(id)');
        $this->addSql('ALTER TABLE clinico.evaluacion_dental ADD CONSTRAINT unique_evaluacion_dental UNIQUE(id_ficha, id_tratamiento, id_pieza_dental, id_pieza_dental_superficie, id_pieza_dental_estado);');


        //Version20180728231804
        $this->addSql("ALTER TABLE clinico.ficha MODIFY tipo_reaccion varchar(255);");
        $this->addSql("ALTER TABLE clinico.ficha MODIFY pieza varchar(255);");
        $this->addSql("ALTER TABLE clinico.ficha MODIFY otras_condiciones varchar(999);");
        $this->addSql("ALTER TABLE clinico.ficha MODIFY anotaciones_varias varchar(999);");
        $this->addSql("ALTER TABLE clinico.ficha MODIFY observaciones varchar(999);");
        $this->addSql("ALTER TABLE clinico.ficha MODIFY comentarios varchar(999);");
        $this->addSql("ALTER TABLE clinico.ficha MODIFY total_ingesta_azucares double;");
        $this->addSql("ALTER TABLE clinico.ficha MODIFY total_placa_bacteriana double;");
        $this->addSql("ALTER TABLE clinico.ficha MODIFY total_indice_cpo_ceo double;");
        $this->addSql("ALTER TABLE clinico.ficha MODIFY id_referencia int(11);");
        $this->addSql("ALTER TABLE clinico.ficha MODIFY id_indice_riesgo_cariogenico int(11);");
        $this->addSql("ALTER TABLE clinico.ficha_tratamiento MODIFY cantidad int(11);");
        $this->addSql("ALTER TABLE clinico.ficha_tratamiento MODIFY acciones_realizar varchar(500);");


        //Version20180816153638
        $this->addSql('ALTER TABLE clinico.ficha_catalogo DROP FOREIGN KEY fk_ficha_catalogo_padre;');
        $this->addSql('ALTER TABLE clinico.ficha_catalogo DROP FOREIGN KEY fk_ficha_catalogo_tipo;');
        $this->addSql('ALTER TABLE clinico.ficha_catalogo DROP COLUMN id_padre;');
        $this->addSql('ALTER TABLE clinico.ficha_catalogo DROP COLUMN id_ficha_catalogo_tipo;');
        $this->addSql('DROP TABLE clinico.ficha_catalogo_tipo;');
        $this->addSql('ALTER TABLE clinico.ficha_catalogo ADD COLUMN id_tipo int(11);');
        $this->addSql('ALTER TABLE clinico.ficha_catalogo ADD CONSTRAINT fk_ficha_catalogo_tipo FOREIGN KEY (id_tipo) REFERENCES clinico.ficha_catalogo (id) on delete cascade;');


        //Version20180816204613
        $this->addSql('
        CREATE TABLE clinico.ficha_atributo (
          id int(11) NOT NULL AUTO_INCREMENT, 
          id_ficha int(11) NOT NULL, 
          id_atributo int(11) NOT NULL, 
          creado timestamp NOT NULL,
          CONSTRAINT ficha_atributo_pk 
            PRIMARY KEY (id)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
        ');
        $this->addSql('ALTER TABLE clinico.ficha_atributo ADD CONSTRAINT fk_ficha_atributo FOREIGN KEY (id_ficha) REFERENCES clinico.ficha (id)');
        $this->addSql('ALTER TABLE clinico.ficha_atributo ADD CONSTRAINT fk_atributo_ficha FOREIGN KEY (id_atributo) REFERENCES clinico.ficha_catalogo (id)');
        $this->addSql('ALTER TABLE clinico.ficha_evaluacion_sistemica DROP FOREIGN KEY fk_ficha_evaluacion_sistemica;');
        $this->addSql('ALTER TABLE clinico.ficha_evaluacion_sistemica DROP FOREIGN KEY fk_evaluacion_sistema;');
        $this->addSql('ALTER TABLE clinico.ficha_habito_bucal DROP FOREIGN KEY fk_ficha_habito_bucal;');
        $this->addSql('ALTER TABLE clinico.ficha_habito_bucal DROP FOREIGN KEY fk_habito_bucal;');
        $this->addSql('ALTER TABLE clinico.ficha_higiene_bucal DROP FOREIGN KEY fk_ficha_higiene_bucal;');
        $this->addSql('ALTER TABLE clinico.ficha_higiene_bucal DROP FOREIGN KEY fk_higiene_bucal;');
        $this->addSql('ALTER TABLE clinico.ingesta_azucar DROP FOREIGN KEY fk_ingesta_azucar_ficha;');
        $this->addSql('ALTER TABLE clinico.ingesta_azucar DROP FOREIGN KEY fk_ingesta_azucar_criterio;');
        $this->addSql('ALTER TABLE clinico.placa_bacteriana DROP FOREIGN KEY fk_placa_bacteriana_ficha;');
        $this->addSql('ALTER TABLE clinico.placa_bacteriana DROP FOREIGN KEY fk_placa_bacteriana_indice;');
        $this->addSql('DROP TABLE clinico.ficha_evaluacion_sistemica');
        $this->addSql('DROP TABLE clinico.ficha_habito_bucal');
        $this->addSql('DROP TABLE clinico.ficha_higiene_bucal');
        $this->addSql('DROP TABLE clinico.ingesta_azucar');
        $this->addSql('DROP TABLE clinico.placa_bacteriana');


        //Version20180816215802
        $this->addSql('ALTER TABLE clinico.evaluacion_dental DROP FOREIGN KEY fk_evaluacion_dental_ficha;');
        $this->addSql('ALTER TABLE clinico.evaluacion_dental DROP FOREIGN KEY fk_evaluacion_dental_tratamiento;');
        $this->addSql('ALTER TABLE clinico.evaluacion_dental DROP FOREIGN KEY fk_evaluacion_pieza_dental;');
        $this->addSql('ALTER TABLE clinico.evaluacion_dental DROP FOREIGN KEY fk_evaluacion_pieza_dental_superficie;');
        $this->addSql('ALTER TABLE clinico.evaluacion_dental DROP FOREIGN KEY fk_evaluacion_pieza_dental_estado;');
        $this->addSql('ALTER TABLE clinico.evaluacion_dental DROP INDEX unique_evaluacion_dental;');
        $this->addSql('ALTER TABLE clinico.evaluacion_dental ADD CONSTRAINT fk_evaluacion_dental_ficha FOREIGN KEY (id_ficha) REFERENCES clinico.ficha (id);');
        $this->addSql('ALTER TABLE clinico.evaluacion_dental ADD CONSTRAINT fk_evaluacion_dental_tratamiento FOREIGN KEY (id_tratamiento) REFERENCES clinico.tratamiento (id);');
        $this->addSql('ALTER TABLE clinico.evaluacion_dental ADD CONSTRAINT fk_evaluacion_pieza_dental_estado FOREIGN KEY (id_pieza_dental_estado) REFERENCES clinico.ficha_catalogo (id);');
        $this->addSql('ALTER TABLE clinico.evaluacion_dental DROP COLUMN id_pieza_dental_superficie;');
        $this->addSql('ALTER TABLE clinico.evaluacion_dental DROP COLUMN id_pieza_dental;');
        $this->addSql('ALTER TABLE clinico.evaluacion_dental ADD COLUMN pieza_dental varchar(10) NOT NULL;');
        $this->addSql('ALTER TABLE clinico.evaluacion_dental ADD COLUMN superficie_dental varchar(15) NOT NULL;');
        $this->addSql('ALTER TABLE clinico.evaluacion_dental ADD CONSTRAINT unique_evaluacion_dental UNIQUE(id_ficha, id_tratamiento, id_pieza_dental_estado, pieza_dental, superficie_dental);');
        $this->addSql('ALTER TABLE clinico.lesion DROP FOREIGN KEY fk_lesion_pieza_dental;');
        $this->addSql('ALTER TABLE clinico.lesion DROP COLUMN id_pieza_dental;');
        $this->addSql('ALTER TABLE clinico.lesion ADD COLUMN pieza_dental varchar(10) NOT NULL;');
        $this->addSql('ALTER TABLE clinico.ficha_tratamiento DROP FOREIGN KEY fk_ficha_tratamiento_pieza_dental;');
        $this->addSql('ALTER TABLE clinico.ficha_tratamiento DROP COLUMN id_pieza_dental;');
        $this->addSql('ALTER TABLE clinico.ficha_tratamiento ADD COLUMN pieza_dental varchar(10) NOT NULL;');


        //Version20180820074826
        $this->addSql('ALTER TABLE clinico.evaluacion_dental ADD prioridad tinyint(3) DEFAULT 0');
        $this->addSql('ALTER TABLE clinico.ficha ADD finalizada tinyint(3) DEFAULT 0');
        $this->addSql('ALTER TABLE clinico.expediente_clinico ADD codigo_expediente varchar(25)');


        //Version20180821155618
        $this->addSql('ALTER TABLE clinico.ficha ADD curso varchar(10)');
        $this->addSql('ALTER TABLE clinico.ficha ADD ciclo varchar(10)');

    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please MODIFY it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        //Version20180821155618
        $this->addSql('ALTER TABLE clinico.ficha DROP COLUMN ciclo');
        $this->addSql('ALTER TABLE clinico.ficha DROP COLUMN curso');


        //Version20180820074826
        $this->addSql('ALTER TABLE clinico.expediente_clinico DROP COLUMN codigo_expediente');
        $this->addSql('ALTER TABLE clinico.ficha DROP COLUMN finalizada');
        $this->addSql('ALTER TABLE clinico.evaluacion_dental DROP COLUMN prioridad');


        //Version20180816215802
        $this->addSql('ALTER TABLE clinico.ficha_tratamiento DROP COLUMN pieza_dental;');
        $this->addSql('ALTER TABLE clinico.ficha_tratamiento ADD COLUMN id_pieza_dental int(11) NOT NULL;');
        $this->addSql('ALTER TABLE clinico.ficha_tratamiento ADD CONSTRAINT fk_ficha_tratamiento_pieza_dental FOREIGN KEY (id_pieza_dental) REFERENCES clinico.ficha_catalogo (id);');
        $this->addSql('ALTER TABLE clinico.lesion DROP COLUMN pieza_dental;');
        $this->addSql('ALTER TABLE clinico.lesion ADD COLUMN id_pieza_dental int(11) NOT NULL;');
        $this->addSql('ALTER TABLE clinico.lesion ADD CONSTRAINT fk_lesion_pieza_dental FOREIGN KEY (id_pieza_dental) REFERENCES clinico.ficha_catalogo (id);');
        $this->addSql('ALTER TABLE clinico.evaluacion_dental DROP FOREIGN KEY fk_evaluacion_pieza_dental_estado;');
        $this->addSql('ALTER TABLE clinico.evaluacion_dental DROP FOREIGN KEY fk_evaluacion_dental_tratamiento;');
        $this->addSql('ALTER TABLE clinico.evaluacion_dental DROP FOREIGN KEY fk_evaluacion_dental_ficha;');
        $this->addSql('ALTER TABLE clinico.evaluacion_dental DROP INDEX unique_evaluacion_dental;');
        $this->addSql('ALTER TABLE clinico.evaluacion_dental DROP COLUMN superficie_dental;');
        $this->addSql('ALTER TABLE clinico.evaluacion_dental DROP COLUMN pieza_dental;');
        $this->addSql('ALTER TABLE clinico.evaluacion_dental ADD COLUMN id_pieza_dental int(11) NOT NULL;');
        $this->addSql('ALTER TABLE clinico.evaluacion_dental ADD COLUMN id_pieza_dental_superficie int(11) NOT NULL;');
        $this->addSql('ALTER TABLE clinico.evaluacion_dental ADD CONSTRAINT unique_evaluacion_dental UNIQUE(id_ficha, id_tratamiento, id_pieza_dental, id_pieza_dental_superficie, id_pieza_dental_estado);');
        $this->addSql('ALTER TABLE clinico.evaluacion_dental ADD CONSTRAINT fk_evaluacion_pieza_dental_estado FOREIGN KEY (id_pieza_dental_estado) REFERENCES clinico.ficha_catalogo (id);');
        $this->addSql('ALTER TABLE clinico.evaluacion_dental ADD CONSTRAINT fk_evaluacion_pieza_dental_superficie FOREIGN KEY (id_pieza_dental_superficie) REFERENCES clinico.ficha_catalogo (id);');
        $this->addSql('ALTER TABLE clinico.evaluacion_dental ADD CONSTRAINT fk_evaluacion_pieza_dental FOREIGN KEY (id_pieza_dental) REFERENCES clinico.ficha_catalogo (id);');
        $this->addSql('ALTER TABLE clinico.evaluacion_dental ADD CONSTRAINT fk_evaluacion_dental_tratamiento FOREIGN KEY (id_tratamiento) REFERENCES clinico.tratamiento (id);');
        $this->addSql('ALTER TABLE clinico.evaluacion_dental ADD CONSTRAINT fk_evaluacion_dental_ficha FOREIGN KEY (id_ficha) REFERENCES clinico.ficha (id);');


        //Version20180816204613
        $this->addSql(' 
        CREATE TABLE clinico.placa_bacteriana (
          id                         int(11) NOT NULL AUTO_INCREMENT, 
          id_ficha                   int(11) NOT NULL, 
          id_placa_bacteriana_indice int(11) NOT NULL, 
          valor                      int(11), 
          creado                     timestamp NOT NULL, 
          CONSTRAINT placa_bacteriana_pk 
            PRIMARY KEY (id)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
        ');
        $this->addSql(' 
        CREATE TABLE clinico.ingesta_azucar (
          id                         int(11) NOT NULL AUTO_INCREMENT, 
          id_ficha                   int(11) NOT NULL, 
          id_ingesta_azucar_criterio int(11) NOT NULL, 
          creado                     timestamp NOT NULL, 
          CONSTRAINT ingesta_azucar_pk 
            PRIMARY KEY (id)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
        ');
        $this->addSql(' 
        CREATE TABLE clinico.ficha_higiene_bucal (
          id               int(11) NOT NULL AUTO_INCREMENT, 
          id_ficha         int(11) NOT NULL, 
          id_higiene_bucal int(11) NOT NULL, 
          creado           timestamp NOT NULL, 
          CONSTRAINT ficha_higiene_pk 
            PRIMARY KEY (id)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
        ');
        $this->addSql(' 
        CREATE TABLE clinico.ficha_habito_bucal (
          id              int(11) NOT NULL AUTO_INCREMENT, 
          id_ficha        int(11) NOT NULL, 
          id_habito_bucal int(11) NOT NULL, 
          creado          timestamp NOT NULL, 
          CONSTRAINT ficha_habitos_pk 
            PRIMARY KEY (id)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
        ');
        $this->addSql('
        CREATE TABLE clinico.ficha_evaluacion_sistemica (
          id                      int(11) NOT NULL AUTO_INCREMENT, 
          id_ficha                int(11) NOT NULL, 
          id_evaluacion_sistemica int(11) NOT NULL, 
          creado                  timestamp NOT NULL, 
          CONSTRAINT ficha_evaluacion_sistemica_pk 
            PRIMARY KEY (id)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
        ');
        $this->addSql('ALTER TABLE clinico.placa_bacteriana ADD CONSTRAINT fk_placa_bacteriana_indice FOREIGN KEY (id_placa_bacteriana_indice) REFERENCES clinico.ficha_catalogo (id);');
        $this->addSql('ALTER TABLE clinico.placa_bacteriana ADD CONSTRAINT fk_placa_bacteriana_ficha FOREIGN KEY (id_ficha) REFERENCES clinico.ficha (id);');
        $this->addSql('ALTER TABLE clinico.ingesta_azucar ADD CONSTRAINT fk_ingesta_azucar_criterio FOREIGN KEY (id_ingesta_azucar_criterio) REFERENCES clinico.ficha_catalogo (id);');
        $this->addSql('ALTER TABLE clinico.ingesta_azucar ADD CONSTRAINT fk_ingesta_azucar_ficha FOREIGN KEY (id_ficha) REFERENCES clinico.ficha (id);');
        $this->addSql('ALTER TABLE clinico.ficha_higiene_bucal ADD CONSTRAINT fk_higiene_bucal FOREIGN KEY (id_higiene_bucal) REFERENCES clinico.ficha_catalogo (id);');
        $this->addSql('ALTER TABLE clinico.ficha_higiene_bucal ADD CONSTRAINT fk_ficha_higiene_bucal FOREIGN KEY (id_ficha) REFERENCES clinico.ficha (id);');
        $this->addSql('ALTER TABLE clinico.ficha_habito_bucal ADD CONSTRAINT fk_habito_bucal FOREIGN KEY (id_habito_bucal) REFERENCES clinico.ficha_catalogo (id);');
        $this->addSql('ALTER TABLE clinico.ficha_habito_bucal ADD CONSTRAINT fk_ficha_habito_bucal FOREIGN KEY (id_ficha) REFERENCES clinico.ficha (id);');
        $this->addSql('ALTER TABLE clinico.ficha_evaluacion_sistemica ADD CONSTRAINT fk_evaluacion_sistema FOREIGN KEY (id_evaluacion_sistemica) REFERENCES clinico.ficha_catalogo (id);');
        $this->addSql('ALTER TABLE clinico.ficha_evaluacion_sistemica ADD CONSTRAINT fk_ficha_evaluacion_sistemica FOREIGN KEY (id_ficha) REFERENCES clinico.ficha (id);');
        $this->addSql('ALTER TABLE clinico.ficha_atributo DROP FOREIGN KEY fk_atributo_ficha;');
        $this->addSql('ALTER TABLE clinico.ficha_atributo DROP FOREIGN KEY fk_ficha_atributo;');
        $this->addSql('DROP TABLE clinico.ficha_atributo');


        //Version20180816153638
        $this->addSql('ALTER TABLE clinico.ficha_catalogo DROP FOREIGN KEY fk_ficha_catalogo_tipo;');
        $this->addSql('ALTER TABLE clinico.ficha_catalogo DROP COLUMN id_tipo;');
        $this->addSql(' 
        CREATE TABLE clinico.ficha_catalogo_tipo (
          id          int(11) NOT NULL AUTO_INCREMENT, 
          id_padre    int(11), 
          codigo      varchar(50) NOT NULL UNIQUE, 
          nombre      varchar(255) NOT NULL, 
          descripcion varchar(500), 
          creado      timestamp NOT NULL, 
          CONSTRAINT ficha_catalogo_tipo_pk 
            PRIMARY KEY (id)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci
        comment=\'lesion_tipo\';
        ');
        $this->addSql('ALTER TABLE clinico.ficha_catalogo ADD COLUMN id_ficha_catalogo_tipo int(11) NOT NULL;');
        $this->addSql('ALTER TABLE clinico.ficha_catalogo ADD COLUMN id_padre int(11);');
        $this->addSql('ALTER TABLE clinico.ficha_catalogo ADD CONSTRAINT fk_ficha_catalogo_tipo FOREIGN KEY (id_ficha_catalogo_tipo) REFERENCES clinico.ficha_catalogo_tipo (id);');
        $this->addSql('ALTER TABLE clinico.ficha_catalogo ADD CONSTRAINT fk_ficha_catalogo_padre FOREIGN KEY (id_padre) REFERENCES clinico.ficha_catalogo (id);');


        //Version20180728231804
        $this->addSql("ALTER TABLE clinico.ficha_tratamiento MODIFY acciones_realizar varchar(500) NOT NULL;");
        $this->addSql("ALTER TABLE clinico.ficha_tratamiento MODIFY cantidad int(11) NOT NULL;");
        $this->addSql("ALTER TABLE clinico.ficha MODIFY id_indice_riesgo_cariogenico int(11) NOT NULL;");
        $this->addSql("ALTER TABLE clinico.ficha MODIFY id_referencia int(11) NOT NULL;");
        $this->addSql("ALTER TABLE clinico.ficha MODIFY total_indice_cpo_ceo real NOT NULL;");
        $this->addSql("ALTER TABLE clinico.ficha MODIFY total_placa_bacteriana real NOT NULL;");
        $this->addSql("ALTER TABLE clinico.ficha MODIFY total_ingesta_azucares real NOT NULL;");
        $this->addSql("ALTER TABLE clinico.ficha MODIFY comentarios varchar(999) NOT NULL;");
        $this->addSql("ALTER TABLE clinico.ficha MODIFY observaciones varchar(999) NOT NULL;");
        $this->addSql("ALTER TABLE clinico.ficha MODIFY anotaciones_varias varchar(999) NOT NULL;");
        $this->addSql("ALTER TABLE clinico.ficha MODIFY otras_condiciones varchar(999) NOT NULL;");
        $this->addSql("ALTER TABLE clinico.ficha MODIFY pieza varchar(255) NOT NULL NOT NULL;");
        $this->addSql("ALTER TABLE clinico.ficha MODIFY tipo_reaccion varchar(255) NOT NULL;");


        //Version20180726233659
        $this->addSql('ALTER TABLE clinico.evaluacion_dental DROP FOREIGN KEY fk_evaluacion_dental_tratamiento');
        $this->addSql('ALTER TABLE clinico.evaluacion_dental DROP FOREIGN KEY fk_evaluacion_pieza_dental_estado');
        $this->addSql('ALTER TABLE clinico.evaluacion_dental DROP FOREIGN KEY fk_evaluacion_pieza_dental_superficie');
        $this->addSql('ALTER TABLE clinico.evaluacion_dental DROP FOREIGN KEY fk_evaluacion_pieza_dental');
        $this->addSql('ALTER TABLE clinico.evaluacion_dental DROP FOREIGN KEY fk_evaluacion_dental_ficha');
        $this->addSql('ALTER TABLE clinico.evaluacion_dental DROP INDEX unique_evaluacion_dental');
        $this->addSql('ALTER TABLE clinico.evaluacion_dental ADD CONSTRAINT fk_evaluacion_pieza_dental_estado FOREIGN KEY (id_pieza_dental_estado) REFERENCES clinico.ficha_catalogo (id);');
        $this->addSql('ALTER TABLE clinico.evaluacion_dental ADD CONSTRAINT fk_evaluacion_pieza_dental_superficie FOREIGN KEY (id_pieza_dental_superficie) REFERENCES clinico.ficha_catalogo (id);');
        $this->addSql('ALTER TABLE clinico.evaluacion_dental ADD CONSTRAINT fk_evaluacion_pieza_dental FOREIGN KEY (id_pieza_dental) REFERENCES clinico.ficha_catalogo (id);');
        $this->addSql('ALTER TABLE clinico.evaluacion_dental ADD CONSTRAINT fk_evaluacion_dental_ficha FOREIGN KEY (id_ficha) REFERENCES clinico.ficha (id);');


        $this->addSql('ALTER TABLE clinico.evaluacion_dental DROP id_tratamiento');
        $this->addSql('
        CREATE TABLE clinico.codigo_tratamiento (
          id                     int(11) NOT NULL AUTO_INCREMENT,
          id_codigo_restauracion int(11) NOT NULL,
          id_codigo_caries       int(11) NOT NULL,
          id_tratamiento         int(11) NOT NULL,
          creado                 timestamp NOT NULL, 
          CONSTRAINT codigo_tratamiento_pk 
            PRIMARY KEY (id)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
        ');
        $this->addSql('ALTER TABLE clinico.codigo_tratamiento ADD CONSTRAINT fk_codigo_restauracion FOREIGN KEY (id_codigo_restauracion) REFERENCES clinico.ficha_catalogo (id);');
        $this->addSql('ALTER TABLE clinico.codigo_tratamiento ADD CONSTRAINT fk_codigo_caries FOREIGN KEY (id_codigo_caries) REFERENCES clinico.ficha_catalogo (id);');
        $this->addSql('ALTER TABLE clinico.codigo_tratamiento ADD CONSTRAINT fk_codigo_tratamiento FOREIGN KEY (id_tratamiento) REFERENCES clinico.tratamiento (id);');
        $this->addSql('ALTER TABLE clinico.evaluacion_dental ADD id_codigo_tratamiento int(11) NOT NULL');
        $this->addSql('ALTER TABLE clinico.evaluacion_dental ADD CONSTRAINT fk_evaluacion_codigo_tratamiento FOREIGN KEY (id_codigo_tratamiento) REFERENCES clinico.codigo_tratamiento (id);');


        //Version20180723074217
        $this->addSql('ALTER TABLE clinico.evaluacion_dental DROP realizado');


        //Version20180723010905
        $this->addSql('ALTER TABLE clinico.tratamiento_estado DROP FOREIGN KEY fk_tratamiento_estado_tratamiento;');
        $this->addSql('ALTER TABLE clinico.tratamiento_estado DROP FOREIGN KEY fk_tratamiento_estado_estado;');
        $this->addSql('DROP TABLE if EXISTS clinico.tratamiento_estado;');
    }
}
