<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Class Version20181011051023
 * @package DoctrineMigrations
 */
final class Version20181011051023 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('
        CREATE OR REPLACE VIEW academico.calificacion_promedio_vw AS
        SELECT calificacion.id id, ec.id id_estudiante_curso, e.id id_evaluacion, e.ponderacion ponderacion, 
        AVG(calificacion.nota) promedio, (AVG(calificacion.nota)*e.ponderacion/100) nota
        FROM academico.calificacion calificacion
        INNER JOIN academico.evaluacion e ON calificacion.id_evaluacion = e.id
        INNER JOIN academico.unidad_integracion ui ON e.id_unidad_integracion = ui.id
        INNER JOIN academico.estudiante_curso ec ON calificacion.id_estudiante_curso = ec.id
        GROUP BY ec.id, e.id
        ');
    }

    public function down(Schema $schema): void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP VIEW IF EXISTS academico.calificacion_promedio_vw');
    }
}
