<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Class Version20181006044831
 * @package DoctrineMigrations
 */
final class Version20181006044831 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        //20180902044831
        $this->addSql('CREATE TABLE clinico.riesgo_detalle (
            id int(11) NOT NULL AUTO_INCREMENT, 
            id_ficha int(11) NOT NULL, 
            total_indice_cpo_ceo real NOT NULL,  
            total_placa_bacteriana real NOT NULL,
            total_ingesta_azucares real NOT NULL, 
            placa_1655 int(1) NOT NULL, 
            placa_1151 int(1) NOT NULL, 
            placa_2665 int(1) NOT NULL, 
            placa_3675 int(1) NOT NULL, 
            placa_3171 int(1) NOT NULL, 
            placa_4685 int(1) NOT NULL, 
            observaciones varchar(999),  
            otras_condiciones varchar(999), 
            comentarios varchar(999),
            creado TIMESTAMP NOT NULL,  
            CONSTRAINT ficha_atributo_pk 
              PRIMARY KEY (id)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
          ');
        $this->addSql('ALTER TABLE clinico.riesgo_detalle ADD CONSTRAINT fk_ficha_riesgo FOREIGN KEY (id_ficha) REFERENCES clinico.ficha (id)');
        $this->addSql('ALTER TABLE clinico.ficha DROP COLUMN total_indice_cpo_ceo;');
        $this->addSql('ALTER TABLE clinico.ficha DROP COLUMN total_placa_bacteriana;');
        $this->addSql('ALTER TABLE clinico.ficha DROP COLUMN total_ingesta_azucares;');
        $this->addSql('ALTER TABLE clinico.ficha DROP COLUMN observaciones;');
        $this->addSql('ALTER TABLE clinico.ficha DROP COLUMN otras_condiciones;');
        $this->addSql('ALTER TABLE clinico.ficha DROP COLUMN comentarios;');


        //20180923172208
        $this->addSql('ALTER TABLE clinico.ficha ADD id_indice_riesgo_real int(11);');
        $this->addSql('ALTER TABLE clinico.ficha ADD CONSTRAINT fk_indice_riesgo_real FOREIGN KEY (id_indice_riesgo_real) REFERENCES clinico.ficha_catalogo (id);');

    }

    public function down(Schema $schema): void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        //20180902044831
        $this->addSql('ALTER TABLE clinico.riesgo_detalle DROP FOREIGN KEY fk_ficha_riesgo;');
        $this->addSql('DROP TABLE clinico.riesgo_detalle');
        $this->addSql('ALTER TABLE clinico.ficha ADD total_indice_cpo_ceo real NOT NULL;');
        $this->addSql('ALTER TABLE clinico.ficha ADD total_placa_bacteriana real NOT NULL;');
        $this->addSql('ALTER TABLE clinico.ficha ADD total_ingesta_azucares real NOT NULL;');
        $this->addSql('ALTER TABLE clinico.ficha ADD observaciones varchar(999);');
        $this->addSql('ALTER TABLE clinico.ficha ADD otras_condiciones varchar(999);');
        $this->addSql('ALTER TABLE clinico.ficha ADD comentarios varchar(999);');


        //20180923172208
        $this->addSql('ALTER TABLE clinico.ficha DROP FOREIGN KEY fk_indice_riesgo_real;');
        $this->addSql('ALTER TABLE clinico.ficha DROP COLUMN id_indice_riesgo_real;');
    }
}
