<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Class Version20180930204115
 * @package DoctrineMigrations
 */
final class Version20180930204115 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE almacen.existencia MODIFY precio float NOT NULL');
        $this->addSql('ALTER TABLE almacen.movimiento MODIFY precio float NOT NULL');

        $this->addSql('ALTER TABLE almacen.vale ADD COLUMN id_usuario int NOT NULL');
        $this->addSql('ALTER TABLE almacen.vale ADD CONSTRAINT fk_vale_usuario FOREIGN KEY (id_usuario) REFERENCES administracion.usuario (id)');

        $this->addSql('
            CREATE OR REPLACE TRIGGER almacen.vale_estado_historial_insert AFTER INSERT ON almacen.vale FOR EACH ROW 
            INSERT INTO almacen.vale_estado_historial(id_vale,id_vale_estado) 
            values (NEW.id,NEW.id_vale_estado)
        ');

        $this->addSql('
            CREATE OR REPLACE TRIGGER almacen.vale_estado_historial_update AFTER UPDATE ON almacen.vale FOR EACH ROW
            INSERT INTO almacen.vale_estado_historial(id_vale,id_vale_estado) 
            values (NEW.id,NEW.id_vale_estado)
        ');

        $this->addSql('
            CREATE OR REPLACE TRIGGER almacen.consumo_delete AFTER DELETE ON almacen.consumo FOR EACH ROW
            DELETE FROM almacen.movimiento where id=OLD.id_movimiento
        ');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TRIGGER almacen.consumo_delete');
        $this->addSql('DROP TRIGGER almacen.vale_estado_historial_update');
        $this->addSql('DROP TRIGGER almacen.vale_estado_historial_insert');

        $this->addSql('ALTER TABLE almacen.vale DROP FOREIGN KEY fk_vale_usuario');
        $this->addSql('ALTER TABLE almacen.vale DROP COLUMN id_usuario');

        $this->addSql('ALTER TABLE almacen.movimiento MODIFY precio decimal(8, 2) NOT NULL');
        $this->addSql('ALTER TABLE almacen.existencia MODIFY precio decimal(8, 2) NOT NULL');
    }
}
