<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180803152500 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('
CREATE TABLE almacen.articulo (
  id          int(11) NOT NULL AUTO_INCREMENT, 
  codigo      varchar(50) NOT NULL UNIQUE, 
  nombre      varchar(255) NOT NULL, 
  descripcion varchar(500), 
  marca       varchar(255), 
  creado      timestamp NOT NULL, 
  CONSTRAINT articulo_pk 
    PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
');
        $this->addSql(' 
CREATE TABLE almacen.articulo_presentacion (
  id                  int(11) NOT NULL AUTO_INCREMENT, 
  id_articulo         int(11) NOT NULL, 
  id_unidad_medida    int(11) NOT NULL, 
  id_subunidad_medida int(11) NOT NULL, 
  cantidad            int(11) NOT NULL, 
  creado              timestamp NOT NULL, 
  CONSTRAINT articulo_presentacion_pk 
    PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
');
        $this->addSql(' 
CREATE TABLE academico.calificacion (
  id                    int(11) NOT NULL AUTO_INCREMENT, 
  id_estudiante_curso   int(11) NOT NULL, 
  id_evaluacion         int(11) NOT NULL, 
  id_calificacion_grupo int(11), 
  id_padre              int(11), 
  nota                  numeric(8, 2) NOT NULL, 
  fecha                 date NOT NULL, 
  permiso               tinyint(3) DEFAULT 0 NOT NULL, 
  creado                timestamp NOT NULL, 
  CONSTRAINT calificacion_pk 
    PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
');
        $this->addSql(' 
CREATE TABLE academico.calificacion_grupo (
  id            int(11) NOT NULL AUTO_INCREMENT, 
  id_grupo      int(11) NOT NULL, 
  id_evaluacion int(11) NOT NULL, 
  id_padre      int(11), 
  nota          numeric(8, 2) NOT NULL, 
  fecha         date NOT NULL, 
  creado        timestamp NOT NULL, 
  CONSTRAINT calificacion_grupo_pk 
    PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
');
        $this->addSql(' 
CREATE TABLE academico.calificacion_penalizacion (
  id              int(11) NOT NULL AUTO_INCREMENT, 
  id_calificacion int(11) NOT NULL, 
  id_penalizacion int(11) NOT NULL, 
  creado          timestamp NOT NULL, 
  CONSTRAINT calificacion_penalizacion_pk 
    PRIMARY KEY (id), 
  CONSTRAINT unique_calificacion_penalizacion 
    UNIQUE (id_calificacion, id_penalizacion)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
');
        $this->addSql(' 
CREATE TABLE academico.carrera (
  id          int(11) NOT NULL AUTO_INCREMENT, 
  codigo      varchar(50) NOT NULL UNIQUE, 
  nombre      varchar(255) NOT NULL, 
  descripcion varchar(500), 
  creado      timestamp NOT NULL, 
  CONSTRAINT carrera_pk 
    PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
');
        $this->addSql(' 
CREATE TABLE administracion.catalogo (
  id               int(11) NOT NULL AUTO_INCREMENT, 
  id_catalogo_tipo int(11) NOT NULL, 
  id_padre         int(11), 
  codigo           varchar(50) NOT NULL UNIQUE, 
  nombre           varchar(255) NOT NULL, 
  descripcion      varchar(500), 
  creado           timestamp NOT NULL, 
  CONSTRAINT catalogo_pk 
    PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci
comment=\'persona_atributo
telefono_movil
telefono_fijo
direccion_trabajo
direccion_procedencia
direccion_area\';
');
        $this->addSql(' 
CREATE TABLE administracion.catalogo_tipo (
  id          int(11) NOT NULL AUTO_INCREMENT, 
  id_padre    int(11), 
  codigo      varchar(50) NOT NULL UNIQUE, 
  nombre      varchar(255) NOT NULL, 
  descripcion varchar(500), 
  creado      timestamp NOT NULL, 
  CONSTRAINT catalogo_tipo_pk 
    PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci
comment=\'pais
departamento
municipio
institucion_tipo
telefono_tipo
direccion_tipo
persona_atributo_tipo
sexo
genero
estado_civil
profesion u oficio
nivel_educativo\';
');
        $this->addSql(' 
CREATE TABLE almacen.consumo (
  id            int(11) NOT NULL AUTO_INCREMENT, 
  id_vale       int(11) NOT NULL, 
  id_movimiento int(11) NOT NULL, 
  creado        timestamp NOT NULL, 
  CONSTRAINT consumo_pk 
    PRIMARY KEY (id), 
  CONSTRAINT unique_consumo 
    UNIQUE (id_vale, id_movimiento)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
');
        $this->addSql(' 
CREATE TABLE clinico.control_sellante (
  id                 int(11) NOT NULL AUTO_INCREMENT, 
  id_ficha           int(11) NOT NULL, 
  id_tratamiento     int(11) NOT NULL, 
  id_estado_sellante int(11) NOT NULL, 
  fecha_sellante     date NOT NULL, 
  fecha_control      date NOT NULL, 
  creado             timestamp NOT NULL, 
  CONSTRAINT control_sellante_pk 
    PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
');
        $this->addSql(' 
CREATE TABLE administracion.cuenta (
  id     int(11) NOT NULL AUTO_INCREMENT, 
  creado timestamp NOT NULL, 
  CONSTRAINT cuenta_pk 
    PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
');
        $this->addSql(' 
CREATE TABLE academico.curso (
  id          int(11) NOT NULL AUTO_INCREMENT, 
  codigo      varchar(10) NOT NULL UNIQUE, 
  nombre      varchar(255) NOT NULL, 
  descripcion varchar(500), 
  creado      timestamp NOT NULL, 
  CONSTRAINT curso_pk 
    PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
');
        $this->addSql(' 
CREATE TABLE administracion.direccion (
  id                   int(11) NOT NULL AUTO_INCREMENT, 
  id_cuenta            int(11) NOT NULL, 
  id_direccion_tipo    int(11) NOT NULL, 
  id_direccion_area    int(11), 
  id_division_politica int(11), 
  calle                varchar(255), 
  direccion            varchar(500), 
  creado               timestamp NOT NULL, 
  CONSTRAINT direccion_pk 
    PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
');
        $this->addSql(' 
CREATE TABLE administracion.email (
  id        int(11) NOT NULL AUTO_INCREMENT, 
  id_cuenta int(11) NOT NULL, 
  email     char(50) NOT NULL, 
  creado    timestamp NOT NULL, 
  CONSTRAINT email_pk 
    PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
');
        $this->addSql(' 
CREATE TABLE academico.estudiante (
  id         int(11) NOT NULL AUTO_INCREMENT, 
  id_persona int(11) NOT NULL, 
  carnet     varchar(8) NOT NULL UNIQUE, 
  creado     timestamp NOT NULL, 
  CONSTRAINT estudiante_pk 
    PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
');
        $this->addSql(' 
CREATE TABLE academico.estudiante_curso (
  id                         int(11) NOT NULL AUTO_INCREMENT, 
  id_estudiante_plan_estudio int(11) NOT NULL, 
  id_curso                   int(11) NOT NULL, 
  id_periodo_ciclo           int(11) NOT NULL, 
  id_estudiante_estado       int(11) NOT NULL, 
  creado                     timestamp NOT NULL, 
  CONSTRAINT estudiante_curso_pk 
    PRIMARY KEY (id), 
  CONSTRAINT unique_estudiante_curso 
    UNIQUE (id_estudiante_plan_estudio, id_curso, id_periodo_ciclo)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
');
        $this->addSql(' 
CREATE TABLE academico.estudiante_estado_bitacora (
  id                         int(11) NOT NULL AUTO_INCREMENT, 
  id_estudiante_plan_estudio int(11) NOT NULL, 
  id_estudiante_estado       int(11) NOT NULL, 
  creado                     timestamp NOT NULL, 
  CONSTRAINT estudiante_estado_bitacora_pk 
    PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
');
        $this->addSql(' 
CREATE TABLE academico.estudiante_estado (
  id          int(11) NOT NULL AUTO_INCREMENT, 
  tipo        varchar(25) NOT NULL, 
  codigo      varchar(50) NOT NULL UNIQUE, 
  nombre      varchar(255) NOT NULL, 
  descripcion varchar(500), 
  creado      timestamp NOT NULL, 
  CONSTRAINT estudiante_estado_pk 
    PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci
comment=\'estudiante
aprobado
reprobado
retiro
reingreso
en_resolucion\';
');
        $this->addSql(' 
CREATE TABLE academico.estudiante_plan_estudio (
  id                   int(11) NOT NULL AUTO_INCREMENT, 
  id_estudiante        int(11) NOT NULL, 
  id_plan_estudio      int(11) NOT NULL, 
  id_estudiante_estado int(11) NOT NULL, 
  cum                  numeric(8, 2) DEFAULT 0.0 NOT NULL, 
  creado               timestamp NOT NULL, 
  CONSTRAINT estudiante_plan_estudio_pk 
    PRIMARY KEY (id), 
  CONSTRAINT unique_estudiante_plan_estudio 
    UNIQUE (id_estudiante, id_plan_estudio)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
');
        $this->addSql(' 
CREATE TABLE rotacion.programa_calendario (
  id               int(11) NOT NULL AUTO_INCREMENT, 
  id_programa      int(11) NOT NULL, 
  id_periodo_ciclo int(11) NOT NULL, 
  id_grupo         int(11), 
  tipo             varchar(10) NOT NULL, 
  fecha            date NOT NULL, 
  descripcion      varchar(500), 
  creado           timestamp NOT NULL, 
  CONSTRAINT programa_calendario_pk
    PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
');
        $this->addSql(' 
CREATE TABLE academico.evaluacion (
  id                    int(11) NOT NULL AUTO_INCREMENT, 
  id_unidad_integracion int(11) NOT NULL, 
  id_evaluacion_tipo    int(11) NOT NULL, 
  id_padre              int(11), 
  nombre                varchar(255) NOT NULL, 
  descripcion           varchar(500), 
  ponderacion           real NOT NULL, 
  grupal                tinyint(3) DEFAULT 0 NOT NULL, 
  creado                timestamp NOT NULL, 
  CONSTRAINT evaluacion_pk 
    PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
');
        $this->addSql(' 
CREATE TABLE clinico.evaluacion_dental (
  id                         int(11) NOT NULL AUTO_INCREMENT, 
  id_ficha                   int(11) NOT NULL, 
  id_pieza_dental            int(11) NOT NULL, 
  id_pieza_dental_superficie int(11),
  id_pieza_dental_estado     int(11) NOT NULL, 
  id_codigo_tratamiento      int(11) NOT NULL,
  creado                     timestamp NOT NULL, 
  CONSTRAINT evaluacion_dental_pk 
    PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
');
        $this->addSql(' 
CREATE TABLE academico.evaluacion_tipo (
  id          int(11) NOT NULL AUTO_INCREMENT, 
  codigo      varchar(50) NOT NULL UNIQUE, 
  nombre      varchar(255) NOT NULL, 
  descripcion varchar(500), 
  creado      timestamp NOT NULL, 
  CONSTRAINT evaluacion_tipo_pk 
    PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
');
        $this->addSql(' 
CREATE TABLE almacen.existencia (
  id                       int(11) NOT NULL AUTO_INCREMENT, 
  id_almacen               int(11) NOT NULL, 
  id_articulo_presentacion int(11) NOT NULL, 
  cantidad                 int(11) NOT NULL, 
  precio                   decimal(8, 2) NOT NULL, 
  adquisicion              date NOT NULL, 
  vencimiento              date NOT NULL, 
  creado                   timestamp NOT NULL, 
  CONSTRAINT existencia_pk 
    PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
');
        $this->addSql(' 
CREATE TABLE clinico.expediente_clinico (
  id                         int(11) NOT NULL AUTO_INCREMENT, 
  id_persona                 int(11) NOT NULL, 
  id_expediente_clinico_tipo int(11) NOT NULL, 
  creado                     timestamp NOT NULL, 
  CONSTRAINT expediente_clinico_pk 
    PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
');
        $this->addSql(' 
CREATE TABLE clinico.ficha (
  id                           int(11) NOT NULL AUTO_INCREMENT, 
  id_cuenta                    int(11) NOT NULL, 
  id_expediente_clinico        int(11) NOT NULL, 
  id_referencia                int(11) NOT NULL, 
  id_frecuencia_cepillado      int(11) NOT NULL, 
  id_indice_riesgo_cariogenico int(11) NOT NULL, 
  fecha_control                date NOT NULL, 
  visita_odontologo            tinyint(3) NOT NULL, 
  trauma_dentoalveolar         tinyint(3) NOT NULL, 
  reacciones_adversas          tinyint(3) NOT NULL, 
  pieza                        varchar(255) NOT NULL, 
  tipo_reaccion                varchar(255) NOT NULL, 
  motivo_consulta              varchar(999) NOT NULL, 
  historia_enfermedad          varchar(999) NOT NULL, 
  total_ingesta_azucares       real NOT NULL, 
  total_placa_bacteriana       real NOT NULL, 
  total_indice_cpo_ceo         real NOT NULL, 
  observaciones                varchar(999) NOT NULL, 
  comentarios                  varchar(999) NOT NULL, 
  otras_condiciones            varchar(999) NOT NULL, 
  anotaciones_varias           varchar(999) NOT NULL, 
  creado                       timestamp NOT NULL, 
  actualizado                  timestamp DEFAULT CURRENT_TIMESTAMP NOT NULL, 
  CONSTRAINT id 
    PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
');
        $this->addSql(' 
CREATE TABLE clinico.ficha_catalogo (
  id                     int(11) NOT NULL AUTO_INCREMENT, 
  id_padre               int(11), 
  id_ficha_catalogo_tipo int(11) NOT NULL, 
  codigo                 varchar(50) NOT NULL UNIQUE, 
  nombre                 varchar(255) NOT NULL, 
  descripcion            varchar(500), 
  creado                 timestamp NOT NULL, 
  CONSTRAINT ficha_catalogo_pk 
    PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
');
        $this->addSql(' 
CREATE TABLE clinico.ficha_catalogo_tipo (
  id          int(11) NOT NULL AUTO_INCREMENT, 
  id_padre    int(11), 
  codigo      varchar(50) NOT NULL UNIQUE, 
  nombre      varchar(255) NOT NULL, 
  descripcion varchar(500), 
  creado      timestamp NOT NULL, 
  CONSTRAINT ficha_catalogo_tipo_pk 
    PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci
comment=\'lesion_tipo\';
');
        $this->addSql(' 
CREATE TABLE clinico.ficha_evaluacion_sistemica (
  id                      int(11) NOT NULL AUTO_INCREMENT, 
  id_ficha                int(11) NOT NULL, 
  id_evaluacion_sistemica int(11) NOT NULL, 
  creado                  timestamp NOT NULL, 
  CONSTRAINT ficha_evaluacion_sistemica_pk 
    PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
');
        $this->addSql(' 
CREATE TABLE clinico.ficha_habito_bucal (
  id              int(11) NOT NULL AUTO_INCREMENT, 
  id_ficha        int(11) NOT NULL, 
  id_habito_bucal int(11) NOT NULL, 
  creado          timestamp NOT NULL, 
  CONSTRAINT ficha_habitos_pk 
    PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
');
        $this->addSql(' 
CREATE TABLE clinico.ficha_higiene_bucal (
  id               int(11) NOT NULL AUTO_INCREMENT, 
  id_ficha         int(11) NOT NULL, 
  id_higiene_bucal int(11) NOT NULL, 
  creado           timestamp NOT NULL, 
  CONSTRAINT ficha_higiene_pk 
    PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
');
        $this->addSql(' 
CREATE TABLE clinico.ficha_tratamiento (
  id                int(11) NOT NULL AUTO_INCREMENT, 
  id_ficha          int(11) NOT NULL, 
  id_tratamiento    int(11) NOT NULL, 
  id_pieza_dental   int(11) NOT NULL, 
  cantidad          int(11) NOT NULL, 
  acciones_realizar varchar(500) NOT NULL, 
  creado            timestamp NOT NULL, 
  CONSTRAINT ficha_tratamiento_pk 
    PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
');
        $this->addSql(' 
CREATE TABLE rotacion.grupo (
  id               int(11) NOT NULL AUTO_INCREMENT, 
  id_padre         int(11), 
  id_periodo_ciclo int(11) NOT NULL, 
  nombre           varchar(5) NOT NULL, 
  rotable          tinyint(3) DEFAULT 0 NOT NULL, 
  creado           timestamp NOT NULL, 
  CONSTRAINT grupo_pk 
    PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
');
        $this->addSql(' 
CREATE TABLE rotacion.grupo_estudiante (
  id                  int(11) NOT NULL AUTO_INCREMENT, 
  id_estudiante_curso int(11) NOT NULL, 
  id_grupo            int(11) NOT NULL, 
  creado              timestamp NOT NULL, 
  CONSTRAINT grupo_estudiante_pk 
    PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
');
        $this->addSql(' 
CREATE TABLE clinico.ingesta_azucar (
  id                         int(11) NOT NULL AUTO_INCREMENT, 
  id_ficha                   int(11) NOT NULL, 
  id_ingesta_azucar_criterio int(11) NOT NULL, 
  creado                     timestamp NOT NULL, 
  CONSTRAINT ingesta_azucar_pk 
    PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
');
        $this->addSql(' 
CREATE TABLE administracion.institucion (
  id          int(11) NOT NULL AUTO_INCREMENT, 
  id_cuenta   int(11) NOT NULL, 
  id_padre    int(11), 
  codigo      varchar(50) NOT NULL UNIQUE, 
  nombre      varchar(255) NOT NULL, 
  descripcion varchar(500), 
  creado      timestamp NOT NULL, 
  CONSTRAINT institucion_pk 
    PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
');
        $this->addSql(' 
CREATE TABLE administracion.institucion_tipo_asociado (
  id                  int(11) NOT NULL AUTO_INCREMENT, 
  id_institucion      int(11) NOT NULL, 
  id_institucion_tipo int(11) NOT NULL, 
  creado              timestamp NOT NULL, 
  CONSTRAINT institucion_tipo_asociado_pk 
    PRIMARY KEY (id), 
  CONSTRAINT unique_institucion_tipo_asociado 
    UNIQUE (id_institucion, id_institucion_tipo)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
');
        $this->addSql(' 
CREATE TABLE almacen.almacen (
  id             int(11) NOT NULL AUTO_INCREMENT, 
  id_institucion int(11) NOT NULL, 
  codigo         varchar(50) NOT NULL, 
  nombre         varchar(255) NOT NULL, 
  descripcion    varchar(500), 
  creado         timestamp NOT NULL, 
  CONSTRAINT almacen_pk 
    PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
');
        $this->addSql(' 
CREATE TABLE clinico.lesion (
  id                   int(11) NOT NULL AUTO_INCREMENT, 
  id_ficha             int(11) NOT NULL, 
  id_pieza_dental      int(11) NOT NULL, 
  id_lesion_valor_tipo int(11) NOT NULL, 
  creado               timestamp NOT NULL, 
  CONSTRAINT lesion_pk 
    PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
');
        $this->addSql(' 
CREATE TABLE almacen.movimiento (
  id                       int(11) NOT NULL AUTO_INCREMENT, 
  id_almacen               int(11) NOT NULL, 
  id_articulo_presentacion int(11) NOT NULL, 
  id_usuario                int(11) NOT NULL, 
  concepto                 varchar(255) NOT NULL, 
  precio                   decimal(8, 2) NOT NULL, 
  cantidad                 int(11) NOT NULL, 
  fecha                    date NOT NULL, 
  confirmado               tinyint(3) NOT NULL, 
  creado                   timestamp NOT NULL, 
  CONSTRAINT movimiento_pk 
    PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci
comment=\'http://saitenlinea.com/wiki/Inventario/Tipos_de_Movimientos\';
');
        $this->addSql(' 
CREATE TABLE clinico.pago (
  id                    int(11) NOT NULL AUTO_INCREMENT, 
  id_expediente_clinico int(11) NOT NULL, 
  numero_recibo         int(11) NOT NULL, 
  concepto              varchar(255) NOT NULL, 
  costo                 real NOT NULL, 
  abono                 real NOT NULL, 
  saldo                 real NOT NULL, 
  fecha                 date NOT NULL, 
  creado                timestamp NOT NULL, 
  CONSTRAINT pago_pk 
    PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
');
        $this->addSql(' 
CREATE TABLE academico.penalizacion (
  id          int(11) NOT NULL AUTO_INCREMENT, 
  codigo      varchar(50) NOT NULL UNIQUE, 
  nombre      varchar(255) NOT NULL, 
  descripcion varchar(500), 
  puntos      int(11) NOT NULL, 
  creado      timestamp NOT NULL, 
  CONSTRAINT penalizacion_pk 
    PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
');
        $this->addSql(' 
CREATE TABLE academico.periodo (
  id     int(11) NOT NULL AUTO_INCREMENT, 
  nombre varchar(255) NOT NULL, 
  inicio date NOT NULL, 
  fin    date NOT NULL, 
  activo tinyint(3) DEFAULT 1 NOT NULL, 
  creado timestamp NOT NULL, 
  CONSTRAINT periodo_pk 
    PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
');
        $this->addSql(' 
CREATE TABLE academico.periodo_ciclo (
  id         int(11) NOT NULL AUTO_INCREMENT, 
  id_periodo int(11) NOT NULL UNIQUE, 
  tipo       varchar(5) NOT NULL, 
  creado     timestamp NOT NULL, 
  CONSTRAINT periodo_ciclo_pk 
    PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
');
        $this->addSql(' 
CREATE TABLE administracion.permiso (
  id          int(11) NOT NULL AUTO_INCREMENT, 
  codigo      varchar(50) NOT NULL UNIQUE, 
  nombre      varchar(255) NOT NULL, 
  descripcion varchar(500), 
  creado      timestamp NOT NULL, 
  CONSTRAINT permiso_pk 
    PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
');
        $this->addSql(' 
CREATE TABLE administracion.persona (
  id         int(11) NOT NULL AUTO_INCREMENT, 
  id_cuenta  int(11) NOT NULL, 
  nombre     varchar(50) NOT NULL, 
  apellido   varchar(50), 
  dui        varchar(10) UNIQUE, 
  nit        varchar(17) UNIQUE, 
  nacimiento date, 
  creado     timestamp NOT NULL, 
  CONSTRAINT persona_pk 
    PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
');
        $this->addSql(' 
CREATE TABLE administracion.persona_atributo (
  id          int(11) NOT NULL AUTO_INCREMENT, 
  id_persona  int(11) NOT NULL, 
  id_atributo int(11) NOT NULL, 
  creado      timestamp NOT NULL, 
  CONSTRAINT persona_atributo_pk 
    PRIMARY KEY (id), 
  CONSTRAINT unique_persona_atributo 
    UNIQUE (id_persona, id_atributo)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
');
        $this->addSql(' 
CREATE TABLE clinico.placa_bacteriana (
  id                         int(11) NOT NULL AUTO_INCREMENT, 
  id_ficha                   int(11) NOT NULL, 
  id_placa_bacteriana_indice int(11) NOT NULL, 
  valor                      int(11), 
  creado                     timestamp NOT NULL, 
  CONSTRAINT placa_bacteriana_pk 
    PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
');
        $this->addSql(' 
CREATE TABLE academico.plan_estudio (
  id         int(11) NOT NULL AUTO_INCREMENT, 
  id_carrera int(11) NOT NULL, 
  anio       int(11) NOT NULL, 
  activo     tinyint(3) NOT NULL, 
  creado     timestamp NOT NULL, 
  CONSTRAINT plan_estudio_pk 
    PRIMARY KEY (id), 
  CONSTRAINT unique_plan_estudio 
    UNIQUE (id_carrera, anio)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
');
        $this->addSql(' 
CREATE TABLE academico.plan_estudio_ciclo (
  id     int(11) NOT NULL AUTO_INCREMENT, 
  numero int(11) NOT NULL UNIQUE, 
  romano varchar(5) NOT NULL UNIQUE, 
  creado timestamp NOT NULL, 
  CONSTRAINT plan_estudio_ciclo_pk 
    PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci
comment=\'I
II
III
IV\';
');
        $this->addSql(' 
CREATE TABLE academico.plan_estudio_curso (
  id                    int(11) NOT NULL AUTO_INCREMENT, 
  id_curso              int(11) NOT NULL, 
  id_plan_estudio       int(11) NOT NULL, 
  id_plan_estudio_ciclo int(11) NOT NULL, 
  creado                timestamp NOT NULL, 
  CONSTRAINT plan_estudio_curso_pk 
    PRIMARY KEY (id), 
  CONSTRAINT unique_plan_estudio_curso 
    UNIQUE (id_curso, id_plan_estudio)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
');
        $this->addSql(' 
CREATE TABLE academico.curso_programa (
  id                    int(11) NOT NULL AUTO_INCREMENT, 
  id_programa           int(11) NOT NULL, 
  id_plan_estudio_curso int(11) NOT NULL, 
  correlativo           int(11) NULL, 
  creado                timestamp NOT NULL, 
  CONSTRAINT curso_programa_pk 
    PRIMARY KEY (id), 
  CONSTRAINT unique_curso_programa 
    UNIQUE (id_programa, id_plan_estudio_curso)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
');
        $this->addSql(' 
CREATE TABLE clinico.plan_preventivo (
  id                      int(11) NOT NULL AUTO_INCREMENT, 
  id_ficha                int(11) NOT NULL, 
  id_tratamiento          int(11) NOT NULL, 
  id_plan_preventivo_area int(11) NOT NULL, 
  creado                  timestamp NOT NULL, 
  CONSTRAINT plan_preventivo_pk 
    PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
');
        $this->addSql(' 
CREATE TABLE rotacion.planificacion (
  id               int(11) NOT NULL AUTO_INCREMENT, 
  id_periodo_ciclo int(11) NOT NULL, 
  confirmado       tinyint(3) NOT NULL, 
  creado           timestamp NOT NULL, 
  CONSTRAINT planificacion_pk 
    PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
');
        $this->addSql(' 
CREATE TABLE rotacion.programa (
  id          int(11) NOT NULL AUTO_INCREMENT, 
  codigo      varchar(50) NOT NULL UNIQUE, 
  nombre      varchar(255) NOT NULL, 
  descripcion varchar(500), 
  creado      timestamp NOT NULL, 
  CONSTRAINT programa_pk 
    PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
');
        $this->addSql(' 
CREATE TABLE rotacion.programa_institucion (
  id                  int(11) NOT NULL AUTO_INCREMENT, 
  id_programa         int(11) NOT NULL, 
  id_institucion      int(11) NOT NULL, 
  cantidad_estudiante int(11) NOT NULL, 
  activo              tinyint(3) NOT NULL, 
  creado              timestamp NOT NULL, 
  CONSTRAINT programa_institucion_pk 
    PRIMARY KEY (id), 
  CONSTRAINT unique_programa_institucion 
    UNIQUE (id_programa, id_institucion)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
');
        $this->addSql(' 
CREATE TABLE administracion.rol (
  id          int(11) NOT NULL AUTO_INCREMENT, 
  codigo      varchar(50) NOT NULL UNIQUE, 
  nombre      varchar(255) NOT NULL, 
  descripcion varchar(500), 
  creado      timestamp NOT NULL, 
  CONSTRAINT rol_pk 
    PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
');
        $this->addSql(' 
CREATE TABLE administracion.rol_permiso (
  id         int(11) NOT NULL AUTO_INCREMENT, 
  id_rol     int(11) NOT NULL, 
  id_permiso int(11) NOT NULL, 
  creado     timestamp NOT NULL, 
  CONSTRAINT rol_permiso_pk 
    PRIMARY KEY (id), 
  CONSTRAINT unique_rol_permiso 
    UNIQUE (id_rol, id_permiso)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
');
        $this->addSql(' 
CREATE TABLE rotacion.rotacion (
  id               int(11) NOT NULL AUTO_INCREMENT, 
  id_padre         int(11), 
  id_planificacion int(11) NOT NULL, 
  id_grupo         int(11) NOT NULL, 
  numero           int(11) NOT NULL, 
  inicio           date NOT NULL, 
  fin              date NOT NULL, 
  creado           timestamp NOT NULL, 
  CONSTRAINT rotacion_pk 
    PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
');
        $this->addSql(' 
CREATE TABLE rotacion.rotacion_estudiante (
  id                      int(11) NOT NULL AUTO_INCREMENT, 
  id_rotacion             int(11) NOT NULL, 
  id_programa_institucion int(11) NOT NULL, 
  id_grupo_estudiante     int(11) NOT NULL, 
  creado                  timestamp NOT NULL, 
  CONSTRAINT rotacion_estudiante_pk 
    PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
');
        $this->addSql(' 
CREATE TABLE administracion.telefono (
  id               int(11) NOT NULL AUTO_INCREMENT, 
  id_cuenta        int(11) NOT NULL, 
  id_telefono_tipo int(11) NOT NULL, 
  numero           varchar(25) NOT NULL, 
  creado           timestamp NOT NULL, 
  CONSTRAINT telefono_pk 
    PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
');
        $this->addSql(' 
CREATE TABLE clinico.tratamiento (
  id                  int(11) NOT NULL AUTO_INCREMENT, 
  id_tratamiento_area int(11) NOT NULL, 
  codigo              varchar(50) NOT NULL,
  nombre              varchar(255) NOT NULL, 
  descripcion         varchar(500), 
  precio              decimal(8, 2) NOT NULL, 
  creado              timestamp NOT NULL, 
  CONSTRAINT tratamiento_pk 
    PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
');
        $this->addSql(' 
CREATE TABLE clinico.tratamiento_prioritario (
  id             int(11) NOT NULL AUTO_INCREMENT, 
  id_ficha       int(11) NOT NULL, 
  id_tratamiento int(11) NOT NULL, 
  creado         timestamp NOT NULL, 
  CONSTRAINT tratamiento_prioritario_pk 
    PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
');
        $this->addSql(' 
CREATE TABLE academico.unidad_integracion (
  id                    int(11) NOT NULL AUTO_INCREMENT, 
  id_plan_estudio_curso int(11) NOT NULL, 
  id_periodo_ciclo      int(11) NOT NULL, 
  id_programa           int(11), 
  nombre                varchar(255) NOT NULL, 
  ponderacion           real NOT NULL, 
  creado                timestamp NOT NULL, 
  CONSTRAINT unidad_integracion_pk 
    PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
');
        $this->addSql(' 
CREATE TABLE almacen.unidad_medida (
  id          int(11) NOT NULL AUTO_INCREMENT, 
  codigo      varchar(50) NOT NULL UNIQUE, 
  nombre      varchar(100) NOT NULL, 
  subunidades text, 
  creado      timestamp NOT NULL, 
  CONSTRAINT unidad_medida_pk 
    PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
');
        $this->addSql(' 
CREATE TABLE administracion.usuario (
  id         int(11) NOT NULL AUTO_INCREMENT, 
  id_cuenta  int(11) NOT NULL, 
  username   varchar(50) NOT NULL UNIQUE, 
  email      varchar(50) NOT NULL UNIQUE, 
  password   varchar(255) NOT NULL, 
  enabled    tinyint(3) DEFAULT 1 NOT NULL, 
  last_login datetime NULL, 
  creado     timestamp NOT NULL,
  CONSTRAINT usuario_pk 
    PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
');
        $this->addSql(' 
CREATE TABLE administracion.usuario_rol (
  id         int(11) NOT NULL AUTO_INCREMENT, 
  id_usuario int(11) NOT NULL, 
  id_rol     int(11) NOT NULL, 
  creado     timestamp NOT NULL, 
  CONSTRAINT usuario_rol_pk 
    PRIMARY KEY (id), 
  CONSTRAINT unique_usuario_rol 
    UNIQUE (id_usuario, id_rol)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
');
        $this->addSql(' 
CREATE TABLE almacen.vale (
  id             int(11) NOT NULL AUTO_INCREMENT,
  id_vale_estado int(11) NOT NULL,
  descripcion    varchar(500),
  datos          text,
  creado         timestamp NOT NULL,
  CONSTRAINT vale_pk 
    PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci
comment=\'No se podrá registrar vales si no se cuenta con fichas ni expedientes\';
');
        $this->addSql(' 
CREATE TABLE almacen.vale_estado_historial (
  id             int(11) NOT NULL AUTO_INCREMENT,
  id_vale        int(11) NOT NULL,
  id_vale_estado int(11) NOT NULL,
  descripcion    varchar(500),
  creado         timestamp NOT NULL,
  CONSTRAINT vale_estado_historial_pk 
    PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
');
        $this->addSql(' 
CREATE TABLE almacen.vale_estado (
  id          int(11) NOT NULL AUTO_INCREMENT, 
  codigo      varchar(50) NOT NULL UNIQUE, 
  nombre      varchar(255) NOT NULL, 
  descripcion varchar(500), 
  creado      timestamp NOT NULL, 
  CONSTRAINT vale_estado_pk 
    PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
');
        $this->addSql(' 
CREATE TABLE clinico.codigo_tratamiento (
  id                     int(11) NOT NULL AUTO_INCREMENT,
  id_codigo_restauracion int(11) NOT NULL,
  id_codigo_caries       int(11) NOT NULL,
  id_tratamiento         int(11) NOT NULL,
  creado                 timestamp NOT NULL, 
  CONSTRAINT codigo_tratamiento_pk 
    PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
');
        $this->addSql(' 
CREATE TABLE almacen.vale_ficha_tratamiento (
  id                   int(11) NOT NULL AUTO_INCREMENT, 
  id_vale              int(11) NOT NULL, 
  id_ficha_tratamiento int(11) NOT NULL, 
  creado               timestamp NOT NULL, 
  PRIMARY KEY (id), 
  CONSTRAINT unique_vale_ficha_tratamiento 
    UNIQUE (id_vale, id_ficha_tratamiento)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
');
        $this->addSql(' 
CREATE TABLE almacen.tratamiento_articulo_presentacion (
  id                       int(11) NOT NULL AUTO_INCREMENT, 
  id_tratamiento           int(11) NOT NULL, 
  id_articulo_presentacion int(11) NOT NULL, 
  cantidad                 int(11) NOT NULL, 
  creado                   timestamp NOT NULL, 
  CONSTRAINT pk_tratamiento_articulo_presentacion 
    PRIMARY KEY (id), 
  CONSTRAINT unique_tratamiento_articulo_presentacion 
    UNIQUE (id_tratamiento, id_articulo_presentacion)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
');

        $this->addSql('ALTER TABLE almacen.articulo_presentacion ADD CONSTRAINT fk_articulo FOREIGN KEY (id_articulo) REFERENCES almacen.articulo (id);');
        $this->addSql('ALTER TABLE almacen.articulo_presentacion ADD CONSTRAINT fk_unidad_medida FOREIGN KEY (id_unidad_medida) REFERENCES almacen.unidad_medida (id);');
        $this->addSql('ALTER TABLE almacen.articulo_presentacion ADD CONSTRAINT fk_sub_unidad_medida FOREIGN KEY (id_subunidad_medida) REFERENCES almacen.unidad_medida (id);');
        $this->addSql('ALTER TABLE academico.calificacion ADD CONSTRAINT fk_calificacion_estudiante_curso FOREIGN KEY (id_estudiante_curso) REFERENCES academico.estudiante_curso (id);');
        $this->addSql('ALTER TABLE academico.calificacion ADD CONSTRAINT fk_calificacion_evaluacion FOREIGN KEY (id_evaluacion) REFERENCES academico.evaluacion (id);');
        $this->addSql('ALTER TABLE academico.calificacion ADD CONSTRAINT fk_calificacion_padre FOREIGN KEY (id_padre) REFERENCES academico.calificacion (id) ON DELETE SET NULL;');
        $this->addSql('ALTER TABLE academico.calificacion_grupo ADD CONSTRAINT fk_grupo_calificacion FOREIGN KEY (id_grupo) REFERENCES rotacion.grupo (id);');
        $this->addSql('ALTER TABLE academico.calificacion_grupo ADD CONSTRAINT fk_calificacion_grupo_evaluacion FOREIGN KEY (id_evaluacion) REFERENCES academico.evaluacion (id);');
        $this->addSql('ALTER TABLE academico.calificacion_grupo ADD CONSTRAINT fk_calificacion_grupo_padre FOREIGN KEY (id_padre) REFERENCES academico.calificacion_grupo (id) ON DELETE SET NULL;');
        $this->addSql('ALTER TABLE academico.calificacion_penalizacion ADD CONSTRAINT fk_calificacion_penalizacion FOREIGN KEY (id_calificacion) REFERENCES academico.calificacion (id);');
        $this->addSql('ALTER TABLE academico.calificacion_penalizacion ADD CONSTRAINT fk_penalizacion_calificacion FOREIGN KEY (id_penalizacion) REFERENCES academico.penalizacion (id);');
        $this->addSql('ALTER TABLE administracion.catalogo ADD CONSTRAINT fk_catalogo_padre FOREIGN KEY (id_padre) REFERENCES administracion.catalogo (id) ON DELETE SET NULL;');
        $this->addSql('ALTER TABLE administracion.catalogo ADD CONSTRAINT fk_catalogo_tipo FOREIGN KEY (id_catalogo_tipo) REFERENCES administracion.catalogo_tipo (id);');
        $this->addSql('ALTER TABLE administracion.catalogo_tipo ADD CONSTRAINT fk_catalogo_tipo_padre FOREIGN KEY (id_padre) REFERENCES administracion.catalogo_tipo (id) ON DELETE SET NULL;');
        $this->addSql('ALTER TABLE almacen.consumo ADD CONSTRAINT fk_consumo_vale FOREIGN KEY (id_vale) REFERENCES almacen.vale (id);');
        $this->addSql('ALTER TABLE almacen.consumo ADD CONSTRAINT fk_consumo_movimiento FOREIGN KEY (id_movimiento) REFERENCES almacen.movimiento (id);');
        $this->addSql('ALTER TABLE clinico.control_sellante ADD CONSTRAINT fk_control_sellante_ficha FOREIGN KEY (id_ficha) REFERENCES clinico.ficha (id);');
        $this->addSql('ALTER TABLE clinico.control_sellante ADD CONSTRAINT fk_control_sellante_tratamiento FOREIGN KEY (id_tratamiento) REFERENCES clinico.tratamiento (id);');
        $this->addSql('ALTER TABLE clinico.control_sellante ADD CONSTRAINT fk_sellante_estado FOREIGN KEY (id_estado_sellante) REFERENCES clinico.ficha_catalogo (id);');
        $this->addSql('ALTER TABLE administracion.direccion ADD CONSTRAINT fk_direccion_cuenta FOREIGN KEY (id_cuenta) REFERENCES administracion.cuenta (id);');
        $this->addSql('ALTER TABLE administracion.direccion ADD CONSTRAINT fk_direccion_tipo FOREIGN KEY (id_direccion_tipo) REFERENCES administracion.catalogo (id);');
        $this->addSql('ALTER TABLE administracion.direccion ADD CONSTRAINT fk_division_politica FOREIGN KEY (id_division_politica) REFERENCES administracion.catalogo (id);');
        $this->addSql('ALTER TABLE administracion.direccion ADD CONSTRAINT fk_direccion_area FOREIGN KEY (id_direccion_area) REFERENCES administracion.catalogo (id);');
        $this->addSql('ALTER TABLE administracion.email ADD CONSTRAINT fk_email_cuenta FOREIGN KEY (id_cuenta) REFERENCES administracion.cuenta (id);');
        $this->addSql('ALTER TABLE academico.estudiante ADD CONSTRAINT fk_estudiante_persona FOREIGN KEY (id_persona) REFERENCES administracion.persona (id);');
        $this->addSql('ALTER TABLE academico.estudiante_curso ADD CONSTRAINT fk_estudiante_curso_plan_estudio FOREIGN KEY (id_estudiante_plan_estudio) REFERENCES academico.estudiante_plan_estudio (id);');
        $this->addSql('ALTER TABLE academico.estudiante_curso ADD CONSTRAINT fk_estudiante_curso FOREIGN KEY (id_curso) REFERENCES academico.curso (id);');
        $this->addSql('ALTER TABLE academico.estudiante_curso ADD CONSTRAINT fk_estudiante_curso_periodo_ciclo FOREIGN KEY (id_periodo_ciclo) REFERENCES academico.periodo_ciclo (id);');
        $this->addSql('ALTER TABLE academico.estudiante_estado_bitacora ADD CONSTRAINT fk_estudiante_estado_bitacora_plan_estudio FOREIGN KEY (id_estudiante_plan_estudio) REFERENCES academico.estudiante_plan_estudio (id);');
        $this->addSql('ALTER TABLE academico.estudiante_estado_bitacora ADD CONSTRAINT fk_estudiante_estado_bitacora FOREIGN KEY (id_estudiante_estado) REFERENCES academico.estudiante_estado (id);');
        $this->addSql('ALTER TABLE academico.estudiante_plan_estudio ADD CONSTRAINT fk_estudiante_plan_estudio FOREIGN KEY (id_estudiante) REFERENCES academico.estudiante (id);');
        $this->addSql('ALTER TABLE academico.estudiante_plan_estudio ADD CONSTRAINT fk_plan_estudio_estudiante FOREIGN KEY (id_plan_estudio) REFERENCES academico.plan_estudio (id);');
        $this->addSql('ALTER TABLE academico.evaluacion ADD CONSTRAINT fk_evaluacion_unidad_integracion FOREIGN KEY (id_unidad_integracion) REFERENCES academico.unidad_integracion (id);');
        $this->addSql('ALTER TABLE academico.evaluacion ADD CONSTRAINT fk_evaluacion_tipo FOREIGN KEY (id_evaluacion_tipo) REFERENCES academico.evaluacion_tipo (id);');
        $this->addSql('ALTER TABLE academico.evaluacion ADD CONSTRAINT fk_evaluacion_padre FOREIGN KEY (id_padre) REFERENCES academico.evaluacion (id) ON DELETE SET NULL;');
        $this->addSql('ALTER TABLE clinico.evaluacion_dental ADD CONSTRAINT fk_evaluacion_dental_ficha FOREIGN KEY (id_ficha) REFERENCES clinico.ficha (id);');
        $this->addSql('ALTER TABLE clinico.evaluacion_dental ADD CONSTRAINT fk_evaluacion_pieza_dental FOREIGN KEY (id_pieza_dental) REFERENCES clinico.ficha_catalogo (id);');
        $this->addSql('ALTER TABLE clinico.evaluacion_dental ADD CONSTRAINT fk_evaluacion_pieza_dental_superficie FOREIGN KEY (id_pieza_dental_superficie) REFERENCES clinico.ficha_catalogo (id);');
        $this->addSql('ALTER TABLE clinico.evaluacion_dental ADD CONSTRAINT fk_evaluacion_pieza_dental_estado FOREIGN KEY (id_pieza_dental_estado) REFERENCES clinico.ficha_catalogo (id);');
        $this->addSql('ALTER TABLE almacen.existencia ADD CONSTRAINT fk_existencia_almacen FOREIGN KEY (id_almacen) REFERENCES almacen.almacen (id);');
        $this->addSql('ALTER TABLE almacen.existencia ADD CONSTRAINT fk_existencia_articulo_presentacion FOREIGN KEY (id_articulo_presentacion) REFERENCES almacen.articulo_presentacion (id);');
        $this->addSql('ALTER TABLE clinico.expediente_clinico ADD CONSTRAINT fk_expediente_clinico_persona FOREIGN KEY (id_persona) REFERENCES administracion.persona (id);');
        $this->addSql('ALTER TABLE clinico.expediente_clinico ADD CONSTRAINT fk_expediente_clinico_tipo FOREIGN KEY (id_expediente_clinico_tipo) REFERENCES clinico.ficha_catalogo (id);');
        $this->addSql('ALTER TABLE clinico.ficha ADD CONSTRAINT fk_ficha_cuenta FOREIGN KEY (id_cuenta) REFERENCES administracion.cuenta (id);');
        $this->addSql('ALTER TABLE clinico.ficha ADD CONSTRAINT fk_ficha_expediente_clinico FOREIGN KEY (id_expediente_clinico) REFERENCES clinico.expediente_clinico (id);');
        $this->addSql('ALTER TABLE clinico.ficha ADD CONSTRAINT fk_referencia FOREIGN KEY (id_referencia) REFERENCES administracion.catalogo (id);');
        $this->addSql('ALTER TABLE clinico.ficha ADD CONSTRAINT fk_frecuencia_cepillado FOREIGN KEY (id_frecuencia_cepillado) REFERENCES clinico.ficha_catalogo (id);');
        $this->addSql('ALTER TABLE clinico.ficha ADD CONSTRAINT fk_indice_riesgo_cariogenico FOREIGN KEY (id_indice_riesgo_cariogenico) REFERENCES clinico.ficha_catalogo (id);');
        $this->addSql('ALTER TABLE clinico.ficha_catalogo ADD CONSTRAINT fk_ficha_catalogo_padre FOREIGN KEY (id_padre) REFERENCES clinico.ficha_catalogo (id) ON DELETE SET NULL;');
        $this->addSql('ALTER TABLE clinico.ficha_catalogo ADD CONSTRAINT fk_ficha_catalogo_tipo FOREIGN KEY (id_ficha_catalogo_tipo) REFERENCES clinico.ficha_catalogo_tipo (id);');
        $this->addSql('ALTER TABLE clinico.ficha_catalogo_tipo ADD CONSTRAINT fk_ficha_catalogo_tipo_padre FOREIGN KEY (id_padre) REFERENCES clinico.ficha_catalogo_tipo (id) ON DELETE SET NULL;');
        $this->addSql('ALTER TABLE clinico.ficha_evaluacion_sistemica ADD CONSTRAINT fk_ficha_evaluacion_sistemica FOREIGN KEY (id_ficha) REFERENCES clinico.ficha (id);');
        $this->addSql('ALTER TABLE clinico.ficha_evaluacion_sistemica ADD CONSTRAINT fk_evaluacion_sistema FOREIGN KEY (id_evaluacion_sistemica) REFERENCES clinico.ficha_catalogo (id);');
        $this->addSql('ALTER TABLE clinico.ficha_habito_bucal ADD CONSTRAINT fk_ficha_habito_bucal FOREIGN KEY (id_ficha) REFERENCES clinico.ficha (id);');
        $this->addSql('ALTER TABLE clinico.ficha_habito_bucal ADD CONSTRAINT fk_habito_bucal FOREIGN KEY (id_habito_bucal) REFERENCES clinico.ficha_catalogo (id);');
        $this->addSql('ALTER TABLE clinico.ficha_higiene_bucal ADD CONSTRAINT fk_ficha_higiene_bucal FOREIGN KEY (id_ficha) REFERENCES clinico.ficha (id);');
        $this->addSql('ALTER TABLE clinico.ficha_higiene_bucal ADD CONSTRAINT fk_higiene_bucal FOREIGN KEY (id_higiene_bucal) REFERENCES clinico.ficha_catalogo (id);');
        $this->addSql('ALTER TABLE clinico.ficha_tratamiento ADD CONSTRAINT fk_ficha_tratamiento FOREIGN KEY (id_ficha) REFERENCES clinico.ficha (id);');
        $this->addSql('ALTER TABLE clinico.ficha_tratamiento ADD CONSTRAINT fk_tratamiento_ficha FOREIGN KEY (id_tratamiento) REFERENCES clinico.tratamiento (id);');
        $this->addSql('ALTER TABLE clinico.ficha_tratamiento ADD CONSTRAINT fk_ficha_tratamiento_pieza_dental FOREIGN KEY (id_pieza_dental) REFERENCES clinico.ficha_catalogo (id);');
        $this->addSql('ALTER TABLE rotacion.grupo ADD CONSTRAINT fk_grupo_padre FOREIGN KEY (id_padre) REFERENCES rotacion.grupo (id) ON DELETE SET NULL;');
        $this->addSql('ALTER TABLE rotacion.grupo ADD CONSTRAINT fk_grupo_periodo_ciclo FOREIGN KEY (id_periodo_ciclo) REFERENCES academico.periodo_ciclo (id);');
        $this->addSql('ALTER TABLE rotacion.grupo_estudiante ADD CONSTRAINT fk_grupo_estudiante_curso FOREIGN KEY (id_estudiante_curso) REFERENCES academico.estudiante_curso (id);');
        $this->addSql('ALTER TABLE rotacion.grupo_estudiante ADD CONSTRAINT fk_grupo_estudiante FOREIGN KEY (id_grupo) REFERENCES rotacion.grupo (id);');
        $this->addSql('ALTER TABLE clinico.ingesta_azucar ADD CONSTRAINT fk_ingesta_azucar_ficha FOREIGN KEY (id_ficha) REFERENCES clinico.ficha (id);');
        $this->addSql('ALTER TABLE clinico.ingesta_azucar ADD CONSTRAINT fk_ingesta_azucar_criterio FOREIGN KEY (id_ingesta_azucar_criterio) REFERENCES clinico.ficha_catalogo (id);');
        $this->addSql('ALTER TABLE administracion.institucion ADD CONSTRAINT fk_institucion_cuenta FOREIGN KEY (id_cuenta) REFERENCES administracion.cuenta (id);');
        $this->addSql('ALTER TABLE administracion.institucion ADD CONSTRAINT fk_institucion_padre FOREIGN KEY (id_padre) REFERENCES administracion.institucion (id) ON DELETE SET NULL;');
        $this->addSql('ALTER TABLE administracion.institucion_tipo_asociado ADD CONSTRAINT fk_institucion_tipo_asociado FOREIGN KEY (id_institucion) REFERENCES administracion.institucion (id);');
        $this->addSql('ALTER TABLE administracion.institucion_tipo_asociado ADD CONSTRAINT fk_tipo_asociado_institucion FOREIGN KEY (id_institucion_tipo) REFERENCES administracion.catalogo (id);');
        $this->addSql('ALTER TABLE almacen.almacen ADD CONSTRAINT fk_almacen_institucion FOREIGN KEY (id_institucion) REFERENCES administracion.institucion (id);');
        $this->addSql('ALTER TABLE clinico.lesion ADD CONSTRAINT fk_lesion_pieza_dental FOREIGN KEY (id_pieza_dental) REFERENCES clinico.ficha_catalogo (id);');
        $this->addSql('ALTER TABLE clinico.lesion ADD CONSTRAINT fk_lesion_ficha FOREIGN KEY (id_ficha) REFERENCES clinico.ficha (id);');
        $this->addSql('ALTER TABLE clinico.lesion ADD CONSTRAINT fk_lesion_valor_tipo FOREIGN KEY (id_lesion_valor_tipo) REFERENCES clinico.ficha_catalogo (id);');
        $this->addSql('ALTER TABLE almacen.movimiento ADD CONSTRAINT fk_movimiento_almacen FOREIGN KEY (id_almacen) REFERENCES almacen.almacen (id);');
        $this->addSql('ALTER TABLE almacen.movimiento ADD CONSTRAINT fk_movimiento_articulo_presentacion FOREIGN KEY (id_articulo_presentacion) REFERENCES almacen.articulo_presentacion (id);');
        $this->addSql('ALTER TABLE clinico.pago ADD CONSTRAINT fk_pago_expediente_clinico FOREIGN KEY (id_expediente_clinico) REFERENCES clinico.expediente_clinico (id);');
        $this->addSql('ALTER TABLE academico.periodo_ciclo ADD CONSTRAINT fk_periodo_ciclo FOREIGN KEY (id_periodo) REFERENCES academico.periodo (id);');
        $this->addSql('ALTER TABLE administracion.persona ADD CONSTRAINT fk_persona_cuenta FOREIGN KEY (id_cuenta) REFERENCES administracion.cuenta (id);');
        $this->addSql('ALTER TABLE administracion.persona_atributo ADD CONSTRAINT fk_persona_atributo FOREIGN KEY (id_persona) REFERENCES administracion.persona (id);');
        $this->addSql('ALTER TABLE administracion.persona_atributo ADD CONSTRAINT fk_atributo_persona FOREIGN KEY (id_atributo) REFERENCES administracion.catalogo (id);');
        $this->addSql('ALTER TABLE clinico.placa_bacteriana ADD CONSTRAINT fk_placa_bacteriana_ficha FOREIGN KEY (id_ficha) REFERENCES clinico.ficha (id);');
        $this->addSql('ALTER TABLE clinico.placa_bacteriana ADD CONSTRAINT fk_placa_bacteriana_indice FOREIGN KEY (id_placa_bacteriana_indice) REFERENCES clinico.ficha_catalogo (id);');
        $this->addSql('ALTER TABLE academico.plan_estudio ADD CONSTRAINT fk_plan_estudio_carrera FOREIGN KEY (id_carrera) REFERENCES academico.carrera (id);');
        $this->addSql('ALTER TABLE academico.plan_estudio_curso ADD CONSTRAINT fk_curso_plan_estudio FOREIGN KEY (id_curso) REFERENCES academico.curso (id);');
        $this->addSql('ALTER TABLE academico.plan_estudio_curso ADD CONSTRAINT fk_plan_estudio_curso FOREIGN KEY (id_plan_estudio) REFERENCES academico.plan_estudio (id);');
        $this->addSql('ALTER TABLE academico.plan_estudio_curso ADD CONSTRAINT fk_plan_estudio_ciclo FOREIGN KEY (id_plan_estudio_ciclo) REFERENCES academico.plan_estudio_ciclo (id);');
        $this->addSql('ALTER TABLE academico.curso_programa ADD CONSTRAINT fk_curso_programa FOREIGN KEY (id_programa) REFERENCES rotacion.programa (id);');
        $this->addSql('ALTER TABLE academico.curso_programa ADD CONSTRAINT fk_plan_estudio_curso_programa FOREIGN KEY (id_plan_estudio_curso) REFERENCES academico.plan_estudio_curso (id);');
        $this->addSql('ALTER TABLE clinico.plan_preventivo ADD CONSTRAINT fk_plan_preventivo_ficha FOREIGN KEY (id_ficha) REFERENCES clinico.ficha (id);');
        $this->addSql('ALTER TABLE clinico.plan_preventivo ADD CONSTRAINT fk_plan_preventivo_tratamiento FOREIGN KEY (id_tratamiento) REFERENCES clinico.tratamiento (id);');
        $this->addSql('ALTER TABLE clinico.plan_preventivo ADD CONSTRAINT fk_plan_preventivo_area FOREIGN KEY (id_plan_preventivo_area) REFERENCES clinico.ficha_catalogo (id);');
        $this->addSql('ALTER TABLE rotacion.programa_institucion ADD CONSTRAINT fk_programa_institucion FOREIGN KEY (id_programa) REFERENCES rotacion.programa (id);');
        $this->addSql('ALTER TABLE rotacion.programa_institucion ADD CONSTRAINT fk_institucion_programa FOREIGN KEY (id_institucion) REFERENCES administracion.institucion (id);');
        $this->addSql('ALTER TABLE administracion.rol_permiso ADD CONSTRAINT fk_rol_permiso FOREIGN KEY (id_rol) REFERENCES administracion.rol (id);');
        $this->addSql('ALTER TABLE administracion.rol_permiso ADD CONSTRAINT fk_permiso_rol FOREIGN KEY (id_permiso) REFERENCES administracion.permiso (id);');
        $this->addSql('ALTER TABLE rotacion.rotacion ADD CONSTRAINT fk_rotacion_padre FOREIGN KEY (id_padre) REFERENCES rotacion.rotacion (id) ON DELETE SET NULL;');
        $this->addSql('ALTER TABLE rotacion.rotacion ADD CONSTRAINT fk_rotacion_planificacion FOREIGN KEY (id_planificacion) REFERENCES rotacion.planificacion (id);');
        $this->addSql('ALTER TABLE rotacion.rotacion_estudiante ADD CONSTRAINT fk_rotacion_estudiante FOREIGN KEY (id_rotacion) REFERENCES rotacion.rotacion (id);');
        $this->addSql('ALTER TABLE rotacion.rotacion_estudiante ADD CONSTRAINT fk_rotacion_estudiante_programa_institucion FOREIGN KEY (id_programa_institucion) REFERENCES rotacion.programa_institucion (id);');
        $this->addSql('ALTER TABLE rotacion.rotacion_estudiante ADD CONSTRAINT fk_rotacion_estudiante_grupo FOREIGN KEY (id_grupo_estudiante) REFERENCES rotacion.grupo_estudiante (id);');
        $this->addSql('ALTER TABLE administracion.telefono ADD CONSTRAINT fk_telefono_cuenta FOREIGN KEY (id_cuenta) REFERENCES administracion.cuenta (id);');
        $this->addSql('ALTER TABLE administracion.telefono ADD CONSTRAINT fk_telefono_tipo FOREIGN KEY (id_telefono_tipo) REFERENCES administracion.catalogo (id);');
        $this->addSql('ALTER TABLE clinico.tratamiento ADD CONSTRAINT fk_tratamiento_area FOREIGN KEY (id_tratamiento_area) REFERENCES clinico.ficha_catalogo (id);');
        $this->addSql('ALTER TABLE clinico.tratamiento_prioritario ADD CONSTRAINT fk_tratamiento_prioritario_ficha FOREIGN KEY (id_ficha) REFERENCES clinico.ficha (id);');
        $this->addSql('ALTER TABLE clinico.tratamiento_prioritario ADD CONSTRAINT fk_tratamiento_prioritario FOREIGN KEY (id_tratamiento) REFERENCES clinico.tratamiento (id);');
        $this->addSql('ALTER TABLE academico.unidad_integracion ADD CONSTRAINT fk_unidad_integracion_plan_estudio FOREIGN KEY (id_plan_estudio_curso) REFERENCES academico.plan_estudio_curso (id);');
        $this->addSql('ALTER TABLE academico.unidad_integracion ADD CONSTRAINT fk_unidad_integracion_periodo_ciclo FOREIGN KEY (id_periodo_ciclo) REFERENCES academico.periodo_ciclo (id);');
        $this->addSql('ALTER TABLE academico.unidad_integracion ADD CONSTRAINT fk_unidad_integracion_programa FOREIGN KEY (id_programa) REFERENCES rotacion.programa (id);');
        $this->addSql('ALTER TABLE administracion.usuario ADD CONSTRAINT fk_usuario_cuenta FOREIGN KEY (id_cuenta) REFERENCES administracion.cuenta (id);');
        $this->addSql('ALTER TABLE administracion.usuario_rol ADD CONSTRAINT fk_usuario_rol FOREIGN KEY (id_usuario) REFERENCES administracion.usuario (id);');
        $this->addSql('ALTER TABLE administracion.usuario_rol ADD CONSTRAINT fk_rol_usuario FOREIGN KEY (id_rol) REFERENCES administracion.rol (id);');
        $this->addSql('ALTER TABLE almacen.vale_estado_historial ADD CONSTRAINT fk_vale_estado_historial FOREIGN KEY (id_vale) REFERENCES almacen.vale (id);');
        $this->addSql('ALTER TABLE almacen.vale_estado_historial ADD CONSTRAINT fk_historial_vale_estado FOREIGN KEY (id_vale_estado) REFERENCES almacen.vale_estado (id);');
        $this->addSql('ALTER TABLE rotacion.programa_calendario ADD CONSTRAINT fk_programa_periodo_ciclo FOREIGN KEY (id_periodo_ciclo) REFERENCES academico.periodo_ciclo (id);');
        $this->addSql('ALTER TABLE academico.estudiante_curso ADD CONSTRAINT fk_estudiante_curso_estado FOREIGN KEY (id_estudiante_estado) REFERENCES academico.estudiante_estado (id);');
        $this->addSql('ALTER TABLE rotacion.programa_calendario ADD CONSTRAINT fk_programa_calendario_grupo FOREIGN KEY (id_grupo) REFERENCES rotacion.grupo (id);');
        $this->addSql('ALTER TABLE rotacion.programa_calendario ADD CONSTRAINT fk_programa_calendario FOREIGN KEY (id_programa) REFERENCES rotacion.programa (id);');
        $this->addSql('ALTER TABLE academico.estudiante_plan_estudio ADD CONSTRAINT fk_estudiante_plan_estudio_estado FOREIGN KEY (id_estudiante_estado) REFERENCES academico.estudiante_estado (id);');
        $this->addSql('ALTER TABLE rotacion.rotacion ADD CONSTRAINT fk_rotacion_grupo FOREIGN KEY (id_grupo) REFERENCES rotacion.grupo (id);');
        $this->addSql('ALTER TABLE clinico.codigo_tratamiento ADD CONSTRAINT fk_codigo_tratamiento FOREIGN KEY (id_tratamiento) REFERENCES clinico.tratamiento (id);');
        $this->addSql('ALTER TABLE clinico.codigo_tratamiento ADD CONSTRAINT fk_codigo_caries FOREIGN KEY (id_codigo_caries) REFERENCES clinico.ficha_catalogo (id);');
        $this->addSql('ALTER TABLE clinico.codigo_tratamiento ADD CONSTRAINT fk_codigo_restauracion FOREIGN KEY (id_codigo_restauracion) REFERENCES clinico.ficha_catalogo (id);');
        $this->addSql('ALTER TABLE clinico.evaluacion_dental ADD CONSTRAINT fk_evaluacion_codigo_tratamiento FOREIGN KEY (id_codigo_tratamiento) REFERENCES clinico.codigo_tratamiento (id);');
        $this->addSql('ALTER TABLE academico.calificacion ADD CONSTRAINT fk_calificacion_grupo FOREIGN KEY (id_calificacion_grupo) REFERENCES academico.calificacion_grupo (id);');
        $this->addSql('ALTER TABLE rotacion.planificacion ADD CONSTRAINT fk_planificacion_periodo_ciclo FOREIGN KEY (id_periodo_ciclo) REFERENCES academico.periodo_ciclo (id);');
        $this->addSql('ALTER TABLE almacen.vale_ficha_tratamiento ADD CONSTRAINT fk_vale_ficha_tratamiento FOREIGN KEY (id_vale) REFERENCES almacen.vale (id);');
        $this->addSql('ALTER TABLE almacen.vale_ficha_tratamiento ADD CONSTRAINT fk_ficha_tratamiento_vale FOREIGN KEY (id_ficha_tratamiento) REFERENCES clinico.ficha_tratamiento (id);');
        $this->addSql('ALTER TABLE almacen.movimiento ADD CONSTRAINT fk_movimiento_usuario FOREIGN KEY (id_usuario) REFERENCES administracion.usuario (id);');
        $this->addSql('ALTER TABLE almacen.vale ADD CONSTRAINT fk_vale_estado FOREIGN KEY (id_vale_estado) REFERENCES almacen.vale_estado (id);');
        $this->addSql('ALTER TABLE almacen.tratamiento_articulo_presentacion ADD CONSTRAINT fk_tratamiento_articulo_presentacion FOREIGN KEY (id_tratamiento) REFERENCES clinico.tratamiento (id);');
        $this->addSql('ALTER TABLE almacen.tratamiento_articulo_presentacion ADD CONSTRAINT fk_articulo_presentacion_tratamiento FOREIGN KEY (id_articulo_presentacion) REFERENCES almacen.articulo_presentacion (id);');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE almacen.articulo_presentacion DROP FOREIGN KEY fk_articulo;');
        $this->addSql('ALTER TABLE almacen.articulo_presentacion DROP FOREIGN KEY fk_unidad_medida;');
        $this->addSql('ALTER TABLE almacen.articulo_presentacion DROP FOREIGN KEY fk_sub_unidad_medida;');
        $this->addSql('ALTER TABLE academico.calificacion DROP FOREIGN KEY fk_calificacion_estudiante_curso;');
        $this->addSql('ALTER TABLE academico.calificacion DROP FOREIGN KEY fk_calificacion_evaluacion;');
        $this->addSql('ALTER TABLE academico.calificacion DROP FOREIGN KEY fk_calificacion_padre;');
        $this->addSql('ALTER TABLE academico.calificacion_grupo DROP FOREIGN KEY fk_grupo_calificacion;');
        $this->addSql('ALTER TABLE academico.calificacion_grupo DROP FOREIGN KEY fk_calificacion_grupo_evaluacion;');
        $this->addSql('ALTER TABLE academico.calificacion_grupo DROP FOREIGN KEY fk_calificacion_grupo_padre;');
        $this->addSql('ALTER TABLE academico.calificacion_penalizacion DROP FOREIGN KEY fk_calificacion_penalizacion;');
        $this->addSql('ALTER TABLE academico.calificacion_penalizacion DROP FOREIGN KEY fk_penalizacion_calificacion;');
        $this->addSql('ALTER TABLE administracion.catalogo DROP FOREIGN KEY fk_catalogo_padre;');
        $this->addSql('ALTER TABLE administracion.catalogo DROP FOREIGN KEY fk_catalogo_tipo;');
        $this->addSql('ALTER TABLE administracion.catalogo_tipo DROP FOREIGN KEY fk_catalogo_tipo_padre;');
        $this->addSql('ALTER TABLE almacen.consumo DROP FOREIGN KEY fk_consumo_vale;');
        $this->addSql('ALTER TABLE almacen.consumo DROP FOREIGN KEY fk_consumo_movimiento;');
        $this->addSql('ALTER TABLE clinico.control_sellante DROP FOREIGN KEY fk_control_sellante_ficha;');
        $this->addSql('ALTER TABLE clinico.control_sellante DROP FOREIGN KEY fk_control_sellante_tratamiento;');
        $this->addSql('ALTER TABLE clinico.control_sellante DROP FOREIGN KEY fk_sellante_estado;');
        $this->addSql('ALTER TABLE administracion.direccion DROP FOREIGN KEY fk_direccion_cuenta;');
        $this->addSql('ALTER TABLE administracion.direccion DROP FOREIGN KEY fk_direccion_tipo;');
        $this->addSql('ALTER TABLE administracion.direccion DROP FOREIGN KEY fk_division_politica;');
        $this->addSql('ALTER TABLE administracion.direccion DROP FOREIGN KEY fk_direccion_area;');
        $this->addSql('ALTER TABLE administracion.email DROP FOREIGN KEY fk_email_cuenta;');
        $this->addSql('ALTER TABLE academico.estudiante DROP FOREIGN KEY fk_estudiante_persona;');
        $this->addSql('ALTER TABLE academico.estudiante_curso DROP FOREIGN KEY fk_estudiante_curso_plan_estudio;');
        $this->addSql('ALTER TABLE academico.estudiante_curso DROP FOREIGN KEY fk_estudiante_curso;');
        $this->addSql('ALTER TABLE academico.estudiante_curso DROP FOREIGN KEY fk_estudiante_curso_periodo_ciclo;');
        $this->addSql('ALTER TABLE academico.estudiante_estado_bitacora DROP FOREIGN KEY fk_estudiante_estado_bitacora_plan_estudio;');
        $this->addSql('ALTER TABLE academico.estudiante_estado_bitacora DROP FOREIGN KEY fk_estudiante_estado_bitacora;');
        $this->addSql('ALTER TABLE academico.estudiante_plan_estudio DROP FOREIGN KEY fk_estudiante_plan_estudio;');
        $this->addSql('ALTER TABLE academico.estudiante_plan_estudio DROP FOREIGN KEY fk_plan_estudio_estudiante;');
        $this->addSql('ALTER TABLE academico.evaluacion DROP FOREIGN KEY fk_evaluacion_unidad_integracion;');
        $this->addSql('ALTER TABLE academico.evaluacion DROP FOREIGN KEY fk_evaluacion_tipo;');
        $this->addSql('ALTER TABLE academico.evaluacion DROP FOREIGN KEY fk_evaluacion_padre;');
        $this->addSql('ALTER TABLE clinico.evaluacion_dental DROP FOREIGN KEY fk_evaluacion_dental_ficha;');
        $this->addSql('ALTER TABLE clinico.evaluacion_dental DROP FOREIGN KEY fk_evaluacion_pieza_dental;');
        $this->addSql('ALTER TABLE clinico.evaluacion_dental DROP FOREIGN KEY fk_evaluacion_pieza_dental_superficie;');
        $this->addSql('ALTER TABLE clinico.evaluacion_dental DROP FOREIGN KEY fk_evaluacion_pieza_dental_estado;');
        $this->addSql('ALTER TABLE almacen.existencia DROP FOREIGN KEY fk_existencia_almacen;');
        $this->addSql('ALTER TABLE almacen.existencia DROP FOREIGN KEY fk_existencia_articulo_presentacion;');
        $this->addSql('ALTER TABLE clinico.expediente_clinico DROP FOREIGN KEY fk_expediente_clinico_persona;');
        $this->addSql('ALTER TABLE clinico.expediente_clinico DROP FOREIGN KEY fk_expediente_clinico_tipo;');
        $this->addSql('ALTER TABLE clinico.ficha DROP FOREIGN KEY fk_ficha_cuenta;');
        $this->addSql('ALTER TABLE clinico.ficha DROP FOREIGN KEY fk_ficha_expediente_clinico;');
        $this->addSql('ALTER TABLE clinico.ficha DROP FOREIGN KEY fk_referencia;');
        $this->addSql('ALTER TABLE clinico.ficha DROP FOREIGN KEY fk_frecuencia_cepillado;');
        $this->addSql('ALTER TABLE clinico.ficha DROP FOREIGN KEY fk_indice_riesgo_cariogenico;');
        $this->addSql('ALTER TABLE clinico.ficha_catalogo DROP FOREIGN KEY fk_ficha_catalogo_padre;');
        $this->addSql('ALTER TABLE clinico.ficha_catalogo DROP FOREIGN KEY fk_ficha_catalogo_tipo;');
        $this->addSql('ALTER TABLE clinico.ficha_catalogo_tipo DROP FOREIGN KEY fk_ficha_catalogo_tipo_padre;');
        $this->addSql('ALTER TABLE clinico.ficha_evaluacion_sistemica DROP FOREIGN KEY fk_ficha_evaluacion_sistemica;');
        $this->addSql('ALTER TABLE clinico.ficha_evaluacion_sistemica DROP FOREIGN KEY fk_evaluacion_sistema;');
        $this->addSql('ALTER TABLE clinico.ficha_habito_bucal DROP FOREIGN KEY fk_ficha_habito_bucal;');
        $this->addSql('ALTER TABLE clinico.ficha_habito_bucal DROP FOREIGN KEY fk_habito_bucal;');
        $this->addSql('ALTER TABLE clinico.ficha_higiene_bucal DROP FOREIGN KEY fk_ficha_higiene_bucal;');
        $this->addSql('ALTER TABLE clinico.ficha_higiene_bucal DROP FOREIGN KEY fk_higiene_bucal;');
        $this->addSql('ALTER TABLE clinico.ficha_tratamiento DROP FOREIGN KEY fk_ficha_tratamiento;');
        $this->addSql('ALTER TABLE clinico.ficha_tratamiento DROP FOREIGN KEY fk_tratamiento_ficha;');
        $this->addSql('ALTER TABLE clinico.ficha_tratamiento DROP FOREIGN KEY fk_ficha_tratamiento_pieza_dental;');
        $this->addSql('ALTER TABLE rotacion.grupo DROP FOREIGN KEY fk_grupo_padre;');
        $this->addSql('ALTER TABLE rotacion.grupo DROP FOREIGN KEY fk_grupo_periodo_ciclo;');
        $this->addSql('ALTER TABLE rotacion.grupo_estudiante DROP FOREIGN KEY fk_grupo_estudiante_curso;');
        $this->addSql('ALTER TABLE rotacion.grupo_estudiante DROP FOREIGN KEY fk_grupo_estudiante;');
        $this->addSql('ALTER TABLE clinico.ingesta_azucar DROP FOREIGN KEY fk_ingesta_azucar_ficha;');
        $this->addSql('ALTER TABLE clinico.ingesta_azucar DROP FOREIGN KEY fk_ingesta_azucar_criterio;');
        $this->addSql('ALTER TABLE administracion.institucion DROP FOREIGN KEY fk_institucion_cuenta;');
        $this->addSql('ALTER TABLE administracion.institucion DROP FOREIGN KEY fk_institucion_padre;');
        $this->addSql('ALTER TABLE administracion.institucion_tipo_asociado DROP FOREIGN KEY fk_institucion_tipo_asociado;');
        $this->addSql('ALTER TABLE administracion.institucion_tipo_asociado DROP FOREIGN KEY fk_tipo_asociado_institucion;');
        $this->addSql('ALTER TABLE almacen.almacen DROP FOREIGN KEY fk_almacen_institucion;');
        $this->addSql('ALTER TABLE clinico.lesion DROP FOREIGN KEY fk_lesion_pieza_dental;');
        $this->addSql('ALTER TABLE clinico.lesion DROP FOREIGN KEY fk_lesion_ficha;');
        $this->addSql('ALTER TABLE clinico.lesion DROP FOREIGN KEY fk_lesion_valor_tipo;');
        $this->addSql('ALTER TABLE almacen.movimiento DROP FOREIGN KEY fk_movimiento_almacen;');
        $this->addSql('ALTER TABLE almacen.movimiento DROP FOREIGN KEY fk_movimiento_articulo_presentacion;');
        $this->addSql('ALTER TABLE clinico.pago DROP FOREIGN KEY fk_pago_expediente_clinico;');
        $this->addSql('ALTER TABLE academico.periodo_ciclo DROP FOREIGN KEY fk_periodo_ciclo;');
        $this->addSql('ALTER TABLE administracion.persona DROP FOREIGN KEY fk_persona_cuenta;');
        $this->addSql('ALTER TABLE administracion.persona_atributo DROP FOREIGN KEY fk_persona_atributo;');
        $this->addSql('ALTER TABLE administracion.persona_atributo DROP FOREIGN KEY fk_atributo_persona;');
        $this->addSql('ALTER TABLE clinico.placa_bacteriana DROP FOREIGN KEY fk_placa_bacteriana_ficha;');
        $this->addSql('ALTER TABLE clinico.placa_bacteriana DROP FOREIGN KEY fk_placa_bacteriana_indice;');
        $this->addSql('ALTER TABLE academico.plan_estudio DROP FOREIGN KEY fk_plan_estudio_carrera;');
        $this->addSql('ALTER TABLE academico.plan_estudio_curso DROP FOREIGN KEY fk_curso_plan_estudio;');
        $this->addSql('ALTER TABLE academico.plan_estudio_curso DROP FOREIGN KEY fk_plan_estudio_curso;');
        $this->addSql('ALTER TABLE academico.plan_estudio_curso DROP FOREIGN KEY fk_plan_estudio_ciclo;');
        $this->addSql('ALTER TABLE academico.curso_programa DROP FOREIGN KEY fk_curso_programa;');
        $this->addSql('ALTER TABLE academico.curso_programa DROP FOREIGN KEY fk_plan_estudio_curso_programa;');
        $this->addSql('ALTER TABLE clinico.plan_preventivo DROP FOREIGN KEY fk_plan_preventivo_ficha;');
        $this->addSql('ALTER TABLE clinico.plan_preventivo DROP FOREIGN KEY fk_plan_preventivo_tratamiento;');
        $this->addSql('ALTER TABLE clinico.plan_preventivo DROP FOREIGN KEY fk_plan_preventivo_area;');
        $this->addSql('ALTER TABLE rotacion.programa_institucion DROP FOREIGN KEY fk_programa_institucion;');
        $this->addSql('ALTER TABLE rotacion.programa_institucion DROP FOREIGN KEY fk_institucion_programa;');
        $this->addSql('ALTER TABLE administracion.rol_permiso DROP FOREIGN KEY fk_rol_permiso;');
        $this->addSql('ALTER TABLE administracion.rol_permiso DROP FOREIGN KEY fk_permiso_rol;');
        $this->addSql('ALTER TABLE rotacion.rotacion DROP FOREIGN KEY fk_rotacion_padre;');
        $this->addSql('ALTER TABLE rotacion.rotacion DROP FOREIGN KEY fk_rotacion_planificacion;');
        $this->addSql('ALTER TABLE rotacion.rotacion_estudiante DROP FOREIGN KEY fk_rotacion_estudiante;');
        $this->addSql('ALTER TABLE rotacion.rotacion_estudiante DROP FOREIGN KEY fk_rotacion_estudiante_programa_institucion;');
        $this->addSql('ALTER TABLE rotacion.rotacion_estudiante DROP FOREIGN KEY fk_rotacion_estudiante_grupo;');
        $this->addSql('ALTER TABLE administracion.telefono DROP FOREIGN KEY fk_telefono_cuenta;');
        $this->addSql('ALTER TABLE administracion.telefono DROP FOREIGN KEY fk_telefono_tipo;');
        $this->addSql('ALTER TABLE clinico.tratamiento DROP FOREIGN KEY fk_tratamiento_area;');
        $this->addSql('ALTER TABLE clinico.tratamiento_prioritario DROP FOREIGN KEY fk_tratamiento_prioritario_ficha;');
        $this->addSql('ALTER TABLE clinico.tratamiento_prioritario DROP FOREIGN KEY fk_tratamiento_prioritario;');
        $this->addSql('ALTER TABLE academico.unidad_integracion DROP FOREIGN KEY fk_unidad_integracion_plan_estudio;');
        $this->addSql('ALTER TABLE academico.unidad_integracion DROP FOREIGN KEY fk_unidad_integracion_periodo_ciclo;');
        $this->addSql('ALTER TABLE academico.unidad_integracion DROP FOREIGN KEY fk_unidad_integracion_programa;');
        $this->addSql('ALTER TABLE administracion.usuario DROP FOREIGN KEY fk_usuario_cuenta;');
        $this->addSql('ALTER TABLE administracion.usuario_rol DROP FOREIGN KEY fk_usuario_rol;');
        $this->addSql('ALTER TABLE administracion.usuario_rol DROP FOREIGN KEY fk_rol_usuario;');
        $this->addSql('ALTER TABLE almacen.vale_estado_historial DROP FOREIGN KEY fk_vale_estado_historial;');
        $this->addSql('ALTER TABLE almacen.vale_estado_historial DROP FOREIGN KEY fk_historial_vale_estado;');
        $this->addSql('ALTER TABLE rotacion.programa_calendario DROP FOREIGN KEY fk_programa_periodo_ciclo;');
        $this->addSql('ALTER TABLE academico.estudiante_curso DROP FOREIGN KEY fk_estudiante_curso_estado;');
        $this->addSql('ALTER TABLE rotacion.programa_calendario DROP FOREIGN KEY fk_programa_calendario_grupo;');
        $this->addSql('ALTER TABLE rotacion.programa_calendario DROP FOREIGN KEY fk_programa_calendario;');
        $this->addSql('ALTER TABLE academico.estudiante_plan_estudio DROP FOREIGN KEY fk_estudiante_plan_estudio_estado;');
        $this->addSql('ALTER TABLE rotacion.rotacion DROP FOREIGN KEY fk_rotacion_grupo;');
        $this->addSql('ALTER TABLE clinico.codigo_tratamiento DROP FOREIGN KEY fk_codigo_tratamiento;');
        $this->addSql('ALTER TABLE clinico.codigo_tratamiento DROP FOREIGN KEY fk_codigo_caries;');
        $this->addSql('ALTER TABLE clinico.codigo_tratamiento DROP FOREIGN KEY fk_codigo_restauracion;');
        $this->addSql('ALTER TABLE clinico.evaluacion_dental DROP FOREIGN KEY fk_evaluacion_codigo_tratamiento;');
        $this->addSql('ALTER TABLE academico.calificacion DROP FOREIGN KEY fk_calificacion_grupo;');
        $this->addSql('ALTER TABLE rotacion.planificacion DROP FOREIGN KEY fk_planificacion_periodo_ciclo;');
        $this->addSql('ALTER TABLE almacen.vale_ficha_tratamiento DROP FOREIGN KEY fk_vale_ficha_tratamiento;');
        $this->addSql('ALTER TABLE almacen.vale_ficha_tratamiento DROP FOREIGN KEY fk_ficha_tratamiento_vale;');
        $this->addSql('ALTER TABLE almacen.movimiento DROP FOREIGN KEY fk_movimiento_usuario;');
        $this->addSql('ALTER TABLE almacen.vale DROP FOREIGN KEY fk_vale_estado;');
        $this->addSql('ALTER TABLE almacen.tratamiento_articulo_presentacion DROP FOREIGN KEY fk_tratamiento_articulo_presentacion;');
        $this->addSql('ALTER TABLE almacen.tratamiento_articulo_presentacion DROP FOREIGN KEY fk_articulo_presentacion_tratamiento;');
        $this->addSql('DROP TABLE IF EXISTS almacen.articulo;');
        $this->addSql('DROP TABLE IF EXISTS almacen.articulo_presentacion;');
        $this->addSql('DROP TABLE IF EXISTS academico.calificacion;');
        $this->addSql('DROP TABLE IF EXISTS academico.calificacion_grupo;');
        $this->addSql('DROP TABLE IF EXISTS academico.calificacion_penalizacion;');
        $this->addSql('DROP TABLE IF EXISTS academico.carrera;');
        $this->addSql('DROP TABLE IF EXISTS administracion.catalogo;');
        $this->addSql('DROP TABLE IF EXISTS administracion.catalogo_tipo;');
        $this->addSql('DROP TABLE IF EXISTS almacen.consumo;');
        $this->addSql('DROP TABLE IF EXISTS clinico.control_sellante;');
        $this->addSql('DROP TABLE IF EXISTS administracion.cuenta;');
        $this->addSql('DROP TABLE IF EXISTS academico.curso;');
        $this->addSql('DROP TABLE IF EXISTS administracion.direccion;');
        $this->addSql('DROP TABLE IF EXISTS administracion.email;');
        $this->addSql('DROP TABLE IF EXISTS academico.estudiante;');
        $this->addSql('DROP TABLE IF EXISTS academico.estudiante_curso;');
        $this->addSql('DROP TABLE IF EXISTS academico.estudiante_estado_bitacora;');
        $this->addSql('DROP TABLE IF EXISTS academico.estudiante_estado;');
        $this->addSql('DROP TABLE IF EXISTS academico.estudiante_plan_estudio;');
        $this->addSql('DROP TABLE IF EXISTS rotacion.programa_calendario;');
        $this->addSql('DROP TABLE IF EXISTS academico.evaluacion;');
        $this->addSql('DROP TABLE IF EXISTS clinico.evaluacion_dental;');
        $this->addSql('DROP TABLE IF EXISTS academico.evaluacion_tipo;');
        $this->addSql('DROP TABLE IF EXISTS almacen.existencia;');
        $this->addSql('DROP TABLE IF EXISTS clinico.expediente_clinico;');
        $this->addSql('DROP TABLE IF EXISTS clinico.ficha;');
        $this->addSql('DROP TABLE IF EXISTS clinico.ficha_catalogo;');
        $this->addSql('DROP TABLE IF EXISTS clinico.ficha_catalogo_tipo;');
        $this->addSql('DROP TABLE IF EXISTS clinico.ficha_evaluacion_sistemica;');
        $this->addSql('DROP TABLE IF EXISTS clinico.ficha_habito_bucal;');
        $this->addSql('DROP TABLE IF EXISTS clinico.ficha_higiene_bucal;');
        $this->addSql('DROP TABLE IF EXISTS clinico.ficha_tratamiento;');
        $this->addSql('DROP TABLE IF EXISTS rotacion.grupo;');
        $this->addSql('DROP TABLE IF EXISTS rotacion.grupo_estudiante;');
        $this->addSql('DROP TABLE IF EXISTS clinico.ingesta_azucar;');
        $this->addSql('DROP TABLE IF EXISTS administracion.institucion;');
        $this->addSql('DROP TABLE IF EXISTS administracion.institucion_tipo_asociado;');
        $this->addSql('DROP TABLE IF EXISTS almacen.almacen;');
        $this->addSql('DROP TABLE IF EXISTS clinico.lesion;');
        $this->addSql('DROP TABLE IF EXISTS almacen.movimiento;');
        $this->addSql('DROP TABLE IF EXISTS clinico.pago;');
        $this->addSql('DROP TABLE IF EXISTS academico.penalizacion;');
        $this->addSql('DROP TABLE IF EXISTS academico.periodo;');
        $this->addSql('DROP TABLE IF EXISTS academico.periodo_ciclo;');
        $this->addSql('DROP TABLE IF EXISTS administracion.permiso;');
        $this->addSql('DROP TABLE IF EXISTS administracion.persona;');
        $this->addSql('DROP TABLE IF EXISTS administracion.persona_atributo;');
        $this->addSql('DROP TABLE IF EXISTS clinico.placa_bacteriana;');
        $this->addSql('DROP TABLE IF EXISTS academico.plan_estudio;');
        $this->addSql('DROP TABLE IF EXISTS academico.plan_estudio_ciclo;');
        $this->addSql('DROP TABLE IF EXISTS academico.plan_estudio_curso;');
        $this->addSql('DROP TABLE IF EXISTS academico.curso_programa;');
        $this->addSql('DROP TABLE IF EXISTS clinico.plan_preventivo;');
        $this->addSql('DROP TABLE IF EXISTS rotacion.planificacion;');
        $this->addSql('DROP TABLE IF EXISTS rotacion.programa;');
        $this->addSql('DROP TABLE IF EXISTS rotacion.programa_institucion;');
        $this->addSql('DROP TABLE IF EXISTS administracion.rol;');
        $this->addSql('DROP TABLE IF EXISTS administracion.rol_permiso;');
        $this->addSql('DROP TABLE IF EXISTS rotacion.rotacion;');
        $this->addSql('DROP TABLE IF EXISTS rotacion.rotacion_estudiante;');
        $this->addSql('DROP TABLE IF EXISTS administracion.telefono;');
        $this->addSql('DROP TABLE IF EXISTS clinico.tratamiento;');
        $this->addSql('DROP TABLE IF EXISTS clinico.tratamiento_prioritario;');
        $this->addSql('DROP TABLE IF EXISTS academico.unidad_integracion;');
        $this->addSql('DROP TABLE IF EXISTS almacen.unidad_medida;');
        $this->addSql('DROP TABLE IF EXISTS administracion.usuario;');
        $this->addSql('DROP TABLE IF EXISTS administracion.usuario_rol;');
        $this->addSql('DROP TABLE IF EXISTS almacen.vale;');
        $this->addSql('DROP TABLE IF EXISTS almacen.vale_estado_historial;');
        $this->addSql('DROP TABLE IF EXISTS almacen.vale_estado;');
        $this->addSql('DROP TABLE IF EXISTS clinico.codigo_tratamiento;');
        $this->addSql('DROP TABLE IF EXISTS almacen.vale_ficha_tratamiento;');
        $this->addSql('DROP TABLE IF EXISTS almacen.tratamiento_articulo_presentacion;');
    }
}
