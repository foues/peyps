<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Class Version20180902010650
 * @package DoctrineMigrations
 */
final class Version20180902010650 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        //V6
        $this->addSql('ALTER TABLE rotacion.grupo MODIFY nombre varchar(50) NOT NULL');
        $this->addSql('ALTER TABLE rotacion.grupo ADD calificable tinyint(3) DEFAULT 0');
        $this->addSql('ALTER TABLE rotacion.grupo ADD id_plan_estudio_curso int(11)');
        $this->addSql('ALTER TABLE rotacion.grupo ADD CONSTRAINT fk_grupo_plan_estudio_curso FOREIGN KEY (id_plan_estudio_curso) REFERENCES academico.plan_estudio_curso (id);');

    }

    public function down(Schema $schema): void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        //V6
        $this->addSql('ALTER TABLE rotacion.grupo DROP FOREIGN KEY fk_grupo_plan_estudio_curso');
        $this->addSql('ALTER TABLE rotacion.grupo DROP COLUMN id_plan_estudio_curso');
        $this->addSql('ALTER TABLE rotacion.grupo DROP COLUMN calificable');
        $this->addSql('ALTER TABLE rotacion.grupo MODIFY nombre varchar(5) NOT NULL');

    }
}
