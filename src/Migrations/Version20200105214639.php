<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Class Version20200105214639
 * @package DoctrineMigrations
 */
final class Version20200105214639 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('
        CREATE TABLE administracion.menu (
          id          int(11) NOT NULL AUTO_INCREMENT,
          id_padre    int(11),
          nombre      varchar(100) NOT NULL,
          descripcion varchar(255),
          url         varchar(255) NOT NULL,
          icon        varchar(50),
          enabled     tinyint(3) DEFAULT 1 NOT NULL,
          creado      timestamp NOT NULL,
          PRIMARY KEY (id)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci
        ');

        $this->addSql('
        CREATE TABLE administracion.menu_permiso (
          id           int(11) NOT NULL AUTO_INCREMENT,
          id_menu      int(11) NOT NULL,
          id_permiso   int(11) NOT NULL,
          asignado_por varchar(50),
          creado       timestamp NOT NULL,
          PRIMARY KEY (id)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci
        ');

        $this->addSql('ALTER TABLE administracion.menu ADD CONSTRAINT fk_menu_padre FOREIGN KEY (id_padre) REFERENCES administracion.menu (id) ON UPDATE CASCADE ON DELETE CASCADE');
        $this->addSql('ALTER TABLE administracion.menu_permiso ADD CONSTRAINT fk_menu_permiso FOREIGN KEY (id_menu) REFERENCES administracion.menu (id) ON UPDATE CASCADE ON DELETE CASCADE');
        $this->addSql('ALTER TABLE administracion.menu_permiso ADD CONSTRAINT fk_permiso_menu FOREIGN KEY (id_permiso) REFERENCES administracion.permiso (id) ON UPDATE CASCADE ON DELETE CASCADE');

    }

    public function down(Schema $schema): void
    {
        $this->addSql('DROP TABLE IF EXISTS administracion.menu_permiso');
        $this->addSql('DROP TABLE IF EXISTS administracion.menu');
    }
}
