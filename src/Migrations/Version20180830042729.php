<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Class Version20180830042729
 * @package DoctrineMigrations
 */
final class Version20180830042729 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('
        create or replace VIEW rotacion.rotacion_detalle_view AS
        SELECT 
          i.nombre AS curso,
          l.carnet,
          b.numero AS rotacion,
          g.id_periodo_ciclo periodo_ciclo,
          m.nombre nombre_periodo_ciclo,
          e.codigo AS programa,
          d.nombre AS institucion,
          b.inicio AS inicio,
          b.fin AS fin,
          CONCAT(DATE_FORMAT(b.inicio,\'%d-%m\'), \' / \', DATE_FORMAT(b.fin,\'%d-%m\')) AS fechas
        FROM rotacion.rotacion_estudiante a
          JOIN rotacion.rotacion b
            ON (a.id_rotacion = b.id)
          JOIN rotacion.programa_institucion c
            ON (a.id_programa_institucion = c.id)
          JOIN administracion.institucion d
            ON (c.id_institucion = d.id)
          JOIN rotacion.programa e
            ON (c.id_programa = e.id)
          JOIN rotacion.grupo_estudiante f
            ON (a.id_grupo_estudiante = f.id)
          join rotacion.grupo g
          on f.id_grupo=g.id
          JOIN academico.estudiante_curso h
            ON (f.id_estudiante_curso = h.id)
          JOIN academico.curso i
            ON (h.id_curso = i.id)
          JOIN academico.estudiante_curso j
           ON f.id_estudiante_curso=j.id
          JOIN academico.estudiante_plan_estudio k
          ON j.id_estudiante_plan_estudio=k.id
          JOIN academico.estudiante l
          on k.id_estudiante=l.id
          JOIN academico.periodo m
          on g.id_periodo_ciclo=m.id
        order by periodo_ciclo,rotacion,programa
        ');

        $this->addSql('
        create or replace view rotacion.rotacion_resumen_email AS
        select curso,periodo_ciclo,nombre_periodo_ciclo,programa,
          max(case when programa=\'ppe\' or rotacion =1 then fechas else null end) rotacion_1,
          max(case when programa=\'ppe\' or  rotacion =2 then fechas else null end) rotacion_2,
          max(case when programa=\'ppe\' or  rotacion =3 then fechas else null end) rotacion_3,
          max(case when programa=\'ppe\' or  rotacion =4 then fechas else null end) rotacion_4,
          max(case when programa=\'ppe\' or  rotacion =5 then fechas else null end) rotacion_5,
          max(case when programa=\'ppe\' or  rotacion =6 then fechas else null end) rotacion_6
          from rotacion.rotacion_detalle_view
          group by curso,programa,periodo_ciclo,nombre_periodo_ciclo
        order by 2,1
        ');
    }

    public function down(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP VIEW IF EXISTS rotacion.rotacion_detalle_view');
        $this->addSql('DROP VIEW IF EXISTS rotacion.rotacion_resumen_email');
    }
}
