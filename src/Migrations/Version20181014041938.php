<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Class Version20181014041938
 * @package DoctrineMigrations
 */
final class Version20181014041938 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');


        //20181009022750
        $this->addSql('ALTER TABLE clinico.ficha_tratamiento DROP FOREIGN KEY fk_ficha_tratamiento;');
        $this->addSql('ALTER TABLE clinico.ficha_tratamiento DROP FOREIGN KEY fk_tratamiento_ficha;');
        $this->addSql('ALTER TABLE clinico.ficha_tratamiento DROP COLUMN id_ficha;');
        $this->addSql('ALTER TABLE clinico.ficha_tratamiento DROP COLUMN id_tratamiento;');
        $this->addSql('ALTER TABLE clinico.ficha_tratamiento DROP COLUMN pieza_dental;');
        $this->addSql('ALTER TABLE clinico.ficha_tratamiento DROP COLUMN cantidad;');
        $this->addSql('ALTER TABLE clinico.ficha_tratamiento ADD id_evaluacion_dental int(11) NOT NULL;');
        $this->addSql('ALTER TABLE clinico.ficha_tratamiento ADD CONSTRAINT fk_ficha_tratamiento_evaluacion FOREIGN KEY (id_evaluacion_dental) REFERENCES clinico.evaluacion_dental (id);');
        $this->addSql('ALTER TABLE clinico.ficha_tratamiento ADD id_cuenta int(11) NOT NULL;');
        $this->addSql('ALTER TABLE clinico.ficha ADD CONSTRAINT fk_ficha_evaluacion_cuenta FOREIGN KEY (id_cuenta) REFERENCES administracion.cuenta (id);');


        //20181009144922
        $this->addSql('ALTER TABLE clinico.evaluacion_dental ADD retratamiento tinyint(1) default 0;');


        //Corrigiendo almacén por cambios en clínico
        $this->addSql('DROP TABLE IF EXISTS almacen.vale_ficha_tratamiento');

        $this->addSql(' 
        CREATE TABLE almacen.vale_evaluacion_dental (
          id                   int(11) NOT NULL AUTO_INCREMENT, 
          id_vale              int(11) NOT NULL, 
          id_evaluacion_dental int(11) NOT NULL, 
          creado               timestamp NOT NULL, 
          PRIMARY KEY (id), 
          CONSTRAINT unique_vale_evaluacion_dental 
            UNIQUE (id_vale, id_evaluacion_dental)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
        ');

        $this->addSql('ALTER TABLE almacen.vale_evaluacion_dental ADD CONSTRAINT fk_vale_evaluacion_dental FOREIGN KEY (id_vale) REFERENCES almacen.vale (id);');
        $this->addSql('ALTER TABLE almacen.vale_evaluacion_dental ADD CONSTRAINT fk_evaluacion_dental_vale FOREIGN KEY (id_evaluacion_dental) REFERENCES clinico.evaluacion_dental (id);');

    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');


        //Corrigiendo almacén por cambios en clínico
        $this->addSql('DROP TABLE IF EXISTS almacen.vale_evaluacion_dental');

        $this->addSql(' 
        CREATE TABLE almacen.vale_ficha_tratamiento (
          id                   int(11) NOT NULL AUTO_INCREMENT, 
          id_vale              int(11) NOT NULL, 
          id_ficha_tratamiento int(11) NOT NULL, 
          creado               timestamp NOT NULL, 
          PRIMARY KEY (id), 
          CONSTRAINT unique_vale_ficha_tratamiento 
            UNIQUE (id_vale, id_ficha_tratamiento)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
        ');

        $this->addSql('ALTER TABLE almacen.vale_ficha_tratamiento ADD CONSTRAINT fk_vale_ficha_tratamiento FOREIGN KEY (id_vale) REFERENCES almacen.vale (id);');
        $this->addSql('ALTER TABLE almacen.vale_ficha_tratamiento ADD CONSTRAINT fk_ficha_tratamiento_vale FOREIGN KEY (id_ficha_tratamiento) REFERENCES clinico.ficha_tratamiento (id);');


        //20181009144922
        $this->addSql('ALTER TABLE clinico.evaluacion_dental DROP COLUMN retratamiento;');


        //20181009022750
        $this->addSql('ALTER TABLE clinico.ficha_tratamiento ADD CONSTRAINT fk_ficha_tratamiento FOREIGN KEY (id_ficha) REFERENCES clinico.ficha (id);');
        $this->addSql('ALTER TABLE clinico.ficha_tratamiento ADD CONSTRAINT fk_tratamiento_ficha FOREIGN KEY (id_tratamiento) REFERENCES clinico.tratamiento (id);');
        $this->addSql('ALTER TABLE clinico.ficha_tratamiento ADD id_ficha int(11) NOT NULL;');
        $this->addSql('ALTER TABLE clinico.ficha_tratamiento ADD id_tratamiento int(11) NOT NULL;');
        $this->addSql('ALTER TABLE clinico.ficha_tratamiento ADD pieza_dental varchar(10) NOT NULL;');
        $this->addSql('ALTER TABLE clinico.ficha_tratamiento ADD cantidad int(11);');
        $this->addSql('ALTER TABLE clinico.ficha_tratamiento DROP FOREIGN KEY fk_ficha_tratamiento_evaluacion;');
        $this->addSql('ALTER TABLE clinico.ficha_tratamiento DROP COLUMN id_evaluacion_dental;');
        $this->addSql('ALTER TABLE clinico.ficha_tratamiento DROP FOREIGN KEY fk_ficha_evaluacion_cuenta;');
        $this->addSql('ALTER TABLE clinico.ficha_tratamiento DROP COLUMN id_cuenta;');
    }
}
