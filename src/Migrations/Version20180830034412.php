<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Class Version20180830034412
 * @package DoctrineMigrations
 */
final class Version20180830034412 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');
        $this->addSql('
CREATE OR REPLACE VIEW rotacion.fechas_rotacion_view AS (
SELECT
  resumen.grupo AS grupo,
  resumen.rotacion AS rotacion,
  resumen.id_grupo AS id_grupo,
  resumen.periodoCiclo AS periodoCiclo,
  MIN(CASE WHEN resumen.programa <> \'PPE\' THEN resumen.fecha ELSE NULL END) INICIO,
  MAX(CASE WHEN resumen.programa <> \'PPE\' THEN resumen.fecha ELSE NULL END) FIN,
  str_to_date(sum(CASE WHEN (resumen.tipo = \'INICIO\' AND resumen.programa = \'CSNS\') THEN resumen.fecha ELSE NULL END),\'%Y%m%d\') AS inicio_csns,
  str_to_date(sum(CASE WHEN (resumen.tipo = \'INICIO\' AND resumen.programa = \'CESA\') THEN resumen.fecha ELSE NULL END),\'%Y%m%d\') AS inicio_cesa,
  str_to_date(sum(CASE WHEN (resumen.tipo = \'INICIO\' AND resumen.programa = \'HNR\') THEN resumen.fecha ELSE NULL END),\'%Y%m%d\') AS inicio_hnr,
  str_to_date(sum(CASE WHEN (resumen.tipo = \'DIA UNICO\' AND resumen.programa = \'ULC\') THEN resumen.fecha ELSE NULL END),\'%Y%m%d\') AS inicio_ulc,
  str_to_date(sum(CASE WHEN (resumen.tipo = \'DIA UNICO\' AND resumen.programa = \'CLC\') THEN resumen.fecha ELSE NULL END),\'%Y%m%d\') AS inicio_clc,
  str_to_date(sum(CASE WHEN (resumen.tipo = \'INICIO\' AND resumen.programa = \'PPE\') THEN resumen.fecha ELSE NULL END),\'%Y%m%d\') AS inicio_ppe,
  str_to_date(sum(CASE WHEN (resumen.tipo = \'FIN\' AND resumen.programa = \'CSNS\') THEN resumen.fecha ELSE NULL END),\'%Y%m%d\') AS fin_csns,
  str_to_date(sum(CASE WHEN (resumen.tipo = \'FIN\' AND resumen.programa = \'CESA\') THEN resumen.fecha ELSE NULL END),\'%Y%m%d\') AS fin_cesa,
  str_to_date(sum(CASE WHEN (resumen.tipo = \'FIN\' AND resumen.programa = \'HNR\') THEN resumen.fecha ELSE NULL END),\'%Y%m%d\') AS fin_hnr,
  str_to_date(sum(CASE WHEN (resumen.tipo = \'FIN\' AND resumen.programa = \'PPE\') THEN resumen.fecha ELSE NULL END),\'%Y%m%d\') AS fin_ppe
FROM (
  SELECT
    a.fecha AS fecha,
    a.tipo AS tipo,
    b.codigo AS programa,
    c.id AS id_grupo,
    c.nombre AS rotacion,
    d.nombre AS grupo,
    e.id_periodo AS periodoCiclo
  FROM
    rotacion.programa_calendario a
    JOIN rotacion.programa b ON(a.id_programa = b.id)
    JOIN rotacion.grupo c ON(a.id_grupo = c.id)
    JOIN rotacion.grupo d ON(c.id_padre = d.id)
    JOIN academico.periodo_ciclo e ON(a.id_periodo_ciclo = e.id)
  ) resumen
GROUP BY resumen.periodoCiclo,resumen.rotacion,resumen.periodoCiclo
ORDER BY resumen.periodoCiclo,resumen.periodoCiclo
)');

    }

    public function down(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('
CREATE OR REPLACE VIEW rotacion.fechas_rotacion_view AS (
SELECT
  resumen.grupo AS grupo,
  resumen.rotacion AS rotacion,
  resumen.periodoCiclo AS periodoCiclo,
  str_to_date(sum(CASE WHEN (resumen.tipo = \'INICIO\' AND resumen.programa = \'CSNS\') THEN resumen.fecha ELSE NULL END),\'%Y%m%d\') AS inicio_csns,
  str_to_date(sum(CASE WHEN (resumen.tipo = \'INICIO\' AND resumen.programa = \'CESA\') THEN resumen.fecha ELSE NULL END),\'%Y%m%d\') AS inicio_cesa,
  str_to_date(sum(CASE WHEN (resumen.tipo = \'INICIO\' AND resumen.programa = \'HNR\') THEN resumen.fecha ELSE NULL END),\'%Y%m%d\') AS inicio_hnr,
  str_to_date(sum(CASE WHEN (resumen.tipo = \'DIA UNICO\' AND resumen.programa = \'ULC\') THEN resumen.fecha ELSE NULL END),\'%Y%m%d\') AS inicio_ulc,
  str_to_date(sum(CASE WHEN (resumen.tipo = \'DIA UNICO\' AND resumen.programa = \'CLC\') THEN resumen.fecha ELSE NULL END),\'%Y%m%d\') AS inicio_clc,
  str_to_date(sum(CASE WHEN (resumen.tipo = \'INICIO\' AND resumen.programa = \'PPE\') THEN resumen.fecha ELSE NULL END),\'%Y%m%d\') AS inicio_ppe,
  str_to_date(sum(CASE WHEN (resumen.tipo = \'FIN\' AND resumen.programa = \'CSNS\') THEN resumen.fecha ELSE NULL END),\'%Y%m%d\') AS fin_csns,
  str_to_date(sum(CASE WHEN (resumen.tipo = \'FIN\' AND resumen.programa = \'CESA\') THEN resumen.fecha ELSE NULL END),\'%Y%m%d\') AS fin_cesa,
  str_to_date(sum(CASE WHEN (resumen.tipo = \'FIN\' AND resumen.programa = \'HNR\') THEN resumen.fecha ELSE NULL END),\'%Y%m%d\') AS fin_hnr,
  str_to_date(sum(CASE WHEN (resumen.tipo = \'FIN\' AND resumen.programa = \'PPE\') THEN resumen.fecha ELSE NULL END),\'%Y%m%d\') AS fin_ppe
FROM (
  SELECT
    a.fecha AS fecha,
    a.tipo AS tipo,
    b.codigo AS programa,
    c.nombre AS rotacion,
    d.nombre AS grupo,
    e.id_periodo AS periodoCiclo
  FROM
    rotacion.programa_calendario a
    JOIN rotacion.programa b ON(a.id_programa = b.id)
    JOIN rotacion.grupo c ON(a.id_grupo = c.id)
    JOIN rotacion.grupo d ON(c.id_padre = d.id)
    JOIN academico.periodo_ciclo e ON(a.id_periodo_ciclo = e.id)
  ) resumen
GROUP BY resumen.periodoCiclo,resumen.rotacion,resumen.periodoCiclo
ORDER BY resumen.periodoCiclo,resumen.periodoCiclo
)');

    }
}
