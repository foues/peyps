<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Class Version20180909172631
 * @package DoctrineMigrations
 */
final class Version20180909172631 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        //nueva vista
        $this->addSql('
        create or replace view rotacion.vw_fechas_rotacion AS
        select id_periodo_ciclo,id_rotacion,id_grupo,rotacion,programa,
          case 
          when a.programa in (\'ulc\',\'clc\') then DATE_FORMAT(a.inicio,\'%d-%m\')
          else CONCAT(DATE_FORMAT(a.inicio,\'%d-%m\'), \' / \', DATE_FORMAT(a.fin,\'%d-%m\'))
          end fechas
          FROM
        (
        select a.id_periodo_ciclo,c.numero rotacion,c.id id_rotacion,a.id_grupo,b.codigo programa,
              min(fecha) inicio,max(fecha) fin
        from rotacion.programa_calendario a
        join rotacion.programa b
        on a.id_programa=b.id
        join rotacion.rotacion c
        on a.id_grupo=c.id_grupo
        group by a.id_periodo_ciclo,b.codigo,a.id_grupo,c.numero,c.id
        order by 1,2,3
        ) a ');

        //cambio
        $this->addSql('
            create or replace view rotacion.rotacion_detalle_view_completa AS
            select a.curso,a.carnet,a.nombre,a.rotacion,
               case 
                  when a.rotacion between 1 and 3 then \'A\'
                  when a.rotacion between 4 and 6 then \'B\'
                  else \'C\' 
              END tipo_rotacion,
                  a.periodo_ciclo,a.nombre_periodo_ciclo,
                  a.programa,a.institucion,b.fechas
            from (
                  SELECT 
                      i.nombre AS curso,
                      l.carnet,
                      concat(n.nombre,\' \',n.apellido) nombre,
                      b.numero AS rotacion,
                      g.id_periodo_ciclo periodo_ciclo,
                      m.nombre nombre_periodo_ciclo,
                      e.codigo AS programa,
                      d.nombre AS institucion,
                      b.inicio AS inicio,
                      b.fin AS fin,
                      CONCAT(DATE_FORMAT(b.inicio,\'%d-%m\'),\' / \', DATE_FORMAT(b.fin,\'%d-%m\')) AS fechas
                    FROM rotacion.rotacion_estudiante a
                      JOIN rotacion.rotacion b
                        ON (a.id_rotacion = b.id)
                      JOIN rotacion.programa_institucion c
                        ON (a.id_programa_institucion = c.id)
                      JOIN administracion.institucion d
                        ON (c.id_institucion = d.id)
                      JOIN rotacion.programa e
                        ON (c.id_programa = e.id)
                      JOIN rotacion.grupo_estudiante f
                        ON (a.id_grupo_estudiante = f.id)
                      join rotacion.grupo g
                      on f.id_grupo=g.id
                      JOIN academico.estudiante_curso h
                        ON (f.id_estudiante_curso = h.id)
                      JOIN academico.curso i
                        ON (h.id_curso = i.id)
                      JOIN academico.estudiante_curso j
                       ON f.id_estudiante_curso=j.id
                      JOIN academico.estudiante_plan_estudio k
                      ON j.id_estudiante_plan_estudio=k.id
                      JOIN academico.estudiante l
                      on k.id_estudiante=l.id
                      JOIN academico.periodo m
                      on g.id_periodo_ciclo=m.id
                      join administracion.persona n
                      on l.id_persona=n.id
                    order by periodo_ciclo,rotacion,programa
              ) a 
            join rotacion.vw_fechas_rotacion b
            on a.periodo_ciclo=b.id_periodo_ciclo and a.rotacion=b.rotacion and a.programa=b.programa
            ');

        //modificacion
        $this->addSql('
        create or replace view rotacion.rotacion_resumen_email AS
        select curso,periodo_ciclo,nombre_periodo_ciclo,tipo_rotacion,programa,
          max(case when programa=\'ppe\' or rotacion  in(1,4) then fechas else null end) rotacion_1,
          max(case when programa=\'ppe\' or  rotacion  in(2,5) then fechas else null end) rotacion_2,
          max(case when programa=\'ppe\' or  rotacion  in(3,6) then fechas else null end) rotacion_3
          from rotacion.rotacion_detalle_view_completa
          group by curso,tipo_rotacion,programa,periodo_ciclo,nombre_periodo_ciclo
        order by 2,1,4');
    }

    public function down(Schema $schema): void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('
        create or replace view rotacion.rotacion_resumen_email AS
        select curso,periodo_ciclo,nombre_periodo_ciclo,programa,
          max(case when programa=\'ppe\' or rotacion =1 then fechas else null end) rotacion_1,
          max(case when programa=\'ppe\' or rotacion =2 then fechas else null end) rotacion_2,
          max(case when programa=\'ppe\' or rotacion =3 then fechas else null end) rotacion_3,
          max(case when programa=\'ppe\' or rotacion =4 then fechas else null end) rotacion_4,
          max(case when programa=\'ppe\' or rotacion =5 then fechas else null end) rotacion_5,
          max(case when programa=\'ppe\' or rotacion =6 then fechas else null end) rotacion_6
          from rotacion.rotacion_detalle_view
          group by curso,programa,periodo_ciclo,nombre_periodo_ciclo
        order by 2,1');

        $this->addSql('DROP VIEW IF EXISTS rotacion.rotacion_detalle_view_completa');
        $this->addSql('DROP VIEW IF EXISTS rotacion.vw_fechas_rotacion');

    }
}
