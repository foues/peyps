<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Class Version20180830165410
 * @package DoctrineMigrations
 */
final class Version20180830165410 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        //Version20180828030210
        $this->addSql('ALTER TABLE administracion.telefono ADD contacto varchar(50)');

    }

    public function down(Schema $schema): void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        //Version20180828030210
        $this->addSql('ALTER TABLE administracion.telefono DROP COLUMN contacto');

    }
}
