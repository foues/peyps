<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Class Version20180901174749
 * @package DoctrineMigrations
 */
final class Version20180901174749 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');
        $this->addSql('
        create or replace view rotacion.vw_cupos_por_rotacion AS
        select e.id_periodo_ciclo,a.id_rotacion rotacion,d.numero AS nombreRotacion,f.codigo programa,b.id_institucion,c.nombre nombre_institucion,
            round(max(case when f.codigo=\'ppe\' then b.cantidad_estudiante else b.cantidad_estudiante/2 end),0) cupos_max,
              COUNT(0) activos
          from rotacion.rotacion_estudiante a
          join rotacion.programa_institucion b
          on a.id_programa_institucion=b.id
          join administracion.institucion c
          on b.id_institucion=c.id
          join rotacion.rotacion d
          on a.id_rotacion=d.id
          join rotacion.grupo e
          on d.id_grupo=e.id
          join rotacion.programa f
          on b.id_programa=f.id
        GROUP BY e.id_periodo_ciclo,a.id_rotacion,c.nombre,f.codigo,b.id_institucion
        order by 2,3
        ');
    }

    public function down(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP VIEW IF EXISTS rotacion.vw_cupos_por_rotacion');
    }
}
