<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Class Version20190211052130
 * @package DoctrineMigrations
 */
final class Version20190211052130 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        //Corrección de llave foránea 20181009022750
        $this->addSql('ALTER TABLE clinico.ficha DROP FOREIGN KEY fk_ficha_evaluacion_cuenta;');
        $this->addSql('ALTER TABLE clinico.ficha_tratamiento ADD CONSTRAINT fk_ficha_evaluacion_cuenta FOREIGN KEY (id_cuenta) REFERENCES administracion.cuenta (id);');

        $this->addSql('DELETE FROM almacen.consumo');
        $this->addSql('DELETE FROM almacen.vale_evaluacion_dental');
        $this->addSql('ALTER TABLE almacen.vale ADD extra tinyint(3) DEFAULT 0');


        //20181022083038
        $this->addSql('CREATE TABLE clinico.datos_clinicos (
            id int(11) NOT NULL AUTO_INCREMENT, 
            id_ficha int(11) NOT NULL, 
            semanas_embarazo int(11),  
            numero_embarazos int(11),
            fur int(11), 
            cuartos int(11), 
            habitantes int(11), 
            personas int(11), 
            leche int(11),
            huevo int(11),
            carne int(11),
            frutas int(11),
            verduras int(11), 
            legumbres int(11), 
            cereales int(11),
            bano int(11), 
            ropa int(11), 
            cepillo int(11), 
            hilo int(11), 
            enjuague int(11),  
            localizacion_lesion varchar(25),  
            color_lesion varchar(25), 
            tamano_lesion varchar(25),
            pulso int(11), 
            frecuencia_respiratoria int(11), 
            frecuencia_cardiaca int(11), 
            presion_arterial int(11), 
            peso int(11), 
            estatura int(11), 
            CONSTRAINT datos_clinicos_pk 
              PRIMARY KEY (id)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
          ');
        $this->addSql('ALTER TABLE clinico.datos_clinicos ADD CONSTRAINT fk_ficha_datos FOREIGN KEY (id_ficha) REFERENCES clinico.ficha (id)');


        //20181022091816
        $this->addSql('ALTER TABLE clinico.ficha ADD id_transfusiones int(11);');
        $this->addSql('ALTER TABLE clinico.ficha ADD CONSTRAINT fk_transfusiones FOREIGN KEY (id_transfusiones) REFERENCES clinico.ficha_catalogo (id);');
        $this->addSql('ALTER TABLE clinico.ficha ADD id_orientacion_sexual int(11);');
        $this->addSql('ALTER TABLE clinico.ficha ADD CONSTRAINT fk_orientacion_sexual FOREIGN KEY (id_orientacion_sexual) REFERENCES clinico.ficha_catalogo (id);');
        $this->addSql('ALTER TABLE clinico.ficha ADD id_material_casa int(11);');
        $this->addSql('ALTER TABLE clinico.ficha ADD CONSTRAINT fk_material_casa FOREIGN KEY (id_material_casa) REFERENCES clinico.ficha_catalogo (id);');
        $this->addSql('ALTER TABLE clinico.ficha ADD id_lesion int(11);');
        $this->addSql('ALTER TABLE clinico.ficha ADD CONSTRAINT fk_lesion FOREIGN KEY (id_lesion) REFERENCES clinico.ficha_catalogo (id);');
        $this->addSql('ALTER TABLE clinico.ficha ADD id_tipo_lesion int(11);');
        $this->addSql('ALTER TABLE clinico.ficha ADD CONSTRAINT fk_tipo_lesion FOREIGN KEY (id_tipo_lesion) REFERENCES clinico.ficha_catalogo (id);');
        $this->addSql('ALTER TABLE clinico.ficha ADD id_consistencia_lesion int(11);');
        $this->addSql('ALTER TABLE clinico.ficha ADD CONSTRAINT fk_consistencia_lesion FOREIGN KEY (id_consistencia_lesion) REFERENCES clinico.ficha_catalogo (id);');


        //20181022104033
        $this->addSql('ALTER TABLE clinico.datos_clinicos ADD numero_abortos int(11);');

    }

    public function down(Schema $schema): void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        //20181022104033
        $this->addSql('ALTER TABLE clinico.datos_clinicos DROP COLUMN numero_abortos;');


        //20181022091816
        $this->addSql('ALTER TABLE clinico.ficha DROP FOREIGN KEY fk_transfusiones;');
        $this->addSql('ALTER TABLE clinico.ficha DROP COLUMN id_transfusiones;');
        $this->addSql('ALTER TABLE clinico.ficha DROP FOREIGN KEY fk_orientacion_sexual;');
        $this->addSql('ALTER TABLE clinico.ficha DROP COLUMN id_orientacion_sexual;');
        $this->addSql('ALTER TABLE clinico.ficha DROP FOREIGN KEY fk_material_casa;');
        $this->addSql('ALTER TABLE clinico.ficha DROP COLUMN id_material_casa;');
        $this->addSql('ALTER TABLE clinico.ficha DROP FOREIGN KEY fk_lesion;');
        $this->addSql('ALTER TABLE clinico.ficha DROP COLUMN id_lesion;');
        $this->addSql('ALTER TABLE clinico.ficha DROP FOREIGN KEY fk_tipo_lesion;');
        $this->addSql('ALTER TABLE clinico.ficha DROP COLUMN id_tipo_lesion;');
        $this->addSql('ALTER TABLE clinico.ficha DROP FOREIGN KEY fk_consistencia_lesion;');
        $this->addSql('ALTER TABLE clinico.ficha DROP COLUMN id_consistencia_lesion;');


        //20181022083038
        $this->addSql('ALTER TABLE clinico.datos_clinicos DROP FOREIGN KEY fk_ficha_datos;');
        $this->addSql('DROP TABLE clinico.datos_clinicos');


        //Corrección de llave foránea 20181009022750
        $this->addSql('ALTER TABLE almacen.vale DROP COLUMN extra;');

        $this->addSql('ALTER TABLE clinico.ficha_tratamiento DROP FOREIGN KEY fk_ficha_evaluacion_cuenta;');
        $this->addSql('ALTER TABLE clinico.ficha ADD CONSTRAINT fk_ficha_evaluacion_cuenta FOREIGN KEY (id_cuenta) REFERENCES administracion.cuenta (id);');
    }
}
