<?php
namespace App\Form\Academico;
use App\Entity\Academico\Evaluacion;
use App\Entity\Academico\EvaluacionTipo;
use App\Entity\Academico\UnidadIntegracion;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\SubmitButton;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UnidadIntegracionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nombre')
            ->add('ponderacion',IntegerType::class,array('attr' => array(
                'min' => '1',
                'max' => '100',
            )));

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => UnidadIntegracion::class,
        ]);
    }
}
