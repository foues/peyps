<?php

namespace App\Form\Clinico\riesgos;

use App\Entity\Clinico\FichaCatalogo;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class RiesgoType extends AbstractType{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        switch ($options['flow_step']) {
            case 1:
                $builder
                    ->add('cpo_c', null, ['label' => 'C', 'attr' => ['readonly' => true]])
                    ->add('cpo_p', null, ['label' => 'P', 'attr' => ['readonly' => true]])
                    ->add('cpo_o', null, ['label' => 'O', 'attr' => ['readonly' => true]])
                    ->add('ceo_c', null, ['label' => 'c', 'attr' => ['readonly' => true]])
                    ->add('ceo_e', null, ['label' => 'e', 'attr' => ['readonly' => true]])
                    ->add('ceo_o', null, ['label' => 'o', 'attr' => ['readonly' => true]])
                    ->add('observaciones_indice', null, ['label' => 'Observaciones', 'required' => false])
                    ->add('total_indice', null, ['label' => 'Total CPO/ceo', 'attr' => ['readonly' => true]]);
                break;
            
            case 2:
                $builder
                    ->add('placa_1655', ChoiceType::class, [
                        'label' => '16/55',
                        'placeholder' => 'Seleccione',
                        'choices' => [0, 1, 2, 3, 4]
                    ])
                    ->add('placa_1151', ChoiceType::class, [
                        'label' => '11/51',
                        'placeholder' => 'Seleccione',
                        'choices' => [0, 1, 2, 3, 4]
                    ])
                    ->add('placa_2665', ChoiceType::class, [
                        'label' => '26/65',
                        'placeholder' => 'Seleccione',
                        'choices' => [0, 1, 2, 3, 4]
                    ])
                    ->add('placa_3675', ChoiceType::class, [
                        'label' => '36/75',
                        'placeholder' => 'Seleccione',
                        'choices' => [0, 1, 2, 3, 4]
                    ])
                    ->add('placa_3171', ChoiceType::class, [
                        'label' => '31/71',
                        'placeholder' => 'Seleccione',
                        'choices' => [0, 1, 2, 3, 4]
                    ])
                    ->add('placa_4685', ChoiceType::class, [
                        'label' => '46/85',
                        'placeholder' => 'Seleccione',
                        'choices' => [0, 1, 2, 3, 4]
                    ])
                    ->add('total_placa', null, ['label' => 'Total placa', 'attr' => ['readonly' => true]])
                    ->add('comentarios_placa', null, ['label' => 'Comentarios', 'required' => false]);
                break;

            case 3:
                $builder
                    ->add('ingesta_azucar', EntityType::class, $this->getCatalog('t.codigo = \'historia_ingesta\'', true, true), ['label' => 'Ingesta de azucar',  "attr"=>['class'=>'switch eval_sis']]);
                break;

            case 4:
                $builder
                    ->add('total_indice', null, ['label' => 'Total CPO/ceo', 'attr' => ['readonly' => true]])
                    ->add('total_placa', null, ['label' => 'Total placa', 'attr' => ['readonly' => true]])
                    ->add('total_ingesta', null, ['label' => 'Total ingesta', 'attr' => ['readonly' => true]])
                    ->add('diagnostico', null, ['label' => 'Diagnostico de riesgo', 'attr' => ['readonly' => true]])
                    ->add('riesgo_real', EntityType::class, $this->getCatalog('t.codigo = \'riesgo_cariogenico\'', false, false), ['label' => 'Diagnostico de riesgo real'])
                    ->add('condiciones', null, ['label' => 'Otras Condiciones', 'required' => false]);
        }

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'App\Form\Clinico\riesgos\CrearRiesgo',
        ]);
    }

    public function getName() {
		return $this->getBlockPrefix();
    }

    public function getBlockPrefix() {
		return 'riesgoType';
    }

    // Needs a better abstraction, probably just make a funcion to return the query builder with the filter as input, all other options must be chose freely
    private function getCatalog($filter, $expanded=false, $multiple=false){
        return array('class'=>FichaCatalogo::class,'query_builder' => function(EntityRepository $er) use ($filter){
            return $er->createQueryBuilder('e')->innerJoin('e.ficha_catalogo_tipo', 't')->andWhere($filter);
        }, 'choice_label' => 'nombre', 'expanded' => $expanded, 'multiple' => $multiple);
    }

}
