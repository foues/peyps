<?php

namespace App\Form\Clinico\riesgos;

use Symfony\Component\HttpFoundation\Request;
use Craue\FormFlowBundle\Form\FormFlow;
use Craue\FormFlowBundle\Form\FormFlowInterface;

class RiesgoFlow extends FormFlow {

	protected $allowDynamicStepNavigation = true;

    protected function loadStepsConfig() {
		return array(
			array(
				'label' => 'Indices cariogenicos',
				'form_type' => 'App\Form\Clinico\riesgos\RiesgoType',
            ),
            array(
				'label' => 'Placa bacteriana',
				'form_type' => 'App\Form\Clinico\riesgos\RiesgoType',
			),
			array(
				'label' => 'Ingesta de azucar',
				'form_type' => 'App\Form\Clinico\riesgos\RiesgoType',
			),
			array(
				'label' => 'Nivel de riesgo',
				'form_type' => 'App\Form\Clinico\riesgos\RiesgoType',
			),
		);
	}

}