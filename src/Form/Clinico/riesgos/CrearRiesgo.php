<?php

namespace App\Form\Clinico\riesgos;

use Symfony\Component\Validator\Constraints as Assert;

class CrearRiesgo {

    public $cpo_c;
    public $cpo_p;
    public $cpo_o;
    public $ceo_c;
    public $ceo_e;
    public $ceo_o;

    /**
     * @Assert\Type("string")
     * @Assert\Length(min="3", max="999")
     * @var string
     */
    public $observaciones_indice;
    public $total_indice;
    
    /**
     * @Assert\NotBlank()
     * @Assert\Type("integer")
     * @Assert\Range(min=0, max=4, minMessage="Debe ser mayor a 0", maxMessage="No puede exceder a 4")
     * @var integer
     */
    public $placa_1655;
    /**
     * @Assert\NotBlank()
     * @Assert\Type("integer")
     * @Assert\Range(min=0, max=4, minMessage="Debe ser mayor a 0", maxMessage="No puede exceder a 4")
     * @var integer
     */
    public $placa_1151;
    /**
     * @Assert\NotBlank()
     * @Assert\Type("integer")
     * @Assert\Range(min=0, max=4, minMessage="Debe ser mayor a 0", maxMessage="No puede exceder a 4")
     * @var integer
     */
    public $placa_2665;
    /**
     * @Assert\NotBlank()
     * @Assert\Type("integer")
     * @Assert\Range(min=0, max=4, minMessage="Debe ser mayor a 0", maxMessage="No puede exceder a 4")
     * @var integer
     */
    public $placa_3675;
    /**
     * @Assert\NotBlank()
     * @Assert\Type("integer")
     * @Assert\Range(min=0, max=4, minMessage="Debe ser mayor a 0", maxMessage="No puede exceder a 4")
     * @var integer
     */
    public $placa_3171;
    /**
     * @Assert\NotBlank()
     * @Assert\Type("integer")
     * @Assert\Length(min="1", max="1")
     * @Assert\Range(min=0, max=4, minMessage="Debe ser mayor a 0", maxMessage="No puede exceder a 4")
     * @var integer
     */
    public $placa_4685;

    /**
     * @Assert\Type("string")
     * @Assert\Length(min="3", max="999")
     * @var string
     */
    public $comentarios_placa;
    public $total_placa;

    public $ingesta_azucar;
    public $total_ingesta;

    /**
     * @Assert\Type("string")
     * @Assert\Length(min="3", max="999")
     * @var string
     */
    public $condiciones;
    public $diagnostico;
    public $riesgo_real;

}
