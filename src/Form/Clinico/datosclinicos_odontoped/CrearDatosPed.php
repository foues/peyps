<?php

namespace App\Form\Clinico\datosclinicos_odontoped;

use Symfony\Component\Validator\Constraints as Assert;


class CrearDatosPed {

    public $ciclo;
    public $curso;

    public $fecha;

    /**
     * @Assert\NotBlank()
     * @Assert\Type("string")
     * @Assert\Length(min="3", max="999")
     * @var string
     */
    public $motivo;

     /**
     * @Assert\NotBlank()
     * @Assert\Type("string")
     * @Assert\Length(min="3", max="999")
     * @var string
     */
    public $historia;

    public $cardiopatias;
    public $diabetes;
    public $alergias;
    public $cancer;
    public $infectocontagiosas;
    public $endocrinopatias;
    public $trastornos_renales;
    public $otros;
    public $otro_detalle;
    public $trastornos_sanguineos;
    public $trastornos_respiratorios;
    public $enfermedad_genetica;

    public $antecedentesFam;
    public $OtrosAntecFam;


    public $Enfermedades_Durante_el_Embarazo;
    public $otras_Enfermedades_Durante_el_Embarazo;
    public $medicamentos_durante_embarazo;
    public $otros_medicamentos_durante_embarazo;


    public $PartoNumero;
    public $Valores;
    public $PesoAlNacer;
    public $PesoAlNacer_units;
    public $TallaAlNacer;
    public $DificultadesNeonatales;
    public $ExpliqueDifNeoNat;
    public $AlimentacionMaterna;
    public $EdadALimMat;
    public $Duracion;
    public $Duracion_units;

    public $LecheCompleta;
    public $EdadLecheCompleta;
    public $EdadLecheCompleta_units;
    public $Formula;
    public $EdadFormula;
    public $EdadFormula_units;
    public $AlimentosSolidos;
    public $EdadAlimentosSolidos;
    public $EdadAlimentosSolidos_units;
    public $DesarrolloFisicomotor;
    public $EdadEnQueGateo;
    public $EdadEnQueGateo_units;
    public $EdadEnQueCamino;
    public $EdadEnQueCamino_units;
    public $EdadEnQueHablo;
    public $EdadEnQueHablo_units;
    public $visita_odontologo;
    public $SistemaNervioso;
    public $SistemaCardiovascular;
    public $SistemaHematopoyetico;
    public $SistemaRespiratorio;
    public $SistemaGastrointestinal;
    public $SistemaEndocrino;
    public $SistemaGenitourinario;
    public $PielYMucosa;
    public $TrastornosSensoriales;
    public $Alergias;
    public $enfermedadesInfecciosasPadecidas;
    public $OtrasEnfermeInf;

    public $HaAsistidoAConsultaEnLosUltimos6Meses;
    public $PorCualMotivo;
    public $SeEncuentraBajoTratamiento;
    public $MotivoDeTratamiento;
    public $MedicamentosQueTomaActualmente;
    public $IntervencionesQuirurgicas;
    public $MotivoDeIntervencion;
    public $HaSidoHospitalizado;
    public $MotivoHospitalizacion;
    public $VacunasRecibidas;
    public $OtrasVacunas;

    public $TodosSusRefuerzosRecibidosHastaLaFecha;
    public $EsquemaDeVacunacion;
    public $EsquemaIncompleto;

    public $EdadErupcionDeDientesPrimarios;
    public $EdadErupcionDeDientesPermanentes;
    public $TraumatismosDentarios;
    public $EdadTraumatismoDentario;
    public $TratamientoRecibido;
    public $HaAsistidoAnteriormenteAlOdontologo;
    public $VecesAlDiaQueSeCepilla;
    public $SeCepillaSolo;
    public $QuienLoCepilla;
    public $CremaDentalQueUtiliza;
    public $UtilizaSedaDentalOEnjuague;

    public $AlimentosIngeridosAlMenosUnaVezPorSemana;
    public $CuantasMeriendasHaceEntreComidas;
    public $QueCome;
    public $PrefiereAlimentosBlandos;
    public $CualesAlimentosBlandos;
    public $AlimentosSolidosQuePrefiere;
    public $Biberon;
    public $ContenidoDelBiberon;
    public $TomaLeche;
    public $CuantosVasos;
    public $ConQueTomaSuLeche;
    public $ConsumeMuchaSal;
    public $ConsumeMuchosCitricos;
    public $DesayunoHabitual;
    public $MeriendaHabitual;
    public $AlmuerzoHabitual;
    public $MeriendaHabitualTarde;
    public $CenaHabitual;

    public $TallaActual;
    public $PesoActual;
    public $Perfil;
    public $Configuracion;
    public $Labios;
    public $TamanioDeNariz;
    public $InsercionDeOrejas;
    public $Observaciones;
    public $Descripcion;

    public $Encias;
    public $MucosaBucal;
    public $Lengua;
    public $FrenilloLabialSuperior;
    public $FrenilloLabialInferior;
    public $FrenilloInferior;
    public $PisoBucal;

    public $DescEncia;
    public $DescMucosaBucal;
    public $DescPisoBucal;
    public $DescPaladarDuro;
    public $DescPaladarBlando;
    public $DescAmigdalas;

    public $DesviacionATM;
    public $Apertura;
    public $Cierre;
    public $Ruidos;
    public $Saltos;

    public $HabitosParafuncionales;
    public $ExpliqueHabitosParafuncionales;
    public $DuracionHabitosParaf;
    public $IntensidadHabitosParaf;
    public $FrecuenciaHabitosParaf;

    public $RelacionMolarDerecha;
    public $RelacionMolarIzquierda;
    public $RelacionCaninaDerecha;
    public $RelacionCaninaIzquierda;
    public $MordidaAbierta;
    public $MordidaCruzada;
    public $MordidaCruzadaOpts;
    public $LineaMediaSuperiorLado;
    public $LineaMediaInferiorLado;

    public $Overjet;
    public $OverjetOpts;
    public $Overbite;
    public $OverbiteOpts;
    public $LineaMediaSuperior;
    public $LineaMediaSuperiorOpts;
    public $LineaMediaInferior;
    public $LineaMediaInferiorOpts;
    public $Dientes;
    public $Diastemas;
    public $EspacioDePrimateSup;
    public $EspacioDePrimateInf;
    public $Apiniamiento;
    public $PerdidasPrematura;


    public $TipoTraumatismo;
    public $EdadTraumatismoDentario_units;
    public $OtrasObservaciones;
    public $ExamenFuncional;
    public $SeDuermeConelB;

    public $reacciones_adversas;


    public $TA60;
    public $TA61;
    public $TA62;
    public $TA63;
    public $TA64;
    public $TA65;
    public $TA66;
    public $TA67;
    public $TA68;
    public $TA69;
    public $TA70;
    public $TA71;
    public $TA72;
    public $TA73;
    public $TA74;
    public $TA75;
    public $TA76;
    public $TA77;
    public $TA78;
    public $TA79;
    public $TA80;
    public $TA81;

    public $PesoActual_units;
    public $TallaActual_units;

    public $Hobby;
    public $Deportes;
    public $Mascotas;
    public $ProgramasTV;
    public $Musica;
    public $Fobias;
    public $Recompensas;
    public $Castigo;
    public $OtrasActividades;
    public $Aprendizaje;

    public $Hobby_exp;
    public $Deportes_exp;
    public $Mascotas_exp;
    public $ProgramasTV_exp;
    public $Musica_exp;
    public $Fobias_exp;
    public $Recompensas_exp;
    public $Castigo_exp;
    public $OtrasActividades_exp;
    public $Aprendizaje_exp;
    public $Exp_enConsulta;

    public $DiagnosticoMedico;
    public $DiagnosticoPeriodontal;
    public $TipoDeCaries;
    public $AlteracionesPulpares;
    public $PatologiasTejBlandoDuro;
    public $Maloclusiones;
    public $RiesgoCariogenico;

    public $DiastemasCheck;
    public $PerdidasPrematuraCheck;




    /**
     * @Assert\Type("string")
     * @Assert\Length(max="255")
     * @var string
     */
    public $tipo_reaccion;

    public $trauma_dento;

    /**
     * @Assert\Type("integer")
     * @Assert\Length(min=2, max="2")
     * @var integer
     */
    public $pieza_trauma;

    /* Agregar aditamientos higiene */
    public $aditamientos_higiene;

    public $frecuencia_cepillado;

    public $eval_sistemica;

    public $habitos_bucales;

    public $padecimiento_bucal;

    public $padecimientos_generales;

    public $medicamentos;

    public $operaciones;

    public $productos;

    public $sustancias;

    public $transfusiones;

    public $orientacion_sexual;

    /* Mujeres */
    public $isMujer;
    public $mujeres;
    /**
     * @Assert\Type("integer")
     * @Assert\Length(min=2, max="2")
     * @var integer
     */
    public $semanas_embarazo;
    /**
     * @Assert\Type("integer")
     * @Assert\Length(min=2, max="2")
     * @var integer
     */
    public $numero_embarazos;
    /**
     * @Assert\Type("integer")
     * @Assert\Length(min=2, max="2")
     * @var integer
     */
    public $numero_abortos;

    public $fur;

    public $enfermedades;

    public $familiares;

    public $molestias;

    /* Vivienda */

    public $material;
    /**
     * @Assert\Type("integer")
     * @Assert\Length(min=0, max="2")
     * @var integer
     */
    public $cuartos;
    /**
     * @Assert\Type("integer")
     * @Assert\Length(min=0, max="2")
     * @var integer
     */
    public $habitantes;
    /**
     * @Assert\Type("integer")
     * @Assert\Length(min=0, max="2")
     * @var integer
     */
    public $personas;
    public $servicios;

    /* Alimentacion */
    /**
     * @Assert\Type("integer")
     * @Assert\Length(min=0, max="3")
     * @var integer
     */
    public $leche;
    /**
     * @Assert\Type("integer")
     * @Assert\Length(min=0, max="3")
     * @var integer
     */
    public $huevo;
    /**
     * @Assert\Type("integer")
     * @Assert\Length(min=0, max="3")
     * @var integer
     */
    public $carne;
    /**
     * @Assert\Type("integer")
     * @Assert\Length(min=0, max="3")
     * @var integer
     */
    public $frutas;
    /**
     * @Assert\Type("integer")
     * @Assert\Length(min=0, max="3")
     * @var integer
     */
    public $verduras;
    /**
     * @Assert\Type("integer")
     * @Assert\Length(min=0, max="3")
     * @var integer
     */
    public $legumbres;
    /**
     * @Assert\Type("integer")
     * @Assert\Length(min=0, max="3")
     * @var integer
     */
    public $cereales;

    /* Higiene */

    /**
     * @Assert\Type("integer")
     * @Assert\Length(min=0, max="2")
     * @var integer
     */
    public $bano;
     /**
     * @Assert\Type("integer")
     * @Assert\Length(min=0, max="2")
     * @var integer
     */
    public $ropa;
     /**
     * @Assert\Type("integer")
     * @Assert\Length(min=0, max="2")
     * @var integer
     */
    public $cepillo;
     /**
     * @Assert\Type("integer")
     * @Assert\Length(min=0, max="2")
     * @var integer
     */
    public $hilo;
     /**
     * @Assert\Type("integer")
     * @Assert\Length(min=0, max="2")
     * @var integer
     */
    public $enjuague;

    /* Lesiones */
    public $dolor_boca;
    public $tipo_lesion;
    public $consistencia;
    public $localizacion_lesion;
    public $color_lesion;
    public $tamano_lesion;

    /* Exploracion */
     /**
     * @Assert\Type("integer")
     * @Assert\Length(min=0, max="4")
     * @var integer
     */
    public $pulso;
    /**
     * @Assert\Type("integer")
     * @Assert\Length(min=0, max="4")
     * @var integer
     */
    public $frecuencia_respiratoria;
    /**
     * @Assert\Type("integer")
     * @Assert\Length(min=0, max="4")
     * @var integer
     */
    public $frecuencia_cardiaca;
    /**
     * @Assert\Type("integer")
     * @Assert\Length(min=0, max="4")
     * @var integer
     */
    public $presion_arterial;
    /**
     * @Assert\Type("integer")
     * @Assert\Length(min=0, max="4")
     * @var integer
     */
    public $peso;
    /**
     * @Assert\Type("integer")
     * @Assert\Length(min=0, max="4")
     * @var integer
     */
    public $estatura;

    public $antecedentes;
    public $OtrosAntec;
    public $pregunta;






}
