<?php

namespace App\Form\Clinico\datosclinicos_odontoped;

use App\Entity\Clinico\FichaCatalogo;
use App\Entity\Academico\Curso;
use App\Entity\Clinico\Pregunta;
use App\Entity\Clinico\OpcionesPregunta;
use App\Entity\Clinico\FichaRespuestas;
use App\Entity\Clinico\Respuesta;
use App\Entity\Academico\PlanEstudioCiclo;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class DatosPedType extends AbstractType{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        switch ($options['flow_step']) {
            case 1:
                $builder
                    ->add('curso', EntityType::class, array('class'=>Curso::class, 'choice_label' => 'codigo',), ['label' => 'Curso del estudiante'])
                    ->add('ciclo', EntityType::class, array('class'=>PlanEstudioCiclo::class, 'choice_label' => 'romano',), ['label' => 'Ciclo cursado']);
                break;

            case 2:
                $builder
                    ->add('fecha', DateType::class, ['label' => 'Fecha de control', 'widget' => 'single_text'])
                    // ->add('motivo')
                    ->add('motivo', EntityType::class,array('class'=>OpcionesPregunta::class,'query_builder' => function(EntityRepository $er){
                        return $er->createQueryBuilder('u')->innerJoin('u.pregunta', 'p')->andWhere('p.id = 76')->orderBy('u.nombre_opcion');},
                    'choice_label' => 'nombre_opcion', 'required' => false, 'placeholder' => '--Vacio--'))
                    ->add('historia');
                break;

            case 3:
                $builder
                ->add('antecedentesFam', EntityType::class,array('class'=>OpcionesPregunta::class,'query_builder' => function(EntityRepository $er){
                    return $er->createQueryBuilder('u')->innerJoin('u.pregunta', 'p')->andWhere('p.id = 75')->andWhere('u.pregunta_compleja=0');},
                'choice_label' => 'nombre_opcion', 'expanded' => true,'multiple' => true, 'label' => 'Antecedentes Familiares'))
                ->add('OtrosAntecFam', null, ['label' => 'Otros. Explique'])
                ;
                break;

            case 4:
                $builder
                    ->add('antecedentes', EntityType::class, array('class'=>OpcionesPregunta::class,'query_builder' => function(EntityRepository $er){
                        return $er->createQueryBuilder('u')->innerJoin('u.pregunta', 'p')->andWhere('p.id = 1')->andWhere('u.pregunta_compleja=0');},
                    'choice_label' => 'nombre_opcion', 'expanded' => true, 'multiple' => true, 'label' => 'Antecedentes Odontológicos' ))
                    ->add('OtrosAntec', null, ['label' => 'Otros. Explique'])
                     ;
                break;

            case 5:
                $builder
                    ->add('Enfermedades_Durante_el_Embarazo', EntityType::class,array('class'=>OpcionesPregunta::class,'query_builder' => function(EntityRepository $er){
                        return $er->createQueryBuilder('u')->innerJoin('u.pregunta', 'p')->andWhere('p.id = 2')->andWhere('u.pregunta_compleja = 0');},
                    'choice_label' => 'nombre_opcion', 'expanded' => true,'multiple' => true,'label' => 'Enfermedades Padecidas Durante el Embarazo'))
                    ->add('otras_Enfermedades_Durante_el_Embarazo', null, ['label' => 'Otros. Explique'])
                    ->add('medicamentos_durante_embarazo', EntityType::class,array('class'=>OpcionesPregunta::class,'query_builder' => function(EntityRepository $er){
                        return $er->createQueryBuilder('u')->innerJoin('u.pregunta', 'p')->andWhere('p.id = 3')->andWhere('u.pregunta_compleja = 0');},
                    'choice_label' => 'nombre_opcion', 'expanded' => true,'multiple' => true, 'label' => 'Medicamentos Tomados Durante el Embarazo'))
                    ->add('otros_medicamentos_durante_embarazo', null, ['label' => 'Otros. Explique'])
                    // ->add('PartoNumero', EntityType::class,array('class'=>OpcionesPregunta::class,'query_builder' => function(EntityRepository $er){
                    //     return $er->createQueryBuilder('u')->innerJoin('u.pregunta', 'p')->andWhere('p.id = 4')->andWhere('u.pregunta_compleja = 1');},
                    // 'choice_label' => 'nombre_opcion', 'required' => false, 'placeholder' => '--Vacio--'), ['label' => false])
                    ->add('PartoNumero', null, ['label' => 'Parto Numero'])
                    ->add('Valores', EntityType::class,array('class'=>OpcionesPregunta::class,  'query_builder' => function(EntityRepository $er){
                        return $er->createQueryBuilder('u')->innerJoin('u.pregunta', 'p')->andWhere('p.id = 4')->andWhere('u.pregunta_compleja = 0');},
                    'choice_label' => 'nombre_opcion', 'expanded' => false, 'multiple' => true, 'label' => false,  'required'=>false))
                    // ->add('PesoAlNacer', EntityType::class,array('class'=>OpcionesPregunta::class,'query_builder' => function(EntityRepository $er){
                    //     return $er->createQueryBuilder('u')->innerJoin('u.pregunta', 'p')->andWhere('p.id = 5');},
                    // 'choice_label' => 'nombre_opcion', 'required' => false, 'placeholder' => '--Vacio--'), ['label' => false])
                    // ->add('TallaAlNacer', EntityType::class,array('class'=>OpcionesPregunta::class,'query_builder' => function(EntityRepository $er){
                    //     return $er->createQueryBuilder('u')->innerJoin('u.pregunta', 'p')->andWhere('p.id = 6');},
                    // 'choice_label' => 'nombre_opcion', 'required' => false, 'placeholder' => '--Vacio--'), ['label' => false])
                    ->add('PesoAlNacer', NumberType::class , ['label' => 'Peso al nacer', 'required'=>false])
                    ->add('PesoAlNacer_units', ChoiceType::class, [
                          'choices'  => [
                              'Kilogramos' => 'Kilogramos',
                              'Libras' => 'Libras'
                          ], 'label'=>false])
                    ->add('TallaAlNacer', NumberType::class, ['label' => 'Talla al Nacer (cm)', 'required'=>false])
                    ->add('DificultadesNeonatales', EntityType::class,array('class'=>OpcionesPregunta::class,'query_builder' => function(EntityRepository $er){
                        return $er->createQueryBuilder('u')->innerJoin('u.pregunta', 'p')->andWhere('p.id = 7')->andWhere('u.pregunta_compleja = 0');},
                    'choice_label' => 'nombre_opcion', 'expanded' => true, 'multiple' => true, 'label'=>'Dificultades Neonatales'),['label' => false])
                    ->add('ExpliqueDifNeoNat', null, ['label' => 'Explique'])
                    ->add('AlimentacionMaterna', EntityType::class,array('class'=>OpcionesPregunta::class,'query_builder' => function(EntityRepository $er){
                        return $er->createQueryBuilder('u')->innerJoin('u.pregunta', 'p')->andWhere('p.id = 8')->andWhere('u.pregunta_compleja = 1');},
                        'choice_label' => 'nombre_opcion', 'expanded' => true, 'multiple' => true, 'label'=> 'Alimentación Materna'),['label' => false])
                    // ->add('Duracion', EntityType::class,array('class'=>OpcionesPregunta::class,  'query_builder' => function(EntityRepository $er){
                    //     return $er->createQueryBuilder('u')->innerJoin('u.pregunta', 'p')->andWhere('p.id = 8')->andWhere('u.pregunta_compleja = 0');},
                    // 'choice_label' => 'nombre_opcion', 'required' => false, 'placeholder' => '--Vacio--'), ['label' => false])
                    ->add('Duracion', NumberType::class, ['label' => 'Duración', 'required'=>false])
                    ->add('Duracion_units', ChoiceType::class, [
                          'choices'  => [
                              'Semanas' => 'Semanas',
                              'Meses' => 'Meses'
                          ], 'label'=>false])
                    ->add('LecheCompleta', EntityType::class,array('class'=>OpcionesPregunta::class,'query_builder' => function(EntityRepository $er){
                        return $er->createQueryBuilder('u')->innerJoin('u.pregunta', 'p')->andWhere('p.id = 9')->andWhere('u.pregunta_compleja = 1');},
                        'choice_label' => 'nombre_opcion', 'expanded' => true, 'multiple' => true),['label' => false])
                    // ->add('EdadLecheCompleta', EntityType::class,array('class'=>OpcionesPregunta::class,  'query_builder' => function(EntityRepository $er){
                    //     return $er->createQueryBuilder('u')->innerJoin('u.pregunta', 'p')->andWhere('p.id = 9')->andWhere('u.pregunta_compleja = 0');},
                    // 'choice_label' => 'nombre_opcion', 'required' => false, 'placeholder' => '--Vacio--'), ['label' => false])
                    ->add('EdadLecheCompleta', NumberType::class, ['label' => 'Edad', 'required'=>false])
                    ->add('EdadLecheCompleta_units', ChoiceType::class, [
                          'choices'  => [
                              'Semanas' => 'Semanas',
                              'Meses' => 'Meses',
                              'Años' => 'Años'
                          ], 'label'=>false])
                    ->add('Formula', EntityType::class,array('class'=>OpcionesPregunta::class,'query_builder' => function(EntityRepository $er){
                        return $er->createQueryBuilder('u')->innerJoin('u.pregunta', 'p')->andWhere('p.id = 10')->andWhere('u.pregunta_compleja = 1');},
                        'choice_label' => 'nombre_opcion', 'expanded' => true, 'multiple' => true),['label' => false])
                    // ->add('EdadFormula', EntityType::class,array('class'=>OpcionesPregunta::class,  'query_builder' => function(EntityRepository $er){
                    //     return $er->createQueryBuilder('u')->innerJoin('u.pregunta', 'p')->andWhere('p.id = 10')->andWhere('u.pregunta_compleja = 0');},
                    // 'choice_label' => 'nombre_opcion', 'required' => false, 'placeholder' => '--Vacio--'), ['label' => false])
                    ->add('EdadFormula', NumberType::class, ['label' => 'Edad', 'required'=>false])
                    ->add('EdadFormula_units', ChoiceType::class, [
                          'choices'  => [
                              'Semanas' => 'Semanas',
                              'Meses' => 'Meses',
                              'Años' => 'Años'
                          ], 'label'=>false])
                    ->add('AlimentosSolidos', EntityType::class,array('class'=>OpcionesPregunta::class,'query_builder' => function(EntityRepository $er){
                        return $er->createQueryBuilder('u')->innerJoin('u.pregunta', 'p')->andWhere('p.id = 11')->andWhere('u.pregunta_compleja = 1');},
                        'choice_label' => 'nombre_opcion', 'expanded' => true, 'multiple' => true, 'label'=>'Alimentos Sólidos'),['label' => false])
                    // ->add('EdadAlimentosSolidos', EntityType::class,array('class'=>OpcionesPregunta::class,  'query_builder' => function(EntityRepository $er){
                    //     return $er->createQueryBuilder('u')->innerJoin('u.pregunta', 'p')->andWhere('p.id = 11')->andWhere('u.pregunta_compleja = 0');},
                    // 'choice_label' => 'nombre_opcion', 'required' => false, 'placeholder' => '--Vacio--'), ['label' => false])
                    ->add('EdadAlimentosSolidos', NumberType::class, ['label' => '¿A que edad?', 'required'=>false])
                    ->add('EdadAlimentosSolidos_units', ChoiceType::class, [
                          'choices'  => [
                              'Semanas' => 'Semanas',
                              'Meses' => 'Meses',
                              'Años' => 'Años'
                          ], 'label'=>false])
                 //    ->add('EdadEnQueGateo', EntityType::class,array('class'=>OpcionesPregunta::class,'query_builder' => function(EntityRepository $er){
                 //       return $er->createQueryBuilder('u')->innerJoin('u.pregunta', 'p')->andWhere('p.id = 12')->andWhere('u.pregunta_compleja = 0');},
                 //   'choice_label' => 'nombre_opcion', 'required' => false, 'placeholder' => '--Vacio--'), ['label' => false])
                 //
                 //   ->add('EdadEnQueCamino', EntityType::class,array('class'=>OpcionesPregunta::class,'query_builder' => function(EntityRepository $er){
                 //      return $er->createQueryBuilder('u')->innerJoin('u.pregunta', 'p')->andWhere('p.id = 12')->andWhere('u.pregunta_compleja = 1');},
                 //  'choice_label' => 'nombre_opcion', 'required' => false, 'placeholder' => '--Vacio--'), ['label' => false])
                 //
                 //  ->add('EdadEnQueHablo', EntityType::class,array('class'=>OpcionesPregunta::class,'query_builder' => function(EntityRepository $er){
                 //     return $er->createQueryBuilder('u')->innerJoin('u.pregunta', 'p')->andWhere('p.id = 12')->andWhere('u.pregunta_compleja = 2');},
                 // 'choice_label' => 'nombre_opcion', 'required' => false, 'placeholder' => '--Vacio--'), ['label' => false])
                  ->add('EdadEnQueGateo', NumberType::class, ['label' => 'Edad en que Gateó', 'required'=>false])
                  ->add('EdadEnQueGateo_units', ChoiceType::class, [
                        'choices'  => [
                            'Semanas' => 'Semanas',
                            'Meses' => 'Meses',
                            'Años' => 'Años'
                        ], 'label'=>false])
                  ->add('EdadEnQueCamino', NumberType::class, ['label' => 'Edad en que Caminó', 'required'=>false])
                  ->add('EdadEnQueCamino_units', ChoiceType::class, [
                        'choices'  => [
                            'Semanas' => 'Semanas',
                            'Meses' => 'Meses',
                            'Años' => 'Años'
                        ], 'label'=>false])
                  ->add('EdadEnQueHablo', NumberType::class, ['label' => 'Edad en que Habló', 'required'=>false])
                  ->add('EdadEnQueHablo_units', ChoiceType::class, [
                        'choices'  => [
                            'Semanas' => 'Semanas',
                            'Meses' => 'Meses',
                            'Años' => 'Años'
                        ], 'label'=>false])
                    ;

                break;

            case 6:
                $builder
                ->add('SistemaNervioso', EntityType::class,array('class'=>OpcionesPregunta::class,'query_builder' => function(EntityRepository $er){
                    return $er->createQueryBuilder('u')->innerJoin('u.pregunta', 'p')->andWhere('p.id = 13');},
                    'choice_label' => 'nombre_opcion', 'expanded' => true, 'multiple' => true),['label' => false, 'attr' => array('style' => 'display:block;', 'cellspacing' =>'250px')])
                ->add('SistemaCardiovascular', EntityType::class,array('class'=>OpcionesPregunta::class,'query_builder' => function(EntityRepository $er){
                    return $er->createQueryBuilder('u')->innerJoin('u.pregunta', 'p')->andWhere('p.id = 14');},
                    'choice_label' => 'nombre_opcion', 'expanded' => true, 'multiple' => true),['label' => false, 'attr' => array('style' => 'display:block;', 'cellspacing' =>'250px')])
                ->add('SistemaHematopoyetico', EntityType::class,array('class'=>OpcionesPregunta::class,'query_builder' => function(EntityRepository $er){
                    return $er->createQueryBuilder('u')->innerJoin('u.pregunta', 'p')->andWhere('p.id = 15');},
                    'choice_label' => 'nombre_opcion', 'expanded' => true, 'multiple' => true, 'label'=>'Sistema Hematopoyético'),['label' => false, 'attr' => array('style' => 'display:block;', 'cellspacing' =>'250px')])
                ->add('SistemaRespiratorio', EntityType::class,array('class'=>OpcionesPregunta::class,'query_builder' => function(EntityRepository $er){
                    return $er->createQueryBuilder('u')->innerJoin('u.pregunta', 'p')->andWhere('p.id = 16');},
                    'choice_label' => 'nombre_opcion', 'expanded' => true, 'multiple' => true),['label' => false, 'attr' => array('style' => 'display:block;', 'cellspacing' =>'250px')])
                ->add('SistemaGastrointestinal', EntityType::class,array('class'=>OpcionesPregunta::class,'query_builder' => function(EntityRepository $er){
                    return $er->createQueryBuilder('u')->innerJoin('u.pregunta', 'p')->andWhere('p.id = 17');},
                    'choice_label' => 'nombre_opcion', 'expanded' => true, 'multiple' => true),['label' => false, 'attr' => array('style' => 'display:block;', 'cellspacing' =>'250px')])
                ->add('SistemaEndocrino', EntityType::class,array('class'=>OpcionesPregunta::class,'query_builder' => function(EntityRepository $er){
                    return $er->createQueryBuilder('u')->innerJoin('u.pregunta', 'p')->andWhere('p.id = 18');},
                    'choice_label' => 'nombre_opcion', 'expanded' => true, 'multiple' => true, 'label'=>'Sistema Endócrino'),['label' => false, 'attr' => array('style' => 'display:block;', 'cellspacing' =>'250px')])
                ->add('SistemaGenitourinario', EntityType::class,array('class'=>OpcionesPregunta::class,'query_builder' => function(EntityRepository $er){
                    return $er->createQueryBuilder('u')->innerJoin('u.pregunta', 'p')->andWhere('p.id = 19');},
                    'choice_label' => 'nombre_opcion', 'expanded' => true, 'multiple' => true),['label' => false, 'attr' => array('style' => 'display:block;', 'cellspacing' =>'250px')])
                ->add('PielYMucosa', EntityType::class,array('class'=>OpcionesPregunta::class,'query_builder' => function(EntityRepository $er){
                        return $er->createQueryBuilder('u')->innerJoin('u.pregunta', 'p')->andWhere('p.id = 20');},
                        'choice_label' => 'nombre_opcion', 'expanded' => true, 'multiple' => true),['label' => false, 'attr' => array('style' => 'display:block;', 'cellspacing' =>'250px')])
                ->add('TrastornosSensoriales', EntityType::class,array('class'=>OpcionesPregunta::class,'query_builder' => function(EntityRepository $er){
                    return $er->createQueryBuilder('u')->innerJoin('u.pregunta', 'p')->andWhere('p.id = 21');},
                    'choice_label' => 'nombre_opcion', 'expanded' => true, 'multiple' => true),['label' => false, 'attr' => array('style' => 'display:block;', 'cellspacing' =>'250px')])
                ->add('Alergias', EntityType::class,array('class'=>OpcionesPregunta::class,'query_builder' => function(EntityRepository $er){
                    return $er->createQueryBuilder('u')->innerJoin('u.pregunta', 'p')->andWhere('p.id = 22');},
                  'choice_label' => 'nombre_opcion', 'expanded' => true, 'multiple' => true),['label' => false, 'attr' => array('style' => 'display:block;', 'cellspacing' =>'250px')])

                  //TextAreas
                  ->add('TA60', TextareaType::class, ['label'=>false, 'required' => false])
                  ->add('TA61', TextareaType::class, ['label'=>false, 'required' => false])
                  ->add('TA62', TextareaType::class, ['label'=>false, 'required' => false])
                  ->add('TA63', TextareaType::class, ['label'=>false, 'required' => false])
                  ->add('TA64', TextareaType::class, ['label'=>false, 'required' => false])
                  ->add('TA65', TextareaType::class, ['label'=>false, 'required' => false])
                  ->add('TA66', TextareaType::class, ['label'=>false, 'required' => false])
                  ->add('TA67', TextareaType::class, ['label'=>false, 'required' => false])
                  ->add('TA68', TextareaType::class, ['label'=>false, 'required' => false])
                  ->add('TA69', TextareaType::class, ['label'=>false, 'required' => false])
                  ->add('TA70', TextareaType::class, ['label'=>false, 'required' => false])
                  ->add('TA71', TextareaType::class, ['label'=>false, 'required' => false])
                  ->add('TA72', TextareaType::class, ['label'=>false, 'required' => false])
                  ->add('TA73', TextareaType::class, ['label'=>false, 'required' => false])
                  ->add('TA74', TextareaType::class, ['label'=>false, 'required' => false])
                  ->add('TA75', TextareaType::class, ['label'=>false, 'required' => false])
                  ->add('TA76', TextareaType::class, ['label'=>false, 'required' => false])
                  ->add('TA77', TextareaType::class, ['label'=>false, 'required' => false])
                  ->add('TA78', TextareaType::class, ['label'=>false, 'required' => false])
                  ->add('TA79', TextareaType::class, ['label'=>false, 'required' => false])
                  ->add('TA80', TextareaType::class, ['label'=>false, 'required' => false])
                  ->add('TA81', TextareaType::class, ['label'=>false, 'required' => false])

                ;

              break;

            case 7:
                $builder
                ->add('enfermedadesInfecciosasPadecidas', EntityType::class,array('class'=>OpcionesPregunta::class,'query_builder' => function(EntityRepository $er){
                    return $er->createQueryBuilder('u')->innerJoin('u.pregunta', 'p')->andWhere('p.id = 23')->andWhere('u.pregunta_compleja=0');},
                  'choice_label' => 'nombre_opcion', 'expanded' => true, 'multiple' => true),['label' => false, 'attr' => array('style' => 'display:block;', 'cellspacing' =>'250px')])
                ->add('OtrasEnfermeInf', null, ['label' => 'Otros. Explique'])
                ;

                break;

            case 8:
                $builder
                ->add('HaAsistidoAConsultaEnLosUltimos6Meses', EntityType::class,array('class'=>OpcionesPregunta::class,'query_builder' => function(EntityRepository $er){
                  return $er->createQueryBuilder('u')->innerJoin('u.pregunta', 'p')->andWhere('p.id = 24')->andWhere('u.pregunta_compleja = 1');},
                  'choice_label' => 'nombre_opcion', 'expanded' => true, 'multiple' => true, 'label'=>'¿Ha asistido a consulta médica en los últimos 6 meses? '),['label' => false, 'attr' => array('style' => 'display:block;', 'cellspacing' =>'250px')])
                // ->add('PorCualMotivo', EntityType::class,array('class'=>OpcionesPregunta::class,'query_builder' => function(EntityRepository $er){
                //   return $er->createQueryBuilder('u')->innerJoin('u.pregunta', 'p')->andWhere('p.id = 24')->andWhere('u.pregunta_compleja = 0');},
                //   'choice_label' => 'nombre_opcion', 'required' => false, 'placeholder' => '--Vacio--'), ['label' => false])
                ->add('PorCualMotivo', null, ['label' => '¿Por Cuál motivo?'])
                ->add('SeEncuentraBajoTratamiento', EntityType::class,array('class'=>OpcionesPregunta::class,'query_builder' => function(EntityRepository $er){
                  return $er->createQueryBuilder('u')->innerJoin('u.pregunta', 'p')->andWhere('p.id = 25')->andWhere('u.pregunta_compleja = 1');},
                  'choice_label' => 'nombre_opcion', 'expanded' => true, 'multiple' => true, 'label'=>'¿Se encuentra bajo tratamiento médico?'),['label' => false, 'attr' => array('style' => 'display:block;', 'cellspacing' =>'250px')])
                // ->add('MotivoDeTratamiento', EntityType::class,array('class'=>OpcionesPregunta::class,'query_builder' => function(EntityRepository $er){
                //   return $er->createQueryBuilder('u')->innerJoin('u.pregunta', 'p')->andWhere('p.id = 25')->andWhere('u.pregunta_compleja = 0');},
                //   'choice_label' => 'nombre_opcion', 'required' => false, 'placeholder' => '--Vacio--'), ['label' => false])
                ->add('MotivoDeTratamiento', null, ['label' => '¿Por Cuál motivo?'])
                // ->add('MedicamentosQueTomaActualmente', EntityType::class,array('class'=>OpcionesPregunta::class,'query_builder' => function(EntityRepository $er){
                //     return $er->createQueryBuilder('u')->innerJoin('u.pregunta', 'p')->andWhere('p.id = 26');},
                //     'choice_label' => 'nombre_opcion', 'expanded' => true, 'multiple' => true),['label' => false, 'attr' => array('style' => 'display:block;', 'cellspacing' =>'250px')])
                ->add('MedicamentosQueTomaActualmente', null, ['label' => 'Medicamentos Que Toma Actualmente'])
                ->add('IntervencionesQuirurgicas', EntityType::class,array('class'=>OpcionesPregunta::class,'query_builder' => function(EntityRepository $er){
                    return $er->createQueryBuilder('u')->innerJoin('u.pregunta', 'p')->andWhere('p.id = 27')->andWhere('u.pregunta_compleja = 1');},
                    'choice_label' => 'nombre_opcion', 'expanded' => true, 'multiple' => true, 'label' => '¿Se le han practicado intervenciones quirúrgicas?'),['label' => false, 'attr' => array('style' => 'display:block;', 'cellspacing' =>'250px')])
                // ->add('MotivoDeIntervencion', EntityType::class,array('class'=>OpcionesPregunta::class,'query_builder' => function(EntityRepository $er){
                //     return $er->createQueryBuilder('u')->innerJoin('u.pregunta', 'p')->andWhere('p.id = 27')->andWhere('u.pregunta_compleja = 0');},
                //     'choice_label' => 'nombre_opcion', 'required' => false, 'placeholder' => '--Vacio--'), ['label' => false])
                ->add('MotivoDeIntervencion', null, ['label' => '¿Cuáles?'])
                ->add('HaSidoHospitalizado', EntityType::class,array('class'=>OpcionesPregunta::class,'query_builder' => function(EntityRepository $er){
                    return $er->createQueryBuilder('u')->innerJoin('u.pregunta', 'p')->andWhere('p.id = 28')->andWhere('u.pregunta_compleja = 1');},
                    'choice_label' => 'nombre_opcion', 'expanded' => true, 'multiple' => true, 'label'=> '¿Ha estado hospitalizado?'),['label' => false, 'attr' => array('style' => 'display:block;', 'cellspacing' =>'250px')])
                // ->add('MotivoHospitalizacion', EntityType::class,array('class'=>OpcionesPregunta::class,'query_builder' => function(EntityRepository $er){
                //     return $er->createQueryBuilder('u')->innerJoin('u.pregunta', 'p')->andWhere('p.id = 28')->andWhere('u.pregunta_compleja = 0');},
                //     'choice_label' => 'nombre_opcion', 'required' => false, 'placeholder' => '--Vacio--'), ['label' => false])
                ->add('MotivoHospitalizacion', null, ['label' => '¿Por cuál motivo?'])
                ->add('VacunasRecibidas', EntityType::class,array('class'=>OpcionesPregunta::class,'query_builder' => function(EntityRepository $er){
                    return $er->createQueryBuilder('u')->innerJoin('u.pregunta', 'p')->andWhere('p.id = 29')->andWhere('u.pregunta_compleja = 0');},
                    'choice_label' => 'nombre_opcion', 'expanded' => true, 'multiple' => true),['label' => false, 'attr' => array('style' => 'display:block;', 'cellspacing' =>'250px')])
                ->add('OtrasVacunas', null, ['label' => 'Otras.Explique'])
                ->add('TodosSusRefuerzosRecibidosHastaLaFecha', EntityType::class,array('class'=>OpcionesPregunta::class,'query_builder' => function(EntityRepository $er){
                    return $er->createQueryBuilder('u')->innerJoin('u.pregunta', 'p')->andWhere('p.id = 30');},
                    'choice_label' => 'nombre_opcion', 'expanded' => true, 'multiple' => true, 'label'=> '¿Ha recibido todos sus refuerzos hasta la fecha?'),['label' => false, 'attr' => array('style' => 'display:block;', 'cellspacing' =>'250px')])
                ->add('EsquemaDeVacunacion', EntityType::class,array('class'=>OpcionesPregunta::class,'query_builder' => function(EntityRepository $er){
                    return $er->createQueryBuilder('u')->innerJoin('u.pregunta', 'p')->andWhere('p.id = 31');},
                    'choice_label' => 'nombre_opcion', 'expanded' => true, 'multiple' => true, 'label'=> 'Esquema de Vacunación'),['label' => false, 'attr' => array('style' => 'display:block;', 'cellspacing' =>'250px')])
                ->add('EsquemaIncompleto', null, ['label' => 'Incompleto. Explique'])
                ;
                  // ->add('molestias', EntityType::class, array('class'=>FichaCatalogo::class,'query_builder' => $this->getQuery('t.codigo = \'molestias\''), 'choice_label' => 'nombre', 'expanded' => true, 'multiple' => true), ['label' => 'Molestias']);
                break;

            case 9:
                $builder
                ->add('EdadErupcionDeDientesPrimarios', EntityType::class,array('class'=>OpcionesPregunta::class,'query_builder' => function(EntityRepository $er){
                    return $er->createQueryBuilder('u')->innerJoin('u.pregunta', 'p')->andWhere('p.id = 32');},
                    'choice_label' => 'nombre_opcion', 'required' => false, 'placeholder' => '--Vacio--', 'label'=> 'Edad de Erupción de los Dientes primarios'), ['label' => false])
                // ->add('EdadErupcionDeDientesPrimarios', null, ['label'=>'Edad de Erupcion De Dientes Primarios'])
                // ->add('EdadErupcionDeDientesPermanentes', null, ['label'=>'Edad de Erupcion De Dientes Permanentes'])
                // ->add('EdadErupcionDeDientesPermanentes', EntityType::class,array('class'=>OpcionesPregunta::class,'query_builder' => function(EntityRepository $er){
                //     return $er->createQueryBuilder('u')->innerJoin('u.pregunta', 'p')->andWhere('p.id = 33');},
                //     'choice_label' => 'nombre_opcion', 'required' => false, 'placeholder' => '--Vacio--'), ['label' => false])
                ->add('TraumatismosDentarios', EntityType::class,array('class'=>OpcionesPregunta::class,'query_builder' => function(EntityRepository $er){
                   return $er->createQueryBuilder('u')->innerJoin('u.pregunta', 'p')->andWhere('p.id = 35')->andWhere('u.pregunta_compleja = 1');},
                    'choice_label' => 'nombre_opcion', 'expanded' => true, 'multiple' => true),['attr' => array('style' => 'display:block;', 'cellspacing' =>'250px')])
                // ->add('EdadTraumatismoDentario', EntityType::class,array('class'=>OpcionesPregunta::class,'query_builder' => function(EntityRepository $er){
                //    return $er->createQueryBuilder('u')->innerJoin('u.pregunta', 'p')->andWhere('p.id = 35')->andWhere('u.pregunta_compleja = 0');},
                //    'choice_label' => 'nombre_opcion', 'required' => false, 'placeholder' => '--Vacio--'), ['label' => false])
                ->add('TipoTraumatismo', EntityType::class,array('class'=>OpcionesPregunta::class,'query_builder' => function(EntityRepository $er){
                   return $er->createQueryBuilder('u')->innerJoin('u.pregunta', 'p')->andWhere('p.id = 79')->andWhere('u.pregunta_compleja = 0')->orderBy('u.nombre_opcion');},
                   'choice_label' => 'nombre_opcion', 'required' => false, 'placeholder' => '--Vacio--'), ['label' => false])
                ->add('EdadTraumatismoDentario', null, ['label' => 'Edad de Traumatismo dentario' ])
                ->add('EdadTraumatismoDentario_units', ChoiceType::class, [
                      'choices'  => [
                          'Semanas' => 'Semanas',
                          'Meses' => 'Meses',
                          'Años' => 'Años'
                      ], 'label'=>false])

                ->add('TratamientoRecibido', EntityType::class,array('class'=>OpcionesPregunta::class,'query_builder' => function(EntityRepository $er){
                   return $er->createQueryBuilder('u')->innerJoin('u.pregunta', 'p')->andWhere('p.id = 35')->andWhere('u.pregunta_compleja = 0')->orderBy('u.nombre_opcion');},
                   'choice_label' => 'nombre_opcion', 'required' => false, 'placeholder' => '--Vacio--'), ['label' => false])
                         // ->add('TratamientoRecibido', null)
                ->add('HaAsistidoAnteriormenteAlOdontologo', EntityType::class,array('class'=>OpcionesPregunta::class,'query_builder' => function(EntityRepository $er){
                   return $er->createQueryBuilder('u')->innerJoin('u.pregunta', 'p')->andWhere('p.id = 36')->andWhere('u.pregunta_compleja = 1');},
                   'choice_label' => 'nombre_opcion', 'expanded' => true, 'multiple' => true, 'label'=> '¿Ha asistido anteriormente al odontólogo?'),['label' => false, 'attr' => array('style' => 'display:block;', 'cellspacing' =>'250px')])
                ->add('VecesAlDiaQueSeCepilla', EntityType::class,array('class'=>OpcionesPregunta::class,'query_builder' => function(EntityRepository $er){
                   return $er->createQueryBuilder('u')->innerJoin('u.pregunta', 'p')->andWhere('p.id = 37')->andWhere('u.pregunta_compleja = 1');},
                   'choice_label' => 'nombre_opcion', 'required' => false, 'placeholder' => '--Vacio--', 'label'=> '¿Cuántas vecesal día se cepilla?'), ['label' => false])
                // ->add('VecesAlDiaQueSeCepilla', null)
                ->add('SeCepillaSolo', EntityType::class,array('class'=>OpcionesPregunta::class,'query_builder' => function(EntityRepository $er){
                   return $er->createQueryBuilder('u')->innerJoin('u.pregunta', 'p')->andWhere('p.id = 37')->andWhere('u.pregunta_compleja = 2');},
                   'choice_label' => 'nombre_opcion', 'expanded' => true, 'multiple' => true, 'label'=> '¿Se cepilla solo?'),['label' => false, 'attr' => array('style' => 'display:block;', 'cellspacing' =>'250px')])
                ->add('QuienLoCepilla', EntityType::class,array('class'=>OpcionesPregunta::class,'query_builder' => function(EntityRepository $er){
                   return $er->createQueryBuilder('u')->innerJoin('u.pregunta', 'p')->andWhere('p.id = 37')->andWhere('u.pregunta_compleja = 3');},
                   'choice_label' => 'nombre_opcion', 'required' => false, 'placeholder' => '--Vacio--', 'label'=> '¿Quién lo cepilla?'), ['label' => false])
                // ->add('QuienLoCepilla', null, ['label'=>'Quien lo Cepilla?'])
                ->add('CremaDentalQueUtiliza', EntityType::class,array('class'=>OpcionesPregunta::class,'query_builder' => function(EntityRepository $er){
                   return $er->createQueryBuilder('u')->innerJoin('u.pregunta', 'p')->andWhere('p.id = 38');},
                   'choice_label' => 'nombre_opcion', 'required' => false, 'placeholder' => '--Vacio--'), ['label' => false])
                ->add('UtilizaSedaDentalOEnjuague', EntityType::class,array('class'=>OpcionesPregunta::class,'query_builder' => function(EntityRepository $er){
                   return $er->createQueryBuilder('u')->innerJoin('u.pregunta', 'p')->andWhere('p.id = 39');},
                   'choice_label' => 'nombre_opcion', 'expanded' => true, 'multiple' => true, 'label'=> '¿Utiliza seda dental o enjuague bucal?'),['label' => false, 'attr' => array('style' => 'display:block;', 'cellspacing' =>'250px')])
                   ;
                break;

            case 10:
                $builder
                ->add('AlimentosIngeridosAlMenosUnaVezPorSemana', EntityType::class,array('class'=>OpcionesPregunta::class,'query_builder' => function(EntityRepository $er){
                   return $er->createQueryBuilder('u')->innerJoin('u.pregunta', 'p')->andWhere('p.id = 40');},
                   'choice_label' => 'nombre_opcion', 'expanded' => true, 'multiple' => true),['label' => false, 'attr' => array('style' => 'display:block;', 'cellspacing' =>'250px')])
                ->add('CuantasMeriendasHaceEntreComidas', EntityType::class,array('class'=>OpcionesPregunta::class,'query_builder' => function(EntityRepository $er){
                   return $er->createQueryBuilder('u')->innerJoin('u.pregunta', 'p')->andWhere('p.id = 41')->andWhere('u.pregunta_compleja = 0');},
                   'choice_label' => 'nombre_opcion', 'required' => false, 'placeholder' => '--Vacio--', 'label' => '¿Cuántas meriendas hace entre comidas?'), ['label' => false])
                // ->add('QueCome', EntityType::class,array('class'=>OpcionesPregunta::class,'query_builder' => function(EntityRepository $er){
                //    return $er->createQueryBuilder('u')->innerJoin('u.pregunta', 'p')->andWhere('p.id = 41 ')->andWhere('u.pregunta_compleja = 1');},
                //    'choice_label' => 'nombre_opcion', 'expanded' => true, 'multiple' => true),['label' => false, 'attr' => array('style' => 'display:block;', 'cellspacing' =>'250px')])
                // ->add('CuantasMeriendasHaceEntreComidas', null, ['label'=>'Cuantas Meriendas Hace Entre Comidas?'])
                ->add('QueCome', null, ['label'=>'¿Que come?'])
                ->add('PrefiereAlimentosBlandos', EntityType::class,array('class'=>OpcionesPregunta::class,'query_builder' => function(EntityRepository $er){
                   return $er->createQueryBuilder('u')->innerJoin('u.pregunta', 'p')->andWhere('p.id = 42 ')->andWhere('u.pregunta_compleja = 0');},
                   'choice_label' => 'nombre_opcion', 'expanded' => true, 'multiple' => true, 'label' => '¿Prefiere Alimentos blandos?'),['label' => false, 'attr' => array('style' => 'display:block;', 'cellspacing' =>'250px')])
                ->add('CualesAlimentosBlandos', null, ['label'=>'¿Cuáles prefiere?'])
                // ->add('AlimentosSolidosQuePrefiere', EntityType::class,array('class'=>OpcionesPregunta::class,'query_builder' => function(EntityRepository $er){
                //    return $er->createQueryBuilder('u')->innerJoin('u.pregunta', 'p')->andWhere('p.id = 43 ')->andWhere('u.pregunta_compleja = 0');},
                //    'choice_label' => 'nombre_opcion', 'expanded' => true, 'multiple' => true),['label' => false, 'attr' => array('style' => 'display:block;', 'cellspacing' =>'250px')])
                ->add('AlimentosSolidosQuePrefiere', null, ['label'=>'¿Alimentos sólidos que prefiere?'])
                ->add('Biberon', EntityType::class,array('class'=>OpcionesPregunta::class,'query_builder' => function(EntityRepository $er){
                   return $er->createQueryBuilder('u')->innerJoin('u.pregunta', 'p')->andWhere('p.id = 44 ')->andWhere('u.pregunta_compleja = 1');},
                   'choice_label' => 'nombre_opcion', 'expanded' => true, 'multiple' => true, 'label' => '¿Toma biberón?'),['label' => false, 'attr' => array('style' => 'display:block;', 'cellspacing' =>'250px')])
                ->add('SeDuermeConelB', EntityType::class,array('class'=>OpcionesPregunta::class,'query_builder' => function(EntityRepository $er){
                   return $er->createQueryBuilder('u')->innerJoin('u.pregunta', 'p')->andWhere('p.id = 44 ')->andWhere('u.pregunta_compleja = 1');},
                   'choice_label' => 'nombre_opcion', 'expanded' => true, 'multiple' => true, 'label' => '¿Se duerme con el biberón?'),['label' => false, 'attr' => array('style' => 'display:block;', 'cellspacing' =>'250px')])

                ->add('ContenidoDelBiberon', EntityType::class,array('class'=>OpcionesPregunta::class,'query_builder' => function(EntityRepository $er){
                   return $er->createQueryBuilder('u')->innerJoin('u.pregunta', 'p')->andWhere('p.id = 44 ')->andWhere('u.pregunta_compleja = 0');},
                   'choice_label' => 'nombre_opcion', 'expanded' => true, 'multiple' => true, 'label' => '¿Qué contiene el biberón?'),['label' => false, 'attr' => array('style' => 'display:block;', 'cellspacing' =>'250px')])
                ->add('TomaLeche', EntityType::class,array('class'=>OpcionesPregunta::class,'query_builder' => function(EntityRepository $er){
                   return $er->createQueryBuilder('u')->innerJoin('u.pregunta', 'p')->andWhere('p.id = 45 ')->andWhere('u.pregunta_compleja = 0');},
                   'choice_label' => 'nombre_opcion', 'expanded' => true, 'multiple' => true, 'label'=>'¿Toma leche?'),['label' => false, 'attr' => array('style' => 'display:block;', 'cellspacing' =>'250px')])
                ->add('CuantosVasos', EntityType::class,array('class'=>OpcionesPregunta::class,'query_builder' => function(EntityRepository $er){
                   return $er->createQueryBuilder('u')->innerJoin('u.pregunta', 'p')->andWhere('p.id = 45')->andWhere('u.pregunta_compleja = 1');},
                   'choice_label' => 'nombre_opcion', 'required' => false, 'placeholder' => '--Vacio--', 'label' => '¿Cuántos Vasos?'), ['label' => false])
                // ->add('CuantosVasos', null, ['label'=>'Cuantos vasos?'])
                ->add('ConQueTomaSuLeche', EntityType::class,array('class'=>OpcionesPregunta::class,'query_builder' => function(EntityRepository $er){
                   return $er->createQueryBuilder('u')->innerJoin('u.pregunta', 'p')->andWhere('p.id = 45')->andWhere('u.pregunta_compleja = 2');},
                   'choice_label' => 'nombre_opcion', 'expanded' => true, 'multiple' => true, 'label' => '¿Con qué toma su leche?'),['label' => false, 'attr' => array('style' => 'display:block;', 'cellspacing' =>'250px')])
                ->add('ConsumeMuchaSal', EntityType::class,array('class'=>OpcionesPregunta::class,'query_builder' => function(EntityRepository $er){
                   return $er->createQueryBuilder('u')->innerJoin('u.pregunta', 'p')->andWhere('p.id = 46')->andWhere('u.pregunta_compleja = 0');},
                   'choice_label' => 'nombre_opcion', 'expanded' => true, 'multiple' => true, 'label' => '¿Consume mucha sal?'),['label' => false, 'attr' => array('style' => 'display:block;', 'cellspacing' =>'250px')])
                ->add('ConsumeMuchosCitricos', EntityType::class, array('class'=>OpcionesPregunta::class,  'query_builder' => function(EntityRepository $er){
                   return $er->createQueryBuilder('u')->innerJoin('u.pregunta', 'p')->andWhere('p.id = 47')->andWhere('u.pregunta_compleja = 0');},
                   'choice_label' => 'nombre_opcion', 'expanded' => true, 'multiple' => true, 'label' => 'Consume muchos cítricos?'),['label' => false, 'attr' => array('style' => 'display:block;', 'cellspacing' =>'250px')])

                // ->add('DesayunoHabitual', EntityType::class, array('class'=>OpcionesPregunta::class,  'query_builder' => function(EntityRepository $er){
                //    return $er->createQueryBuilder('u')->innerJoin('u.pregunta', 'p')->andWhere('p.id = 48')->andWhere('u.pregunta_compleja = 0');},
                //   'choice_label' => 'nombre_opcion', 'expanded' => true, 'multiple' => true),['label' => false, 'attr' => array('style' => 'display:block;', 'cellspacing' =>'250px')])
                // ->add('MeriendaHabitual', EntityType::class, array('class'=>OpcionesPregunta::class,  'query_builder' => function(EntityRepository $er){
                //    return $er->createQueryBuilder('u')->innerJoin('u.pregunta', 'p')->andWhere('p.id = 48')->andWhere('u.pregunta_compleja = 1');},
                //    'choice_label' => 'nombre_opcion', 'expanded' => true, 'multiple' => true),['label' => false, 'attr' => array('style' => 'display:block;', 'cellspacing' =>'250px')])
                // ->add('AlmuerzoHabitual', EntityType::class, array('class'=>OpcionesPregunta::class,  'query_builder' => function(EntityRepository $er){
                //    return $er->createQueryBuilder('u')->innerJoin('u.pregunta', 'p')->andWhere('p.id = 48')->andWhere('u.pregunta_compleja = 2');},
                //    'choice_label' => 'nombre_opcion', 'expanded' => true, 'multiple' => true),['label' => false, 'attr' => array('style' => 'display:block;', 'cellspacing' =>'250px')])
                // ->add('MeriendaHabitualTarde', EntityType::class, array('class'=>OpcionesPregunta::class,  'query_builder' => function(EntityRepository $er){
                //     return $er->createQueryBuilder('u')->innerJoin('u.pregunta', 'p')->andWhere('p.id = 48')->andWhere('u.pregunta_compleja = 3');},
                //     'choice_label' => 'nombre_opcion', 'expanded' => true, 'multiple' => true),['label' => false, 'attr' => array('style' => 'display:block;', 'cellspacing' =>'250px')])
                // ->add('CenaHabitual', EntityType::class, array('class'=>OpcionesPregunta::class,  'query_builder' => function(EntityRepository $er){
                //     return $er->createQueryBuilder('u')->innerJoin('u.pregunta', 'p')->andWhere('p.id = 48')->andWhere('u.pregunta_compleja = 4');},
                //     'choice_label' => 'nombre_opcion', 'expanded' => true, 'multiple' => true),['label' => false, 'attr' => array('style' => 'display:block;', 'cellspacing' =>'250px')])
                ->add('DesayunoHabitual', null, ['label'=>'Desayuno Habitual'])
                ->add('MeriendaHabitual', null, ['label'=>'Merienda Habitual'])
                ->add('AlmuerzoHabitual', null, ['label'=>'Almuerzo Habitual'])
                ->add('MeriendaHabitualTarde', null, ['label'=>'Merienda Habitual'])
                ->add('CenaHabitual', null, ['label'=>'Cena Habitual'])
                    // ->add('leche', null, ['label' => 'Leche (semanalmente)'])
                //     ->add('huevo', null, ['label' => 'Huevo (semanalmente)'])
                //     ->add('carne', null, ['label' => 'Carne (semanalmente)'])
                //     ->add('frutas', null, ['label' => 'Frutas (semanalmente)'])
                //     ->add('verduras', null, ['label' => 'Verduras (semanalmente)'])
                //     ->add('legumbres', null, ['label' => 'Legumbres (semanalmente)'])
                //     ->add('cereales', null, ['label' => 'Cereales (semanalmente)']);
                ;
                break;

            case 11:
                $builder
                // ->add('TallaActual', EntityType::class,array('class'=>OpcionesPregunta::class,'query_builder' => function(EntityRepository $er){
                //    return $er->createQueryBuilder('u')->innerJoin('u.pregunta', 'p')->andWhere('p.id = 49')->andWhere('u.pregunta_compleja = 0');},
                //    'choice_label' => 'nombre_opcion', 'required' => false, 'placeholder' => '--Vacio--'), ['label' => false])
                // ->add('PesoActual', EntityType::class,array('class'=>OpcionesPregunta::class,'query_builder' => function(EntityRepository $er){
                //    return $er->createQueryBuilder('u')->innerJoin('u.pregunta', 'p')->andWhere('p.id = 50')->andWhere('u.pregunta_compleja = 0');},
                //    'choice_label' => 'nombre_opcion', 'required' => false, 'placeholder' => '--Vacio--'), ['label' => false])
                ->add('TallaActual', null, ['label'=>'Talla Actual'])
                ->add('PesoActual', null, ['label'=>'Peso Actual'])

                ->add('TallaActual_units', ChoiceType::class, [
                      'choices'  => [
                          'centímetros' => 'centímetros',
                          'metros' => 'metros'
                      ], 'label'=>false])
                ->add('PesoActual_units', ChoiceType::class, [
                      'choices'  => [
                          'libras' => 'libras',
                          'kilogramos' => 'kilogramos'
                      ], 'label'=>false])

                ->add('Perfil', EntityType::class,array('class'=>OpcionesPregunta::class,'query_builder' => function(EntityRepository $er){
                   return $er->createQueryBuilder('u')->innerJoin('u.pregunta', 'p')->andWhere('p.id = 51')->andWhere('u.pregunta_compleja = 0');},
                   'choice_label' => 'nombre_opcion', 'required' => false, 'placeholder' => '--Vacio--'), ['label' => false])
                ->add('Configuracion', EntityType::class,array('class'=>OpcionesPregunta::class,'query_builder' => function(EntityRepository $er){
                   return $er->createQueryBuilder('u')->innerJoin('u.pregunta', 'p')->andWhere('p.id = 52')->andWhere('u.pregunta_compleja = 0');},
                   'choice_label' => 'nombre_opcion', 'required' => false, 'placeholder' => '--Vacio--', 'label' => 'Configuración'), ['label' => false])
                ->add('Labios', EntityType::class,array('class'=>OpcionesPregunta::class,'query_builder' => function(EntityRepository $er){
                   return $er->createQueryBuilder('u')->innerJoin('u.pregunta', 'p')->andWhere('p.id = 53')->andWhere('u.pregunta_compleja = 0');},
                   'choice_label' => 'nombre_opcion', 'required' => false, 'placeholder' => '--Vacio--'), ['label' => false])
                ->add('TamanioDeNariz', EntityType::class,array('class'=>OpcionesPregunta::class,'query_builder' => function(EntityRepository $er){
                   return $er->createQueryBuilder('u')->innerJoin('u.pregunta', 'p')->andWhere('p.id = 54')->andWhere('u.pregunta_compleja = 0');},
                   'choice_label' => 'nombre_opcion', 'required' => false, 'placeholder' => '--Vacio--', 'label' => 'Tamaño de Nariz'), ['label' => false])
                ->add('InsercionDeOrejas', EntityType::class,array('class'=>OpcionesPregunta::class,'query_builder' => function(EntityRepository $er){
                   return $er->createQueryBuilder('u')->innerJoin('u.pregunta', 'p')->andWhere('p.id = 55')->andWhere('u.pregunta_compleja = 0');},
                   'choice_label' => 'nombre_opcion', 'required' => false, 'placeholder' => '--Vacio--', 'label' => 'Inserción de Orejas'), ['label' => false])

                ->add('Observaciones', EntityType::class, array('class'=>OpcionesPregunta::class,  'query_builder' => function(EntityRepository $er){
                   return $er->createQueryBuilder('u')->innerJoin('u.pregunta', 'p')->andWhere('p.id = 56')->andWhere('u.pregunta_compleja = 0');},
                   'choice_label' => 'nombre_opcion', 'expanded' => true, 'multiple' => true),['label' => false, 'attr' => array('style' => 'display:block;', 'cellspacing' =>'250px')])

                ->add('Descripcion', null, ['label' => 'Describa'])
                ;

                //     ->add('bano', null, ['label' => 'Baños (semanalmente)'])
                //     ->add('ropa', null, ['label' => 'Cambios de ropa (semanalmente)'])
                //     ->add('cepillo', null, ['label' => 'Cepillados (diario)'])
                //     ->add('hilo', null, ['label' => 'Hilo (diario)'])
                //     ->add('enjuague', null, ['label' => 'Enjuagues (diario)']);
                break;

            case 12:
                $builder
                ->add('Encias', EntityType::class, array('class'=>OpcionesPregunta::class,  'query_builder' => function(EntityRepository $er){
                   return $er->createQueryBuilder('u')->innerJoin('u.pregunta', 'p')->andWhere('p.id = 57')->andWhere('u.pregunta_compleja = 0');},
                   'choice_label' => 'nombre_opcion', 'expanded' => true, 'multiple' => true, 'label' => 'Encías'),['label' => false, 'attr' => array('style' => 'display:block;', 'cellspacing' =>'250px')])
                ->add('MucosaBucal', EntityType::class, array('class'=>OpcionesPregunta::class,  'query_builder' => function(EntityRepository $er){
                   return $er->createQueryBuilder('u')->innerJoin('u.pregunta', 'p')->andWhere('p.id = 57')->andWhere('u.pregunta_compleja = 1');},
                   'choice_label' => 'nombre_opcion', 'expanded' => true, 'multiple' => true),['label' => false, 'attr' => array('style' => 'display:block;', 'cellspacing' =>'250px')])
                ->add('Lengua', EntityType::class, array('class'=>OpcionesPregunta::class,  'query_builder' => function(EntityRepository $er){
                   return $er->createQueryBuilder('u')->innerJoin('u.pregunta', 'p')->andWhere('p.id = 57')->andWhere('u.pregunta_compleja = 2');},
                   'choice_label' => 'nombre_opcion', 'expanded' => true, 'multiple' => true),['label' => false, 'attr' => array('style' => 'display:block;', 'cellspacing' =>'250px')])
                ->add('FrenilloLabialSuperior', EntityType::class, array('class'=>OpcionesPregunta::class,  'query_builder' => function(EntityRepository $er){
                   return $er->createQueryBuilder('u')->innerJoin('u.pregunta', 'p')->andWhere('p.id = 57')->andWhere('u.pregunta_compleja = 3');},
                   'choice_label' => 'nombre_opcion', 'expanded' => true, 'multiple' => true),['label' => false, 'attr' => array('style' => 'display:block;', 'cellspacing' =>'250px')])
                ->add('FrenilloLabialInferior', EntityType::class, array('class'=>OpcionesPregunta::class,  'query_builder' => function(EntityRepository $er){
                   return $er->createQueryBuilder('u')->innerJoin('u.pregunta', 'p')->andWhere('p.id = 57')->andWhere('u.pregunta_compleja = 4');},
                   'choice_label' => 'nombre_opcion', 'expanded' => true, 'multiple' => true),['label' => false, 'attr' => array('style' => 'display:block;', 'cellspacing' =>'250px')])
                ->add('FrenilloInferior', EntityType::class, array('class'=>OpcionesPregunta::class,  'query_builder' => function(EntityRepository $er){
                   return $er->createQueryBuilder('u')->innerJoin('u.pregunta', 'p')->andWhere('p.id = 57')->andWhere('u.pregunta_compleja = 5');},
                   'choice_label' => 'nombre_opcion', 'expanded' => true, 'multiple' => true),['label' => false, 'attr' => array('style' => 'display:block;', 'cellspacing' =>'250px')])
                ->add('PisoBucal', EntityType::class, array('class'=>OpcionesPregunta::class,  'query_builder' => function(EntityRepository $er){
                   return $er->createQueryBuilder('u')->innerJoin('u.pregunta', 'p')->andWhere('p.id = 57')->andWhere('u.pregunta_compleja = 6');},
                   'choice_label' => 'nombre_opcion', 'expanded' => true, 'multiple' => true),['label' => false, 'attr' => array('style' => 'display:block;', 'cellspacing' =>'250px')])

                ->add('DescEncia', null, ['label' => 'Encía'])
                ->add('DescMucosaBucal', null, ['label' => 'Mucosa Bucal'])
                ->add('DescPisoBucal', null, ['label' => 'Piso Bucal'])
                ->add('DescPaladarDuro', null, ['label' => 'Pladar Duro'])
                ->add('DescPaladarBlando', null, ['label' => 'Paladar Blando'])
                ->add('DescAmigdalas', null, ['label' => 'Amigdalas'])
                // ->add('dolor_boca', EntityType::class, array('class'=>FichaCatalogo::class,'query_builder' => $this->getQuery('t.codigo = \'lesiones_tejidos\''), 'choice_label' => 'nombre', 'required' => false, 'placeholder' => '--Vacio--'), ['label' => 'Materiales vivienda'])
                // ->add('tipo_lesion', EntityType::class, array('class'=>FichaCatalogo::class,'query_builder' => $this->getQuery('t.codigo = \'lesiones_tejido_tipo\''), 'choice_label' => 'nombre', 'required' => false, 'placeholder' => '--Vacio--'), ['label' => 'Materiales vivienda'])
                // ->add('consistencia', EntityType::class, array('class'=>FichaCatalogo::class,'query_builder' => $this->getQuery('t.codigo = \'lesiones_tejido_consistencia\''), 'choice_label' => 'nombre', 'required' => false, 'placeholder' => '--Vacio--'), ['label' => 'Materiales vivienda'])
                // ->add('localizacion_lesion', null, ['label' => 'Localizacion de lesion'])
                // ->add('color_lesion', null, ['label' => 'Color de lesion'])
                // ->add('tamano_lesion', null, ['label' => 'Tamaño de lesion']);
                ;
                break;

            case 13:
                $builder
                  ->add('DesviacionATM', EntityType::class, array('class'=>OpcionesPregunta::class,  'query_builder' => function(EntityRepository $er){
                   return $er->createQueryBuilder('u')->innerJoin('u.pregunta', 'p')->andWhere('p.id = 58')->andWhere('u.pregunta_compleja = 0');},
                   'choice_label' => 'nombre_opcion', 'expanded' => true, 'multiple' => true, 'label'=> 'Desviación ATM'),[ 'attr' => array('style' => 'display:block;', 'cellspacing' =>'250px')])
                ->add('Apertura', EntityType::class, array('class'=>OpcionesPregunta::class,  'query_builder' => function(EntityRepository $er){
                   return $er->createQueryBuilder('u')->innerJoin('u.pregunta', 'p')->andWhere('p.id = 58')->andWhere('u.pregunta_compleja = 1');},
                   'choice_label' => 'nombre_opcion', 'expanded' => true, 'multiple' => true),['label' => false, 'attr' => array('style' => 'display:block;', 'cellspacing' =>'250px')])
                ->add('Cierre', EntityType::class, array('class'=>OpcionesPregunta::class,  'query_builder' => function(EntityRepository $er){
                   return $er->createQueryBuilder('u')->innerJoin('u.pregunta', 'p')->andWhere('p.id = 58')->andWhere('u.pregunta_compleja = 2');},
                   'choice_label' => 'nombre_opcion', 'expanded' => true, 'multiple' => true),['label' => false, 'attr' => array('style' => 'display:block;', 'cellspacing' =>'250px')])
                ->add('Ruidos', EntityType::class, array('class'=>OpcionesPregunta::class,  'query_builder' => function(EntityRepository $er){
                   return $er->createQueryBuilder('u')->innerJoin('u.pregunta', 'p')->andWhere('p.id = 77')->andWhere('u.pregunta_compleja = 0');},
                   'choice_label' => 'nombre_opcion', 'expanded' => true, 'multiple' => true),['label' => false, 'attr' => array('style' => 'display:block;', 'cellspacing' =>'250px')])
                ->add('Saltos', EntityType::class, array('class'=>OpcionesPregunta::class,  'query_builder' => function(EntityRepository $er){
                   return $er->createQueryBuilder('u')->innerJoin('u.pregunta', 'p')->andWhere('p.id = 78')->andWhere('u.pregunta_compleja = 0');},
                   'choice_label' => 'nombre_opcion', 'expanded' => true, 'multiple' => true),['label' => false, 'attr' => array('style' => 'display:block;', 'cellspacing' =>'250px')])
              ;
                // ->add('pulso', null, ['label' => 'Pulso'])
                // ->add('frecuencia_respiratoria', null, ['label' => 'Frecuencia respiratoria'])
                // ->add('frecuencia_cardiaca', null, ['label' => 'Frecuencia cardiaca'])
                // ->add('presion_arterial', null, ['label' => 'Presion arterial'])
                // ->add('peso', null, ['label' => 'Peso'])
                // ->add('estatura', null, ['label' => 'Estatura']);
                break;

            case 14:
                $builder
                 ->add('HabitosParafuncionales', EntityType::class, array('class'=>OpcionesPregunta::class,  'query_builder' => function(EntityRepository $er){
                   return $er->createQueryBuilder('u')->innerJoin('u.pregunta', 'p')->andWhere('p.id = 59')->andWhere('u.pregunta_compleja = 0');},
                   'choice_label' => 'nombre_opcion', 'expanded' => true, 'multiple' => true, 'label' => 'Hábitos parafuncionales'),['label' => false, 'attr' => array('style' => 'display:block;', 'cellspacing' =>'250px')])
                ->add('ExpliqueHabitosParafuncionales', null, ['label' => 'Otros. Explique'])
                ->add('DuracionHabitosParaf', EntityType::class,array('class'=>OpcionesPregunta::class,'query_builder' => function(EntityRepository $er){
                   return $er->createQueryBuilder('u')->innerJoin('u.pregunta', 'p')->andWhere('p.id = 59')->andWhere('u.pregunta_compleja = 3');},
                   'choice_label' => 'nombre_opcion', 'required' => false, 'placeholder' => '--Vacio--', 'label' => 'Duración'))
                ->add('IntensidadHabitosParaf', EntityType::class,array('class'=>OpcionesPregunta::class,'query_builder' => function(EntityRepository $er){
                   return $er->createQueryBuilder('u')->innerJoin('u.pregunta', 'p')->andWhere('p.id = 59')->andWhere('u.pregunta_compleja = 2');},
                   'choice_label' => 'nombre_opcion', 'required' => false, 'placeholder' => '--Vacio--', 'label' => 'Intensidad'), ['label' => false])
                ->add('FrecuenciaHabitosParaf', EntityType::class,array('class'=>OpcionesPregunta::class,'query_builder' => function(EntityRepository $er){
                   return $er->createQueryBuilder('u')->innerJoin('u.pregunta', 'p')->andWhere('p.id = 59')->andWhere('u.pregunta_compleja = 1');},
                   'choice_label' => 'nombre_opcion', 'required' => false, 'placeholder' => '--Vacio--', 'label' => 'Frecuencia'), ['label' => false])

                  ;

                break;
            case 15:
                $builder
                ->add('RelacionMolarDerecha', EntityType::class, array('class'=>OpcionesPregunta::class,  'query_builder' => function(EntityRepository $er){
                  return $er->createQueryBuilder('u')->innerJoin('u.pregunta', 'p')->andWhere('p.id = 60')->andWhere('u.pregunta_compleja = 0');},
                  'choice_label' => 'nombre_opcion', 'expanded' => true, 'multiple' => true, 'label' => 'Relación Molar Derecha'),['label' => false, 'attr' => array('style' => 'display:block;', 'cellspacing' =>'250px')])
                ->add('RelacionMolarIzquierda', EntityType::class, array('class'=>OpcionesPregunta::class,  'query_builder' => function(EntityRepository $er){
                  return $er->createQueryBuilder('u')->innerJoin('u.pregunta', 'p')->andWhere('p.id = 61')->andWhere('u.pregunta_compleja = 0');},
                  'choice_label' => 'nombre_opcion', 'expanded' => true, 'multiple' => true, 'label' => 'Relación Molar Izquierda'),['label' => false, 'attr' => array('style' => 'display:block;', 'cellspacing' =>'250px')])
                ->add('RelacionCaninaDerecha', EntityType::class, array('class'=>OpcionesPregunta::class,  'query_builder' => function(EntityRepository $er){
                   return $er->createQueryBuilder('u')->innerJoin('u.pregunta', 'p')->andWhere('p.id = 62')->andWhere('u.pregunta_compleja = 0');},
                   'choice_label' => 'nombre_opcion', 'expanded' => true, 'multiple' => true, 'label' => 'Relación Canina Derecha'),['label' => false, 'attr' => array('style' => 'display:block;', 'cellspacing' =>'250px')])
                ->add('RelacionCaninaIzquierda', EntityType::class, array('class'=>OpcionesPregunta::class,  'query_builder' => function(EntityRepository $er){
                   return $er->createQueryBuilder('u')->innerJoin('u.pregunta', 'p')->andWhere('p.id = 63')->andWhere('u.pregunta_compleja = 0');},
                   'choice_label' => 'nombre_opcion', 'expanded' => true, 'multiple' => true, 'label' => 'Relación Canina Izquierda'),['label' => false, 'attr' => array('style' => 'display:block;', 'cellspacing' =>'250px')])
                ->add('MordidaAbierta', EntityType::class, array('class'=>OpcionesPregunta::class,  'query_builder' => function(EntityRepository $er){
                   return $er->createQueryBuilder('u')->innerJoin('u.pregunta', 'p')->andWhere('p.id = 64')->andWhere('u.pregunta_compleja = 0');},
                   'choice_label' => 'nombre_opcion', 'expanded' => true, 'multiple' => true),['label' => false, 'attr' => array('style' => 'display:block;', 'cellspacing' =>'250px')])
                ->add('MordidaCruzada', EntityType::class, array('class'=>OpcionesPregunta::class,  'query_builder' => function(EntityRepository $er){
                   return $er->createQueryBuilder('u')->innerJoin('u.pregunta', 'p')->andWhere('p.id = 65')->andWhere('u.pregunta_compleja = 0');},
                   'choice_label' => 'nombre_opcion', 'expanded' => true, 'multiple' => true),['label' => false, 'attr' => array('style' => 'display:block;', 'cellspacing' =>'250px')])
                // ->add('MordidaCruzadaOpts', EntityType::class, array('class'=>OpcionesPregunta::class,  'query_builder' => function(EntityRepository $er){
                //    return $er->createQueryBuilder('u')->innerJoin('u.pregunta', 'p')->andWhere('p.id = 65')->andWhere('u.pregunta_compleja = 1');},
                //    'choice_label' => 'nombre_opcion', 'required' => false, 'placeholder' => '--Vacio--', 'label' => 'Mordida Cruzada Unilateral'))
                // ->add('MordidaCruzadaOpts', null, ['label' => 'Mordida Cruzada Unilateral'])

                // ->add('Overjet', EntityType::class,array('class'=>OpcionesPregunta::class,'query_builder' => function(EntityRepository $er){
                //    return $er->createQueryBuilder('u')->innerJoin('u.pregunta', 'p')->andWhere('p.id = 66')->andWhere('u.pregunta_compleja = 0');},
                //    'choice_label' => 'nombre_opcion', 'required' => false, 'placeholder' => '--Vacio--'), ['label' => false])
                ->add('Overjet', NumberType::class, ['label'=>'Overjet(mm)', 'required'=>false])
                ->add('OverjetOpts', EntityType::class, array('class'=>OpcionesPregunta::class,  'query_builder' => function(EntityRepository $er){
                   return $er->createQueryBuilder('u')->innerJoin('u.pregunta', 'p')->andWhere('p.id = 66')->andWhere('u.pregunta_compleja = 1');},
                   'choice_label' => 'nombre_opcion', 'expanded' => true, 'multiple' => true, 'label' => 'Overjet Opciones'),[ 'attr' => array('style' => 'display:block;', 'cellspacing' =>'250px')])
                // ->add('Overbite', EntityType::class,array('class'=>OpcionesPregunta::class,'query_builder' => function(EntityRepository $er){
                //    return $er->createQueryBuilder('u')->innerJoin('u.pregunta', 'p')->andWhere('p.id = 67')->andWhere('u.pregunta_compleja = 0');},
                //    'choice_label' => 'nombre_opcion', 'required' => false, 'placeholder' => '--Vacio--'), ['label' => false])
                ->add('Overbite', NumberType::class, ['label'=>'Overbite(mm)', 'required'=>false])
                ->add('OverbiteOpts', EntityType::class, array('class'=>OpcionesPregunta::class,  'query_builder' => function(EntityRepository $er){
                   return $er->createQueryBuilder('u')->innerJoin('u.pregunta', 'p')->andWhere('p.id = 67')->andWhere('u.pregunta_compleja = 1');},
                   'choice_label' => 'nombre_opcion', 'expanded' => true, 'multiple' => true, 'label' => 'Overbite Opciones'),[ 'attr' => array('style' => 'display:block;', 'cellspacing' =>'250px')])

                ->add('LineaMediaSuperior', EntityType::class, array('class'=>OpcionesPregunta::class,  'query_builder' => function(EntityRepository $er){
                   return $er->createQueryBuilder('u')->innerJoin('u.pregunta', 'p')->andWhere('p.id = 68')->andWhere('u.pregunta_compleja = 0');},
                   'choice_label' => 'nombre_opcion', 'expanded' => true, 'multiple' => true),['label' => false, 'attr' => array('style' => 'display:block;', 'cellspacing' =>'250px')])
                ->add('LineaMediaSuperiorLado', EntityType::class, array('class'=>OpcionesPregunta::class,  'query_builder' => function(EntityRepository $er){
                   return $er->createQueryBuilder('u')->innerJoin('u.pregunta', 'p')->andWhere('p.id = 68')->andWhere('u.pregunta_compleja = 2');},
                   'choice_label' => 'nombre_opcion', 'expanded' => false, 'multiple' => true, 'label' => 'Desviacion', 'required'=>false),[ 'attr' => array('style' => 'display:block;', 'cellspacing' =>'250px')])

                // ->add('LineaMediaSuperiorOpts', EntityType::class,array('class'=>OpcionesPregunta::class,'query_builder' => function(EntityRepository $er){
                //    return $er->createQueryBuilder('u')->innerJoin('u.pregunta', 'p')->andWhere('p.id = 68')->andWhere('u.pregunta_compleja = 1');},
                //    'choice_label' => 'nombre_opcion', 'required' => false, 'placeholder' => '--Vacio--'), ['label' => false])
                ->add('LineaMediaSuperiorOpts',  NumberType::class, ['label'=>'Desviacion(mm)', 'required'=>false])
                ->add('LineaMediaInferior', EntityType::class, array('class'=>OpcionesPregunta::class,  'query_builder' => function(EntityRepository $er){
                   return $er->createQueryBuilder('u')->innerJoin('u.pregunta', 'p')->andWhere('p.id = 69')->andWhere('u.pregunta_compleja = 0');},
                   'choice_label' => 'nombre_opcion', 'expanded' => true, 'multiple' => true),['label' => false, 'attr' => array('style' => 'display:block;', 'cellspacing' =>'250px')])
                // ->add('LineaMediaInferiorOpts', EntityType::class,array('class'=>OpcionesPregunta::class,'query_builder' => function(EntityRepository $er){
                //    return $er->createQueryBuilder('u')->innerJoin('u.pregunta', 'p')->andWhere('p.id = 69')->andWhere('u.pregunta_compleja = 1');},
                //    'choice_label' => 'nombre_opcion', 'required' => false, 'placeholder' => '--Vacio--'), ['label' => false])
                ->add('LineaMediaInferiorLado', EntityType::class, array('class'=>OpcionesPregunta::class,  'query_builder' => function(EntityRepository $er){
                   return $er->createQueryBuilder('u')->innerJoin('u.pregunta', 'p')->andWhere('p.id = 69')->andWhere('u.pregunta_compleja = 2');},
                   'choice_label' => 'nombre_opcion', 'expanded' => false, 'multiple' => true, 'label' => 'Desviación' , 'required'=>false),['label' => false, 'attr' => array('style' => 'display:block;', 'cellspacing' =>'250px')])

                ->add('LineaMediaInferiorOpts', NumberType::class, ['label'=>'Desviación(mm)', 'required'=>false])
                ->add('Dientes', EntityType::class, array('class'=>OpcionesPregunta::class,  'query_builder' => function(EntityRepository $er){
                   return $er->createQueryBuilder('u')->innerJoin('u.pregunta', 'p')->andWhere('p.id = 70')->andWhere('u.pregunta_compleja = 0');},
                   'choice_label' => 'nombre_opcion', 'expanded' => true, 'multiple' => true),['label' => false, 'attr' => array('style' => 'display:block;', 'cellspacing' =>'250px')])
                // ->add('Diastemas', EntityType::class,array('class'=>OpcionesPregunta::class,'query_builder' => function(EntityRepository $er){
                //    return $er->createQueryBuilder('u')->innerJoin('u.pregunta', 'p')->andWhere('p.id = 70')->andWhere('u.pregunta_compleja = 1');},
                //    'choice_label' => 'nombre_opcion', 'required' => false, 'placeholder' => '--Vacio--'), ['label' => false])
                ->add('DiastemasCheck', EntityType::class, array('class'=>OpcionesPregunta::class,  'query_builder' => function(EntityRepository $er){
                   return $er->createQueryBuilder('u')->innerJoin('u.pregunta', 'p')->andWhere('p.id = 70')->andWhere('u.pregunta_compleja = 1');},
                   'choice_label' => 'nombre_opcion', 'expanded' => true, 'multiple' => true, 'label'=>'Diastemas'))
                ->add('Diastemas', null, ['label'=>'Explique'])

                ->add('EspacioDePrimateSup', EntityType::class, array('class'=>OpcionesPregunta::class,  'query_builder' => function(EntityRepository $er){
                   return $er->createQueryBuilder('u')->innerJoin('u.pregunta', 'p')->andWhere('p.id = 71')->andWhere('u.pregunta_compleja = 0');},
                   'choice_label' => 'nombre_opcion', 'expanded' => true, 'multiple' => true, 'label'=>'Espacio de Primate Superior'),['label' => false, 'attr' => array('style' => 'display:block;', 'cellspacing' =>'250px')])
                ->add('EspacioDePrimateInf', EntityType::class, array('class'=>OpcionesPregunta::class,  'query_builder' => function(EntityRepository $er){
                   return $er->createQueryBuilder('u')->innerJoin('u.pregunta', 'p')->andWhere('p.id = 72')->andWhere('u.pregunta_compleja = 0');},
                   'choice_label' => 'nombre_opcion', 'expanded' => true, 'multiple' => true, 'label'=>'Espacio de Primate Inferior'),['label' => false, 'attr' => array('style' => 'display:block;', 'cellspacing' =>'250px')])
                ->add('Apiniamiento', EntityType::class, array('class'=>OpcionesPregunta::class,  'query_builder' => function(EntityRepository $er){
                   return $er->createQueryBuilder('u')->innerJoin('u.pregunta', 'p')->andWhere('p.id = 73')->andWhere('u.pregunta_compleja = 0');},
                   'choice_label' => 'nombre_opcion', 'expanded' => true, 'multiple' => true, 'label'=>'Apiñamiento'),['label' => false, 'attr' => array('style' => 'display:block;', 'cellspacing' =>'250px')])

                ->add('PerdidasPrematuraCheck', EntityType::class, array('class'=>OpcionesPregunta::class,  'query_builder' => function(EntityRepository $er){
                   return $er->createQueryBuilder('u')->innerJoin('u.pregunta', 'p')->andWhere('p.id = 74')->andWhere('u.pregunta_compleja = 127');},
                   'choice_label' => 'nombre_opcion', 'expanded' => true, 'multiple' => true, 'label'=>'Perdidas Prematuras '),['label' => false, 'attr' => array('style' => 'display:block;', 'cellspacing' =>'250px')])


                ->add('PerdidasPrematura', null, ['label' => 'Perdidas Prematuras'])
                ->add('OtrasObservaciones', null, ['label' => 'Otros'])
                    ;

                break;
            case 16:
              $builder
              ->add('Hobby', EntityType::class, array('class'=>OpcionesPregunta::class,  'query_builder' => function(EntityRepository $er){
                 return $er->createQueryBuilder('u')->innerJoin('u.pregunta', 'p')->andWhere('p.id = 83')->andWhere('u.pregunta_compleja = 0');},
                 'choice_label' => 'nombre_opcion', 'expanded' => true, 'multiple' => true, 'label'=>'Hobbies. Juegos Preferidos'))
              ->add('Hobby_exp', null, ['label' => 'Explique'])

              ->add('Deportes', EntityType::class, array('class'=>OpcionesPregunta::class,  'query_builder' => function(EntityRepository $er){
                 return $er->createQueryBuilder('u')->innerJoin('u.pregunta', 'p')->andWhere('p.id = 84')->andWhere('u.pregunta_compleja = 0');},
                 'choice_label' => 'nombre_opcion', 'expanded' => true, 'multiple' => true, 'label'=>'Deportes'))
              ->add('Deportes_exp', null, ['label' => 'Explique'])

              ->add('Mascotas', EntityType::class, array('class'=>OpcionesPregunta::class,  'query_builder' => function(EntityRepository $er){
                 return $er->createQueryBuilder('u')->innerJoin('u.pregunta', 'p')->andWhere('p.id = 85')->andWhere('u.pregunta_compleja = 0');},
                 'choice_label' => 'nombre_opcion', 'expanded' => true, 'multiple' => true, 'label'=>'Mascotas'))
              ->add('Mascotas_exp', null, ['label' => 'Explique'])

              ->add('ProgramasTV', EntityType::class, array('class'=>OpcionesPregunta::class,  'query_builder' => function(EntityRepository $er){
                 return $er->createQueryBuilder('u')->innerJoin('u.pregunta', 'p')->andWhere('p.id = 86')->andWhere('u.pregunta_compleja = 0');},
                 'choice_label' => 'nombre_opcion', 'expanded' => true, 'multiple' => true, 'label'=>'Programas Preferidos de TV'))
              ->add('ProgramasTV_exp', null, ['label' => 'Explique'])

              ->add('Musica', EntityType::class, array('class'=>OpcionesPregunta::class,  'query_builder' => function(EntityRepository $er){
                 return $er->createQueryBuilder('u')->innerJoin('u.pregunta', 'p')->andWhere('p.id = 87')->andWhere('u.pregunta_compleja = 0');},
                 'choice_label' => 'nombre_opcion', 'expanded' => true, 'multiple' => true, 'label'=>'Musica Preferida'))
              ->add('Musica_exp', null, ['label' => 'Explique'])

              ->add('Fobias', EntityType::class, array('class'=>OpcionesPregunta::class,  'query_builder' => function(EntityRepository $er){
                 return $er->createQueryBuilder('u')->innerJoin('u.pregunta', 'p')->andWhere('p.id = 88')->andWhere('u.pregunta_compleja = 0');},
                 'choice_label' => 'nombre_opcion', 'expanded' => true, 'multiple' => true, 'label'=>'Fobias'))
              ->add('Fobias_exp', null, ['label' => 'Explique'])

              ->add('Recompensas', EntityType::class, array('class'=>OpcionesPregunta::class,  'query_builder' => function(EntityRepository $er){
                 return $er->createQueryBuilder('u')->innerJoin('u.pregunta', 'p')->andWhere('p.id = 89')->andWhere('u.pregunta_compleja = 0');},
                 'choice_label' => 'nombre_opcion', 'expanded' => true, 'multiple' => true, 'label'=>'Recompensas'))
              ->add('Recompensas_exp', null, ['label' => 'Explique'])

              ->add('Castigo', EntityType::class, array('class'=>OpcionesPregunta::class,  'query_builder' => function(EntityRepository $er){
                 return $er->createQueryBuilder('u')->innerJoin('u.pregunta', 'p')->andWhere('p.id = 90')->andWhere('u.pregunta_compleja = 0');},
                 'choice_label' => 'nombre_opcion', 'expanded' => true, 'multiple' => true, 'label'=>'Castigos'))
              ->add('Castigo_exp', null, ['label' => 'Explique'])

              ->add('OtrasActividades', EntityType::class, array('class'=>OpcionesPregunta::class,  'query_builder' => function(EntityRepository $er){
                 return $er->createQueryBuilder('u')->innerJoin('u.pregunta', 'p')->andWhere('p.id = 91')->andWhere('u.pregunta_compleja = 0');},
                 'choice_label' => 'nombre_opcion', 'expanded' => true, 'multiple' => true, 'label'=>'Otras Actividades'))
              ->add('OtrasActividades_exp', null, ['label' => 'Explique'])

              ->add('Aprendizaje', EntityType::class, array('class'=>OpcionesPregunta::class,  'query_builder' => function(EntityRepository $er){
                 return $er->createQueryBuilder('u')->innerJoin('u.pregunta', 'p')->andWhere('p.id = 92')->andWhere('u.pregunta_compleja = 0');},
                 'choice_label' => 'nombre_opcion', 'expanded' => true, 'multiple' => true, 'label'=>'Aprendizaje'))
              ->add('Aprendizaje_exp', null, ['label' => 'Otros. Explique'])
              ->add('Exp_enConsulta', TextareaType::class, ['label' => 'Experiencia y Comportamiento en la consulta Odontológica', 'required'=>false])
              ;

                ;

                break;
            case 17:
              $builder
                ->add('DiagnosticoMedico', TextareaType::class, ['label' => 'Diagnóstico Médico', 'required'=>false])
                ->add('DiagnosticoPeriodontal', TextareaType::class, ['label' => 'Diagnóstico Periodontal', 'required'=>false])
                ->add('TipoDeCaries', TextareaType::class, ['label' => 'Tipo de Caries', 'required'=>false])
                ->add('AlteracionesPulpares', TextareaType::class, ['label' => 'Alteraciones Pulpares', 'required'=>false])
                ->add('PatologiasTejBlandoDuro', TextareaType::class, ['label' => 'Patologías de Tejidos Duros o Blandos(Diagnóstico Provisional)', 'required'=>false])
                ->add('Maloclusiones', TextareaType::class, ['label' => 'Maloclusiones', 'required'=>false])
                ->add('RiesgoCariogenico', TextareaType::class, ['label' => 'Riesgo Cariogénico', 'required'=>false])
                ;
                break;

        }

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'App\Form\Clinico\datosclinicos_odontoped\CrearDatosPed',
        ]);
    }

    public function getName() {
		return $this->getBlockPrefix();
    }

    public function getBlockPrefix() {
		return 'datosPedType';
    }

    //Should be replaced with getQuery whenever you need more options on your builder
    private function getCatalog($filter){
        return array('class'=>FichaCatalogo::class,'query_builder' => function(EntityRepository $er) use ($filter){
            return $er->createQueryBuilder('e')->innerJoin('e.ficha_catalogo_tipo', 't')->andWhere($filter);
        }, 'choice_label' => 'nombre',);
    }

    private function getQuery($filter){
        return function(EntityRepository $er) use ($filter){
            return $er->createQueryBuilder('e')->innerJoin('e.ficha_catalogo_tipo', 't')->andWhere($filter);
        };
    }

}
