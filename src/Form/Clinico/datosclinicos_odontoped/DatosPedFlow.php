<?php

namespace App\Form\Clinico\datosclinicos_odontoped;

use Symfony\Component\HttpFoundation\Request;
use Craue\FormFlowBundle\Form\FormFlow;
use Craue\FormFlowBundle\Form\FormFlowInterface;

class DatosPedFlow extends FormFlow {

	protected $allowDynamicStepNavigation = true;

    protected function loadStepsConfig() {
		return array(
			array(
				'label' => 'Academicos',
				'form_type' => 'App\Form\Clinico\datosclinicos_odontoped\DatosPedType',
			),
			array(
				'label' => 'Personales',
				'form_type' => 'App\Form\Clinico\datosclinicos_odontoped\DatosPedType',
			),
			array(
				'label' => 'Antecedentes Familiares',
				'form_type' => 'App\Form\Clinico\datosclinicos_odontoped\DatosPedType',
			),
			array(
				'label' => 'Antecedentes Odontológicos',
				'form_type' => 'App\Form\Clinico\datosclinicos_odontoped\DatosPedType',
			),
			array(
				'label' => 'Historia Prenatal y Neonatal',
				'form_type' => 'App\Form\Clinico\datosclinicos_odontoped\DatosPedType',
			),
			array(
				'label' => 'Historia Médica',
				'form_type' => 'App\Form\Clinico\datosclinicos_odontoped\DatosPedType',
			),
			array(
				'label' => 'Enfermedades Infecciosas Padecidas',
				'form_type' => 'App\Form\Clinico\datosclinicos_odontoped\DatosPedType',
			),
			array(
				'label' => 'Tratamiento Médico',
				'form_type' => 'App\Form\Clinico\datosclinicos_odontoped\DatosPedType',
			),
			array(
				'label' => 'Historia Odontológica',
				'form_type' => 'App\Form\Clinico\datosclinicos_odontoped\DatosPedType',
			),
			array(
				'label' => 'Encuesta de Dieta',
				'form_type' => 'App\Form\Clinico\datosclinicos_odontoped\DatosPedType',
			),
			array(
				'label' => 'Examen Clínico Extra Oral',
				'form_type' => 'App\Form\Clinico\datosclinicos_odontoped\DatosPedType',
			),
			array(
				'label' => 'Examen Clínico Intra Oral',
				'form_type' => 'App\Form\Clinico\datosclinicos_odontoped\DatosPedType',
			),
			array(
				'label' => 'Examen Funcional',
				'form_type' => 'App\Form\Clinico\datosclinicos_odontoped\DatosPedType',
			),
			array(
				'label' => 'Habitos Parafuncionales',
				'form_type' => 'App\Form\Clinico\datosclinicos_odontoped\DatosPedType',
			),
			array(
				'label' => 'Examen Ortodontico',
				'form_type' => 'App\Form\Clinico\datosclinicos_odontoped\DatosPedType',
			),
			array(
				'label' => 'Historia Psico-Conductual',
				'form_type' => 'App\Form\Clinico\datosclinicos_odontoped\DatosPedType',
			),
			array(
				'label' => 'Diagnóstico Integral',
				'form_type' => 'App\Form\Clinico\datosclinicos_odontoped\DatosPedType',
			),
		);
	}

}
