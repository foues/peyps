<?php

namespace App\Form\Clinico\fichas;

use App\Entity\Academico\PeriodoCiclo;
use App\Entity\Clinico\FichaCatalogo;
use App\Entity\Academico\Curso;
use DateTime;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use App\Entity\Clinico\OpcionesPregunta;

class FichaType extends AbstractType{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        switch ($options['flow_step']) {
            case 1:
                $builder
                    ->add('curso', EntityType::class, array(
                        'class' => Curso::class, 'choice_label' => function ($curso) {
                            /** @var Curso $curso */
                            return $curso->getCodigo() . " - " . $curso->getNombre();
                        },
                        'label' => 'Curso del estudiante',
                    ))
                    ->add('ciclo', EntityType::class, array('class'=>PeriodoCiclo::class, 'choice_label' => 'periodo.nombre', 'label' => 'Ciclo cursado'));
                break;

            case 2:
                $builder
                    ->add('fecha', DateType::class, ['label' => 'Fecha de control', 'widget' => 'single_text', 'data'=> new DateTime()])
                    // ->add('motivo', TextareaType::class)
                    ->add('motivo', EntityType::class,array('class'=>OpcionesPregunta::class,'query_builder' => function(EntityRepository $er){
                        return $er->createQueryBuilder('u')->innerJoin('u.pregunta', 'p')->andWhere('p.id = 76');},
                    'choice_label' => 'nombre_opcion', 'required' => false, 'placeholder' => '--Vacio--'))
                    ->add('historia', TextareaType::class);
                break;

            case 3:
                $builder
                    ->add('embarazada', CheckboxType::class, ['label' => 'Embarazada', 'required' => false, "attr"=>['class'=>'switch']])
                    ->add('visita_odontologo', CheckboxType::class, ['label' => 'Visita al odontologo', 'required' => false, "attr"=>['class'=>'switch']])
                    ->add('reacciones_adversas', CheckboxType::class, ['label' => 'Reacciones adversas', 'required' => false, "attr"=>['class'=>'switch']])
                    ->add('tipo_reaccion', null, ['label' => false, 'attr' => array('style' => 'display:none;', 'placeholder' => 'Tipo de reaccion')])
                    ->add('trauma_dento', CheckboxType::class, ['label' => 'Trauma dentoalveolar', 'required' => false])
                    ->add('pieza_trauma', null, ['label' => false, 'attr' => array('style' => 'display:none;', 'placeholder' => 'Pieza de trauma')])
                    ->add('aditamientos_higiene', EntityType::class, array('class'=>FichaCatalogo::class,'query_builder' => $this->getQuery('t.codigo = \'aditamentos\''), 'choice_label' => 'nombre', 'expanded' => true, 'multiple' => true, 'label' => 'Aditamientos higiene bucal', 'attr'=>['name'=>'rq', 'class'=>'switch eval_sis']))
                    ->add('frecuencia_cepillado', EntityType::class, $this->getCatalog('t.codigo = \'frecuencia_cepillado\'', 'Frecuencia de cepillado'));
                break;

            case 4:
                $builder
                    ->add('eval_sistemica', EntityType::class, array('class'=>FichaCatalogo::class,'query_builder' => $this->getQuery('t.codigo = \'evaluaciones\''), 'choice_label' => 'nombre', 'expanded' => true, 'multiple' => true, 'label' => 'Evaluación Sistemica', "attr"=>['class'=>'switch eval_sis']))
                    ->add('habitos_bucales', EntityType::class, array('class'=>FichaCatalogo::class,'query_builder' => $this->getQuery('t.codigo = \'habitos_bucales\''), 'choice_label' => 'nombre', 'expanded' => true, 'multiple' => true, 'label' => 'Hábitos Bucales', "attr"=>['class'=>'switch eval_sis']));
                break;
        }

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'App\Form\Clinico\fichas\CrearFicha',
        ]);
    }

    public function getName() {
		return $this->getBlockPrefix();
    }

    public function getBlockPrefix() {
		return 'fichaType';
    }

    //Should be replaced with getQuery whenever you need more options on your builder
    private function getCatalog($filter, $label = ''){
        return array('class' => FichaCatalogo::class, 'query_builder' => function (EntityRepository $er) use ($filter) {
            return $er->createQueryBuilder('e')->innerJoin('e.ficha_catalogo_tipo', 't')->andWhere($filter);
        },
            'choice_label' => 'nombre',
            'label' => $label,
        );
    }

    private function getQuery($filter){
        return function(EntityRepository $er) use ($filter){
            return $er->createQueryBuilder('e')->innerJoin('e.ficha_catalogo_tipo', 't')->andWhere($filter);
        };
    }

}
