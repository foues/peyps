<?php

namespace App\Form\Clinico\fichas;

use Symfony\Component\Validator\Constraints as Assert;


class CrearFicha {
    
    public $ciclo;
    public $curso;

    public $fecha;

    /**
     * @Assert\NotBlank()
     * @Assert\Type("string")
     * @Assert\Length(min="3", max="999")
     * @var string
     */
    public $motivo;

     /**
     * @Assert\NotBlank()
     * @Assert\Type("string")
     * @Assert\Length(min="3", max="999")
     * @var string
     */
    public $historia;

    public $visita_odontologo;

    public $reacciones_adversas;

    /**
     * @Assert\Type("string")
     * @Assert\Length(max="255")
     * @var string
     */
    public $tipo_reaccion;

    public $trauma_dento;

    /**
     * @Assert\Type("integer")
     * @Assert\Length(min=2, max="2")
     * @var integer
     */
    public $pieza_trauma;

    public $aditamientos_higiene;

    public $frecuencia_cepillado;

    public $eval_sistemica;

    public $habitos_bucales;

    public $embarazada;
}