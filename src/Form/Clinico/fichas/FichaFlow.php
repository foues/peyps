<?php

namespace App\Form\Clinico\fichas;

use Symfony\Component\HttpFoundation\Request;
use Craue\FormFlowBundle\Form\FormFlow;
use Craue\FormFlowBundle\Form\FormFlowInterface;

class FichaFlow extends FormFlow {

	protected $allowDynamicStepNavigation = true;

    protected function loadStepsConfig() {
		return array(
			array(
				'label' => 'Academicos',
				'form_type' => 'App\Form\Clinico\fichas\FichaType',
			),
			array(
				'label' => 'Personales',
				'form_type' => 'App\Form\Clinico\fichas\FichaType',
			),
			array(
				'label' => 'Historia Odontologica',
				'form_type' => 'App\Form\Clinico\fichas\FichaType',
			),
			array(
				'label' => 'Habitos bucales',
				'form_type' => 'App\Form\Clinico\fichas\FichaType',
			),
		);
	}

}