<?php

namespace App\Form\Clinico\datosclinicos;

use App\Entity\Clinico\FichaCatalogo;
use App\Entity\Academico\Curso;
use App\Entity\Academico\PlanEstudioCiclo;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;

class DatosType extends AbstractType{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        switch ($options['flow_step']) {
            case 1:
                $builder
                    ->add('curso', EntityType::class, array('class'=>Curso::class, 'choice_label' => 'codigo',), ['label' => 'Curso del estudiante'])
                    ->add('ciclo', EntityType::class, array('class'=>PlanEstudioCiclo::class, 'choice_label' => 'romano',), ['label' => 'Ciclo cursado']);
                break;

            case 2:
                $builder
                    ->add('fecha', DateType::class, ['label' => 'Fecha de control', 'widget' => 'single_text'])
                    ->add('motivo')
                    ->add('historia');
                break;

            case 3:
                $builder
                    ->add('visita_odontologo', CheckboxType::class, ['label' => 'Visita al odontologo', 'required' => false])
                    ->add('reacciones_adversas', CheckboxType::class, ['label' => 'Reacciones adversas', 'required' => false])
                    ->add('tipo_reaccion', null, ['label' => false, 'attr' => array('style' => 'display:none;', 'placeholder' => 'Tipo de reaccion')])
                    ->add('trauma_dento', CheckboxType::class, ['label' => 'Trauma dentoalveolar', 'required' => false])
                    ->add('pieza_trauma', null, ['label' => false, 'attr' => array('style' => 'display:none;', 'placeholder' => 'Pieza de trauma')])
                    ->add('aditamientos_higiene', EntityType::class, array('class'=>FichaCatalogo::class,'query_builder' => $this->getQuery('t.codigo = \'aditamentos\''), 'choice_label' => 'nombre', 'expanded' => true, 'multiple' => true), ['label' => 'Aditamientos higiene bucal', 'attr'=>array('name'=>'rq')])
                    ->add('frecuencia_cepillado', EntityType::class, $this->getCatalog('t.codigo = \'frecuencia_cepillado\''), ['label' => 'Frecuencia de cepillado']);
                break;

            case 4:
                $builder
                    ->add('eval_sistemica', EntityType::class, array('class'=>FichaCatalogo::class,'query_builder' => $this->getQuery('t.codigo = \'evaluaciones\''), 'choice_label' => 'nombre', 'expanded' => true, 'multiple' => true), ['label' => 'Evaluacion sistemica'])
                    ->add('habitos_bucales', EntityType::class, array('class'=>FichaCatalogo::class,'query_builder' => $this->getQuery('t.codigo = \'habitos_bucales\''), 'choice_label' => 'nombre', 'expanded' => true, 'multiple' => true), ['label' => 'Habitos bucales']);
                break;
            
            case 5:
                $builder
                    ->add('padecimiento_bucal', EntityType::class, array('class'=>FichaCatalogo::class,'query_builder' => $this->getQuery('t.codigo = \'padecimiento_bucal\''), 'choice_label' => 'nombre', 'expanded' => true, 'multiple' => true), ['label' => 'Padecimiento bucal'])
                    ->add('padecimientos_generales', EntityType::class, array('class'=>FichaCatalogo::class,'query_builder' => $this->getQuery('t.codigo = \'enfermedades_ficha\''), 'choice_label' => 'nombre', 'expanded' => true, 'multiple' => true), ['label' => 'Padecimientos/Enfermedades'])
                    ->add('operaciones', EntityType::class, array('class'=>FichaCatalogo::class,'query_builder' => $this->getQuery('t.codigo = \'traumatismos\''), 'choice_label' => 'nombre', 'expanded' => true, 'multiple' => true), ['label' => 'Operaciones/Traumatismos'])
                    ->add('medicamentos', EntityType::class, array('class'=>FichaCatalogo::class,'query_builder' => $this->getQuery('t.codigo = \'medicamentos\''), 'choice_label' => 'nombre', 'expanded' => true, 'multiple' => true), ['label' => 'Consumo de medicamentos']);
                break;

            case 6:
                $builder
                    ->add('productos', EntityType::class, array('class'=>FichaCatalogo::class,'query_builder' => $this->getQuery('t.codigo = \'alergia\''), 'choice_label' => 'nombre', 'expanded' => true, 'multiple' => true), ['label' => 'Alergia a productos'])
                    ->add('sustancias', EntityType::class, array('class'=>FichaCatalogo::class,'query_builder' => $this->getQuery('t.codigo = \'sustancias\''), 'choice_label' => 'nombre', 'expanded' => true, 'multiple' => true), ['label' => 'Consumo de sustancias'])
                    ->add('transfusiones', EntityType::class, array('class'=>FichaCatalogo::class,'query_builder' => $this->getQuery('t.codigo = \'transfuciones\''), 'choice_label' => 'nombre', 'required' => false, 'placeholder' => '--Vacio--'), ['label' => 'Transfusiones'])
                    ->add('orientacion_sexual', EntityType::class, array('class'=>FichaCatalogo::class,'query_builder' => $this->getQuery('t.codigo = \'orientacion_sexual\''), 'choice_label' => 'nombre', 'required' => false, 'placeholder' => '--Vacio--'), ['label' => 'Orientacion sexual']);
                break;
            
            case 7:
                $builder
                    ->add('mujeres', EntityType::class, array('class'=>FichaCatalogo::class,'query_builder' => $this->getQuery('t.codigo = \'en_mujeres\''), 'choice_label' => 'nombre', 'expanded' => true, 'multiple' => true), ['label' => 'Alergia a productos'])
                    ->add('semanas_embarazo', null, ['label' => 'Semanas de embarazo'])
                    ->add('numero_embarazos', null, ['label' => 'Cantidad de embarazos'])
                    ->add('numero_abortos', null, ['label' => 'Cantidad de abortos'])
                    ->add('fur', null, ['label' => 'FUR']);
                break;

            case 8:
                $builder
                    ->add('enfermedades', EntityType::class, array('class'=>FichaCatalogo::class,'query_builder' => $this->getQuery('t.codigo = \'historial_enfermedades\''), 'choice_label' => 'nombre', 'expanded' => true, 'multiple' => true), ['label' => 'Enfermedades'])
                    ->add('familiares', EntityType::class, array('class'=>FichaCatalogo::class,'query_builder' => $this->getQuery('t.codigo = \'historial_enfermedades_familiares\''), 'choice_label' => 'nombre', 'expanded' => true, 'multiple' => true), ['label' => 'Enfermedades de familiares'])
                    ->add('molestias', EntityType::class, array('class'=>FichaCatalogo::class,'query_builder' => $this->getQuery('t.codigo = \'molestias\''), 'choice_label' => 'nombre', 'expanded' => true, 'multiple' => true), ['label' => 'Molestias']);
                break;

            case 9:
                $builder
                    ->add('material', EntityType::class, array('class'=>FichaCatalogo::class,'query_builder' => $this->getQuery('t.codigo = \'vivienda_material\''), 'choice_label' => 'nombre', 'required' => false, 'placeholder' => '--Vacio--', 'label' => 'Materiales vivienda'), ['label' => 'Materiales vivienda'])
                    ->add('cuartos', null, ['label' => 'Cuartos vivienda'])
                    ->add('habitantes', null, ['label' => 'Habitantes vivienda'])
                    ->add('personas', null, ['label' => 'Personas por cuarto'])
                    ->add('servicios', EntityType::class, array('class'=>FichaCatalogo::class,'query_builder' => $this->getQuery('t.codigo = \'vivienda_servicios\''), 'choice_label' => 'nombre', 'expanded' => true, 'multiple' => true), ['label' => 'Servicios']); 
                break;

            case 10:
                $builder
                    ->add('leche', null, ['label' => 'Leche (semanalmente)'])
                    ->add('huevo', null, ['label' => 'Huevo (semanalmente)'])
                    ->add('carne', null, ['label' => 'Carne (semanalmente)'])
                    ->add('frutas', null, ['label' => 'Frutas (semanalmente)'])
                    ->add('verduras', null, ['label' => 'Verduras (semanalmente)'])
                    ->add('legumbres', null, ['label' => 'Legumbres (semanalmente)'])
                    ->add('cereales', null, ['label' => 'Cereales (semanalmente)']);
                break;

            case 11:
                $builder
                    ->add('bano', null, ['label' => 'Baños (semanalmente)'])
                    ->add('ropa', null, ['label' => 'Cambios de ropa (semanalmente)'])
                    ->add('cepillo', null, ['label' => 'Cepillados (diario)'])
                    ->add('hilo', null, ['label' => 'Hilo (diario)'])
                    ->add('enjuague', null, ['label' => 'Enjuagues (diario)']);
                break;

            case 12:
                $builder
                ->add('dolor_boca', EntityType::class, array('class'=>FichaCatalogo::class,'query_builder' => $this->getQuery('t.codigo = \'lesiones_tejidos\''), 'choice_label' => 'nombre', 'required' => false, 'placeholder' => '--Vacio--'), ['label' => 'Materiales vivienda'])
                ->add('tipo_lesion', EntityType::class, array('class'=>FichaCatalogo::class,'query_builder' => $this->getQuery('t.codigo = \'lesiones_tejido_tipo\''), 'choice_label' => 'nombre', 'required' => false, 'placeholder' => '--Vacio--'), ['label' => 'Materiales vivienda'])
                ->add('consistencia', EntityType::class, array('class'=>FichaCatalogo::class,'query_builder' => $this->getQuery('t.codigo = \'lesiones_tejido_consistencia\''), 'choice_label' => 'nombre', 'required' => false, 'placeholder' => '--Vacio--'), ['label' => 'Materiales vivienda'])
                ->add('localizacion_lesion', null, ['label' => 'Localizacion de lesion'])
                ->add('color_lesion', null, ['label' => 'Color de lesion'])
                ->add('tamano_lesion', null, ['label' => 'Tamaño de lesion']);
                break;
            
            case 13:
                $builder
                ->add('pulso', null, ['label' => 'Pulso'])
                ->add('frecuencia_respiratoria', null, ['label' => 'Frecuencia respiratoria'])
                ->add('frecuencia_cardiaca', null, ['label' => 'Frecuencia cardiaca'])
                ->add('presion_arterial', null, ['label' => 'Presion arterial'])
                ->add('peso', null, ['label' => 'Peso'])
                ->add('estatura', null, ['label' => 'Estatura']);
                break;
                
        }

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'App\Form\Clinico\datosclinicos\CrearDatos',
        ]);
    }

    public function getName() {
		return $this->getBlockPrefix();
    }

    public function getBlockPrefix() {
		return 'datosType';
    }

    //Should be replaced with getQuery whenever you need more options on your builder
    private function getCatalog($filter){
        return array('class'=>FichaCatalogo::class,'query_builder' => function(EntityRepository $er) use ($filter){
            return $er->createQueryBuilder('e')->innerJoin('e.ficha_catalogo_tipo', 't')->andWhere($filter);
        }, 'choice_label' => 'nombre',);
    }

    private function getQuery($filter){
        return function(EntityRepository $er) use ($filter){
            return $er->createQueryBuilder('e')->innerJoin('e.ficha_catalogo_tipo', 't')->andWhere($filter);
        };
    }

}
