<?php

namespace App\Form\Clinico\datosclinicos;

use Symfony\Component\Validator\Constraints as Assert;


class CrearDatos {
    
    public $ciclo;
    public $curso;

    public $fecha;

    /**
     * @Assert\NotBlank()
     * @Assert\Type("string")
     * @Assert\Length(min="3", max="999")
     * @var string
     */
    public $motivo;

     /**
     * @Assert\NotBlank()
     * @Assert\Type("string")
     * @Assert\Length(min="3", max="999")
     * @var string
     */
    public $historia;

    public $visita_odontologo;

    public $reacciones_adversas;

    /**
     * @Assert\Type("string")
     * @Assert\Length(max="255")
     * @var string
     */
    public $tipo_reaccion;

    public $trauma_dento;

    /**
     * @Assert\Type("integer")
     * @Assert\Length(min=2, max="2")
     * @var integer
     */
    public $pieza_trauma;

    /* Agregar aditamientos higiene */
    public $aditamientos_higiene;

    public $frecuencia_cepillado;

    public $eval_sistemica;

    public $habitos_bucales;

    public $padecimiento_bucal;

    public $padecimientos_generales;

    public $medicamentos;

    public $operaciones;

    public $productos;

    public $sustancias;

    public $transfusiones;

    public $orientacion_sexual;

    /* Mujeres */
    public $isMujer;
    public $mujeres;
    /**
     * @Assert\Type("integer")
     * @Assert\Length(min=2, max="2")
     * @var integer
     */
    public $semanas_embarazo;
    /**
     * @Assert\Type("integer")
     * @Assert\Length(min=2, max="2")
     * @var integer
     */
    public $numero_embarazos;
    /**
     * @Assert\Type("integer")
     * @Assert\Length(min=2, max="2")
     * @var integer
     */
    public $numero_abortos;

    public $fur;

    public $enfermedades;

    public $familiares;

    public $molestias;

    /* Vivienda */

    public $material;
    /**
     * @Assert\Type("integer")
     * @Assert\Length(min=0, max="2")
     * @var integer
     */
    public $cuartos;
    /**
     * @Assert\Type("integer")
     * @Assert\Length(min=0, max="2")
     * @var integer
     */
    public $habitantes;
    /**
     * @Assert\Type("integer")
     * @Assert\Length(min=0, max="2")
     * @var integer
     */
    public $personas;
    public $servicios;

    /* Alimentacion */
    /**
     * @Assert\Type("integer")
     * @Assert\Length(min=0, max="3")
     * @var integer
     */
    public $leche;
    /**
     * @Assert\Type("integer")
     * @Assert\Length(min=0, max="3")
     * @var integer
     */
    public $huevo;
    /**
     * @Assert\Type("integer")
     * @Assert\Length(min=0, max="3")
     * @var integer
     */
    public $carne;
    /**
     * @Assert\Type("integer")
     * @Assert\Length(min=0, max="3")
     * @var integer
     */
    public $frutas;
    /**
     * @Assert\Type("integer")
     * @Assert\Length(min=0, max="3")
     * @var integer
     */
    public $verduras;
    /**
     * @Assert\Type("integer")
     * @Assert\Length(min=0, max="3")
     * @var integer
     */
    public $legumbres;
    /**
     * @Assert\Type("integer")
     * @Assert\Length(min=0, max="3")
     * @var integer
     */
    public $cereales;

    /* Higiene */

    /**
     * @Assert\Type("integer")
     * @Assert\Length(min=0, max="2")
     * @var integer
     */
    public $bano;
     /**
     * @Assert\Type("integer")
     * @Assert\Length(min=0, max="2")
     * @var integer
     */
    public $ropa;
     /**
     * @Assert\Type("integer")
     * @Assert\Length(min=0, max="2")
     * @var integer
     */
    public $cepillo;
     /**
     * @Assert\Type("integer")
     * @Assert\Length(min=0, max="2")
     * @var integer
     */
    public $hilo;
     /**
     * @Assert\Type("integer")
     * @Assert\Length(min=0, max="2")
     * @var integer
     */
    public $enjuague;

    /* Lesiones */
    public $dolor_boca;
    public $tipo_lesion;
    public $consistencia;
    public $localizacion_lesion;
    public $color_lesion;
    public $tamano_lesion;

    /* Exploracion */
     /**
     * @Assert\Type("integer")
     * @Assert\Length(min=0, max="4")
     * @var integer
     */
    public $pulso;
    /**
     * @Assert\Type("integer")
     * @Assert\Length(min=0, max="4")
     * @var integer
     */
    public $frecuencia_respiratoria;
    /**
     * @Assert\Type("integer")
     * @Assert\Length(min=0, max="4")
     * @var integer
     */
    public $frecuencia_cardiaca;
    /**
     * @Assert\Type("integer")
     * @Assert\Length(min=0, max="4")
     * @var integer
     */
    public $presion_arterial;
    /**
     * @Assert\Type("integer")
     * @Assert\Length(min=0, max="4")
     * @var integer
     */
    public $peso;
    /**
     * @Assert\Type("integer")
     * @Assert\Length(min=0, max="4")
     * @var integer
     */
    public $estatura;







}