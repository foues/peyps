<?php

namespace App\Form\Clinico\datosclinicos;

use Symfony\Component\HttpFoundation\Request;
use Craue\FormFlowBundle\Form\FormFlow;
use Craue\FormFlowBundle\Form\FormFlowInterface;

class DatosFlow extends FormFlow {

	protected $allowDynamicStepNavigation = true;

    protected function loadStepsConfig() {
		return array(
			array(
				'label' => 'Academicos',
				'form_type' => 'App\Form\Clinico\datosclinicos\DatosType',
			),
			array(
				'label' => 'Personales',
				'form_type' => 'App\Form\Clinico\datosclinicos\DatosType',
			),
			array(
				'label' => 'Historial',
				'form_type' => 'App\Form\Clinico\datosclinicos\DatosType',
			),
			array(
				'label' => 'Habitos',
				'form_type' => 'App\Form\Clinico\datosclinicos\DatosType',
			),
			array(
				'label' => 'Padecimientos',
				'form_type' => 'App\Form\Clinico\datosclinicos\DatosType',
			),
			array(
				'label' => 'Sustancias',
				'form_type' => 'App\Form\Clinico\datosclinicos\DatosType',
			),
			array(
				'label' => 'Mujeres',
				'form_type' => 'App\Form\Clinico\datosclinicos\DatosType',
				'skip' => function($estimatedCurrentStepNumber, FormFlowInterface $flow) {
					return $estimatedCurrentStepNumber > 1 && !$flow->getFormData()->isMujer;
				},
			),
			array(
				'label' => 'Enfermedades',
				'form_type' => 'App\Form\Clinico\datosclinicos\DatosType',
			),
			array(
				'label' => 'Vivienda',
				'form_type' => 'App\Form\Clinico\datosclinicos\DatosType',
			),
			array(
				'label' => 'Alimentacion',
				'form_type' => 'App\Form\Clinico\datosclinicos\DatosType',
			),
			array(
				'label' => 'Higiene',
				'form_type' => 'App\Form\Clinico\datosclinicos\DatosType',
			),
			array(
				'label' => 'Lesiones',
				'form_type' => 'App\Form\Clinico\datosclinicos\DatosType',
			),
			array(
				'label' => 'Exploracion',
				'form_type' => 'App\Form\Clinico\datosclinicos\DatosType',
			),
		);
	}

}