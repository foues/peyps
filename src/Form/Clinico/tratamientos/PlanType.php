<?php

namespace App\Form\Clinico\tratamientos;

use App\Entity\Clinico\Tratamiento;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use App\Entity\Clinico\EvaluacionDental;

class PlanType extends AbstractType{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        ->add('prioritarios', EntityType::class, array(
        'class'=>EvaluacionDental::class,
        'query_builder' => function(EntityRepository $er) use ($query, $filter){
            return $er->createQueryBuilder('e')->innerJoin('e.tratamiento', 't')->andWhere($query)->setParameter('ficha', $filter);
        }, 
        'choice_label' => function ($prioritarios) { return $prioritarios->getPieza()->getCodigo();}, 
        'expanded' => false, 
        'multiple' => true
        ), 
        ['label' => 'Tratamientos prioritarios',  'attr' => ['class' => 'select2custom']]);
        
        
        
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'App\Form\Clinico\tratamientos\CrearPlan',
        ]);
    }
    
    // Needs a better abstraction, probably just make a funcion to return the query builder with the filter as input, all other options must be chose freely
    private function getTratamiento($query, $filter, $expanded=false, $multiple=false){
        return array('class'=>EvaluacionDental::class,'query_builder' => function(EntityRepository $er) use ($query, $filter){
            return $er->createQueryBuilder('e')->innerJoin('e.tratamiento', 't')->andWhere($query)->setParameter('f', $filter);
        }, 'choice_label' => function ($prioritarios) { return $prioritarios->getPieza()->getCodigo();}, 'expanded' => $expanded, 'multiple' => $multiple);
    }



}