<?php

namespace App\Form\Clinico\expedientes;

use Symfony\Component\Validator\Constraints as Assert;
use DateTime;

class CrearExpediente
{
    /**
     * @Assert\NotBlank()
     * @Assert\Type("string")
     * @Assert\Length(min="7", max="7")
     * @Assert\Regex("/^[0-9]{4}-[0-9]{2}$/")
     * @var string
     */
    public $codigo;

    /**
     * @Assert\NotBlank()
     * @Assert\Type("string")
     * @Assert\Length(min="3", max="100")
     * @var string
     */
    public $nombres;

    /**
     * @Assert\NotBlank()
     * @Assert\Type("string")
     * @Assert\Length(min="5", max="100")
     * @var string
     */
    public $apellidos;

    public $tipo_paciente;

    /**
     * @Assert\Length(min="9", max="9")
     * @var string
     */
    public $dui;

    public $sexo;
    /**
     * @Assert\NotBlank()
     * @Assert\DateTime()
     */
    public $fecha_nacimiento;

    public $estado_civil;

    public $profesion;

    public $nivel_educativo;

    public $ocupacion;

    /**

     * @Assert\Type("integer")
     * @Assert\Length(min="8", max="20")
     * @var integer
     */
    public $telefono_casa;

    /**

     * @Assert\Length(min="8", max="20")
     * @Assert\Type("integer")
     * @var integer
     */
    public $telefono_movil;

    /**
     * @Assert\Email()
     * @Assert\Length(max="50")
     * @var string
     */
    public $email;

    /**

     * @Assert\Length(min="3", max="50")
     * @var string
     */
    public $lugar_trabajo;

    /**
     * @Assert\Length(min="8", max="20")
     */
    public $telefono_trabajo;

    /**
     * @Assert\NotBlank()
     * @Assert\Length(min="3", max="100")
     * @Assert\Type("string")
     * @var string
     */
    public $apoderado;

    /**
     * @Assert\NotBlank()
     * @Assert\Length(min="8", max="9")
     */
    public $telefono_apoderado;

    public $email_apoderado;

    /**
     * @Assert\Length(min="9", max="9")
     * @Assert\Type("int")
     * @var string
     */
    public $dui_apoderado;

    /**
     * @Assert\NotBlank()
     * @Assert\Length(min="3", max="150")
     * @Assert\Type("string")
     * @var string
     */
    public $centro_educativo_paciente;

    public $archivo_consentimiento;

    public $departamento;

    public $municipio;

    /**
     * @Assert\NotBlank()
     * @Assert\Length(max="100")
     * @var string
     */
    public $direccion;

    /**
     * @Assert\NotBlank()
     * @Assert\Length(max="100")
     * @var string
     */
    public $residencia;

    public $direccion_tipo;

    public function isMenor() {
        $actual = new DateTime();
		return (($actual->diff($this->fecha_nacimiento)->y)<18);
	}
}
