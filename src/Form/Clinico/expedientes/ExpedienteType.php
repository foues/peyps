<?php

namespace App\Form\Clinico\expedientes;

use App\Entity\Administracion\Catalogo;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

//archivos
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Validator\Constraints\File;

class ExpedienteType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        switch ($options['flow_step']) {
            case 1:
                $builder
                    ->add('nombres')
                    ->add('apellidos')
                    ->add('fecha_nacimiento', DateType::class, ['widget' => 'single_text',])
                    ->add('sexo', EntityType::class, $this->getCatalog('t.codigo = \'sexo\'', 'Sexo'))
                    ->add('telefono_casa')
                    ->add('telefono_movil')
                    ->add('email')
                    ->add('nivel_educativo', EntityType::class, $this->getCatalog('t.codigo = \'nivel_academico\'', 'Nivel educativo'))
                    ->add('codigo', null, [
                        'label' => 'Código de expediente',
                        'attr' => ['placeholder' => date('Y') . '-##'],
                    ]);
                break;

            case 2:
                $builder
                    ->add('apoderado', null, [
                        'label' => 'Persona Responsable'
                      ]) //responsable
                    ->add('telefono_apoderado', null, [
                        'label' => 'Telefono Encargado'
                      ])
                    ->add('email_apoderado', null, [
                        'label' => 'Email Encargado'
                      ])
                    ->add('dui_apoderado',  null, [
                        'label' => 'DUI Encargado'
                      ])
                    ->add('centro_educativo_paciente',  null, [
                        'label' => 'Centro educativo Menor'
                      ])
                    // ->add('archivo_consentimiento', FileType::class, [
                    //       'label' => 'Consentimiento (PDF 4MB)',
                    //
                    //       // unmapped means that this field is not associated to any entity property
                    //       'mapped' => false,
                    //
                    //       // make it optional so you don't have to re-upload the PDF file
                    //       // everytime you edit the Product details
                    //       'required' => false,
                    //
                    //       // unmapped fields can't define their validation using annotations
                    //       // in the associated entity, so you can use the PHP constraint classes
                    //       'constraints' => [
                    //           new File([
                    //               'maxSize' => '4096k',
                    //               'mimeTypes' => [
                    //                   'application/pdf',
                    //                   'application/x-pdf',
                    //               ],
                    //               'mimeTypesMessage' => 'Por favor suba un Archivo valido en PDF',
                    //           ])
                    //       ],
                    //   ])
                      ;

                break;

            case 3:
                $builder
                    ->add('dui', null, ['label' => 'DUI'])
                    ->add('estado_civil', EntityType::class, $this->getCatalog('t.codigo = \'estado_civil\'', 'Estado Civil'))
                    ->add('profesion', EntityType::class, $this->getCatalog('t.codigo = \'profesion\'', 'Profesión'))
                    ->add('ocupacion', EntityType::class, $this->getCatalog('t.codigo = \'ocupacion\'', 'Ocupación'))
                    ->add('lugar_trabajo')
                    ->add('telefono_trabajo');
                break;

            case 4:
                $builder
                    ->add('departamento', EntityType::class, $this->getCatalog('t.codigo = \'tipo_area\'', 'Departamento'))
                    ->add('municipio', EntityType::class, $this->getCatalog('t.codigo = \'division_politica\'', 'Municipio'))
                    ->add('direccion')
                    ->add('residencia')
                    ->add('direccion_tipo', EntityType::class, $this->getCatalog('t.codigo = \'tipo_direccion\'', 'Tipo Residencia'));
                break;
        }

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'App\Form\Clinico\expedientes\CrearExpediente',
        ]);
    }

    public function getName()
    {
        return $this->getBlockPrefix();
    }

    public function getBlockPrefix()
    {
        return 'expedienteType';
    }

    private function getCatalog($filter, $label = '')
    {
        return array('class' => Catalogo::Class, 'query_builder' => function (EntityRepository $er) use ($filter) {
            return $er->createQueryBuilder('e')->innerJoin('e.catalogo_tipo', 't')->andWhere($filter);
        },
            'choice_label' => 'nombre',
            'label' => $label,
        );
    }
}
