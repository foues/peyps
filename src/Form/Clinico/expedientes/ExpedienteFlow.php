<?php

namespace App\Form\Clinico\expedientes;

use Symfony\Component\HttpFoundation\Request;
use Craue\FormFlowBundle\Form\FormFlow;
use Craue\FormFlowBundle\Form\FormFlowInterface;


class ExpedienteFlow extends FormFlow {

	protected $allowDynamicStepNavigation = true;

	protected function loadStepsConfig() {
		return array(
			array(
				'label' => 'Personales',
				'form_type' => 'App\Form\Clinico\expedientes\ExpedienteType',
			),
			array(
				'label' => 'Registro Responsable',
				'form_type' => 'App\Form\Clinico\expedientes\ExpedienteType'
				// 'skip' => function($estimatedCurrentStepNumber, FormFlowInterface $flow) {
				// 	return $estimatedCurrentStepNumber > 1 && !$flow->getFormData()->isMenor();
				// },
			),
			array(
				'label' => 'Mayor de Edad',
				'form_type' => 'App\Form\Clinico\expedientes\ExpedienteType',
				'skip' => function($estimatedCurrentStepNumber, FormFlowInterface $flow) {
					return $estimatedCurrentStepNumber > 1 && $flow->getFormData()->isMenor();
				},
			),
			array(
				'label' => 'Direccion',
				'form_type' => 'App\Form\Clinico\expedientes\ExpedienteType',
			),
		);
	}



}
