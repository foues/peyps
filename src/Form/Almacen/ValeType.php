<?php

namespace App\Form\Almacen;

use App\Entity\Almacen\Vale;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ValeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('creado')
            ->add('descripcion')
            ->add('datos')
            ->add('vale_estado')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Vale::class,
        ]);
    }
}
